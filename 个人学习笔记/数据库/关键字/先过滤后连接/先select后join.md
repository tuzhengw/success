---
typora-root-url: images 
---

# 先过滤后连接

# 1 主键过滤删除

# 2 数据库连接

> 注意：`on`：只能筛选<button>子表</button>，<button>主表</button>还得通过`where`条件进行筛选

## 2.1 where先过滤后连接

过滤掉被删除的学生(status = 0 ： 未删除，1：已删除)

```mysql
select *
from score t1
left join student t2
	on t1.studentId = t2.id
where t2.status=0; # 最后对主表进行过滤
```

## 2.2 先连接，最后用where过滤

```mysql
select *
from score t1
left join
	( select * from student where status=0 ) t2  # 先过滤后连接
	on 
		t1.studentId=t2.id
```

## 2.3 两者总结

​	查询优化器会<button>自动进行优化</button>，在连接列有进行条件的筛选时会**先**进行资料的筛选，**再**进行join的动作

赞。

​	提示：可以自行通过：查询分析器中进行分析。在查询分析器中，按一下` Ctrl + L` ，看看执行计划， 两个语句的效率是一样的，最终都转换同样的语句。

---

## 2.4 on和where条件的区别

（1）**on条件（针对子表）**：是在**生成临时表**时使用的条件，它不管on中的条件是否为真，都会返回左边表中的记录。

（2）**where条件（针对主表）**：是在临时表生成好后，再对临时表进行过滤的条件。这时已经没有left join的含义（必须返回左边表的记录）了，**条件不为真的就全部过滤掉**。

----

`eg`：

![](../images/表.png)

```mysql
select * form 
	tab1 
	left join tab2 
	on 
		(tab1.size = tab2.size and tab2.name=’AAA’)
```

![](../images/表3.png)

```mysql
select * form 
	tab1 
	left join tab2 
	on 
		(tab1.size = tab2.size) 
	where
    	tab2.name=’AAA’
```

![](../images/表2.png)

# 3 数据库基本操作

## 3.1 查询

```ts
# 防止SQL注入
let sq1: string = `select * from scdzphz.fact_dzphzsqb where SQID=?`;
# 第二参数为：数组
let queryDzphzInfo = table.executeQuery(sq1, [sqid])
```



