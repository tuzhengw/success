/**
 * ============================================================
 * 作者: tuzhengw
 * 审核人员：liuyz
 * 创建日期：2021-01-11
 * 功能描述：
 *      1. 获取规则表达式
 *      2. 初始化规则相关表信息，并获取执行任务的ID值
 * 帖子地址：https://jira.succez.com/browse/CSTM-17594
 * ============================================================
 */
import dw from "svr-api/dw";
import db from "svr-api/db";
import security from 'svr-api/security';
import util from "svr-api/utils";

const CHECK_TASK_RES_TABLE = "SZSYS_4_DG_CHECK_TASK_TABLES";

/** 返回信息 */
interface ResultInfo {
    /** 返回结果 */
    result: boolean;
    /** 消息 */
    message?: string;
    /** 返回数据 */
    data?: any;
    [propname: string]: any;
}

/**
 * 查询DG_RULES_LIB表获取待治理表ID和对应的别名
 * @param request
 * @param response
 * @param params
 */
export function getRuleTableInfos(request: HttpServletRequest, response: HttpServletResponse, params: any) {
    const query: QueryInfo = {
        sources: [{
            id: "model1",
            path: "/sysdata/data/tables/dg/DG_RULES_LIB.tbl"
        }],
        fields: [{
            name: "MAIN_TABLE_ID", exp: "model1.MAIN_TABLE_ID"
        }, {
            name: "ALIAS", exp: "model1.ALIAS"
        }, {
            name: "RULES_LIB_NAME", exp: "model1.RULES_LIB_NAME"
        }]
    }
    return dw.queryData(query).data;
}

/**
 * 根据tableId查询对应table字段信息
 * @param request
 * @param response
 * @param params
 */
export function getFieldInfos(request: HttpServletRequest, response: HttpServletResponse, params: any) {
    const tableId = params.tableId;
    const isDimension = params.isDimension;
    const tableInfo = dw.getDwTable(tableId);
    if (isDimension) {  // 拼接表的维度信息和度量信息
        return tableInfo.dimensions;
    } else {
        return tableInfo.measures
    }
}

/**
 * 获取当前所有检测字段的字段信息
 * @param request
 * @param response
 * @param params
 */
export function getAllTableFieldInfos(request: HttpServletRequest, response: HttpServletResponse, params: any) {
    let json: JSONObject = {};
    const query: QueryInfo = {
        sources: [{
            id: "model1",
            path: "/sysdata/data/tables/dg/DG_RULES_LIB.tbl"
        }],
        fields: [{
            name: "MAIN_TABLE_ID", exp: "model1.MAIN_TABLE_ID"
        }, {
            name: "ALIAS", exp: "model1.ALIAS"
        }]
    };
    let data = dw.queryData(query).data;
    for (let i = 0; i < data.length; i++) {
        let d = data[i];
        let tableId = d[0] as string;
        let sourceId = d[1] as string;
        let tableInfo = dw.getDwTable(`${tableId.trim()}`);
        json[sourceId] = { dimensions: tableInfo.dimensions, measures: tableInfo.measures };
    }
    return json;
}

/**
 * 新建检测资源检查资源是否有主键
 */
export function checkResidPrimaryKey(request: HttpServletRequest, response: HttpServletResponse, params: any) {
    let resid = params.resid;
    if (hasPrimaryKey(resid)) {
        return true;
    }
    return false;
}

/**
 * 判断一个模型表是否有主键
 */
function hasPrimaryKey(resid) {
    let model = dw.getDwTable(resid);
    if (!model) {
        return false;
    }
    let pk = model.properties && model.properties.primaryKeys;
    if (pk && pk.length > 0) {
        return true;
    }
    return false;
}

/**
 * 往检测任务中添加资源和规则，仅限标准检测任务
 */
export function addResToTask(request: HttpServletRequest, response: HttpServletResponse, params: any) {
    let catalog = params.catalog;
    let resid = params.resid;
    let bm = params.bm;
    let rule_lib_id = params.rule_lib_id;
    let ds = db.getDefaultDataSource();
    let catalogTable = ds.openTableData("DIM_CHECK_RES_CAT");
    let result = catalogTable.selectRows("STD_LIB_ID", { "ID": catalog });
    if (result.length == 0 || !result[0][0]) {
        return { success: true };
    }
    let task_id = result[0][0];
    let checkTaskRuleTable = ds.openTableData("SZSYS_4_DG_CHECK_TASK_RULES");
    let checkTaskResTable = ds.openTableData(CHECK_TASK_RES_TABLE);
    let std_task_id = "STD_" + task_id;
    let resultRes = checkTaskResTable.selectRows("*", { "CHECK_TASK_ID": std_task_id, "DW_TABLE_ID": resid });
    if (resultRes.length > 0) {
        return { success: false, message: "该资源已经存在，不能重复添加" }
    }
    checkTaskResTable.insert({ "CHECK_TASK_ID": std_task_id, "DW_TABLE_ID": resid, "ALIAS": bm, "GZKDM": rule_lib_id });
    checkTaskRuleTable.insert([{ "CHECK_TASK_ID": std_task_id, "RULE_ID": "STRUCT", "DW_TABLE_ID": resid, "RULE_FROM": 'STD' },
    { "CHECK_TASK_ID": std_task_id, "RULE_ID": "VALUE_NOT_EMPTY", "DW_TABLE_ID": resid, "RULE_FROM": 'STD' },
    { "CHECK_TASK_ID": std_task_id, "RULE_ID": "VALUE_RANGE", "DW_TABLE_ID": resid, "RULE_FROM": 'STD' }]);
    return { success: true };
}

/**
 * 删除某个检测任务
 */
export function deleteTask(request: HttpServletRequest, response: HttpServletResponse, params: any) {
    let task_id = params.task_id;
    let ds = db.getDefaultDataSource();
    let checkTaskTable = ds.openTableData("SZSYS_4_DG_CHECK_TASKS");
    let checkTaskRuleTable = ds.openTableData("SZSYS_4_DG_CHECK_TASK_RULES");
    let checkTaskResTable = ds.openTableData(CHECK_TASK_RES_TABLE);
    checkTaskTable.del({ "CHECK_TASK_ID": task_id });
    checkTaskRuleTable.del({ "CHECK_TASK_ID": task_id });
    checkTaskResTable.del({ "CHECK_TASK_ID": task_id });
    return { success: true };
}

/**
 * 同步标准实例表到数据标准检测任务中，同步流程如下
 * 1.根据传入的数据标准库id查询出所有的实例表
 * 2.判断这些实例表是否以及被添加到检测任务中
 * 2.1 如果某个实例表不存在于检测资源中，则将该资源新增到检测资源中。
 * 2.2 如果该检测资源不存在于实例表中，则将该检测资源删除掉。
 * 2.3 其余的资源不做任何修改
 * @param request
 * @param response
 * @param params
 */
export function syncDataStandardTable(request: HttpServletRequest, response: HttpServletResponse, params: any) {
    let standard_id = params.standard_id;
    let result_lib_res = getStandardInstTable([standard_id]);
    let check_tables = getCheckTables(standard_id);
    let needDeleteTable = [].concat(check_tables);
    let needAddTable = [];
    for (let i = 0; i < result_lib_res.length; i++) {
        let res = result_lib_res[i];
        if (check_tables.indexOf(res[1]) == -1) {
            needAddTable.push(res);
        } else {
            needDeleteTable.remove(res[1]);
        }
    }
    deleteCheckTable(needDeleteTable);
    needAddTable.length > 0 && addCheckTable(needAddTable, standard_id);
    return {
        result: true, needAddTable: needAddTable, needDeleteTable: needDeleteTable
    }
}

/**
 * 删除掉多余的检测资源
 * @param tableList
 */
function deleteCheckTable(tableList) {
    if (tableList.length > 0) {
        let ds = db.getDefaultDataSource();
        let checkTaskTable = ds.openTableData(CHECK_TASK_RES_TABLE);
        let checkRuleTable = ds.openTableData('SZSYS_4_DG_CHECK_TASK_RULES');
        let filter = `DW_TABLE_ID in (${tableList.join(",")})`
        checkTaskTable.del(filter);
        checkRuleTable.del(filter);
    }
}

/**
 * 根据标准库ID获取对应所有的实例表
 * @param standardList
 */
function getStandardInstTable(standardList) {
    let result_lib_res = dw.queryDwTableData({
        sources: [{
            id: 'model1',
            path: "/sysdata/data/tables/dg/DG_STD_TABLES_INST.tbl"//标准实例关联表
        }, {
            id: "model2",
            path: "/sysdata/data/tables/meta/META_FILES.tbl"//文件表
        }, {
            id: "model3",
            path: "/sysdata/data/tables/dg/DG_STD_TABLES.tbl"//标准内容表
        }],
        fields: [{
            name: "STD_LIB_ID",
            exp: `model3.STD_LIB_ID`
        }, {
            name: "INST_TABLE_ID",
            exp: `model1.INST_TABLE_ID`
        }, {
            name: "NAME",
            exp: `model2.NAME`
        }, {
            name: "DESC",
            exp: `model2.DESC`
        }, {
            name: "PARENT_DIR",
            exp: `model2.PARENT_DIR`
        }, {
            name: "STD_TABLE_FOLD_ID",
            exp: `model3.STD_TABLE_FOLD_ID`
        }, {
            name: "STD_TABLE_ID",
            exp: `model3.STD_TABLE_ID`
        }],
        joinConditions: [{
            leftTable: 'model1',
            rightTable: 'model2',
            joinType: "LeftJoin",
            clauses: [{
                leftExp: 'model1.INST_TABLE_ID',
                operator: ClauseOperatorType.Equal,
                rightExp: 'model2.ID'
            }]
        }, {
            leftTable: 'model1',
            rightTable: 'model3',
            joinType: "LeftJoin",
            clauses: [{
                leftExp: 'model1.STD_TABLE_ID',
                operator: ClauseOperatorType.Equal,
                rightExp: 'model3.STD_TABLE_ID'
            }]
        }],
        filter: [{
            clauses: [{
                leftExp: 'model3.STD_LIB_ID',
                operator: ClauseOperatorType.In,
                rightValue: standardList.join(",")
            }]
        }, {
            clauses: [{
                leftExp: 'model2.TYPE',
                operator: ClauseOperatorType.Equal,
                rightValue: "tbl"
            }]
        }],
        select: true
    });
    return result_lib_res;
}

/**
 * 获取检测的表
 */
function getCheckTables(standard_id) {
    let check_res = dw.queryDwTableData({
        sources: [{
            id: 'model1',
            path: "/sysdata/data/tables/dg/DG_CHECK_TASK_TABLES.tbl"
        }],
        fields: [{
            name: "DW_TABLE_ID",
            exp: "model1.DW_TABLE_ID"
        }],
        filter: [{
            clauses: [{
                leftExp: `model1.CHECK_TASK_ID`,
                operator: ClauseOperatorType.Equal,
                rightValue: "STD_" + standard_id
            }]
        }]
    });
    let check_tables = []
    for (let i = 0; i < check_res.length; i++) {
        check_tables.push(check_res[i][0]);
    }
    return check_tables;
}

/** 规则信息 */
interface DG_CHECK_TASK_INFO {
    /** 任务编码 */
    CHECK_TASK_ID: string;
    /** 规则代码 */
    RULE_ID: string;
    /** 检测资源元数据id */
    DW_TABLE_ID: string;
    /** 规则来源（关联：DIM_GOVERN_RULE_TYPE） */
    RULE_FROM: string;
}

/** 数据源 */
const DATA_BASE_NAME = "default";
// const SCHEM = "basp";
const dataSource = db.getDataSource(DATA_BASE_NAME);

/**
 * 20220111 tuzw
 * 处理规则表信息
 * @param resid 检测的模型表【resid】
 * @param checkTaskRulesData 检测的规则信息
 * @param checkType 检测的类型，eg：temp：临时、apart：部分、all：全部
 * @param sourceIds 资源编码
 * @param filterDate 过滤时间
 * @param executeNums 随机检测多少条数据
 * @parma isIncrement 是否增量，eg：0 | 1
 * @param easyFieldFilterExp 多字段过滤表达式
 * @param customFilterExp 自定义过滤表达式
 * @param checkResultInputModelPath 检测结果输出的模型路径，默认当前检测表的数据名
 * @param executeExistCheckTaskId 已经存在的检测任务ID
 * 
 * @return { result: true,  checkTaskId: "xxxxxx" }
 */
export function dealRuleTable(request: HttpServletRequest, response: HttpServletResponse, params: {
    resid: string,
    checkTaskRulesData,
    checkType: string,
    sourceIds: string,
    filterDate: string,
    executeNums: number,
    isIncrement: number,
    easyFieldFilterExp: string,
    customFilterExp: string,
    checkResultInputModelPath: string,
    executeExistCheckTaskId: string
}): ResultInfo {
    console.debug(`---dealRuleTable()，处理规则表信息，params：${params}--start`);
    let resultInfo: ResultInfo = { result: true };

    let resid = params.resid.trim();
    let checkType: string = params.checkType.trim();
    let sourceIds: string = params.sourceIds.trim();
    let executeExistCheckTaskId: string = params.executeExistCheckTaskId;

    if (!resid || !checkType || !sourceIds) {
        return { result: false, message: `执行后端参数错误，params：${params}` }
    }

    let { sourceAlias, rulesLibId } = getSourceAlias(resid);
    if (!sourceAlias || !rulesLibId) {
        return { result: false, message: "资源引入没有获取到【规则库编码】、【别名】，请检测系统规则库" };
    }
    let { dealCheckTaskResult, checkTaskId } = dealCheckTaskTable(checkType, executeExistCheckTaskId);
    if (!dealCheckTaskResult) {
        return { result: false, message: "新增 | 修改【检测任务】表信息失败" };
    }

    let checkTaskRulesData = params.checkTaskRulesData;
    if (checkTaskRulesData.length == 0 && checkType != "apart") { // 检测当前检测表所有规则
        console.debug(`当前检测类型为：${checkType}，获取当前规则库编码：【${rulesLibId}】对应检测表的所有规则`);
        checkTaskRulesData = getAllCheckRule(rulesLibId);
    }

    let rules: DG_CHECK_TASK_INFO[] = [];
    for (let i = 0; i < checkTaskRulesData.length; i++) {
        let isEnable = checkTaskRulesData[i]["ENABLE"];
        if (!!isEnable && isEnable != 1) {
            continue;
        }
        rules.push({
            "CHECK_TASK_ID": checkTaskId,
            "RULE_ID": checkTaskRulesData[i]["RULE_ID"],
            "DW_TABLE_ID": resid,
            "RULE_FROM": "CSTM",
        });
    }
    if (rules.length == 0) {
        return { result: false, message: "没有要执行的规则" };
    }
    resultInfo = dealDgCheckTaskRules(rules, checkTaskId);
    if (!resultInfo.result) {
        resultInfo.message = "修改【检测任务规则信息】表的规则信息失败";
        return resultInfo;
    }

    console.debug(`开始处理【检测任务的表信息】表信息，设置：检测范围，检测结果输出路径，增量方式等信息`);
    let filterDate: string = params.filterDate;
    let executeNums: number = params.executeNums;
    let easyFieldFilterExp: string = params.easyFieldFilterExp;
    let customFilterExp: string = params.customFilterExp;
    let { setCheckFilterResult, checkFilterCondition } = getCheckFilterCondition(filterDate, executeNums, easyFieldFilterExp, customFilterExp, resid);
    if (!setCheckFilterResult) {
        return { result: false, message: `${resid} 没有设置主键字段，无法设置主键过滤` };
    }
    let isIncrement: number = params.isIncrement;
    let checkResultInputModelPath: string = params.checkResultInputModelPath;
    resultInfo = dealDgCheckTaskTables(resid, sourceAlias, checkTaskId, checkFilterCondition, isIncrement, checkResultInputModelPath);
    if (!resultInfo.result) {
        resultInfo.message = "初始化【检测任务的表信息】表信息失败";
        return resultInfo;
    }

    resultInfo = dealRulesLibRefs(resid, sourceAlias, rulesLibId, sourceIds);
    if (!resultInfo.result) { // 后续放到检测资源处理，todo
        resultInfo.message = "初始化【规则库引用信息】表信息失败";
        return resultInfo;
    }
    resultInfo.data = { "checkTaskId": checkTaskId };
    console.debug(`---dealRuleTable()，处理规则表信息，检测的任务id：${checkTaskId}--end`);
    return resultInfo;
}

/** 检测规则 */
interface CheckRule {
    /** 规则编码 */
    RULE_ID: string;
    /** 是否启用 */
    ENABLE?: string;
}

/**
 * 若检测类型为：all，则通过当前检测资源对应的【规则库编码】过滤，获得配置的所有规则信息
 * @param rulesLibId 规则库编码
 * @return []
 */
function getAllCheckRule(rulesLibId: string): CheckRule[] {
    let physicalTableName: string = "SZSYS_4_DG_RULES";
    let checkRule: CheckRule[] = [];
    try {
        let table: TableData = dataSource.openTableData(physicalTableName);
        checkRule = table.select(["RULE_ID", "ENABLE"], { "RULES_LIB_ID": rulesLibId }) as CheckRule[];
    } catch (e) {
        console.error(`--getAllCheckRule()，获取${rulesLibId}所包含的全部规则失败--error`);
        console.error(e);
        throw Error(e);
    }
    return checkRule;
}


/**
 * 新增 | 修改【检测任务】表信息
 * @param checkType 检测的类型
 * @param checkTaskId 检测任务ID
 * @return { result: true }
 */
function dealCheckTaskTable(checkType: string, checkTaskId: string): {
    dealCheckTaskResult: boolean,
    checkTaskId: string
} {
    console.debug(`---新增 | 修改【检测任务】，dealCheckTaskTable()--start`);
    let executeResult: boolean = true;
    let physicalTableName: string = "szsys_4_dg_check_tasks";

    let scheduleCode: string = "医院病案管理系统";
    if (checkType == "temp") { // 若是临时计划，则添加后缀
        if (!checkTaskId) {
            checkTaskId = util.uuid();
            checkTaskId = `temp_${checkTaskId}`;
        }
        scheduleCode = `temp_${scheduleCode}`;
    } else {
        if (!checkTaskId) {
            checkTaskId = util.uuid();
        }
    }
    console.debug(`CHECK_TASK_ID：${checkTaskId}`);
    try {
        let table: TableData = dataSource.openTableData(physicalTableName);
        let checkContentIsNullSQL: string = `select count(1) as COUNT from ${physicalTableName} where CHECK_TASK_ID=?`;
        let queryData = table.executeQuery(checkContentIsNullSQL, [checkTaskId]);
        if (Number(queryData[0]["COUNT"]) == 0) {
            table.insert({
                "CHECK_TASK_ID": checkTaskId,
                "CHECK_TASK_NAME": "医院病案管理系统",
                "CHECK_TASK_DESC": "",
                "SCHEDULE_CODE": scheduleCode,
                "CREATE_TIME": new java.sql.Timestamp(new Date().getTime()),
                "CREATOR": security.getCurrentUser().userInfo.userId,
            });
        } else {
            table.update({
                "MODIFY_TIME": new java.sql.Timestamp(new Date().getTime()),
                "MODIFIER": security.getCurrentUser().userInfo.userId,
            }, {
                "CHECK_TASK_ID": checkTaskId
            });
        }
    } catch (e) {
        executeResult = false;
        console.error(`--dealCheckTaskTable()，处理【检测任务】--error`);
        console.error(e);
    }
    console.debug(`---新增 | 修改 【检测任务】，dealCheckTaskTable()--end`);
    return { dealCheckTaskResult: executeResult, checkTaskId: checkTaskId };
}

/**
 * 修改【检测任务规则信息】表的规则信息
 * @param taskRuleData 任务规则数据
 * @param checkTaskId 检测任务id
 * @return { result: true }
 */
function dealDgCheckTaskRules(taskRuleData: Array<DG_CHECK_TASK_INFO>, checkTaskId: string): ResultInfo {
    console.debug(`---修改【检测任务规则信息】表的规则信息，dealDgCheckTaskRules()--start`);
    if (taskRuleData == null || taskRuleData.length == 0) {
        return { result: false, message: "【检测任务规则信息】表的规则信息为NULL" }
    }
    let resultInfo: ResultInfo = { result: true };
    let physicalTableName: string = "szsys_4_dg_check_task_rules";
    try {
        let table: TableData = dataSource.openTableData(physicalTableName);
        table.insert(taskRuleData);
    } catch (e) {
        resultInfo.result = false;
        console.error(`--dealDgCheckTaskRules()，处理【检测任务规则信息】--error`);
        console.error(e);
    }
    console.debug(`---修改【检测任务规则信息】表的规则信息，dealDgCheckTaskRules()--end`);
    return resultInfo;
}

/**
 * 初始化【检测任务的表信息】表信息
 * @param executeDataResid 检测的资源Resid
 * @param sourceAlias 检测资源的别名
 * @param checkTaskId 检测任务ID
 * @param checkFilterCondition 检测数据范围，eg：
 *      主键过滤：USERID in("30a94032d240f2db17fc51c51624eddd","13e832f713bac133042ef7fca59475f7")
        时间过滤：年：CI_LASTUPDATE > "2019"，年月：CI_LASTUPDATE > "2019-01" 年月日：CI_LASTUPDATE > "2019-01-01"
   @param isIncrement 是否增量
   @param checkResultInputModelPath 检测结果输出的模型路径，默认当前检测表的数据名
   注意：
   （1）根据检测表的【最后修改时间】字段进行增量检测，每次检测数据过滤条件依据【检测任务的表信息】的【最后成功检测时间】字段
 * @return { result: true }
 */
function dealDgCheckTaskTables(executeDataResid: string, sourceAlias: string, checkTaskId: string, checkFilterCondition: string, isIncrement: number, checkResultInputModelPath: string): ResultInfo {
    console.debug(`---初始化【检测任务的表信息】表信息，dealDgCheckTaskTables()--start`);
    let resultInfo: ResultInfo = { result: true };
    let physicalTableName: string = "szsys_4_dg_check_task_tables";
    if (!isIncrement) { // 若未指定，则默认：0
        isIncrement = 0;
    }
    try {
        let table: TableData = dataSource.openTableData(physicalTableName);
        let checkContentIsNullSQL: string = `select count(1) as COUNT from ${physicalTableName} where CHECK_TASK_ID=?`;
        let queryData = table.executeQuery(checkContentIsNullSQL, [checkTaskId]);
        if (Number(queryData[0]["COUNT"]) == 0) {
            console.debug(`当前检测任务的表信息：${checkTaskId} 不存在，插入`);
            table.insert({
                "CHECK_TASK_ID": checkTaskId,
                "DW_TABLE_ID": executeDataResid,
                /**
                 * 20220223 tuzw
                 * 该字段过滤条件可能很长，字段类型改成：clob
                 */
                "DATA_FILTER": checkFilterCondition,
                "DATA_FILTER_DESC": "",
                "IS_INCREMENT": isIncrement,
                "LAST_CHECK_TIME": new java.sql.Timestamp(new Date().getTime()),
                "LAST_CHECK_TIME_EXP": "",
                "ERR_DATAIL_TABLE_PATH": !!checkResultInputModelPath ? checkResultInputModelPath : "",
                "FIELD_VALUES": "",
                "ALIAS": sourceAlias
            });
        } else {
            console.debug(`当前检测任务的表信息：${checkTaskId} 已存在，更新`);
            table.update({
                "DW_TABLE_ID": executeDataResid,
                "DATA_FILTER": checkFilterCondition,
                "DATA_FILTER_DESC": "",
                "IS_INCREMENT": isIncrement,
                "LAST_CHECK_TIME": new java.sql.Timestamp(new Date().getTime()),
                "LAST_CHECK_TIME_EXP": "",
                "ERR_DATAIL_TABLE_PATH": !!checkResultInputModelPath ? checkResultInputModelPath : "",
                "FIELD_VALUES": "",
                "ALIAS": sourceAlias
            }, {
                "CHECK_TASK_ID": checkTaskId
            });
        }
    } catch (e) {
        resultInfo.result = true;
        resultInfo.message = "dealDgCheckTaskTables()，处理【检测任务的表信息】--error"
        console.error(`--dealDgCheckTaskTables()，处理【检测任务的表信息】--error`);
        console.error(e);
    }
    console.debug(`---初始化【检测任务的表信息】表信息，dealDgCheckTaskTables()--end`);
    return resultInfo;
}

/**
 * 初始化【规则库引用信息】表信息
 * 注意：规则引用应该与规则库信息一同设置
 * @param executeDataResid 检测的资源Resid
 * @param sourceAlias 检测的资源别名
 * @param rulesLibId 检测的资源对应规则库编码
 * @param sourceIds 资源编码
 * @return { result: true }
 */
function dealRulesLibRefs(executeDataResid: string, sourceAlias: string, rulesLibId: string, sourceIds: string): ResultInfo {
    console.debug(`---初始化【规则库引用信息】表信息，dealRulesLibRefs()--start`);
    let resultInfo: ResultInfo = { result: true };
    let physicalTableName: string = "szsys_4_dg_rules_lib_refs";
    try {
        let table: TableData = dataSource.openTableData(physicalTableName);
        let checkContentIsNullSql: string = `select count(1) as COUNT from ${physicalTableName} where DW_TABLE_ID=?`;
        let queryData = table.executeQuery(checkContentIsNullSql, [executeDataResid]);
        if (Number(queryData[0]["COUNT"]) == 0) {
            table.insert({
                "DG_REF_ID": sourceIds,
                "RULES_LIB_ID": rulesLibId,
                "DW_TABLE_ID": executeDataResid,
                "DG_REF_ALIAS": sourceAlias,
                "CREATE_TIME": new java.sql.Timestamp(new Date().getTime()),
                "CREATOR": ""
            });
        } else {
            table.update({
                "RULES_LIB_ID": rulesLibId,
                "DW_TABLE_ID": executeDataResid,
                "DG_REF_ALIAS": sourceAlias,
                "CREATE_TIME": new java.sql.Timestamp(new Date().getTime()),
                "CREATOR": ""
            }, {
                "DG_REF_ID": sourceIds,
            });
        }
    } catch (e) {
        resultInfo.result = true;
        console.error(`--dealRulesLibRefs()，处理【规则库引用信息】--error`);
        console.error(e);
    }
    console.debug(`---初始化【规则库引用信息】表信息，dealRulesLibRefs()--end`);
    return resultInfo;
}

/**
 * 获取检测资源表的：【规则库编码】、【别名】
 * @param executeDataResid 模型表Resid
 * @return { sourceAlias: 资源别名, rulesLibId: 规则库编码 }
 */
function getSourceAlias(executeDataResid: string): { sourceAlias: string, rulesLibId: string } {
    let physicalTableName: string = "szsys_4_dg_rules_lib";
    let sourceAlias: string = "";
    let rulesLibId: string = "";
    try {
        let table: TableData = dataSource.openTableData(physicalTableName);
        let queryData = table.select(["RULES_LIB_ID", "ALIAS"], { "MAIN_TABLE_ID": executeDataResid });
        if (queryData != null && queryData.length != 0) {
            rulesLibId = queryData[0].RULES_LIB_ID;
            sourceAlias = queryData[0].ALIAS;
        }
        console.debug(`--[Resid：【${executeDataResid}】对应的资源：规则库编码：【${rulesLibId}】、别名：【${sourceAlias}】`);
    } catch (e) {
        console.error(`--getSourceAlias()，获取检测资源表的：【规则库编码】、【别名】--error`);
        console.error(e);
        throw Error(e);
    }
    return { "sourceAlias": sourceAlias, "rulesLibId": rulesLibId };
}

/**
 * 设置检测任务的检测数据范围
 * 注意：
 * （1）过滤字段是静态的，若需要调整，需要修改对应的字段信息
 * @param filterDate 时间范围，eg: 20220102
 * @param executeNums 检测随机数量
 * @param easyFieldFilterExp 多字段过滤表达式
 * @param customFilterExp 自定义过滤表达式
 * @param checkModelResid 检测的模式resid
 * @return
 */
function getCheckFilterCondition(filterDate: string, executeNums: number, easyFieldFilterExp: string, customFilterExp: string, checkModelResid: string): {
    setCheckFilterResult: boolean,
    checkFilterCondition?: string
} {
    let checkFilterCondition: string = ""; // 检测表的过滤范围
    if (!!filterDate) { // 模型时间过滤字段，统一为：FCYTIME
        checkFilterCondition = `FCYDATE = '${filterDate}'`;
    }
    if (!!executeNums) { // 设置随机数量的检测过滤条件
        let relationDbTableName: string = getModelDbTableName(checkModelResid);
        let { datasource, schema } = getModelDataSourceAndSchema(checkModelResid);
        let filterPkFiled: string = getTablePrimaryKeys(datasource, relationDbTableName)[0]; // 多个，取首个
        if (filterPkFiled == "") {
            console.debug(`表【${relationDbTableName}】没有主键`);
            return { setCheckFilterResult: false };
        }
        let filterPks: string[] = [];
        if (!!filterDate) {
            filterPks = getModelPkValues(datasource, relationDbTableName, filterPkFiled, executeNums, `FCYDATE = '${filterDate}'`, schema);
        } else {
            filterPks = getModelPkValues(datasource, relationDbTableName, filterPkFiled, executeNums, schema);
        }
        if (filterPks.length > 0 && checkFilterCondition == "") {
            checkFilterCondition = `${filterPkFiled} in ('${filterPks.join("','")}')`;
        } else {
            if (filterPks.length > 0) {
                checkFilterCondition = `${checkFilterCondition} and ${filterPkFiled} in ('${filterPks.join("','")}')`;
            }
        }
    }
    if (checkFilterCondition == "") { // 设置多字段过滤表达式
        checkFilterCondition = easyFieldFilterExp;
    } else {
        if (easyFieldFilterExp != "") {
            checkFilterCondition = `${checkFilterCondition} and ${easyFieldFilterExp}`;
        }
    }
    if (checkFilterCondition == "") { // 设置自定义过滤表达式
        checkFilterCondition = customFilterExp;
    } else {
        if (customFilterExp != "") {
            checkFilterCondition = `${checkFilterCondition} and ${customFilterExp}`;
        }
    }
    console.debug(`检测任务的检测数据范围：${checkFilterCondition}`);
    return { setCheckFilterResult: true, checkFilterCondition: checkFilterCondition };
}

/**
 * 获取指定模型所在的数据源名
 * @param modelResid 模型的id值
 * @return 数据源名
 */
function getModelDataSourceAndSchema(modelResid: string): { datasource: string, schema: string } {
    if (!modelResid) {
        return null;
    }
    let model: DwTableInfo = dw.getDwTable(modelResid);
    if (!model) {
        return null;
    }
    let dataSource: string = model.properties && model.properties.datasource;
    let schema: string = model.properties && model.properties.dbSchema;
    if (dataSource == "defdw") { // 默认数据源在配置文件中名字不同，需要校验
        dataSource = "default";
    }
    console.debug(`【${modelResid}】的所在数据源名为：${dataSource}，schema：${schema}`);
    return { datasource: dataSource, schema: schema };
}

/**
 * 获取指定模型关联的物理表名
 * @param modelResid 模型的id值
 * @return 物理表名
 */
function getModelDbTableName(modelResid: string): string {
    if (!modelResid) {
        return null;
    }
    let model: DwTableInfo = dw.getDwTable(modelResid);
    if (!model) {
        return null;
    }
    let dbTableName: string = model.properties && model.properties.dbTableName;
    console.debug(`【${modelResid}】的关联的物理表名为：${dbTableName}`);
    return dbTableName;
}

/**
 * 获取指定模型的主键
 * @param dataBase 数据源名
 * @param tableName 表名
 * @return []
 */
function getTablePrimaryKeys(dataBase: string, tableName: string, schema?: string): string[] {
    if (dataBase == null || tableName == null) {
        return [];
    }
    let ds = db.getDataSource(dataBase);
    if (!schema) {
        schema = ds.getDefaultSchema();
    }
    let dsMeta = ds.getTableMetaData(tableName, schema);
    let keys = dsMeta.getPrimaryKey();
    let pks = [];
    for (let i = 0; keys && i < keys.length; i++) {
        pks.push(keys[i]);
    }
    console.debug(`getTablePrimaryKeys()，【${tableName}】表的主键为：${pks}`);
    return pks.length == 0 ? null : pks;
}

/**
 * 获取指定数据源下的某表，随机N条数据的主键值
 * @param dataSource 数据源名
 * @param pyTableName 物理表名
 * @param pkFieldName 查询的主键字段
 * @param randomNumber 随机查询的条数
 * @param filterCondtionExp 随机的范围过滤条件，eg："ID='360825'"
 * @param schem 
 * @return [] 主键值
 */
function getModelPkValues(dataSource: string, pyTableName: string, pkFieldName: string, randomNumber: number, filterCondtionExp?: string, schem?: string): string[] {
    let ds = db.getDataSource(dataSource);
    if (!schem) {
        schem = ds.getDefaultSchema();
    }
    let pks: string[] = [];
    let preparestatement = null;
    let conn = ds.getConnection();
    try {
        let randomQuerySQL: string = ""
        /**
         * 20220223 tuzw
         * 注意：产品的selectQuery方法查出的结果值是相同的，这里改为原生JDBC写法
         */
        if (!!filterCondtionExp) {
            randomQuerySQL = `SELECT ${pkFieldName} FROM  ${schem}.${pyTableName} where ${filterCondtionExp}  ORDER BY RAND() LIMIT ?`; // 随机查询N条数据的SQL
        } else {
            randomQuerySQL = `SELECT ${pkFieldName} FROM  ${schem}.${pyTableName}   ORDER BY RAND() LIMIT ?`; // 随机查询N条数据的SQL
        }
        preparestatement = conn.prepareStatement(randomQuerySQL);
        preparestatement.setInt(1, randomNumber);
        let queryData = preparestatement.executeQuery();
        while (queryData.next()) {
            let pk = queryData.getString(1);
            pks.push(pk);
        }
    } catch (e) {
        console.error(`获取指定数据源（${dataSource}）下的${pyTableName}表，随机${randomNumber}条数据的主键值失败`);
        console.error(e);
    } finally {
        conn && conn.close();
        preparestatement && preparestatement.close();
    }
    return pks;
}

import JSONObject = com.alibaba.fastjson.JSONObject;
/**
 * 20220302 tuzw
 * 修改某表的信息
 * @param dataBase 数据库名
 * @param tableName 表名
 * @param updateData 更新数据，JSON格式，eg：[{"ID": 1001 }]
 * @param updateWhere 更新条件，JSON格式，eg：[{"ID": 1001 }]
 * @param shcema
 */
export function updateTableInfo(request: HttpServletRequest, response: HttpServletResponse, params: {
    dataBase: string,
    tableName: string,
    updateData,
    updateWhere,
    schema: string
}): { result: boolean, message?: string } {
    console.debug(`updateTableInfo()，数据数据，params：【${params}】`);
    let dataBase: string = params.dataBase;
    let tableName: string = params.tableName;
    let schema: string = params.schema;
    let updateData = params.updateData;
    let updateWhere = params.updateWhere;

    if (!dataBase || !tableName || !updateData || !updateWhere) {
        return { result: false, message: `updateTableInfo()，修改数据库【${dataBase}】中表【${tableName}】，参数有误，params：${params}` };
    }
    if (updateData.length == 0) {
        return { result: true }
    }
    let ds = db.getDataSource(dataBase);
    if (!schema) {
        schema = ds.getDefaultSchema();
    }
    let table = ds.openTableData(tableName, schema);

    if (updateData.getClass() == "class java.util.LinkedHashMap") { // 由于前端传递的参数是通过JAVA处理的，JSON数组变成了Map，需要将其转换为JSON数组
        updateData = new JSONObject(updateData);
    }
    if (updateWhere.getClass() == "class java.util.LinkedHashMap") {
        updateWhere = new JSONObject(updateWhere);
    }
    let updateNums: number = 0;
    if (!!updateWhere) { // class com.alibaba.fastjson.JSONObject 通过JSON.parse方法转换为前端可识别的JSON格式
        updateNums = table.update(JSON.parse(updateData), JSON.parse(updateWhere));
    }
    console.debug(`updateTableInfo()，修改数据库【${dataBase}】中表【${tableName}】${updateNums}条数据`);
    return { result: true };
}



/** 检测结果对象 */
interface CheckErrorField {
    /** 规则id */
    RULE_ID?: string;
    /** 检测字段 */
    CHECK_FIELD?: string;
    /** 规则名称 */
    RULE_NAME?: string;
}

/**
 * 20220302 tuzw
 * 获取规则检测字段信息
 * 根据检测任务ID，获取【检测结果表】对应规则【检测的字段】信息
 * @param dataBase 数据库名
 * @param tableName 表名
 * @parma checkTaskId 任务ID
 * @param schema 
 * @return {
 *      result: boolean
 *      queryCheckFileds: []
 * }
 */
export function getRuleCheckFields(request: HttpServletRequest, response: HttpServletResponse, params: {
    dataBase: string,
    tableName: string,
    checkTaskId: string,
    schema: string
}): { result: boolean, message?: string, queryCheckFileds?: CheckErrorField[] } {
    console.debug(`getRuleCheckFields()，获取规则检测字段信息，params：${params}`);
    let dataBase: string = params.dataBase;
    let tableName: string = params.tableName;
    let schema: string = params.schema;
    let checkTaskId: string = params.checkTaskId;
    if (!dataBase || !tableName || !checkTaskId) {
        return { result: false, message: `getRuleCheckFields()，获取规则检测字段信息参数有误，params：${params}` };
    }
    let ds = db.getDataSource(dataBase);
    if (!schema) {
        schema = ds.getDefaultSchema();
    }
    let table = ds.openTableData(tableName, schema);
    let queryCheckData = table.select(["RULE_ID"], { "CHECK_TASK_ID": checkTaskId });
    if (queryCheckData.length == 0) {
        return { result: true };
    }
    let ruleIds: string[] = [];
    for (let i = 0; i < queryCheckData.length; i++) {
        ruleIds.push(queryCheckData[i]["RULE_ID"]);
    }
    if (ruleIds.length == 0) {
        return { result: false, message: `getRuleCheckFields()，未获取到检测结果表，检测ID【${checkTaskId}】对应的规则信息` };;
    }
    let queryCheckFiledSQL: string = `select RULE_ID, CHECK_FIELD, RULE_NAME from baglxt.szsys_4_dg_rules where RULE_ID in('${ruleIds.join("','")}')`;
    console.debug(`查询的SQL：${queryCheckFiledSQL}`);
    let ruleTable = ds.openTableData("szsys_4_dg_rules", "baglxt");
    let queryCheckFileds = ruleTable.executeQuery(queryCheckFiledSQL) as CheckErrorField[];
    console.debug(`getRuleCheckFields()，获取规则检测字段信息结束`);
    return { result: true, queryCheckFileds: queryCheckFileds };
}