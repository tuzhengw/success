import {
    ctx, SZEvent, Component, throwInfo, throwError, showConfirmDialog, showFormDialog, showErrorMessage, showMenu, isEmpty, deepAssign, uuid, message,
    quoteExpField, showDialog, showSuccessMessage, showProgressDialog, rc, downloadFile, waitRender, showWaiting, assign, IDataProvider, formatDateFriendly,
    parseDate, formatDate, tryWait, rc_get, showWarningMessage, ServiceTaskPromise, wait
} from 'sys/sys';

 waitPromise = showWaiting(promise, document.body, true, { layoutTheme: customWaitStyle });
 
 
 /** 
   自定义等待框样式（showWaiting）
   帖子地址：https://jira.succez.com/browse/CSTM-18520
 */
.waiting-ring-active {
    width: 400px! important;
    height: 230px !important;
    background-image: url(images/SY/jz2.gif)  !important ;
    object-fit: cover !important ;
    background-repeat: no-repeat !important;
    background-position: center !important;
    // background-size: 100% !important;
    border: 1px solid rgb(221, 219, 219) !important;
    box-shadow: 0 1px 8px 0 rgb(0 0 0 / 40%) !important;
}
.waiting-ring .progressring-base {
    border-radius: 0% !important;
    background-color: transparent !important;
}
.waiting-ring-active .progressring-ring {
    display: none !important;
}
.waiting-ring-active .progressbar-loop {
    display: none !important;
}
.waiting-ring-active .progressring-circle { // 隐藏"转圈圈"样式
    display: none !important;
}