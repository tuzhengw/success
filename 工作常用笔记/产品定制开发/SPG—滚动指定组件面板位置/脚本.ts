/**
 * ========================================================
 * 作者：liuyz
 * 创建日期：2022-01-23
 * 脚本用途：
 *      处理数据交换上报、生成工单等逻辑
 * ========================================================
 */
import { MetaFileViewer_miniapp } from "app/miniapp.js";
import { SpgEmbedSuperPage } from "app/superpage/components/embed";
import { SuperPage } from "app/superpage/superpage.js";
import { SuperPageData } from "app/superpage/superpagedatamgr";
import { Dialog, showFormDialog } from "commons/dialog";
import { ExportReusltType, html2pdf } from "commons/export";
import { ExpDialog, IFieldsDataProvider, showExpDialog } from 'commons/exp/expdialog';
import { ExpContentType } from 'commons/exp/exped';
import { ProgressPanel } from "commons/progress";
import { getQueryManager, pushData } from "dw/dwapi";
import { getCurrentUser, getMetaRepository, ResourceTreeDataProvider } from "metadata/metadata";
import { InterActionEvent } from "metadata/metadata-script-api";
import { showResourceDialog } from "metadata/metamgr";
import {
	Component, formatDate, isEmpty, parseDate, rc, showConfirmDialog, showErrorMessage, showProgressDialog, showSuccessMessage, showWaiting, hideWaiting,
	SZEvent, uuid, showWarningMessage, assign, IDataProvider, message, copyToClipboard, deepEqual, EventListenerManager, GroupInfo, IFindArgs, ItemDataConfInfo, wait
} from "sys/sys";
import {
	CustomJs_List, CustomJs_List_Head, getAppPath, showScheduleDialog, CustomExpDp, openCustomResourceDialog, SysLogUtils
} from '../commons/commons.js?v=202202141716';
import { getScheduleMgr, ScheduleLogInfo, ScheduleMgr, TaskLogInfo } from "schedule/schedulemgr";
import { Button, ComboboxArgs, MultipleInputArgs } from "commons/basic";
import { Form, FormItemArgs } from "commons/form";
import { DbConf, parseJdbcUrl, SUPPORTED_DB_CONFS } from "datasources/datasourcesdata";
import { Spg_Mis_DataBase } from "../mis-mgr/mis-mgr.js?v=166003142803242";
import { DwTableModelFieldProvider } from "dw/dwdps";
import { DataProvider } from "commons/basic";
import { TableEditor } from "commons/table";
import { SpgPanel } from "app/superpage/components/layout";
import { SpgText } from "app/superpage/components/others";



/**
 * 数据交互-API接口文档详情
 */
export class Spg_Dx_Api_Doc {

	public CustomActions: any;

	constructor() {
		this.CustomActions = {
			button_localSignCompoent: this.button_localSignCompoent.bind(this),
			button_downloadApi: this.button_downloadApi.bind(this)
		}
	}

	/**
	 * 2022-11-09 tuzw
	 * 点击左侧目录节点，自动定位到指定位置
	 * @param titleName 定位标题中文名, eg: 1.3. 平台地址
	 */
	private button_localSignCompoent(event: InterActionEvent): void {
		let page = event.page;
		let params = event.params as JSONObject;
		let titleName: string = params?.titleName;
		if (isEmpty(titleName)) {
			showWarningMessage(`请选择左侧目录标题`);
			return;
		}
		titleName = titleName.replaceAll(" ", "");

		let localCompId: string = "";
		let contentPanel = page.getComponent("canvas"); // SPG最外层画布ID
		let allChildrenComponents = contentPanel.node.builder.getComponents(); // 获得指定面板内的所有子控件
		if (!allChildrenComponents || allChildrenComponents.length == 0) {
			showWarningMessage(`未获取相关输入控件`);
			return;
		}
		for (let i = 0; i < allChildrenComponents.length && localCompId == ""; i++) { // 遍历当前SPG所有控件, 直到寻找完成 或 匹配成功
			let component = allChildrenComponents[i];
			let metaInfo = component.metaInfo;
			let metaInfoId: string = metaInfo.id;
			let compValue: string = metaInfo.value;

			if (metaInfoId.indexOf("floatpanel") != -1) { // 若当前控件为浮动面板, 则遍历浮动面板内部子节点
				let floatPanel = page.getComponent(metaInfoId);
				let childrenPanels = floatPanel.component.panels;
				for (let panelIndex = 0; panelIndex < childrenPanels.length; panelIndex++) {
					localCompId = this.matchFloatPanelChildrenId(childrenPanels[panelIndex], titleName);
					if (localCompId != "") { // 若找到则结束匹配
						break;
					}
				}
			}
			if (isEmpty(compValue)) {
				continue;
			}
			let compareValue: string = compValue.replaceAll(" ", "");
			if (compareValue.indexOf(titleName) != -1) { // 避免标题空格不一致，将其空格都去掉
				localCompId = component.id;
				break;
			}
		}
		if (localCompId == "") {
			showWarningMessage("未找到指定内容");
			return;
		}
		page.setParameter("localCompId", localCompId);
	}

	/**
	 * 匹配浮动面板控件的控件值，获取对应控件id
	 * @param parentPanel 匹配控件所在父面板对象
	 * @param compValue 匹配控件value值
	 * @return 
	 */
	private matchFloatPanelChildrenId(parentPanel: SpgPanel, compValue: string): string {
		let matchCompId: string = "";
		let componentsMap = parentPanel.componentsMap;
		for (let childrenComp of componentsMap.values()) {
			let map = childrenComp.componentsMap;
			if (!isEmpty(map)) {
				matchCompId = this.matchFloatPanelChildrenId(childrenComp as SpgPanel, compValue);
				if (matchCompId != "") {
					break;
				}
			}
			let childrenId: string = childrenComp.id;
			if (childrenId.indexOf("text") == -1) { // 判断当前控件是否文本控件
				continue;
			}
			let comp = childrenComp.getInnerComoponent() as SpgText;
			let textValue: string = comp.getValue()?.text?.replaceAll(" ", "");
			if (textValue.indexOf(compValue) != -1) {
				matchCompId = childrenId;
				break;
			}
		}
		return matchCompId;
	}

	/**
	 * 下载api文档
	 * @param importCompId 导出控件ID
	 * @param fileName? 文件名
	 */
	private button_downloadApi(event: InterActionEvent): void {
		let page = event.page;
		let params = event.params;
		let fileName: string = params?.file_name != null
			? `${params?.file_name}_接口文档`
			: 'API接口文档';
		let importCompId: string = params?.importCompId;
		if (isEmpty(importCompId)) {
			showWarningMessage(`未获取到需要导出的控件ID`);
			return;
		}
		let redener = event.renderer;
		let fileInfo = page.getFileInfo();
		let resid = fileInfo.id;
		let importComp = redener?.getComponent(importCompId);
		if (isEmpty(importComp)) {
			showWarningMessage(`导出控件不存在`);
			return;
		}
		let domBase = importComp?.getDomBase() as HTMLElement;
		html2pdf(resid, domBase, fileName, ExportReusltType.DOWNLOAD, { width: (domBase.widthNoPadding + 140), margin: { all: 70 } });
	}
}