/**
 * 去除对象数组的重复值
 * 注意：必须保证对象内没有包含：对象
 * @params obj：需要处理的对象数组
 * @return uniques：去除重复值后的对象数组
 */
function removeRepeatDatas(objArray) {
    let uniques = [];
    let stringify = {};
    for (let i = 0; i < objArray.length; i++) {
        let keys = Object.keys(objArray[i]);
        keys.sort(function (a, b) {
            return (Number(a) - Number(b));
        });
        let str = '';
        for (let j = 0; j < keys.length; j++) {
            str += JSON.stringify(keys[j]);
            str += JSON.stringify(objArray[i][keys[j]]);
        }
        if (!stringify.hasOwnProperty(str)) {
            uniques.push(objArray[i]);
            stringify[str] = true;
        }
    }
    uniques = uniques;
    return uniques;
}