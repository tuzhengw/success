import { IExpEvalDataProvider } from "commons/exp/expeval";
import { BeautifyCode } from "commons/beautify"

/**
 * 美化JSON对象数据, 转换字符串后, 按照JSON本身格式显示在前端
 * 方法优化了JSON.stringify的实现，保留了换行符
 * 
 * @param jsonStr JSON字符串, eg: 
 *  {"cookie":"111","content-length":"208","content-type":"application/json"}
 * 
 * @return 转换后的字符串, 按原JSON对象格式返回, eg: {
    "cookie": "111",
    "content-length": "208",
    "content-type": "application/json"
 * }
 * @description 产品扩展表达式不支持object对象作为参数
 */
export function SDI_BEAUTIFY_JSONDATA_SHOW_FORMAT(context: IExpEvalDataProvider, jsonStr: string): string {
    if (!jsonStr) {
        return "";
    }
    try {
        let jsonObj = JSON.parse(jsonStr);
        let beautifyCode = new BeautifyCode();
        let formatJson = beautifyCode.json(jsonObj, {
            jsonLength: 10, // JSON字符串超过多少长度才会执行格式化操作
            arrayLength: 120
        });
        return formatJson;
    } catch (e) {
        return jsonStr; // 参数不为JSON字符串, 转换失败, 直接返回转换的字符串
    }
}