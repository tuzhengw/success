const DwDataValidateFailHandleEnum = com.succez.dw.data.submit.DwDataValidateFailHandleEnum;

https://jira.succez.com/browse/BI-47751

/**
 * 将处理好的excel数据写入指定模型
 * @param data 写入的数据
 */
private writeDataToModel(data: FieldValue[][]): void {
    let dataPackage: CommonTableSubmitDataPackage = {};
    dataPackage = {
        addRows: [{
            fieldNames: [xxx],
            rows: data
        }],
        // @ts-ignore
        updateConfig: {
            /**
             * updateDwTableData方法走的是提交表单交互, 若校验失败则无法写入, 此处修改写入模式。
             * writeAll: 插入所有数据，如果模型上有设置检验状态和校验结果描述字段，则进行记录)
             */
            validateFail: DwDataValidateFailHandleEnum.values()[2],
            checkUpdateResult: "none" // 关闭更新行校验
        }
    }
    dw.updateDwTableData("xxx/xxx.tbl", dataPackage);
}