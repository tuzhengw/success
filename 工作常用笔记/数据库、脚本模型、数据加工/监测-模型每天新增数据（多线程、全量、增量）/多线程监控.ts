/**
 * 作者：tuzw
 * 创建日期：2022-11-14
 * 脚本用途：脚本实现监控各个地市数据的推送，更新条数，日志记录
 * 地址：https://jira.succez.com/browse/CSTM-21188
 * 
 * 1. 不统计模型不存在 行政区划字段的模型
 * 2. 模型存在字段: ETL_TIME 数据更新标志 则增量，无，则全量
 */
import metadata from "svr-api/metadata";
import db from "svr-api/db";
import dw from "svr-api/dw";
import sys from "svr-api/sys";
import utils from "svr-api/utils";
import { SDILog, ResultInfo } from "/sysdata/app/zt.app/commons/commons.action";

const logger: SDILog = new SDILog({ name: "sdi.monitor" });
const Timestamp = Java.type('java.sql.Timestamp');

/**
 * 批处理脚本模版
 */
function main() {
    monitorModelPushInfo();
}

/**
 * 监控各个地市数据的推送，更新条数，日志记录
 * @description 
 * (1) 处理完一张模型表就写入监控结果日志表
 * (2) 考虑到监测表多, 采用多线程方式监控
 */
function monitorModelPushInfo(): void {
    let startTime: number = Date.now();
    logger.addLog(`开始执行[monitorModelPushInfo], 监控各个地市数据的推送，更新条数，日志记录`);
    let monitor = new MonitorModelInfoMgr({
        monitorModelParentPath: "/sdi/data/tables/etl/SHARE/全省数据—推送/ODS",
        groupField: "XZQH",
        incrementField: "ETL_TIME",
        logger: logger
    });
    let getMonitorModelResult = monitor.execute();
    if (!getMonitorModelResult.result) {
        logger.addLog(getMonitorModelResult);
        return;
    }
    let monitorModelInfos: MonitorModelInfo[] = monitor.getMonitorModelPaths();
    if (monitorModelInfos.length == 0) {
        logger.addLog(`当前没有需要监测的模型信息, 执行结束`);
        return;
    }
    logger.addLog(`获取需要监测的模型完成, 开始执行监测`);

    /** 外部文件控制程序执行 */
    let isStopExecutePath: string = "/sysdata/script/contrlMonitorModelPush.txt";
    let isStop: string = "";

    /** 线程池核心执行数量 */
    let corePoolSize: number = 5;
    let threadPool = sys.getThreadPool({
        name: "sdiExChangeMonitorModelPush", // 线程池中唯一的标识, 若name已经存在，则会获取已存在的线程池
        corePoolSize: 4,
        maximumPoolSize: 5
    });
    /** 正在运行的线程个数 */
    let runingThreadNums: number = 0;
    try {
        let dbSource = db.getDataSource("sjzt_exchange_qs");
        /** 当天已经监测过的模型ID */
        let monitoredModelResids: string[] = queryTodayMonitoredModelIds();
        let sumMonitorNums: number = monitorModelInfos.length;

        for (let modelIndex = 0; modelIndex < sumMonitorNums; modelIndex++) {
            isStop = metadata.getString(isStopExecutePath);
            if (isStop == 'true') {
                logger.addLog(`--监控被手动停止, 还剩[${sumMonitorNums - modelIndex + 1} 个模型未监控], 共: [${sumMonitorNums}] 个模型`);
                break;
            }
            let monitorModelInfo: MonitorModelInfo = monitorModelInfos[modelIndex];
            let modelName: string = monitorModelInfo.modelName;
            logger.addLog(`(${modelIndex + 1}) 开始监控模型: [${modelName}], 共: [${sumMonitorNums}] 个模型`);

            let modelId: string = monitorModelInfo.modelId;
            if (monitoredModelResids.includes(modelId)) {
                logger.addLog(`--模型当天已监测过, 无需再次监测, 越过`);
                continue;
            }

            let modelGroupSQL: string = getSignModelGroupQuerySQL(monitorModelInfo);
            if (!modelGroupSQL) {
                logger.addLog(`--模型获取分组查询SQL失败, 越过`);
                continue;
            }
            logger.addLog(`--分类汇总SQL: ${modelGroupSQL}`);
            do {
                runingThreadNums = threadPool.getActiveCount();
                if (runingThreadNums >= corePoolSize) {
                    logger.addLog(`--当前线程池运行数量已经大于[${corePoolSize}], 需要等待线程池释放`);
                    sys.sleep(10000);
                }
                isStop = metadata.getString(isStopExecutePath);
                if (isStop == 'true') {
                    logger.addLog(`--监控被手动停止, 还剩[${sumMonitorNums - modelIndex + 1} 个模型未监控], 共: [${sumMonitorNums}] 个模型`);
                    break;
                }
                if (runingThreadNums < corePoolSize) {
                    logger.addLog(`--线程池存在空闲线程, 开始分配`);
                    break;
                }
            } while (runingThreadNums >= corePoolSize); // 若正在允许线程数量大于可分配线程数量, 则循环等待线程释放

            threadPool.submit({
                method: "dealSingalModelPushInfo",
                params: [modelIndex + 1, dbSource, monitorModelInfo, modelGroupSQL],
                path: "/sysdata/data/batch/t-脚本实现监控各个地市数据的推送，更新条数，日志记录.action"
            });
        }
    } catch (e) {
        logger.addLog(`--监控各个地市数据的推送, 更新条数, 日志记录---error`);
        logger.addLog(e);
    }

    do {
        runingThreadNums = threadPool.getActiveCount();
        if (runingThreadNums > 0) {
            logger.addLog(` 当前线程池正在运行数量: [${runingThreadNums}].....`);
            sys.sleep(30000);
        }
        isStop = metadata.getString(isStopExecutePath);
        if (isStop == 'true') {
            logger.addLog(`手动结束任务, 当前线程池正在运行数量: [${runingThreadNums}`);
            break;
        }
    } while (runingThreadNums > 0); // 等待所有线程都执行结束后, 再结束任务

    logger.addLog(`监控各个地市数据的推送, 更新条数, 日志记录完成, 共耗时: (${(Date.now() - startTime) / 1000}) s`);
}

/**
 * 处理单个模型推送数据情况
 * @param threndId 线程序号
 * @param dbSource 监测模型所在数据源对象
 * @param monitorModelInfo 监控模型信息
 */
export function dealSingalModelPushInfo(threndId: number, dbSource: Datasource, monitorModelInfo: MonitorModelInfo, modelGroupSQL: string): void {
    console.info(`线程[${threndId}] 开始执行`);
    let monitorModelResults: MonitorModelResult[] = getSignModelMonitorResult(dbSource, monitorModelInfo, modelGroupSQL);
    recordSignTablePushInfo(monitorModelResults);
    console.info(`线程[${threndId}] 执行结束`);
}

/**
 * 获取指定模型监测推送结果
 * @param dbSource 监测模型所在数据源对象
 * @param monitorModelInfo 监控模型信息
 * @param modelGroupSQL 模型分组查询SQL, 由于dw模块不支持多线程, 改为主线程传入
 * @return 
 * 
 * @description 一个模型有多条日志信息（根据资源ID和分类字段值区分）
 */
function getSignModelMonitorResult(dbSource: Datasource, monitorModelInfo: MonitorModelInfo, modelGroupSQL: string): MonitorModelResult[] {
    let groupField: string = monitorModelInfo.groupField;
    /** 分组汇总字段 */
    let countField: string = monitorModelInfo.countField;
    /**
     * 分组查询结果, eg: [{
            "XZQH": "310702",
            "XZQH_NUMS": 475
        }, {
            "XZQH": "121503",
            "XZQH_NUMS": 296
        }]
     */
    let pushResults = dbSource.executeQuery(modelGroupSQL);
    if (pushResults.length == 0) {
        return [];
    }
    let modelId: string = monitorModelInfo.modelId;
    let modelName: string = monitorModelInfo.modelName;
    let monitorModelResults: MonitorModelResult[] = [];
    for (let i = 0; i < pushResults.length; i++) {
        let data = pushResults[i];
        let monitorModelResult: MonitorModelResult = {
            MONITOR_ID: utils.uuid(),
            DATA_RES_ID: modelId,
            MODEL_NAME: modelName,
            GROUP_FIELD_VALUE: data[groupField],
            MONITOR_TIME: new Timestamp(new Date().getTime()),
            PUSH_DATA_NUMS: data[countField]
        };
        monitorModelResults.push(monitorModelResult);
    }
    console.info(`${modelName} 共有: ${monitorModelResults.length} 条监测结果`);
    return monitorModelResults;
}

/**
 * 记录指定推送表推送情况
 * @param table 检测结果表对象
 * @param monitorModelResult 监控模型结果
 * 
 * @description 日志表数据源为默认数据源, 并且数据每次都是新增
 */
function recordSignTablePushInfo(monitorModelResults: MonitorModelResult[]): void {
    let defaultDb = db.getDefaultDataSource();
    let monitorTable = defaultDb.openTableData("DATA_EXCHANGE_MONITOR_PUSH");
    monitorTable.setAutoCommit(false);
    try {
        let insertNums: number = monitorTable.insert(monitorModelResults);
        console.info(`成功写入监测日志表: ${insertNums} 条数据`);
        monitorTable.commit();
    } catch (e) {
        monitorTable.rollback();
        console.error(`记录指定推送表推送情况失败----error`);
        console.error(e);
    }
}

/**
 * 获取指定模型分组查询SQL
 * @param monitorModelInfo 监控模型信息
 * @return  eg：select t0.XZQH as XZQH, count(t0.XZQH) as XZQH_NUMS from QSSJ.T_CORP t0 group by t0.XZQH
 * 
 * @description dw模块不支持多线程
 */
function getSignModelGroupQuerySQL(monitorModelInfo: MonitorModelInfo): string {
    if (!monitorModelInfo) {
        return "";
    }
    let modelPath: string = monitorModelInfo.modelPath;
    let monitorType = monitorModelInfo.monitorType;
    let incrementField: string = monitorModelInfo.incrementField;
    let groupField: string = monitorModelInfo.groupField;
    let countField: string = monitorModelInfo.countField;

    let modelFilter: FilterInfo[] = [];
    if (monitorType == MonitorType.Increment) {
        modelFilter.push({
            exp: `model1.${incrementField} >= today()`
        });
    }
    let queryFields: QueryFieldInfo[] = [];
    queryFields.push({
        name: groupField,
        group: true,
        exp: `model1.${groupField}`
    });
    queryFields.push({
        name: countField,
        exp: `COUNT(model1.${groupField})`
    });
    let queryInfo: QueryInfo = {
        sources: [{
            id: "model1",
            path: modelPath
        }],
        fields: queryFields,
        sort: [],
        filter: modelFilter,
    }
    let sql = dw.getQuerySql(queryInfo);
    return sql;
}

/**
 * 获取监测日志表当天已经监测过的模型ID
 */
function queryTodayMonitoredModelIds(): string[] {
    let monitoredIds: string[] = [];
    let query: QueryInfo = {
        sources: [{
            id: "model1",
            path: "/sysdata/data/tables/sdi/data-exchange/ex-query/DATA_EXCHANGE_MONITOR_PUSH.tbl"
        }],
        fields: [{
            name: "DATA_RES_ID", exp: "model1.DATA_RES_ID"
        }],
        filter: [{
            exp: `model1.MONITOR_TIME >= today() `
        }],
        distinct: true
    }
    let queryData = dw.queryData(query).data;
    for (let i = 0; i < queryData.length; i++) {
        let data = queryData[i];
        monitoredIds.push(data[0] as string);
    }
    logger.addLog(`当天已经监测模型: ${monitoredIds.length} 个`);
    logger.addLog(monitoredIds);
    return monitoredIds;
}

/**
 * 监控模型信息管理
 */
export class MonitorModelInfoMgr {

    /** 监控模型存放父路径 */
    private monitorModelParentPath: string;
    /** 分类汇总字段 */
    private groupField: string;
    /** 增量监控字段 */
    private incrementField: string;
    /** 日志对象 */
    private logger: SDILog;

    /** 监控的模型信息 */
    private monitorModelInfos: MonitorModelInfo[];

    constructor(args: {
        monitorModelParentPath: string,
        groupField: string,
        incrementField: string,
        logger: SDILog
    }) {
        this.monitorModelParentPath = args.monitorModelParentPath;
        this.groupField = args.groupField?.toUpperCase();
        this.incrementField = args.incrementField?.toUpperCase();
        this.logger = args.logger;
        this.monitorModelInfos = [];
    }

    /**
     * 预检测
     */
    private preCheck(): ResultInfo {
        if (!this.monitorModelParentPath) {
            return { result: false, message: "监控模型存放父路径参数为空" };
        }
        if (!this.groupField) {
            return { result: false, message: "分类汇总字段为空" };
        }
        if (!this.incrementField) {
            return { result: false, message: "增量时间字段为空" };
        }
        return { result: true };
    }

    /**
     * 执行
     */
    public execute(): ResultInfo {
        let preCheckResult = this.preCheck();
        if (!preCheckResult.result) {
            return preCheckResult;
        }
        this.queryMonitorModelPaths();
        return { result: true };
    }

    /**
     * 查询监控的推送模型路径
     * 
     * @description
     * (1) 模型不含分类字段，则不记录
     * (2) 模型不包含增量时间字段，则全量监控
     */
    private queryMonitorModelPaths(): void {
        if (!this.monitorModelParentPath) {
            return;
        }
        let metaFileInfos: MetaFileInfo[] = metadata.searchFiles({
            parentDir: this.monitorModelParentPath,
            type: "tbl",
            recur: true
        });
        let modelSize: number = metaFileInfos.length;
        this.logger.addLog(`当前目录下模型共有: ${modelSize} 个`);
        for (let i = 0; i < modelSize; i++) {
            let metaFile = metaFileInfos[i];
            let modelPath: string = metaFile.path;
            let modelFields = this.getSignModelFields(modelPath);
            if (modelFields.length == 0) {
                continue;
            }
            if (!modelFields.includes(this.groupField)) { // 校验模型是否含分类字段
                continue;
            }
            let monitorType: string = MonitorType.All;
            if (modelFields.includes(this.incrementField)) { // 判断是否增量监控
                monitorType = MonitorType.Increment;
            }
            let modelId: string = metaFile.id;
            let modelName: string = metaFile.name;
            let modelDesc = metaFile.desc as string;
            let monitorModelInfo: MonitorModelInfo = {
                modelId: modelId,
                modelPath: modelPath,
                modelName: !!modelDesc ? modelDesc : modelName,
                monitorType: monitorType,
                groupField: this.groupField,
                countField: `${this.groupField}_NUMS`,
                incrementField: this.incrementField
            }
            this.monitorModelInfos.push(monitorModelInfo);
        }
        this.logger.addLog(`实际需监控: ${this.monitorModelInfos.length} 个模型推送情况`);
    }

    /**
     * 获取指定模型字段
     * @param modelPath 模型路径
     * @return 
     */
    private getSignModelFields(modelPathOrId: string): string[] {
        if (!modelPathOrId) {
            return [];
        }
        let modelFields: string[] = [];
        let modelStrInfo = metadata.getString(modelPathOrId);
        if (modelStrInfo != null) {
            let modelInfo = JSON.parse(modelStrInfo);
            let modelDimisions = modelInfo['dimensions'];
            for (let i = 0; i < modelDimisions?.length; i++) {
                let modelFieldInfo = modelDimisions[i];
                let dbField: string = modelFieldInfo["dbfield"];
                modelFields.push(dbField?.toUpperCase());
            }
            let measures = modelInfo['measures'];
            for (let i = 0; i < measures?.length; i++) {
                let measure = measures[i];
                let dbField: string = measure["dbfield"];
                modelFields.push(dbField?.toUpperCase());
            }
        }
        return modelFields;
    }

    /**
     * 获取推送的监控模型
     */
    public getMonitorModelPaths(): MonitorModelInfo[] {
        return this.monitorModelInfos;
    }
}

/** 监控数据类型 */
enum MonitorType {
    /** 全量 */
    All = "all",
    /** 增量 */
    Increment = "increment"
}

/** 监控模型信息 */
interface MonitorModelInfo {
    /** 模型ID */
    modelId: string;
    /** 模型路径 */
    modelPath: string;
    /** 模型名 */
    modelName: string;
    /** 监控方式（全量|增量） */
    monitorType: string;
    /** 增量统计时间字段 */
    incrementField: string;
    /** 分类字段 */
    groupField: string;
    /** 分组汇总字段, eg: count(分类字段) */
    countField: string;
}

/** 监控模型结果 */
interface MonitorModelResult {
    /** 序号 */
    MONITOR_ID: string;
    /** 数据表资源ID */
    DATA_RES_ID: string;
    /** 模型名称 */
    MODEL_NAME: string;
    /** 分类字段值 */
    GROUP_FIELD_VALUE: string;
    /** 监测时间 */
    MONITOR_TIME: Date;
    /** 今日更新数据条数 */
    PUSH_DATA_NUMS: number;
}