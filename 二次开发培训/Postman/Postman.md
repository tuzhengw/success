---
typora-root-url: images
---

# Postman

> 参考网址：https://blog.csdn.net/fxbin123/article/details/80428216

# 一 参考文档

[官方英文文档](https://www.getpostman.com/docs/v6/)

[postman中文使用教程](http://chromecj.com/web-development/2017-12/870.html)

---

# 二 下载安装

## 1 普通下载

1.1 [Postman for MAC](https://app.getpostman.com/app/download/osx64?utm_source=site&utm_medium=apps&utm_campaign=macapp&_ga=2.21151352.2119858274.1527039878-1088353859.1527039878)

1.2 [Postman for windows X64](https://app.getpostman.com/app/download/win64?_ga=2.201562513.1250696341.1530543681-1582181135.1530543681)

1.3 [Postman for windows X86](https://app.getpostman.com/app/download/win32?_ga=2.21151352.2119858274.1527039878-1088353859.1527039878)

1.4 [Postman for linux X64](https://app.getpostman.com/app/download/linux64?_ga=2.96050783.2119858274.1527039878-1088353859.1527039878)

1.5 [Postman for Linux X86](https://app.getpostman.com/app/download/linux32?_ga=2.96050783.2119858274.1527039878-1088353859.1527039878)

## 2 官网下载地址

https://www.getpostman.com/apps

---

## 3 安装失败

> postman 安装失败 Failed to install the .NET Framework, try installingthe latest version manully

![](../images/安装失败.png)

​	安装 postman 需要`  .Net Framework 框架`的环境才能安装

----

PostMan (官网下载实在太慢了)：https://apk-1257934361.cos.ap-guangzhou.myqcloud.com/Postman/Postman-win64-7.25.0-Setup.exe

.NET  框架文件 :  https://apk-1257934361.cos.ap-guangzhou.myqcloud.com/Postman/NDP452-KB2901907-x86-x64-AllOS-ENU.exe

---

### 3.1 .NET框架安装

 https://www.microsoft.com/en-us/download/confirmation.aspx?id=42642 

### 3.2 安装 PostMan

https://dl.pstmn.io/download/latest/win64 

# 三 基础功能

![](../images/postman基础页面.png)

# 四 接口请求流程

> 测试地址：http://www.baidu.com

## 1 GET

GET请求：点击Params，输入参数及value，可**输入多个**，即时显示在URL链接上。

> 所以，GET请求的请求头与请求参数如在接口文档中**无特别声明时，可以不填**

![](../images/get.png)

响应体示例： 响应的格式可以有多种，我这里由于请求的是 百度——响应的是 html ,
一般情况下，我们自定义接口的话是：json格式的响应体

![](../images/json.png)

## 2 POST-表单提交

**提交参数**

![](../images/Post.png)

**content-Type**

![](../images/contenttype.png)

## 3 POST-JSON提交

![](../images/postjson.png)

## 4 POST-XML提交



