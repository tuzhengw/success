/**
 * =====================================================================
 * 作者：tuzw
 * 审核人员：liuyz
 * 创建日期：20220727
 * 脚本用途：无锡数据中台支持新版标签打标后端代码
 * 地址：https://jira.succez.com/browse/CSTM-19806
 * ====================================================================
 */
import db from "svr-api/db";
import dw from "svr-api/dw";
import { getFile } from "svr-api/metadata";

/**
 * 获取当前模型对应的所有硬标签：标签编码和名称之间的映射关系
 * @param modelResid 模型id
 */
export function getLabelCodeAndNameRelation(request: HttpServletRequest, response: HttpServletResponse, params: {
    modelResidOrPath: string
}) {
    let modelResidOrPath: string = params.modelResidOrPath;
    print(`start-- 获取【${modelResidOrPath}】对应的所有硬标签：标签编码和名称之间的映射关系`);
    if (!modelResidOrPath) {
        print(`获取当前模型对应的所有硬标签：标签编码和名称之间的映射关系，没有指定模型ID`);
        return [];
    }
    let modelResid: string = getFile(modelResidOrPath).id;
    let querySQL: string = `
        select LABEL_CODE,LABEL_NAME, LABEL_CLASS_TYPE from SJZT.SZSYS_4_DW_DATA_LABELS WHERE LABEL_TYPE = '2' AND LABEL_LIB_CODE = (
            select LABEL_LIB_CODE from SJZT.SZSYS_4_DW_DATA_LABELS_LIB  WHERE  MAIN_TABLE_ID = ?
        )
    `;
    let defaultDb = db.getDefaultDataSource();
    let defaultSchema = defaultDb.getDefaultSchema();
    let queryResult = defaultDb.executeQuery(querySQL, [modelResid]);
    print('end----标签编码和名称之间的映射关系获取完成');
    return queryResult;
}

/**
 * 刷新指定模型缓存（后端）
 * @param refreshModelPath 刷新的模型路径
 */
export function refreshEndScriptCache(request: HttpServletRequest, response: HttpServletResponse, params: {
    refreshModelPaths: string[] | string
}) {
    print(`refreshEndScriptCache: 开始刷新指定模型缓存---start`);
    let refreshModelPaths: string[] = dealStrAvailable(params.refreshModelPaths);
    for (let i = 0; i < refreshModelPaths.length; i++) {
        let path: string = refreshModelPaths[i];
        if (dw.getDwTable(path) == null) {
            return;
        }
        print(`开始刷新模型：${path} 后端数据缓存`);
        dw.refreshState(path); // 清理后端缓存数据
    }
    print(`refreshEndScriptCache: 刷新指定模型缓存---end`);
}

/**
 * 处理可能为字符串或者字符串数组的变量，返回字符串数组
 * @param strAvailable 处理的变量
 * @return 
 */
function dealStrAvailable(strAvailable: string | string[]): string[] {
    if (!strAvailable) {
        return [];
    }
    if (typeof strAvailable != 'string') {
        return strAvailable;
    }
    return strAvailable.split(",");
}