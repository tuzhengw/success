/**
 * =====================================================
 * 作者：tuzw
 * 创建日期:2023-02-13
 * 功能描述: 服务器端脚本
 */
import dw from "svr-api/dw";
import fs from "svr-api/fs";
import sys from "svr-api/sys";
import utils from "svr-api/utils";

import { getFileStorageFilePath, ResultInfo, createDiskFile } from "/bgxt/app/wdmh.app/commons/commons.action";
import JavaFile = java.io.File;
import ZipOutputStream = java.util.zip.ZipOutputStream;
import ZipEntry = java.util.zip.ZipEntry;
import FileOutputStream = java.io.FileOutputStream;
import FileInputStream = java.io.FileInputStream;
import IOUtils = org.apache.commons.io.IOUtils;
import ByteArrayInputStream = java.io.ByteArrayInputStream;
import Base64 = java.util.Base64;

/**
 * 将付款清单表和清单内附件(发票)一起写入压缩包(存储临时目录下)
 * <p>
 *  2023-02-13 tuzw 
 *  地址: https://jira.succez.com/browse/CSTM-21964
 * </p>
 * @param param.paymentApplyId 付款申请单ID
 * @param param.applyBase64 付款申请单PDF-Base64值(此需要通过前端生成, 再传递到后端)
 * @param param.zipName 付款申请单和压缩包名一致, eg:申请日期+申请人+款项类型+付款申请, 例如：20230213姚梦婷项目成本付款申请
 * @return {
 *   result: true,
 *   zipPath: "压缩包存放磁盘路径"
 * }
 */
export function arrangePaymentApply(request: HttpServletRequest, response: HttpServletResponse, params: {
    paymentApplyId: string,
    applyBase64: string,
    zipName: string
}): ResultInfo {
    console.info(`开始执行[arrangePaymentApply], 将付款清单表和清单内附件(发票)一起写入压缩包(存储临时目录下)`);
    let paymentApplyId = params.paymentApplyId;
    let applyBase64 = params.applyBase64;
    if (!paymentApplyId) {
        return { result: false, message: "未指定付款清单表ID" };
    }
    if (!applyBase64) {
        return { result: false, message: "未获取付款清单表Base64值" };
    }
    let resultInfo: ResultInfo = { result: true };
    let workDir: string = sys.getWorkDir();
    let tempZipName: string = params.zipName;
    let zipFilePath: string = `${workDir}/temp/${utils.formatDate("yyyyMMdd")}/${tempZipName}.zip`;
    console.info(`当前临时记录导出文件压缩包路径: ${zipFilePath}`);
    let zipFile: JavaFile = createDiskFile(zipFilePath);

    let attachInfos: string[][] = queryPaymentApplyAttach(paymentApplyId);
    let fileInputStream: FileInputStream = null;
    let zipOutputStream: ZipOutputStream = null;
    let actualZipNums: number = 0;
    try {
        let paymentApplyBytes = Base64.getDecoder().decode(applyBase64); // base64转换为字节数组
        zipOutputStream = new ZipOutputStream(new FileOutputStream(zipFile));
        let payMentApplyEntry = new ZipEntry(`${tempZipName}.pdf`);
        zipOutputStream.putNextEntry(payMentApplyEntry);
        fileInputStream = new ByteArrayInputStream(paymentApplyBytes);
        IOUtils.copy(fileInputStream, zipOutputStream);
        fileInputStream.close();
        actualZipNums++;

        for (let i = 0; i < attachInfos.length; i++) {
            let attachInfo: string[] = attachInfos[i];
            let attachName: string = attachInfo[0];
            let attachId: string = attachInfo[1];
            console.info(`${i + 1}) 开始压缩附件: [${attachId}], ${attachName}`);
            let attachDiskPath: string = getFileStorageFilePath(attachId, attachName);;
            if (attachDiskPath == "") {
                continue;
            }
            let attachFile = fs.getFile(attachDiskPath);
            if (!attachFile.exists()) {
                continue;
            }
            let attachEntry = new ZipEntry(attachName);
            zipOutputStream.putNextEntry(attachEntry);

            fileInputStream = attachFile.openInputStream();
            IOUtils.copy(fileInputStream, zipOutputStream);
            fileInputStream.close();
            actualZipNums++;
        }
        resultInfo.zipPath = zipFilePath; // 记录压缩包路径
    } catch (e) {
        resultInfo.result = false;
        resultInfo.message = "付款清单表和清单内附件压缩失败";
        console.error(`付款清单表和清单内附件压缩执行失败, 删除临时创建的压缩包: [${zipFile.delete()}]----error`);
        console.error(e);
    } finally {
        fileInputStream && fileInputStream.close();
        zipOutputStream && zipOutputStream.close();
        if (actualZipNums == 0) {
            console.info(`没有文件可导出, 删除临时压缩包: ${zipFile.delete()}`);
            resultInfo.result = false;
            resultInfo.message = `没有文件可导出`;
        }
    }
    console.info(resultInfo);
    console.info(`执行结束[arrangePaymentApply], 将付款清单表和清单内附件(发票)一起写入压缩包(存储临时目录下)`);
    return resultInfo;
}

/**
 * 查询指定付款申请单内的附件信息
 * @param applyId 付款申请单ID
 * @return 
 */
function queryPaymentApplyAttach(applyId: string): string[][] {
    let query: QueryInfo = {
        sources: [{
            id: "model1",
            path: "/bgxt/data/tables/FACT/FKSQ/FACT_FKSQFJ.tbl"
        }],
        fields: [{
            name: "WJM", exp: "model1.WJM"
        }, {
            name: "WJNR", exp: "model1.WJNR"
        }],
        filter: [{
            exp: `model1.FKSQID = '${applyId}'`
        }],
    }
    let queryData = dw.queryData(query).data as string[][];
    return queryData;
}