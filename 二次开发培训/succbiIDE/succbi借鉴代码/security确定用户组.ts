/**
 * 此脚本定义系统登录、权限初始化相关的一些事件脚本。
 * 
 * 1. 此脚本全局唯一，在`sysdata`项目中，位于`/sysdata/settings/hooks/security.action`。
 * 2. 推荐直接编辑`security.action.ts`，可以有语法提示，系统会自动编译生成`security.action`。
 * 3. 当【系统设置】-【单点登录】-【脚本定制】启用后这里还可以实现sso扩展接口中所有定义的接口，
 * 	具体见： https://docs.succbi.com/dev/extension-points/sso/


/**
 * 用于在登录过程中动态决定一个用户属于哪些用户组。
 * 
 * 1. 此函数执行的时机是，用户完成登录校验后，并初始化用户的权限时
 * 2. 如果此函数存在，那么系统将完全信任此函数返回的信息，默认不读取用户组用户关系表中的用户组信息
 * 
 * @param userInfo 用来登录的用户信息
 * @returns 
 * 	1. 用户组ID数组（是一个字符串数组），代表当前用户将被分配到这些用户组里面去
 *  2. 字符串，表示用户属于哪个用户组
  * 3. undefined，或者抛出异常：表示使用系统默认的用户组逻辑
 *  3. 其它值将被忽略，系统完全信任此函数返回的信息，默认不读取用户组用户关系表中的用户组信息
 */
function resolveUserGroups(userInfo: UserInfo): Array<string> | string {
    return addGroups_(userInfo);
}

const IS_DEBUG = true;

/**
 * 自定义添加用户组
 */
function addGroups_(userInfo: UserInfo): Array<string> | string {
    let yhjs = userInfo.yhjs;
    if (!yhjs) {//如果用户角色存在空值，则直接返回，走系统默认逻辑，避免后台设置的用户组掉了，比如succez开发用户
        return;
    }
    let yhzidItems: string[] = split(yhjs);
    let result: string[] = [].concat(yhzidItems);
    IS_DEBUG && print(`用户：${userInfo.userId}分配的用户组为：${result.join(",")}`);
    return result;
}

function split(s: string): string[] {
    if (s == null || s == '') {
        return [];
    }
    return s.split(',');
}
