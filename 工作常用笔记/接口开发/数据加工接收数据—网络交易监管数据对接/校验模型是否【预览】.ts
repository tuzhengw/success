/**
 * ============================================================
 * 作者:tuzhengw
 * 审核人员：
 * 创建日期：2021-12-27
 * 功能描述：
 *      该脚本主要是用于【网络交易监管数据对接接口开发】
 * ============================================================
 */
import { dealRequest } from "/sdi/script/requestExternalInterface.action";

/** 模型请求URL地址 */
const requestPath = "/gdopenapi/saba/statics/page";
/** 模型对应物理表名 */
const dbTableName = "ODS_WLJYJG_ZDXX";

/**
 * 返回脚本节点字段结构
 */
function onProcessFields(context: IDataFlowScriptNodeContext): DbFieldInfo[] {
    return outputFields;
}

/**
 * 返回脚本节点的数据结构
 */
function onProcessData(context: IDataFlowScriptNodeContext): DbTableInfo | any[][] {
    let rows = [];
    let isPreview = context.isPreview; // 是否为预览数据
    if (isPreview) {
        print(`--【字典表】脚本，当前状态【预览】，不执行脚本`);
        return rows;
    }
    let requestResult = dealRequest(requestPath, dbTableName, outputFields);
    if (requestResult.result) {
        rows = requestResult.rows;
    }
    return rows;
}

/** 字典表：ODS_WLJYJG_ZDXX_SCRIPT */
const outputFields = [
    { name: "字典编号", dbfield: "pk", dataType: FieldDataType.C, length: 50 },
    { name: "更新时间", dbfield: "gmtModified", dataType: FieldDataType.P },
    { name: "类型", dbfield: "type", dataType: FieldDataType.C, length: 50 },
    { name: "字典值", dbfield: "value", dataType: FieldDataType.C, length: 50 },
    { name: "字典描述", dbfield: "description", dataType: FieldDataType.C, length: 50 },
    { name: "排序", dbfield: "sort", dataType: FieldDataType.C, length: 50 },
    { name: "创建时间", dbfield: "createTime", dataType: FieldDataType.P }
]