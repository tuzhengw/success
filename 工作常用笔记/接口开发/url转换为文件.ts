/**
 * 20230524 tuzw
 * 地址: https://jira.succez.com/browse/CSTM-23004
 * 功能: 将表中对应的URL地址对应的文件存储到指定目录下。
 */
import utils from "svr-api/utils";
import metadata from 'svr-api/metadata';
import sys from "svr-api/sys";
import fs from "svr-api/fs";
import dw from "svr-api/dw";
import db from "svr-api/db"
const HttpClients = Java.type("org.apache.http.impl.client.HttpClients");
const EntityUtils = Java.type("org.apache.http.util.EntityUtils");
const HttpGet = Java.type("org.apache.http.client.methods.HttpGet");
const RequestConfig = Java.type("org.apache.http.client.config.RequestConfig");
import { ResultInfo, SysFileInfoProperty, DealStatus } from "/sdi/script/commons/commons.action";
const PoolingHttpClientConnectionManager = org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
const Timestamp = Java.type('java.sql.Timestamp');
/**
 * 脚本入口
 * 作用: 将指定表中的URL地址转换为文件, 并通过ftp推送到97.180集群节点(对方服务器)
 */
function main() {
    /** 存储文件父文件夹名, 按日期(yyyyMMdd命名), eg: 20230526, 默认: 当天 */
    let folderName: string = "";
    // let dealDatas = queryUrlDatas();
    // if (dealDatas.length == 0) {
    //     return;
    // }
    let dealDatas = [{
        fileName: "鄂食健广审(文)第230318-00639号-保健食品-7.jpg",
        fileUrlAddress: "https://scjg.hubei.gov.cn/scjgyzt-public/awsFile//yFile/downloadFile?fiId=6527998200b34f53a8b51dee2c13c1fa"
    }, {
        fileName: "鄂食健广审(文)第230318-00639号-保健食品-7.xml",
        fileUrlAddress: "https://scjg.hubei.gov.cn/scjgyzt-public/awsFile//yFile/downloadFile?fiId=025bbebc780f475ca5c6debf90489ba8"
    }]
    let urlFileMgr = new UrlFileMgr({
        dealDate: folderName,
        fileInfos: dealDatas
    });
    urlFileMgr.execute();
}

/**
 * url地址对应的文件管理
 * <p>
 *  将url地址对应的文件转换为本地文件。
 * </p>
 */
class UrlFileMgr {

    /** 文件父目录(绝对路径), eg: ${sys.workDir()}/jr/AdvertisingRegulationFile/${utils.formatDate("yyyyMMdd")} */
    private fileSaveDir: string;
    /** 更新日期, 用于将其作为文件名, 默认当前时间, eg: 20230526 */
    private dealDate: string;

    /** 处理的文件信息, eg: ["https://scjg.hubei.gov.cn/scjgyzt-public/awsFile//yFile/downloadFile?fiId=bef4162bd38c423b8dd5f5507ae696e6"] */
    private fileInfos: SysFileInfoProperty[];
    /** 外部文件控制脚本执行, 停止: true, 执行: false */
    private ControlScriptFilePath: string = "/sdi/script/AdvertisingRegulation/isStopFileCreate.txt";

    /** 设置连接超时时间为5秒 */
    private connectionTimeout = 5000;
    /** 设置读取超时时间为10秒 */
    private socketTimeout = 10000;
    /** 集群节点 */
    private clusterNodeName: string;

    /** http请求客户端(使用完后内部会自动关闭) */
    private httpclient;

    constructor(args: {
        fileInfos: SysFileInfoProperty[],
        /** 处理日期(yyyyMMdd), 例如: 20230525 */
        dealDate?: string
    }) {
        let dealDate = args.dealDate;
        if (!dealDate || dealDate == "") {
            dealDate = utils.formatDate("yyyyMMdd");
        }
        this.dealDate = dealDate;
        this.fileInfos = args.fileInfos;
        this.fileSaveDir = `${sys.getWorkDir()}/jr/AdvertisingRegulationFile/${dealDate}`;
        this.clusterNodeName = sys.getClusterNodeName();
    }

    /**
     * 开始执行转换
     */
    public execute(): void {
        let connectionManager = new PoolingHttpClientConnectionManager(); // 脚本大量请求改为连接池管理
        this.httpclient = HttpClients.custom().setConnectionManager(connectionManager).build();
        let requestConfig = RequestConfig.custom()   // 创建RequestConfig对象，并设置连接超时和读取超时
            .setConnectTimeout(this.connectionTimeout)
            .setSocketTimeout(this.socketTimeout)
            .build();
        try {
            this.httpclient = HttpClients.custom()
                .setConnectionManager(connectionManager)
                .setDefaultRequestConfig(requestConfig)
                .build();
            this.covertUrltoFiles();
        } catch (e) {
            print(`URL文件转换失败[${e.toString()}]`);
            print(e);
        } finally {
            this.httpclient && this.httpclient.close();
            connectionManager && connectionManager.close();
        }
        return;
    }

    /**
     * 将URL地址转换为文件
     */
    private covertUrltoFiles(): void {
        let fileSaveDir: string = this.fileSaveDir;
        print(`开始执行将url地址转换磁盘文件, 文件存储父目录: [${fileSaveDir}]`);
        let fileInfos: SysFileInfoProperty[] = this.fileInfos;
        // let ds = db.getDefaultDataSource();
        // let logTable = ds.openTableData("AD_FILE_INFO_LOG");
        for (let fileIndex = 0; fileIndex < fileInfos.length; fileIndex++) {
            let isStop: string = metadata.getString(this.ControlScriptFilePath);
            if (isStop == "true") {
                print(`当前执行到第[${fileIndex + 1}]个文件, 还有[${fileInfos.length - fileIndex - 1}]个文件待处理, 程序被外部终止, 执行结束`);
                break;
            }
            let fileInfo: SysFileInfoProperty = fileInfos[fileIndex];
            // let logId: string = utils.uuid();
            // let optionTableId: string = fileInfo.id;
            let fileName = fileInfo.fileName as string;
            let urlAddress: string = fileInfo.fileUrlAddress;
            if (fileIndex % 5 == 0) {
                print(`(${fileIndex + 1}) 开始处理url地址[${urlAddress}]`);
            }
            let dealResult: ResultInfo = { result: true };
            let filePath = `${fileSaveDir}/${fileName}`;
            let file = fs.getFile(filePath);
            if (file.exists()) {
                print(`文件[${filePath}]已存在, 忽略创建`); // 文件直属父目录名是当天时间, 当天内重复文件不覆盖。
                continue;
            }
            try {
                dealResult = this.getFileIo(urlAddress);
                if (dealResult.result) {
                    let { fileIo } = dealResult;
                    /**
                     * 文件名暂时用表记录的FILE_NAME字段值
                     * let fileType: string = (fileName as string).substring((fileName as string).lastIndexOf(".") + 1);
                       let diskFileName: string = encodeURIComponent(`${urlAddress}.${fileType}`);
                     */
                    dealResult = this.createFile(filePath, fileIo as number[]);
                }
            } catch (e) {
                print(`处理地址[${urlAddress}]文件失败[${e.toString()}]`);
                dealResult.result = false;
                dealResult.message = `${e.toString()}`;
            }
            // logTable.insert({
            //     "LOG_ID": logId,
            //     "OPTION_ID": optionTableId,
            //     "ADVERTIDING_INFO_ID": fileInfo.advertidingInfoId,
            //     "UPDATE_DATE": this.dealDate,
            //     "FILE_NAME": fileName,
            //     "FILE_PARENT_PATH": fileSaveDir,
            //     "URL_PATH": urlAddress,
            //     "STATUS": dealResult.result ? DealStatus.Success : DealStatus.Fail,
            //     "PUSH_MESSAGE": dealResult.message,
            //     "CLUSTER_NODE_NAME": this.clusterNodeName,
            //     "CREATE_TIME": new Timestamp(Date.now())
            // });
        }
        print(`执行结束将url地址转换磁盘文件, 文件存储父目录: [${fileSaveDir}]`);
    }

    /**
     * 存储父目录(绝对路径)
     */
    public getFileDir(): string {
        return this.fileSaveDir;
    }

    /**
     * 在磁盘创建一个文件
     * <p>
     *  文件命名格式: ${url地址}.${文件类型}
     *  直接将按指定要求命名文件夹, 最终路径在执行创建文件时提示: 文件名不合法, 原因是URL路径"//"是转义字符, 导致无法识别。
     *  异常: java.io.IOException: 文件名、目录名或卷标语法不正确。
     *  解决办法: encodeURIComponent(`${urlAddress}.${fileType}`);
     * 20230525 tuzw
     *  文件名直接采用表字段: FILE_NAME。
     * </p>
     * @param fileName 文件名(含后缀类型), eg: xxx.txt
     * @param fileIoByte 文件字节数组
     * @return {
     *   result: true
     * }
     */
    private createFile(filePath: string, fileIoByte: number[]): ResultInfo {
        let file = new java.io.File(filePath);
        if (file.exists()) {
            // print(`--文件已存在, 删除旧文件[${fileName}], 删除状态: ${file.delete()}--`);
            return { result: true, message: "文件已存在 忽略" };
        }
        let resultInfo: ResultInfo = { result: true };
        print(`开始创建文件, 路径为: ${filePath}`);
        let parentFile = file.getParentFile();
        if (!parentFile.exists()) {
            parentFile.mkdirs();
        }
        file.createNewFile();
        file = fs.getFile(filePath);
        // @ts-ignore
        let outStream: OutputStream = null;
        try {
            outStream = file.openOutputStream();
            outStream.write(fileIoByte);
        } catch (e) {
            resultInfo.result = false;
            resultInfo.message = `${e.toString()}`;
            print(`--写入文件${filePath}内容失败[${e.toString()}]---`);
        } finally {
            outStream && outStream.close();
        }
        return resultInfo;
    }

    /**
     * 读取url文件内容和文件名(含类型)
     * @param url 文件下载链接
     * @return eg: { 
     *     fileIo: [文件二进制流数组],
     *     fileName: "test.png"
     * }
     */
    private getFileIo(url: string): ResultInfo {
        let resultInfo: ResultInfo = { result: true };
        let response;
        try {
            let httpGet = new HttpGet(url);
            response = this.httpclient.execute(httpGet);
            /**
             * 获取文件名，不可使用getValuegetValue来获取，改为直接使用toString()方法将其对象转换为字符串
             * let fileName = response.getFirstHeader("Content-Disposition").getValuegetValue().split(';')[1].split('=')[1];
             * class org.apache.http.message.BufferedHeader ——> Content-Disposition: inline; filename="926/0-220925-wx-207109-1.jpg"
             */
            let contentDisposition = response.getFirstHeader("Content-Disposition").toString();
            let fileName = contentDisposition.split(';')[1].split('=')[1];
            let entity = response.getEntity();
            let encodeFileName = decodeURIComponent(fileName); // 对方提供的url地址获取的文件名是URL编码的字符串, 需要解码(url中没有设置Content-type)
            resultInfo.fileIo = EntityUtils.toByteArray(entity);
            resultInfo.fileName = encodeFileName.replace(/\"/g, "");
        } catch (e) {
            resultInfo.result = false;
            resultInfo.message = e.toString();
            console.error(`请求url[${url}]文件字节失败[${e.toString()}]`);
            // console.error(e);
        } finally {
            response && response.close();
        }
        return resultInfo;
    }
}

/** 
 * 查询转换的URL地址
 * <p>
 *  增量按照创建时间来查询。
 * </p>
 */
function queryUrlDatas(): SysFileInfoProperty[] {
    print(`开始执行[queryUrlDatas], 查询转换的URL地址数据`);

    let now = new Date();
    let today = new Date(now.getFullYear(), now.getMonth(), now.getDate());
    let timestamp: number = today.getTime();

    let queryInfo: QueryInfo = {
        sources: [{
            id: "model1",
            path: "/sdi/data/tables/etl/tmp/fmk/SJSB/ggjg/GGJG_CLB_WJSB.tbl"
        }],
        fields: [{
            name: "ID", exp: "model1.ID"
        }, {
            name: "ADVERTIDING_INFO_ID", exp: "model1.ADVERTIDING_INFO_ID"
        }, {
            name: "FILE_NAME", exp: "model1.FILE_NAME"
        }, {
            name: "FILE_PATH", exp: "model1.FILE_PATH"
        }],
        // filter: [{ // todo GGJG_CLB_WJSB表可以增加日志表关联, 忽略已成功的数据, 每次处理失败和未处理的数据
        //     exp: `model1.CREATE_TIME >= ${timestamp} `
        // }]
    }
    let fileInfos: SysFileInfoProperty[] = [];
    let queryDatas = dw.queryData(queryInfo).data;
    for (let i = 0; i < queryDatas.length; i++) {
        let datas = queryDatas[i] as string[];
        fileInfos.push({
            id: datas[0],
            advertidingInfoId: datas[1],
            fileName: datas[2],
            fileUrlAddress: datas[3]
        });
    }
    print(`执行结束[queryUrlDatas], 查询转换的URL地址数据: [${queryDatas.length}] 条`);
    return fileInfos;
}