
import Files_ = java.nio.file.Files;
import Paths_ = java.nio.file.Paths;
import { getWorkDir } from "svr-api/sys";

/**
 * 获取服务器指定文件，返回给客户端，供用户：【下载】 | 【预览】
 * 注意：
 * （1）下载和预览，可通过设置：【请求头】 处理
 * （2）response 设置响应头 ：Content-Disposition 消息头指示回复的内容：该以何种形式展示，是以内联的形式（即网页或者页面的一部分），还是以附件的形式下载并保存到本地
 * （3）因为是直接对response对象操作，这里不需要返回，浏览器自己会发：请求
 * 调用：
 * （1）custom.ts 调用，downloadFile("/TMP/app/保存测试.app/test.action?method=downFileInfo");
 * （2）SPG组件：图片、文档（PDF）调用
 * 
 * @param onClick 触发的事件，【下载】 | 【预览】
 * @param filePath 文件路径，eg：/zhjg/workdir/clusters-share/2022-01-04/internetDeal/0-211231-wx-66364-1/filePath_19597833251401884snapshot.png
 */
export function showFile(request: HttpServletRequest, response: HttpServletResponse, params: any) {
    print(`---downFileInfo()，获取服务器指定文件，返回给客户端，供用户下载---start`);
    let filePath: string = params["filePath"];
    let fileName: string = filePath.substring(filePath.lastIndexOf("/") + 1);
    let fileType: string = filePath.substring(filePath.lastIndexOf(".") + 1);
    let workDir: string = `${getWorkDir()}/clusters-share/`;
    let sendFile = new java.io.File(`${workDir}${filePath}`);
    let fileBytes = Files_.readAllBytes(Paths_.get(sendFile.getAbsolutePath())); // 通过绝对路径获取文件字节流
    /**
     * Content-Disposition设置（下载）
     * （1）inline（浏览器打开）：如果浏览器支持该文件类型的【预览】，就会打开，而不是下载
     * （2）attachment（附件下载）：浏览器则直接进行【下载】，纵使他能够预览该类型的文件
     */
    response.setHeader("Content-Disposition", "attachment; filename=" + fileName); // 给定【文件下载】请求头
    response.setContentType("application/octet-stream");
    response.setContentLength(fileBytes.length);

    let out: OutputStream = null;
    try {
        out = response.getOutputStream();
        out.write(fileBytes);
        out.flush();
    } catch (e) {
        print(e);
    } finally {
        out && out.close();
    }
    print(`---downFielInfo()，获取服务器指定文件，返回给客户端，供用户下载---end`);
}