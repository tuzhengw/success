/**
 * 存放通用的服务器端脚本
 * @author liuyz
 */

import dw from "svr-api/dw";
import db from "svr-api/db";
/**
 * 更新数据模型数据，避免客户端发出的请求有权限问题
 */
export function updateDwTableData(request: HttpServletRequest, response: HttpServletResponse, params: any) {
    let tablePath = params.tablePath;
    let datapackage = params.datapackage;
    dw.updateDwTableData(tablePath, datapackage);
}

/**
 * 增加资源对应查看次数
 */
export function addViewCount(request: HttpServletRequest, response: HttpServletResponse, params: any) {
    let resid = params.resid;
    let type = params.type;
    if (resid == null) {
        return { result: false, message: '请传递资源id' };
    }
    let number = db.getNumberSequencer(resid).next();
    dw.updateDwTableData("/sysdata/data/tables/sdi/SDI_FIELS_VIEW.tbl", {
        mergeIntoRows: [{
            fieldNames: ["RESID", "VIEW_COUNT", "TYPE"],
            rows: [[resid, number, type]]
        }]
    });
}