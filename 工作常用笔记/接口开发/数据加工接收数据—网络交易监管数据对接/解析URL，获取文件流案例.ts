
/*
 帖子：
 https://jira.succez.com/browse/BI-42821?focusedCommentId=192313&page=com.atlassian.jira.plugin.system.issuetabpanels%3Acomment-tabpanel#comment-192313
*/

import { get } from "svr-api/http";
import { getCachedValue, putCachedValue } from "svr-api/memcache";
import { getDataSource } from "svr-api/db";
import HttpClients = org.apache.http.impl.client.HttpClients;
import EntityUtils = org.apache.http.util.EntityUtils;
import HttpGet = org.apache.http.client.methods.HttpGet;

const DATA_SOURCE_NAME = "Vertica";
const client_id = "c335498d82b84132ba860e1f3d2ff852";
const client_secret = "380cee08c8d34ede99859d38caa9779f";
const cacheKey = "HG_IMAGE_TOKEN_CACHE";

export function execute(request: HttpServletRequest, response: HttpServletResponse, params: { id?: string, time?: string; }) {
    const { id, time } = params;
    const outputStream = response.getOutputStream();
    if (!id || !time) {
        writeNoImage(outputStream);
        return;
    }
    const dataSource = getDataSource(DATA_SOURCE_NAME);
    const rows = dataSource.executePageQueryRows('select xxzyywmc, bclj from public.ds_dm_hgfxkhyzt_xxsklstxxx where czbh = ? and xpsj = ?', [1], id, time);
    if (rows.length === 0) {
        writeNoImage(outputStream);
        return;
    }
    const [[resourceName, imagePath]] = rows as [[string, string]];
    const token = getToken();
    const baseUrl = resourceName === "dm_fxkhyzt_xxskzdtxxx1"
        ? "http://10.0.178.50:9090/service/api/e2b949879db64be494d9edaa0b465f8f"
        : "http://10.0.178.50:9090/service/api/1a63ec58617142dd94e1f0729693a129";
    const url = `${baseUrl}${imagePath}?client_id=${client_id}&access_token=${token}`;
    const image = requestImage(url);
    if (!image || typeof image === "string") {
        writeNoImage(outputStream);
        return;
    }
    outputStream.write(image as number[]);
}

function writeNoImage(outputStream: OutputStream) {
    const noImage = "no image";
    outputStream.write(new java.lang.String(noImage).getBytes() as number[]);
}

/**
 *  解析URL，获取文件流
 */
function requestImage(url: string): ArrayLike<number> | undefined | string {
    const client = HttpClients.createDefault();
    try {
        const httpGet = new HttpGet(url);
        const response = client.execute(httpGet);
        try {
            const entity = response.getEntity();
            const contentType = response.getFirstHeader("Content-type").getValue();
            if (contentType.indexOf("image") === -1) {
                print(url);
                const content = EntityUtils.toString(entity);
                if (content.indexOf("认证失败") !== -1) {
                    return content;
                }
                return undefined;
            }
            return EntityUtils.toByteArray(entity);
        } finally {
            response?.close();
        }
    } finally {
        client?.close();
    }
}

function requestForToken() {
    const url = `http://10.0.178.50:9090/oauth/token?client_id=${client_id}&client_secret=${client_secret}`;
    const response = get(url);
    const { access_token, expires_in } = JSON.parse(response) as { access_token: string, expires_in: number; };
    return { token: access_token, expire: (expires_in / 60) - 1 };
}

function getToken(): string {
    const value = getCachedValue(cacheKey);
    if (value) return value;
    const { token, expire } = requestForToken();
    putCachedValue(cacheKey, token, expire);
    return token;
}

