/**
 * 连接时不需要额外指定连接条件，因为会自动按照相同属性进行等值连接，且会删除结果集中的重复属性
 *（不包含：重复项）
 */
SELECT * FROM 
	insane_emp 
natural join insane_dept;


SELECT * FROM 
	insane_emp INNER JOIN insane_dept 
using(deptno);