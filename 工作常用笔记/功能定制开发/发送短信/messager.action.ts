/**
 * =================================================
 * 作者：tuzw
 * 审批者: liuyz
 * 创建日期：2022-10-09
 * 脚本用途: 通过脚本使用三方服务API发送短信验证码
 * ================================================
 */
import util from "svr-api/utils";
import DefaultHttpClient = org.apache.http.impl.client.DefaultHttpClient;
import HttpPost = org.apache.http.client.methods.HttpPost;
import EntityUtils = org.apache.http.util.EntityUtils;
import UrlEncodedFormEntity = org.apache.http.client.entity.UrlEncodedFormEntity;
import BasicNameValuePair = org.apache.http.message.BasicNameValuePair;
import StringEntity = org.apache.http.entity.StringEntity;

/**
 * 发送短信
 * 
 * @param phones 手机号, eg: { 1351716xxxx }
 * @param messageInfo 消息主体，可能是一个json，也可能是一个 string，调用 sys 模块的发送短信接口 `sendSMS` 时传递的参数是什么，这里就是什么
 * @returns 返回短信服务商服务器发送短信后的返回数据，数据将会被记录到日志表中，为方便调试以及定位错误，请务必要设置返回值
 * 			对返回格式做一下约定，以便调用者处理返回结果信息：
 * 			{
 * 				code: string, 
 * 				message?: string,
 * 				detial?: any
 * 			}
 * 			code返回"ok"时，表示发送成功；其他情况则发送可能有问题，message里存放问题的简短信息，detail存放详细信息
 * 
 * @description 统一下验证码消息-以后有需求在做区分：您的验证码：xxxx，请您在3分钟内填写
 */
function sendSMS(phones: string[], messageInfo: ShortMessage | string): any {
    let title: string = "您的验证码: ";
    let sendContent: string = "";
    console.debug(messageInfo);
    if (!(messageInfo instanceof String)) {
        sendContent = title + (messageInfo as ShortMessage).code + "，请您在3分钟内填写";
    } else {
        sendContent = messageInfo as string;
    }
    return sendShortMessages(phones[0], sendContent);
}

/**
 * 发送短信，由于运营商限制，每天只能向同一个手机号发送5条短信
 * @param phones 手机号码集
 * @param sendContent 发送内容
 * 
 * @description 调用第三方短信接口, 发送短信
 */
function sendShortMessages(mobiles: string, sendContent: string): string | ResultInfo {
    if (!mobiles) {
        return { statusCode: ReturnStatusCode.NoSignPhone, respStr: "未获取到需要发送的手机号码" };
    }
    let resultInfo: ResultInfo = { statusCode: ReturnStatusCode.SuccessCode };
    let ecName: string = "南通市市场监督管理局";
    let apId: string = "ntscjg";
    let secretKey: string = "spaqNT2021a!";
    let sign: string = "y4w9qLmBF";
    let addSerial: string = "";
    let macStr: string = `${ecName}${apId}${secretKey}${mobiles}${sendContent}${sign}${addSerial}`;
    let mac: string = util.md5(macStr);
    let sendParams: ShortMessageParam = {
        ecName: ecName,
        apId: apId,
        secretKey: secretKey,
        mobiles: mobiles,
        content: sendContent,
        sign: sign,
        addSerial: "",
        mac: mac
    };
    console.debug(sendParams);
    let base64Params: string = util.encodeBase64(JSON.stringify(sendParams));
    let shortMessageSendURL: string = `http://112.35.1.155:1992/sms/norsubmit`;
    let httpclient = new DefaultHttpClient();
    try {
        let httpost = new HttpPost(shortMessageSendURL);
        httpost.setHeader("Content-Type", "application/json;charset=UTF-8");
        let se: StringEntity = new StringEntity(base64Params); // 携带Json格式的参数(base64加密后的值)
        se.setContentType("text/json");
        httpost.setEntity(se);

        let response = httpclient.execute(httpost);
        let statusCode = response.getStatusLine().getStatusCode();
        let entity = response.getEntity();

        if (statusCode == 200) {
            let requestResult: InterfaceReturnMessage = EntityUtils.toString(entity, 'utf-8');
            let sendStatus: boolean = requestResult.success;
            let rspcod: string = requestResult.rspcod;
            let msgGroup: string = requestResult.msgGroup;

            if (sendStatus) {
                console.debug(`发送成功`);
                resultInfo.respStr = "发送成功";
            } else {
                console.debug(requestResult);
                resultInfo.statusCode = ReturnStatusCode.SendError;
                resultInfo.respStr = `短信发送失败: ${rspcod} ${msgGroup}`;
            }
        } else {
            console.debug(`请求发送接口失败`);
            console.debug(response);
            resultInfo.statusCode = statusCode;
            resultInfo.respStr = "请求短信接口失败";
        }
    } catch (e) {
        resultInfo.respStr = "发送服务器异常";
        resultInfo.statusCode = ReturnStatusCode.ServerError;
        console.error(`给手机号: [${mobiles}] 发送短信失败----error`);
        console.error(e);
    }
    return resultInfo;
}


/** 短信接口请求参数 */
interface ShortMessageParam {
    /** 集团客户名称 */
    ecName: string;
    /** 用户名 */
    apId: string;
    /** 密码 */
    secretKey: string;
    /** 发送手机号码, eg: "18137282928,18137282922,18137282923" */
    mobiles: string;
    /** 发送短信内容 */
    content: string;
    /** 网关签名编码，必填，签名编码在中国移动集团开通帐号后分配，可以在云MAS网页端管理子系统-SMS接口管理功能中下载 */
    sign: string;
    /** 扩展码，根据向移动公司申请的通道填写，如果申请的精确匹配通道，则填写空字符串("")，否则添加移动公司允许的扩展码 */
    addSerial: string;
    /** API输入参数签名结果，签名算法：将ecName，apId，secretKey，mobiles，content ，sign，addSerial按照顺序拼接，然后通过md5(32位小写)计算后得出的值 */
    mac: string;
}

/** 返回状态码 */
enum ReturnStatusCode {
    /** 发送成功 */
    SuccessCode = '100',
    /** 未指定发送手机号码 */
    NoSignPhone = '101',
    /** 发送服务器异常 */
    ServerError = '500',
    /** 发送失败 */
    SendError = '102'
}

/** 短信信息 */
interface ShortMessage {
    /** 短信验证码, 返回"ok"时，表示发送成功；其他情况则发送可能有问题 */
    code: ReturnStatusCode;
    /** 问题的简短信息 */
    message?: string;
    /** 详细信息 */
    detial?: string;
}

/** 返回消息 */
interface ResultInfo {
    /** 状态码 */
    statusCode: string;
    /** 消息 */
    respStr?: string;
}

/** 接口返回消息 */
interface InterfaceReturnMessage {
    /** 响应码 */
    rspcod: string;
    /** 消息批次号，由云MAS平台生成，用于验证短信提交报告和状态报告的一致性（取值msgGroup）注:如果数据验证不通过msgGroup为空 */
    msgGroup: string;
    /** 是否发送成功 */
    success: boolean;
}