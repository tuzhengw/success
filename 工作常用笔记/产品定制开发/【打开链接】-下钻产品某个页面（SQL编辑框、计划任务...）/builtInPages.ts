/**
 * 20211130 liuyz
 * 将产品一些内置页面嵌入到前台门户中
 *
 * 20220725 tuzw
 * 实现授信应用、系统备份等系统管理页面
 * 帖子：https://jira.succez.com/browse/CSTM-19784
 */
import { getDataSourceManager, LeftTreeView, MMP_Data } from "dw/dwmgr";
import { Component, getUrlParameters, UrlInfo, assign, isEmpty, DefaultItemDataInfo, SZEvent, showWaiting, showWarningMessage, BASIC_EVENT, message, showAlertDialog, showSuccessMessage, showErrorMessage } from "sys/sys";
import { MetaModulePageArgs, MetaFilesPageArgs, showDetailImpactInfo } from "metadata/metamgr";
import { ToolbarArgs, TabbarArgs } from "commons/basic";
import { SettingPage_securityConf } from 'metadata/syssettings/syssettings-securityConf';
import { List, ListItem } from "commons/tree";
import { showMenu } from "commons/menu";
import { getMetaEventListeners, getMetaRepository } from "metadata/metadata";
import { getScheduleMgr, ScheduleConfPage, ScheduleLogsPage, ScheduleMgrPage, ScheduleMgrSubPage, ScheduleNewLogPage, SUB_PAGE_TYPE, TaskListPage } from "schedule/schedulemgr";
import { SettingPage_backup, SettingPage_restore } from "metadata/syssettings/syssettings-sysbackup";
import { SettingPage_trustedapps } from "metadata/syssettings/syssettings-trustedapps";
import { SDI_TASK_FLOW } from '../data-process/data-schedule.js?v=202205210272';
import { MMP_Datasources } from "datasources/datasourcesmgr";

enum PagesType {
	/** 数据查看页面 */
	DATA = 'data',
	/** 系统安全设置页面 */
	SECURITY = 'security',
	/** 计划任务日志页面 */
	SCHEDULE_LOG = "schedulePage",
	/** 系统-备份 */
	SYS_BACKUP = 'sysBackup',
	/** 系统-恢复 */
	SYS_RESTORE = 'sysRestore',
	/** 系统授权页面 */
	SYS_TRUSTEDAPP = 'sysTrustedapps',
	/** 系统数据源页面 */
	SYS_DATABASE = 'sysDataBase'
}

/**
 * 产品内置页面类
 */
export class BuiltInPages extends Component {
	private url: string;
	private urlParams: JSONObject;
	/** 定位的URL参数 */
	private localUrlParams: JSONObject;
	private innerPage: Component;

	protected _init_default(args) {
		super._init_default(args);
		this.url = args.url;
		this.praseSearch();
	}

	/**
	 * 解析URL参数
	 */
	private praseSearch() {
		this.urlParams = getUrlParameters(this.url);
		this.localUrlParams = getUrlParameters("/?" + this.urlParams.urlParams);
	}

	protected _init(args) {
		let domBase = super._init(args);
		let pageType: PagesType = this.urlParams.type;
		switch (pageType) {
			case PagesType.DATA:
				this.createDataPage(domBase);
				break;
			case PagesType.SECURITY:
				this.createSecurityPage(domBase);
				break;
			case PagesType.SCHEDULE_LOG:
				this.createSchedulePage(domBase);
				break;
			case PagesType.SYS_BACKUP:
				this.createSysBackUpPage(domBase);
				break;
			case PagesType.SYS_RESTORE:
				this.createSysRestorePage(domBase);
				break;
			case PagesType.SYS_TRUSTEDAPP:
				this.createSysTrustedAppsPage(domBase);
				break;
			case PagesType.SYS_DATABASE:
				this.createSysDataSoucesPage(domBase);
				break;
		}
		domBase.style.height = "100%"
		return domBase;
	}

	/**
	 * 创建数据页面
	 */
	private createDataPage(domBase: HTMLElement) {
		let innerPage = this.innerPage = new SDI_MMP_Data({
			domParent: domBase,
			name: "data",
			projectName: this.urlParams.projectName,
			executeSQL: this.urlParams.executeSQL,
			onlyViewDataSource: this.urlParams.onlyViewDataSource,
			localUrlParams: this.localUrlParams,
			viewDataSource: this.urlParams.viewDataSource,
			hideToolbar: this.urlParams.hideToolbar
		});
	}

	/**
	 * 创建系统安全设置页面
	 */
	private createSecurityPage(domBase: HTMLElement) {
		let innerPage = this.innerPage = new SoleSystemSecurtiyAdaptPage({
			domParent: domBase
		});
	}

	/**
	 * 创建系统备份页面
	 */
	private createSysBackUpPage(domBase: HTMLElement): void {
		let innerPage = this.innerPage = new SDI_SettingPageBackUp({
			domParent: domBase
		});
	}

	/**
	 * 创建系统恢复页面
	 */
	private createSysRestorePage(domBase: HTMLElement): void {
		let innerPage = this.innerPage = new SDI_SettingPageRestore({
			domParent: domBase
		});
	}

	/**
	 * 创建系统授权页面
	 */
	private createSysTrustedAppsPage(domBase: HTMLElement): void {
		let innerPage = this.innerPage = new SDI_SettingPageTrustedapps({
			domParent: domBase
		});
	}

	/**
	 * 20220610 tuzw
	 * 打开计划页面某个子页面，URL参数：
	 * @param :infopage 页面类型
	 * @param :schedule 打开的计划任务ID
	 * @param :type 页面类型
	 */
	private createSchedulePage(domBase: HTMLElement) {
		let localUrlParams: ScheduleUrlParam = {
			":infopage": this.urlParams[':infopage'],
			":schedule": this.urlParams[':schedule'],
			":type": this.urlParams[':type']
		}
		if (["newlog", "tasks", "logs"].includes(localUrlParams[":type"])) { // 以列表形式展示
			localUrlParams[":viewmode"] = "list";
		}
		let innerPage = this.innerPage = new SDI_ScheduleMgrPage({
			domParent: domBase,
			name: "schedulePage",
			localUrlParams: localUrlParams,
			projectName:"sys"
		});
	}

	/**
	 * 创建系统数据源页面
	 */
	private createSysDataSoucesPage(domBase: HTMLElement): void {
		let innerPage = this.innerPage = new SDI_SysDataSoucesPage({
			domParent: domBase
		});
	}
}

export interface SDI_MetaModulePageArgs extends MetaModulePageArgs {
	readonly?: boolean;
	localUrlParams?: JSONObject;
	//要执行的sql
	executeSQL?: string;
	//要查看数据源，当设置之后，数据源列表只显示传入的数据源
	viewDataSource?: string;
	//是否只查看数据源，如果为true，那么默认隐藏模型面板
	onlyViewDataSource?: string;
	//是否隐藏工具栏，如果为true，那么默认隐藏工具栏
	hideToolbar?: string
}

/**
 * 数据页面
 */
export class SDI_MMP_Data extends MMP_Data {
	private executeSQL: string;
	private viewDataSource: string | string[];
	private onlyViewDataSource: string;
	private localUrlParams: JSONObject;
	private dataSourcesPanel: any;
	private hideToolbar: string;
	constructor(args: SDI_MetaModulePageArgs) {
		super(args);
		getMetaEventListeners().on(BASIC_EVENT.filechange, this.modulechange_handler);
		this.executeSQL = args.executeSQL;
		this.viewDataSource = args.viewDataSource;
		this.onlyViewDataSource = args.onlyViewDataSource;
		this.localUrlParams = args.localUrlParams;
		this.hideToolbar = args.hideToolbar;
		this.showUrl(assign({
			path: ""
		}, {
			params: this.localUrlParams
		})).then((self) => { // 等待渲染后，校验是否有指定SQL内容
			let leftTreeView: LeftTreeView = self.leftTreeView;
			if (!!this.executeSQL && this.executeSQL != '') {
				let sqlModelSqlEditor = self.filesBook.filesBook.getActiveComponent().getPanels().getCurrentPanel(); // filesBook 为当前页面编辑区域，内部含有多个小的fileBook
				let sqlEditor = sqlModelSqlEditor.editor.editor;
				sqlEditor.setValue(args.executeSQL);
			}
			if (this.onlyViewDataSource == "true") {
				this.pageOnlyViewDataSource(leftTreeView);
			}
			if (!isEmpty(this.viewDataSource)) {
				this.pageViewDataSource(leftTreeView);
			}
			/**
			 * 2022-08-25 dingy
			 * CSTM-20148
			 * 隐藏数据源管理界面上方工具栏
			 */
			if (this.hideToolbar === 'true') {
				self.toolbar.setVisible(false);
			}
		});
	}

	/**
	 * 2022-05-18 liuyz
	 * CSTM-18826
	 * 数据管控需要能够让用户在前端查看前端数据库，并且能够编写sql，同时不能操作数据源，只能查看指定的数据源
	 * @param leftTreeView 资源管理界面左侧的视图控件
	 */
	private pageOnlyViewDataSource(leftTreeView: LeftTreeView): void {
		//this.toolbar.setVisible(false);
		leftTreeView?.dataTabBar?.tabbarItems[0].setVisible(false);
		let dataSourcePanel = leftTreeView?.dataSourcesPanel;
		let sefl = this;
		this.dataSourcesPanel = dataSourcePanel;
		dataSourcePanel.showDatasourceOperMenu = function (e: SZEvent, list: List, listItem: ListItem) {
			dataSourcePanel.opeDatasource = listItem.getValue();
			dataSourcePanel.toolTips && dataSourcePanel.toolTips.hideFloat();
			showMenu({
				keyPrefix: "dwmgr.datasources.operate",
				id: "datasourceoperateMenu",
				showAt: listItem.getDomToolbar(), // 菜单显示到下拉图标下，展示菜单时，下拉图标不消失
				toggleVisible: true,
				onhide: () => {
					dataSourcePanel.dbContextMenuVisible = false;
				},
				onshow: () => {
					dataSourcePanel.dbContextMenuVisible = true;
				},
				items: [{
					id: "refresh",
					cmd: "refreshDataSource"
				}, {
					id: "prop",
					cmd: "editDataSource"
				}, {
					id: "sql",
					cmd: "newSql"
				}],
				commandProvider: {
					getAllAvailableCommands: (): Array<string> => {
						return ['refresh', 'prop', 'sql']
					},
					cmd: dataSourcePanel.cmd.bind(self)
				}
			})
		}
		dataSourcePanel.panel_lower.tableList.oncontextmenu = function (sze: SZEvent, list: List, item: ListItem) {
			if (!item) {
				return;
			}
			showMenu({
				id: 'menu-dwamgr-tablelist-operate',
				keyPrefix: "tablelist.menu",
				showAt: <MouseEvent>sze.srcEvent,
				items: [{
					id: "createDbTableQuery",
					permissionOperation: "mgr-c-tbl",
					permissionTarget: 'module'
				}, {
					id: "viewDbTableData",
					permissionOperation: "view-basic",
					permissionTarget: 'source'
				},
				{
					id: "viewDbTableStructure",
					permissionOperation: "view-basic",
					permissionTarget: 'source'
				}, {
					id: "viewDbTableDDL",
					permissionOperation: "view-basic",
					permissionTarget: 'source'
				}],
				commandProvider: {
					cmd: dataSourcePanel.cmd.bind(dataSourcePanel),
				}
			})
			return false;
		}
	}

	/**
	 * 展示数据源页面
	 * @param leftTreeView 资源管理界面左侧的视图控件
	 */
	private pageViewDataSource(leftTreeView: LeftTreeView): void {
		let viewDataSource = this.viewDataSource;
		if (typeof this.viewDataSource == "string") {
			viewDataSource = this.viewDataSource.split(",");
		}
		let dataSourcePanel = leftTreeView?.dataSourcesPanel;
		let dbListDataProvider = dataSourcePanel.dbListDataProvider;
		dbListDataProvider.fetchDataOld = dbListDataProvider.fetchData;
		//重载掉数据源dp的fetchData方法，过滤最后的结果
		dbListDataProvider.fetchData = function (args?: { startIndex?: number, fetchCount?: number, parentData?: DefaultItemDataInfo }) {
			return dbListDataProvider.fetchDataOld(args).then(result => {
				return result.filter((i: DefaultItemDataInfo) => {
					return viewDataSource.includes(i.id);
				});
			})
		}
		let dbPanel = dataSourcePanel?.panel_upper;
		let dbList = dbPanel?.dbList;
		dbPanel.waitRender().then(() => {
			let items = dbList?.getItems();
			items && items.forEach((i: ListItem) => {
				if (!viewDataSource.includes(i.getId())) {
					i.setVisible(false);
				}
			})
		});
	}

	public _init_default(args: MetaFilesPageArgs) {
		super._init_default(args);
	}

	public _init(args: MetaFilesPageArgs): HTMLElement {
		let domBase = super._init(args);
		domBase.style.height = "100%"
		return domBase;
	}

	protected _getToolbarArguments(): JSONObject {
		let json = <ToolbarArgs>super._getToolbarArguments();
		json.items && (json.items = json.items.filter(item => item.id != "switchview"));
		return json;
	}

	/**
	 * 路由到到某个模块页面去
	 */
	public navigate(urlInfo: UrlInfo): Promise<any> {
		return this.showUrl(urlInfo).then(() => {
			return this;
		});
	}

	private refreshDataSource() {
		this.dataSourcesPanel.refreshDataSource();
	}

	private editDataSource() {
		let datasourceName = this.dataSourcesPanel.opeDatasource;
		return getDataSourceManager().then(dsm => {
			let editPromise = dsm.getConnection(this.projectName, datasourceName)
			return showWaiting(editPromise, this.getParentComponent().getDomParent(), false)
				.then((dbConnectionInfo: DbConnectionInfo) => {
					return import("datasources/datasourcesmgr").then((m) => {
						return m.showDbConnectionDialog({
							projectName: this.projectName,
							dbConnectionInfo: assign(dbConnectionInfo, { name: datasourceName }),
							readonly: true
						}).then(newDatasourceName => {
							delete this.dataSourcesPanel.errors[datasourceName];
							let selectedDatasourceName = this.dataSourcesPanel.selectedDatasourceName;
							//新建或编辑选中数据源后，选中新建后或编辑后的数据源；编辑未选中的数据源后，选中原先被选中的数据源
							let select = (selectedDatasourceName && (datasourceName !== selectedDatasourceName) ? selectedDatasourceName : newDatasourceName);
							this.dataSourcesPanel.refresh(select);
						});
					});
				});
		});
	}

	private newSql() {
		this.dataSourcesPanel.newSql();
	}

}

/**
 * 系统模块——安全页面
 */
export class SoleSystemSecurtiyAdaptPage extends SettingPage_securityConf {

	protected _init(args: any): HTMLElement {
		let domBase = super._init(args);
		return domBase;
	}

	/**
	 * @override
	 * 控制页面显示的模块，要求隐藏：回归测试、安全等保、阈值设置、日志记录
	 */
	protected getTabbarArgs(): TabbarArgs {
		return {
			layoutTheme: "texttabset",
			className: "syssettings-web-tabbar",
			keyPrefix: "sys.syssettings.securityConf",
			items: [
				{ id: 'passwordSettings' },
				{ id: 'loginSettings' },
				{ id: 'sessionSettings' },
				// { id: 'securityLevelProtection' },
				// { id: 'thresholds' },
				// { id: 'regressionTesting' },
				// { id: 'log' },
				{ id: 'masking' },
				{ id: 'algorithms' },
				// { id: 'sys' }
			],
			value: 'passwordSettings', // 默认选项不可以为空
		};
	}
}

/**
 * 计划任务
 * 问题：数据中台计划任务管理界面定制需求
 * 帖子地址：https://jira.succez.com/browse/CSTM-19061
 */
export class SDI_ScheduleMgrPage extends ScheduleMgrPage {

	/** 跳转的URL参数 */
	public schedulePageUrlParams: ScheduleUrlParam;

	constructor(args: MetaModulePageArgs) {
		super(args);
		this.schedulePageUrlParams = args.localUrlParams;
		this.localScheduleChildrenPage();
	}

	protected _init(args: MetaModulePageArgs): HTMLElement {
		let domBase = super._init(args);
		this.setScheduleDomStyle(domBase);
		return domBase;
	}

	/**
	 * 设置计划任务DOM样式
	 * @param domBase
	 */
	private setScheduleDomStyle(domBase: HTMLElement): void {
		domBase.className = "scheduleMetamodulepage-base";
		domBase.id = "scheduleMetamodulepage-base-id";
		domBase.style.position = "absolute";
		domBase.style.left = "0";
		domBase.style.top = "5px";
		domBase.style.width = "100%";
		domBase.style.height = "auto";
		domBase.style.bottom = "0";
	}


	/**
	 * 打开计划任务某个子页面
	 */
	private localScheduleChildrenPage(): Promise<any> {
		return this.initPromise.then(() => {
			if (!this.schedulePageUrlParams[':schedule']) {
				showWarningMessage("当前计划不存在计划任务列表中");
			}
			this.showUrl({ // 注意：若计划ID不存在计划任务列表，则显示所有计划
				params: this.schedulePageUrlParams
			})
		});
	}

	/**
	 * 重写打开计划子页面（ 打开某个计划的信息界面，通常会发生URL变化 ）
	 * 由于子页面不需要展示标题工具栏，重写此方法，将其标题栏显示设置为：false
	 * @param pageType 打开的界面ID：基本信息编辑界面、任务列表界面、计划执行日志界面
	 * @param needAnimation 是否需要动画，默认需要滑动动画，默认从计划列表进入计划详情界面需要，但是通过url路由打开，不需要动画。
	 * @param refreshHistoryState 是否需要刷新URL。
	 */
	public openScheduleInfoPage(pageType: SUB_PAGE_TYPE, urlInfo?: UrlInfo, needAnimation = true, refreshHistoryState = true): Promise<void> {
		this.currentInfoPageId = pageType;
		this.cancelPollingScheduleList();
		this.startListenSchedule();
		this.initSubPageContainer();

		this.titleNavigator.setVisible(false); // 不显示标题栏

		let refreshNavigator = this.subPageNavigator.refresh();
		let showPage = this.showScheduleInfoPage(pageType, urlInfo, needAnimation);
		let refreshHistroy = refreshHistoryState && this.pageContainer && this.pageContainer.refreshHistoryState();
		return <Promise<any>>Promise.all([refreshNavigator, showPage, refreshHistroy]);
	}

	/**
	 * 初始化计划任务模块子模块
	 *
	 * 20220907 tuzw
	 * 要求：计划任务管理——流程图内容英文名改成节点自带的中文描述
	 * 地址：https://jira.succez.com/browse/CSTM-20370
	 */
	protected initSubPage(pageId: SUB_PAGE_TYPE): ScheduleMgrSubPage {
		let page = this.subPages.get(pageId) as ScheduleMgrSubPage;
		if (!isEmpty(page)) {
			return page;
		}
		let className = "scheduleMgr-subPage";
		switch (pageId) {
			case SUB_PAGE_TYPE.Settings:
				page = new ScheduleConfPage({
					commandRenderer: this,
					className: className
				});
				break;
			case SUB_PAGE_TYPE.Tasks:
				page = new TaskListPage({
					pageContainer: this,
					className: className
				});
				break;
			case SUB_PAGE_TYPE.Logs:
				page = new ScheduleLogsPage({
					pageContainer: this,
					commandRenderer: this,
					className: className
				});
				break;
			case SUB_PAGE_TYPE.NewLog:
				page = new ScheduleNewLogPage({
					pageContainer: this,
					className: className
				});
				break;
			case SUB_PAGE_TYPE.DataFlow:
				page = new SDI_TASK_FLOW({
					pageContainer: this,
					scheduleId: this.getCurrentSchedule().id,
					isNeedToolBar: false
				});
				break;
			default:
				return null;
		}
		this.subPages.set(pageId, page);
		return page;
	}
}

/** 计划页面URL参数 */
interface ScheduleUrlParam {
	/** 页面类型，eg："tasks" */
	":infopage": string;
	/** 显示模式，eg："list" */
	":viewmode"?: string;
	/** 打开的计划ID，eg："48918c989d4f4bceafaefdaa2bcd4b6b" */
	":schedule": string;
	/** 显示的计划类型，为空显示所有计划，eg：etl */
	":type"?: string;
}

/**
 * 系统备份页面
 */
export class SDI_SettingPageBackUp extends SettingPage_backup {

	protected _init(args: any): HTMLElement {
		let domBase = super._init(args);
		return domBase;
	}

}

/**
 * 系统恢复页面
 */
export class SDI_SettingPageRestore extends SettingPage_restore {

	protected _init(args: any): HTMLElement {
		let domBase = super._init(args);
		this.setDomStyle(domBase);
		return domBase;
	}

	/**
	 * 更改DOM样式
	 * @param domBase 当前页面对象DOM
	 */
	private setDomStyle(domBase: HTMLElement): void {
		let childrenNodes = domBase.childNodes;
		for (let i = 0; i < childrenNodes.length; i++) {
			if (childrenNodes[i].className == 'syssettings-page-title') {
				domBase.childNodes[i].style.marginLeft = '10px';
				break;
			}
		}
	}
}

/**
 * 系统授权页面
 */
export class SDI_SettingPageTrustedapps extends SettingPage_trustedapps {

	protected _init(args: any): HTMLElement {
		let domBase = super._init(args);
		return domBase;
	}

}

/**
 * 2022-09-26 tuzw
 * 系统数据源页面
 * 
 * 2022-10-10 tuzw
 * 问题：定制的数据源页面，在进行新增、删除、编辑数据源等操作后，无法触发数据源刷新事件
 * 解决办法：初始化时，脚本添加元数据变化监听事件
 * 
 * 注意：
 * 1）产品绑定了元数据变化事件监听，经过排查原有是产品在初始化数据源页面时，会将当前项目的所有模块信息获取，
 * 在触发元数据变化刷新事件，会从一开始获取的模块信息查询，若没有获取到，则不触发刷新事: metadata.ts-refresh()
 * 2）定制数据源页面嵌入在iframe组件，存在多个页面，元数据对象被多次构造，初始获取的模块信息被清空，导致在页面进行新增等操作后，无法刷新页面数据
 */
export class SDI_SysDataSoucesPage extends MMP_Datasources {

	constructor(args: any) {
		args.projectName = 'sys';
		args.name = 'datasources';
		super(args);
		getMetaRepository().projectsMap = { "sysdata": {} }; // 元数据(metadata)刷新方法会先判空，但判空后会将内部元素置空，故初始化给个空壳子，越过判空操作
		getMetaEventListeners().on(BASIC_EVENT.filechange, this.modulechange_handler);
	}

	protected _init(args: any): HTMLElement {
		let domBase = super._init(args);
		super.refresh(); // 手动调用refresh方法，设置dp对象，查询数据源信息
		return domBase;
	}
}