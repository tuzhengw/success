
"index.dash": {
	onRender: (event) => {
		let page = <SuperPage>event.renderer;
		let currentBrowserInfo: BROWSER = getBrowserVersion();
		// 若浏览器为google，并且版本大于：'96.0.4664.45'，则隐藏更新图标
		if (currentBrowserInfo.BROWSER_NAME == 'chrome' && currentBrowserInfo.BROWSER_VERSION >= MIN_BROWSER_VERSION) {
			
			page.getComponent('icon1').setVisible(false);
			
		}
	}
}
	
	


/** 指定最低浏览器版本 */
const MIN_BROWSER_VERSION: string = '96.0.4664.45';
/**
 * 浏览器信息
 */
interface BROWSER {
	/** 浏览器名 */
	BROWSER_NAME: string;
	/** 浏览器版本 */
	BROWSER_VERSION: string;
}

/**
 * 获取当前登录的版本号
 * return {
 *   BROWSER_NAME: "chrome",
 *   BROWSER_VERSION: "96.0.4664.45"
 * }
 */
function getBrowserVersion(): BROWSER {
	let userAgent = navigator.userAgent.toLowerCase();
	let matchExpression = /(msie|firefox|chrome|opera|version).*?([\d.]+)/;
	/**
	 *  0: "chrome/96.0.4664.45"
		1: "chrome"：浏览器类型
		2: "96.0.4664.45"：浏览器版本
		groups: undefined
		index: 80
		input: "mozilla/5.0 (windows nt 6.1; win64; x64) applewebkit/537.36 (khtml, like gecko) chrome/96.0.4664.45 safari/537.36"
	 */
	let currentBrowserInfo = userAgent.match(matchExpression);
	let returnBroserInfo: BROWSER = {
		BROWSER_NAME: currentBrowserInfo[1],
		BROWSER_VERSION: currentBrowserInfo[2]
	}
	return returnBroserInfo;
}