/**
 * =====================================================
 * 作者：tuzw
 * 审核人员：liuyz
 * 创建日期:2022-06-28
 * 功能描述：数字化发票管理系统定巡校验详情配置界面“校验时间”设置定制
 * 地址：https://jira.succez.com/browse/CSTM-19376
 * =====================================================
 */
import { CronResultInfo, ScheduleComboBox, ScheduleComboBoxArgs } from "schedule/schedulecombobox/schedulecombobox";
import { isEmpty, showWarningMessage } from "sys/sys";
/**
 * 自定义计划页面-时间选择面板
 */
export class CustomScheduleTimePanel extends ScheduleComboBox {

    /** 记录设置执行频率的组件ID，eg：input1 */
    private saveScheduleRateCompId: string;
    /** 记录执行频率的中文描述组件ID，eg：Input2 */
    private saveScheduleDescCompId: string;

    /** event对象 */
    private event;

    /**
     * @param saveTimeValueCompId 记录设置时间值的组件ID
     * @param saveScheduleDescCompId 记录执行频率的中文描述组件ID
     */
    constructor(args?: ScheduleComboBoxArgs) {
        super(args);
        this.saveScheduleRateCompId = args.saveScheduleRateCompId;
        this.saveScheduleDescCompId = args.saveScheduleDescCompId;
    }

    protected _init(args: any): HTMLElement {
        let domBase = super._init(args);
        this.setDomStyle(domBase);
        this.event = args.renderer;
        return domBase;
    }

    /**
     * 设置样式
     */
    private setDomStyle(domBase: HTMLElement): void {
        domBase.style.width = '100%';
        domBase.style.height = '100%';
        domBase.style.minWidth = '300px';
        domBase.style.minHeight = '30px';
        domBase.style.display = 'flex';
        domBase.style.alignItems = 'center';
        domBase.style.justifyContent = 'center';
        domBase.style.border = '1px solid';
        domBase.style.borderColor = "rgb(210, 210, 210)";
    }

    /**
     * 将其设置的时间，set到指定控件中
     * 注意：args.renderer.getData().getComponent("input3").setValue("123")
     * 1）提交的组件处于隐藏状态，无法获取到dom，从而无法setValue值
     */
    public setValue(value: CronResultInfo, isChange = false): void {
        super.setValue(value);
        if (!isEmpty(this.saveScheduleRateCompId)) {
            let scheduleRateComp = this.event.getData().getComponent(this.saveScheduleRateCompId);
            if (isEmpty(scheduleRateComp)) {
                showWarningMessage(`没有获取到组件ID为：${this.saveScheduleRateCompId} 组件对象，无法写入设置的执行频率`);
                return;
            }
            scheduleRateComp.setValue(value.cronText);

            let scheduleDescComp = this.event.getData().getComponent(this.saveScheduleDescCompId);
            if (isEmpty(scheduleDescComp)) {
                showWarningMessage(`没有获取到组件ID为：${this.saveScheduleDescCompId} 组件对象，无法将执行频率中文描述写入`);
                return;
            }
            scheduleDescComp.setValue(this.domBase.textContent);
        }
    }
}