/**
 * 4.0 前端脚本注释规范
 
 
 * 作者：XXX
 * 审核人员：
 * 创建日期：
 * 脚本入口：CustomJS
 * 功能描述：
 * API参考文档：
 */
import { IMetaFileCustomJS, InterActionEvent, IAppCustomJS, IDwCustomJS, FAppInterActionEvent, IFApp, IDataset, IFAppForm } from "metadata/metadata-script-api";
import { showDrillPage, getMetaRepository, MetaFileViewer, getCurrentUser, getSysSettings } from 'metadata/metadata';

/**
 * 数据交换列表界面类
 */
export class Spg_Data_Exchange_List {
	public CustomActions: any;
	constructor() {
		this.CustomActions = {
			button_queryAPI: this.button_queryAPI.bind(this)
		}
	}

	public onRender(event: InterActionEvent) {
		let component = event.component;
		let dataView = component && component.getDataViewRow();
		if (component && (component.getId() == 'button11' || component.getId() == 'text17') && dataView && dataView.getKeys()) {
			let value = <string>component.getValue();
			value = value.trim();
			let color = null;
			if (value == "申请通过") {
				color = "#32BF72";
			} else if (value !== "待业务部门审批" && value !== "待技术部门审批" && value.indexOf('待审批') == -1) {
				color = "#FF1616";
			}
			let font = assign({}, component.getProperty("font"));
			color && (font.color = color);
			color && component.setProperty("font", font)
		}
	}

	/**
	 * 20220214  tuzw
	 * 帖子地址：https://jira.succez.com/browse/CSTM-17814
	 * 设置计划任务---编辑选中的计划
	 */
	private button_queryAPI(event: InterActionEvent) {
		let params = event.params;
		let scheduleId = params.scheduleId;
		return getScheduleMgr().getSchedule(scheduleId).then(scheduleInfo => {
			showScheduleDialog(scheduleInfo, true, false);
		})
	}
}

/**
 * 通用功能写在：spg{ }中，独有功能，按SPG分类，使用new XXX()
	export const CustomJS: { [type: string]: IMetaFileCustomJS } = {
		
	"/SJDWFW/app/数据服务.app/数据服务/检验报告信息.spg": new SJDWFW(),
	....
	
	4.19以上版本支持
 */
export const CustomJS: { [type: string]: IMetaFileCustomJS } = {

	tbl: <IDwCustomJS>{
		onDidInitPanel: (panel: IDwTableEditorPanel): void | Promise<void> => {
		},
		onRefreshPanel: (panel: IDwTableEditorPanel): void | Promise<void> => {
		},
		onDidRefreshPanel: (panel: IDwTableEditorPanel): void | Promise<void> => {
		},
		onDidLoadFile: (viewer: DwTableEditor, file: MetaFileInfo) => {
		}
	},
	spg: {
		CustomActions: {
			file_list_showAddLabel: (event: InterActionEvent) => {
			}
		}
	},
	"导入excel.spg": {
		// 可将某SPG内的点击事件写成一个独立的区块
		CustomActions: Label_CustomActions
	},
	app: {
		onDidInitFrame: (frame: Component) => {
		}
	} as IAppCustomJS,
	fapp: {
		CustomActions: {
			ex_apply_list_showFormSpg: (event: InterActionEvent) => {
				let vc = event.component;
				let selectedRows = vc.getSelectedDataRows();
				if (selectedRows.length === 0) {
					return;
				}
			}
		}
	},
	dash: {
		onRender: (event: InterActionEvent): Promise<void> => {

            return Promise.resolve();
        }
		onRefresh: (event) => {
		}，
		onDidLoadFile: (viewer, file) => {
		}，
		CustomActions: {
			showMyMessageDialog: (event) => {
			}
		}
	}
}