/**
 * ============================================================
 * 作者: tuzhengw
 * 审核人员：liuyongz
 * 创建日期：20220614
 * 项目：江苏数据中台
 * 功能描述：
 *      通过脚本批量检查归集的数据加工，将至少存在一行数据属性——选中
 * 帖子地址：https://jira.succez.com/browse/BI-44959?page=com.atlassian.jira.plugin.system.issuetabpanels%3Acomment-tabpanel&focusedCommentId=198113
 * ============================================================
 */
import { getString, modifyFile } from "svr-api/metadata";
import { getBean } from "svr-api/bean";

/**
 * 脚本执行入口
 */
function main() {
    let parentFilePath: string = "/sdi/data/tables/sys/data-access/";
    batchUpdateProcessProperties(parentFilePath);
}

/**
 * 批量更新指定目录下数据加工属性（至少存在N行数据）
 * @param fileDirPath 元数据文件目录地址
 * 
 * 异常：
 * 1. 保存元数据报：唯一键异常，原因：
 * （1） 每次保存元数据，会触发emerge 或 insert，将当前模型的字段（dimensions）写入到文件表（META_FIELDS.tbl)
 * （2） 修改的元数据部分字段name相同，导致写入重复（主键重复）（文件表主键（模型resid和字段描述））
 */
function batchUpdateProcessProperties(fileDirPath: string): void {
    print(`开始执行批处理脚本——批量更新数据加工-至少存在N行属性`);
    let startTime: number = Date.now();

    let fileMgr = getBean("com.succez.metadata.service.MetaServiceRepository");
    let recordModifyErrorModelTable: string[] = [];

    /** 目录下的所有子文件夹 */
    let listFolderPaths: string[] = fileMgr.listFiles(fileDirPath, false);
    for (let folderIndex = 0; folderIndex < listFolderPaths.size(); folderIndex++) { // 外层子文件夹
        let listFolderPath: string = listFolderPaths.get(folderIndex);
        let listFilePaths: string[] = fileMgr.listFiles(listFolderPath, false);
        print(`${folderIndex + 1}、开始处理路径：${listFolderPath} 下的数据加工文件，共需要处理： ${listFilePaths.size()} 个`);

        for (let metaFileIndex = 0; metaFileIndex < listFilePaths.size(); metaFileIndex++) { // 内层数据加工元数据文件
            let metaFilePath: string = listFilePaths[metaFileIndex];
            print(`（${metaFileIndex + 1}）、开始处理模型：${metaFilePath}`);
            /**
             * 注意：getTable方法获取的元数据信息对象类型不是JSON，不能使用Object.keys方法
             * 由于需要修改属性——至少存在N行数据，在nodes的第一个key中，但第一个key是一个uuid值，必须通过Object.key方法动态获取第一个key，从而获取对应的value值，eg：
             * nodes: {
             *   "a561538e705d45c098a931a24ef64245": {
             *      "checkRows": false,
                    "checkRowCount": 1
             *    }
             * }
             */
            let metaTable: DwTableInfo = JSON.parse(getString(metaFilePath));
            let metaTableNode = metaTable.dataFlow.nodes;
            let nodeKey: string = Object.keys(metaTableNode)[0];

            let checkRows: boolean = metaTableNode[nodeKey]["checkRows"];
            let checkRowCount: number = metaTableNode[nodeKey]["checkRowCount"];
            if (!!checkRows && !!checkRowCount && checkRowCount > 0) {
                print(`已设置至少N行属性，不重复设置`);
                continue;
            }
            metaTable.dataFlow.nodes[nodeKey]["checkRows"] = true;
            metaTable.dataFlow.nodes[nodeKey]["checkRowCount"] = 1;

            let metaFileModifyInfo: MetaFileModifyInfo = {
                content: JSON.stringify(metaTable)
            }
            try {
                modifyFile(metaFilePath, metaFileModifyInfo);
                print(`设置至少N行属性完成\n`);
            } catch (e) {
                recordModifyErrorModelTable.push(metaFilePath);
                print(`更新失败`);
                print(e.toString());
            }
        }
        print(`已全部更新完成：路径：${listFolderPath} 下的数据加工文件\n`);
    }

    print(`更新失败的模型路径：`);
    print(recordModifyErrorModelTable);

    print(`结束执行批处理脚本——批量更新数据加工-至少存在N行属性，共耗时：${(Date.now() - startTime) / 1000} s`);
}