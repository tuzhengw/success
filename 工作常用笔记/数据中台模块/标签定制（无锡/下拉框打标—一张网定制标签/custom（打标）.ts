"/ZHJG/app/home.app/JsComponent/编辑标签模块/labelDemo.spg": {
		CustomActions: {
			button_editLabelInfos: (event: InterActionEvent) => {
				let page = event.page;
				let params = event?.params;
				let jsComponentId: string = params.jsComponentId;
				if (!jsComponentId) {
					showWarningMessage("未给定绑定的JS控件ID值");
					return false;
				}
				let jsComp: EditLabelModule = page.getComponent(jsComponentId).component.getInnerComoponent();
				jsComp.adjustLabelInfo(); // 根据勾选差异，更新当前行标签信息 
			}
		}
	}