import dw from "svr-api/dw";
import db from "svr-api/db";
import metadata from "svr-api/metadata";
import utils from 'svr-api/utils';
import security from 'svr-api/security';


/**
 * 功能：获取查看物理表的SQL编辑器框URL
 * @param dataSource 数据源名
 * @param dbTable 查看的物理表名
 * @param isLocalNewSQLPage 是否定位到新建SQL查询页面，eg：'true'，默认：false
 * @param executeSQL? 执行的SQL语句
 * @param schema? 
 * @return  {
 * 	 result: boolean,
 *   message: string,
 *   url: string
 * }
 */
export function getDbTableUrl(request: HttpServletRequest, response: HttpServletResponse, params: {
	dataSource: string;
	dbTable: string;
	isLocalNewSQLPage: string;
	executeSQL?: string;
	schema?: string;
}) {
	console.debug(`getDbTableUrl(), 获取查看物理表的SQL编辑器框URL, params: ${params}`);
	let dataSource: string = params.dataSource;
	if (!dataSource) {
		return { result: false, message: "参数数据源名不可以为空" };
	}
	let dbTable: string = params.dbTable;
	let isLocalNewSQLPage: boolean = !!params.isLocalNewSQLPage ? true : false;
	let schema = params.schema as string;
	let executeSQL: string = !!params.executeSQL ? params.executeSQL : '';
	let ds = db.getDataSource(dataSource);
	if (!schema) {
		schema = ds.getDefaultSchema();
	}
	let url: string = "";
	if (isLocalNewSQLPage) { // 打开新建SQL查询页面
		url = `/sysdata/app/zt.app/commons/biBuiltInPages.html?urlParams=:new=tbl%40Sql&:locate=dbtable@${dataSource}:${schema}&executeSQL=${executeSQL}`;
	} else { // 打开指定表的SQL查询页面
		if (!ds.isTableExists(`${schema}.${dbTable}`)) {
			console.debug(`物理表:【${dbTable}】不存在`);
			return { result: false, message: `数据源【${dataSource}】下, 物理表【${dbTable}】不存在` };
		}
		url = `/sysdata/app/zt.app/commons/biBuiltInPages.html?urlParams=:open=dbtable@${dataSource}:${schema}:${dbTable}&:tblview=modelData`;
	}
	console.debug(`getDbTableUrl(), 请求URL: ${url}`);
	return { result: true, url: url };
}


// /sysdata/app/zt.app/commons/biBuiltInPages.html?type=security    urlParam 就是一个接受传递参数的变量