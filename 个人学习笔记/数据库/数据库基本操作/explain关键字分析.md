# explain关键字分析

```mysql
mysql> explain select * from servers;
```

​	分析结果：

|  id  | select_type |  table  | type | possible_keys | key  | key_len | ref  | rows | Extra |
| :--: | :---------: | :-----: | :--: | :-----------: | :--: | :-----: | :--: | :--: | ----- |
|  1   |   simple    | servers | all  |     null      | null |  null   | null |  1   | null  |

# 1. id

> **注意：**Id越大，优先级越高（<button>先执行那条SQL</button>）
>
> ​	（这里的ID指：explain分析结果表中的ID）

（1） id相同时，执行顺序由上至下

（2）子查询：id的序号会递增，id值越大优先级越高，越先被执行

（3）id相同的一组，从上往下顺序执行；在所有组中，id值越大，优先级越高，越先执行

---

# 2. select_type

|         类型         | 说明                                                         |
| :------------------: | ------------------------------------------------------------ |
|        simple        | 简单SELECT，不使用UNION或子查询等                            |
|       primary        | 查询中若包含<button>任何复杂的子部分</button>，最外层的select被标记为PRIMARY |
|        union         | UNION中的第二个 或 后面的SELECT语句                          |
|   dependent union    | UNION中的第二个或后面的SELECT语句，取决于<button>外面的查询</button> |
|     union result     | UNION的结果                                                  |
|       subquery       | 子查询中的第一个SELECT                                       |
|  dependent subquery  | 子查询中的第一个SELECT，取决于外面的查询                     |
|       derived        | 派生表的SELECT，FROM子句的子查询                             |
| uncacheable subquery | 一个子查询的结果不能被缓存，必须重新评估外链接的第一行       |

# 3. table

​	显示这一行的数据是关于哪张表的

```mysql
explain select * from (
    select * from 
    	( select * from t1 where id=2602)  a )  
    b;
```

​	**结果：**有时《不是真实的》表名字，看到的是derivedx（x是个数字，理解是第几步执行的结果）

|  id  | select_type |  table   |  type  | possible_keys | key  | key_len | ref  | rows | Extra |
| :--: | :---------: | :------: | :----: | :-----------: | :--: | :-----: | :--: | :--: | ----- |
|  1   |   primary   | derived1 | system |     null      | null |  null   | null |  1   |       |
|  2   |   derived   | derived2 | system |     null      | null |  null   | null |  1   |       |
|  3   |   derived   |    t1    | const  |      ...      | ...  |   ...   | ...  | ...  | ...   |

# 4. type

​	MySQL在表中找到所需行的方式，又称《访问类型》

> 常用的类型有：
>
> ​	 **ALL, index, range, ref, eq_ref, const, system, NULL（从左到右，性能从差到好）**

---

|  类型  | 说明                                                         |
| :----: | ------------------------------------------------------------ |
|  all   | **（Full Table Scan）** 遍历全表以找到匹配的行               |
| index  | （**Full Index Scan**），index与ALL区别为index类型《只遍历索引树》 |
| range  | 只检索给定范围的行，使用一个索引来选择行                     |
|  ref   | 表示《所有具有匹配的索引的行》都被用到                       |
| eq_ref | 类似ref，区别就在使用的索引是《唯一索引》<br />对于每个索引键值，表中只有一条记录匹配<br />简单来说，就是多表连接中使用 primary key 或者 unique key 作为关联条件 |
| const  | 当MySQL对查询某部分进行优化，并转换为一个<button>常量</button>时，使用这些类型访问<br />如：将主键置于where列表中，MySQL就能将该查询转换为一个常量 |
| system | 是const类型的特例，当查询的表<button>只有一行</button>的情况下，使用system |
|  null  | 优化过程中分解语句，执行时甚至《不用访问表或索引》<br />例如：从一个索引列里选取最小值可以通过单独索引查找完成 |

# 5. possble_key

​	指出MySQL《能使用哪个索引》在表中找到记录，查询<button>涉及到的字段</button>上《若存在索引，则该索引将被列出》，但不一定被查询使用

​	如果该列是NULL，则没有相关的索引

​	在这种情况下，可以通过检查WHERE子句看是否它《引用某些列或适合索引的列》来提高你的查询性能

---

# 6. key

​	key列显示MySQL《实际决定使用的键》（索引）

​	如果没有选择索引，键是NULL

# 7. key_len

​	表示索引中使用的：**字节数**

​	字节数：可通过《**该列计算查询中使用的索引的长度**》（key_len显示的值为索引字段的最大可能长度，**并非实际使用长度**，即key_len是根据表定义计算而得，不是通过表内检索出的）

​	不损失精确性的情况下，长度**越短越好** 

---

# 8. ref

​	表示《上述表》的：连接匹配条件，即《哪些列或常量》被 用于查找索引列上的值

---

# 9.  row

​	 表示MySQL根据：表统计信息 及 索引选用情况，估算的找到：《所需的记录》 、《所需要读取的行数》

---

# 10. Extra

​	**该列包含MySQL解决查询的详细信息**

|                情况                | 说明                                                         |
| :--------------------------------: | ------------------------------------------------------------ |
|            Using where             | 列数据：仅仅使用了索引中的信息，而没有读取实际的行动的表返回的<br />这发生在《对表的全部的请求列》<button>都是同一个索引的部分</button>的时候<br />（表示mysql服务器将在存储引擎检索行后再进行过滤） |
|          Using temporary           | 需要使用<button>临时表</button>来存储结果集，常见于：排序和分组查询 |
|           Using filesort           | 《无法利用索引》完成的排序操作称为：“文件排序”               |
|                                    | （如果出现 Using temporary 和 Using filesort 说明效率低）    |
|         Using join buffer          | 在获取连接条件时《没有使用索引》，并且需要连接缓冲区来存储中间结果<br />（如出现，那应注意，根据查询的具体情况《可能需要添加索引》来改进能） |
|          mpossible where           | 强调了where语句会导致<button>没有符合</button>条件的行       |
| Select tables <br />optimized away | 《仅》通过使用索引，优化器可能仅从聚合函数结果中返回一行     |



​	