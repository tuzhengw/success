


/**
 * 校验指定数据源在当前项目模块下是否可用
 * <p>
 *  2023-01-13 tuzw
 *  问题：原本由前端发起webApi检测数据源使用状态, 但检测时间过长, 页面无等待样式, 看似像卡住
 *  解决办法：前端发起检测请求, 并增加等待框
 * </p>
 * @param projectName 当前项目名，eg：sys、sdi
 * @param dbSourceName 检测的数据源名
 * @param optionType 操作类型，eg：add 、edit
 * @param dbName 数据库名
 * @param dbHost 数据库IP地址
 * @param dbPort 数据库端口号
 * @param dbUser 数据库用户
 * @param dbPassword 数据库密码
 * @param dbUrl jdbc链接地址
 * @return 
 */
private checkSignDbSourceUseStatus(event: InterActionEvent): Promise<any> {
	let params = event.params;
	let { projectName, dbSourceName, optionType, dbName, dbHost, dbPort, dbUser, dbPassword, dbUrl } = params || {};
	let checkUsePromise: Promise<any> = rc({
		url: '/zt/misMgr/checkDbSourceUse',
		method: "POST",
		data: {
			projectName: projectName,
			dbSourceName: dbSourceName,
			optionType: optionType,
			dbName: dbName,
			dbHost: dbHost,
			dbPort: dbPort,
			dbUser: dbUser,
			dbPassword: dbPassword,
			dbUrl: dbUrl
		}
	});
	return showWaiting(checkUsePromise).then((result => {
		if (!result.result) {
			showWarningMessage(result.message);
			return false;
		}
		return new Promise((resolve, reject) => { // new一个异步，通过resolve来判断是否执行结束
			showConfirmDialog({
				message: "当前数据源不可用是否要确认保存?",
				onok: (event, dialog) => {
					resolve(true);
				},
				oncancel: (event, dialog) => {
					resolve(false);
				}
			});
		})
	}));
}