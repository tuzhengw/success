```
# **ADJUST_TABLE_PRE_OR_SUF(tablePrefix: string, tableName: string): string**

 根据选择【前后缀】信息，修改表的前后缀。

**类型必须是string，且不为NULL!**

## 参数
***tablePrefix***： 前后缀和操作信息
***tableName***：处理的表名
***ignoreCase***：比较是否忽略大小写，默认不忽略

## 示例
1. `ADJUST_TABLE_PRE_OR_SUF("delPre.ODS_:addPre.NEW_", "ODS_TABLE_NAME")`  NEW_TABLE_NAME
2. `ADJUST_TABLE_PRE_OR_SUF("delSuf._SUF:addsuf._NEW", "TABLE_NAME_SUF")`  TABLE_NAME_NEW
3. `ADJUST_TABLE_PRE_OR_SUF("delSuf._suf:addsuf._NEW", "TABLE_NAME_SUF", true)`  TABLE_NAME_NEW
```

