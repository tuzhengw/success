/**
 * 4.0 前端脚本注释规范
 * 作者：XXX
 * 审核人员：
 * 创建日期：
 * 脚本入口：CustomJS
 * 功能描述：
 * API参考文档：
 */
import { IMetaFileCustomJS, InterActionEvent, IAppCustomJS, IDwCustomJS, FAppInterActionEvent, IFApp, IDataset, IFAppForm } from "metadata/metadata-script-api";
import { showDrillPage, getMetaRepository, MetaFileViewer, getCurrentUser, getSysSettings } from 'metadata/metadata';
import { ctx, SZEvent, Component, throwInfo, throwError, showConfirmDialog, showFormDialog, showErrorMessage, showMenu, isEmpty, deepAssign, uuid, message, showSuccessMessage, showWarningMessage, showProgressDialog, rc, DataChangeType, UrlInfo, downloadFile, waitRender, showWaiting, assign, IDataProvider, formatDateFriendly, parseDate, formatDate, tryWait, rc_get } from 'sys/sys';
import { showGallery } from "commons/basic";
import { Gallery, ShowGalleryArgs, ViewerType } from "commons/gallery";

export const CustomJS: { [type: string]: IMetaFileCustomJS } = {
    spg: {
        CustomActions: {
            /**
             * 在前端页面展示存储到：服务器上的文件，并可以实现：下载和预览
			 * filePath 与 fileType 一定要是同一类型， 否则不显示
             */
            down_local_file: (event: InterActionEvent) => {
                let params = event.params;
                let certificateNo: string = params.certificateNo;
                let qRPath = params.qRPath;
                if (!certificateNo) {
                    showWarningMessage("请选择需要展示的数据");
                    return;
                }
                if (!qRPath) {
                    showWarningMessage("单条数据没有二维码信息");
                    return;
                }
                let requestPath: string = `/HBLLJZJGC/app/RQ.app/custom.action?method=dealBlobFields&certificateNo=${certificateNo}`;
                showGallery({
                    items: [{
                        // 展示文件路径（"/"开头）
                        path: requestPath,
                        // 资源下载的url，如果存在，那么会显示下载按钮，否则不显示，以"/"开头
                        url: requestPath,
                        viewerType: getShowGalleryFileType("image")
                    }]
                });
            }
        }
    }
}

/**
 * 获取【showGallery】组件文件类型
 * @param fileType 文件类型，eg："png"
 * @return ViewerType 某常量
 */
function getShowGalleryFileType(fileType): ViewerType {
    switch (fileType) {
        case "image": return ViewerType.Image;
            break;
        case "video": return ViewerType.Video;
            break;
        case "audio": return ViewerType.Audio;
            break;
        case "pdf": return ViewerType.Pdf;
            break;
    }
}