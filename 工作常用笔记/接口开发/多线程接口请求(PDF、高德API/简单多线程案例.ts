
import db from "svr-api/db";
import sys from "svr-api/sys";
import security from "svr-api/security";
import { getFile, getString } from 'svr-api/metadata';

/** 等待时间毫秒数 */
let waitTime = 3000;
/** 最大线程池数量 */
let maximumPoolSize = 10;

function main() {
    let rest = new ThreadTest();
    rest.refreshModelInfos();
}

class ThreadTest {

   
    public refreshModelInfos(): void {
        let threadpool: ThreadPool = sys.getThreadPool({
            name: 'autoFlush',
            maximumPoolSize: maximumPoolSize,
            corePoolSize: 4
        });
        let refreshModelNums: number = 1;
        let refreshModelIndex: number = 0;
        let progress = sys.getThreadProcessMonitor();
        security.loginWithoutPassword("admin");
        while (refreshModelIndex < refreshModelNums) {
            while (threadpool.getActiveCount() >= 10) {
                print(`线程池已满，等待线程池释放资源，当前还有：${refreshModelNums - refreshModelIndex - 1} 个模型待刷新`);
                sys.sleep(waitTime);
            }
            let tesk = threadpool.submit({
                method: 'autoFlush',
                params: [],
                path: "/sysdata/data/batch/t-test.action"
                progress: progress
            });
			tesk.get(); // 是获取当前所有线程执行的结果, 如果此处获取的话，这里就不会多线程执行，此处需要等待所有线程结束后获取
            refreshModelIndex++;
        }
    }
}


// 多线程执行脚本[/sysdata/data/batch/t-test.action]
import { test } from "/sysdata/app/ZSY.app/script/test/test.action"; // 引用脚本中不能包含引用时就执行查询的操作, 否则多线程提交后不会执行, 也不会报错。
/**
 * 自动刷新模型状态
 * @param modelInfo 刷新的模型信息
 * 
 * 2022-7-26
 * yueyang
 * 帖子地址:https://jira.succez.com/browse/CSTM-19801
 */
export function autoFlush(): void {
    for (let i = 0; i < 3; i++) {
        print(i);
    }
}

// 多线程执行脚本中引用脚本路径: [/sysdata/app/ZSY.app/script/test/test.action]
/**
 * 下方写法: 其他脚本 在引用 当前脚本时 就会创建实例对象, 构造器就会直接发起查询
 */
export class MainIndexMergeConfService { 

	constructor() {
		this.initMergeConf();
	}
	
	private initMergeConf() {
        // dw模块查询
    }
}
const mainIndexMergeConfService = new MainIndexMergeConfService();