# 常用语法

| 符号  |                     作用                      |
| :---: | :-------------------------------------------: |
|   y   |                      年                       |
|   M   |                      月                       |
|   d   |                      日                       |
| h / H |                  1-12 / 0-23                  |
|   m   |                      分                       |
| s / S |                   秒 / 毫秒                   |
|   E   |                     星期                      |
|   D   |                 一年中第几天                  |
|   F   |              一月中第几个星期几               |
| w / W |           一年 / 一月 中第几个星期            |
|   a   |              上午 / 下午 标记符               |
| k / K | 在一天中（1-24）时 / 在上午或者下午（0-11）时 |
|   z   |                     时区                      |



# Date转String

> 注意：需要将日期转换为：<button>时间毫秒数</button>

```
import SimpleDateFormat = java.text.SimpleDateFormat;
```

```ts
const SimpleDateFormat = Java.type('java.text.SimpleDateFormat');

const sdf1 = new SimpleDateFormat("yyyy-dd-mm hh:mm:ss");
const sdf2 = new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");
const sdf3 = 
      new SimpleDateFormat("一年中的第 D 天 一年中第w个星期 一月中第W个星期 在一天中k时 z时区");

let nowDate = new Date().getTime();

// 2021-04-06 04:06:07
print(sdf1.format(nowDate));
// 2021年11月04日 16时06分07秒
print(sdf2.format(nowDate));
// 一年中的第 308 天 一年中第45个星期 一月中第1个星期 在一天中16时 CST时区
print(sdf3.format(nowDate));
```



# 格式化时间

```ts
let sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
print(sdf.format(new java.util.Date())); //  2022-03-03 12:04:45
```



# 获取当天零点

```ts
import utils from "svr-api/utils";
function main() { // 产品方法
    print(utils.formatDate("yyyy-MM-dd HH:mm:ss", new Date().setHours(0, 0, 0)));
}
```

```ts
import LocalDateTime = java.time.LocalDateTime;
import LocalTime = java.time.LocalTime
import ZoneOffset = java.time.ZoneOffset;

// "2021-12-12 23:55:00"
let now = LocalDateTime.now();
let currentDay = LocalDateTime.of(now.toLocalDate(), LocalTime.MIN);
let times = currentDay.toInstant(ZoneOffset.of("+8")).toEpochMilli();
let sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
let lastSendTime: string = sdf.format(new java.util.Date(times - 24 * 60 * 60 * 1000 - 5 * 60 * 1000));
```

