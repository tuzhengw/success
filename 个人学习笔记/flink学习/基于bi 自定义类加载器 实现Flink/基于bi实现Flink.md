---
typora-root-url: images
---

# START

# 文档前提

​	`Flink、cdc、dlink`本身引用了很多依赖包，这些包可能会跟应用环境冲突，故需要整合一个独立的`jar`，使其内部依赖独立.

# 一 方法1 `maven` 工具生成一个整体`jar`

​	环境的应用加载机制不一样, 即使整合一个包，若加载的依赖是平级或者高于当前内的类的加载，==依然会和所属应用环境依赖包冲突==，但都是极少的包，若出现则需要自行排除.



## 1 `maven`打包工具介绍

==参考==：https://blog.csdn.net/ioteye/article/details/106867201

| `plugin`                | function                                         |
| ----------------------- | ------------------------------------------------ |
| `maven-jar-plugin`      | `maven 默认打包插件，用来创建 project jar`       |
| `maven-shade-plugin`    | 用来打可执行包，包含依赖，以及对依赖进行取舍过滤 |
| `maven-assembly-plugin` | 支持定制化打包方式，更多是对项目目录的重新组装   |

### 1.1 资源转换

在打包时，存在将多个构件中的class文件或资源文件聚合的需求。shade插件提供了丰富的Transformer工具类。这里介绍一些常用的Transformer。

==`ManifestResourceTransformer`==：往MANIFEST文件中写入Main-Class是可执行包的必要条件

```xml
<configuration>
	<transformers>
		<transformer
			implementation="org.apache.maven.plugins.shade.resource.ManifestResourceTransformer">
			<mainClass>com.lcifn.Application</mainClass>
		</transformer>
	</transformers>
</configuration>
```

==`AppendingTransformer`==：用来处理多个jar包中存在重名的配置文件的合并，尤其是spring

```xml
<configuration>
	<transformers>
		<transformer
			implementation="org.apache.maven.plugins.shade.resource.AppendingTransformer">
			<resource>META-INF/spring.handlers</resource>
		</transformer>
		<transformer
			implementation="org.apache.maven.plugins.shade.resource.AppendingTransformer">
			<resource>META-INF/spring.schemas</resource>
		</transformer>
	</transformers>
</configuration>
```

==`ServicesResourceTransformer`==：JDK的服务发现机制是基于META-INF/services/目录的，如果同一接口存在多个实现需要合并 ，则可以使用此Transformer

```xml
<configuration>
  <transformers>
    <transformer implementation="org.apache.maven.plugins.shade.resource.ServicesResourceTransformer"/>
  </transformers>
</configuration>
```



### 1.2 仅包含项目本身文件, 排除依赖

```xml
  <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-assembly-plugin</artifactId>
                <version>3.0.0</version>
            </plugin>
        </plugins>
```



## 2 `ServiceLoader`注意点

​	将第三方`jar`整合到一个`fat jar`包，若是项目里面使用了`ServiceLoader `加载对于的类，需要在`jar`的` META-INF/services/`下定义对于的文件, 例如：`apache.flink` 相关`jar`包内部定义了：

![](/例举apache类定义.png)

​	若是不定义则`ServiceLoader `无法匹配，从而提示==识别不到此实现类==，例如：

```java
Could not find any factories that implement 'org.apache.flink.table.delegation.PlannerFactory' in the classpath.
```

## 3 多个`jar`内配置文件路径和名称一致

​	打包后生成一个可执行文件: `flinkbi-1.0-SNAPSHOT.jar`， 直接拖到终端执行则会提示：`Could not instantiate the executor. Make sure a planner module is on the classpath`

### 3.1  `Make sure a planner module is on the classpath`

> `Could not instantiate the executor. Make sure a planner module is on the classpath`

```java
SLF4J: Failed to load class "org.slf4j.impl.StaticLoggerBinder".
SLF4J: Defaulting to no-operation (NOP) logger implementation
SLF4J: See http://www.slf4j.org/codes.html#StaticLoggerBinder for further details.
Exception in thread "main" org.apache.flink.table.api.TableException: Could not instantiate the executor. Make sure a planner module is on the classpath
	at org.apache.flink.table.api.bridge.internal.AbstractStreamTableEnvironmentImpl.lookupExecutor(AbstractStreamTableEnvironmentImpl.java:109)
	at com.dlink.executor.CustomTableEnvironmentImpl.create(CustomTableEnvironmentImpl.java:129)
	at com.dlink.executor.CustomTableEnvironmentImpl.create(CustomTableEnvironmentImpl.java:111)
	at com.dlink.executor.executor.RemoteStreamExecutor.createCustomTableEnvironment(RemoteStreamExecutor.java:31)
	at com.dlink.executor.executor.Executor.updateStreamExecutionEnvironment(Executor.java:173)
	at com.dlink.executor.executor.Executor.initStreamExecutionEnvironment(Executor.java:167)
	at com.dlink.executor.executor.Executor.init(Executor.java:141)
	at com.dlink.executor.executor.RemoteStreamExecutor.<init>(RemoteStreamExecutor.java:26)
	at com.dlink.admin.FlinkSync.flinkJobSubmit(FlinkSync.java:70)
	at TEST.main(TEST.java:14)
Caused by: org.apache.flink.table.api.TableException: Unexpected error when trying to load service provider.
	at org.apache.flink.table.factories.FactoryUtil.discoverFactories(FactoryUtil.java:826)
	at org.apache.flink.table.factories.FactoryUtil.discoverFactory(FactoryUtil.java:525)
	at org.apache.flink.table.api.bridge.internal.AbstractStreamTableEnvironmentImpl.lookupExecutor(AbstractStreamTableEnvironmentImpl.java:106)
	... 9 more
Caused by: java.util.ServiceConfigurationError: org.apache.flink.table.factories.Factory: org.apache.flink.table.sources.CsvBatchTableSourceFactory not a subtype
	at java.base/java.util.ServiceLoader.fail(ServiceLoader.java:588)
	at java.base/java.util.ServiceLoader$LazyClassPathLookupIterator.hasNextService(ServiceLoader.java:1236)
	at java.base/java.util.ServiceLoader$LazyClassPathLookupIterator.hasNext(ServiceLoader.java:1264)
	at java.base/java.util.ServiceLoader$2.hasNext(ServiceLoader.java:1299)
	at java.base/java.util.ServiceLoader$3.hasNext(ServiceLoader.java:1384)
	at org.apache.flink.table.factories.FactoryUtil.discoverFactories(FactoryUtil.java:813)
	... 11 more
```

​	没有找到某些`SPI`的实现类的意思，这些实现类本来应该是在依赖的Jar中的

### 3.2 分析: 生成的`jar`，仅取首个配置内容

​	例如：项目引用的多个`Flink jar`内的`META-INF`下的`services`下的配置文件：==路径和名称一致，但内容不同==。

![](/flink多个jar内配置路径和名称一致.png)

​	上述情况，会导致`mvn clean install`生成的`jar`文件仅取==首个配置内容==，其他相同文件内容都丢失，示例：

![](/整合一个jar后配置文件取首个.png)

### 3.3 配置内容合并

​	使用`maven`的`mvn-shade-plugin`实现

​	==参考==：https://blog.csdn.net/RL_LEEE/article/details/128134800

​	==参考==：https://zhuanlan.zhihu.com/p/610074889（避免多个jar通过maven打包成一个jar，多个同名配置文件发生覆盖问题）

​	`POM`文件内容：

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
 
    <groupId>com.chinaums.dss</groupId>
    <artifactId>flink-mock</artifactId>
    <version>1.0-SNAPSHOT</version>
 
	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<java.version>1.8</java.version>
		<maven.compiler.source>${java.version}</maven.compiler.source>
		<maven.compiler.target>${java.version}</maven.compiler.target>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
		<flink.version>1.16.0</flink.version>
		<slf4j.version>1.7.15</slf4j.version>
		<log4j.version>2.12.1</log4j.version>
		<fastjson.version>1.2.83</fastjson.version>
	</properties>
 
	<dependencies>
		<!-- flink start-->
		<dependency>
			<groupId>org.apache.flink</groupId>
			<artifactId>flink-table-planner_2.12</artifactId>
			<version>${flink.version}</version>
		</dependency>
		<dependency>
			<groupId>org.apache.flink</groupId>
			<artifactId>flink-table-api-java-bridge</artifactId>
			<version>${flink.version}</version>
		</dependency>
		<dependency>
			<groupId>org.apache.flink</groupId>
			<artifactId>flink-table-api-java</artifactId>
			<version>${flink.version}</version>
		</dependency>
		<dependency>
			<groupId>org.apache.flink</groupId>
			<artifactId>flink-clients</artifactId>
			<version>${flink.version}</version>
		</dependency>
		<!-- flink end-->
		<dependency>
			<groupId>org.projectlombok</groupId>
			<artifactId>lombok</artifactId>
			<version>RELEASE</version>
			<scope>provided</scope>
		</dependency>
	</dependencies>
	<!-- 指定maven编译方式为jdk1.8版本 -->
	<profiles>
		<profile>
			<id>prod</id>
			<activation>
				<activeByDefault>true</activeByDefault>
				<jdk>1.8</jdk>
			</activation>
			<properties>
				<maven.compiler.source>1.8</maven.compiler.source>
				<maven.compiler.target>1.8</maven.compiler.target>
				<maven.compiler.compilerVersion>1.8</maven.compiler.compilerVersion>
			</properties>
		</profile>
	</profiles>
 
	<build>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-shade-plugin</artifactId>
				<executions>
					<execution>
						<phase>package</phase>
						<goals>
							<goal>shade</goal>
						</goals>
						<configuration>
							<transformers>
                                 <!--  处理多个Flink内同路径同名配置内容合并 -->
                                <transformer implementation="org.apache.maven.plugins.shade.resource.ServicesResourceTransformer">
                                </transformer>
                                <!--  配置主类 -->
								<transformer implementation="org.apache.maven.plugins.shade.resource.ManifestResourceTransformer">
									<manifestEntries>
										<Main-Class>SqlMockTask</Main-Class>
									</manifestEntries>
								</transformer>
							</transformers>
							<filters>
								<filter>
									<artifact>*:*</artifact>
									<excludes>
										<exclude>META-INF/*.SF</exclude>
										<exclude>META-INF/*.DSA</exclude>
										<exclude>META-INF/*.RSA</exclude>
									</excludes>
								</filter>
							</filters>
						</configuration>
					</execution>
				</executions>
			</plugin>
		</plugins>
	</build>
</project>
```

​	==必须指定 "主类"==，否则使用生成的包，运行时会提示：

```
服务配置文件不正确, 或构造处理程序对象Invalid signature file digest for Manifest main attributes时抛出异常错误
```

​	合并后，其最终的包内配置文件，例如：

![](/合并依赖内的同名配置内容.png)

 

## 4 整体`jar`放入其他项目注意点 

### 1  `flink`本身重写`apache`相关类

​		其他项目==可能直接引用==了`Flink`重写的`apache`的包，这样就==导致两者冲突==，例如：

​		包 `flink-table-planner_2.12-1.16.2.jar`，其重写内容：

![](/Flink内重写apache相关类内容示例.png)

​	基于bi时，其类`calcite`冲突，提示：

```
Could not initialize class org.apache.calcite.config.Lex
```

![](/bi冲突jar冲突.png)

​	去掉后，成功提交



# 二 方法2 自定义类加载器动态加载依赖

​	提交到`bi`合并请求：

> https://gitlab.succez.com/product/bi/-/merge_requests/36277//diffs#edc730b836727cd88db31354a1c1962fddef1d3a

---

## 1 关于类加载说明

​	`Dlink`本身实现了一个类加载机制，但它是将其作为顶层类加载，若是外部自定义类加载器，内部无法识别.

## 2 `Dlink`类加载增加自定义父加载

![](/dlink本身自定义类加载.png)

## 3 `DynamicJarClassLoader`

```java
package classLoader;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;

/**
 * 提供Jar隔离的加载机制，会把传入的路径、及其子路径、以及路径中的jar文件加入到class path.
 * <p>用于环境动态加载指定文件夹下的所有jar文件</p>
 *
 * @createdate 2024-09-07
 */
public class DynamicJarClassLoader extends URLClassLoader {

    public DynamicJarClassLoader(String[] paths) throws Exception {
        this(paths, DynamicJarClassLoader.class.getClassLoader());
    }

    public DynamicJarClassLoader(String[] paths, ClassLoader parent) throws Exception {
        super(getURLs(paths), parent);
    }

    private static URL[] getURLs(String[] paths) throws Exception {
        List<File> jarFiles = new ArrayList<>();
        for (String path : paths) {
            findJarFiles(path, jarFiles);
        }
        if (jarFiles.size() == 0) {
            throw new Exception("paths下未匹配到jar文件对象");
        }
        List<URL> jarURLs = new ArrayList<>(jarFiles.size());
        for (File jar : jarFiles) {
            try {
                jarURLs.add(jar.toURI().toURL());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return jarURLs.toArray(new URL[0]);
    }

    /**
     * 递归地获取所有子目录中的jar文件
     *
     * @param filePath 寻找文件路径
     * @param jarFiles 记录符合的jar文件对象
     */
    public static void findJarFiles(String filePath, List<File> jarFiles) {
        File fileInfo = new File(filePath);
        if (!fileInfo.exists()) {
            return;
        }
        if (fileInfo.isDirectory()) {
            File[] files = fileInfo.listFiles();
            if (files == null) {
                return;
            }
            for (File file : files) {
                if (file.isDirectory()) {
                    findJarFiles(file.getPath(), jarFiles); // 递归调用，处理子目录
                } else if (file.getName().endsWith(".jar")) {
                    jarFiles.add(file);
                }
            }
        } else {
            if (fileInfo.getName().endsWith(".jar")) {
                jarFiles.add(fileInfo);
            }
        }
    }
}
```



## 4 `DynamicJarClassLoaderFactory`

```java
package classLoader;

import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 动态类加载工程类
 *
 * @createdate 2024-09-07
 */
@Component
public class DynamicJarClassLoaderFactory {

    /**
     * 缓存已加载过的类加载器
     */
    private Map<String, DynamicJarClassLoader> classLoaderCache = new ConcurrentHashMap<>();
    /**
     * 缓存类加载器对应加载路径
     */
    private Map<String, String[]> loaderPathsMapping = new ConcurrentHashMap<>();

    /**
     * 创建类加载器对象
     *
     * @param loaderKey 获取类加载器唯一标识(key全局唯一）
     * @param paths     加载指定文件夹或者jar文件
     */
    public DynamicJarClassLoader createJarLoader(String loaderKey, String[] paths) throws Exception {
        DynamicJarClassLoader jarLoader = classLoaderCache.get(loaderKey);
        if (jarLoader != null) {
            throw new Exception(loaderKey + "已经存在对应的类加载对象, 请重新指定或先清理已有key");
        }
        if (paths == null || paths.length == 0) {
            throw new Exception("入参paths不可为空");
        }
        jarLoader = new DynamicJarClassLoader(paths);
        classLoaderCache.put(loaderKey, jarLoader);
        loaderPathsMapping.put(loaderKey, paths);
        return jarLoader;
    }

    /**
     * 获取类加载器对象
     *
     * @param loaderKey 获取类加载器唯一标识
     * @return 若不存在则返回null.
     */
    public DynamicJarClassLoader getJarLoader(String loaderKey) {
        return classLoaderCache.get(loaderKey);
    }

    /**
     * 销毁对应的类加载对象
     *
     * @param loaderKey 获取类加载器唯一标识
     */
    public boolean disposeJarLoader(String loaderKey) throws IOException {
        DynamicJarClassLoader jarLoader = classLoaderCache.get(loaderKey);
        if (jarLoader == null) {
            return true;
        }
        jarLoader.close();
        classLoaderCache.entrySet().removeIf(entry -> loaderKey.equals(entry.getKey()));
        loaderPathsMapping.entrySet().removeIf(entry -> loaderKey.equals(entry.getKey()));
        return true;
    }

    /**
     * 刷新类加载对象
     * <p>会根据历史配置的加载路径重新加载最新的jar对象</p>
     *
     * @param loaderKey 获取类加载器唯一标识
     */
    public void refreshJarLoader(String loaderKey) throws Exception {
        DynamicJarClassLoader jarLoader = classLoaderCache.get(loaderKey);
        if (jarLoader == null) {
            return;
        }
        disposeJarLoader(loaderKey); // 更新前先关闭已有对象.
        jarLoader = new DynamicJarClassLoader(loaderPathsMapping.get(loaderKey));
        classLoaderCache.put(loaderKey, jarLoader); // 更新类加载对象.
    }
}
```



## 5 `spring-config.xml`

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:mvc="http://www.springframework.org/schema/mvc"
       xsi:schemaLocation="http://www.springframework.org/schema/mvc http://www.springframework.org/schema/mvc/spring-mvc.xsd
	http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd
	http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context.xsd">

    <context:component-scan base-package="classLoader"/>

</beans>
```



## 6 测试自定义加载器

```java
package classLoader;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class LoadeTest {

    public static void main(String[] args) throws Exception {
        String[] path = new String[]{"D:\\tmp\\flink\\flinkRelationJar"};
        testJarLoaderMgr(path);
    }

    /**
     * 通过classLoader管理类实现
     */
    public static void testJarLoaderMgr(String[] paths) throws Exception {

        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        DynamicJarClassLoaderFactory mySingletonBean1 = context.getBean(DynamicJarClassLoaderFactory.class);

        DynamicJarClassLoader jarLoader = mySingletonBean1.createJarLoader("mgr1", paths);

        DynamicJarClassLoaderFactory mySingletonBean2 = context.getBean(DynamicJarClassLoaderFactory.class);
        mySingletonBean2.createJarLoader("mgr1", paths);

//        DynamicJarClassLoader jarLoader2 = mySingletonBean2.getJarLoader("mgr1");
//
//        Class<?> dinkyClassLoaderContextHolder = jarLoader2.loadClass("com.dlink.context.DinkyClassLoaderContextHolder");
//        // 获取一个接收N个参数的方法对象(获取的方法需要同时指定入参类型).
//        Method setParentMethod = dinkyClassLoaderContextHolder.getMethod("setParent", ClassLoader.class);
//        setParentMethod.invoke(dinkyClassLoaderContextHolder, jarLoader2);
//
//        Class<?> loadedClass = jarLoader2.loadClass("com.flinkbi.test.TestFlinkAync");
//        Object instance = loadedClass.newInstance();
//        Method flinkJobSubmit = loadedClass.getMethod("flinkJobSubmit");
//        flinkJobSubmit.invoke(instance);
    }

    /**
     * 仅JarClassLoader测试
     */
    public static void testOnlyJarLoader(String[] path) throws Exception {
        DynamicJarClassLoader jarLoader = new DynamicJarClassLoader(path);
        try {
            Class<?> dinkyClassLoaderContextHolder = jarLoader.loadClass("com.dlink.context.DinkyClassLoaderContextHolder");
            // 获取的方法需要同时指定入参类型.
            Method setParentMethod = dinkyClassLoaderContextHolder.getMethod("setParent", ClassLoader.class);
            setParentMethod.invoke(dinkyClassLoaderContextHolder, jarLoader);

            Class<?> loadedClass = jarLoader.loadClass("com.flinkbi.test.TestFlinkAync");
            Object instance = loadedClass.newInstance();
            Method flinkJobSubmit = loadedClass.getMethod("flinkJobSubmit");
            flinkJobSubmit.invoke(instance);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }
}

```





# end 