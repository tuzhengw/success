
import db from "svr-api/db"; //数据库相关API
import http from "svr-api/http";
import sys from "svr-api/sys";

function main() {

}

/**
 * 测试多线程输出
 */
function text_thread(): void {
    let threadPool = sys.getThreadPool({ // 获取一个线程池
        name: "inputI", // 线程池中唯一的标识, 若name已经存在，则会获取已存在的线程池
        corePoolSize: 4,
        maximumPoolSize: 10
    });
    // let task: Array<Task> = [];
    // 提交到线程池 执行的代码中输出的日志，批处理中不会显示，可通过系统日志查看
    for (let i = 0; i < 10; i++) {
        threadPool.submit({
            method: "inputI",
            params: [i],
            path: "/sysdata/data/batch/制证中心电子证照pdf数据落地应用服务器（多线程-tuzw.action"
        })
    }
}

export function inputI(thrend): void {
    for (let i = 0; i < 5; i++) {
        console.debug(`当前线程为：${thrend}，循环：${i}`)
    }
}