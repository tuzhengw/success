/**
 * =====================================================
 * 作者：dingy tuzw
 * 审核人员：liuyz
 * 创建日期: 2022-10-17
 * 脚本入口: CustomJs
 * 功能描述：互联互通脚本入口
 * =====================================================
 */
import { IMetaFileCustomJS, InterActionEvent, IVPage } from "metadata/metadata-script-api";
import {
	rc, showErrorMessage, ctx, showWaiting, showSuccessMessage, showWarningMessage, browser, showConfirmDialog, Component, message, compareStr, isEmpty
} from "sys/sys";
import { DEFAULT_READY_PROMISE } from "dw/dwdps";
import { modifyClass, getAppPath } from './commons/commons.js';
import { showUploadDialog } from "commons/basic";
import { SZUploadEvent, Upload } from "commons/upload";
import { getDwTableDataManager } from "dw/dwapi";
import { MonacoEditor } from "commons/monaco/monacoeditor";
import { AnaActionTriggerResultType } from "ana/anabrowser";


/**
 * 互联互通-注册页面
 */
export class HTLD_RegisterWord {

	public CustomActions: any;
	constructor() {
		this.CustomActions = {
			button_uploadXmlFile: this.button_uploadXmlFile.bind(this)
		}
	}

	/**
	 * 2022-10-17 丁煜
	 * https://jira.succez.com/browse/CSTM-20823
	 * 在spg中实现xml上传功能，并且在后端脚本中获取到上传的文件
	 */
	public button_uploadXmlFile(event: InterActionEvent): Promise<any> {
		let page = event.page;
		return new Promise((resolve, reject) => { // 自定义promise, 通过resolve来判断是否执行结束
			return showUploadDialog({
				fileTypes: ["xml"],
				multiple: false,
				maxSize: 1024 * 1024,
				onchange: (sez: SZUploadEvent) => {
					resolve(this.getUploadFile(page, sez));
				},
				oncancel: (e: SZUploadEvent) => { // 选择文件时点了取消按钮，不应该执行导入数据节点
					page.setParameter("taskId", "");
					resolve(false);
				}
			});
		});
	}

	/**
	 * 请求获取上传文件
	 * @param page
	 * @param sez 
	 */
	private getUploadFile(page: IVPage, sez: SZUploadEvent): Promise<boolean> {
		return rc<{ result: boolean, message: string, wordId?: string, taskId?: string }>({
			url: "/HLHT/app/shareWord.app/API/xmlPaser.action?method=getUploadFile",
			data: {
				fileId: sez.value![0].id
			}
		}).then(data => {
			let caption = '导入文件校验';
			if (!data.result) {
				caption = '导入文件校验出错';
			}
			if (data.wordId && data.taskId) {
				page.setParameter("taskId", data.taskId);
				return true;
			} else {
				showConfirmDialog({
					caption: caption,
					message: data.message
				});
				return false;
			}
		});
	}
}