/**
 * ================================================
 * 作者：liuyz
 * 创建日期：2022-01-04
 * 脚本用途：
 * ================================================
 */
import { InterActionEvent, IVComponent } from "metadata/metadata-script-api";
import { AnaNodeData } from "ana/datamgr";
import { SpgList } from "app/superpage/components/tables";
import { TableCellBuilder, TableEditor } from "commons/table";
import { stopSmoothScroll, wait, showWaiting, isEmpty, SZEvent, showWarningMessage, Drag } from "sys/sys";

/**
 * 列表支持自动滚动效果
 * 
 * <pre>
 *  private setSignListAutoScroll(spgList: SpgList): void {
        let customListAutoScroll: Custom_ListAutoScroll = spgList.customListAutoScroll;
        if (isEmpty(customListAutoScroll)) { // 将其自动滚动对象赋在对应列表上, 避免重复创建对象
            let customListAutoScroll = new Custom_ListAutoScroll({
                spgList: spgList,
                scrollType: "smoothScroll",
                scrollSpeed: 2,
                loopScroll: true
            });
            spgList.customListAutoScroll = customListAutoScroll;
            customListAutoScroll.setAutoScroll();
        } else {
            customListAutoScroll.setAutoScroll();
        }
    }
 * </pre>
 * @description 初始化时，将其自动滚动对象赋到SpgList对象上, 避免多次调用，重复创建多个对象
 */
export class Custom_ListAutoScroll {

    /** SPG列表对象 */
    private spgList: SpgList;
    /** 列表编辑对象 */
    private tableEditor: TableEditor;
    /** 滚动类型, eg: smoothScroll: 平滑滚动, scrollByPage: 逐页 */
    private scrollType: string;
    /** 滚动速率(秒) */
    private scrollSpeed: number;
    /** 是否循环滚动, 默认: true */
    private loopScroll: boolean;
    /** 列表自动滚动状态, 默认：true */
    private autoScrollStatus: boolean;

    /** 判断当前是否在进行平滑滚动，页面销毁或隐藏时需要停止滚动 */
    protected smoothScrolling: boolean = false;
    /** 记录自动滚动的定时器 */
    protected scrollTimer: number;

    constructor(args: {
        spgList: SpgList,
        scrollType: string,
        scrollSpeed: number,
        loopScroll?: boolean
    }) {
        this.spgList = args.spgList;
        this.scrollType = args.scrollType;
        this.scrollSpeed = args.scrollSpeed;
        this.tableEditor = this.spgList?.getTableEditor();
        this.loopScroll = !!args.loopScroll ? args.loopScroll : true;
        this.autoScrollStatus = true;
    }

    /**
     * 给列表对象添加自动滚动效果
     */
    public setAutoScroll(): void {
        if (isEmpty(this.spgList) || isEmpty(this.tableEditor)) {
            showWarningMessage(`列表对象不存在`);
            return;
        }
        if (!this.autoScrollStatus || this.smoothScrolling) { // 判断是否停止自动滚动 或 正在滚动
            return;
        }
        let refreshPromise = this.spgList.refreshPromise; // 等列表数据渲染完成后, 才可获取到滚动条高度
        showWaiting(refreshPromise).then(() => {
            this.adjustListMouseEvent();
            this.adjustMacScrollBarMouseEvent();

            this.renderSmoothScroll(); // 开始自动滚动
        });
    }

    /**
     * 给鼠标移入、移出列表事件，调整列表自动滚动
     * @description 等待列表对象渲染完成后, 再对DOM添加监听事件
     */
    private adjustListMouseEvent(): void {
        this.tableEditor.onmouseout = (event: SZEvent, tableEditor: TableEditor) => { // 鼠标移出触发事件
            this.openAutoScroll();
        }
        // 鼠标移入、移出单元格触发事件
        this.tableEditor.onhovercell = (event: SZEvent, tableEditor: TableEditor, outCell: TableCellBuilder, enterCell: TableCellBuilder) => {
            this.closeAutoScroll();
        };
    }

    /**
     * 重写纵向滚动条对象，鼠标移入、移出事件，调整列表自动滚动
     * @description 等待滚动条对象渲染完成
     * @description 鼠标的滚动条对象构造时，就将其内部已有的鼠标点击、松开事件绑定，重写——对应监听事件后，需要重新绑定
     * @description 滚动条绑定的鼠标点击事件在构造时——设置监听事件: 自动判断是否开始拖动，重新赋予前，需要先将此监听删除，
     * 然后，重新设置监听事件——重写后的doMouseDown方法
     */
    private adjustMacScrollBarMouseEvent(): void {
        /** 纵向滚动条对象 */
        let scrollBarVDragger: Drag = this.tableEditor.macScrollBar.scrollBarVDragger
        let oldDoMouseDown = scrollBarVDragger.doMouseDown.bind(scrollBarVDragger);
        let newDoMouseDown = (event: MouseEvent) => { // 鼠标点击滚动条对象，关闭自动滚动
            this.closeAutoScroll();
            oldDoMouseDown(event);
        }
        let oldDoMouseUp = scrollBarVDragger.doMouseUp.bind(scrollBarVDragger);
        let newDoMouseUp = (event: MouseEvent) => { // 鼠标松开滚动条，开启自动滚动
            oldDoMouseUp(event);
            this.openAutoScroll();
        }
        /**
         * 移除构造时绑定的鼠标自动拖动监听事件，若不移除，则会触发两次doMouseDown()方法。
         * 拖动属于一直处于点击状态。
         */
        this.tableEditor.macScrollBar.scrollBarVDragger.dom.removeEventListener("mousedown", scrollBarVDragger.onmousedown_handler);
        this.tableEditor.macScrollBar.scrollBarVDragger.onmousedown_handler = newDoMouseDown;
        if (scrollBarVDragger.listenMousedown) { // 是否设置：自动判断是否开始拖动
            this.tableEditor.macScrollBar.scrollBarVDragger.dom.addEventListener("mousedown", scrollBarVDragger.onmousedown_handler);
        }
        this.tableEditor.macScrollBar.scrollBarVDragger.onmouseup_handler = newDoMouseUp;
    }

    /**
     * 处理表格的自动滚动,切换到查看界面滚动开始，回到编辑界面滚动停止；
     * 在表格的行高发生变化或者切换模式的时候都应该调用此方法。
     * 
     * @description 滚动条是列表[数据]渲染完成后计算的
     * @description 滚动位置会自动获取当前列表滚动条所在位置，开始自动滚动
     */
    public renderSmoothScroll(): void {
        this.scrollTimer && clearInterval(this.scrollTimer);
        this.scrollTimer = null;

        let tableEditor: TableEditor = this.tableEditor;
        if (this.smoothScrolling) { // 先关闭正在滚动的 
            this.closeAutoScroll();
        }
        let tableBuilder = tableEditor.getBuilder();
        if (isEmpty(this.scrollSpeed)) {
            this.scrollSpeed = 10
        }
        let domScrollContainer = tableEditor.domScrollContainer;
        let leftH: number = 0;
        let defaultAnimaTime = 100;
        let dataRowIndex: number = 1;
        let row = tableBuilder.getRow(dataRowIndex);
        if (!row) {
            return;
        }
        /** 逐条的高度 */
        let height: number = row.getHeight();
        switch (this.scrollType) {
            case "smoothScroll": // 平滑滚动
                let scrollFunc = (): void => {
                    let domScrollH: number = domScrollContainer.scrollHeight
                        - Math.ceil(domScrollContainer.clientHeight + domScrollContainer.scrollTop);
                    if (domScrollH == 0 || domScrollH < 0) {
                        if (this.loopScroll) { // 滚动到底部时，若设置循环滚动, 则需要从上向下继续滚动
                            wait(50).then(() => {
                                domScrollContainer.scrollTop = 0;
                                tableEditor.requestRender({ onlyScroll: true }).then(() => {
                                    scrollFunc();
                                });
                            });
                        }
                        return;
                    }
                    defaultAnimaTime = this.scrollSpeed * 1000 * Math.ceil(domScrollH / height);
                    this.smoothScrolling = true;
                    tableEditor.smoothScroll({ scrollTop: domScrollH, animationTime: defaultAnimaTime }).then(() => {
                        if (!this.smoothScrolling) {
                            return;
                        }
                        if (!this.autoScrollStatus || !this.tableEditor
                            || !this.tableEditor.domScrollContainer) { // 列表被销毁, 则停止滚动
                            return;
                        }
                        /**
                         * 	  调整了电脑屏幕的缩放比例之后domScrollContainer.scrollTop得到的是一串小数点，
                         * 导致滚动到底部之后但是leftH算的结果为零点几，会出现无法循环滚动。
                         */
                        leftH = domScrollContainer.scrollHeight
                            - Math.ceil(domScrollContainer.clientHeight + domScrollContainer.scrollTop);
                        if ((leftH == 0 || leftH < 0) && this.loopScroll) {
                            wait(50).then(() => { // 从上向下继续滚动
                                domScrollContainer.scrollTop = 0;
                                tableEditor.requestRender({ onlyScroll: true }).then(() => {
                                    /**
                                     * 	当有多个线程都走到这里的时候，对scrollTimerFunc的递归调用会导致除最后一个以外的timer不会被clear，
                                     * 所以在函数开头总是clear
                                     */
                                    scrollFunc();
                                });
                            });
                        }
                    });
                }
                scrollFunc();
                return;
            case "scrollByPage": // 逐页
                defaultAnimaTime = 300;
                // 找出当前列表显示的可见区域。
                let editorInfo = tableEditor.getEditorInfo();
                let totalVisibleRows = editorInfo.mainDisplayEndRow - editorInfo.mainDisplayStartRow;
                height = Math.floor(height * totalVisibleRows);
                break;
        }
        let scrollTimerFunc = (): void => {
            clearInterval(this.scrollTimer);
            this.scrollTimer = null;
            this.scrollTimer = setInterval(() => {
                /**
                 *   调整了电脑屏幕的缩放比例之后domScrollContainer.scrollTop得到的是一串小数点，
                 * 导致滚动到底部之后但是leftH算的结果为零点几，会出现无法循环滚动。
                 */
                leftH = domScrollContainer.scrollHeight - Math.ceil(domScrollContainer.clientHeight + domScrollContainer.scrollTop);
                if (leftH > 0) {
                    tableEditor.smoothScroll({ scrollTop: height, animationTime: defaultAnimaTime });
                } else { // 滚动到底部了，若循环滚动，则默认等待一段时间从头开始；否则停止
                    if (!this.loopScroll) {
                        clearInterval(this.scrollTimer);
                        this.scrollTimer = null;
                    } else {
                        clearInterval(this.scrollTimer);
                        this.scrollTimer = null;
                        wait(50).then(() => { // 从上向下继续滚动
                            domScrollContainer.scrollTop = 0;
                            tableEditor.requestRender({ onlyScroll: true }).then(() => {
                                /**
                                 *    当有多个线程都走到这里的时候，对scrollTimerFunc的递归调用会导致除最后一个以外的timer不会被clear，
                                 * 所以在函数开头总是clear
                                 */
                                scrollTimerFunc();
                            });
                        });
                    }
                }
            }, this.scrollSpeed * 1000);
        }
        scrollTimerFunc();
    }

    /**
     * 设置列表自动滚动状态, 并根据设置的状态调整列表自动滚动
     * @param status 状态
     * 
     * @description 列表自动停止滚动需要主动调用stopSmoothScroll()方法
     * 
     * <pre>
     * 		let customListAutoScroll: Custom_ListAutoScroll = spgList.customListAutoScroll;
            customListAutoScroll.setAutoScrollStatus(false);
     * </pre>
     */
    public setAutoScrollStatus(status: boolean): void {
        if (isEmpty(status)) {
            return;
        }
        this.autoScrollStatus = status;
        if (status) {
            this.openAutoScroll();
        } else {
            this.closeAutoScroll();
        }
    }

    /**
     * 关闭列表自动滚动
     */
    private closeAutoScroll(): void {
        if (this.smoothScrolling) {
            this.smoothScrolling = false;
            stopSmoothScroll(this.tableEditor.domScrollContainer);
        }
    }

    /** 
     * 启用列表自动滚动
     */
    private openAutoScroll(): void {
        if (this.autoScrollStatus) { // 判断列表是否开启自动滚动
            if (this.scrollTimer) {
                clearInterval(this.scrollTimer);
                this.scrollTimer = null;
            }
            this.smoothScrolling = true;
            this.renderSmoothScroll();
        }
    }
}

/**
 * spg列表自动滚动类
 */
export class SPG_ListAutoScroll {
    public CustomActions: any;
    constructor() {
        this.CustomActions = {
            button_stop_listAutoScroll: this.button_stop_listAutoScroll.bind(this)
        }
    }

    public onRender(event: InterActionEvent): void {
        let comp = event.component as IVComponent;
        let compId: string = comp.getId();
        if (["list1"].includes(compId)) {
            let anaNodeData = comp.getComponent(compId) as AnaNodeData;
            let spgList: SpgList = anaNodeData.component;
            this.setSignListAutoScroll(spgList);
        }
    }

    /**
     * 给指定列表添加自动滚动效果
     * @param spgList spg列表对象
     */
    private setSignListAutoScroll(spgList: SpgList): void {
        let customListAutoScroll: Custom_ListAutoScroll = spgList.customListAutoScroll;
        if (isEmpty(customListAutoScroll)) { // 将其自动滚动对象赋在对应列表上, 避免重复创建对象
            let customListAutoScroll = new Custom_ListAutoScroll({
                spgList: spgList,
                scrollType: "smoothScroll",
                scrollSpeed: 2,
                loopScroll: true
            });
            spgList.customListAutoScroll = customListAutoScroll;
            customListAutoScroll.setAutoScroll();
        } else {
            customListAutoScroll.setAutoScroll();
        }
    }

    /**
     * 停止指定列表自动滚动
     * @param listId 列表ID
     * @param 
     */
    public button_stop_listAutoScroll(event: InterActionEvent): void {
        let params = event.params;
        let listId: string = params?.listId;
        if (isEmpty(listId)) {
            showWarningMessage(`未给定暂定自动滚动列表ID`);
            return;
        }
        let comp = event.component as IVComponent;
        let anaNodeData = comp.getComponent(listId) as AnaNodeData;
        let spgList: SpgList = anaNodeData.component;
        let customListAutoScroll: Custom_ListAutoScroll = spgList.customListAutoScroll;
        customListAutoScroll.setAutoScrollStatus(false);
    }
}