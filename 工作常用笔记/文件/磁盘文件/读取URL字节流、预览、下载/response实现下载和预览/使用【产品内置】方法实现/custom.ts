/**
 * 4.0 前端脚本注释规范
 * 作者：XXX
 * 审核人员：
 * 创建日期：
 * 脚本入口：CustomJS
 * 功能描述：
 * API参考文档：
 */
import { IMetaFileCustomJS, InterActionEvent, IAppCustomJS, IDwCustomJS, FAppInterActionEvent, IFApp, IDataset, IFAppForm } from "metadata/metadata-script-api";
import { showDrillPage, getMetaRepository, MetaFileViewer, getCurrentUser, getSysSettings } from 'metadata/metadata';
import { ctx, SZEvent, Component, throwInfo, throwError, showConfirmDialog, showFormDialog, showErrorMessage, showMenu, isEmpty, deepAssign, uuid, message, showSuccessMessage, showWarningMessage, showProgressDialog, rc, DataChangeType, UrlInfo, downloadFile, waitRender, showWaiting, assign, IDataProvider, formatDateFriendly, parseDate, formatDate, tryWait, rc_get } from 'sys/sys';
import { showGallery } from "commons/basic";
import { Gallery, ShowGalleryArgs, ViewerType } from "commons/gallery";

export const CustomJS: { [type: string]: IMetaFileCustomJS } = {
    spg: {
        CustomActions: {
			/**
             * 在前端页面展示存储到：服务器上的文件，并可以实现：下载和预览
			 * @param filePath 存在服务器上的文件【绝对路径】
			 * @param fileType 文件类型，eg：images、pdf
             */
			down_local_file1: (event: InterActionEvent) => {  // 支持多张图片左右翻动展示和下载
				let filePath: string = event.params.filePath;
				if (!filePath) {
					showWarningMessage("当前没有可以展示的图片信息");
					return;
				}
				let displayFilePath: string[] = filePath.split(",");
				let fileType: string = event.params.fileType;
				let requestPath: string = `/ZHJG/app/home.app/custom.action?method=showFile&&fileAbsolutePath=`;
				let dispalyImages: GalleryFileInfo[] = [];
				for (let i = 0; i < displayFilePath.length; i++) {
					if (!displayFilePath[i]) {
						continue;
					}
					/**
					 * 20220221 tuzw
					 * 场景：URL携带的参数若包含：+ 号，后端接受到时，会丢失，变成了空格
					 * 解决办法：
					 * （1）前端：将其"+"替换成：%2B，orgcode.replace(/\+/g, '%2B')
					 * （2）后端：将"%2B"替换成: +，orgcode.replaceAll("%2B","+");
					 */
					if (displayFilePath[i].indexOf("+") != -1) {
						displayFilePath[i] = displayFilePath[i].replace(/\+/g, '%2B');
					}
					let temp: GalleryFileInfo = {
						// path 与 url路径一致
						path: `${requestPath}${displayFilePath[i]}`, // 展示文件路径（"/"开头）
						url: `${requestPath}${displayFilePath[i]}`, // 资源下载的url，如果存在，那么会显示下载按钮，否则不显示，以"/"开头
						viewerType: getShowGalleryFileType(fileType.toLocaleLowerCase())
					}
					dispalyImages.push(temp);
				}
				showGallery({
					items: dispalyImages
				});
			},
            /**
             * 在前端页面展示存储到：服务器上的文件，并可以实现：下载和预览
			 * filePath 与 fileType 一定要是同一类型， 否则不显示
             */
            down_local_file2: (event: InterActionEvent) => {  // 仅支持一张图片展示和下载
				// 文件的路径，注意区分：相对 | 绝对路径
                let filePath: string = event.params.filePath;
				// eg：image、video、audio、pdf
                let fileType: string = event.params.fileType;
                let requestPath: string = `/ZHJG/app/应用1.app/test.action?method=showFile&&fileAbsolutePath=${filePath}`;
                /**
				 * url多个参数用：& 连接
				 
                 * showGallery({
                    items: [{
                        path: "/ZHJG/app/应用1.app/test.action?method=showFile&&fileAbsolutePath=2022-01-04/internetDeal/saba_83ce37384ea54a4880365ff12a6e2b73/filePath_19386512200306939snapshot.png",
                        // 资源下载的url，如果存在，那么会显示下载按钮，否则不显示
                        url: "/ZHJG/app/应用1.app/test.action?method=showFile&&fileAbsolutePath=2022-01-04/internetDeal/saba_83ce37384ea54a4880365ff12a6e2b73/filePath_19386512200306939snapshot.png",
                        viewerType: ViewerType.Image
                    }]
                });
                 */
                showGallery({
                    items: [{
						
						
                        // 展示文件路径（"/"开头）
                        path: requestPath,
                        // 资源下载的url，如果存在，那么会显示下载按钮，否则不显示，以"/"开头
                        url: requestPath,
                        viewerType: getShowGalleryFileType(fileType.toLocaleLowerCase())
                    }]
                });
				
				
				/**
				（1）多个【左右翻动】
				showGallery({
                    items: [{
                        // 展示文件路径（"/"开头）
                        path: `/ZHJG/app/应用1.app/test.action?method=showFile&&filePath=/zhjg/workdir/wljy_fjcc/2022-01-20/0-220218-wx-95864-1/翻糖蝴蝶结小熊（暗粉色）~动物奶油生日蛋糕.jpg`,
                        // 资源下载的url，如果存在，那么会显示下载按钮，否则不显示，以"/"开头
                        url: `/ZHJG/app/应用1.app/test.action?method=showFile&&filePath=/zhjg/workdir/wljy_fjcc/2022-01-20/0-220218-wx-95864-1/翻糖蝴蝶结小熊（暗粉色）~动物奶油生日蛋糕.jpg`,
                        viewerType: getShowGalleryFileType(fileType)
                    }, {
                        // 展示文件路径（"/"开头）
                        path: `/ZHJG/app/应用1.app/test.action?method=showFile&&filePath=/zhjg/workdir/wljy_fjcc/2022-01-20/0-220218-wx-95848-1/名流 避孕套精品组合系列 安全套超薄大颗粒螺纹加倍润滑成人情趣性用品 至薄002(30只装).jpg`,
                        // 资源下载的url，如果存在，那么会显示下载按钮，否则不显示，以"/"开头
                        url: `/ZHJG/app/应用1.app/test.action?method=showFile&&filePath=/zhjg/workdir/wljy_fjcc/2022-01-20/0-220218-wx-95848-1/名流 避孕套精品组合系列 安全套超薄大颗粒螺纹加倍润滑成人情趣性用品 至薄002(30只装).jpg`,
                        viewerType: getShowGalleryFileType(fileType)
                    }]
				*/
            }
        }
    }
}


/**
 * 获取【showGallery】组件文件类型
 * @param fileType 文件类型，eg："png"
 * @return ViewerType 某常量
 */
function getShowGalleryFileType(fileType): ViewerType {
    switch (fileType) {
        case "image": return ViewerType.Image;
            break;
        case "video": return ViewerType.Video;
            break;
        case "audio": return ViewerType.Audio;
            break;
        case "pdf": return ViewerType.Pdf;
            break;
    }
}