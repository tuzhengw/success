import dw from "svr-api/dw";
import db from "svr-api/db";
import metadata, { mkdirs } from "svr-api/metadata";
import utils from 'svr-api/utils';
import security from 'svr-api/security';


/**最开始的目录，用于规定需要导入几个层级 */
const STARTPATH = '/sdi/data/tables/domains';
const RES_CAT = '/sysdata/data/tables/sdi/data-check/dims/DIM_GOVERN_RES_CAT.tbl'
const CHECK_TASK_RES_TABLE = "SZSYS_4_DG_CHECK_TASK_TABLES";
const CHECK_TASK_TABLE = "SZSYS_4_DG_CHECK_TASKS";
/**
 * 自定义规则表单，规则表达式关联模型
 */
export function getTableId(request: HttpServletRequest, response: HttpServletResponse, params: any) {
    //return "zwL4SZrVkrENQnIo8MnKSE";
    return dw.queryDwTableData({
        sources: [{
            id: 'model1',
            path: '/sysdata/data/tables/dg/DG_RULES_LIB.tbl'
        }],
        fields: [{
            name: 'MAIN_TABLE_ID',
            exp: 'model1.MAIN_TABLE_ID',
        }],
        filter: [{
            clauses: [{
                leftExp: 'model1.RULES_LIB_ID',
                operator: ClauseOperatorType.Equal,
                rightValue: params.resId,
            }]
        }],
        options: {
            needCodeDesc: true
        },
        distinct: true,
        select: true
    });
}

/**
 * 获取标准规则数据
 */
export function getStdResource(request: HttpServletRequest, response: HttpServletResponse, params: any) {
    let ids = params.ids;
    let ids_ = ids.split(",");
    for (let i = 0; i < ids_.length; i++) {
        ids_[i] = "'" + ids_[i] + "'";
    }
    ids = ids_.join(",");
    return dw.queryDwTableData({
        sources: [{
            id: 'model1',
            path: '/sysdata/data/tables/dg/DG_RULES_LIB.tbl'
        }, {
            id: 'model2',
            path: '/sysdata/data/tables/dg/DG_STD_TABLES_INST.tbl'
        }, {
            id: 'model3',
            path: '/sysdata/data/tables/dg/DG_STD_TABLES.tbl'
        }, {
            id: 'model4',
            path: '/sysdata/data/tables/dg/DG_STD_LIB.tbl'
        }],
        fields: [{
            name: 'RULES_LIB_ID',
            exp: 'model1.RULES_LIB_ID',
        }, {
            name: 'STD_LIB_ID',
            exp: 'model3.STD_LIB_ID'
        }, {
            name: 'MAIN_TABLE_ID',
            exp: 'model1.MAIN_TABLE_ID'
        }],
        joinConditions: [{
            leftTable: 'model1',
            rightTable: 'model2',
            joinType: SQLJoinType.LeftJoin,
            clauses: [{
                leftExp: 'model1.MAIN_TABLE_ID',
                operator: ClauseOperatorType.Equal,
                rightExp: 'model2.INST_TABLE_ID',
            }]
        }, {
            leftTable: 'model2',
            rightTable: 'model3',
            joinType: SQLJoinType.LeftJoin,
            clauses: [{
                leftExp: 'model2.STD_TABLE_ID',
                operator: ClauseOperatorType.Equal,
                rightExp: 'model3.STD_TABLE_ID',
            }]
        }, {
            leftTable: 'model3',
            rightTable: 'model4',
            joinType: SQLJoinType.LeftJoin,
            clauses: [{
                leftExp: 'model3.STD_LIB_ID',
                operator: ClauseOperatorType.Equal,
                rightExp: 'model4.STD_LIB_ID',
            }]
        }],
        filter: [{
            clauses: [{
                leftExp: 'model1.RULES_LIB_ID',
                operator: ClauseOperatorType.In,
                rightValue: ids,
            }]
        }, {
            clauses: [{
                leftExp: 'model3.STD_LIB_ID',
                operator: ClauseOperatorType.NotIs,
                rightValue: null,
            }]
        }, {
            clauses: [{
                leftExp: "model4.IS_DELETE",
                operator: ClauseOperatorType.Is,
                rightValue: null
            }, {
                leftExp: "model4.IS_DELETE",
                operator: ClauseOperatorType.Equal,
                rightValue: "0"
            }],
            matchAll: false
        }],
        options: {
            needCodeDesc: true,
        },
        sort: [{
            exp: 'model1.RULES_LIB_ID'
        }],
        distinct: true,
        select: true
    });
}

/**
 * 查询数据元的SQL
 */
const SQL_SJY = `SELECT t0.RULES_LIB_ID,t0.MAIN_TABLE_ID,t1.countId FROM SZSYS_4_DG_RULES_LIB t0 LEFT JOIN
    (SELECT FILE_ID, count(DATA_ELEMENT_ID) as countId FROM SZSYS_4_META_FIELDS GROUP BY  FILE_ID) t1 ON(t0.MAIN_TABLE_ID = t1.FILE_ID)
    where t1.countId >0 and t0.RULES_LIB_ID in (`;

/**
 * 获取关联数据元
 */
export function getSJYResource(request: HttpServletRequest, response: HttpServletResponse, params: any) {
    let ids = params.ids;
    let ids_ = ids.split(",");
    for (let i = 0; i < ids_.length; i++) {
        ids_[i] = "'" + ids_[i] + "'";
    }
    ids = ids_.join(",");
    let dataSource = db.getDefaultDataSource();
    let sql = SQL_SJY + ids + ")"
    return dataSource.executeQueryRows(sql);
}

/**
 * 主数据所在目录
 */
const DIR_MAIN_DATA = "/sdi/data/tables/main-data%";

/**
 * 查询主数据的sql
 */
const SQL_ZSJ = `SELECT t0.RULES_LIB_ID,t0.MAIN_TABLE_ID,t1.countId FROM SZSYS_4_DG_RULES_LIB t0
LEFT JOIN
    (SELECT t0.FILE_ID, count(t0.DIM_TABLE_ID) as countId FROM SZSYS_4_META_FIELDS t0
	LEFT JOIN SZSYS_4_META_FILES t1 ON(t0.DIM_TABLE_ID = t1.ID)
	WHERE(t1.PARENT_DIR LIKE '${DIR_MAIN_DATA}') AND(t0.DIM_TABLE_ID IS NOT NULL)
	GROUP BY  t0.FILE_ID) t1
ON(t0.MAIN_TABLE_ID = t1.FILE_ID) where t1.countId >0 and t0.RULES_LIB_ID in (`;

/**
 * 获取关联主数据
 */
export function getZSJResource(request: HttpServletRequest, response: HttpServletResponse, params: any) {
    let ids = params.ids;
    let ids_ = ids.split(",");
    for (let i = 0; i < ids_.length; i++) {
        ids_[i] = "'" + ids_[i] + "'";
    }
    ids = ids_.join(",");
    let dataSource = db.getDefaultDataSource();
    let sql = SQL_ZSJ + ids + ")"
    return dataSource.executeQueryRows(sql);
}

/**
 * 检查规则库的资源别名是否有重复
 */
export function checkRuleLibName(request: HttpServletRequest, response: HttpServletResponse, params: any) {
    if (params && params.name) {
        let result = dw.queryDwTableData({
            sources: [{
                id: 'model1',
                path: '/sysdata/data/tables/dg/DG_RULES_LIB.tbl'
            }],
            fields: [{
                name: 'DX_ORG_ID',
                exp: 'model1.RULES_LIB_ID',
            }, {
                name: 'DX_ORG_DESC',
                exp: 'model1.RULES_LIB_NAME',
            }],
            filter: [{
                clauses: [{
                    leftExp: 'model1.BM',
                    operator: ClauseOperatorType.Equal,
                    rightValue: params.name,
                }]
            }],
            options: {
                needCodeDesc: true
            },
            distinct: true,
            select: true
        });
        if (result.length == 0) {
            return {
                result: true
            };
        } else {
            return {
                result: false,
                id: result[0][0],
                name: result[0][1]
            }
        }
    }
}

/**
 * 查询主键为空的数据条数
 * @param request
 * @param response
 * @param params
 */
export function queryPkNullInfo(request: HttpServletRequest, response: HttpServletResponse, params: any) {
    let resid = params.resid;
    let modelFile = metadata.getFile(resid);
    let path = modelFile.path;
    let pks = params.pk;
    let pks_ = pks.split(",");
    let filter = [];
    let field = [];
    for (let i = 0; i < pks_.length; i++) {
        let pk = pks_[i];
        pks_[i] = "'" + pks_[i] + "'";
        filter.push({
            leftExp: `model1.${pk}`,
            operator: ClauseOperatorType.is,
            rightValue: null,
        });
        field.push({
            name: pk,
            exp: `model1.${pk}`,
        })
    }
    let result = dw.queryDwTableData({
        sources: [{
            id: 'model1',
            path: path
        }],
        fields: [{
            name: "pk0",
            exp: `model1.${pks_[0]}`,
        }],
        filter: [{
            clauses: filter,
            matchAll: false
        }],
        select: true
    });
    return {
        length: result.length
    }
}

/**
 * 查询主键重复的数据条数
 * @param request
 * @param response
 * @param params
 */
export function queryPkMultipleInfo(request: HttpServletRequest, response: HttpServletResponse, params: any) {
    let resid = params.resid;
    let modelFile = metadata.getFile(resid);
    let path = modelFile.path;
    let pks = params.pk;
    let pks_ = pks.split(",");
    if (pks_.length == 0) {
        return null;
    }
    let field = [];
    for (let i = 0; i < pks_.length; i++) {
        let pk = pks_[i];
        field.push({
            name: pk,
            exp: `model1.${pk}`,
            group: true
        })
    }
    field.push({
        name: "count",
        exp: `count(model1.${pks_[0]})`,
    })
    let result = dw.queryDwTableData({
        sources: [{
            id: 'model1',
            path: path
        }],
        fields: field,
        having: [{
            exp: "COUNT(*)>1"
        }],
        // having: ["COUNT(*)>1"],
        select: true
    });
    let totalCount = 0;
    if (result.length > 0) {
        for (let i = 0; i < result.length; i++) {
            let num = result[i][field.length - 1];
            totalCount += parseInt(num);
        }
        return {
            totalCount: totalCount
        }
    }
    return {
        totalCount: totalCount,
        data: result
    };
}

/**
 * 规则库表
 */
const RULES_LIB = "SZSYS_4_DG_RULES_LIB";

/**
 * 规则关联关系表
 */
const RULES_LIB_REFS = "SZSYS_4_DG_RULES_LIB_REFS";

/**
 * 规则关联关系表地址
 */
const PATH_RULE_REF = "/sysdata/data/tables/dg/DG_RULES_LIB_REFS.tbl";

/**
 * 规则库关联关系表
 * 资源编码，规则库编码，对应数据表ID，资源别名,创建时间，创建者
 */
const RULE_LIB_REF_FIELD = ["DG_REF_ID", "RULES_LIB_ID", "DW_TABLE_ID", "DG_REF_ALIAS", "CREATE_TIME", 'CREATOR']

/**
 * 更新规则引用关联关系表
 * 1.查询别名在关联关系表是否已经存在，存在不用管，不存在的话，通过别名查询出对应的规则库编码和资源id，然后新增关联关系
 * 2.如果是修改了表达式的上报，表达式中删除的别名暂时不做删除处理，因为别名可能在其他规则引用了，只在删除资源的时候删除资源对应的关联关系
 */
export function updateRuleRef(request: HttpServletRequest, response: HttpServletResponse, params: any) {
    print("【updateRuleRef】 更新规则关联关系表---------start");
    let names = params.names;
    let names_arr_origin = names.split(",");
    return _updateRuleRef(names_arr_origin);
}

/**
 * 将更新关联关系抽出一个方法，共其他地方调用
 * @param names_arr_origin
 */
function _updateRuleRef(names_arr_origin) {
    print("\t 需要更新的所有关联关系别名：" + names_arr_origin.join(","));
    if (names_arr_origin.length == 0) {
        print("\t 别名为空不需要更新");
        return;
    }
    let names_arr = [];
    for (let i = 0; i < names_arr_origin.length; i++) {
        names_arr[i] = "'" + names_arr_origin[i] + "'";
    }
    let names_str = names_arr.join(",");
    let ds = db.getDefaultDataSource();
    let tableData_lib = ds.openTableData(RULES_LIB);
    let datas_lib = tableData_lib.selectRows(["BM", "RULES_LIB_ID", "MAIN_TABLE_ID"], `BM in (${names_str})`);
    let resultinfos = {
        result: true
    };//返回给客户端的信息
    if (datas_lib.length < names_arr_origin.length) {
        print("【updateRuleRef】 有资源别名对应的检测资源不存在,停止更新");
        resultinfos['result'] = false;
        let name_exist_lib = [];
        for (let j = 0; j < datas_lib.length; j++) {
            let data = datas_lib[j];
            name_exist_lib.push(data[0]);
        }

        let name_error = names_arr_origin.filter(function (val) { return name_exist_lib.indexOf(val) === -1 });
        resultinfos["name_error"] = name_error;
        print("\t 不存在的资源，name_error：" + name_error.join(","));
        print("【updateRuleRef】 更新规则关联关系表---------end");
        return resultinfos;
    }
    let tableData_lib_ref = ds.openTableData(RULES_LIB_REFS);
    let datas_ref = tableData_lib_ref.selectRows(["DG_REF_ALIAS"], `DG_REF_ALIAS in (${names_str})`);
    let name_existRef = [];
    for (let k = 0; k < datas_ref.length; k++) {
        name_existRef.push(datas_ref[k][0]);
    }
    print("\t 关联关系表中已有的别名，name_existRef：" + name_existRef.join(','))
    let name_add = names_arr_origin.filter(function (val) { return name_existRef.indexOf(val) === -1 });
    if (name_add.length == 0) {
        print("\t 所有关联关系都已经存在，不需要新增");
        print("【updateRuleRef】 更新规则关联关系表---------end");
        return resultinfos;
    } else {
        print("\t 需要新增的资源，" + name_add.join(","));
    }
    let addRows_ = [];
    let userId = security.getCurrentUser().userInfo.userId;
    let time = new Date();
    for (let j = 0; j < datas_lib.length; j++) {
        let data = datas_lib[j];
        let uuid = utils.uuid();
        if (name_add.indexOf(data[0]) > -1) {
            print("\t 新增关联关系，规则编码：" + data[1]);
            addRows_.push([uuid, data[1], data[2], data[0], time, userId])
        }
    }

    dw.updateDwTableData(PATH_RULE_REF, {
        addRows: [
            {
                fieldNames: RULE_LIB_REF_FIELD,
                rows: addRows_
            }
        ]
    });
    print("【updateRuleRef】 更新规则关联关系表---------end");
    return resultinfos;
}

/*-----------------------------新建目录、批量导入数据资产、数据标准、数据交换资源------------------------------------------start */

/**
 * 数据检测资源目录表名
 */
const NAME_RES_CAT = "DIM_GOVERN_RES_CAT"

/**
 * 数据检测资源目录地址
 */
const PATH_RES_CAT = '/sysdata/data/tables/sdi/data-check/dims/DIM_GOVERN_RES_CAT.tbl'

/**
 * 数据检测资源目录要更新的字段
 */
const FIELD_RES_CAT = ["PID", "ID", "CAPTION", "SZ_LEVEL", "SZ_LEAF", "SZ_PID0", "SZ_PID1", "SZ_PID2", "SZ_PID3", "SZ_PID4",];

/**
 * 规则库（检测资源）表地址
 */
const PATH_RULE_LIB = "/sysdata/data/tables/dg/DG_RULES_LIB.tbl";

/**
 * 检测资源新增目录
 * 考虑用左侧树更新接口插入数据，不用脚本插入
 */
export function addNewMl(request: HttpServletRequest, response: HttpServletResponse, params: any) {
    let datas_str = params.datas;
    print(`【addNewMl】 检测资源新增目录---------start`);
    print(`【addNewMl】,datas:${datas_str}`);
    let datas = JSON.parse(datas_str);
    let ds = db.getDefaultDataSource();
    // selectId、
    let id = datas.id;
    let tableData_res_cat = ds.openTableData(NAME_RES_CAT);
    let datas_res_cat = tableData_res_cat.selectFirstRow(["id"], { "ID": id });
    if (datas_res_cat && datas_res_cat.length > 0) {
        return {
            result: false,
            errorInfo: `id为${id}的目录已存在`
        }
    }
    let fields = [].concat(FIELD_RES_CAT);
    //考虑用左侧树更新接口插入数据，不用脚本插入
    return;
    dw.updateDwTableData(PATH_RES_CAT, {
        addRows: [
            {
                fieldNames: fields,
                rows: []
            }
        ]
    });
    return "{}";
}

/**
 * 批量导入检测资源（规则库）需要更新的字段
 */
const FIELDS_ADD_RULELIB = ['RULES_LIB_ID', 'RULES_LIB_NAME', 'MAIN_TABLE_ID', 'CREATE_TIME', 'CREATOR', 'ML', 'BM', 'ZYDZ'];


/**
 * 是否输出批量导入数据资产日志信息
 */
const DEBUG_DETAILS_ASSET = true;

/**
 * 检测资源批量导入数据资产，根据modelsInfo里面的数据层级进行添加
 * 对modelsInfo得出的数组进行遍历，其它层级的是否叶子节点都为false,并且修改现在引入的那个pidInfo为false,到最后一层目录也就是倒数第二级，设置为true
 */
export function batchImportAsset(request: HttpServletRequest, response: HttpServletResponse, params: any) {
    DEBUG_DETAILS_ASSET && print(`【batchImportAsset】 检测资源批量导入数据资产---------start`);
    // return;
    let datas_str = params.datas;
    print(`【batchImportAsset】,datas:${datas_str}`);
    let datas = JSON.parse(datas_str);
    let catInfos = datas.catInfos;
    let modelInfos = datas.modelInfos;
    let pidInfo = datas.pidInfo;
    if (pidInfo.id) {
        DEBUG_DETAILS_ASSET && print(`【batchImportAsset】,批量导入到:${pidInfo.id}-${pidInfo.caption} 目录下,该根节点层级为：${pidInfo.level}`);
    } else {
        DEBUG_DETAILS_ASSET && print(`【batchImportAsset】,批量导入到根节点`);
    }
    //1.更新目录信息
    DEBUG_DETAILS_ASSET && print(`【batchImportAsset】,开始更新目录信息，总共导入的目录层级:${catInfos.length}`);
    /**目前的问题：
     * 1.导入的时候只会显示两个层级
     * 2.根目录导入会有问题
     * 20210401  wanglf
     */
    //1、如果有选中的节点，改变选中节点的Isleaf的状态
    if (pidInfo.id) {
        DEBUG_DETAILS_ASSET && print(`\t【batchImportAsset】,跟节点导入了目录，需要改变根节点叶子状态`);
        dw.updateDwTableData(RES_CAT, {
            modifyRows: [
                {
                    fieldNames: ["SZ_LEAF"],
                    rows: [{
                        keys: [pidInfo.id],
                        row: [0]
                    }]
                }
            ]
        });
        DEBUG_DETAILS_ASSET && print(`\t【batchImportAsset】,根节点叶子状态修改完成`);
    } else {
        DEBUG_DETAILS_ASSET && print(`\t【batchImportAsset】,不需要改变根节点叶子状态`);
    }
    //2.获取最大层级，以及有空目录的时候也直接将空目录放入modelsInfo中
    let maxIndex = 0;
    let rows_cat_add = [];
    let field_new = [];//获取到应该插入的sz_pid数组
    let modelInfos_new = modelInfos;
    for (let i = 0; i < catInfos.length; i++) {
        print(`catInfos--------------------------------------------------------------------${catInfos[i]}`);
        modelInfos_new = modelInfos_new.concat(catInfos[i]);
    }
    print(`modelinfos_new的值为------------------------------------------------------------------------${modelInfos_new}`);
    print(`modelinfos的值为------------------------------------------------------------------------${modelInfos}`);
    for (let k = 0; k < modelInfos_new.length; k++) {
        let index_new = modelInfos_new[k].path.split('/').length - (STARTPATH.split('/').length);
        if (index_new > maxIndex)
            maxIndex = index_new;
    }
    print(`初始最大层级-----------------------------------------------------${maxIndex}`);
    //3.获取表中已存在的目录以及父目录，方便引入的有已经存在的目录时，直接在该目录下面新添加表数据
    let content = [];
    let szPrim = [];
    let existMLRes = dw.queryDwTableData({
        sources: [{
            id: 'model1',
            path: RES_CAT
        }],
        fields: [{
            name: 'MLDM',
            exp: 'model1.MLDM'
        }, {
            name: 'PID',
            exp: 'model1.PID'
        }, {
            name: 'ID',
            exp: 'model1.ID'
        },]
    });
    let existML = {};	//存储某资源所在的父节点以及当前节点的代码（uuid）
    for (let m = 0; m < existMLRes.length; m++) {
        let pid = existMLRes[m][1] ? existMLRes[m][1] : '';//没有选中节点时，记录父亲为''
        if (existML[existMLRes[m][0]] == null || existML[existMLRes[m][0]] == undefined) {
            existML[existMLRes[m][0]] = [[pid], [existMLRes[m][2]]];
        } else if (existML[existMLRes[m][0]][1].indexOf(existMLRes[m][2]) == -1) {
            existML[existMLRes[m][0]][0].push(pid);
            existML[existMLRes[m][0]][1].push(existMLRes[m][2]);
        }
    }
    //4.获取当前节点需要添加的父节点的层级信息
    /** 在一个节点下面添加数据，根据该id查出该节点前面的结构*/
    if (pidInfo.id) {
        let field = [];
        for (let n = 0; n <= pidInfo.level; n++) {
            field.push({
                name: `SZ_PID${n}`,
                exp: `model1.SZ_PID${n}`
            })
            field_new.push(`SZ_PID${n}`);
            print(`field_new-----------------------------------------------------${field_new}`)
        }
        print(`field-----------------------------------------------------${field.length}`)
        //获取当前点击节点已经存在的结构数据
        let resSZ = dw.queryDwTableData({
            sources: [{
                id: 'model1',
                path: RES_CAT
            }],
            fields: field,
            filter: [{
                exp: `model1.ID='${pidInfo.id}'`
            }]
        });
        for (let n = 0; n < resSZ[0].length; n++) {
            content.push(resSZ[0][n]);
        }
    }
    print(`更新的字段----------------------------------------${maxIndex}`);
    let userId = security.getCurrentUser().userInfo.userId;
    let time = new Date();
    /**得到需要拼接的sz_pid字段 */
    maxIndex = (pidInfo.id || modelInfos.length <= 0) ? maxIndex : maxIndex - 1;
    print(`maxIndex-----------------------------${maxIndex}`);
    let level = pidInfo.id ? (pidInfo.level + 1) : 0;
    print(`层级为----------${level}`);
    for (let m = 0; m < maxIndex; m++) {
        field_new.push(`SZ_PID${m + (level)}`);
    }
    print(`要插入的字段为----------${field_new}`);
    let source = [];
    let parUid = '';
    let file_cat = {};
    //5.遍历节点数据，存储对应的目录数据，写入res_cat表
    for (let i = 0; i < modelInfos_new.length; i++) {//---------------------------------------------------
        let modelInfo = modelInfos_new[i];
        print(`此modelInfo的值为-------${modelInfo}`);
        parUid = pidInfo.id ? pidInfo.id : '';	//用于记录每个目录上一级目录的uuid，没有点击的节点则为空
        let fileInfo = metadata.getFile(modelInfos_new[i].path);	//获取到每一个表的目录，用于分割并插入目录数据
        let splitArr = modelInfos_new[i].path.slice(STARTPATH.length + 1).split('/');
        print('长度为：---------------------------------------------------------------' + splitArr.length);
        let strPath = STARTPATH;	//拼接路径
        let szContentArr = [];//真正要插入的pid结构
        print(`content的值为--------------------------------------------------------${content}`);
        szContentArr = content;
        /**根据父路径创建对应的目录 */
        for (let j = 0; j < splitArr.length; j++) {
            if (j != splitArr.length - 1 || !splitArr[j].endsWith('.tbl')) {
                let uuid = utils.uuid();
                print(`此uuid为------------------------------------${uuid}`);
                strPath += '/' + splitArr[j];
                print(`文件为---------------------------------------${strPath}`);
                let mlInfo = metadata.getFile(strPath);	//当前目录的信息
                let mlId = mlInfo.id;
                let mlDesc = mlInfo.desc;
                print(`要写入的数据为：---------------------------------------------${szContentArr},目录的Id为${mlId}`);
                if (existML[mlId] == null || existML[mlId] == undefined) {//如果之前不存在对应的文件，则初始化对应的map结构为空
                    existML[mlId] = [[], []];
                }
                print(`父目录的id为-------${parUid}`);
                //如果当前文件没有导入过当前的父文件夹下，导入并在对应的map结构中记录
                if (existML[mlId][0].indexOf(parUid) == -1) {
                    print(`${parUid}未导入过`);
                    existML[mlId][0].push(parUid);
                    existML[mlId][1].push(uuid);
                    let level = pidInfo.id ? (pidInfo.level + j + 1) : j;
                    let row = (j == splitArr.length - 2) ? [uuid, parUid, mlId, mlDesc, level, 1, time, userId] : [uuid, parUid, mlId, mlDesc, level, 0, time, userId];
                    szContentArr = szContentArr.concat([uuid]);
                    row = row.concat(szContentArr);
                    print(`szContent的值为-------------------------------------------------${szContentArr}`);
                    print(`maxIndex的值为-------------------------------------------------${maxIndex}`);
                    print(`需要记录${maxIndex - (szContentArr.length - content.length)}个null值`);
                    for (let p = 0; p < maxIndex - (szContentArr.length - content.length); p++) {
                        print(`记录第${p}个null`);
                        row.push(null);
                    }
                    rows_cat_add.push(row);
                    parUid = uuid;
                } else {
                    let index = existML[mlId][0].indexOf(parUid);
                    parUid = existML[mlId][1][index];
                    szContentArr = szContentArr.concat([parUid]);
                    print(`${parUid}目录下存在${mlId}的数据------------------------------------------`);
                }
                if (j == splitArr.length - 2) {
                    //存储对应的表路径与目录的对应关系
                    file_cat[modelInfo.path] = parUid;
                }
            }
        }
    }
    let field_add = ['ID', 'PID', 'MLDM', 'CAPTION', 'SZ_LEVEL', 'SZ_LEAF', 'CJSJ', 'CJR'].concat(field_new);
    print(`字段为---------------------------------------${field_add}`);
    if (rows_cat_add.length > 0) {
        dw.updateDwTableData(RES_CAT, {
            addRows: [
                {
                    fieldNames: field_add,
                    rows: rows_cat_add
                }
            ]
        });
        dw.refreshState(RES_CAT);
    } else {
        DEBUG_DETAILS_ASSET && print(`\t 没有需要新增的资源目录`);
    }
    modelInfos.length > 0 ? modelInfos = getResidsNewMaxName(modelInfos) : modelInfos;//获取要导入表的别名
    let ignoreResInfo = [];//忽略的资源
    let addResInfo = [];//实际新增的资源
    for (let k = 0; k < modelInfos.length; k++) {
        let modelInfo = modelInfos[k];
        let resid = modelInfo.resid;
        //没有主键的资源要忽略掉
        if (!hasPrimaryKey(modelInfo.resid)) {
            ignoreResInfo.push(modelInfo);
            DEBUG_DETAILS_ASSET && print(`\t\t 【warn】资源对应的模型表没有主键,忽略该条资源，path:${modelInfo.path}`);
            continue;
        }
        let name = modelInfo['newName'];
        let pid = modelInfo.pinfo ? modelInfo.pinfo.uuid : null;
        let row = [modelInfo.uuid, modelInfo.desc, resid, time, userId, file_cat[modelInfos[k].path], name, modelInfo.path];
        source.push(row);
        addResInfo.push(modelInfo);
    }
    DEBUG_DETAILS_ASSET && print(`\t 更新交换资源目录数据——完毕`);
    if (modelInfos.length > 0) {
        DEBUG_DETAILS_ASSET && print(`\t 新增的资源条数:${source.length}`);
        DEBUG_DETAILS_ASSET && print(`\t 没有主键需要忽略的资源条数：${ignoreResInfo.length}`);
        if (source.length > 0) {
            dw.updateDwTableData(PATH_RULE_LIB, {
                addRows: [
                    {
                        fieldNames: FIELDS_ADD_RULELIB,
                        rows: source
                    }
                ]
            });
        } else {
            DEBUG_DETAILS_ASSET && print(`\t 所有资源都忽略了，没有需要新增的资源`);
        }
    } else {
        DEBUG_DETAILS_ASSET && print(`\t 没有需要新增的资源`);
    }
    dw.refreshState(PATH_RULE_LIB);
    /** */
    DEBUG_DETAILS_ASSET && print(`【batchImportAsset】所有新增资源更新完毕！`);
    DEBUG_DETAILS_ASSET && print(`【batchImportAsset】 检测资源批量导入数据资产---------end`);
    return {
        ignoreResInfo: ignoreResInfo,
        addResInfo: addResInfo
    };
}

/**
 * 判断一个模型表是否有主键
 */
function hasPrimaryKey(resid) {
    let model = dw.getDwTable(resid);
    if (!model) {
        return false;
    }
    let pk = model.properties && model.properties.primaryKeys;
    if (pk && pk.length > 0) {
        return true;
    }
    return false;
}

const DEBUGGER_GETMAXNAME = true;

/**
 * 获取一批模型表的最新别名
 */
function getResidsNewMaxName(models) {
    DEBUGGER_GETMAXNAME && print('\t 【getResidsNewMaxName】收集资源最新别名---------start')
    let resids_arr = [];
    for (let i = 0; i < models.length; i++) {
        let resid = models[i].resid;
        resids_arr.push(`'${resid}'`);
    }
    let resids_str = resids_arr.join(',');
    let ds = db.getDefaultDataSource();
    let rows = ds.executeQueryRows(`
        SELECT max(BM),
		MAIN_TABLE_ID
        FROM SZSYS_4_DG_RULES_LIB
        WHERE MAIN_TABLE_ID IN (${resids_str})
		AND BM LIKE '%_%'
        GROUP BY  MAIN_TABLE_ID
    `);
    let residMaxName = {};
    for (let j = 0; j < rows.length; j++) {
        let row = rows[j];
        let maxName = row[0];
        residMaxName[row[1]] = maxName;
    }
    DEBUGGER_GETMAXNAME && print(`\t 【getResidsNewMaxName】有最大别名的资源信息：${JSON.stringify(residMaxName)}`);

    for (let k = 0; k < models.length; k++) {
        let model = models[k];
        let maxName = residMaxName[model.resid];
        let newName;
        DEBUGGER_GETMAXNAME && print(`\t\t ${k}-要分析别名的资源：${model.name}`);
        if (!maxName) {
            let name_old = model.name;
            let index2 = name_old.lastIndexOf('.');
            let name = name_old.substr(0, index2);
            newName = `${name}_1`;
            DEBUGGER_GETMAXNAME && print(`\t\t 没有最大别名，直接取模型表的名字：${newName}`);
        } else {
            let index1 = maxName.lastIndexOf('_');
            let maxIndex = parseInt(maxName.substring(index1 + 1));
            let name_prex = maxName.substr(0, index1);
            if (maxIndex.toString() == 'NaN') {
                DEBUGGER_GETMAXNAME && print(`\t\t 【error】别名的后缀不是数字！`);
                newName = `${name_prex}_${utils.randomString()}_1`;
            } else {
                newName = `${name_prex}_${maxIndex + 1}`;
            }
            DEBUGGER_GETMAXNAME && print(`\t\t 有最大别名，最大别名后缀加一获得新别名：${newName}`);
        }
        model["newName"] = newName;
    }
    DEBUGGER_GETMAXNAME && print('\t 【getResidsNewMaxName】收集资源最新别名---------end')
    return models;
}

/**
 * 是否输出批量导入数据标准关联表日志信息
 */
const DEBUG_DETAILS_STD = true;

/**
 * 检测资源目录的最大层级，从0 开始
 */
const MAXLEVEL_CHECKCAT = 10;
/**
 * 批量导入数据标准
 * 目录：标准加有实例表的标准表的目录
 * 资源: 关联了实例表的资源
 */
export function batchImportStandard_new(request: HttpServletRequest, response: HttpServletResponse, params: any) {
    let datas_str = params.datas;
    let isAddFolder = params.isAddFolder;
    let pidInfo_str = params.pidInfo;
    DEBUG_DETAILS_STD && print(`【batchImportStandard_new】 检测资源批量导入数据标准关联表---------start`);
    DEBUG_DETAILS_STD && print(`【batchImportStandard_new】,datas:${datas_str}`);
    DEBUG_DETAILS_STD && print(`【batchImportStandard_new】,pidInfo:${pidInfo_str}`);
    let datas = JSON.parse(datas_str);
    let pidInfo = JSON.parse(pidInfo_str);
    /*
    let pidInfo = {
        id: '111',
        level: 1,
        level_field: ['SZ_PID0', 'SZ_PID1'],
        level_data: ['1111', '2222']
    }*/
    /**
     * stdlibInfos =
     * [{
     *  id:'111',
     *  caption:'标准一'
     * },{
     *  id:'222',
     *  caption:'标准二'
     * }]
     */
    let stdlibInfos = datas;//标准库id
    if (!stdlibInfos || stdlibInfos.length == 0) {
        DEBUG_DETAILS_STD && print(`【batchImportStandard_new】,没有传入标准信息，无须导入！`);
        return;
    }
    let userId = security.getCurrentUser().userInfo.userId;
    let time = new Date();
    //1.更新目录
    DEBUG_DETAILS_STD && print(`\t 1.开始更新目录，需要更新的标准个数：${stdlibInfos.length}---start`);
    let stdlibs_ = [];
    for (let i = 0; i < stdlibInfos.length; i++) {
        stdlibs_.push(`'${stdlibInfos[i]['id']}'`);
    }
    DEBUG_DETAILS_STD && print(`\t\t 1.1 收集要更新目录的所有字段.`);
    let result_lib_res = getStandardInstTable(stdlibs_);
    if (result_lib_res.length == 0) {
        DEBUG_DETAILS_STD && print(`\t 该标准没有关联的实例表，无须导入`);
        DEBUG_DETAILS_STD && print(`【batchImportStandard_new】 检测资源批量导入数据标准关联表---------end`);
        return {
            ignoreResInfo: [],
            addResInfo: []
        };
    }
    let libCat = [];//实例表的标准表所在标准目录的id
    for (let i = 0; i < result_lib_res.length; i++) {
        let data = result_lib_res[i];
        let cat = data[5];
        libCat.push(`'${cat}'`);
    }
    DEBUG_DETAILS_STD && print(`\t\t ---所有关联了实例表的标准表信息：${libCat.join(",")}`);
    //1.获得实际要更新的所有层级字段
    let ds = db.getDefaultDataSource();
    let conn = ds.getConnection();
    let stdCatId_uuid = {}; //所欲要更新的标准目录与uuid的对应——废弃，考虑到之后要能自动更新检测资源目录结构，不要采用自己生产的uuid，保留原有的id
    try {
        conn.setAutoCommit(false);
        let tableData = ds.openTableData('SZSYS_4_DG_STD_TABLE_FOLDS', null, conn);
        let filter = `STD_TABLE_FOLD_ID in (${libCat.join(',')})`;
        // let maxLevel_result = tableData.executeQuery(`select max(SZ_LEVEL) as maxlevel from SZSYS_4_DG_STD_TABLE_FOLDS where ${filter}`);
        // let maxLevel_std_all = maxLevel_result[0]['MAXLEVEL'];

        /**
         * 每个标准的最大层级
         */
        let map_std_level = {};
        let maxLevel_std_all = 0;
        let result_levels = tableData.executeQuery(`SELECT max(SZ_LEVEL) AS maxlevel,SZ_PID0 FROM SZSYS_4_DG_STD_TABLE_FOLDS WHERE ${filter} group by SZ_PID0`);
        for (let i = 0; i < result_levels.length; i++) {
            let result_level = result_levels[i];
            print(`result_level:${result_level}`)
            map_std_level[result_level['SZ_PID0']] = result_level['MAXLEVEL'];
            if (maxLevel_std_all < result_level['MAXLEVEL']) {
                maxLevel_std_all = result_level['MAXLEVEL'];
            }
        }
        DEBUG_DETAILS_STD && print(`\t\t ---所有要导入的标准对应的最大层级：${JSON.stringify(map_std_level)}`);
        DEBUG_DETAILS_STD && print(`\t\t ---有实例表的标准目录的最大层级：${maxLevel_std_all}`);
        let pid_level = -1;
        if (pidInfo && pidInfo['id']) {
            pid_level = pidInfo['level'];//pid  sz_pid0,sz_pid1
        }
        let field_level = [];
        for (let p = 1; p < maxLevel_std_all + 2; p++) {
            let level = pid_level + p;
            field_level.push(`SZ_PID${level}`);
        }
        if (pid_level + maxLevel_std_all > MAXLEVEL_CHECKCAT - 1) {
            DEBUG_DETAILS_STD && print(`\t 【error】导入失败！要导入的目录超出了检测资源目录的最大层级，要导入层级为：${pid_level + maxLevel_std_all}`);
            DEBUG_DETAILS_STD && print(`【batchImportStandard_new】 检测资源批量导入数据标准关联表---------end`);
            return {
                state: 'error',
                level: pid_level + maxLevel_std_all
            };
        }

        let field_add_cat_old = ['ID', 'PID', 'CAPTION', 'SZ_LEVEL', 'SZ_LEAF', 'MLLX', 'STD_LIB_ID', 'CJSJ', 'CJR'];
        let field_add_cat = [].concat(field_add_cat_old);
        if (pidInfo && pidInfo['id']) {
            field_add_cat.pushAll(pidInfo['level_field']);
            field_add_cat.pushAll(field_level);
            DEBUG_DETAILS_STD && print(`\t\t---有选中节点，新增到选中节点下，更新字段：${field_add_cat.join(',')}`);
        } else {
            field_add_cat.pushAll(field_level);
            DEBUG_DETAILS_STD && print(`\t\t---没有选中节点，直接新增到第0级，更新字段：${field_add_cat.join(',')}`);
        }
        DEBUG_DETAILS_STD && print(`\t\t 1.要更新的目录的字段 field_add_cat: ${field_add_cat}`);

        print(`\t\t 1.2.收集所有要更新的目录的数据`);
        /**
         * 2.获得所有需要更新的目录的id，包括有实例资源的标准目录及其上级
         * 先查出所有有梳理表的目录的所有信息，收集这些目录的所有SZ_PID层级，获得一个要插入目录的及其上级的id数组
         */
        let result_cat_resid = tableData.select("*", filter, null, 'SZ_LEVEL asc');
        DEBUG_DETAILS_STD && print(`\t\t ---有实例资源的标准目录的个数：${result_cat_resid.length}`);
        let ids_arr = [];
        /**
         * 所有要更新的标准目录与检测资源uuid的对应
         */

        for (let n = 0; n < result_cat_resid.length; n++) {
            let data = result_cat_resid[n];
            print(`\t\t---${n},result_cat_resid：${data}`);
            let level = data['SZ_LEVEL'];
            for (let a = 0; a < level + 1; a++) {
                let id_c = data[`SZ_PID${a}`];
                if (ids_arr.indexOf(`'${id_c}'`) == -1) {
                    ids_arr.push(`'${id_c}'`);
                    let uuid = utils.uuid();
                    stdCatId_uuid[id_c] = uuid;
                } else {
                    print(`\t\t---${n},id_c:${id_c} 已经收集过`)
                }
            }
        }
        print(`\t\t---当前目录id为${ids_arr.join(",")}`);
        let result_cats = tableData.select("*", `STD_TABLE_FOLD_ID in (${ids_arr.join(',')})`, null, 'SZ_LEVEL asc');
        print(`\t\t---所有要更新的目录的个数（包括上级）：${result_cats.length}`)
        let add_rows_cat = [];
        let catId_res = {};
        let now = new Date();
        // let userId = security.getCurrentUser().userInfo.userId;
        for (let j = 0; j < result_cats.length; j++) {
            let data = result_cats[j];
            let level = data['SZ_LEVEL'];
            let row = [];
            // let uuid = utils.uuid();
            // row.push(uuid);
            // row.push(data['STD_TABLE_FOLD_ID']);
            let uuid_cat = data['STD_TABLE_FOLD_ID'];
            row.push(uuid_cat);
            // catId_res[data['STD_TABLE_FOLD_ID']] = stdCatId_uuid[data['STD_TABLE_FOLD_ID']];
            if (level == 0 && pidInfo && pidInfo['id']) {
                row.push(pidInfo['id']);
            } else {
                // row.push(data['PARENT_STD_TABLE_FOLD_ID']);
                row.push(data['PARENT_STD_TABLE_FOLD_ID']);
            }
            row.push(data['STD_TABLE_FOLD_NAME']);
            let level_ = level;
            if (pid_level >= 0) {
                level_ = level + pid_level + 1;
            }
            row.push(level_);
            let isleaf = 'false';
            if (parseInt(level) == parseInt(map_std_level[data['SZ_PID0']])) {
                isleaf = 'true';
            } else {
                print(`--------------level:${level},map_std_level:${map_std_level[data['SZ_PID0']]}`);
            }
            row.push(isleaf);
            /**
             * 第0级是标准
             */
            if (level == 0) {
                row.push('STD');
                row.push(data['STD_TABLE_FOLD_ID']);
                row.push(now);
                row.push(userId);
            } else {
                row.push(null);
                row.push(null);
                row.push(now);
                row.push(userId);
            }
            if (pidInfo && pidInfo['id']) {
                row.pushAll(pidInfo['level_data']);
            }
            for (let k = 0; k < level + 1; k++) {
                // row.push(data[`SZ_PID${k}`]);
                row.push(data[`SZ_PID${k}`]);
            }
            for (let m = 0; m < maxLevel_std_all - level; m++) {
                row.push(null);
            }
            add_rows_cat.push(row);
            print(`${j}:row:${row}`);
        }

        DEBUG_DETAILS_STD && print(`\t\t 1.3 要更新的目录数据：${add_rows_cat}`);
        /**20210511
         * wanglingf
         * 添加参数判断引入的时候是否要添加目录
         * */
        if (isAddFolder == 'false') {
            for (let i = 0; i < result_lib_res.length; i++) {
                result_lib_res[i][5] = pidInfo['id'];
            }
            let result = dw.queryDwTableData({
                sources: [{
                    id: 'model1',
                    path: PATH_RES_CAT
                }],
                fields: [{
                    name: 'STD_LIB_ID',
                    exp: 'model1.STD_LIB_ID'
                }],
                filter: [{
                    exp: `model1.ID='${pidInfo.id}'`
                }]
            });
            if (result.length > 0) {//如果要导入的目录下之前导入过标准，则删除原来标准生成的检测任务，防止下次导入标准时出错
                let std_lib_id = result[0][0];
                dw.updateDwTableData("/sysdata/data/tables/dg/DG_CHECK_TASKS.tbl", {
                    deleteRows: [['STD_' + result[0][0]]]
                });
            }
            dw.updateDwTableData(PATH_RES_CAT, {
                modifyRows: [
                    {
                        fieldNames: ['MLLX', 'STD_LIB_ID'],
                        rows: [{
                            keys: [pidInfo['id']],
                            row: ['STD', datas[0]['id']]
                        }]
                    }
                ]
            });
        } else {
            dw.updateDwTableData(PATH_RES_CAT, {
                addRows: [
                    {
                        fieldNames: field_add_cat,
                        rows: add_rows_cat
                    }
                ]
            });
        }
        DEBUG_DETAILS_STD && print(`\t 1.目录更新完毕---end`);
        //2.更新选中节点叶子状态
        if (pidInfo && !pidInfo.leaf && result_cat_resid.length > 0) {
            DEBUG_DETAILS_STD && print(`\t 2.父节点导入了目录，需要改变父节点叶子状态---start`);
            dw.updateDwTableData(PATH_RES_CAT, {
                modifyRows: [
                    {
                        fieldNames: ["SZ_LEAF"],
                        rows: [{
                            keys: [pidInfo.id],
                            row: [0]
                        }]
                    }
                ]
            });
            DEBUG_DETAILS_STD && print(`\t 2.父节点叶子状态修改完成--end`);
        } else {
            DEBUG_DETAILS_STD && print(`\t 2.没有父节点或没有要导入的资源，不需要改变父节点叶子状态`);
        }

        dw.refreshState(PATH_RES_CAT);
        conn.commit();
    }
    finally {
        conn.close();
    }

    //3 创建检测任务
    DEBUG_DETAILS_STD && print(`\t 3.创建检测任务`);
    let add_task_rows = [];
    let task_fields = ['CHECK_TASK_ID', 'CHECK_TASK_NAME', 'CREATE_TIME', 'CREATOR', 'RWLX'];
    for (let i = 0; i < stdlibInfos.length; i++) {
        let row = [];
        let id = stdlibInfos[i]['id']
        let caption = stdlibInfos[i]['caption'] + "检测任务";
        let task_id = "STD_" + id;
        row.push(task_id);
        row.push(caption);
        row.push(time);
        row.push(userId);
        row.push('STD');
        add_task_rows.push(row);
    }
    DEBUG_DETAILS_STD && print(`\t---新建检测任务个数：${add_task_rows.length}`);
    DEBUG_DETAILS_STD && print(`\t---新建检测任务信息：${add_task_rows}`);
    dw.updateDwTableData("/sysdata/data/tables/dg/DG_CHECK_TASKS.tbl", {
        addRows: [{
            fieldNames: task_fields,
            rows: add_task_rows
        }]
    })
    DEBUG_DETAILS_STD && print(`\t 检测任务新建完毕`);
    let updateCheckTableResult = updateCheckTable(result_lib_res, time, userId);
    return updateCheckTableResult;
}

const DEBUGGER_GETMAXNAME_STD = true;

/**
 * 将标准实例表写入到检测资源，关联关系，检测规则表中
 * @param result_lib_res
 * @param time
 * @param userId
 */
function updateCheckTable(result_lib_res, time, userId) {
    DEBUG_DETAILS_STD && print(`\t 4.开始更新标准对应的资源，需要更新个数：${result_lib_res.length}---start`);
    let ignoreResInfo = [];//忽略的资源
    let addResInfo = [];//实际新增的资源
    if (!result_lib_res || result_lib_res.length == 0 || result_lib_res[0].length == 0 || !result_lib_res[0][0]) {
        DEBUG_DETAILS_STD && print(`\t 4.没有需要更新的资源`);
        DEBUG_DETAILS_STD && print(`【batchImportStandard_new】 检测资源批量导入数据标准关联表---------end`);
        return {
            ignoreResInfo: ignoreResInfo,
            addResInfo: addResInfo
        };
    }
    //查询所有资源的最新别名
    DEBUG_DETAILS_STD && print(`\t\t 4.1 收集所有资源的最新别名---start`);
    let result_resid_name = getResidsNewMaxName_std(result_lib_res);
    DEBUG_DETAILS_STD && print(`\t\t 4.1 收集所有资源的最新别名---end`);
    DEBUG_DETAILS_STD && print(`\t\t 4.2 收集所有要更新资源的数据`);
    let std_id_res = [];
    let rows_add_res = [];
    let task_add_res = [];//往检测资源表中添加的数据
    let task_add_rules = [];//往检测规则表中添加的数据
    let bmList = [];//别名数组
    for (let i = 0; i < result_lib_res.length; i++) {
        let result = result_lib_res[i];
        let resid = result[1];
        let path_res = `${result[4]}/${result[2]}`;
        if (!hasPrimaryKey(resid)) {
            ignoreResInfo.push({
                resid: resid,
                name: result[2],
                path: path_res
            })
            DEBUG_DETAILS_STD && print(`\t\t 【warn】资源对应的模型表没有主键,忽略该条资源，path:${path_res}`);
            continue;
        } else {
            addResInfo.push({
                resid: resid,
                name: result[2],
                path: path_res
            })
        }

        if (std_id_res.indexOf(result[0]) == -1) {
            std_id_res.push(result[0]);
        }
        let name = result_resid_name[result[1]];
        bmList.push(name);
        let std_id = result[0];
        let std_table_id = result[6];
        let stdCatId_old = result[5];
        let stdCatId = stdCatId_old;
        let task_id = 'STD_' + std_id;
        let resuuid = utils.uuid();
        let row = [resuuid, result[3], result[1], time, userId, stdCatId, name, path_res];
        DEBUG_DETAILS_STD && print(`\t\t---第${i}条需要更新的资源数据：${row}`);
        rows_add_res.push(row);
        // let res_row = [task_id, result[1], std_id, name];
        let res_row = [task_id, result[1], resuuid, name];
        task_add_res.push(res_row);
        DEBUG_DETAILS_STD && print(`\t\t---第${i}条需要更新的检测资源的数据：${res_row}`);
        task_add_rules.push([task_id, 'STRUCT', result[1], 'STD']);
        task_add_rules.push([task_id, 'VALUE_NOT_EMPTY', result[1], 'STD']);
        task_add_rules.push([task_id, 'VALUE_RANGE', result[1], 'STD']);
        task_add_rules.push([task_id, 'ITEM_NOT_EXIST', result[1], 'MD']);
        //0304---------------------------------------START
        task_add_rules.push([task_id, 'RELATION_EXIST', result[1], 'MD']);
        insertRelationInfo(result[1], resuuid);
        //0304-------------------------------------------------------------------END
    }
    DEBUG_DETAILS_STD && print(`\t\t 4.2 所有要更新资源的数据：${rows_add_res}`);
    dw.updateDwTableData(PATH_RULE_LIB, {
        addRows: [
            {
                fieldNames: FIELDS_ADD_RULELIB,
                rows: rows_add_res
            }
        ]
    });
    DEBUG_DETAILS_STD && print(`\t 4.标准对应的资源更新完毕！---end`);

    DEBUG_DETAILS_STD && print(`\t 5.更新标准对应的检测任务的信息---start`);
    dw.updateDwTableData("/sysdata/data/tables/dg/DG_CHECK_TASK_TABLES.tbl", {
        addRows: [{
            fieldNames: ['CHECK_TASK_ID', 'DW_TABLE_ID', 'GZKDM', 'BM'],
            rows: task_add_res
        }]
    })
    dw.updateDwTableData("/sysdata/data/tables/dg/DG_CHECK_TASK_RULES.tbl", {
        addRows: [{
            fieldNames: ['CHECK_TASK_ID', 'RULE_ID', 'DW_TABLE_ID', 'RULE_FROM'],
            rows: task_add_rules
        }]
    })
    //更新关联关系
    _updateRuleRef(bmList);
    DEBUG_DETAILS_STD && print(`\t 5.更新标准对应的检测任务的信息---end`);
    DEBUG_DETAILS_STD && print(`【batchImportStandard_new】,检测资源批量导入数据标准关联表---------end`);
    return {
        ignoreResInfo: ignoreResInfo,
        addResInfo: addResInfo
    };
}

/**
 * 获取到被检测模型关联表，插入到规则库引用信息中
 * @param resid 被检测模型ID
 * @param rule_lib_id 检测模型规则库ID
 */
function insertRelationInfo(resid: string, rule_lib_id: string) {
    let result_relations = dw.queryDwTableData({
        sources: [{
            id: 'model1',
            path: '/sysdata/data/tables/dw/DW_TABLE_RELATION.tbl'
        }],
        fields: [{
            name: 'target_resid',
            exp: 'model1.TARGET_RESID'
        }],
        filter: [{
            exp: `model1.SRC_RESID='${resid}' and model1.RELATION_TYPE='Fact'`
        }]
    });
    let result_refs = dw.queryDwTableData({
        sources: [{
            id: 'model1',
            path: '/sysdata/data/tables/dg/DG_RULES_LIB_REFS.tbl'
        }],
        fields: [{
            name: 'DW_TABLE_ID',
            exp: 'model1.DW_TABLE_ID'
        }]
    });
    let table_refsArr = [];
    for (let i = 0; i < result_refs.length; i++) {
        table_refsArr.push(result_refs[i][0]);
    }
    let row_relation = [];
    for (let j = 0; j < result_relations.length; j++) {
        let uuid_relation = utils.uuid();
        let file = metadata.getFile(result_relations[j][0]);
        let fileName = (file != null) ? file.name.replace(".tbl", "") : null;
        let uid = utils.randomString();
        if (table_refsArr.indexOf(result_relations[j][0]) == -1 && fileName) {
            row_relation.push([uuid_relation, rule_lib_id, result_relations[j][0], fileName + '_' + uid, new Date()]);
        }
    };
    dw.updateDwTableData("/sysdata/data/tables/dg/DG_RULES_LIB_REFS.tbl", {
        addRows: [{
            fieldNames: ['DG_REF_ID', 'RULES_LIB_ID', 'DW_TABLE_ID', 'DG_REF_ALIAS', 'CREATE_TIME'],
            rows: row_relation
        }]
    });
}

/**
 * 收集一批模型表的resid与最新别名的键值对(标准)
 */
function getResidsNewMaxName_std(models_arr: any[][]) {
    DEBUGGER_GETMAXNAME_STD && print('\t\t 【getResidsNewMaxName】收集资源最新别名---------start');
    let result_resid_name = {};
    let resids_arr = [];
    for (let i = 0; i < models_arr.length; i++) {
        let resid = models_arr[i][1];
        resids_arr.push(`'${resid}'`);
    }
    let resids_str = resids_arr.join(',');
    let ds = db.getDefaultDataSource();
    let rows = ds.executeQueryRows(`
        SELECT max(BM),
		MAIN_TABLE_ID
        FROM SZSYS_4_DG_RULES_LIB
        WHERE MAIN_TABLE_ID IN (${resids_str})
		AND BM LIKE '%_%'
        GROUP BY  MAIN_TABLE_ID
    `);
    let residMaxName = {};
    for (let j = 0; j < rows.length; j++) {
        let row = rows[j];
        let maxName = row[0];
        residMaxName[row[1]] = maxName;
    }
    DEBUGGER_GETMAXNAME_STD && print(`\t\t 【getResidsNewMaxName】有最大别名的资源信息：${JSON.stringify(residMaxName)}`);

    for (let k = 0; k < models_arr.length; k++) {
        let model_arr = models_arr[k];
        let maxName = residMaxName[model_arr[1]];
        let newName;
        DEBUGGER_GETMAXNAME_STD && print(`\t\t\t ${k}-要分析别名的资源：${model_arr[2]}`);
        if (!maxName) {
            let name_old = model_arr[2];
            let index2 = name_old.lastIndexOf('.');
            let name = name_old.substr(0, index2);
            newName = `${name}_1`;
            DEBUGGER_GETMAXNAME_STD && print(`\t\t\t 没有最大别名，直接取模型表的名字：${newName}`);
        } else {
            let index1 = maxName.lastIndexOf('_');
            let name_old = model_arr[2];
            let index2 = name_old.lastIndexOf('.');
            let name = name_old.substr(0, index2);
            if (index1 == -1) {
                newName = `${name}_1`;
                DEBUGGER_GETMAXNAME_STD && print(`\t\t\t 最大别名不包含_,别名序号从_1开始：${newName}`);
            } else {
                let maxIndex = parseInt(maxName.substring(index1 + 1));
                let name_prex = maxName.substr(0, index1);
                DEBUGGER_GETMAXNAME_STD && print(`\t\t\t maxIndex:${maxIndex},name_prex:${name_prex}`);
                if (maxIndex.toString() == 'NaN') {
                    DEBUGGER_GETMAXNAME_STD && print(`\t\t\t 【error】别名的后缀不是数字！`);
                    newName = `${maxName}_1`;
                } else {
                    newName = `${name_prex}_${maxIndex + 1}`;
                }
            }
            DEBUGGER_GETMAXNAME_STD && print(`\t\t\t 有最大别名，最大别名后缀加一获得新别名：${newName}`);
        }
        result_resid_name[model_arr[1]] = newName;
        // model_arr.push(newName);
    }
    DEBUGGER_GETMAXNAME_STD && print('\t\t 【getResidsNewMaxName】收集资源最新别名---------end')
    return result_resid_name;
}

const DEBUG_DETAILS_EXT = true;

/**
 * 检测资源批量导入数据交换表
 */
export function batchImportExchange(request: HttpServletRequest, response: HttpServletResponse, params: any) {
    let datas_str = params.datas;
    DEBUG_DETAILS_EXT && print(`【batchImportExchange】 检测资源批量导入数据交换表---------start`);
    DEBUG_DETAILS_EXT && print(`【batchImportExchange】,datas:${datas_str}`);
    let datas = JSON.parse(datas_str);
    return;
    let catInfos = datas.catInfos;//目录信息
    let modelInfos = datas.modelInfos;//资源信息
    //1.更新目录信息
    DEBUG_DETAILS_EXT && print(`【batchImportExchange】,开始更新目录信息，总共导入的目录层级:${catInfos.length}`);
    for (let i = 0; i < catInfos.length; i++) {
        DEBUG_DETAILS_EXT && print(`\t开始导入第 ${i} 级目录`);
        let isleaf = false;
        if (i == (catInfos.length - 1)) {
            //是目录的叶子节点
            isleaf = true;
        }
        let catInfo = catInfos[i];
        //一个层级的SZ_PID个数是一样的
        let field_level = catInfo[0].fields_level['fields'];
        let field_add = ['ID', 'PID', 'CAPTION', 'SZ_LEVEL', 'SZ_LEAF'].concat(field_level);
        DEBUG_DETAILS_EXT && print(`\t第 ${i} 级目录所有字段：${field_add.join(",")}`);
        let rows = [];
        for (let j = 0; j < catInfo.length; j++) {
            let info = catInfo[j];
            //用uuid做主键，可以重复导入资源目录
            let pid = info.pinfo ? info.pinfo.uuid : null;
            let row = [info.uuid, info.pid, info.caption, info.level, isleaf].concat(info.fields_level['data']);
            rows.push(row);
        }
        DEBUG_DETAILS_EXT && print(`\t第 ${i} 级目录数据：${rows}`);
        // dw.updateDwTableData(PATH_RES_CAT, {
        //     addRows: [
        //         {
        //             fieldNames: field_add,
        //             rows: rows
        //         }
        //     ]
        // });
        DEBUG_DETAILS_EXT && print(`\t成功导入第 ${i} 级目录`);
    }
    DEBUG_DETAILS_EXT && print(`【batchImportExchange】,所有目录信息更新完毕！`);
    //2.更新资源信息
    DEBUG_DETAILS_EXT && print(`【batchImportExchange】,开始更新资源信息，总共新增的的资源条数:${modelInfos.length}`);
    let userId = security.getCurrentUser().userInfo.userId;
    let time = new Date();
    let rows_add_res = [];
    for (let k = 0; k < modelInfos.length; k++) {
        let modelInfo = modelInfos[k];
        //别名不能重复，暂用name+随机字符串命名
        let name;
        if (modelInfo.name) {
            //去掉tbl后缀
            name = modelInfo.name.split(".")[0] + "_" + utils.randomString();
        } else {
            name = utils.randomString();
        }
        let pid = modelInfo.pid;
        let path = modelInfo.path ? modelInfo.path : null;
        let row = [modelInfo.uuid, modelInfo.caption, modelInfo.resid, time, userId, pid, name, path];
        rows_add_res.push(row);
    }
    if (modelInfos.length > 0) {
        DEBUG_DETAILS_EXT && print(`\t 新增的资源数据：${rows_add_res}`);
    }
    // let field_add_res = ['RULES_LIB_ID', 'RULES_LIB_NAME', 'MAIN_TABLE_ID', 'CREATE_TIME', 'CREATOR', 'ML', 'BM', 'ZYDZ']
    // dw.updateDwTableData(PATH_RULE_LIB, {
    //     addRows: [
    //         {
    //             fieldNames: FIELDS_ADD_RULELIB,
    //             rows: rows_add_res
    //         }
    //     ]
    // });
    DEBUG_DETAILS_EXT && print(`【batchImportExchange】所有新增资源更新完毕！`);
    DEBUG_DETAILS_EXT && print(`【batchImportExchange】 检测资源批量导入数据交换表---------end`);
    return '{}';
}

/*-----------------------------新建目录、批量导入数据资产、数据标准、数据交换资源------------------------------------------end */

/**
 * 删除某个检测任务
 */
export function deleteTask(request: HttpServletRequest, response: HttpServletResponse, params: any) {
    let task_id = params.task_id;
    let ds = db.getDefaultDataSource();
    let checkTaskTable = ds.openTableData("SZSYS_4_DG_CHECK_TASKS");
    let checkTaskRuleTable = ds.openTableData("SZSYS_4_DG_CHECK_TASK_RULES");
    let checkTaskResTable = ds.openTableData(CHECK_TASK_RES_TABLE);
    checkTaskTable.del({ "CHECK_TASK_ID": task_id });
    checkTaskRuleTable.del({ "CHECK_TASK_ID": task_id });
    checkTaskResTable.del({ "CHECK_TASK_ID": task_id });
    return { success: true };
}

/**
 * 往检测任务中添加资源和规则，仅限标准检测任务
 */
export function addResToTask(request: HttpServletRequest, response: HttpServletResponse, params: any) {
    let catalog = params.catalog;
    let resid = params.resid;
    let bm = params.bm;
    let rule_lib_id = params.rule_lib_id;
    let ds = db.getDefaultDataSource();
    let catalogTable = ds.openTableData("DIM_CHECK_RES_CAT");
    let result = catalogTable.selectRows("STD_LIB_ID", { "ID": catalog });
    if (result.length == 0 || !result[0][0]) {
        return { success: true };
    }
    let task_id = result[0][0];
    let checkTaskRuleTable = ds.openTableData("SZSYS_4_DG_CHECK_TASK_RULES");
    let checkTaskResTable = ds.openTableData(CHECK_TASK_RES_TABLE);
    let std_task_id = "STD_" + task_id;
    let resultRes = checkTaskResTable.selectRows("*", { "CHECK_TASK_ID": std_task_id, "DW_TABLE_ID": resid });
    if (resultRes.length > 0) {
        return { success: false, message: "该资源已经存在，不能重复添加" }
    }
    checkTaskResTable.insert({ "CHECK_TASK_ID": std_task_id, "DW_TABLE_ID": resid, "BM": bm, "GZKDM": rule_lib_id });
    checkTaskRuleTable.insert([{ "CHECK_TASK_ID": std_task_id, "RULE_ID": "STRUCT", "DW_TABLE_ID": resid, "RULE_FROM": 'STD' },
    { "CHECK_TASK_ID": std_task_id, "RULE_ID": "VALUE_NOT_EMPTY", "DW_TABLE_ID": resid, "RULE_FROM": 'STD' },
    { "CHECK_TASK_ID": std_task_id, "RULE_ID": "VALUE_RANGE", "DW_TABLE_ID": resid, "RULE_FROM": 'STD' }]);
    return { success: true };
}

/**新建检测资源检查资源是否有主键 */
export function checkResidPrimaryKey(request: HttpServletRequest, response: HttpServletResponse, params: any) {
    let resid = params.resid;
    if (hasPrimaryKey(resid)) {
        return true;
    }
    return false;
}

/**
 * 根据标准库ID获取对应所有的实例表
 * @param standardList
 */
function getStandardInstTable(standardList) {
    let result_lib_res = dw.queryDwTableData({
        sources: [{
            id: 'model1',
            path: "/sysdata/data/tables/dg/DG_STD_TABLES_INST.tbl"//标准实例关联表
        }, {
            id: "model2",
            path: "/sysdata/data/tables/meta/META_FILES.tbl"//文件表
        }, {
            id: "model3",
            path: "/sysdata/data/tables/dg/DG_STD_TABLES.tbl"//标准内容表
        }],
        fields: [{
            name: "STD_LIB_ID",
            exp: `model3.STD_LIB_ID`
        }, {
            name: "INST_TABLE_ID",
            exp: `model1.INST_TABLE_ID`
        }, {
            name: "NAME",
            exp: `model2.NAME`
        }, {
            name: "DESC",
            exp: `model2.DESC`
        }, {
            name: "PARENT_DIR",
            exp: `model2.PARENT_DIR`
        }, {
            name: "STD_TABLE_FOLD_ID",
            exp: `model3.STD_TABLE_FOLD_ID`
        }, {
            name: "STD_TABLE_ID",
            exp: `model3.STD_TABLE_ID`
        }],
        joinConditions: [{
            leftTable: 'model1',
            rightTable: 'model2',
            joinType: "LeftJoin",
            clauses: [{
                leftExp: 'model1.INST_TABLE_ID',
                operator: ClauseOperatorType.Equal,
                rightExp: 'model2.ID'
            }]
        }, {
            leftTable: 'model1',
            rightTable: 'model3',
            joinType: "LeftJoin",
            clauses: [{
                leftExp: 'model1.STD_TABLE_ID',
                operator: ClauseOperatorType.Equal,
                rightExp: 'model3.STD_TABLE_ID'
            }]
        }],
        filter: [{
            clauses: [{
                leftExp: 'model3.STD_LIB_ID',
                operator: ClauseOperatorType.In,
                rightValue: standardList.join(",")
            }]
        }, {
            clauses: [{
                leftExp: 'model2.TYPE',
                operator: ClauseOperatorType.Equal,
                rightValue: "tbl"
            }]
        }],
        select: true
    });
    return result_lib_res;
}

function getCheckTables(standard_id) {
    let check_res = dw.queryDwTableData({
        sources: [{
            id: 'model1',
            path: "/sysdata/data/tables/dg/DG_CHECK_TASK_TABLES.tbl"
        }],
        fields: [{
            name: "DW_TABLE_ID",
            exp: "model1.DW_TABLE_ID"
        }],
        filter: [{
            clauses: [{
                leftExp: `model1.CHECK_TASK_ID`,
                operator: ClauseOperatorType.Equal,
                rightValue: "STD_" + standard_id
            }]
        }]
    });
    let check_tables = []
    for (let i = 0; i < check_res.length; i++) {
        check_tables.push(check_res[i][0]);
    }
    return check_tables;
}

/**
 * 同步标准实例表到数据标准检测任务中，同步流程如下
 * 1.根据传入的数据标准库id查询出所有的实例表
 * 2.判断这些实例表是否以及被添加到检测任务中
 * 2.1 如果某个实例表不存在于检测资源中，则将该资源新增到检测资源中。
 * 2.2 如果该检测资源不存在于实例表中，则将该检测资源删除掉。
 * 2.3 其余的资源不做任何修改
 * @param request
 * @param response
 * @param params
 */
export function syncDataStandardTable(request: HttpServletRequest, response: HttpServletResponse, params: any) {
    let standard_id = params.standard_id;
    let result_lib_res = getStandardInstTable([standard_id]);
    let check_tables = getCheckTables(standard_id);
    let needDeleteTable = [].concat(check_tables);
    let needAddTable = [];
    for (let i = 0; i < result_lib_res.length; i++) {
        let res = result_lib_res[i];
        if (check_tables.indexOf(res[1]) == -1) {
            needAddTable.push(res);
        } else {
            needDeleteTable.remove(res[1]);
        }
    }
    deleteCheckTable(needDeleteTable);
    needAddTable.length > 0 && addCheckTable(needAddTable, standard_id);
    return {
        result: true, needAddTable: needAddTable, needDeleteTable: needDeleteTable
    }
}

/**
 * 删除掉多余的检测资源
 * @param tableList
 */
function deleteCheckTable(tableList) {
    if (tableList.length > 0) {
        let ds = db.getDefaultDataSource();
        let checkTaskTable = ds.openTableData(CHECK_TASK_RES_TABLE);
        let checkRuleTable = ds.openTableData('SZSYS_4_DG_CHECK_TASK_RULES');
        let filter = `DW_TABLE_ID in (${tableList.join(",")})`
        checkTaskTable.del(filter);
        checkRuleTable.del(filter);
    }
}

/**
 * 新增检测资源，需要五张表中进行新增
 * 1.往DIM_GOVERN_RES_CAT 中添加，让其能在左侧树中展示
 * 2.往DG_RULES_LIB、DG_RULES_LIB_REFS 中新增， 让其成为检测资源
 * 3.往DG_CHECK_TASK_TABLES、DG_CHECK_TASK_RULES中新增，添加到对应的检测任务中
 * @param needAddTables 待插入的资源
 * @param standard_id 数据标准ID
 */
function addCheckTable(needAddTables, standard_id) {
    let now = new Date();
    let userId = security.getCurrentUser().userInfo.userId;
    updateTableCatalogData(needAddTables, standard_id);
    updateCheckTable(needAddTables, now, userId);
}

/**
 * 获取到需要往DIM_GOVERN_RES_CAT的数据
 * 需要从标准目录表 DG_STD_TABLE_FOLDS中取到待插入数据的目录信息，补全缺少的层级
 * 1.查询出待插入数据表所在标准的层级
 * 2.查询出当前数据标准所在检测资源的层级，得出一共有多少字段需要更新
 * 3.获取到待插入资源在标准中的所有上级节点，然后判断这些节点有没有被插入，如果有则跳过，没有则是需要插入的数据
 * @param needAddTables
 * @param standard_id
 */
function updateTableCatalogData(needAddTables, standard_id) {
    let instCatId = [];//实例表所在目录ID
    let max_level = 0;
    for (let index = 0; index < needAddTables.length; index++) {
        instCatId.push(`'${needAddTables[index][5]}'`);
    }
    let filter = `STD_TABLE_FOLD_ID in (${instCatId.join(',')})`;
    let cat_fields_result = getNeedAddCatatlogFields(max_level, filter, standard_id);
    if (!cat_fields_result.result) {
        return cat_fields_result;
    }
    max_level = cat_fields_result.max_level;
    let add_check_cat_field = cat_fields_result.fields;
    let pid_level = cat_fields_result.pid_level;
    let pid_data = cat_fields_result.pid_data;
    let add_check_cat_data = getNeedAddCatalogDatas(filter, standard_id, pid_level, pid_data, max_level);
    dw.updateDwTableData(PATH_RES_CAT, {
        addRows: [
            {
                fieldNames: add_check_cat_field,
                rows: add_check_cat_data
            }
        ]
    });
}

/**
 * 获取待插入的字段
 * @param max_level
 * @param filter
 * @param standard_id
 */
function getNeedAddCatatlogFields(max_level, filter, standard_id) {
    let ds = db.getDefaultDataSource();
    let dg_std_table_fold = ds.openTableData('SZSYS_4_DG_STD_TABLE_FOLDS');
    let max_level_result = dg_std_table_fold.executeQuery(`SELECT max(SZ_LEVEL) AS maxlevel,SZ_PID0 FROM SZSYS_4_DG_STD_TABLE_FOLDS WHERE ${filter} group by SZ_PID0`);
    for (let i = 0; i < max_level_result.length; i++) {
        if (max_level < max_level_result[i]['MAXLEVEL']) {
            max_level = max_level_result[i]['MAXLEVEL'];
        }
    }
    let dim_check_res_table = ds.openTableData('DIM_CHECK_RES_CAT');
    let pid_info = dim_check_res_table.select("*", { "STD_LIB_ID": standard_id });
    if (pid_info.length == 0) {
        return { result: false, message: "未查找到对应检测任务" };
    }
    let pid_level = parseInt(pid_info[0]["SZ_LEVEL"]);//找到当前标准所在检测资源目录的层级
    let pid_data = [];
    for (let p = 0; p < pid_level; p++) {
        pid_data.push(pid_info[0][`SZ_PID${p}`]);
    }
    let field_level = [];
    for (let p = 0; p < (max_level + pid_level + 1); p++) {
        field_level.push(`SZ_PID${p}`);
    }
    if ((pid_level + max_level) > (MAXLEVEL_CHECKCAT - 1)) {
        return {
            result: false, message: "超出最大层级"
        }
    }
    let add_check_cat_field = ['ID', 'PID', 'CAPTION', 'SZ_LEVEL', 'SZ_LEAF', 'MLLX', 'STD_LIB_ID', 'CJSJ', 'CJR'];//待更新的字段
    add_check_cat_field.pushAll(field_level);
    return { result: true, fields: add_check_cat_field, pid_level: pid_level, pid_data: pid_data, max_level: max_level };
}

/**
 * 获取到需要新增到目录表的数据
 * @param filter 过滤条件
 * @param standard_id 标准库ID
 * @param pid_level 父节点层级
 * @param pid_data 父节点层级数据
 * @param max_level 最大层级数
 */
function getNeedAddCatalogDatas(filter, standard_id, pid_level, pid_data, max_level) {
    let ds = db.getDefaultDataSource();
    let conn = ds.getConnection();
    let rows = [];
    try {
        conn.setAutoCommit(false);
        let dg_std_table_fold = ds.openTableData('SZSYS_4_DG_STD_TABLE_FOLDS', null, conn);
        let dim_check_res_table = ds.openTableData('DIM_CHECK_RES_CAT', null, conn);
        let std_fold_result = dg_std_table_fold.select("*", filter, null, 'SZ_LEVEL asc');
        let pid = `SZ_PID${pid_level}`;
        let check_res_result = dim_check_res_table.select("ID", `${pid}='${standard_id}'`);
        let std_fold_ids = [];
        let now = new Date();
        let userId = security.getCurrentUser().userInfo.userId;
        for (let i = 0; i < std_fold_result.length; i++) {//取出所有层级的目录ID
            let fold_info = std_fold_result[i];
            let level = fold_info["SZ_LEVEL"];
            for (let j = 0; j < (level + 1); j++) {
                let id = fold_info[`SZ_PID${j}`];
                if (std_fold_ids.indexOf(`'${id}'`) == -1) {
                    std_fold_ids.push(`'${id}'`);
                }
            }
        }
        print("查询出标准目录数据" + std_fold_ids);
        let need_add_fold_ids = [].concat(std_fold_ids);
        for (let i = 0; i < check_res_result.length; i++) {//获取需要新增的目录层级
            let check_cat_id = check_res_result[i];
            if (std_fold_ids.indexOf(`'${check_cat_id["ID"]}'`) != -1) {
                need_add_fold_ids.remove(`'${check_cat_id["ID"]}'`);
            }
        }
        print("查询出dim_check数据" + need_add_fold_ids);
        print("pid_data:" + pid_data)
        let std_fold_detail_result = need_add_fold_ids.length > 0 ? dg_std_table_fold.select("*", `STD_TABLE_FOLD_ID in (${need_add_fold_ids.join(',')})`, null, 'SZ_LEVEL asc') : [];
        for (let i = 0; i < std_fold_detail_result.length; i++) {
            let fold_detail = std_fold_detail_result[i];
            let level = fold_detail["SZ_LEVEL"];
            let uuid_cat = fold_detail["STD_TABLE_FOLD_ID"];
            let row = [];
            row.push(uuid_cat);
            row.push(fold_detail["PARENT_STD_TABLE_FOLD_ID"]);
            row.push(fold_detail['STD_TABLE_FOLD_NAME']);
            let level_ = level + pid_level;
            row.push(level_);
            let isleaf = fold_detail['SZ_LEAF'] == 1 ? 'true' : 'false';
            row.push(isleaf);
            row.push(null);
            row.push(null);
            row.push(now);
            row.push(userId);
            row.pushAll(pid_data);
            for (let l = 0; l < (level + 1); l++) {
                row.push(fold_detail[`SZ_PID${l}`]);
            }
            for (let m = 0; m < (max_level - level); m++) {
                row.push(null);
            }
            rows.push(row);
        }
    } finally {
        conn.close();
    }
    return rows;
}


/**
 * 更新关联关系规则
 */
export function updateRelationRule(request: HttpServletRequest, response: HttpServletResponse, params: any) {
    let task_id = params.task_id;
    if (task_id == null) {
        return { result: false, message: "未传递检测任务id" }
    }
    let task_add_rules = [];
    let query: QueryInfo = {
        sources: [{
            id: "model1",
            path: "/sysdata/data/tables/dg/DG_CHECK_TASK_TABLES.tbl"
        }],
        fields: [{
            name: "DW_TABLE_ID", exp: "model1.DW_TABLE_ID"
        }, {
            name: "GZKDM", exp: "model1.GZKDM"
        }],
        filter: [{
            exp: `model1.CHECK_TASK_ID='${task_id}'`
        }]
    }
    let check_resid_result = dw.queryData(query);
    let data = check_resid_result.data;
    for (let i = 0; i < data.length; i++) {
        let info = data[i];
        let resid = <string>info[0];
        let rule_lib_id = <string>info[1];
        insertRelationInfo(resid, rule_lib_id);
        task_add_rules.push([task_id, 'RELATION_EXIST', resid, 'MD']);
    }
    dw.updateDwTableData("/sysdata/data/tables/dg/DG_CHECK_TASK_RULES.tbl", {
        mergeIntoRows: [{
            fieldNames: ['CHECK_TASK_ID', 'RULE_ID', 'DW_TABLE_ID', 'RULE_FROM'],
            rows: task_add_rules
        }]
    })
    return { result: true, message: `本次更新${data.length}条数据` };
}

/**
 * 查询DG_RULES_LIB表获取待治理表ID和对应的别名,提供给前端表达式对话框使用
 * @param request
 * @param response
 * @param params
 */
export function getRuleTableInfos(request: HttpServletRequest, response: HttpServletResponse, params: any) {
    const query: QueryInfo = {
        sources: [{
            id: "model1",
            path: "/sysdata/data/tables/dg/DG_RULES_LIB.tbl"
        }],
        fields: [{
            name: "MAIN_TABLE_ID", exp: "model1.MAIN_TABLE_ID"
        }, {
            name: "BM", exp: "model1.ALIAS"
        }, {
            name: "RULES_LIB_NAME", exp: "model1.RULES_LIB_NAME"
        }]
    }
    return dw.queryData(query).data;
}

/**
 * 获取指定模型的所有字段信息，返回给表达式对话框使用
 * @param request
 * @param response
 * @param params
 */
export function getFieldInfos(request: HttpServletRequest, response: HttpServletResponse, params: any) {
    const tableId = params.tableId;
    const isDimension = params.isDimension;
    const tableInfo = dw.getDwTable(tableId);
    // 拼接表的维度信息和度量信息
    if (isDimension) {
        return tableInfo.dimensions;
    } else {
        return tableInfo.measures
    }
}

/**
 * 获取当前所有检测字段的字段信息
 * @param request
 * @param response
 * @param params
 */
export function getAllTableFieldInfos(request: HttpServletRequest, response: HttpServletResponse, params: any) {
    let json: JSONObject = {};
    const query: QueryInfo = {
        sources: [{
            id: "model1",
            path: "/sysdata/data/tables/dg/DG_RULES_LIB.tbl"
        }],
        fields: [{
            name: "MAIN_TABLE_ID", exp: "model1.MAIN_TABLE_ID"
        }, {
            name: "ALIAS", exp: "model1.ALIAS"
        }]
    };
    let data = dw.queryData(query).data;
    for (let i = 0; i < data.length; i++) {
        const d = data[i];
        const tableId = <string>d[0];
        const sourceId = <string>d[1];
        const tableInfo = dw.getDwTable(tableId);
        json[sourceId] = { dimensions: tableInfo.dimensions, measures: tableInfo.measures };
    }
    return json;
}

/**删除当前检测任务的指定类型的检测规则 */
export function deleteExistCheckRules(request: HttpServletRequest, response: HttpServletResponse, params: any) {
    let taskId = params.taskId;
    let type = params.type;
    if (type == "" || taskId == "") {
        return false;
    }
    let ds = db.getDefaultDataSource();
    let checkRulesTable = ds.openTableData('SZSYS_4_DG_CHECK_TASK_RULES');
    if (type == "nzgz") {
        checkRulesTable.executeUpdate("delete from SZSYS_4_DG_CHECK_TASK_RULES where CHECK_TASK_ID=? AND RULE_FROM!='CSTM'", [taskId]);
    } else if (type == "cstm") {
        checkRulesTable.executeUpdate("delete from SZSYS_4_DG_CHECK_TASK_RULES where CHECK_TASK_ID=? AND RULE_FROM='CSTM'", [taskId]);
    } else if (type == "all") {
        checkRulesTable.executeUpdate("delete from SZSYS_4_DG_CHECK_TASK_RULES where CHECK_TASK_ID=?", [taskId]);
    }
}
const INSERT_DG_RULES_FIELDS = ["RULE_ID", "RULES_LIB_ID", "DG_RES_CATALOG", "RULE_NAME", "RULE_DEFINE_TYPE", "RULE_EXP", "CHECK_FIELD", "BUILTIN_TYPE", "ENABLE", "ABANDON", "CREATE_TIME", "CREATOR"];
/**
 * 更新内置规则信息，通过查询加工判断哪些内置规则是新增的，哪些内置规则已经不存在
 * @param request
 * @param response
 * @param params
 */
export function updateBuiltInRule(request: HttpServletRequest, response: HttpServletResponse, params: { tableId?: string }) {
    let tableId = params && params.tableId;
    let queryAddRuleInfo: QueryInfo = {
        sources: [{
            id: "model1",
            path: "/sysdata/data/tables/sdi/data-check/check_rules/SDI_BUILTINT_RULE_ADD.tbl"
        }],
        fields: [{
            name: "RULES_LIB_ID", exp: "model1.RULES_LIB_ID"
        }, {
            name: "RULE_DESC", exp: "model1.RULE_DESC"
        }, {
            name: "RULE_EXP", exp: "model1.RULE_EXP"
        }, {
            name: "CHECK_FIELD", exp: "model1.CHECK_FIELD"
        }, {
            name: "BUILTIN_TYPE", exp: "model1.BUILTIN_TYPE"
        }],
        filter: [{
            exp: tableId != null && tableId != "" ? "model1.RULES_LIB_ID=param1" : ""
        }],
        params: [{
            name: "param1", value: tableId
        }]
    }
    let needAddBuiltInResult = dw.queryData(queryAddRuleInfo);
    let needAddBuiltInRows = needAddBuiltInResult.data;
    let addRows = [];
    let now = new Date().getTime();
    let userId = security.getCurrentUser().userInfo.userId;
    for (let i = 0; i < needAddBuiltInRows.length; i++) {
        let row = [];
        let needAddRow = needAddBuiltInRows[i];
        let ruleId = utils.uuid();
        row.push(ruleId);
        row.push(needAddRow[0]);
        row.push(needAddRow[0]);
        row.push(needAddRow[1]);
        row.push('BUILTIN');
        row.push(needAddRow[2]);
        row.push(needAddRow[3]);
        row.push(needAddRow[4]);
        row.push(1);
        row.push(0);
        row.push(now);
        row.push(userId)
        addRows.push(row);
    }
    let queryDelRuleInfo: QueryInfo = {
        sources: [{
            id: "model1",
            path: "/sysdata/data/tables/sdi/data-check/check_rules/SDI_BUILTINT_RULE_DELETE.tbl"
        }],
        fields: [{
            name: "RULE_ID", exp: "model1.RULE_ID"
        }],
        filter: [{
            exp: tableId != null && tableId != "" ? "model1.RULES_LIB_ID=param1" : ""
        }],
        params: [{
            name: "param1", value: tableId
        }]
    };
    let needDelResult = dw.queryData(queryDelRuleInfo);
    let needDelRows = needDelResult.data;
    /**
     * 对于已经不存在的内置规则，不要直接删除，因为要保留历史记录,查看历史检测结果时也要能看到具体的说明，
     * 增加一个废弃标志，用于处理
     * 只有废弃的内置规则才能删除，同时不能修改启用标志
     * 同时自动将检测任务中已经在使用的对应规则移除掉
     */
    let modifyRows = [];
    for (let i = 0; i < needDelRows.length; i++) {
        const delRow = needDelRows[i];
        modifyRows.push({
            keys: delRow,
            row: ['0', '1']
        })
    }
    console.info("待删除的内置规则信息为" + JSON.stringify(modifyRows))
    dw.updateDwTableData("/sysdata/data/tables/dg/DG_RULES.tbl", {
        mergeIntoRows: [{
            fieldNames: INSERT_DG_RULES_FIELDS,
            rows: addRows
        }],
        modifyRows: [{
            fieldNames: ["ENABLE", "ABANDON"],
            rows: modifyRows
        }]
    });
}

/**
 * 删除自定义规则
 * @param request
 * @param response
 * @param params
 */
export function deleteCstmRule(request: HttpServletRequest, response: HttpServletResponse, params: { ruleId?: string }) {
    let ruleId: string = params.ruleId;
    if (!ruleId) {
        return { result: false, message: "未传递参数！" };
    }
    let ds = db.getDefaultDataSource();
    let table = ds.openTableData("SZSYS_4_DG_RULES");
    let row = table.selectFirst("*", { "RULE_ID": ruleId });
    table.del({ "RULE_ID": ruleId });
    return { result: true, message: `删除自定义规则${row['RULE_NAME']}成功` }
}

/**
 * 创建指定的检测数据表
 * 创建的模型在/sdi/sys/data-gonver/${检测任务id}/${模型名称}
 * 表有两个${表名}_SUCCESS_TABLE和${表名}_INTERCEPT_TABLE
 * @param request
 * @param response
 * @param params
 */
export function createCheckOutPutTable(request: HttpServletRequest, response: HttpServletResponse, params: { taskId?: string }) {
    let taskId = params?.taskId;
    if (taskId) {
        return { result: false, message: "未传递参数" };
    }
    console.debug("开始自动创建输出结果表,传入检测任务id为" + taskId);
    let ds = db.getDefaultDataSource();
    let checkTaskTable = ds.openTableData(CHECK_TASK_TABLE);
    let checkTaskInfo = checkTaskTable.selectFirst("*", { "CHECK_TASK_ID": taskId });
    let isOutPut = checkTaskInfo["IS_OUTPUT_RESULT"];
    if (isOutPut != "1") {
        console.debug("当前任务没有指定输出结果，中止创建");
        return { result: true, message: "没有指定输出结果" };
    }
    let outPutSource = checkTaskInfo["OUTPUT_DATASOURCE"];
    let checkResTable = ds.openTableData(CHECK_TASK_RES_TABLE);
    let checkResInfo = checkResTable.select("*", { "CHECK_TASK_ID": taskId });
}

interface CheckTaskInfo {
    taskId: string;
    taskName: string;
    schedule?: string;
    isOutPut?: string;
    dataSource?: string;
    outputDirPath?: string;
}

interface CheckTableInfo {
    resid: string;
    dataFilter?: string;
    isIncrement?: string;
    lastCheckTime?: number;
    lastCheckTimeExp?: string;
    errDetailTablePath: string;
    outputSuccessTable?: string;
    outputInterceptTable?: string;
}

/**
 * 将检测任务的检测资源自动生成加工
 */
class checkTaskToDataFlow {
    private taskId: string;//检测任务id
    private taskInfo: CheckTaskInfo;

    constructor(args: { taskId: string }) {
        this.taskId = args.taskId;
        this.taskInfo = this.buildTaskInfo();
    }

    private buildTaskInfo() {
        let taskId = this.taskId;
        let ds = db.getDefaultDataSource();
        let checkTaskTable = ds.openTableData(CHECK_TASK_TABLE);
        let checkTaskInfo = checkTaskTable.selectFirst("*", { "CHECK_TASK_ID": taskId });
        let outputDirPath = this.createOutputDir();
        let taskInfo: CheckTaskInfo = {
            taskId,
            taskName: checkTaskInfo["CHECK_TASK_NAME"],
            schedule: checkTaskInfo["SCHEDULE_CODE"],
            isOutPut: checkTaskInfo["IS_OUTPUT_RESULT"],
            dataSource: checkTaskInfo["OUTPUT_DATASOURCE"],
            outputDirPath
        }
        return taskInfo;
    }

    public createDataFlow() {
        let taskInfo = this.taskInfo;
        let ds = db.getDefaultDataSource();
        let checkResTable = ds.openTableData(CHECK_TASK_RES_TABLE);
        let checkResInfo = checkResTable.select("*", { "CHECK_TASK_ID": this.taskId });
        for (let i = 0; i < checkResInfo.length; i++) {
            let row = checkResInfo[i];
            let tableInfo = this.buildTableInfo(row);
            let createCheckTable = new CheckTableToDataFlow({ tableInfo, taskInfo })
        }
    }

    /**
     * 构建检测资源生成加工所需的数据结构
     */
    private buildTableInfo(row: any) {
        let errDetailTable = row["ERR_DETAIL_TABLE_PATH"];
        if (errDetailTable == null) {//如果没有指定错误明细表那么获取系统指定的明细表
            let resid = row["DW_TABLE_ID"];
            let dwTable = dw.getDwTable(resid);
            let prop = dwTable.properties;
            let dataSource = prop.datasource;
            errDetailTable = "/sysdata/data/tables/dg/confs/" + dataSource + ".tbl";
        }
        let tableInfo: CheckTableInfo = {
            resid: row["DW_TABLE_ID"],
            dataFilter: row["DATA_FILTER"],
            isIncrement: row["IS_INCREMENT"],
            lastCheckTime: row["LAST_CHECK_TIME"],
            lastCheckTimeExp: row["LAST_CHECK_TIME_EXP"],
            errDetailTablePath: row["ERR_DETAIL_TABLE_PATH"],
            outputSuccessTable: row["OUTPUT_SUCCESS_TABLE"],
            outputInterceptTable: row["INTERCEPT_OUTPUT_TABLE"]
        };
        return tableInfo;
    }

    /**
     * 生成检测任务的目录
     */
    private createOutputDir() {
        let taskInfo = this.taskInfo;
        let path = "/sdi/data/tables/sys/data-govern/" + taskInfo.taskId;
        mkdirs({
            name: taskInfo.taskId,
            desc: taskInfo.taskName,
            path
        });
        return path;
    }
}

/**
 * 将检测资源自动输出成加工
 */
class CheckTableToDataFlow {
    private tableInfo: CheckTableInfo;//检测资源表信息
    private taskInfo: CheckTaskInfo;
    private tableDirPath: string;
    constructor(args: { tableInfo: CheckTableInfo, taskInfo: CheckTaskInfo }) {
        this.tableInfo = args.tableInfo;
        this.taskInfo = args.taskInfo;
        this.createOutPutTableDir();
    }

    /**
     * 创建输出表
     */
    public createOutPutDataFlow() {

    }

    /**
     * 生成检测任务输出表的目录
     */
    public createOutPutTableDir() {
        let tableInfo = this.tableInfo;
        let taskInfo = this.taskInfo;
        let resid = tableInfo.resid;
        let outputDirPath = taskInfo.outputDirPath;
        let outputTableDirPath = this.tableDirPath = outputDirPath + "/" + resid;
        let metaFile = metadata.getFile(resid);
        let desc = metaFile.desc || metaFile.name;
        mkdirs({
            name: resid,
            desc,
            path: outputTableDirPath
        });
    }
}