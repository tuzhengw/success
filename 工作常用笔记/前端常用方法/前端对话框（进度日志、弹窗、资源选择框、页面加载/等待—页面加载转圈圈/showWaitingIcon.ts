import { showWaitingIcon } from 'sys/sys';

//  直接将需要【等待的事务】作为参数传递进去即可

spg: {
	showWaitingIconBeforeFinish: (event: InterActionEvent) => {
		showWaitingMessage('保存中');
	},
	showWaitingIcon({
		// 显示在那个DOM中
		target: event.renderer.domBase,
		// 等待的event事件
		promise: 
			url2pdf(
				fileInfo.id,
				ctx(`/SJDWFW/app/数据服务.app/数据服务/检验报告信息.spg?pageUniqueCode='2021100010001'`),
				// 导出名
				`${importPdfName}`,
				ExportReusltType.DOWNLOAD,
				exportSizeOptionsInfo
		),
		delay: 200,
		// layoutTheme: 'waiting-icon-default',   这里可以指定要应用的CLASS样式
		throwOnError: true
	});
}

 showWaitingIcon({
	target: document.body,
	promise: promise,
	delay: 200,
	layoutTheme: 'waiting-icon-default',
	throwOnError: true
});
	
	
/**
 * 在指定的元素上显示一个表示等待的动画。
 *
 * * 动画的dom会追加到目标dom里。
 * * 如果对同一个dom多次调用showWaiting，动画结束的时间取决于最后一次调用时传递的参数。
 *
 * @param args.target 动画显示的目标位置。
 * @param args.promise 延迟对象，它被resolve或reject时，动画结束。动画显示期间，如果多次调用，那么动画的结束时机由最后一个promise决定。
 * @param args.delay 动画显示的延迟。默认200ms。
 * @param args.layoutTheme 布局主题。默认"waiting-icon-default"。
 * @param args.colorTheme 颜色主题。默认取目标位置或者其上层控件的颜色主题。
 * @param args.throwOnError 出现异常后是否抛出异常。默认true。
 */
export function showWaitingIcon({
	target,
	promise = null,
	delay = 200,
	layoutTheme = 'waiting-icon-default',
	colorTheme,
	throwOnError = true
}: {
	target: HTMLElement,
	promise?: Promise<any>,
	delay?: number,
	layoutTheme?: string,
	colorTheme?: string,
	throwOnError?: boolean
}): void {
	if (!(target instanceof Element)) {
		return;
	}

	// let stack = new Error().stack;
	let context = waitingIcons.find(icon => icon.target === target);
	const hide = () => {
		// 执行hide时，判断当前等待的延时对象与此时resolve的延时对象是否是同一个，如果不是就什么不做，直到当前等待的延时对象resolve
		// 才会取消等待
		if (promise === context.waitingPromise) {
			cancelWaitingIcon(target);
		}
	}
	if (context) {
		context.timer && clearTimeout(context.timer);
		context.timer = null;
	} else {
		context = waitingIcons.find(context => context.target == null);
		if (context) {
			context.target = target;
			context.cycles++;
		} else {
			// 内存保护，防止页面上出现超过100个圆圈动画。
			if (waitingIcons.length > 100) {
				console.warn('waitingIcons.length > 100');
				waitingIcons.forEach(context => {
					cancelWaitingIcon(context.target);
					context.ring && context.ring.dispose();
				})
				waitingIcons.length = 0;
			}
			context = { target, sn: waitingIcons.length, cycles: 0 };
			waitingIcons.push(context);
		}
	}
	const show = () => {
		if (context.target !== target) { // 动画的显示可能有延时的，可能到这里时这个动画已经被cancel了。
			return;
		}
		context.timer = null;
		// context.stack = stack;
		let ring = context.ring;
		if (colorTheme == null) {
			const component = getSZObjectFromDom(target, (dom, obj) => obj instanceof Component) as Component;
			colorTheme = component?.colorTheme || "";
		} else {
			colorTheme = colorTheme || "";
		}
		if (ring) {
			ring.setLayoutTheme(layoutTheme);
			ring.setColorTheme(colorTheme);
			target.classList.add(WAITING_ICON_CLASS);
			ring.setDomParent(target);
			return;
		}
		Promise.resolve(ProgressRingClass || import('../commons/basic').then(m => ProgressRingClass = <Constructable<ProgressRing>>(m.ProgressRing))).then(() => {
			ring = context.ring;
			if (!ring) {//有可能对同一个context多次进入这个then
				ring = context.ring = new ProgressRingClass({
					className: WAITING_RING_CLASS,
					value: -1,
					layoutTheme: layoutTheme,
					colorTheme: colorTheme,
				});
				// (<any>ring).__component_create_stacktrace = stack;
				// (<any>ring).__ring_count = context.sn;
				// (<any>ring).__ring_random = Math.random();
			}
			if (context.target === target) { // 动画的显示是异步的，可能到这里时这个动画已经被cancel了。
				target.classList.add(WAITING_ICON_CLASS);
				ring.setDomParent(target);
			}
		});
	}

	/**
	 * 记住等待最新的延时对象，用于在hideWaiting时调用，重复对一个target调用showWatingIcon时，只有最新的延时对象resolve才
	 * 去隐藏等待
	 */
	context.waitingPromise = promise;

	promise && promise.then(hide, e => {
		if ((context.waitingPromise === promise) && throwOnError) {
			hide();
			throw e;
		}
	});

	if (!context.ring || context.ring.getDomParent() !== target) {//有可能连续调用2此，第二次调用时已经显示出来了
		context.delay = delay;
		if (delay) {
			context.timer = setTimeout(show, delay);
		} else {
			show();
		}
	}
}

/**
 * 取消指定元素上使用showWaitingIcon方法显示的动画，即使最初传递的promise仍是pending状态。
 *
 * @param target 目标元素。
 */
export function cancelWaitingIcon(target: Element): void {
	const context = target && waitingIcons.find(context => context.target === target);
	if (context) {
		context.timer && clearTimeout(context.timer);
		target.classList.remove(WAITING_ICON_CLASS);
		context.timer = null;
		context.waitingPromise = null;
		context.target = null;
		context.ring && context.ring.setDomParent();
	}
}