

import dw from "svr-api/dw";
import utils from "svr-api/utils";
import db from "svr-api/db";
import metadata from "svr-api/metadata";


/**
 * 批处理脚本模版
 */
function main() {
    let path: string = `/HLHT/data/tables/xml/registerTable/产后访视服务信息`;
    print(metadata.getFolder(path)); // 返回null，则文件夹不存在
	print(metadata.getFile("table/xml/1.txt")); // 返回null，则文件不存在

    try{
		metadata.remove(path);
	} catch(e) {}
    metadata.mkdirs(path); // 创建文件夹，注意: 创建的文件，可能存在，但页面不显示，创建前，先进行删除
	
	metadata.createFile("table/xml/1.txt"); // 创建文件
}
