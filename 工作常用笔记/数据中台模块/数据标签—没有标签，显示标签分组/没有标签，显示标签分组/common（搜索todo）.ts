import { InputTreeDataProvider } from "app/superpage/superpagebuilder";
import { Button, DataProvider } from "commons/basic";
import { Dialog } from "commons/dialog";
import { Form } from "commons/form";
import { filter } from "commons/jszip/jszip.min";
import { showMenu } from "commons/menu";
import "css!./common.css";
import { deleteLabels, getDwTableDataManager, getLabels, getQueryManager, saveLabelLib, updateDwTableDatas, refreshModelState } from "dw/dwapi";
import { getCurrentUser, getMetaRepository, MetaFileViewer, showDrillPage } from "metadata/metadata";
import { assign, browser, cancelWaitingIcon, clone, Component, ComponentArgs, ContainerComponent, ContainerComponentArgs, ctx, DataReadyState, deepEqual, dispatchResizeEvent, DEF_ITEM_DATA_CONF, getSZObjectFromEvent, IDataProvider, isEmpty, ItemDataConfInfo, rc, setIcon, ShowFloatLimitMode, showWaitingIcon, SZEvent, SZEventCallback, throwError, waitAjax, waitAnimationFrame, showErrorMessage, waitRender, showFormDialog, showSuccessMessage, showConfirmDialog, getCurrentMobileApp, uuid } from "sys/sys";
const DYNAMICLABELPATH = '/BQGL/app/label.app/data-label/dynamicLabelSpg.html';
const doc = !browser.nodejs && document;

const DEFAULT_ITEM_HEIGHT = 24;

export interface TileArgs extends ContainerComponentArgs {
	/**
	 * 用于说明如何从data中读取value、caption、icon等信息。默认DEF_ITEM_DATA_CONF。
	 */
	itemDataConf?: ItemDataConfInfo;
	/**
	 * 由初始时加载的节点组成的json数组
	 */
	items?: any[];
	/**
	 * list动态数据的提供者，是一个类对象，list在初始化、tree在展开节点时都可能会向dataProvider请求数据。
	 */
	dataProvider?: IDataProvider<any>;
	/**
	 * 块块高度。
	 */
	itemHeight?: number;
	/**
	 * 点击节点时调用，点击节点的文字（包括文字后方的空白区域）和图标都会触发onclick。
	 */
	onclick?: (sze: SZEvent, tile: Tile, item: TileItem) => void | boolean;
	/**
	 * 绘制一个item的文字时调用，主要用来对显示出来的文字进行转换，而又不需要改变控件所持有的数据。
	 */
	onrendertext?: (tile: Tile, item: TileItem) => void | string;
	/**
	 * 是否能编辑标签，默认为false
	 */
	allowEditLabel?: boolean;
	/**
	  * 是否自动展开
	  */
	autoExpand?: boolean;
	/**
	 * 能否添加标签
	 */
	allowAddLabel?: boolean;
}

/**
 * 平铺控件
 * 使用场景：
 * 1. 数据标签展示界面
 */
export class Tile extends ContainerComponent {
	public itemDataConf: ItemDataConfInfo;
	public dataProvider: IDataProvider<any>;
	public itemHeight: number;
	public datas: DataLabelTileGroupInfo[];
	protected items: Array<TileItem>;
	protected itemsMap: { [key: string]: TileItem };
	protected groups: Array<TileGroup>;
	protected refreshPromise: Promise<void>;
	//protected init_promise: Promise<void>;
	protected renderPromise: Promise<void>;
	public allowEditLabel: boolean;
	public allowAddLabel: boolean;
	protected autoExpand: boolean;

	public onclick: (sze: SZEvent, tile: Tile, item: TileItem) => void | boolean;
	public onrendertext: (tile: Tile, item: TileItem) => void | string;

	constructor(args: TileArgs) {
		super(args);
	}

	protected _init_default(args: TileArgs): void {
		super._init_default(args);

		if (args.autoPlaceholder == null || args.autoPlaceholder) {
			this.autoPlaceholder = true;
		}
		this.itemDataConf = Object.assign({}, DEF_ITEM_DATA_CONF, args.itemDataConf);
		this.dataProvider = args.dataProvider || null;
		this.itemHeight = args.itemHeight || DEFAULT_ITEM_HEIGHT;
		this.items = [];
		this.itemsMap = {};
		this.groups = [];
		this.renderPromise = null;
		this.onclick = args.onclick || null;
		this.onrendertext = args.onrendertext || null;
		this.allowEditLabel = args.allowEditLabel || false;
		this.allowAddLabel = args.allowAddLabel || false;
		this.autoExpand = args.autoExpand || null;
	}

	protected _init(args: TileArgs): HTMLElement {
		let domBase = super._init(args);
		domBase.onclick = this.doClickAny.bind(this);
		//this.waitRender();
		return domBase;
	}

	// protected _init_data(args: TileArgs, childrenItemsEnabled?: boolean): Promise<void> {
	// 	let dataProvider = this.dataProvider;
	// 	if (!dataProvider && args.items) {
	// 		dataProvider = this.dataProvider = new DataProvider({
	// 			keyPrefix: this.keyPrefix,
	// 			data: args.items,
	// 			itemDataConf: this.itemDataConf,
	// 			childrenItemsEnabled: !!childrenItemsEnabled,
	// 		})
	// 	}
	// 	return !dataProvider || !dataProvider.fetchData
	// 		? null
	// 		: (this.init_promise = this.refreshPromise = dataProvider.fetchData().then((datas: DataLabelTileGroupInfo[]) => {
	// 			// 如果本次刷新未完成又有下一次刷新在执行，则取消loadData。原因：只需要保持最后的刷新状态。
	// 			if (this.init_promise !== this.refreshPromise) {
	// 				this.init_promise = null;
	// 				return;
	// 			};
	// 			this.refreshPromise = null;
	// 			this.init_promise = null;
	// 			if (datas && datas.length) {
	// 				this.loadData(datas);
	// 			} else {
	// 				this.showPlaceholder();
	// 			}
	// 			return this.renderPromise;
	// 		}));
	// }

	public setDataProvider(dp: IDataProvider<any>) {
		this.dataProvider = dp;
		if (!dp) {
			return Promise.resolve();
		}
		return this.refresh();
	}

	public setAllow(allow: boolean) {
		this.allowEditLabel = allow;
	}

	public setShowAddBtn(allow: boolean) {
		this.allowAddLabel = allow;
	}

	public dispose(): void {
		let domBase = this.domBase;
		if (domBase) {
			domBase.onclick = null;
			this.groups.forEach(i => i.dispose());
		}

		super.dispose();
	}

	/**
	 * 点击list内任意dom时触发。
	 * @param event
	 */
	protected doClickAny(event: Event) {
		const target = <HTMLElement>event.target;
		const item = this.getEventItem(event);
		if (!item) return;
		const szevent: SZEvent = {
			component: this,
			item: item,
			data: item && item.getData(),
			srcEvent: event,
			target: target,
		};
		if (!(target.closest('.label_tile_icon_more') || target.closest('.labelDisplay-addLabel')) && this.onclick && this.onclick(szevent, this, item) === false) {
			return; // 阻止控件的默认点击行为，包括点击来勾选、选中等。
		}
	}

	public getEventItem(event: Event): TileItem {
		let item = getSZObjectFromEvent(event);
		if (item && item instanceof TileItem) {
			return item;
		}
		return null;
	}

	public getItems(): TileItem[] {
		return this.items;
	}

	/**
	 * 返回指定位置的item对象。
	 * @returns 指定位置的item对象，不存在时为undefined。
	 */
	public getItem(index: number): TileItem {
		return this.items[index];
	}

	/**
	 * 返回指定id对应的item对象。
	 * @param id 想要获取的item对象的id。
	 * @returns 指定id对应的item，如果找不到，为undefined。
	 */
	public getItemById(id: string): TileItem {
		let itemsMap = this.itemsMap;
		if (!itemsMap) {
			itemsMap = this.itemsMap = {};
			let items = this.items;
			for (let i = 0, len = items.length; i < len; i++) {
				let item = items[i];
				let itemId = item.getId();
				if (itemId != null) {
					itemsMap[itemId] = item;
				}
			}
		}
		return itemsMap[id];
	}

	/**
	 * 刷新。如果上一次刷新未完成，只执行本次刷新。
	 * @returns 延迟对象，等待刷新数据并且界面渲染完成。
	 */
	public refresh(): Promise<void> {
		let dataProvider = this.dataProvider;
		if (!dataProvider) return Promise.resolve();
		let promise: Promise<void>;
		this.showPlaceholder(null, DataReadyState.Loading);
		promise = dataProvider.fetchData().then((datas: any[]) => {
			if (promise !== this.refreshPromise) return;
			// 暂不考虑dom的复用
			this.loadData(datas);
			this.refreshPromise = null;
			return this.renderPromise;
		}).catch(e => {
			throw e;
		})
		return this.refreshPromise = promise;
	}

	public loadData(datas: DataLabelTileGroupInfo[]): void {
		if (!datas || !datas.length || deepEqual(datas, this.datas, 3)) {
			return;
		}
		this.reset();
		this.datas = datas;
		// todo 
		for (let i = 0; i < datas.length; i++) {
			let data = datas[i];
			if (data.isGroupNode) {
				data.expand = this.autoExpand;
				let group = new TileGroup(this, data, this.itemDataConf);
				this.groups.push(group);
				continue;
			}
			if (!data.items || data.items.length == 0) {
				continue;
			}
			data.expand = this.autoExpand;
			let group = new TileGroup(this, data, this.itemDataConf);
			this.groups.push(group);
			this.items.pushAll(group.items);
		}
		this.requestRender();
	}

	// public waitRender(): Promise<void> {
	// 	return Promise.resolve(this.init_promise).then(() => wait(20)).then(() => {
	// 		return this.renderPromise && this.waitRender();
	// 	})
	// }

	protected requestRender(): Promise<void> {
		let renderPromise = this.renderPromise;
		if (renderPromise) {
			return renderPromise;
		}
		return this.renderPromise = waitAnimationFrame().then(() => {
			if (this.domBase) {
				/**
				 * 有可能在将要渲染时tree已经被dispose了，所以需要判断domBase是否存在。
				 */
				this.render();
			}
			this.renderPromise = null;//放在render后面吧，因为也许render过程中万一requestRender了还能不再次刷新。
		});
	}

	protected render(): void {
		let dom = this.domBase;
		this.groups.forEach(e => {
			dom.appendChild(e._createDom(true));
			if (dom.offsetParent) {
				e.checkShowBtn();
			}
		});
		super._init_scrollbar({
			overflowX: "hidden",
			overflowY: "scroll",
			autoPlaceholder: true,
			placeholder: DataReadyState.Nodata
		});
	}

	public reset(): void {
		this.groups = [];
		this.items = [];
		this.itemsMap = {};
		this.domBase.removeAllChildren();
	}

	public checkShowBtn(): void {
		this.groups.forEach(e => {
			e.checkShowBtn();
		});
	}

}

/**
 * Tile中的一个平铺块块
 */
export class TileItem {
	protected owner: Tile;
	public domBase: HTMLElement;
	public domImage: HTMLElement;
	public domWrapper: HTMLElement;
	public domCaption: HTMLElement;
	public domMore: HTMLElement;

	protected id: string;
	protected value: string;
	protected caption: string;
	protected image: string;
	protected color: string;
	protected selected: boolean;
	protected className: string;
	protected data: DataLabelTileItemInfo;
	protected dataConf: ItemDataConfInfo;

	/**
	 * 等待icon所使用的延迟对象，即便有等待图标显示，此属性也可能为空
	 */
	protected waitingPromise: Promise<any>;
	/**
	 * 当前是否在显示等待图标，
	 */
	protected waitingIconVisible: boolean;

	public group: TileGroup;

	constructor(group: TileGroup, data: DataLabelTileItemInfo, dataConf: ItemDataConfInfo) {
		this.owner = group.owner;
		this.id = data[dataConf.idField] || null;
		this.value = data[dataConf.valueField] || null;
		this.caption = data[dataConf.captionField] || null;
		this.image = data[dataConf.imageField] || null;
		this.color = data['color'] || null;
		this.className = data[dataConf.classNameField] || null;
		this.data = data;
		this.group = group;
		this.dataConf = dataConf;
	}

	public dispose() {
		this.waitingIconVisible && this.domImage && cancelWaitingIcon(this.domImage);
		// BI-24625 js对象可能会被一些全局的对话框，菜单，面板持有，导致该js对象持有的dom和数据对象无法被GC, 这里简单将所有属性置为null来切断持有关系。
		for (let p in this) {
			if (this.hasOwnProperty(p)) {
				this[p] = null;
			}
		}
	}

	/**
	 * 创建dom
	 */
	public _createDom(): HTMLElement {
		let domItem = this._createDomBase("tileitem-base");
		this.createDomWraper(domItem);
		return domItem;
	}

	/**
	 * 创建控件的根dom，默认是一个div，子类可以自己重载。
	 * @returns 根dom。
	 */
	protected _createDomBase(className: string): HTMLElement {
		let domBase = this.domBase = doc.createElement('span');
		domBase.classList.add(className);
		return domBase;
	}

	/**
	 * 创建图标dom，并添加到domBase中。
	 * @param domBase 根dom。图标dom将添加到这个dom里。
	 * @returns 图标dom。
	 */
	protected createDomImage(domBase: HTMLElement): HTMLElement {
		let domImage = this.domImage = doc.createElement('span');
		domImage.classList.add('item-img');
		domBase.appendChild(domImage);
		return domImage;
	}

	/**
	 * 创建标题的dom，并添加到domBase中。
	 * @param domBase 根dom。标题dom将添加到这个dom里。
	 * @returns 标题dom。
	 */
	protected createDomCaption(domBase: HTMLElement): HTMLElement {
		let domCaption = this.domCaption = doc.createElement('span');
		domCaption.classList.add('item-caption');
		domBase.appendChild(domCaption);
		return domCaption;
	}

	/**
	 * 创建每个标签的编辑按钮
	 * @param domBase 根dom。编辑按钮dom将添加到这个dom里
	 */
	protected createDomEditBtn(domBase: HTMLElement) {
		let domMore = this.domMore = document.createElement("span");
		domMore.classList.add("label_tile_icon_more", "icon-more");
		domMore.onclick = this.doClickMore.bind(this);
		domBase.appendChild(domMore);
	}

	protected doClickMore(event) {
		let data = this.data;
		let self = this;
		let modelIdOrPath = this.owner.dataProvider.modelIdOrPath;
		let domFileViewer = this.domBase.closest('.metafileviewer-base');
		let groups = this.owner.groups.map(g => { return { id: g.id, caption: g.caption } });
		let labelCat = this.group.getId();
		let menuItem = [{
			id: "add",
			caption: "数据标记",
			layoutTheme: "smallbtn",
			value: "add"
		}, {
			id: "modify",
			caption: "修改标签",
			layoutTheme: "smallbtn",
			value: "modify"
		}, {
			id: "delete",
			caption: "删除标签",
			layoutTheme: "smallbtn",
			value: "delete"
		}];
		// if (data.label_type == '2') {
		// 	menuItem.remove(0);
		// }
		showMenu({
			id: "labelDisplay_more_menu",
			layoutTheme: "small",
			showAt: event.target,
			items: menuItem,
			onclick: (sze, menu, menuitem) => {
				let id = menuitem.getId();
				getDwTableDataManager().getDwDataLabelLib(modelIdOrPath).then(lib => {
					if (isEmpty(lib)) {
						return null;
					}
					let libCode = lib.code;
					if (id == 'add') {
						let spgName;
						let mode;
						if (data.label_type == '1') {
							spgName = '软标签';
							mode = 'soft';
						} else {
							spgName = '硬标签';
							mode = 'hard';
						}
						let fileViewer: MetaFileViewer = domFileViewer.szobject as MetaFileViewer;
						fileViewer.showDrillPage({
							url: {
								path: DYNAMICLABELPATH,
								params: { 'labelCode': data.id, 'labelLib': libCode, 'mainTableId': modelIdOrPath, 'labelName': data.caption, 'labelType': mode }
							},
							breadcrumb: true,
							preferredThemes: [],
							target: ActionDisplayType.Container,
							title: `${data.caption}`
						}).then(page => {
							let labelSpg = page;
							let breadCrumb = labelSpg.pageContainer && labelSpg.pageContainer.breadCrumb;
							let dombase: HTMLElement = breadCrumb && breadCrumb.domBase;
							if (dombase && dombase.children.length == 0) {
								let backButton = document.createElement('span');
								backButton.innerText = '返回';
								backButton.classList.add('data-label-mgr-custom-back');
								dombase.appendChild(backButton);
								backButton.onclick = () => {
									labelSpg.goBackPage();
								}
							}
						})
					} else if (id == 'modify') {
						showFormDialog({
							id: "customComp.modifyLabel",
							className: "customAddLabelForm",
							caption: "修改标签",
							content: {
								captionWidth: 72,
								items: [{
									id: "labelCode",
									caption: "标签id",
									formItemType: 'edit',
									visible: false
								}, {
									id: "labelName",
									caption: "标签名称",
									formItemType: 'edit',
									required: true
								}, {
									id: "labelCatalog",
									caption: "标签分组",
									formItemType: "combobox",
									required: true,
									itemDataConf: { valueField: "id" }
								}, {
									id: "labelType",
									caption: "打标方式",
									formItemType: 'selectpanel',
									required: true,
									visible: false,
									compArgs: {
										items: [{
											value: "1",
											caption: "软标签(通过筛选条件标记)"
										}, {
											value: "2",
											caption: "硬标签(通过人工挑选、匹配excel、关联数据表标记)"
										}],
										direction: "column"
									},
									enabled: false
								}, {
									id: 'desc',
									caption: "描述",
									formItemType: "textarea"
								}]
							},
							onshow: (event: SZEvent, dialog: Dialog) => {
								let form = <Form>dialog.content;
								form.waitInit().then(() => {
									let query: QueryInfo = {
										sources: [{
											id: "model1",
											path: "/sysdata/data/tables/dw/DW_DATA_LABELS.tbl",
											filter: `model1.LABEL_CODE='${data.id}'`
										}],
										fields: [{
											name: "LABEL_DESC", exp: "model1.LABEL_DESC"
										}]
									}
									getQueryManager().queryData(query, uuid()).then(result => {
										let data = result.data;
										if (!isEmpty(data)) {
											let labelDesc = form.getFormItemComp('desc');
											labelDesc.setValue(data[0][0]);
										}
									});
									let labelCatalog = form.getFormItemComp("labelCatalog");
									labelCatalog.setItems(groups);
								})
								form.loadData({ 'labelCode': data.id, 'labelName': data.caption, labelType: data.label_type, labelCatalog: labelCat })
							},
							buttons: [{
								id: "ok",
								layoutTheme: "defbtn",
								onclick: (event: SZEvent, dialog: Dialog, item: any) => {
									let form = <Form>dialog.content;
									let validate = form.validate();
									if (validate) {
										let data = form.getData();
										let row = [];
										row.push(data.labelName);
										row.push(data.desc);
										row.push(data.labelCatalog);
										let datapackage = {
											modifyRows: [{
												fieldNames: ['LABEL_NAME', 'LABEL_DESC', 'LABEL_CATEGORY'],
												rows: [{
													keys: [data.labelCode],
													row: row
												}]
											}]
										};
										rc({
											method: "POST",
											url: "/sysdata/extensions/succ-dataVisualization-dataLabelDisplay/dataLabelItem.action?method=updateDwLabelTable",
											data: {
												data: datapackage
											}
										}).then(() => {
											dialog.close();
											showSuccessMessage('修改标签成功');
											self.owner.dataProvider.setData([]);
											self.owner.refresh();
										})
									}
								}
							}, 'cancel']
						})
					} else {//刷新控件
						showConfirmDialog({
							caption: "删除标签",
							message: `将要删除${data.caption}这个标签，是否确定？`,
							onok: () => {
								deleteLabels([data.id], libCode).then(() => {
									showSuccessMessage('删除标签成功');
									self.owner.dataProvider.setData([]);
									self.owner.refresh();
								})
							}
						})
					}
				})

			}
		})
	}

	/**
	 * 创建标题、图标，以及容纳它们的dom，并添加到domBase中。
	 * @param domBase 根dom。容纳标题、图标的dom将添加到这个dom里。
	 * @returns 容纳标题、图标的dom。
	 */
	protected createDomWraper(domBase: HTMLElement): HTMLElement {
		let domWraper = this.domWrapper = doc.createElement('span');
		domWraper.classList.add('item-wraper');
		let label_level = this.data.label_level;
		let own_dept = this.data.own_dept;
		let desc = this.data.desc;
		if (label_level && own_dept) {
			domWraper.title = label_level + "：" + own_dept;
		}
		desc && (domWraper.title += (`\n标签描述：` + desc));
		if (this.image) {
			this.createDomImage(domWraper);
		} else {
			this.createDomCaption(domWraper);
		}
		let allow_edit = this.data.allow_edit;
		if (allow_edit && this.owner.allowEditLabel) {
			this.createDomEditBtn(domWraper);
		}
		domBase.appendChild(domWraper);
		return domWraper;
	}

	/**
	 * 接收dom
	 * @private
	 */
	public acceptDom(dom: HTMLElement): void {
		this.domBase = dom;
		this.renderText();
		dom.szobject = this;
		if (dom.id != '' || this.id) {
			dom.id = this.id;
		}
		let classList = dom.classList;
		//classList.toggle('item-selected', this.selected);
		this.className && classList.add(this.className);
	}

	/**
	 * 释放dom
	 * @private
	 */
	public releaseDom(): void {
		if (this.domBase) {
			this.domBase = null;
			//https://jira.succez.com/browse/BI-22116 释放dom的时候，如果有等待图标，也必须释放，否则会留给下一个节点的
			this.waitingIconVisible && this.domImage && cancelWaitingIcon(this.domImage);
			this.domImage = null;
			this.domCaption = null;
			this.className && this.domBase.classList.remove(this.className);
			this.domBase.szobject = null;
		}
	}

	/**
	 * 渲染，当有图标时则显示图标，没有才显示文字块
	 */
	public renderText(): void {
		let text = this.getRenderText();
		if (this.image) {
			this.setImage(this.image, true, text);
			this.waitingIconVisible && this.domImage && showWaitingIcon({
				target: this.domImage,
				promise: this.waitingPromise,
				delay: 0
			});
		} else {
			let domCaption = this.domCaption;
			if (domCaption) {
				domCaption.textContent = text;
				if (this.color) {
					this.domBase.style.borderColor = this.color;
					domCaption.style.color = this.color;
				}
			}
		}
		this.data.width && (this.domBase.style.width = this.data.width + "px");
	}

	/**
	 * 返回节点应该渲染出来的文本。
	 */
	public getRenderText(): string {
		const onrendertext = this.owner.onrendertext;
		if (onrendertext) {
			const text = onrendertext(this.owner, this);
			if (text !== undefined) {
				return <string>text;
			}
		}
		let text = this.caption || this.value;
		return text == null ? "" : text.toString();
	}

	/**
	 * 设置css类。
	 * @param className 根dom上的class，不能是空格分割的多个class，只能是一个class。
	 */
	public setClassName(className: string): void {
		let oldClassName = this.className;
		if (oldClassName != className) {
			const domBase = this.domBase;
			if (domBase) {
				oldClassName && domBase.classList.remove(oldClassName);
				className && domBase.classList.add(className);
			}
			this.className = className;
		}
	}

	/**
	 * 在节点的图标位置显示表示等待的动画。
	 *
	 * @param promise 当这个promise成功或失败时，结束动画。
	 * @param delay 动画的延迟时间。默认200。
	 * @returns 返回传入的Promise对象。
	 */
	public showWaitingIcon<T>(promise?: Promise<T>, delay?: number): Promise<T> {
		/**
		 * 有可能节点还未渲染，此时是没有dom的
		 */
		this.domImage && showWaitingIcon({
			target: this.domImage,
			promise,
			delay
		});
		this.waitingIconVisible = true;
		/**
		 * * a-b-d
		 * * a-c
		 * promise链中，如果c抛出了异常，d无法捕获，所以执行顺序必须是a-b-c-d。
		 * 所以这里返回  promise.then之后的延时对象。
		 */
		promise && (promise = promise.then((d) => {
			if (this.waitingPromise === promise) {
				this.waitingPromise = null;
				this.waitingIconVisible = false;
			};
			return d;
		}, (err) => {
			if (this.waitingPromise === promise) {
				this.waitingPromise = null;
				this.waitingIconVisible = false;
			};
			throw err;
		}));
		return this.waitingPromise = promise;
	}

	/**
	 * 取消显示节点的等待动画。
	 * @param restoreIcon 取消等待后，节点的图标。默认为显示等待之前，节点的图标。
	 */
	public cancelWaitingIcon(restoreIcon = this.image): void {
		this.domImage && cancelWaitingIcon(this.domImage);
		this.waitingPromise = null;
		this.waitingIconVisible = false;
		restoreIcon && this.setImage(restoreIcon, true);
	}

	public getId(): string {
		return this.id;
	}

	public getValue(): string {
		return this.value;
	}

	public getCaption(): string {
		return this.caption;
	}

	public getImage(): string | IconInfo {
		return this.image;
	}

	public getColor(): string {
		return this.color;
	}

	public getData(): DataLabelTileItemInfo {
		return this.data;
	}

	public setId(v: string): void {
		if (this.id != v) {
			this.id = v;
			this.domBase && (this.domBase.id = v);
		}
	}

	public setValue(v: string): void {
		if (this.value != v) {
			this.value = v;
			let domCaption = this.domCaption;
			domCaption && (domCaption.textContent = v == null ? "" : v);
		}
	}

	public setCaption(v: string): void {
		if (v != this.caption) {
			this.caption = v;
			let domCaption = this.domCaption;
			domCaption && (domCaption.textContent = v == null ? "" : v);
		}
	}

	public setImage(v: string, forceUpdate?: boolean, text?: string): void {
		if (this.image != v || forceUpdate) {
			this.image = v;
			// if (v && typeof v !== "string") {
			// 	v = assign({}, v, { fontSize: null });
			// }
			// setIcon(this.domImage, v, forceUpdate);
			// DATA-URL方式无法显示SVG内嵌的<image>, 这里还是直接把内容丢到dom结构里
			this.domImage.innerHTML = v;
			//this.domBase.title = text || this.caption;
		}
	}

	public setColor(v: string): void {
		if (this.color != v) {
			this.color = v;
			let domCaption = this.domCaption;
			if (domCaption) {
				domCaption.style.borderColor = this.color;
				domCaption.style.color = this.color;
			}
		}
	}

	public setData(data: DataLabelTileItemInfo): void {
		let dataConf = this.dataConf;
		this.data = data;
		this.setId(data[dataConf.idField] || null);
		dataConf.classNameField !== undefined && this.setClassName(data[dataConf.classNameField]); // 可能第一次刷新传入了classNameField的值，第二次没有传入，此时需要清除
		this.setValue(data[dataConf.valueField] || null);
		if (this.image) {
			this.setImage(data[dataConf.iconField] || null);
		} else {
			this.setCaption(data[dataConf.captionField] || null);
			this.setColor(data[dataConf.iconField] || null);
		}
		if (this.group.domBase) {
			this.renderText();
		}
	}
}

/**
 * Tile中的一组，一组有多个块块
 * 可能单行，可能多行，多行具备展开收起
 */
export class TileGroup {
	public owner: Tile;
	public domBase: HTMLElement;
	public domWraper: HTMLElement;
	public domCaption: HTMLElement;
	public expandBtn: Button;
	public collapseBtn: Button;
	public items: Array<TileItem>;
	public domLabelButton: HTMLElement;
	public addlabelBtn: Button;

	protected id: string;
	protected caption: string;
	protected expand: boolean;
	protected className: string;
	protected hightState: number;
	protected data: DataLabelTileGroupInfo;
	protected dataConf: ItemDataConfInfo;

	constructor(owner: Tile, data: DataLabelTileGroupInfo, dataConf: ItemDataConfInfo) {
		this.owner = owner;
		this.data = data;
		this.id = data[dataConf.idField] || null;
		this.caption = data[dataConf.captionField] || null;
		this.className = data[dataConf.classNameField] || null;
		this.expand = !!data.expand;
		this.dataConf = dataConf;
		this.initItems();
	}

	public initItems(): void {
		// todo
		if (!this.data.items) {
			return;
		}
		let items = this.data.items.sort((a, b) => a.id.localeCompare(b.id));
		this.items = items.map(e => this.createItem(e));
	}

	public createItem(data: DataLabelTileItemInfo): TileItem {
		return new TileItem(this, data, this.dataConf);
	}

	public _createDom(accept?: boolean): HTMLElement {
		if (this.domBase) {
			return this.domBase;
		}
		let domBase = this._createDomBase('group-base');
		this.createDomCaption(domBase);
		let domContainer = doc.createElement('div');
		domContainer.className = 'group-container';
		this.createDomWraper(domContainer);
		domBase.appendChild(domContainer);
		this.createDomButtons(domBase);

		accept && this.acceptDom(domBase);
		return domBase;
	}

	protected _createDomBase(className: string): HTMLElement {
		let domBase = this.domBase = doc.createElement('div');
		domBase.classList.add(className);
		return domBase;
	}

	protected createDomCaption(domBase: HTMLElement): HTMLElement {
		let domCaption = this.domCaption = doc.createElement('span');
		domCaption.classList.add('group-caption');
		domBase.appendChild(domCaption);
		return domCaption;
	}

	protected createDomAddLabelBtn(domBase) {
		let self = this;
		let groupId = this.id;
		let groupCaption = this.caption;
		let domLabelButton = this.domLabelButton = doc.createElement('div');
		domLabelButton.classList.add('group-addLabels-button');
		let modelIdOrPath = this.owner.dataProvider.modelIdOrPath;
		this.addlabelBtn = new Button({
			id: "addLabelBtn",
			icon: "icon-add",
			domParent: domBase,
			className: "labelDisplay-addLabel",
			onclick: (e: SZEvent, btn: Button) => {
				let domFileViewer = domBase.closest('.metafileviewer-base');
				let fileViewer: MetaFileViewer = domFileViewer.szobject as MetaFileViewer;
				let userInfo = getCurrentUser().userInfo;
				let dimData = userInfo["_dimData_"];
				let orgInfo = dimData["ORG_ID"];
				let isAdmin = getCurrentUser().isAdmin;
				getDwTableDataManager().getDwDataLabelLib(modelIdOrPath).then(lib => {
					let libCode = lib.code;
					let own_dept = userInfo.deptId;
					showFormDialog({
						id: "customComp.addLabel",
						className: "customAddLabelForm",
						caption: "新建标签",
						content: {
							items: [{
								id: "labelLibCode",
								caption: '标签库ID',
								formItemType: 'edit',
								visible: false
							}, {
								id: "labelCode",
								caption: "标签id",
								formItemType: 'edit',
								visible: false
							}, {
								id: "labelName",
								caption: "标签名称",
								formItemType: 'edit',
								required: true
							}, {
								id: "labelCatalog",
								caption: "标签分组",
								formItemType: "edit",
								visible: false
							}, {
								id: "labelType",
								caption: "打标方式",
								formItemType: 'selectpanel',
								required: true,
								compArgs: {
									items: [{
										value: "1",
										caption: "软标签(通过筛选条件标记)"
									}, {
										value: "2",
										caption: "硬标签(通过人工挑选、匹配excel、关联数据表标记)"
									}],
									direction: "column"
								}
							}, {
								id: 'desc',
								caption: "描述",
								formItemType: "textarea"
							}, {
								id: "ownDept",
								caption: "归口管理部门",
								formItemType: "edit",
								visible: false
							}, {
								id: "labelLevel",
								caption: "层级",
								formItemType: 'edit',
								visible: false
							}]
						},
						onshow: (event: SZEvent, dialog: Dialog) => {
							let form = <Form>dialog.content;
							form.waitInit().then(() => {
								rc({
									url: `/BQGL/app/label.app/data-label/dataLabelMgr.action?method=getNextLabelId`,
									method: 'POST'
								}).then(result => {
									let labelId = result;
									let labelIdComp = form.getFormItemComp('labelCode');
									labelIdComp.setValue(labelId);
								})
							})

							let level = (orgInfo && (orgInfo["SZ_LEVEL"] == '0' || isAdmin)) ? '市' : '区';
							form.loadData({ 'labelLibCode': libCode, 'labelCatalog': groupId, labelType: "1", ownDept: own_dept, labelLevel: level })
						},
						buttons: [{
							id: "ok",
							layoutTheme: "defbtn",
							onclick: (event: SZEvent, dialog: Dialog, item: any) => {
								let form = <Form>dialog.content;
								let validate = form.validate();
								if (validate) {
									let data = form.getData();
									let row = [];
									row.push(data.labelLibCode);
									row.push(data.labelCode);
									row.push(data.labelName);
									row.push(data.desc);
									row.push(data.labelCatalog);
									row.push(data.labelType);
									row.push(userInfo.userId);
									row.push(new Date().getTime())
									row.push(data.ownDept);
									row.push("1");
									let datapackage = {
										addRows: [{
											fieldNames: ['LABEL_LIB_CODE', 'LABEL_CODE', 'LABEL_NAME', 'LABEL_DESC', 'LABEL_CATEGORY', 'LABEL_TYPE', 'CREATOR', 'CREATE_TIME', 'MANAGMENT_DEPT', 'IS_SHARE'],
											rows: [row]
										}]
									};
									rc({
										method: "POST",
										url: "/sysdata/extensions/succ-dataVisualization-dataLabelDisplay/dataLabelItem.action?method=updateDwLabelTable",
										data: {
											data: datapackage
										}
									}).then(() => {
										dialog.close();
										showSuccessMessage('保存标签成功');
										self.owner.dataProvider.setData([]);
										self.owner.refresh();
										refreshModelState("/sysdata/data/tables/dw/DW_DATA_LABELS.tbl");
									})
								}
							}
						}, 'cancel']
					})
				})
			}
		});
		(!this.owner.allowAddLabel || !this.owner.allowEditLabel) && this.addlabelBtn.setVisible(false);
	}

	protected createDomWraper(domBase: HTMLElement): HTMLElement {
		let domWraper = this.domWraper = doc.createElement('span');
		domWraper.classList.add('group-wraper');
		let items = this.items;
		let self = this;
		this.createDomAddLabelBtn(domWraper);
		// todo 
		if (!!items) {
			for (let i = 0, len = items.length; i < len; i++) {
				let domItem = items[i]._createDom();
				domWraper.appendChild(domItem);
			}
		}
		this.domBase.appendChild(domWraper);
		return domWraper;
	}

	protected createDomButtons(domBase: HTMLElement): HTMLElement {
		let domButtons = doc.createElement('div');
		domButtons.classList.add('group-buttons');

		this.expandBtn = new Button({
			id: "expandBtn",
			icon: "icon-arrow",
			domParent: domButtons,
			visible: false,
			onclick: (sze: SZEvent, btn: Button) => {
				this.expandOrCollapse(true);
			}
		});

		this.collapseBtn = new Button({
			id: "collapseBtn",
			icon: "icon-arrow",
			domParent: domButtons,
			visible: false,
			onclick: (sze: SZEvent, btn: Button) => {
				this.expandOrCollapse(false);
			}
		});


		domBase.appendChild(domButtons);
		return domButtons;
	}

	protected expandOrCollapse(expand: boolean): void {
		this.expand = expand;
		if (expand) {//展开
			this.domBase.style.height = "100%";
			this.expandBtn.setVisible(false);
			this.collapseBtn.setVisible(true);

		} else {//收起
			this.domBase.style.height = `${this.owner.itemHeight + 30 + 1}px`;
			this.collapseBtn.setVisible(false);
			this.expandBtn.setVisible(true);
		}
		this.owner._updateScrollBar(false, false);
	}

	public acceptDom(dom: HTMLElement): void {
		this.domBase = dom;
		let domCaption = this.domCaption;
		if (domCaption) {
			domCaption.textContent = this.caption;
		}
		if (dom.id != '' || this.id) {
			dom.id = this.id;
		}
		this.className && dom.classList.add(this.className);
		const items = this.items;
		const childNodes = this.domWraper && this.domWraper.childNodes;
		// todo
		if (!!items) {
			for (let i = 0, len = items.length; i < len; i++) {
				items[i].acceptDom(<HTMLElement>childNodes[i + 1]);
			}
		}
	}

	public checkShowBtn() {
		if (this.expandBtn && this.collapseBtn) {
			let offsetHieght = this.domWraper.scrollHeight;
			if (offsetHieght >= 2 * this.owner.itemHeight) {
				this.expandOrCollapse(this.expand);
			} else {
				this.expandBtn.setVisible(false);
				this.collapseBtn.setVisible(false);
			}

		}
	}

	public getId(): string {
		return this.id;
	}

	public getCaption(): string {
		return this.caption;
	}

	public getData(): DataLabelTileGroupInfo {
		return this.data;
	}

	public setId(v: string): void {
		if (this.id !== v) {
			this.id = v;
			this.domBase && (this.domBase.id = v);
		}
	}

	public setCaption(v: string): void {
		if (v != this.caption) {
			this.caption = v;
			let domCaption = this.domCaption;
			domCaption && (domCaption.textContent = v == null ? "" : v);
		}
	}

	public setClassName(className: string): void {
		let oldClassName = this.className;
		if (oldClassName != className) {
			const domBase = this.domBase;
			if (domBase) {
				oldClassName && domBase.classList.remove(oldClassName);
				className && domBase.classList.add(className);
			}
			this.className = className;
		}
	}

	public setData(data: DataLabelTileGroupInfo) {
		let dataConf = this.dataConf;
		this.data = data;
		this.setId(data[dataConf.idField] || null);
		this.setCaption(data[dataConf.captionField]);
		dataConf.classNameField !== undefined && this.setClassName(data[dataConf.classNameField]);

		let items = this.items;
		for (let i = 0, len = items.length; i < len; i++) {
			items[i].setData(data.items[i]);
		}
	}

	public releaseDom(): HTMLElement {
		let items = this.items;
		for (let i = 1, len = items.length; i < len; i++) {
			items[i].releaseDom();
		}
		let dom = this.domBase;
		if (dom) {
			this.domBase = null;
			this.domCaption = null;
			this.expandBtn = null;
			this.collapseBtn = null;
			this.className && dom.classList.remove(this.className);
		}
		return dom;
	}

	public dispose(): void {
		this.expandBtn && this.expandBtn.dispose();
		this.collapseBtn && this.collapseBtn.dispose();
		let items = this.items;
		if (items) {
			for (let i = 0, len = items.length; i < len; i++) {
				items[i].dispose();
			}
			items = null;
		}
		for (let p in this) {
			if (this.hasOwnProperty(p)) {
				this[p] = null;
			}
		}
	}

	public getItem(index: number): TileItem {
		return this.items[index];
	}

	public getItemById(id: string): TileItem {
		return this.items.find(i => i.getId() === id);
	}

}

export interface DataLabelInputArgs extends ComponentArgs {
	/**
	 * 是否显示下拉面板，默认显示
	 */
	dropPanelVisible?: boolean;
	/**
	 * 是否显示边框，默认显示
	 */
	borderVisible?: boolean;
	/**
	 * 提供搜索结果
	 */
	dataProvider?: IDataProvider<JSONObject>;
	/**
	 * 定义数据格式的信息
	 */
	itemDataConf?: ItemDataConfInfo;
	/**
	 * 是否显示下拉箭头，如果显示，点击下拉箭头时显示下拉框内容；如果不显示，只有在输入框输入内容搜索时才显示下拉框内容
	 */
	arrowVisible?: boolean;
	/**
	 * 是否显示删除按钮，有些可能不需要在控件上删除，而是有自己管理的地方，
	 * 例如表单全局按钮的管理就是在对话框中管理的，不需要在输入框中删除，默认显示
	 */
	removeVisible?: boolean;
	/**
	 * 是否展示控件value的title，默认false
	 */
	showValueTitle?: boolean;
	/**
	 * 是否自动展开，默认false
	 */
	autoExpand?: boolean;
	/**
	 * 多值输入框的值
	 */
	value?: Array<JSONObject>;
	/**
	 * 值改变后触发的事件
	 */
	onchange?: SZEventCallback;
}


export class DataLabelInput extends Component {
	/**
	 * 是否显示下拉面板，默认显示
	 */
	private dropPanelVisible: boolean;
	/**
	 * 是否显示边框
	 */
	private borderVisible: boolean;
	/**
	 * 提供搜索结果，存在dataProvider则可以搜索，否则不能搜索
	 */
	private dataProvider: IDataProvider<JSONObject>;
	/**
	 * 定义数据格式的信息
	 */
	private itemDataConf?: ItemDataConfInfo;
	/**
	 * 是否显示下拉箭头，默认显示，如果显示，点击下拉箭头时显示下拉框内容；如果不显示，只有在输入框输入内容搜索时才显示下拉框内容
	 */
	private arrowVisible?: boolean;
	/**
	 * 是否显示删除按钮，有些可能不需要在控件上删除，而是有自己管理的地方，
	 * 例如表单全局按钮的管理就是在对话框中管理的，不需要在输入框中删除，默认显示
	 */
	private removeVisible?: boolean;
	/**
	 * 值改变后触发的事件
	 */
	public onchange?: SZEventCallback;
	/**
	 * 是否展示控件value的title，默认false
	 */
	private showValueTitle?: boolean;

	public domArrow?: HTMLElement;

	/**
	 * 选中的所有节点
	 */
	private domSelectedItems?: HTMLElement[];

	private items: Map<string, HTMLElement>;

	private value: Array<string>;

	private autoExpand: boolean;

	/**
	 * 记录列表显示的位置，隐藏在拾取过程中输入框高度改变，外部容器高度发生改变，
	 * 此时会触发滚动事件，导致浮动面板隐藏。
	 * 根据彭文的建议，如果隐藏了，那么再显示出来，此时应该还是显示在之前的位置上，
	 * 避免浮动面板改变位置后影响使用者拾取
	 */
	private position: { left: number, top: number };

	/**
	 * 如果拾取内容过程中导致当前控件高度发生改变，此时会触发滚动事件导致列表隐藏了，
	 * 此时应该是要显示的，设置为true，在隐藏的事件中通过该参数阻止列表面板隐藏
	 */
	private needShow: boolean;

	public tile: Tile;

	constructor(args: DataLabelInputArgs) {
		super(args);
	}

	protected _init_default(args: DataLabelInputArgs): void {
		super._init_default(args);
		this.arrowVisible = args.arrowVisible;
		this.itemDataConf = Object.assign({}, DEF_ITEM_DATA_CONF, args.itemDataConf);
		this.removeVisible = args.removeVisible !== void 0 ? args.removeVisible : true;
		this.onchange = args.onchange;
		this.value = [];
		this.domSelectedItems = [];
		this.dropPanelVisible = args.dropPanelVisible !== false;
		this.borderVisible = args.borderVisible !== false;
		this.showValueTitle = args.showValueTitle === true;
		this.dataProvider = args.dataProvider;
		this.autoExpand = args.autoExpand === true;
	}

	protected _init(args: DataLabelInputArgs): HTMLElement {
		let domBase = super._init(args);
		domBase.className = "datalabelinput-base";
		domBase.setAttribute('tabindex', '0');
		// 20190115 guob 改用mousedown，因为可以在mousedown中通过组织默认事件来避免触发blur事件导致的无法多选问题，只有鼠标真正的离开控件后才触发blur事件
		domBase.onmousedown = this.doMouseDown.bind(this);
		this.borderVisible || domBase.classList.add("datalabelinput-noneborder");
		domBase.onfocus = () => this.isEnabled() && this.borderVisible && domBase.classList.add('datalabelinput-focus');
		domBase.onblur = this.doBlur.bind(this);

		if (this.arrowVisible !== false) {
			domBase.classList.add("datalabelinput-showarrow");
			let domArrow = this.domArrow;
			if (!domArrow) {
				domArrow = this.domArrow = doc.createElement("div");
				domArrow.className = "datalabelinput-arrow icon-lowtrip";
				domBase.appendChild(domArrow);
			}
		}

		//this.setDataProvider(args.dataProvider);

		//this.setValue(args.value);
		return domBase;

	}

	private initTile(): Tile {
		let tile = new Tile({
			layoutTheme: "multipleinputlist",
			visible: false,
			autoExpand: this.autoExpand,
			dataProvider: this['dataProvider'],
			onhide: this.doHideList.bind(this),
			onclick: this.doClick.bind(this)
		});

		return this.tile = tile;
	}


	public dispose(): void {
		let domBase = this.domBase;
		if (domBase) {
			domBase.onmousedown = null;
			domBase.onfocus = null;
			domBase.onblur = null;
			this.tile && this.tile.dispose();
		}

		super.dispose();
	}

	/**
	 * 获取焦点的时候让输入框也获取焦点并设置获取焦点的样式
	 */
	public focus(): void {
		super.focus();
		this.domBase && this.domBase.classList.add('datalabelinput-focus');
	}

	/**
	 * 设置dataprovider
	 * @param dp
	 */
	public setDataProvider(dp: IDataProvider<JSONObject>): Promise<void> {
		this.dataProvider = dp;
		if (!dp) {
			return Promise.resolve();
		}
		let tile = this.tile;
		if (tile) {
			return tile.setDataProvider(dp);
		}
		return Promise.resolve();
	}

	/**
	 * 隐藏列表时触发的事件，确定是否要隐藏列表
	 */
	private doHideList(): boolean | void {
		let needShow = this.needShow;
		this.needShow = false;
		let pos = this.position;
		this.position = null;
		if (needShow) {
			// 虽然阻止了面板隐藏，但是却删除了一些事件导致无法隐藏面板，通过showFloat将这些事件再注册回来
			requestAnimationFrame(() => {
				this.tile && this.tile.showFloat({
					showAt: pos || this.domBase
				}).then(() => {
					if (pos) {
						let domBaseStyle = this.tile.domBase.style;
						domBaseStyle.top = pos.top + 'px';
						domBaseStyle.left = pos.left + 'px';
						domBaseStyle.width = this.domBase.offsetWidth + "px";
					}
				});
			});
			return false;
		}
	}

	private doClick(sze: SZEvent, tile: Tile, item: TileItem) {
		let data = item.getData();
		let newValue = data[this.itemDataConf.valueField];
		if (!data || !newValue) {
			return;
		}
		let oldValue: string[] = clone(this.value);
		if (!oldValue.includes(newValue)) {
			this.addValue(data);
		}
		this.tile.setVisible(false);
	}

	private doBlur(): void {
		let tile = this.tile;
		if (tile && tile.isVisible()) {
			return;
		}
		this.domBase.classList.remove('datalabelinput-focus');
		this.clearSelected();
	}

	/**
	 * 点击选中节点
	 * @param event
	 */
	private doMouseDown(event: MouseEvent): void {
		if (!this.isEnabled()) {
			return;
		}
		let domTarget = <HTMLElement>event.target;
		let classList = domTarget.classList;
		if (classList.contains("datalabelinput-delete")) {
			this.deleteBlock(domTarget.parentElement);
		} else if (this.isSelectBlock(domTarget)) {
			this.selectBlock(domTarget);
		} else if (classList.contains('datalabelinput-arrow') || classList.contains('datalabelinput-base')) {
			this.clearSelected();
			this.showFloatPanel();
		} else {
			this.selectBlock();
		}

		/**
		 * 20190228 guob ISSUE:BI-24519
		 * 问题原因：下拉面板显示时在body上监控了mousedown事件并设置了autoHideComponent，当前事件冒泡到body上了导致又被隐藏了
		 * 解决方案：阻止事件冒泡到body上
		 */
		this.dropPanelVisible && event.stopPropagation();
	}

	/**
	 * 是否点击在了文字块上了，如果是，那么需要选中该文字块
	 */
	private isSelectBlock(dom: Element): boolean {
		if (!dom) {
			return false;
		}
		let classList = dom.classList;
		return classList.contains("datalabelinput-block") || classList.contains("datalabelinput-block-desc") || classList.contains("datalabelinput-block-img");
	}

	/**
	 * 删除文字块
	 */
	private deleteBlock(domTarget: HTMLElement): void {
		let oldValue = clone(this.value);
		this.deleteDomTarget(domTarget) && this.onchange && this.onchange({
			srcEvent: event,
			component: this,
			value: this.value
		}, this);
	}

	/**
	 * 删除目标DOM节点
	 */
	private deleteDomTarget(domTarget: HTMLElement): boolean {
		if (!domTarget) {
			return false;
		}
		let domItems = this.getItems()
		for (let [k, domItem] of domItems) {
			if (domItem === domTarget) {
				this.deleteValue(k);
				return true;
			}
		}
		return false;
	}

	/**
	 * 选中指定的文字块
	 * @param domTarget 被选中的DOM
	 *
	 */
	private selectBlock(domTarget?: HTMLElement): void {
		domTarget = this.isSelectBlock(domTarget) ? <HTMLElement>domTarget.closest('.datalabelinput-block') : null;
		let domSelectedItems = this.domSelectedItems;
		this.clearSelected();
		if (!domTarget) {
			return;
		}
		let classList = domTarget.classList;
		classList.add("datalabelinput-selected");
		domSelectedItems.distinctPush(domTarget);
	}

	/**
	 * 清空所有选中的节点
	 */
	private clearSelected(): void {
		let domSelectedItems = this.domSelectedItems;
		domSelectedItems.forEach(domItem => { domItem.classList.remove("datalabelinput-selected"); });
		domSelectedItems.length = 0;
	}

	/**
	 * 获取值和文字块节点的映射
	 */
	private getItems(): Map<string, HTMLElement> {
		let items = this.items;
		if (!items) {
			items = this.items = new Map<string, HTMLElement>();
		}
		return items;
	}

	/**
	 * 清空所有值
	 */
	public clean(): void {
		let items = this.getItems();
		items.forEach((domItem, k) => { domItem.remove(); });
		this.value.length = 0;
		items.clear();
	}

	/**
	 * 删除值
	 * @param v
	 */
	public deleteValue(v: string): void {
		if (!v) {
			return;
		}
		let items = this.getItems();
		let domBlock = items.get(v);
		if (!domBlock) {
			return;
		}
		let domSelectedItems = this.domSelectedItems;
		domSelectedItems && domSelectedItems.remove(domBlock);
		this.value.remove(v);
		items.delete(v);
		domBlock.remove();
	}

	/**
	 * 设置值，v不存在时清空输入框。
	 * @param v
	 */
	public setValue(v: Array<JSONObject>): Promise<void> {
		if (!v || v.length == 0) {
			this.clean();
			return Promise.resolve();
		}
		let itemDataConf = this.itemDataConf;
		let valueField = itemDataConf.valueField;
		let captionField = itemDataConf.captionField;
		let eqValues = v.map((info: JSONObject) => info[valueField]);
		let eqCaptions = v.map((info: JSONObject) => info[captionField]);
		if (deepEqual(this.getValue(), eqValues) && deepEqual(this.getText(), eqCaptions)) {
			return Promise.resolve();
		}
		this.clean();
		this.value = v.map((v: JSONObject) => v[valueField]);
		return Promise.resolve(v.forEach((info) => { this.addValue(<JSONObject>info) }));

	}

	/**
	 * 获取所有文字块的标题
	 */
	public getText(): Array<string> {
		let captions: Array<string> = [];
		let items = this.getItems();
		this.value.forEach((v) => {
			let item = items.get(v);
			captions.push(item ? item.textContent : v);
		})
		return captions;
	}

	/**
	 * 获取值
	 */
	public getValue(): Array<string> {
		return this.value;
	}

	/**
	 * 增加值
	 * @param v
	 */
	public addValue(v: DataLabelTileItemInfo): boolean {
		if (!v) {
			return false;
		}
		const doc = document;
		let itemDataConf = this.itemDataConf;
		let dataValue = v[itemDataConf.valueField];
		if (!dataValue) {
			return false;
		}
		let value = clone(this.value);
		if (value && value.length !== 0 && this.getItems().has(dataValue)) {
			return false;
		}
		let dataCaption = v[itemDataConf.captionField];
		let image = v[itemDataConf.imageField];
		let domBlock = doc.createElement("span");
		domBlock.className = `datalabelinput-block ${v.className ? v.className : ''}`;
		v.width && (domBlock.style.width = v.width + "px");
		let desc = dataCaption || dataValue || "";
		//设置了展示所有item的title，就不要再单独展示单个的title了
		this.showValueTitle || (domBlock.title = desc.escapeHTML());
		if (image) {
			let domBlockImg = doc.createElement("span");
			domBlockImg.className = "datalabelinput-block-img";
			domBlock.appendChild(domBlockImg);
			// DATA-URL方式无法显示SVG内嵌的<image>, 这里还是直接把内容丢到dom结构里
			//setIcon(domBlockImg, image, true);
			domBlockImg.innerHTML = image;
		} else {
			let domBlockDesc = doc.createElement("span");
			domBlockDesc.className = "datalabelinput-block-desc";
			domBlockDesc.textContent = desc;
			let color = v['color'];
			if (color) {
				domBlock.style.borderColor = color;
				domBlockDesc.style.color = color;
			}
			domBlock.appendChild(domBlockDesc);
		}

		let domBase = this.domBase;
		domBase.appendChild(domBlock);

		if (this.removeVisible) {
			let domDelete = doc.createElement("span");
			domDelete.className = "datalabelinput-delete icon-close-small";
			domBlock.appendChild(domDelete);
		}

		this.getItems().set(dataValue, domBlock);
		this.value.distinctPush(dataValue);
		this.showValueTitle && (this.domBase.title = this.getText().map(m => `[${m}]`).join("，").escapeHTML());

		this.onchange && this.onchange({
			srcEvent: event,
			component: this,
			value: this.value
		}, this);

		return true;
	}

	private showFloatPanel() {
		if (!this.dropPanelVisible) {
			return;
		}
		let dataProvider = this.dataProvider;
		if (!dataProvider) {
			return;
		}
		let tile = this.tile;
		let domBase = this.domBase;
		let rect = domBase.getBoundingClientRect();
		let pos = this.position = { left: rect.left, top: rect.top + rect.height };
		let show = () => {
			let items = tile.getItems();
			if (!items || !items.length) {
				return tile.setVisible(false);
			} else {
				tile.showFloat({
					showAt: this.domBase,
					crossXY: false,
					limitInWidow: ShowFloatLimitMode.LimitSize
				}).then(() => {
					let domBaseStyle = tile.domBase.style;
					domBaseStyle.top = pos.top + 'px';
					domBaseStyle.left = pos.left + 'px';
					domBaseStyle.width = domBase.offsetWidth + "px";
					tile.checkShowBtn();
				});
			}
		}
		if (!tile) {
			tile = this.tile = this.initTile();
			return tile.refresh().then(() => {
				return show();
			});
		}

		return show();
	}

	public refresh() {
		if (!this.dropPanelVisible) {
			return;
		}
		let dataProvider = this.dataProvider;
		if (!dataProvider) {
			return;
		}
		let tile = this.tile;
		if (!tile) {
			tile = this.tile = this.initTile();
		}
		return tile.refresh();
	}
}

export class DataLabelInput2 extends Component {
	private input: DataLabelInput;
	private tile: Tile;
	private splitline: DataLabelSplitLine;
	public dataProvider: DataProvider<JSONObject>;
	public onchange?: SZEventCallback;
	public itemDataConf: ItemDataConfInfo;
	public allowEditLabel?: boolean;

	constructor(args) {
		super(args);
	}

	protected _init_default(args): void {
		super._init_default(args);
		this.dataProvider = args.dataProvider;
		this.onchange = args.onchange;
		this.allowEditLabel = args.allowEditLabel;
		this.itemDataConf = Object.assign({}, DEF_ITEM_DATA_CONF, args.itemDataConf);
	}

	protected _init(args): HTMLElement {
		let domBase = super._init(args);

		this.tile = new Tile({
			domParent: domBase,
			dataProvider: this.dataProvider,
			itemDataConf: this.itemDataConf,
			allowEditLabel: this.allowEditLabel,
			onclick: (sze: SZEvent, tile: Tile, item: TileItem) => {
				let data = item.getData();
				let newValue = data[this.itemDataConf.valueField];
				if (!data || !newValue) {
					return;
				}
				let oldValue: string[] = clone(this.input.getValue());
				if (!oldValue.includes(newValue)) {
					this.input.addValue(data);
				}

			}
		});

		let domSplit = document.createElement('div');
		domSplit.className = 'datalabelsplit';
		domBase.appendChild(domSplit);
		this.splitline = new DataLabelSplitLine({
			domParent: domSplit
		})

		let domContent = document.createElement('div');
		domContent.className = "datalabelinput2-content";
		let domTitle = document.createElement('div');
		domTitle.className = "datalabelinput2-title";
		domTitle.textContent = "标签过滤：";
		domContent.appendChild(domTitle);

		this.input = new DataLabelInput({
			arrowVisible: false,
			dropPanelVisible: false,
			borderVisible: false,
			domParent: domContent,
			dataProvider: this.dataProvider,
			itemDataConf: this.itemDataConf,
			onchange: (sze: SZEvent, input: DataLabelInput) => {
				this.onchange && this.onchange(sze, this);
			}
		});

		domBase.appendChild(domContent);

		return domBase;
	}

	public getTileHeight() {
		let tile = this.tile.getDomBase();
		return tile.offsetHeight;
	}

	public setSplitlineResizeComp(params: JSONObject) {
		let currentComp = params.currentComp;
		let downComp = params.downComp;
		this.splitline && this.splitline.setTogglnBtnVisible(true);
		this.splitline.setToggleBtnTop(this.getTileHeight() + "px")
		this.splitline && this.splitline.setResizeComp({ upComp: currentComp, downComp: downComp })
	}

	public setDataProvider(dp: DataLabelDataProvider) {
		this.input.setDataProvider(dp);
		this.tile.setDataProvider(dp);
	}

	public refresh() {
		return this.tile.refresh();
	}

	public clean() {
		this.input.clean();
	}

	public setShowEditBtn(allow: boolean) {
		this.tile.setAllow(allow);
	}

	public setShowAddBtn(allow: boolean) {
		this.tile.setShowAddBtn(allow);
	}

	public dispose(): void {
		if (this.domBase) {
			this.tile && this.tile.dispose();
			this.input && this.input.dispose();
			this.dataProvider = null;
			this.onchange = null;
			this.itemDataConf = null;
		}
		super.dispose();
	}
}

export class DataLabelDataProvider extends InputTreeDataProvider {
	private modelIdOrPath: string;

	private loadlabelDatas(): Promise<DataLabelTileItemInfo[]> {
		return getDataLabelImage(null, this.modelIdOrPath);
	}

	public fetchData(): Promise<DataLabelTileGroupInfo[]> {
		let compDef = <any>this.compBuilder;
		let modelIdOrPath: string = compDef.getModel();
		if (!modelIdOrPath) {
			//showErrorMessage("没有设置模型表id或者路径");
			return Promise.resolve(this.data);
		}

		if (!isEmpty(this.data) && modelIdOrPath == this.modelIdOrPath) {
			return Promise.resolve(this.data);
		}
		this.modelIdOrPath = modelIdOrPath;
		return this.loadlabelDatas().then(labelDatas => {
			// if (isEmpty(labelDatas)) {
			// 	return Promise.resolve([]);
			// }
			return getDwTableDataManager().getDwDataHierarchyProvider({ dwTablePath: modelIdOrPath, pseudoDim: "$LABELS" }).then(dp => {
				return dp.fetchAllData().then(() => {
					let roots = dp.childrenMap && dp.childrenMap["_root_"];
					if (isEmpty(roots)) {
						return [];
					}
					let datas: DataLabelTileGroupInfo[] = [];
					for (let key in dp.itemMap) {
						let info: DataLabelTileGroupInfo = {};
						let id = info.id = key;
						let rootItem = dp.itemMap[id];
						info.caption = rootItem.getCaption();
						if (rootItem.getLevel() == 0) {
							continue;
						}
						info.items = labelDatas.filter(f => f.category == id);
						datas.push(info);
					}
					/* roots.forEach(e => {
						let info: DataLabelTileGroupInfo = {};
						let id = info.id = e;
						let rootItem = dp.itemMap[id];
						info.caption = rootItem.getCaption();
						info.items = labelDatas.filter(f => f.category == id);
						datas.push(info);
					}); */
					// todo 
					return getDwTableDataManager().getDwDataLabelLib(modelIdOrPath).then((libResult: DwDataLabelLib) => { // 查询当前所选标签库的父ID
						let libCode: string = libResult.code;
						return rc({
							url: `/sysdata/extensions/succ-dataVisualization-dataLabelDisplay/dataLabelItem.action?method=queryGroupNodeInfos`,
							method: "POST",
							data: {
								libCode: libCode
							}
						}).then((queryData: DataLabelTileGroupInfo[]) => {
							for (let i = 0; i < queryData.length; i++) {
								let info: DataLabelTileGroupInfo = {
									id: queryData[i]["CODE"],
									caption: queryData[i]["CAPTION"]
								}
								let nodeNoExist: boolean = true;
								for (let dataIndex = 0; dataIndex < datas.length; dataIndex++) {
									if (datas[dataIndex].id == info.id) { // 校验节点是否已经存在data中
										nodeNoExist = false;
										if (!datas[dataIndex].items || datas[dataIndex].items.length == 0) { // 校验当前节点是否存在items（子节点）属性
											datas[dataIndex].isGroupNode = true;
										}
										break;
									}
								}
								if (nodeNoExist) { // 新增的节点都不存在子节点
									info.isGroupNode = true;
									datas.push(info);
								}
							}
							this.data = datas;
							return this.data;
						})
					});
				})
			})
		})
	}
}

/**
 * 标签选择控件data数据格式
 */
export interface DataLabelTileGroupInfo extends JSONObject {
	/** 标签编码 */
	id?: string;
	/** 标签题目 */
	caption?: string;
	/** 是否分组节点，且没有子节点 */
	isGroupNode?: boolean;
	items?: Array<DataLabelTileItemInfo>
}

export interface DataLabelTileItemInfo extends JSONObject {
	id?: string;
	value?: string;
	caption?: string;
	image?: string;
	width?: number;
	heigth?: number;
	className?: string;
	category?: string;
}

/**
 * 获取标签svg图片信息
 * @param id 标签id
 * @param modelIdOrPath 模型id或路径
 */
export function getDataLabelImage(id?: string, modelIdOrPath?: string): Promise<DataLabelTileItemInfo[]> {
	return rc({
		method: "POST",
		url: "/sysdata/extensions/succ-dataVisualization-dataLabelDisplay/dataLabelItem.action?method=getDataLabelItem",
		data: {
			id: id,
			modelIdOrPath: modelIdOrPath
		}
	}).then(rs => {
		if (rs == null) {
			return null;
		}
		let infos = [];
		(<Array<any>>rs).forEach(e => {
			typeof e === 'string' && (e = JSON.parse(e));
			if (e == null) {
				return;
			}
			infos.push({
				id: e.id,
				value: e.id,
				desc: e.desc,
				caption: e.caption,
				height: e.height,
				width: e.width,
				// DATA-URL方式无法显示SVG内嵌的<image>, 这里还是直接把内容丢到dom结构里
				// image: `data:image/svg+xml;base64,${window.btoa(unescape(encodeURIComponent(rs.svg)))}`
				image: e.svg,
				className: e.className,
				category: e.category,
				own_dept: e.own_dept,
				label_level: e.label_level,
				allow_edit: e.allow_edit,
				label_type: e.label_type
			})
		});
		return infos;
	})
}


/**
 * @date 20210423
 * @author liuyongz
 * 标签栏分割条控件，用于隐藏上方标签选择栏
 */
export class DataLabelSplitLine extends Component {
	public togglnBtn: HTMLElement;
	public expand: boolean;
	public upComp: Component;
	public downComp: Component;
	public top: string;
	public resizeCompInfos: Array<{ comp: Component, width: number, height: number, minWidth: number, minHeight: number }>;
	protected _init(args): HTMLElement {
		let domBase = super._init(args);
		let lineBar = document.createElement('div');
		lineBar.classList.add('datalabelsplitline-linebar');
		domBase.appendChild(lineBar);
		let btn = this.togglnBtn = document.createElement('div');
		btn.classList.add('datalabelsplitline-togglebtn', 'icon-greater', 'previous');
		btn.style.display = "none";
		btn.onclick = this.doClick.bind(this);
		domBase.appendChild(btn);
		this.expand = true;
		return domBase;
	}

	public setTogglnBtnVisible(visibile: boolean) {
		let display = visibile ? "" : "none";
		this.togglnBtn.style.display = display;
	}

	public doClick(event) {
		let resizeCompInfos = this.resizeCompInfos;
		if (!resizeCompInfos) {
			return;
		}
		let item0 = resizeCompInfos[0], item1 = resizeCompInfos[1];
		let dom0 = item0.comp.getDomBase(), dom1 = item1.comp.getDomBase();
		let domTile = item0.comp.getInnerComoponent().tile.getDomBase().style;
		let itemStyle0 = dom0.style, itemStyle1 = dom1.style;
		let expand = this.expand = !this.expand;
		let getValue = (data: JSONObject, key: string): string => {
			return typeof data[key] === 'string' ? data[key] : `${data[key]}px`;
		}
		if (!expand) {
			Object.assign(domTile, { height: '0px', minHeight: '0px', flexGrow: domTile.flexGrow ? 0 : '' });
			Object.assign(itemStyle1, { height: `100%` });
			this.togglnBtn.style.top = "";
		} else {
			Object.assign(itemStyle1, { height: "100%" });//门户的总高度是动态的，不要给固定值
			Object.assign(domTile, { height: 'auto', minHeight: null });
			this.togglnBtn.style.top = this.top;
		}
		dispatchResizeEvent(dom0, true);
		dispatchResizeEvent(dom1, true);
		item0.comp.getInnerComoponent().tile.setVisible(expand);
		this.togglnBtn.classList.toggle("previous", expand);
	}

	public setToggleBtnTop(top: string) {
		this.top = top;
		this.togglnBtn.style.top = top;
	}

	public setResizeComp(params: JSONObject) {
		let upComp = this.upComp = params.upComp;
		let downComp = this.downComp = params.downComp;
		if (upComp != null && downComp != null) {
			let domBase0 = upComp.getDomBase(), domBase1 = downComp.getDomBase();
			let style0 = domBase0.style, style1 = domBase1.style;
			let width0 = style0.width !== '0px' ? domBase0.offsetWidth : upComp.getCompBuilder().getProperty(PropertyNames.Width).value;
			let height0 = style0.height !== '0px' ? domBase0.offsetHeight : upComp.getCompBuilder().getProperty(PropertyNames.Height).value;
			let width1 = style1.width !== '0px' ? domBase1.offsetWidth : downComp.getCompBuilder().getProperty(PropertyNames.Width).value;
			let height1 = style1.height !== '0px' ? domBase1.offsetHeight : downComp.getCompBuilder().getProperty(PropertyNames.Height).value;
			let temp0 = domBase0.offsetWidth - domBase0.clientWidth, temp1 = domBase0.offsetHeight - domBase0.clientHeight;
			height1 -= temp1;
			this.resizeCompInfos = [
				{ comp: upComp, width: width0, height: height0, minWidth: parseInt(style0.minWidth || '0'), minHeight: parseInt(style0.minHeight || '0') },
				{ comp: downComp, width: width1, height: height1, minWidth: parseInt(style1.minWidth || '0'), minHeight: parseInt(style1.minHeight || '0') },
			]
		}
	}
}