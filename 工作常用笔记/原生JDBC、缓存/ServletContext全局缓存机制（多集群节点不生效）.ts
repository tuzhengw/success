/**
 * =================================================
 * 作者：tuzw
 * 创建日期：20230411
 * 脚本用途: 
 *  1 数据交换接口调用频率限制, 限制同一个时间同一个用户并发数量
 * ================================================
 */
import { getDefaultDataSource } from 'svr-api/db';
import { getTimeMills } from 'svr-api/sys';
import ServletContextHolder = com.succez.commons.spring.support.context.ServletContextHolder;

/**
 * 请求任何资源时都会执行此过滤器
 * 说明：
 * 1、本扩展点执行的非常频繁，所有http请求都会调用本方法，所以脚本务必要高效
 * 2、如果脚本想重定向请求到另外的页面，可以调用`response.sendRedirect("/xxx")`跳转，然后return false。
 *
 * @returns 返回`false`将不处理`FilterChain`上的下一个Filter
 */
function onFilter(request: HttpServletRequest, response: HttpServletResponse): boolean | void {
    let dxRequestLimit = new DataExchangeRequestLimit({
        request: request,
        response: response
    });
    dxRequestLimit.interceptDxFootUrl();
}

/**
 * 数据交换接口调用频率限制
 * <p>
 *  1 servlet生命周期 = 当前服务器生命周期。
 *  2 全局缓存会记录到服务器中, 但不支持多集群节点之间的共享。
 *    (每个逻辑主机都拥有它自己的servlet context。servlet context不能跨虚拟主机共享)
 *    解决办法: 1) 部署redis; 2) 创建一张表进行记录, 表是创建在defalut库中, 不会和业务数据库有关系, 不用担心影响性能问题
 * </p>
 */
export class DataExchangeRequestLimit {

    /** 食品快检接口单次并发最大数量 */
    private FootInterfaceMaxRequestNums: number = 2;

    /** 请求对象 */
    private request: HttpServletRequest;
    /** 响应对象 */
    private response: HttpServletResponse;

    constructor(args: {
        request: HttpServletRequest,
        response: HttpServletResponse
    }) {
        this.request = args.request;
        this.response = args.response;
    }

    /**
     * 若请求URL属于数据交换, 则根据通道ID来判断是否拦截请求
     * <p>
     *  20211230 kengjt
     *  地址: https://jira.succez.com/browse/CSTM-17526
     * </p>
     */
    public interceptDxUrl(): void {
        let uri: string = this.request.getRequestURI();
        if (uri.startsWith('/api/dx/')) {
            let currentTime = new java.math.BigDecimal(getTimeMills());
            let arr: string[] = uri.split('/');
            let dxId = arr[3];
            if (dxId) {
                let lastCallTime = new java.math.BigDecimal(this.getObjectFromCache(dxId));
                this.setObjectToCache(dxId, currentTime)
                if (lastCallTime && currentTime - lastCallTime <= 1000 * 5) {
                    this.setResponseInfo(JSON.stringify({ code: 103, message: '接口调用过于频繁, 请稍后尝试' }));
                }
            }
        }
    }

    /**
     * 食品快检接口调用并发量限制
     * <p>
     *  20230411 tuzw
     *  地址: https://jira.succez.com/browse/CSTM-22580
     *  将用户ID作为key, 记录每个用户请求的信息
     * </p>
     */
    public interceptDxFootUrl(): void {
        let uri: string = this.request.getRequestURI();
        let contextPath = this.request.getContextPath(); //  示例: /HBZHSCJG
        // @ts-ignore
        if (uri.startsWith(`${contextPath}/api/dx/`)) {
            console.info(`开始执行[interceptDxFootUrl], 食品快检接口调用并发量限制`);
            let arrInfos: string[] = uri.split("?");
            if (arrInfos[0].endsWith("insertFoodInfos")) { // 仅处理食品快检接口
                let tokenValue = this.request.getParameter("access_token");
                if (!tokenValue) {
                    this.setResponseInfo(JSON.stringify({ result: false, message: "url未携带token参数" }));
                    return;
                }
                let userResult = this.queryAuthorizeUserId(tokenValue);
                if (!userResult.result) {
                    this.setResponseInfo(JSON.stringify(userResult));
                    return;
                }
                let userId: string = userResult.data;
                let currentTime = new java.math.BigDecimal(getTimeMills());
                let userRequestInfo = this.getObjectFromCache(userId) as FootInterfaceCacheInfo;
                if (!userRequestInfo) {
                    this.setObjectToCache(userId, {
                        requestNumx: 1,
                        lastCallTime: currentTime
                    });
                } else {
                    let lastCallTime = userRequestInfo.lastCallTime;
                    let requestNumx = userRequestInfo.requestNumx;
                    if (lastCallTime && currentTime - lastCallTime <= 1000 * 3) { // 判断请求是否统一时间请求(可能存在误差)
                        if (requestNumx && requestNumx > this.FootInterfaceMaxRequestNums) {
                            this.setResponseInfo(JSON.stringify({ code: 103, message: '接口调用过于频繁, 请稍后尝试' }));
                        } else { // 并发调用时间在同一时间, 但并发数量还符合, 则累计请求次数
                            this.setObjectToCache(userId, {
                                requestNumx: ++requestNumx,
                                lastCallTime: currentTime
                            });
                        }
                    } else {
                        this.setObjectToCache(userId, { // 若并发调用相差时间不在同一时间内, 则重置请求次数
                            requestNumx: 1,
                            lastCallTime: currentTime
                        });
                    }
                }
            }
        }
    }

    /**
     * 阻止指定请求的html页面
     */
    public preventHtmlPage() {
        let request = this.request;
        let response = this.response;
        let uri = request.getRequestURI();
        if (uri.indexOf('frameNest.html') !== -1) { // frameNest也需要阻止一下
            let filterType = ['personal', 'navPage', 'regPage', 'forgetPwd'];
            let param = request.getParameter('pageId');
            let city = request.getParameter('city');
            if (!param || (filterType.indexOf(param) < 0 && city)) {
                response.sendRedirect("/public/404.html")
            }
        }
        if (uri.indexOf('frameMenu.html') !== -1) {
            let param = request.getParameter('pageId');
            if (!param) {
                response.sendRedirect("/public/404.html")
            }
        }
    }

    /**
     * 设置响应信息
     * @param responseInfo 响应消息
     */
    private setResponseInfo(responseInfo: string): void {
        let response = this.response;
        let returnStr = new java.lang.String(responseInfo);
        response.setHeader('Content-Type', 'application/json; charset=utf-8')
        let out = response.getOutputStream();
        out.write(returnStr.getBytes());
        out.close();
    }

    /**
     * 从缓存中获取对象
     * @params key 属性key
     * @return 
     */
    public getObjectFromCache(key: string): unknown {
        return ServletContextHolder.getServletContext().getAttribute(key);
    }

    /**
     * 设置对象至全局缓存里
     * @params key
     * @params value 
     */
    public setObjectToCache(key: string, value: unknown): void {
        ServletContextHolder.getServletContext().setAttribute(key, value);
    }

    /**
     * 将全局缓存里的对象删除
     * @params key 
     */
    public delObjectOnCache(key: string): void {
        ServletContextHolder.getServletContext().removeAttribute(key);
    }

    /**
     * 根据token值查询授权用户ID
     * <p>
     *  token有效值为2个小时, 避免相同key多次查询, 将token作为key存储到缓存中。
     * </p>
     * @param tokenValue 访问标识
     * @return 
     */
    public queryAuthorizeUserId(tokenValue: string): ResultInfo {
        let userId = this.getObjectFromCache(tokenValue) as string;
        if (!!userId) { // 优先从缓存获取, token失效由验证token有效的代码处理, 此处仅根据token获取授权用户
            console.info(`从缓存获取, [${tokenValue}]值对应的授权用户[${userId}]`);
            return { result: true, data: userId };
        }
        let resultInfo: ResultInfo = { result: true };
        let defaultDb = getDefaultDataSource();
        try {
            let querySQL: string = `SELECT USER_ID FROM (
                select USER_ID from HBZHSCJG.SZSYS_4_ACCESS_TOKENS where ACCESS_TOKEN = ? ORDER BY ACCESS_TOKEN_DEADLINE DESC
            ) WHERE ROWNUM = 1
            `;
            let queryDatas = defaultDb.executeQueryRows(querySQL, [tokenValue]);
            if (queryDatas.length > 0) {
                userId = queryDatas[0][0];
            }
            this.setObjectToCache(tokenValue, userId);
            resultInfo.data = userId;
        } catch (e) {
            resultInfo.result = false;
            resultInfo.message = `服务器内部错误-根据token获取授权用户异常`;
            console.error(`获取[${tokenValue}]对应的授权用户失败[${e.toString()}]`);
            console.error(e);
        }
        console.info(`[${tokenValue}]值对应的授权用户[${userId}]`);
        return resultInfo;
    }
}

/** 食品快检缓存记录信息 */
interface FootInterfaceCacheInfo {
    /** 请求次数 */
    requestNumx: number;
    /** 上次请求时间(单位/毫秒) */
    lastCallTime: number;
}

/** 执行结果信息 */
interface ResultInfo {
    /** 执行结果 */
    result: boolean;
    /** 执行消息 */
    message?: string;
    [field: string]: any;
}
