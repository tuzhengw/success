

import fs from "svr-api/fs";
import sys from "svr-api/sys";
import Files_ = java.nio.file.Files;
import Paths_ = java.nio.file.Paths;
import JavaFile = java.io.File;

/**
 * 获取指定图片附件ID的Base64编码
 * @description 文件读取出来时用base64编码返回，一般用于返回二进制文件的base64编码形式
 * <p>
 *  1 将Base64解析图片, 则必须给Base64加上图片前缀信息(做标记)
 *    在线解析地址: base64解析图片地址: https://www.it399.com/image/base64
 * </p>
 * @param attachId 附件ID
 * @param attachName 附件名, eg: xxx.png
 * @param includeBase64Prx 是否包含Base64前缀名, 默认: 不包含, eg: data:image/png;base64
 *        1) 若需要将获取的64值放置标签, 则需要增加前缀。
 *        2) 仅获取64值, 则不需要, 否则获取后再转换为字节会报异常(无法识别未知字符)。
 * @return 
 */
export function getSignAttachBase64(args: {
    attachId: string,
    attachName: string,
    includeBase64Prx?: string
}): string {
    let attachId: string = args.attachId;
    if (!attachId) {
        return "";
    }
    let picatureFilePath: string = getFileStorageFilePath(attachId);
    let picatureFile = fs.getFile(picatureFilePath);
    if (!picatureFile.exists()) {
        return `附件 ${attachId} 不存在`;
    }
    let prefixBase64: string = "";
    if (!!args.includeBase64Prx) {
        let attachName = args.attachName;
        if (!attachName) {
            return "未指定附件名, 无法添加Base64前缀信息";
        }
        let attachFileType: string = attachName.substring(attachName.lastIndexOf(".") + 1);
        let prefixBase64: string = PicturePrxfixBase64[attachFileType];
        if (!prefixBase64) {
            return "附件不是图片类型, 获取base64前缀失败";
        }
    }
    return `${prefixBase64}${picatureFile.readBase64()}`;
}


/**
 * 根据指定附件ID获取磁盘路径
 * <p>
 *  产品源码参考: filestorage/impl/WorkDirFileStorageServiceAbstract.java
 * </p>
 * @param attachId 附件ID
 *        1) 存储模式default, eg: default:e6243167dd7d43558d8039f1ebb3a0c1
 *        2) 存储模式工作区, eg: workdir:33e2fa4876f5e02744dd8047a7af9b600538-将描述加入到页面.png
 * @return
 */
export function getFileStorageFilePath(attachId: string): string {
    let arr = attachId.split(":");
    let attachMode: string = arr[0];
    let attachName: string = arr[1];
    let attachPath: string = `${sys.getClusterShareDir()}/file-storage`;
    switch (attachMode) {
        case "default":
            let parentFoldName: string = attachName.substring(0, 2); // 前两位作为附件的父目录
            attachPath = `${attachPath}/default/${parentFoldName}/${attachName}`;
            break;
        case "workdir":
            attachPath = `${attachPath}/relativePath/${attachName}`;
            break;
        default:
            attachPath = "";
            console.info(`附件: [${attachId}] 存储模式[${attachMode}] 未定义`);
    }
    console.info(`附件[${attachId}]存储磁盘路径: ${attachPath}`);
    return attachPath;
}


/** 
 * 图片Base 64前缀信息
 * 参考地址: https://blog.csdn.net/weixin_42614981/article/details/107176914
 */
const PicturePrxfixBase64 = {
    "png": "data:image/png;base64,",
    "jpg": "data:image/jpeg;base64,",
    "gif": "data:image/gif;base64,",
    "svg": "data:image/svg+xml;base64,",
    "ico": "data:image/x-icon;base64,",
    "bmp": "data:image/bmp;base64,"
};