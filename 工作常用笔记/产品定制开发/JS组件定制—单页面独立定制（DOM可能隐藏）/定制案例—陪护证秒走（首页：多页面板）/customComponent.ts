import { Component, formatDate } from 'sys/sys';
import 'css!./customComponent.css';


/**
 * 陪护证首页走秒的js控件
 */
export class AutoRefreshTime extends Component {
    public cdiv: HTMLElement;
    public daySpan: HTMLElement;
    public timeSpan: HTMLElement;
    public timmer: number;
    public _reInit(args: any): void {
        let domParent = args.domParent;
    }
    protected _init(args: any): HTMLElement {
        let domBase = super._init(args);
        let cdiv = this.cdiv = document.createElement("div");
        cdiv.classList.add("autoRefreshTime-cdiv");

        let timeSpan = this.timeSpan = document.createElement("span");
        cdiv.appendChild(timeSpan);
        timeSpan.classList.add("autoRefreshTime-cdiv-timeSpan");

        let daySpan = this.daySpan =  document.createElement("span");
        cdiv.appendChild(daySpan);
        daySpan.classList.add("autoRefreshTime-cdiv-daySpan");

        domBase.appendChild(cdiv);
        this.timmer = setInterval(() => {
            let date = new Date();
            let day = formatDate(date,'yyyy-MM-dd');
            let time = formatDate(date, 'HH:mm:ss');
            //console.log(`走秒:${day} ${time}`);
            this.daySpan.innerText = day;
            this.timeSpan.innerText = time;
        }, 1000);

        return domBase;
    }

    public dispose() {
        clearInterval(this.timmer);
        super.dispose();
    }
}