
import { updateDwTableData, queryData} from "svr-api/dw";

/**
 * 查询需要指定共享任务资源信息
 * @param dxShareTaskIds: string
 */
function querySignShareResInfos(dxShareTaskIds: string[]): string[][] {
    let query: QueryInfo = {
		sources:[{
			id:"model1",
			dbSql:"xxxxxxx"
		}],
        fields: [{
            name: "TASK_ID", exp: "model1.TASK_ID.SCHEDULE.NAME", group: true  // 分组查询
        }, {
            name: "DX_RES_ID", exp: "model1.DX_RES_ID.DX_RES_ID"
        }, {
            name: "SCHEDULE_TASK_ID", exp: "count(model1.ID)" // 误设置select为true
        }],
        filter: [{
            exp: `model1.ZJID in ('${dxShareTaskIds.join("','")}')`
        },{
			 exp: `model1.PARENT_DIR LIKE '/sysdata/data/sources%' AND TYPE = 'jdbc'`
		}],
		 // select: true, // 为true就不会分组查询
    }
    let queryData = dw.queryData(query).data;
    print(queryData);
    return [];
}

/**
 * 查询大于当天零点的数据
 */
function queryCurrentDay(): void {
	let queryInfo: QueryInfo = {
            sources: [{
                id: "model1",
                path: this.statisticPreLibReceiveTablePath
            }],
            fields: [{
                name: "PRE_LIB_ID", exp: "model1.PRE_LIB_ID"
            }, {
                name: "TABLE_NAME", exp: "model1.TABLE_NAME"
            }],
            filter: [{
                exp: "model1.CREATE_TIME >= today()" // today(): 仅显示当前年份，月份和日期
            }]
        }
}