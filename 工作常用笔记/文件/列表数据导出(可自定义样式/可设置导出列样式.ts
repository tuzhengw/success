
import { showConfirmDialog } from "commons/dialog";
import "css!./data-govern-mgr.css";
import { DatasetDataPackageRowInfo, IDataset, InterActionEvent, IVComponent } from "metadata/metadata-script-api";
import { ExpDialog, IFieldsDataProvider, showExpDialog } from 'commons/exp/expdialog';
import { Component, isEmpty, SZEvent, throwInfo, rc, message, uuid, showProgressDialog, showWaiting, rc_task, assign, showWarningMessage, LogItemInfo, ServiceTaskPromise, showDialog } from "sys/sys";
import { CustomJs_List, CustomJs_List_Head, getAppPath, showScheduleDialog } from "../commons/commons.js";
import { ExpVar, IExpDataProvider } from "commons/exp/expcompiler";
import { checkCstmRule, CheckCstmRuleArgs, runCheckTask } from "dg/dgapi";
import { getDwTableDataManager, getQueryManager } from "dw/dwapi";
import { showDrillPage, getMetaRepository } from 'metadata/metadata';
import { ExpContentType } from 'commons/exp/exped';
import { getScheduleMgr, ScheduleMgr, ScheduleLogInfo, TaskLogInfo } from "schedule/schedulemgr";
import { ProgressLogs } from "commons/progress"
import { Dialog } from "commons/dialog";

/**
 * 20211129 tuzw
 * 数据中台的自定义规则设置界面支持导入导出
 * 帖子地址：https://jira.succez.com/browse/CSTM-16899
 * 
 * 20220628 tuzw
 * 导出自定义规则的时候，增加左侧树和模型已有的过滤条件
 * 
 * @param importType 导出类型，eg：importPageDisplayData：导出当前查询的所有数据、importChoseData：导出页面勾选的数据（默认）
 * @param importChoseDataKeys? 页面勾选的行主键集
 * @param ruleResCataLog? 规则所在目录ID（维表主键）
 */
public button_checkRule_import(event: InterActionEvent): Promise<void> {
	let page = event.page;
	let params = event?.params;
	let importType: string = params?.importType;
	let ruleResCataLog: string = params?.ruleResCataLog;

	let spgResid: string = page.getFileInfo().id;
	let dataFilter: Array<FilterInfo> = [{
		exp: `ENABLE='1' and RULE_DEFINE_TYPE != 'BUILTIN'`
	}];
	let importChoseDataKeys: Array<string> = [];
	if (importType == 'importPageDisplayData') {
		let fieldsFilter: IVComponent = event.page.getComponent("fieldsFilter1");
		let filterClauses: FilterClauseInfo[] | string = fieldsFilter.getValue() as string;
		if (!isEmpty(filterClauses)) {
			filterClauses = JSON.parse(filterClauses) as FilterClauseInfo[];
			if (!isEmpty(ruleResCataLog)) {
				filterClauses.push({
					exp: `DG_RES_CATALOG ='${ruleResCataLog}'`
				});
			}
			dataFilter.pushAll(filterClauses as FilterClauseInfo[]);
		}
	} else {
		importChoseDataKeys = params?.importChoseDataKeys;
		if (isEmpty(importChoseDataKeys) || importChoseDataKeys.length == 0) {
			showWarningMessage(`当前页面没有勾选的行`);
			return Promise.resolve();
		}
		if (typeof importChoseDataKeys == 'string') {
			importChoseDataKeys = (importChoseDataKeys as string).split(",");
		}
		let filterExp: string = `model1.RULE_ID in ('${importChoseDataKeys.join("','")}')`;
		if (!isEmpty(ruleResCataLog)) {
			filterExp = `${filterExp} and DG_RES_CATALOG='${ruleResCataLog}'`;
		}
		dataFilter.push({
			exp: `model1.RULE_ID in ('${importChoseDataKeys.join("','")}')`
		});
	}
	let exportDataParam = this.setImportDataParams(spgResid, dataFilter);
	return exportQuery(exportDataParam);
}

/**
 * 构建导出自定义规则的参数配置
 * @param spgResid 当前导出页SPG resid值
 * @Param dataFilter 导出过滤条件
 * @retrn 
 */
private setImportDataParams(spgResid: string, dataFilter: FilterInfo[]): ExportDataParams {
	let fieldQueryInfo: QueryInfo = {
		fields: [
			{ name: "规则编码", exp: "model1.RULE_ID" }, { name: "规则说明", exp: "model1.RULE_NAME" },
			{ name: "规则内容", exp: "model1.RULE_EXP + ' , ' + model1.RULE_DATA_FLOW + ' , ' + model1.RULE_QUERY" },
			{ name: "规则表达式描述", exp: "model1.RULE_EXP_DESC" }, { name: "错误数据显示表达式", exp: "model1.ERROR_DATA_DISPLAY_EXP" },
			{ name: "错误级别", exp: "model1.ERROR_LEVEL" }, { name: "分值", exp: "model1.SCORE" }
		],
		sources: [{
			id: "model1",
			path: "/sysdata/data/tables/dg/DG_RULES.tbl"
		}],
		filter: dataFilter
	}
	let cellsBordersInfo: CellsBordersInfo = { // 单元格边框信息：全部
		all: {
			color: "#000000",
			width: 1
		}
	}
	let exportColumnStyles: ExportColumnStyle[] = [ // 导出excel表格的列样式
		{ width: 72 * 3 }, { width: 72 * 3 }, { width: 72 * 7 }, { width: 72 * 6 }, { width: 72 * 6 }, { width: 72 }, { width: 72 }
	];
	let exportTableStyle: ExportTableStyle = { // 设置导出Excel格式
		showGridLine: false,
		bordersInfo: cellsBordersInfo,
		columns: exportColumnStyles,

		header: { // 列标题样式
			backgroundColor: "#BBFFFF", // 颜色对应表：http://www.mgzxzs.com/sytool/se.htm
			rowHeight: 45,
			font: {
				family: "宋体",
				bold: true,
				size: 18
			}
		}
	}
	let exportDataParam: ExportDataParams = {
		resid: spgResid,
		query: fieldQueryInfo,
		fileFormat: DataFormat.xlsx,
		fileName: "自定义规则列表",
		exportTableStyle: exportTableStyle
	}
	return exportDataParam;
}