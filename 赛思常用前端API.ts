/**
 * =====================================================================
 * 作者：tuzhengwei
 * 创建日期：20220215
 * 脚本用途：
 * 		记录常见的前端API方法
 * ====================================================================
 */
1. (nodeChildrens[i] as HTMLElement).className == "tree-indent"

2. ?.dispose()：校验是否有这个方法

3. onClickAdjustRowEvent: this.refreshPanelScrollBar.bind(this, 参数(可为JSON)); // 将其当前对象绑定

4. rc 请求后端方法, 项目平台设置的：tomcat最大保持连接时间, 默认：60/s,
                  超过规定时间, 则会报: 504, 没有及时从上游服务器收到请求
5. private cacheWordNodeMetaDataInfos: { [interIdentifiter: string]: MetaDataMappingTable };


/**
 * 前端下拉框
 * 所有表的字段都在系统表（/sysdata/data/tables/meta/META_FIELDS.tbl)中
 * 下拉框绑定：字段表--下拉框属性（某个字段）---维项过滤
 */

/**
 * 1. 获取对应模型的resid值
 */
 import { getMetaRepository } from 'metadata/metadata';
 return getMetaRepository().getFile(importModelPath, false, false, false).then(file => {
	let importModelResid: string = file.id;
	return import("dw/dwapi").then((m) => {
		m.exportQuery({
			resid: importModelResid,
			query: fieldQueryInfo,
			fileFormat: DataFormat.xlsx,
			fileName: "指标列表"
		});
	});
})

import db from 'svr-api/db';
/**
 * 2. 获取指定数据库所支持的最大表名长度（后端）
 */
 function getMaxTableNameLength(): number {
	let sourceDataBase = "default";
	let sourceDs = db.getDataSource(sourceDataBase);
	let sourceFactory = sourceDs.getConnectionFactory();
	let maxTableNameLength = sourceFactory.getDbMetaData().getMaxTableNameLength();
	print(maxTableNameLength); // 64
 }
 
/**
 * 3. 前端等待
 */
 wait(100).then(() => {
	page.setParameter("param6", false);
})


/**
 * 4. 前端刷新模型数据
 * 注意：后端也可能存在缓存，可通过请求的data属性查看后端返回的数据是否是最新的（SQL不能判断）
 */
import { deleteLabels, getDwTableDataManager, getLabels, getQueryManager, saveLabelLib, updateDwTableDatas, refreshModelState } from "dw/dwapi";
1. 
getDwTableDataManager().updateCache([{ // 考虑脚本存在【异步】，检测任务ID更新后，需要更新【检测结果表】数据，刷新使用脚本执行
	path: "/ZHJG/data/tables/FACT/sy/jcry/dbrwlb_jg.tbl",
	type: DataChangeType.refreshall
}])
2.
refreshModelState("/sysdata/data/tables/dw/DW_DATA_LABELS.tbl");
3. 注意：参数需要包含：父类必填属性
let updateCachePath: DwDataChangeEvent[] = [{
	path: `/${this.owner.projectName}/app/label.app/data/labelData/SDI_LABELS_COUNT.tbl`,
	type: DataChangeType.refreshall
}]
getDwTableDataManager().updateCache(updateCachePath);

getDwTableDataManager().updateCache([getDwTableChangeEvent('/sysdata/data/tables/dg/DG_STD_TABLE_FOLDS.tbl', { updateAll: true })], true);
 
 
/**
 * 5. 获取元数据文件内容（后端）
 */
import metadata from "svr-api/metadata";
let file: string = metadata.getString("/sysdata/data/batch/自动生成加工，并且执行.action.ts");
file = file.replace("xxx", taskId); // 若需要对读取的内容中某个变量赋值，可以使用replace


// 前端
import { getMetaRepository } from "metadata/metadata";
let repository = getMetaRepository();
return repository.getFile(`${projectRootAddress}/commons/projectRootAddress.txt`).then(result => {
	let fileContent: string = result.content;
});


/**
 * 6. 前端返回promise
 */
return Promise.resolve(false);
return Promise.resolve({ moduleIds: [], topicIds: []});;

/**
 * 7. 动态创建JSON对象
 {
	"RES_ID": "RES_ID",
	"DIMENTION_ID": "DIMENTION_ID",
	"ODS_GYXY_SYS_DATASOURCE": "数据来源_ODS_GYXY",
	"ODS_GYXY_SYS_UPDATE_TIME": "数据更新时间_ODS_GYXY"
}
 */
let ctcTableFieldDesc = {};
for (let i = 0; i < queryDescResult.length; i++) {
	let filedName: string = queryDescResult[i].COLUMN_NAME;
	let filedDesc: string = queryDescResult[i].COMMENTS;
	ctcTableFieldDesc[filedName] = filedDesc; // 动态添加
}


/**
 * 8. 当前浏览器属性对象，可校验当前是移动端还是PC端
 *  移动端测试——都在【门户】打开
 */
方法一：(alias) const browser: ParsedUserAgentInfo
方法二：
import { IMobileFrame } from "app/mobilepage";
import { getCurrentMobileApp } from 'sys/sys';
let mobileApp: IMobileFrame = getCurrentMobileApp();  if(mobileApp) { ....
 

/**
 * 9. 浏览器控制台使用产品qunit对象
 * 环境自定义js文件, 需要携带环境的上下文路径, eg: sdi2_1
 * 
 * 前端写测试用例，需要刷新页面才会加载脚本，页面打开后进入f12，脚本不会加载
 */
requirejs(['sys/jquery/dist/jquery.min','sys/sys','qunit/qunit-utils','/sdi2_1/sysdata/test/custom.js'])
qunit.queryVisibleElement(""); // id class 中文名都可以

/**
 * 10. 前端解码错误异常
 * 前提：要打断点
 */
 requirejs(['sys/sys'], async function(args){
    let lzString = args.LZString;
    let decompressStr = lzString.decompressFromBase64("");
    console.log(decompressStr)
})
 
 
 /**
  * 11. 多个promise等待
  */
Promise.all([promise1, promise2, promise3]).then((values) => {
  console.log(values);
});

/**
 * 12. 前端更新数据库表方法
 */
 import { queryDwTablesState, queryLabelLibs, saveLabelLib, updateDwTableDatas, getDwTableDataManager } from 'dw/dwapi';
 updateDwTableDatas({
	"/sysdata/data/tables/dw/DW_DATA_LABELS_LIB.tbl": {
		modifyRows: [{
			fieldNames: ['SHARED_MODEL_RESID'],
			rows: [{
				keys: [libCode],
				row: [idList.join(',')]
			}]
		}]
	}
});

/**
 * 13. 获取移动端app
 */
let mobileApp: IMobileFrame = getCurrentMobileApp();

/**
 * 14. 前端调用产品后端API方法，捕获异常
 * 使用showWaiting的catch捕获
 */
let promise = getDataSourceManager().saveConnection(formData, "sdi", type == 'edit' ? name : null);
return showWaiting(promise, undefined, true).then((datas: string) => {
	if (datas == "true") {
		showSuccessMessage("dsmgr.dialog.saveDataBase.success", 3000);
		return true;
	} else {
		showErrorMessage("dsmgr.dialog.saveDataBase.failed");
		return false;
	}
}).catch(e => {
	e.errorCode = "err.db.sqlException";
	showErrorMessage(`保存数据源信息出现异常，异常原因【${e.causeMessage || e.message}】`);
	return false;
});


/**
 * 15. 异步执行结束后, then内部要使用外部的变量，内部才会加载
 */
public addRichtextinputOnkeydown() {
	let richtextInput = this.superPage.getComponent(this.richtextinputId);
	if (isEmpty(richtextInput)) {
		return;
	}
	let tinyMCEText: TinyMCEText = richtextInput.component.getInnerComoponent();
	this.tinyMCEText = tinyMCEText;

	let tinyMCETextOldDoInit = tinyMCEText.doInit.bind(tinyMCEText);
	tinyMCEText.doInit = () => { // 重写富文本初始渲染方法
		tinyMCEText;// 使用外部参数, 内部才会加载
	}
}

/**
 * 16. 列表逆向删除法
 */
let historyCheckedPks: JSONObject[] = listMultipageChose.getListCheckedPks();
if (!historyCheckedPks || historyCheckedPks.length == 0) {
	return;
}
for (let i = historyCheckedPks.length - 1; i >= 0; i--) { // 避免删除后，下标错位，用逆向循环删除
	let sourceTable = historyCheckedPks[i]['SOURCE_TBALE'];
	if (deleteSourceTables.includes(sourceTable)) {
		historyCheckedPks.splice(i, 1);  // 去除当前从配置表删除的来源表信息
	}
}

/**
 * 17. 处理前端URL包含特殊字符, eg: 空格 汉字
 * url拼接参数时，参数值带有"+"、"="等特殊字符，没有做特殊处理，导致传递后，获取的参数值不正确。
 * 调用encodeURIComponent()函数，处理含特殊字符的参数值。
 * encodeURIComponent()：针对URL，不处理ASCII字母 数字 ~!*()’，编码范围比encodeURI()大
 */
encodeURIComponent(" 汉字");  == "'%20%E6%B1%89%E5%AD%97'"