
import db from "svr-api/db"; //数据库相关API
import http from "svr-api/http";
import { sleep } from "svr-api/sys";
import fs from "svr-api/fs";
import File_ = java.io.File; // 工具类不需要对象，直接import导入


/** 查询文件没有创建成功的共享—国家—食品经营信息 */
const QUERY_SHARE_FOOD_INFO_SQL =
    `SELECT * FROM public.GX_GJ_SPJY t0 
            WHERE NOT EXISTS (
                SELECT 1 FROM public.GX_GJ_SPJY_LOG t1
                    WHERE  t1.ACK_CODE ='SUCCESS' AND t0.AUTH_CODE = t1.AUTH_CODE
            )
            ORDER BY ISSU_DATE
            LIMIT ? OFFSET ?`;
/** 一次查询的最大数量 */
const QUERY_MAX_NUMS = 10;
/**
 * 20220303 tuzw
 * 获取共享—国家—食品经营信息
 * @param offset 查询起始页，0：第一页
 * @return []
 */
function getShareFoodInfos(offset: number): GX_GJ_SPJY[] {
    let ds = db.getDataSource(DATA_BASE_NAME);
    let queryData = ds.executeQuery(QUERY_SHARE_FOOD_INFO_SQL, [QUERY_MAX_NUMS, offset]) as GX_GJ_SPJY[];
    return queryData;
}

 
 