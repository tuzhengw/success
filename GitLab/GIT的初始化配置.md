---
typora-root-url: images
---

# 一 GIT的初始化配置

> **第一步：**
>
> 初次运行Git的配置（windows系统）
> 打开Git命令行输入以下命令：

```cmd
succez@LAPTOP-KTLS3R8I MINGW64 ~/Desktop
$ git config --global user.name 'tzwN'

succez@LAPTOP-KTLS3R8I MINGW64 ~/Desktop
$ git config --global user.email succ@tzw.com
```

> **第二步：**
>
> Git命令查看配置信息：

```cmd
succez@LAPTOP-KTLS3R8I MINGW64 ~/Desktop
$ git config --list

diff.astextplain.textconv=astextplain
filter.lfs.clean=git-lfs clean -- %f
filter.lfs.smudge=git-lfs smudge -- %f
filter.lfs.process=git-lfs filter-process
filter.lfs.required=true
http.sslbackend=openssl
http.sslcainfo=D:/mysoftware/Git/Git/mingw64/ssl/certs/ca-bundle.crt
core.autocrlf=true
core.fscache=true
core.symlinks=false
pull.rebase=false
credential.helper=manager-core
credential.https://dev.azure.com.usehttppath=true
init.defaultbranch=master
user.name=tzwN
user.email=succ@tzw.com
```

# 二 生成ssh公钥

```cmd
succez@LAPTOP-KTLS3R8I MINGW64 ~/Desktop
$ ssh-keygen -t rsa -C "succ@tzw.com"   # 执行命令

Generating public/private rsa key pair.
Enter file in which to save the key (/c/Users/succez/.ssh/id_rsa):
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in /c/Users/succez/.ssh/id_rsa
Your public key has been saved in /c/Users/succez/.ssh/id_rsa.pub
The key fingerprint is:
SHA256:fB+OJBTiEsertTLr8C2MsdGNz3k4pjqC+JcQWDUEDa8 succ@tzw.com
The key's randomart image is:
```

> 执行后，在C盘的user目录下的`.ssh`文件中

> 如果提示公钥已经存在, 输入y . 之后按回车 会提示公钥存储的位置.
> https://www.cnblogs.com/zjfjava/p/10080569.html

# 三 基本命令的使用

```git
Git的基本命令：
git clone ...

git init    # 初始化一个git项目

git status   # 查看当前文件的git状态

git add 文件名称   # 告诉git，管理某个文件（追踪某个文件）

git add .(点)    # 添加当前目录下的所有文件

git commit -m "注释"   # 提交版本
```

<img src="../images/流程.png" style="zoom:67%;" />