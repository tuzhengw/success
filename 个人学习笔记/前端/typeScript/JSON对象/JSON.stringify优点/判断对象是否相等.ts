/**
 *  判断两数组/对象是否相等
 */
function main() {
    var a = [1,2,3];
    var b = [1,2,3];
    // false
    console.log(a == b);
    // true
    console.log(JSON.stringify(a) == JSON.stringify(b));
}