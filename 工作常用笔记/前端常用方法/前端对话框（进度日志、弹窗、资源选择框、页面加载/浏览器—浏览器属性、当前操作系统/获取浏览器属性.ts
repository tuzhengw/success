
/** 指定最低浏览器版本 */
const MIN_BROWSER_VERSION: string = '96.0.4664.45';
/**
 * 浏览器信息
 */
interface BROWSER {
	/** 浏览器名 */
	BROWSER_NAME: string;
	/** 浏览器版本 */
	BROWSER_VERSION: string;
}

/**
 * 获取当前登录的版本号
 * return {
 *   BROWSER_NAME: "chrome",
 *   BROWSER_VERSION: "96.0.4664.45"
 * }
 */
function getBrowserVersion(): BROWSER {
	let userAgent = navigator.userAgent.toLowerCase();
	let matchExpression = /(msie|firefox|chrome|opera|version).*?([\d.]+)/;
	/**
	 *  0: "chrome/96.0.4664.45"
		1: "chrome"：浏览器类型
		2: "96.0.4664.45"：浏览器版本
		groups: undefined
		index: 80
		input: "mozilla/5.0 (windows nt 6.1; win64; x64) applewebkit/537.36 (khtml, like gecko) chrome/96.0.4664.45 safari/537.36"
	 */
	let currentBrowserInfo = userAgent.match(matchExpression);
	let returnBroserInfo: BROWSER = {
		BROWSER_NAME: currentBrowserInfo[1],
		BROWSER_VERSION: currentBrowserInfo[2]
	}
	return returnBroserInfo;
}

// ----------------------------------------------------------------------------------------
const MIN_BROWSER_VERSION_chrome: string = "96.0.4664.45";
print(browserVersionCompare("96.0.4664.93", MIN_BROWSER_VERSION_chrome)); -- false

/**
 * 浏览器版本号进行比较
 * 注意：字符串比较时根据ascll码比较，可能会存在精度不准的情况，这里需要转换为整型比较，eg："45" > "100" -- true
 * @param compareVersion 当前比较的版本号
 * @param minBrowerVersion 最小版本号
 * @return true：需要更新
 */
function browserVersionCompare(compareVersion: string, minBrowerVersion: string): boolean {
	if (minBrowerVersion == null) {
		return true;
	}
	let compareVersionArr: Array<string> = compareVersion.split(".");
	let minVersionArr: Array<string> = minBrowerVersion.split(".");
	let minVersionArrLen = minVersionArr.length;
	let compareVersionArrLen = compareVersionArr.length;
	let minVersionIndex = 0, compareVersionIndex = 0;

	/** 是否需要更新 */
	let isNeedUpdate: boolean = true;
	while (minVersionIndex < minVersionArrLen && compareVersionIndex < compareVersionArrLen) {
		let minVersion: number = parseInt(minVersionArr[minVersionIndex++]);
		let compareVersion: number = parseInt(compareVersionArr[compareVersionIndex++]);
		if (minVersion < compareVersion) {
			isNeedUpdate = false;
			break;
		}
		if (minVersion > compareVersion) {
			break;
		}
	}
	// 两者【相同位置】的元素比较完后，若比较结果为：相等，则那个数组还有【未比较完的元素】则谁大
	if (isNeedUpdate) {
		if (minVersionIndex > compareVersionIndex) {
			isNeedUpdate = true;
		}
	}
	return isNeedUpdate;
}