
import "css!./commons/swiper-bundle.min.css";
import * as swiper from "./commons/swiper-bundle.min.js";
import { HomeTemplatePage } from "app/templatepage";
import {
	SZEvent, throwError, assign, message, rc, Component, showSuccessMessage, ajax, browser, rc1, showErrorMessage, showWarningMessage,
	IPageContainer, UrlInfo, encodeUrlInfo, ctx, showWaiting, showDialog, ctxIf, AjaxArgs, wait, waitRender, showMenu, showFormDialog,
	uuid, isEmpty, throwInfo, formatDate, dispatchResizeEvent, CommandItemInfo, showInfoDialog
} from "sys/sys";
import {
	IMetaFileViewer, MetaFileViewerArgs, IMetaFileCustomJS, InterActionEvent, getCurrentUser, CurrentUser, showDrillPage, getMetaRepository
} from "metadata/metadata";
import { IAppCustomJS, IDwCustomJS, IFAppCustomJS } from "metadata/metadata-script-api";
import { PortalTemplatePage } from "app/templatepage";
import { SlideTemplate } from "extension/extensions/succ-portalTemplate-slidepages/main";
import { Button, ComboboxArgs, DataProvider, Toolbar, ToolbarItemAlign, SelectPanelItem } from "commons/basic";
import { MenuItem } from "commons/menu";
import { DwTableEditor, IDwTableEditorPanel } from "dw/dwtable";
import { EasyFilter } from "commons/extra";
import { Dialog, showConfirmDialog } from "commons/dialog";
import { Form } from "commons/form";
import { getLabels, getQueryManager, updateDwTableDatas, DwTableDataCache, getDwTableDataManager, deleteLabels, labelData, addLabels, modifyLabel, refreshModelState } from "dw/dwapi";
import { ExDTable } from "commons/dtable";
import { modifyClass } from './commons/commons.js';
import { DisplayFormat } from "commons/exp/expeval";
import { getScheduleMgr } from "schedule/schedulemgr";
import { VisualSelectPanel } from "ana/anabasic";
import { DwTableModelFieldProvider, DwDataHierarchyProvider } from 'dw/dwdps';


export const CustomJS: { [type: string]: IMetaFileCustomJS } = { 
	"市场主体查询.spg": {
		CustomActions: {
			/**
			 * 20220728 tuzw
			 * 将单选面板控件当前选中的节点添加到指定全局参数中
			 * @description 触发条件：点击单选面板节点
			 * @description 含义：由于页面利用全局参数实现多组件之间的联动，当选中单选面板某个节点的时候，需要将其节点添加 | 删除于全局参数中
			 * @param selectPanelId 选择面板ID
			 * @param globelParamValue 全局参数目前最新值
			 * @param globelParamName 需要将其值添加的全局变量名
			 */
			button_label_setChoseValueToParam: (event: InterActionEvent) => {
				let page = event.page;
				let param = event.params;
				let selectPanelId: string = param.selectPanelId;
				let selectPanelComp: VisualSelectPanel = page.getComponent(selectPanelId).getUIComponent().getInnerComoponent();
				let selectItemIds: string[] = [];
				let allItemIds: string[] = []; // 记录单选面板所有选项的ID
				let allItems: SelectPanelItem[] = selectPanelComp.items;
				for (let i = 0; i < allItems.length; i++) {
					let item: SelectPanelItem = allItems[i];
					if (item.checked) {
						selectItemIds.push(item.id);
					}
					allItemIds.push(item.id);
				}
				let globelParamName: string = param.globelParamName;
				let globelParamValues: string[] = dealStrAvailable(param.globelParamValue);
				let newGlobelParamValues: string[] = [];

				for (let i = 0; i < globelParamValues.length; i++) {
					let labelCode: string = globelParamValues[i];
					if (allItemIds.indexOf(labelCode) != -1) { // 校验当前值是否属于单选面板的选项
						let choseValueIndex: number = selectItemIds.indexOf(labelCode);
						/**
						 * 若节点属于单选面板的选项值，并且此选项值（历史选项值）不存在最新选项中，则视为删除
						 * 注意：新增节点是独立于全局参数以外的值，不会在循环内比较
						 */
						if (choseValueIndex == -1) { // 若当前选中节点不存在全局变量，则视为：删除
							continue;
						}
						if (choseValueIndex != -1) { // 若已存在，并删除其值，避免重复添加
							selectItemIds.remove(choseValueIndex);
						}
					}
					newGlobelParamValues.push(labelCode);
				}
				if (selectItemIds.length > 0) { // 若最新选中的值还存在，则视为新增标签节点
					newGlobelParamValues.pushAll(selectItemIds);
				}
				page.setParameter(globelParamName, newGlobelParamValues.join(","));
			},
			/**
			 * 20220728 tuzw
			 * 调整标签信息
			 * @description 注意：页面存在多个定制标签JS组件，其调整是同步进行，这里接收外部所有需要调整的JS控件ID
			 * @param customLabelJsCompIds js定制组件ID集
			 * 
			 * 20220902 tuzw
			 * 页面自定义标签和上级下级标签可以分开打标，单独打标时，需要做空校验
			 */
			button_label_adjustLabelDatas: (event: InterActionEvent) => {
				let page = event.page;
				let param = event.params;
				let customLabelJsCompIds: string[] = dealStrAvailable(param.customLabelJsCompIds);
				for (let i = 0; i < customLabelJsCompIds.length; i++) {
					let sdiCustomLable = page.getComponent(customLabelJsCompIds[i]).getUIComponent();
					if (isEmpty(sdiCustomLable)) { // 校验当前页面是否有开启此类型打标（js组件是否被隐藏）
						continue;
					}
					let labelComp = sdiCustomLable.getInnerComoponent();
					labelComp.adjustCompanyLabel();
				}
			},
			/**
			 * 20220728 tuzw
			 * 刷新指定单选面板
			 * @description 目的：由于新增标签节点后，需要重新查询当前最新的标签节点
			 * @param selectPanelId 刷新的单选面板ID
			 * @param refreshModelPath 刷新模型路径
			 * 
			 * 模型数据被修改后，页面没有显示最新的数据
			 * 帖子：https://jira.succez.com/browse/CSTM-19855
			 * 解决办法：刷新前后端缓存信息
			 */
			button_label_refreshSelectPanel: (event: InterActionEvent) => {
				let page = event.page;
				let params = event.params;
				let selectPanelId: string = params.selectPanelId;
				let refreshModelPath: string = params.refreshModelPath;
				if (isEmpty(selectPanelId)) {
					showWarningMessage(`未指定刷新的单选面板`);
					return;
				}
				return rc({
					url: `${ProjectRootPath}/script/jsCustomLable/labelScript.action?method=refreshEndScriptCache`,
					method: 'POST',
					data: {
						refreshModelPaths: [refreshModelPath]
					}
				}).then(() => {
					let selectPanelComp: VisualSelectPanel = page.getComponent(selectPanelId).getUIComponent().getInnerComoponent();
					let inputOptionsDataProvider = selectPanelComp.dataProvider;
					inputOptionsDataProvider.needRefresh = true;
					let dwDataHierarchyProvider: DwDataHierarchyProvider = inputOptionsDataProvider.dataProvider;
					dwDataHierarchyProvider.originalDwDataHierarcy = null;
					getDwTableDataManager().cache = {};
					dwDataHierarchyProvider.fetchData();
					selectPanelComp.refresh();
				});
			},
			/**
			 * 新增一条标签
			 * @param libCode 标签库编码
			 * @param labelCode 标签编码
			 * @param labelName 标签名
			 * @param labelCateGory 标签分组
			 * @param labelType 标签类型
			 * @description 注意：直接将标签入库，产品不认可是标签，必须要走产品的新增标签逻辑
			 */
			button_label_addLabel: (event: InterActionEvent) => {
				let params = event.params;
				let libCode: string = params.libCode;
				let labelCode: string = params.labelCode;
				let labelName: string = params.labelName;
				let labelCateGory: string = params.labelCateGory;
				let labelType: number = params.labelType;
				let createTime: number = Date.now();
				let creater: string = getCurrentUser().userInfo.userId;
				return addLabels([{
					libCode: libCode,
					code: labelCode,
					name: labelName,
					category: labelCateGory,
					labelType: labelType == 1 ? DwDataLabelType.Soft : DwDataLabelType.Hard,
					createTime: createTime,
					creator: creater,
					share: true
				}], libCode
				).then(() => {
					showSuccessMessage("新增标签成功");

					return true;
				});
			},
			/**
			 * 修改一条标签信息
			 * @param libCode 标签库编码
			 * @param labelCode 标签编码
			 * @param labelName 标签名
			 * @param labelCateGory 标签分组
			 * @param labelType 标签类型
			 * 
			 * @description 
			 * 注意：直接将标签入库，产品不认可是标签，必须要走产品的新增标签逻辑，或者提交数据后，调用产品的更新标签api
			 */
			button_label_midifyLabel: (event: InterActionEvent) => {
				let params = event.params;
				let libCode: string = params.libCode;
				let labelCode: string = params.labelCode;
				let labelName: string = params.labelName;
				let labelCateGory: string = params.labelCateGory;
				let labelType: number = params.labelType;
				let createTime: number = Date.now();
				let creater: string = getCurrentUser().userInfo.userId;
				return modifyLabel({
					libCode: libCode,
					code: labelCode,
					name: labelName,
					category: labelCateGory,
					labelType: labelType == 1 ? DwDataLabelType.Soft : DwDataLabelType.Hard,
					createTime: createTime,
					creator: creater,
					share: true
				}).then(() => {
					return true;
				});
			},
			/**
			 * 覆盖保存软标签
			 */
			button_label_soft: (event: InterActionEvent) => {
				let params = event.params;
				let { labelCode, labelLib, filterCompId } = params;
				let filedFilterComp = event.page.getComponent(filterCompId);
				let value = filedFilterComp.getValue();
				if (typeof (value) === 'string') {
					value = JSON.parse(value)
				}
				let filters: FilterInfo[] = [];
				let filterExp: string = value && value[0] && value[0].exp;
				if (!!filterExp) { // 校验当前过滤方式是否为表达式过滤
					filters.push({
						exp: filterExp
					});
				} else {
					filters = !isEmpty(value) ? value.map(filter => {
						let newFilter: FilterInfo = assign({}, filter);
						newFilter.clauses.forEach(clause => {
							clause.leftExp && (clause.leftExp = clause.leftExp.replace("model1.", ""));//软标签在查询时不支持带有前缀，此处暂时去掉前缀。
							clause.exp && (clause.exp = clause.exp.replace("model1.", ""));
						})
						newFilter.clauses = newFilter.clauses.filter(f => { return f.rightValue != null || f.operator == "is null" || f.operator == "is not null" });//如果值为null需要把此过滤条件给去掉
						if (newFilter.clauses == null || newFilter.clauses.length == 0) {
							return null;
						}
						return newFilter;
					}) : [];
					filters = filters.filter(f => {
						return f != null;
					})
				}
				/**
				 * 2022-06-13 liuyz
				 * https://jira.succez.com/browse/CSTM-19149
				 * 如果一个过滤条件都没有，给出提示，此时不需要执行打标流程
				 */
				if (isEmpty(filters)) {
					showWarningMessage("请设置过滤条件!");
					return;
				}
				showConfirmDialog({
					caption: "数据打标",
					message: `将要从对指定条件的数据进行打标，是否确定？`,
					onok: () => {
						labelData({ labelCode: labelCode, labelLibCode: labelLib, filters: filters }).then(() => {
							throwInfo("保存成功！")
							return getDwTableDataManager().getLabelLib(labelLib);
						}).then(lib => {
							lib && refreshModelState(lib.mainTableId);
						});
					}
				})
			}
		}
	}
}