---
typora-root-url: images
---

# 定制JS组件—

# 单独显示：我的消息

> <button>`TS`文件位置</button>：web/me/...

# 一、实现效果

![](../images/原效果.png)

# 二、实现思路

​	定制一个`JSComponent`控件，找到实现：个人中心-我的消息部分的类MySMessagePanel，在定制控件里创建消息面板

## 1 脚本位置

<img src="../images/脚本位置.png" style="zoom:80%;" />

## 2  思路

（1）外层是：iframe标签，而：我的消息，是一个单独的页面，将其<button>提取出来单独显示</button>即可

（2）调式前端脚本，理解：个人中心——我的消息  <button>整个加载流程</button>

（3）找到加载过程中，初始化：我的消息  的<button>入口</button>：new PersonalCenterPage_smessage(参数)

（4）通过上下文，理解传递的<button>参数</button>是什么

​		————父DOM，也就是：我的消息  页面在那个父DOM内，默认：个人中心（外层iframe）是父DOM

```ts
/*
* 20211018
* 功能：初始化——我的消息，将其添加到所指定的父容器（DOM）中
* 默认实现方式，继承：个人中心页面对象
*/
export class PersonalCenterPage_smessage extends PersonalCenterPage {
    /**
     * 1. 初始时，先初始：个人中心， 并获取它所在的：DOM
     */
	protected _init(args: PersonalCenterPageArgs) {
		let domBase = super._init(args);
        /**
         * 初始：我的消息 页面，并将其添加到指定的：父DOM内
         * new MySMessagePanel：我的消息（电脑端）对象
         * 参数：父DOM对象
         */
		import('./smessage').then((m) => 
                                  new m.MySMessagePanel({ domParent: domBase }));
        /**
         * 在new MySMessagePanel后，我的消息 控件 会自己添加到传入的：父DOM内
         * 最后将其返回前端页面展示
         */
		return domBase;
	}

	public dispose() {
		super.dispose();
	}
}
```

​	重写：

​	（1）默认父容器为：个人中心

​	（2）《更改》 父容器对象，改为：某个面板对象，继承 component

```ts
/*
*  20211018
*  重写：初始化——我的消息，将其添加到所指定的父容器（DOM）中
*  Component：当前页面的组件对象
*/
export class 自定义类名 extends Component {
    /**
     * args：JS组件 所在 父容器（DOM）：面板对象
     */
	protected _init(args: any) {
		let domBase = super._init(args);
        /**
         * MySMessagePanel：我的消息（电脑端）对象
         * 参数：父DOM对象
         */
		import('./smessage').then((m) => 
                                  new m.MySMessagePanel({ domParent: domBase }));
        /**
         * 在new MySMessagePanel后，我的消息 控件 会自己添加到传入的：父DOM内
         * 最后将其返回渲染的效果
         */
		return domBase;
	}
    /** 
     * 若当前类没有属性需要消耗，则不需要重写该方法
     */
	// public dispose() {
	//	super.dispose();
	//}
}
```

（5）SPG中使用控件

![](../images/使用控件.png)



# 三、调式步骤

## 1	找到点击事件入口

![](../images/测试入口.png)

## 2 单步调式

![](../images/单步调式.png)

## 3 找到new我的消息

![](../images/new.png)

```ts
/**项目设置页面的配置 */
const MODULES: { [id: string]: typeof PersonalCenterPage } = {
    settings: PersonalCenterPage_settings,
    smessage: PersonalCenterPage_smessage,
};
```

## 4 我的消息 初始化入口

` new MySMessagePanel`

```ts
/*
* 20211018
* 功能：初始化——我的消息，将其添加到所指定的父容器（DOM）中
* 默认实现方式，继承：个人中心页面对象
*/
export class PersonalCenterPage_smessage extends PersonalCenterPage {
    /**
     * 初始时，先初始：个人中心， 并获取它所在的：DOM
     */
	protected _init(args: PersonalCenterPageArgs) {
		let domBase = super._init(args);
        /**
         * MySMessagePanel：我的消息（电脑端）对象
         * 参数：父DOM对象
         */
		import('./smessage').then((m) => 
                                  new m.MySMessagePanel({ domParent: domBase }));
        /**
         * 在new MySMessagePanel后，我的消息 控件 会自己添加到传入的：父DOM内
         * 最后将其返回
         */
		return domBase;
	}

	public dispose() {
		super.dispose();
	}
}
```

# 四、重写：初始化我的消息类

## PersonalCenterPage_smessage

## 1 脚本位置

<img src="../images/脚本位置.png" style="zoom:80%;" />

## 2 脚本代码

### 2.1 默认实现脚本

> 文件路径：产品代码/web/me/personlcenter.ts
>
> 文件名：personlcenter.ts

```ts
/*
* 20211018
* 功能：初始化——我的消息，将其添加到所指定的父容器（DOM）中
* 默认实现方式，继承：个人中心页面对象
*/
export class PersonalCenterPage_smessage extends PersonalCenterPage {
    /**
     * 1. 初始时，先初始：个人中心， 并获取它所在的：DOM
     */
	protected _init(args: PersonalCenterPageArgs) {
		let domBase = super._init(args);
        /**
         * 初始：我的消息 页面，并将其添加到指定的：父DOM内
         * new MySMessagePanel：我的消息（电脑端）对象
         * 参数：父DOM对象
         */
		import('./smessage').then((m) => 
                                  new m.MySMessagePanel({ domParent: domBase }));
        /**
         * 在new MySMessagePanel后，我的消息 控件 会自己添加到传入的：父DOM内
         * 最后将其返回前端页面展示
         */
		return domBase;
	}

	public dispose() {
		super.dispose();
	}
}
```

### 2.2 重写后的实现脚本

> 文件路径：APP应用/script/customcomponent.ts
>
> 文件名：customcomponent.ts

```ts
import { Component, formatDate, UrlInfo } from 'sys/sys';
import 'css!./customComponent.css';
import { MMP_Me, PersonalCenterPage_smessage } from 'me/personalcenter'
import { MMySMessagePage, MySMessagePanel } from "me/smessage";

/**
 * 20211018 tuzhengw
 * 帖子：https://jira.succez.com/browse/CSTM-16712
 * 场景：需要在其他页面嵌入个人中心——我的消息页面
 * 需求：仅我的消息页面，不要外层的iframe
 * 步骤：重写PersonalCenterPage_smessage对象的init方法
   
   使用：此方法创建了一个控件，类似：下拉框等，在spg中引入方法：《面板》 内嵌套 《JS组件》
 */
export class SoleMyMessagePage extends Component {
    // public cdiv: HTMLElement;
    protected _init(args: any): HTMLElement {
        /**
        * console.log(domBase);
        * <div class="solemymessagepage-base"></div>
        * 注意：找到这个element后，可以查看它的事件流程
        */
        let domBase = super._init(args);
        console.log("-- SoleMyMessagePage， 在spg中嵌入系统的用户消息页面 -- start ");
        /**
         * 该控件是在面板内部，不需要额外再嵌入一个<div>
         * 若 《不使用》：面板，则需要额外创建：div外层对象，最后需要将其：div添加到spg某个对象内
         *
         * let domParent = this.cdiv = document.createElement("div");
         * domParent.classList.add("SoleMyMessagePage-domParent");
         * spg某个DOM对象.appendChild(domParent);
         */
        
        // import('./smessage').then((m) => 
        //				new m.MySMessagePanel({ domParent: domBase }));
        
        import('me/smessage').then((m) => 
                                   new m.MySMessagePanel({ domBase }));
        
        console.log("-- SoleMyMessagePage， 在spg中嵌入系统的用户消息页面 -- end");
        return domBase;
    }
}
```

## 3 使用自定义控件

> 引入组件：JS组件
>
> 属性——JS文件：customcomponent.ts
>
> 控件类目：自定义实现的控件类名，例如：SoleMyMessagePage（单独的我的消息页面）

![](../images/使用控件.png)



## 4 效果

![](../images/实现效果.png)









