import security from "svr-api/security";

function onQueryData(queryInfo: QueryInfo): DbTableInfo | any[][] | QueryDataResultInfo {
    let groupInfos: UserGroupInfo[] = security.getCurrentUser().getGroups();
    let groupIds: string[] = [];
    for (let i = 0; i < groupInfos.length; i++) {
        groupIds.push(groupInfos[i].groupId);
    }
    let querySQL: string = `
    SELECT *
	FROM ( SELECT t1.LABEL_LIB_CODE,
		 t1.LABEL_CODE,
		 t1.LABEL_NAME,
		 t1.LABEL_DESC,
		 t1.LABEL_CATEGORY,
		 t1.LABEL_TYPE,
		 t1.IS_SHARE,
		 t1.FILTERS,
		 t1.CREATE_TIME,
		 t1.MODIFY_TIME,
		 t1.CREATOR,
		 t1.CREATOR_ORGID,
		 t1.RELEASE_ORGID,
		 t1.PUBLISH_STATE,
		 t1.MODIFIER,
		 t1.DATA_ANA_TIME,
		 t1.FONT,
		 t1.IMAGE_PATH,
		 t1.IMAGE,
		 t1.SVG,
		 t1.CJ,
		 t1.GKGLBM,
		 t1.PUBLIC_TIME,
		 t1.LABEL_CLASS_TYPE
	FROM SJZT.SZSYS_4_DW_DATA_LABELS t1
	WHERE t1.LABEL_CLASS_TYPE in ('${groupIds.join("','")}')
			AND t1.LABEL_TYPE = '2'
			AND t1.LABEL_LIB_CODE = 'N3CAvNslQqBw731QLlqiEB'
	ORDER BY  t1.PUBLIC_TIME DESC nulls last )
WHERE rownum <= 10
    `;
    print(querySQL);
    let dbTableInfo: DbTableInfo = {
        ddl: querySQL,
        datasource: "default"
    }
    return dbTableInfo;
}