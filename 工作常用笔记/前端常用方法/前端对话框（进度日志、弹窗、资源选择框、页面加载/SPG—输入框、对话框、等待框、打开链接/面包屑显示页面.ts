
import { getCurrentUser, getMetaRepository, InterActionEvent, MetaFileViewer, showDrillPage } from "metadata/metadata";
import { 
	Component, MacScrollBar, assign, hideMessage, isEmpty, message, rc, ServiceTaskInfo, ServiceTaskPromise, ServiceTaskState, 
	showConfirmDialog, showDialog, showErrorMessage, showMessageBox, showProgressDialog, showSuccessMessage, showWaiting,
	showWaitingMessage, showWarningMessage, SZEvent, timerv, uuid 
 } from "sys/sys";


function openPage(): void {
	let domFileViewer = document.getElementsByClassName('metafileviewer-base');
	let fileViewer: MetaFileViewer = domFileViewer[1].szobject as MetaFileViewer;
	fileViewer.showDrillPage({
		url: {
			// 若URL和参数一致则不会重复下钻, 内部会将当前页面返回
			path: `/sdi/data?:locate=${dataProcessDirPath}`
		},
		title: '数据源加工目录',
		target: ActionDisplayType.Container,
		breadcrumb: true
	})
}