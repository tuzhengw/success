/**
 * ===================================================
 * 修改人：luofuw
 * 审核员：liujingj
 * 修改日期：2022-10-28
 * 脚本用途: 客户端调用科大讯飞语音听写接口
 * 帖子地址：https://jira.succez.com/browse/CSTM-20825
 * ===================================================
 */
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
define(["require", "exports", "./crypto-js.min.js"], function (require, exports, crypto_js_min_js_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.Yytx = void 0;
    crypto_js_min_js_1 = __importDefault(crypto_js_min_js_1);
    const IP_HOST = location.host;
    const PROTOCOL = location.protocol;
    const transWorker = new Worker(`${PROTOCOL}//${IP_HOST}/xyfl/hbzfba/app/协同执法办案移动端NEW.app/scripts/yysb/transcode.worker.js`);
    const APPID = '81c15974';
    const API_SECRET = 'YjAxYWM3YjU1N2YxNDY1NjBlOWRjYjBj';
    const API_KEY = '9f938eda71a462c995d82a94a2f12df4';
    // 每一帧音频大小的整数倍
    const FRAME_SIZE = 1280;
    // 科大讯飞语音听写接口调用
    class Yytx {
        constructor({ appId, vad, language, accent } = {}) {
            let self = this;
            this.status = 'null';
            this.language = language || 'zh_cn';
            this.accent = accent || 'mandarin';
            this.appId = appId || APPID;
            if (vad) {
                this.vad = vad < 10000 ? vad : 3000;
            }
            else {
                this.vad = 3000;
            }
            // 记录音频数据
            this.audioData = [];
            // 记录听写结果
            this.resultText = '';
            // wpgs下的听写结果需要中间状态辅助记录
            this.resultTextTemp = '';
            transWorker.onmessage = function (event) {
                self.audioData.push(...event.data);
            };
            this.CustomActions = {
                recorderInit: this.recorderInit.bind(this),
            };
        }
        /**
         * 修改录音听写状态
         *
         * @params status 录音听写状态
         */
        setStatus(status) {
            this.onWillStatusChange && this.status !== status && this.onWillStatusChange(this.status, status);
            this.status = status;
        }
        /**
         * 获取当前录音听写状态
         *
         * @params status 录音听写状态
         */
        getStatus() {
            return this.status;
        }
        /**
         * 设置识别结果内容
         *
         * @params resultText 记录听写结果
         * @params resultTextTemp wpgs下的听写结果需要中间状态辅助记录
         */
        setResultText(resultText, resultTextTemp) {
            this.onTextChange && this.onTextChange(resultTextTemp || resultText || '');
            resultText !== undefined && (this.resultText = resultText);
            resultTextTemp !== undefined && (this.resultTextTemp = resultTextTemp);
        }
        /**
         * 修改听写参数
         *
         * @params vad 静音等待时间
         * @params language 语言
         * @params accent 方言，当前仅在language为中文时，支持方言选择。
         */
        setParams(vad, language, accent) {
            vad && (this.vad = vad);
            language && (this.language = language);
            accent && (this.accent = accent);
        }
        /**
         * 对处理后的音频数据进行base64编码
         *
         * @params buffer 数据缓冲流
         */
        toBase64(buffer) {
            let binary = '';
            let bytes = new Uint8Array(buffer);
            for (let i = 0; i < bytes.byteLength; i++) {
                binary += String.fromCharCode(bytes[i]);
            }
            return window.btoa(binary);
        }
        /**
         * 连接WebSocket
         */
        connectWebSocket() {
            return this.getWebSocketUrl().then(url => {
                let iatWS;
                if ('WebSocket' in window) {
                    iatWS = new WebSocket(url);
                }
                else if ('MozWebSocket' in window) {
                    iatWS = new MozWebSocket(url);
                }
                else {
                    alert('浏览器不支持WebSocket!');
                    return false;
                }
                this.webSocket = iatWS;
                this.setStatus('init');
                iatWS.onopen = (e) => {
                    this.setStatus('ing');
                    // 重新开始录音
                    setTimeout(() => {
                        this.webSocketSend();
                    }, 500);
                    console.log('打开连接');
                };
                iatWS.onmessage = (e) => {
                    this.result(e.data);
                };
                iatWS.onerror = (e) => {
                    this.recorderStop();
                };
                iatWS.onclose = (e) => {
                    this.recorderStop();
                    console.log('关闭连接');
                };
            });
        }
        /**
         * 初始化浏览器录音
         */
        recorderInit() {
            navigator.getUserMedia =
                navigator.getUserMedia ||
                    navigator.webkitGetUserMedia ||
                    navigator.mozGetUserMedia ||
                    navigator.msGetUserMedia;
            // 创建音频环境
            try {
                this.audioContext = new (window.AudioContext || window.webkitAudioContext)();
                this.audioContext.resume();
                if (!this.audioContext) {
                    alert('浏览器不支持webAudioApi相关接口');
                    return false;
                }
            }
            catch (e) {
                if (!this.audioContext) {
                    alert('浏览器不支持webAudioApi相关接口');
                    return false;
                }
            }
            // 获取浏览器录音权限
            if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
                navigator.mediaDevices.getUserMedia({
                    audio: true,
                    video: false
                }).then(stream => {
                    getMediaSuccess(stream);
                }).catch((e) => {
                    getMediaFail(e);
                });
            }
            else if (navigator.getUserMedia) {
                navigator.getUserMedia({
                    audio: true,
                    video: false
                }, (stream) => {
                    getMediaSuccess(stream);
                }, function (e) {
                    getMediaFail(e);
                });
            }
            else {
                if (navigator.userAgent.toLowerCase().match(/chrome/) && location.origin.indexOf('https://') < 0) {
                    console.error('获取浏览器录音功能，因安全性问题，需要在localhost 或 127.0.0.1 或 https 下才能获取权限！');
                }
                else {
                    alert('对不起：未识别到录音设备!');
                }
                this.audioContext && this.audioContext.close();
                return false;
            }
            // 获取浏览器录音权限成功时回调
            let getMediaSuccess = (stream) => {
                // 创建一个用于通过JavaScript直接处理音频
                this.scriptProcessor = this.audioContext.createScriptProcessor(0, 1, 1);
                this.scriptProcessor.onaudioprocess = (e) => {
                    if (this.status === 'ing') {
                        transWorker.postMessage(e.inputBuffer.getChannelData(0));
                    }
                };
                // 创建一个新的MediaStreamAudioSourceNode 对象，使来自MediaStream的音频可以被播放和操作
                this.mediaSource = this.audioContext.createMediaStreamSource(stream);
                // 连接
                this.mediaSource.connect(this.scriptProcessor);
                this.scriptProcessor.connect(this.audioContext.destination);
                this.connectWebSocket();
            };
            // 获取浏览器录音权限失败时回调
            let getMediaFail = (e) => {
                // alert('获取MediaStream对象失败');
                alert(e);
                console.log(e);
                this.audioContext && this.audioContext.close();
                this.audioContext = undefined;
                // 关闭websocket
                if (this.webSocket && this.webSocket.readyState === 1) {
                    this.webSocket.close();
                }
            };
        }
        /**
         * 启动录音
         */
        recorderStart() {
            if (!this.audioContext) {
                this.recorderInit();
            }
            else {
                this.audioContext.resume();
                this.connectWebSocket();
            }
        }
        /**
         * 停止录音
         */
        recorderStop() {
            if (!(/Safari/.test(navigator.userAgent) && !/Chrome/.test(navigator.userAgen))) {
                // safari下suspend后再次resume录音内容将是空白，设置safari下不做suspend
                this.audioContext && this.audioContext.suspend();
            }
            this.setStatus('end');
        }
        /**
         * 向webSocket发送数据
         */
        webSocketSend() {
            if (this.webSocket.readyState !== 1) {
                return false;
            }
            let audioData = this.audioData.splice(0, FRAME_SIZE);
            let params = {
                common: {
                    app_id: this.appId,
                },
                business: {
                    language: this.language,
                    domain: 'iat',
                    accent: this.accent,
                    vad_eos: this.vad, // 开始录入音频后，音频后面部分最长静音时长取值范围[0,10000]
                    // dwa: 'wpgs', //为使该功能生效，需到控制台开通动态修正功能（该功能免费）
                },
                data: {
                    status: 0,
                    format: 'audio/L16;rate=16000',
                    encoding: 'raw',
                    audio: this.toBase64(audioData),
                },
            };
            this.webSocket.send(JSON.stringify(params));
            this.handlerInterval = setInterval(() => {
                // websocket未连接
                if (this.webSocket.readyState !== 1) {
                    this.audioData = [];
                    clearInterval(this.handlerInterval);
                    return false;
                }
                if (this.status === 'end') {
                    this.webSocket.send(JSON.stringify({
                        data: {
                            status: 2,
                            format: 'audio/L16;rate=16000',
                            encoding: 'raw',
                            audio: '',
                        },
                    }));
                    this.audioData = [];
                    clearInterval(this.handlerInterval);
                    return false;
                }
                audioData = this.audioData.splice(0, FRAME_SIZE);
                // 中间帧
                this.webSocket.send(JSON.stringify({
                    data: {
                        status: 1,
                        format: 'audio/L16;rate=16000',
                        encoding: 'raw',
                        audio: this.toBase64(audioData),
                    },
                }));
            }, 40);
        }
        /**
         * 识别结果
         *
         * @params resultData webSocket返回数据
         */
        result(resultData) {
            // 识别结束
            let jsonData = JSON.parse(resultData);
            if (jsonData.code !== 0) {
                this.webSocket.close();
                console.log(`${jsonData.code}:${jsonData.message}`);
            }
            if (jsonData.code === 0 && jsonData.data.status === 2) {
                this.setStatus('end');
                this.webSocket.close();
            }
            if (jsonData.data && jsonData.data.result) {
                let data = jsonData.data.result;
                let str = '';
                let ws = data.ws;
                for (let i = 0; i < ws.length; i++) {
                    str = str + ws[i].cw[0].w;
                }
                // 开启wpgs会有此字段(前提：在控制台开通动态修正功能)
                // 取值为 "apd"时表示该片结果是追加到前面的最终结果；取值为"rpl" 时表示替换前面的部分结果，替换范围为rg字段
                if (data.pgs) {
                    // 动态修正功能暂未实现 TODO
                }
                else {
                    this.setResultText(this.resultText + str);
                }
            }
        }
        /**
         * 开始调用语音识别接口
         */
        start() {
            this.recorderStart();
            this.setResultText('', '');
        }
        ;
        /**
         * 停止调用语音识别接口
         */
        stop() {
            this.recorderStop();
        }
        ;
        // 获取请求websocket的url
        getWebSocketUrl() {
            return new Promise((resolve, reject) => {
                // 请求地址根据语种不同变化
                let url = 'wss://iat-api.xfyun.cn/v2/iat';
                let host = 'iat-api.xfyun.cn';
                let apiKey = API_KEY;
                let apiSecret = API_SECRET;
                let date = new Date().toUTCString();
                let algorithm = 'hmac-sha256';
                let headers = 'host date request-line';
                let signatureOrigin = `host: ${host}\ndate: ${date}\nGET /v2/iat HTTP/1.1`;
                let signatureSha = crypto_js_min_js_1.default.HmacSHA256(signatureOrigin, apiSecret);
                let signature = crypto_js_min_js_1.default.enc.Base64.stringify(signatureSha);
                let authorizationOrigin = `api_key="${apiKey}", algorithm="${algorithm}", headers="${headers}", signature="${signature}"`;
                let authorization = btoa(authorizationOrigin);
                url = `${url}?authorization=${authorization}&date=${date}&host=${host}`;
                resolve(url);
            });
        }
    }
    exports.Yytx = Yytx;
    ;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaWZseXRlay15eXR4LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiaWZseXRlay15eXR4LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7OztHQVFHOzs7Ozs7Ozs7SUFHSCxNQUFNLE9BQU8sR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDO0lBQzlCLE1BQU0sUUFBUSxHQUFHLFFBQVEsQ0FBQyxRQUFRLENBQUM7SUFDbkMsTUFBTSxXQUFXLEdBQUcsSUFBSSxNQUFNLENBQUMsR0FBRyxRQUFRLEtBQUssT0FBTyxvRUFBb0UsQ0FBQyxDQUFDO0lBQzVILE1BQU0sS0FBSyxHQUFHLFVBQVUsQ0FBQztJQUN6QixNQUFNLFVBQVUsR0FBRyxrQ0FBa0MsQ0FBQztJQUN0RCxNQUFNLE9BQU8sR0FBRyxrQ0FBa0MsQ0FBQztJQUNuRCxjQUFjO0lBQ2QsTUFBTSxVQUFVLEdBQUcsSUFBSSxDQUFDO0lBRXhCLGVBQWU7SUFDZixNQUFhLElBQUk7UUFJYixZQUFZLEVBQUUsS0FBSyxFQUFFLEdBQUcsRUFBRSxRQUFRLEVBQUUsTUFBTSxFQUFFLEdBQUcsRUFBRTtZQUM3QyxJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7WUFDaEIsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7WUFDckIsSUFBSSxDQUFDLFFBQVEsR0FBRyxRQUFRLElBQUksT0FBTyxDQUFDO1lBQ3BDLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxJQUFJLFVBQVUsQ0FBQztZQUNuQyxJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssSUFBSSxLQUFLLENBQUM7WUFDNUIsSUFBSSxHQUFHLEVBQUU7Z0JBQ0wsSUFBSSxDQUFDLEdBQUcsR0FBRyxHQUFHLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQzthQUN2QztpQkFBTTtnQkFDSCxJQUFJLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQzthQUNuQjtZQUNELFNBQVM7WUFDVCxJQUFJLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQztZQUNwQixTQUFTO1lBQ1QsSUFBSSxDQUFDLFVBQVUsR0FBRyxFQUFFLENBQUM7WUFDckIsdUJBQXVCO1lBQ3ZCLElBQUksQ0FBQyxjQUFjLEdBQUcsRUFBRSxDQUFDO1lBQ3pCLFdBQVcsQ0FBQyxTQUFTLEdBQUcsVUFBVSxLQUFLO2dCQUNuQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxHQUFHLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUN2QyxDQUFDLENBQUE7WUFDRCxJQUFJLENBQUMsYUFBYSxHQUFHO2dCQUNqQixZQUFZLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO2FBQzdDLENBQUE7UUFDTCxDQUFDO1FBRUQ7Ozs7V0FJRztRQUNILFNBQVMsQ0FBQyxNQUFNO1lBQ1osSUFBSSxDQUFDLGtCQUFrQixJQUFJLElBQUksQ0FBQyxNQUFNLEtBQUssTUFBTSxJQUFJLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLE1BQU0sQ0FBQyxDQUFDO1lBQ2xHLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO1FBQ3pCLENBQUM7UUFFRDs7OztXQUlHO1FBQ0gsU0FBUztZQUNMLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUN2QixDQUFDO1FBRUQ7Ozs7O1dBS0c7UUFDSCxhQUFhLENBQUMsVUFBVyxFQUFFLGNBQWU7WUFDdEMsSUFBSSxDQUFDLFlBQVksSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDLGNBQWMsSUFBSSxVQUFVLElBQUksRUFBRSxDQUFDLENBQUM7WUFDM0UsVUFBVSxLQUFLLFNBQVMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLEdBQUcsVUFBVSxDQUFDLENBQUM7WUFDM0QsY0FBYyxLQUFLLFNBQVMsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLEdBQUcsY0FBYyxDQUFDLENBQUM7UUFDM0UsQ0FBQztRQUVEOzs7Ozs7V0FNRztRQUNILFNBQVMsQ0FBQyxHQUFHLEVBQUUsUUFBUSxFQUFFLE1BQU07WUFDM0IsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUMsQ0FBQztZQUN4QixRQUFRLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQyxDQUFDO1lBQ3ZDLE1BQU0sSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDLENBQUM7UUFDckMsQ0FBQztRQUVEOzs7O1dBSUc7UUFDSCxRQUFRLENBQUMsTUFBTTtZQUNYLElBQUksTUFBTSxHQUFHLEVBQUUsQ0FBQztZQUNoQixJQUFJLEtBQUssR0FBRyxJQUFJLFVBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNuQyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsS0FBSyxDQUFDLFVBQVUsRUFBRSxDQUFDLEVBQUUsRUFBRTtnQkFDdkMsTUFBTSxJQUFJLE1BQU0sQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDM0M7WUFDRCxPQUFPLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDL0IsQ0FBQztRQUVEOztXQUVHO1FBQ0gsZ0JBQWdCO1lBQ1osT0FBTyxJQUFJLENBQUMsZUFBZSxFQUFFLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFO2dCQUNyQyxJQUFJLEtBQUssQ0FBQztnQkFDVixJQUFJLFdBQVcsSUFBSSxNQUFNLEVBQUU7b0JBQ3ZCLEtBQUssR0FBRyxJQUFJLFNBQVMsQ0FBTSxHQUFHLENBQUMsQ0FBQztpQkFDbkM7cUJBQU0sSUFBSSxjQUFjLElBQUksTUFBTSxFQUFFO29CQUNqQyxLQUFLLEdBQUcsSUFBSSxZQUFZLENBQUMsR0FBRyxDQUFDLENBQUM7aUJBQ2pDO3FCQUFNO29CQUNILEtBQUssQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO29CQUMxQixPQUFPLEtBQUssQ0FBQztpQkFDaEI7Z0JBQ0QsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7Z0JBQ3ZCLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQ3ZCLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLEVBQUUsRUFBRTtvQkFDakIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDdEIsU0FBUztvQkFDVCxVQUFVLENBQUMsR0FBRyxFQUFFO3dCQUNaLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztvQkFDekIsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFBO29CQUNQLE9BQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQ3hCLENBQUMsQ0FBQTtnQkFDRCxLQUFLLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQyxFQUFFLEVBQUU7b0JBQ3BCLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUN4QixDQUFDLENBQUE7Z0JBQ0QsS0FBSyxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUMsRUFBRSxFQUFFO29CQUNsQixJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7Z0JBQ3hCLENBQUMsQ0FBQTtnQkFDRCxLQUFLLENBQUMsT0FBTyxHQUFHLENBQUMsQ0FBQyxFQUFFLEVBQUU7b0JBQ2xCLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztvQkFDcEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDeEIsQ0FBQyxDQUFBO1lBQ0wsQ0FBQyxDQUFDLENBQUE7UUFDTixDQUFDO1FBRUQ7O1dBRUc7UUFDSCxZQUFZO1lBQ1IsU0FBUyxDQUFDLFlBQVk7Z0JBQ2xCLFNBQVMsQ0FBQyxZQUFZO29CQUN0QixTQUFTLENBQUMsa0JBQWtCO29CQUM1QixTQUFTLENBQUMsZUFBZTtvQkFDekIsU0FBUyxDQUFDLGNBQWMsQ0FBQztZQUU3QixTQUFTO1lBQ1QsSUFBSTtnQkFDQSxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsWUFBWSxJQUFJLE1BQU0sQ0FBQyxrQkFBa0IsQ0FBQyxFQUFFLENBQUM7Z0JBQzdFLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxFQUFFLENBQUM7Z0JBQzNCLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFO29CQUNwQixLQUFLLENBQUMsdUJBQXVCLENBQUMsQ0FBQztvQkFDL0IsT0FBTyxLQUFLLENBQUM7aUJBQ2hCO2FBQ0o7WUFBQyxPQUFPLENBQUMsRUFBRTtnQkFDUixJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRTtvQkFDcEIsS0FBSyxDQUFDLHVCQUF1QixDQUFDLENBQUM7b0JBQy9CLE9BQU8sS0FBSyxDQUFDO2lCQUNoQjthQUNKO1lBQ0QsWUFBWTtZQUNaLElBQUksU0FBUyxDQUFDLFlBQVksSUFBSSxTQUFTLENBQUMsWUFBWSxDQUFDLFlBQVksRUFBRTtnQkFDL0QsU0FBUyxDQUFDLFlBQVksQ0FBQyxZQUFZLENBQUM7b0JBQ2hDLEtBQUssRUFBRSxJQUFJO29CQUNYLEtBQUssRUFBRSxLQUFLO2lCQUNmLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUU7b0JBQ2IsZUFBZSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUM1QixDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRTtvQkFDWCxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3BCLENBQUMsQ0FBQyxDQUFBO2FBQ0w7aUJBQU0sSUFBSSxTQUFTLENBQUMsWUFBWSxFQUFFO2dCQUMvQixTQUFTLENBQUMsWUFBWSxDQUFDO29CQUNuQixLQUFLLEVBQUUsSUFBSTtvQkFDWCxLQUFLLEVBQUUsS0FBSztpQkFDZixFQUFFLENBQUMsTUFBTSxFQUFFLEVBQUU7b0JBQ1YsZUFBZSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUM1QixDQUFDLEVBQUUsVUFBVSxDQUFDO29CQUNWLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDcEIsQ0FBQyxDQUFDLENBQUE7YUFDTDtpQkFBTTtnQkFDSCxJQUFJLFNBQVMsQ0FBQyxTQUFTLENBQUMsV0FBVyxFQUFFLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxJQUFJLFFBQVEsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsRUFBRTtvQkFDOUYsT0FBTyxDQUFDLEtBQUssQ0FBQyw0REFBNEQsQ0FBQyxDQUFDO2lCQUMvRTtxQkFBTTtvQkFDSCxLQUFLLENBQUMsZUFBZSxDQUFDLENBQUM7aUJBQzFCO2dCQUNELElBQUksQ0FBQyxZQUFZLElBQUksSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLEVBQUUsQ0FBQztnQkFDL0MsT0FBTyxLQUFLLENBQUM7YUFDaEI7WUFDRCxpQkFBaUI7WUFDakIsSUFBSSxlQUFlLEdBQUcsQ0FBQyxNQUFNLEVBQUUsRUFBRTtnQkFDN0IsMkJBQTJCO2dCQUMzQixJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMscUJBQXFCLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDeEUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxjQUFjLEdBQUcsQ0FBQyxDQUFDLEVBQUUsRUFBRTtvQkFDeEMsSUFBSSxJQUFJLENBQUMsTUFBTSxLQUFLLEtBQUssRUFBRTt3QkFDdkIsV0FBVyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO3FCQUM1RDtnQkFDTCxDQUFDLENBQUE7Z0JBQ0QsZ0VBQWdFO2dCQUNoRSxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsdUJBQXVCLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQ3JFLEtBQUs7Z0JBQ0wsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO2dCQUMvQyxJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dCQUM1RCxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztZQUM1QixDQUFDLENBQUE7WUFDRCxpQkFBaUI7WUFDakIsSUFBSSxZQUFZLEdBQUcsQ0FBQyxDQUFDLEVBQUUsRUFBRTtnQkFDckIsOEJBQThCO2dCQUM5QixLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ1QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDZixJQUFJLENBQUMsWUFBWSxJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxFQUFFLENBQUM7Z0JBQy9DLElBQUksQ0FBQyxZQUFZLEdBQUcsU0FBUyxDQUFDO2dCQUM5QixjQUFjO2dCQUNkLElBQUksSUFBSSxDQUFDLFNBQVMsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsS0FBSyxDQUFDLEVBQUU7b0JBQ25ELElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLENBQUM7aUJBQzFCO1lBQ0wsQ0FBQyxDQUFBO1FBQ0wsQ0FBQztRQUVEOztXQUVHO1FBQ0gsYUFBYTtZQUNULElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFO2dCQUNwQixJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7YUFDdkI7aUJBQU07Z0JBQ0gsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLEVBQUUsQ0FBQztnQkFDM0IsSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7YUFDM0I7UUFDTCxDQUFDO1FBRUQ7O1dBRUc7UUFDSCxZQUFZO1lBQ1IsSUFBSSxDQUFDLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxFQUFFO2dCQUM3RSxxREFBcUQ7Z0JBQ3JELElBQUksQ0FBQyxZQUFZLElBQUksSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLEVBQUUsQ0FBQzthQUNwRDtZQUNELElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDMUIsQ0FBQztRQUVEOztXQUVHO1FBQ0gsYUFBYTtZQUNULElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLEtBQUssQ0FBQyxFQUFFO2dCQUNqQyxPQUFPLEtBQUssQ0FBQzthQUNoQjtZQUNELElBQUksU0FBUyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxVQUFVLENBQUMsQ0FBQztZQUNyRCxJQUFJLE1BQU0sR0FBRztnQkFDVCxNQUFNLEVBQUU7b0JBQ0osTUFBTSxFQUFFLElBQUksQ0FBQyxLQUFLO2lCQUNyQjtnQkFDRCxRQUFRLEVBQUU7b0JBQ04sUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFRO29CQUN2QixNQUFNLEVBQUUsS0FBSztvQkFDYixNQUFNLEVBQUUsSUFBSSxDQUFDLE1BQU07b0JBQ25CLE9BQU8sRUFBRSxJQUFJLENBQUMsR0FBRyxFQUFFLG9DQUFvQztvQkFDdkQsOENBQThDO2lCQUNqRDtnQkFDRCxJQUFJLEVBQUU7b0JBQ0YsTUFBTSxFQUFFLENBQUM7b0JBQ1QsTUFBTSxFQUFFLHNCQUFzQjtvQkFDOUIsUUFBUSxFQUFFLEtBQUs7b0JBQ2YsS0FBSyxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDO2lCQUNsQzthQUNKLENBQUE7WUFDRCxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFDNUMsSUFBSSxDQUFDLGVBQWUsR0FBRyxXQUFXLENBQUMsR0FBRyxFQUFFO2dCQUNwQyxlQUFlO2dCQUNmLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLEtBQUssQ0FBQyxFQUFFO29CQUNqQyxJQUFJLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQztvQkFDcEIsYUFBYSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQztvQkFDcEMsT0FBTyxLQUFLLENBQUM7aUJBQ2hCO2dCQUNELElBQUksSUFBSSxDQUFDLE1BQU0sS0FBSyxLQUFLLEVBQUU7b0JBQ3ZCLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUNmLElBQUksQ0FBQyxTQUFTLENBQUM7d0JBQ1gsSUFBSSxFQUFFOzRCQUNGLE1BQU0sRUFBRSxDQUFDOzRCQUNULE1BQU0sRUFBRSxzQkFBc0I7NEJBQzlCLFFBQVEsRUFBRSxLQUFLOzRCQUNmLEtBQUssRUFBRSxFQUFFO3lCQUNaO3FCQUNKLENBQUMsQ0FDTCxDQUFDO29CQUNGLElBQUksQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDO29CQUNwQixhQUFhLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO29CQUNwQyxPQUFPLEtBQUssQ0FBQztpQkFDaEI7Z0JBQ0QsU0FBUyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxVQUFVLENBQUMsQ0FBQztnQkFDakQsTUFBTTtnQkFDTixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FDZixJQUFJLENBQUMsU0FBUyxDQUFDO29CQUNYLElBQUksRUFBRTt3QkFDRixNQUFNLEVBQUUsQ0FBQzt3QkFDVCxNQUFNLEVBQUUsc0JBQXNCO3dCQUM5QixRQUFRLEVBQUUsS0FBSzt3QkFDZixLQUFLLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUM7cUJBQ2xDO2lCQUNKLENBQUMsQ0FDTCxDQUFDO1lBQ04sQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFBO1FBQ1YsQ0FBQztRQUVEOzs7O1dBSUc7UUFDSCxNQUFNLENBQUMsVUFBVTtZQUNiLE9BQU87WUFDUCxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ3RDLElBQUksUUFBUSxDQUFDLElBQUksS0FBSyxDQUFDLEVBQUU7Z0JBQ3JCLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLENBQUM7Z0JBQ3ZCLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxRQUFRLENBQUMsSUFBSSxJQUFJLFFBQVEsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDO2FBQ3ZEO1lBQ0QsSUFBSSxRQUFRLENBQUMsSUFBSSxLQUFLLENBQUMsSUFBSSxRQUFRLENBQUMsSUFBSSxDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUU7Z0JBQ25ELElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ3RCLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLENBQUM7YUFDMUI7WUFDRCxJQUFJLFFBQVEsQ0FBQyxJQUFJLElBQUksUUFBUSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUU7Z0JBQ3ZDLElBQUksSUFBSSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDO2dCQUNoQyxJQUFJLEdBQUcsR0FBRyxFQUFFLENBQUM7Z0JBQ2IsSUFBSSxFQUFFLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQztnQkFDakIsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7b0JBQ2hDLEdBQUcsR0FBRyxHQUFHLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7aUJBQzdCO2dCQUNELCtCQUErQjtnQkFDL0IsOERBQThEO2dCQUM5RCxJQUFJLElBQUksQ0FBQyxHQUFHLEVBQUU7b0JBQ1Ysa0JBQWtCO2lCQUNyQjtxQkFBTTtvQkFDSCxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxVQUFVLEdBQUcsR0FBRyxDQUFDLENBQUM7aUJBQzdDO2FBQ0o7UUFDTCxDQUFDO1FBRUQ7O1dBRUc7UUFDSCxLQUFLO1lBQ0QsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1lBQ3JCLElBQUksQ0FBQyxhQUFhLENBQUMsRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDO1FBQy9CLENBQUM7UUFBQSxDQUFDO1FBRUY7O1dBRUc7UUFDSCxJQUFJO1lBQ0EsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1FBQ3hCLENBQUM7UUFBQSxDQUFDO1FBRUYsb0JBQW9CO1FBQ3BCLGVBQWU7WUFDWCxPQUFPLElBQUksT0FBTyxDQUFDLENBQUMsT0FBTyxFQUFFLE1BQU0sRUFBRSxFQUFFO2dCQUNuQyxlQUFlO2dCQUNmLElBQUksR0FBRyxHQUFHLCtCQUErQixDQUFBO2dCQUN6QyxJQUFJLElBQUksR0FBRyxrQkFBa0IsQ0FBQTtnQkFDN0IsSUFBSSxNQUFNLEdBQUcsT0FBTyxDQUFBO2dCQUNwQixJQUFJLFNBQVMsR0FBRyxVQUFVLENBQUE7Z0JBQzFCLElBQUksSUFBSSxHQUFHLElBQUksSUFBSSxFQUFFLENBQUMsV0FBVyxFQUFFLENBQUM7Z0JBQ3BDLElBQUksU0FBUyxHQUFHLGFBQWEsQ0FBQTtnQkFDN0IsSUFBSSxPQUFPLEdBQUcsd0JBQXdCLENBQUE7Z0JBQ3RDLElBQUksZUFBZSxHQUFHLFNBQVMsSUFBSSxXQUFXLElBQUksd0JBQXdCLENBQUE7Z0JBQzFFLElBQUksWUFBWSxHQUFHLDBCQUFRLENBQUMsVUFBVSxDQUFDLGVBQWUsRUFBRSxTQUFTLENBQUMsQ0FBQTtnQkFDbEUsSUFBSSxTQUFTLEdBQUcsMEJBQVEsQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQUMsQ0FBQTtnQkFDM0QsSUFBSSxtQkFBbUIsR0FBRyxZQUFZLE1BQU0saUJBQWlCLFNBQVMsZUFBZSxPQUFPLGlCQUFpQixTQUFTLEdBQUcsQ0FBQTtnQkFDekgsSUFBSSxhQUFhLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUE7Z0JBQzdDLEdBQUcsR0FBRyxHQUFHLEdBQUcsa0JBQWtCLGFBQWEsU0FBUyxJQUFJLFNBQVMsSUFBSSxFQUFFLENBQUE7Z0JBQ3ZFLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQTtZQUNoQixDQUFDLENBQUMsQ0FBQTtRQUNOLENBQUM7S0FDSjtJQXpXRCxvQkF5V0M7SUFBQSxDQUFDIn0=