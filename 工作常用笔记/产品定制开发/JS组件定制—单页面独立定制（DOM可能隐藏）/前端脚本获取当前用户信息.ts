/**  导入的路径在产品代码的的最上方 */
import { SZEvent, Component, rc, browser, showInfoDialog, showWaiting, downloadFile, showMenu, deepEqual, showSuccessMessage, showErrorMessage, BASIC_EVENT, showConfirmDialog } from 'sys/sys';
import { ICustomJS, IMetaFileCustomJS, InterActionEvent, IVPage, IAppCustomJS, DatasetDataPackageRowInfo, IVComponent, InputDataChangeInfo } from "metadata/metadata-script-api";
import { TableCellEditor } from "commons/table";
import { Paginator } from "commons/basic";
import { SuperPageBuilder } from "app/superpage/superpagebuilder";
import { SpgListBuilder, SpgListColumnBuilder } from "app/superpage/components/tables";
import { BaseNodeBuilder } from "metadata/bo/builder";
import { AnaDataset } from "ana/datamgr";
import { showMetaFileDialog, showDrillPage, getCurrentUser, IMetaFileViewer, CurrentUser, MetaFileViewer } from 'metadata/metadata';


export const CustomJS: { [type: string]: IMetaFileCustomJS } = {
    /**
     * 当前项目的所有spg都会进到此方法
     */
    spg: {
        CustomActions: {
            /** 
             * tuzw test
             * 测试获取用户的用户组
             */
            test_getUserGroup: (event: InterActionEvent) => {
                checkCurrentUserGroupAuthority();
            }
        }
    }
}


/**
 * 校验当前用户组是否属于：卫健委机构 或 医疗机构列
 * @return boolean
 */
function checkCurrentUserGroupAuthority(): boolean {
    /** 指定用户组ID集 */
    const adminGroupIds = ["admin", "wjwyh"];
    /** 当前用户所在的用户组ID集 */
    let currentUserGroupIds = getCurrentUserGroupIds();
    for (let i = 0; i < currentUserGroupIds.length; i++) {
        if (adminGroupIds.indexOf(currentUserGroupIds[i]) != -1) {
            return true;
        }
    }
    return false;
}

/**
 * 获取当前登录用户所在用户组的ID集
 * 
 * eg:[{
 *      enabled: true
        groupId: "wjwyh"
        groupName: "卫健委"
 *    },{
 *      enabled: true
        groupId: "admin"
        groupName: "admin"
 * }]
 * 转换为：["wjwyh", "admin"]
 */
function getCurrentUserGroupIds(): Array<string> {
    let currentUserGroups: UserGroupInfo[] = getCurrentUser().groups;
    let currentUserGroupIds: Array<string> = currentUserGroups.map(item => {
        return item.groupId;
    });
    return currentUserGroupIds;
}

