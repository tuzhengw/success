
import {
	throwError, assign, message, rc, Component, showSuccessMessage, isEmpty, showWarningMessage, SZEvent, uuid, showWaiting
} from "sys/sys";
import { IMetaFileCustomJS } from "metadata/metadata";
import { InterActionEvent } from "metadata/metadata-script-api";
import { ReportData } from "ana/rpt/reportdatamgr";
import { Report } from "ana/rpt/report";
import { Combobox, ComboboxFloatPanel } from "commons/basic";
import { ListItem } from "commons/tree";
import type { AnaObjectData } from "ana/datamgr";
import { getQueryManager } from "dw/dwapi";
import { MetaFileViewer_AnaObject } from "ana/anabrowser";

/**
 * 指标灵活查询页面导出
 * <p>
 *  2023-05-24 tuzw
 *  地址: https://jira.succez.com/browse/CSTM-22977
 * </p>
 * @param embedreportId 导出报表ID, 默认: embedreport1
 * @param fileName 文件名, 默认: 当前报表所在SPG描述, 无描述则名称
 * @return 
 */
private exportIndexRelationModelDatas(event: InterActionEvent): Promise<ExportAnaObjectResult> | void {
	let page = event.page;
	let params = event.params;
	let embedreportId: string = params?.embedreportId;
	if (isEmpty(embedreportId)) {
		embedreportId = "embedreport1";
	}

	let embedreportComp = page.getComponent(embedreportId);
	if (isEmpty(embedreportComp)) {
		showWarningMessage(`未找到导出报表对象`);
		return;
	}
	let pageInfo = page.getFileInfo()
	let fileName: string = params?.fileName;
	if (isEmpty(fileName)) {
		fileName = !!pageInfo.desc ? pageInfo.desc : pageInfo.name;
	}
	let metaFileViewer: MetaFileViewer_AnaObject = embedreportComp["component"].getInnerComoponent();
	let report = metaFileViewer.getAnaObject() as unknown as Report;
	return report.exportExcel({ // 列表导出和报表导出区别在于: 报表内部嵌入了聚合函数
		path: pageInfo.id,
		fileName: fileName,
		allPageData: true
	});
}