import 'css!./customComponent.css';
import { Component, formatDate, UrlInfo, ComponentArgs } from 'sys/sys';
import { SettingFormPage_securityConf, SettingPage_securityConf } from 'metadata/syssettings/syssettings-securityConf';


/**
 注意：方法是否携带 "default"
	若携带，则JS组件无法显示，只能门户中使用
  export default class
*/


/**
 * SettingPage_securityConf类
 * 安全策略设置界面，包含子面板：密码安全，登录安全，安全等保，阈值设置，回归测试，数据脱敏，数据加密
 * 用法：
 * （1）SPG内使用JS组件，注意：在custom.less文件中可以设置全局样式
 * （2）直接将代码拖到门户中
 
 * export default：门户显示
 * export ：JS组件显示
 */
export class SoleSystemSecurtiyAdaptPage extends SettingPage_securityConf {
    /**
     * 注意：通过js组件去内嵌一个 系统设置页面后，系统设置页面只占有了页面的一部分，不会铺满
     * 解决办法：https://jira.succez.com/browse/BI-41364
     * （
     *  （1）通过脚本去掉最外层面板的spgcomponent-body的display属性样式
     *  （2）JS组件设置交互————触发脚本
     * ）
     * args：JS组件（DOM）
	 
	 * 系统版本：4.19
     */
    protected _init(args: any): HTMLElement {
		/**
         * 注意：在custom.less中将部分设置了隐藏
         
			// 隐藏系统设置-回归测试
			#regressionTesting {
				display: none!important;
			}
			// 隐藏系统设置-安全登报
			#securityLevelProtection {
				display: none!important;
			}
			// 隐藏系统设置-阈值设置
			#thresholds {
			display: none!important;
			}
			// 隐藏系统设置-日志记录
			#log {
				display: none!important;
			}
         */
        let domBase = super._init(args);
        return domBase;
    }
}




/**
 * 系统模块——安全页面
 * 系统版本：4.20 
 */
export class SoleSystemSecurtiyAdaptPage extends SettingPage_securityConf {
	protected _init(args: any): HTMLElement {
		let domBase = super._init(args);
		return domBase;
	}

	/**
	 * @override
	 * 控制页面显示的模块，要求隐藏：回归测试、安全等保、阈值设置、日志记录
	 */
	protected getTabbarArgs(): TabbarArgs {
		return {
			layoutTheme: "texttabset",
			className: "syssettings-web-tabbar",
			keyPrefix: "sys.syssettings.securityConf",
			items: [
				{ id: 'passwordSettings' },
				{ id: 'loginSettings' },
				{ id: 'sessionSettings' },
				// { id: 'securityLevelProtection' },
				// { id: 'thresholds' },
				// { id: 'regressionTesting' },
				// { id: 'log' },
				{ id: 'masking' },
				{ id: 'algorithms' },
				// { id: 'sys' }
			],
			value: 'passwordSettings', // 默认选项不可以为空
		};
	}
}