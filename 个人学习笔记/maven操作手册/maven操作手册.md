---
typora-root-url: images
---

# Maven操作手册

## 1 以树的形式展示jar引用形式

```
mvn dependency:tree
```

## 2 打包

### 2.1 maven打包

```
mvn clean package -Dspotless.check.skip=true
```

​	若pom.xml内没有配置maven-shade-plugin，一般打包不会将依赖jar合并.

### 2.2 不同jar同名类相互覆盖

​	jar包类的包路径和类名可能与其他jar包一致，程序加载的时候无法准确识别要加载对应的类，可能会加载错误的类。

​	以`Flink cdc `jar作为示例：

```
flink-connector-mysql-cdc-2.4.0.jar
debezium-connector-mysql-1.9.7.Final.jar
```

​	上述两个jar包就存在不同jar同路径同类名的问题，可参考：https://github.com/ververica/flink-cdc-connectors/issues/2423，

```
java.lang.NoSuchMethodError: io.debezium.connector.mysql.MySqlConnection$MySqlConnectionConfiguration.(Lio/debezium/config/Configuration;Ljava/util/Properties;)V
```

​	在创建MySqlConnectionConfiguration的实例对象时，程序没有找到正确的类，导致双方类名虽然一致，但其构造参数类型和数量都不一致，从而创建实例失败。

​	解决办法：下载`CDC`对应版本分支源码，自行本地打包（fat jar），`CDC`本身自带`maven-shade-plugin`插件，可将项目的依赖jar放入到jar中，直接: `mvn install` or `mvn package`。

​	打fat jar可参考：https://zhuanlan.zhihu.com/p/43238220

### 2.3 `Maven Shade Plugin`介绍

​		遮蔽方法会把依赖包里的类路径进行修改到某个子路径下，这样可以一定程度上**避免同名类相互覆盖**的问题，最终发布的 jar 也不会带入传递依赖冲突问题给下游。

​	要在IDEA中配置maven-shade-plugin来处理多个子项目，以`Flink CDC `为例，您可以按照以下步骤进行操作：

1. 打开您的父项目的pom.xml文件。
2. 在`<build>`标签下添加以下配置：

```xml
<build>
    <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-shade-plugin</artifactId>
            <executions>
                <execution>
                    <id>shade-flink</id>
                    <phase>package</phase>
                    <goals>
                        <goal>shade</goal>
                    </goals>
                    <configuration>
                        <!-- Shading test jar have bug in some previous version, so close this configuration here,
                        see https://issues.apache.org/jira/browse/MSHADE-284 -->
                        <shadeTestJar>false</shadeTestJar>
                        <shadedArtifactAttached>false</shadedArtifactAttached>
                        <createDependencyReducedPom>true</createDependencyReducedPom>
                        <dependencyReducedPomLocation>
                            ${project.basedir}/target/dependency-reduced-pom.xml
                        </dependencyReducedPomLocation>
                        <filters combine.children="append">
                            <filter>
                                <artifact>*:*</artifact>
                                <excludes>
                                    <exclude>module-info.class</exclude>
                                    <exclude>META-INF/*.SF</exclude>
                                    <exclude>META-INF/*.DSA</exclude>
                                    <exclude>META-INF/*.RSA</exclude>
                                </excludes>
                            </filter>
                        </filters>
                        <artifactSet>
                            <includes>
                                <!-- include nothing -->
                                <include></include>
                            </includes>
                        </artifactSet>
                    </configuration>
                </execution>
            </executions>
        </plugin>
</build>
```

​	请注意，您需要将`your.main.class`替换为您的主类的完整路径。

1. 对于每个子项目，您可以在其pom.xml文件中添加以下配置：

```xml
<build>
    <plugins>
         <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-shade-plugin</artifactId>
                <version>3.2.4</version>
                <executions>
                    <execution>
                        <id>shade-flink</id>
                        <phase>package</phase>
                        <goals>
                            <goal>shade</goal>
                        </goals>
                        <configuration>
                            <shadeTestJar>false</shadeTestJar>
                            <artifactSet>
                                <includes>
                                    <include>io.debezium:debezium-api</include>
                                    <include>io.debezium:debezium-embedded</include>
                                    <include>io.debezium:debezium-core</include>
                                    <include>io.debezium:debezium-ddl-parser</include>
                                    <include>io.debezium:debezium-connector-mysql</include>
                                    <include>com.ververica:flink-connector-debezium</include>
                                    <include>com.ververica:flink-connector-mysql-cdc</include>
                                    <include>org.antlr:antlr4-runtime</include>
                                    <include>org.apache.kafka:*</include>
                                    <include>mysql:mysql-connector-java</include>
                                    <include>com.zendesk:mysql-binlog-connector-java</include>
                                    <include>com.fasterxml.*:*</include>
                                    <include>com.google.guava:*</include>
                                    <include>com.esri.geometry:esri-geometry-api</include>
                                    <include>com.zaxxer:HikariCP</include>
                                    <!--  Include fixed version 30.1.1-jre-14.0 of flink shaded guava  -->
                                    <include>org.apache.flink:flink-shaded-guava</include>
                                </includes>
                            </artifactSet>
                            <filters>
                                <filter>
                                    <artifact>org.apache.kafka:*</artifact>
                                    <excludes>
                                        <exclude>kafka/kafka-version.properties</exclude>
                                        <exclude>LICENSE</exclude>
                                        <!-- Does not contain anything relevant.
                                            Cites a binary dependency on jersey, but this is neither reflected in the
                                            dependency graph, nor are any jersey files bundled. -->
                                        <exclude>NOTICE</exclude>
                                        <exclude>common/**</exclude>
                                    </excludes>
                                </filter>
                            </filters>
                            <relocations>
                                <relocation>
                                    <pattern>org.apache.kafka</pattern>
                                    <shadedPattern>
                                        com.ververica.cdc.connectors.shaded.org.apache.kafka
                                    </shadedPattern>
                                </relocation>
                                <relocation>
                                    <pattern>org.antlr</pattern>
                                    <shadedPattern>
                                        com.ververica.cdc.connectors.shaded.org.antlr
                                    </shadedPattern>
                                </relocation>
                                <relocation>
                                    <pattern>com.fasterxml</pattern>
                                    <shadedPattern>
                                        com.ververica.cdc.connectors.shaded.com.fasterxml
                                    </shadedPattern>
                                </relocation>
                                <relocation>
                                    <pattern>com.google</pattern>
                                    <shadedPattern>
                                        com.ververica.cdc.connectors.shaded.com.google
                                    </shadedPattern>
                                </relocation>
                                <relocation>
                                    <pattern>com.esri.geometry</pattern>
                                    <shadedPattern>com.ververica.cdc.connectors.shaded.com.esri.geometry</shadedPattern>
                                </relocation>
                                <relocation>
                                    <pattern>com.zaxxer</pattern>
                                    <shadedPattern>
                                        com.ververica.cdc.connectors.shaded.com.zaxxer
                                    </shadedPattern>
                                </relocation>
                            </relocations>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
    </plugins>
</build>
```

这将确保每个子项目都会生成一个带有`shaded`分类的可执行jar文件。

1. 在IDEA中，右键单击父项目，选择"Maven" -> "Reimport"，以确保所有依赖和插件都正确加载。
2. 最后，您可以使用Maven命令`mvn package`或在IDEA中使用Maven插件来构建和打包项目。生成的可执行jar文件将位于每个子项目的`target`目录下。

希望这些步骤能帮助到您！如果您有任何进一步的问题，请随时提问。

### 2.4 异常个字符解析错误

个字符解析错误。解析 JAVADOC_TAG ，详情： no viable alternative at input '         *'

在 Javadoc 注释的首句末尾添加句号，以符合规范。

```
/**
 * 这是一个示例方法。
 * 该方法用于执行某个操作。
 */
public void exampleMethod() {
    // 方法的具体实现
}
```

### 2.5 `Too many files with unapproved license`

​	异常、使用-Drat.skip=true跳过 或者 对没有授权的文件开头添加授权。

```
mvn package -DskipTests -P dist,release,rpm-prepare,rpm -Drat.skip=true
```

### 2.6 `(javadoc) JavadocType: 缺少 Javadoc 。`

​	类没有注释，注意以 ： “."，结尾。

```
/** The context for fetch task that fetching data of snapshot split from MySQL data source. */
public class MySqlSourceFetchTaskContext extends JdbcSourceFetchTaskContext { 
  ...
}
```

## 3 依赖jar包查看

![](/IDEA查看依赖jar.png)

​	例如：某环境添加jar，直接从官网下载的jar包不包含依赖jar包，需要自己手动添加依赖jar，添加的jar版本必须跟主jar设置的依赖版本一致，可通过idea查看某jar包的所有依赖jar和版本。



## 4 生成jar不包含依赖

```
mvn clean package
```



# end