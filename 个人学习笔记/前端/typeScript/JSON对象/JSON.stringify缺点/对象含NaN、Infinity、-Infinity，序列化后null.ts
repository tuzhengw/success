/**
 * 如果obj里有NaN、Infinity和-Infinity
 * 则序列化的结果会变成null
 */
function main() {
    const test = {
        age: NaN
    };
    const copyed = JSON.parse(JSON.stringify(test));
    // { age: NaN }
    console.log(test);
    // { age: null }
    console.log(copyed);
}