/**
 * 4.0 前端脚本注释规范
 
 
 * 作者：XXX
 * 审核人员：
 * 创建日期：
 * 脚本入口：CustomJS
 * 功能描述：
 * API参考文档：
 */
import { IMetaFileCustomJS, InterActionEvent, IAppCustomJS, IDwCustomJS, FAppInterActionEvent, IFApp, IDataset, IFAppForm } from "metadata/metadata-script-api";
import { showDrillPage, getMetaRepository, MetaFileViewer, getCurrentUser, getSysSettings } from 'metadata/metadata';
import { ctx, SZEvent, Component, throwInfo, throwError, showConfirmDialog, showFormDialog, showErrorMessage, showMenu, isEmpty, deepAssign, uuid, message, showSuccessMessage, showWarningMessage, showProgressDialog, rc, DataChangeType, UrlInfo, downloadFile, waitRender, showWaiting, assign, IDataProvider, formatDateFriendly, parseDate, formatDate, tryWait, rc_get } from 'sys/sys';
import { SuperPageBuilder } from "app/superpage/superpagebuilder";
/**
 * 通用功能写在：spg{ }中，独有功能，按SPG分类，使用new XXX()
	export const CustomJS: { [type: string]: IMetaFileCustomJS } = {
		
	"/SJDWFW/app/数据服务.app/数据服务/检验报告信息.spg": new SJDWFW(),
	....
	
	4.19以上版本支持
 */
export const CustomJS: { [type: string]: IMetaFileCustomJS } = {
    spg: {
        CustomActions: {
            down_local_file: (event: InterActionEvent) => {
                // downloadFile("/TMP/app/保存测试.app/test.action?method=downFileInfo");
                rc({
                    method: "GET",
                    url: "/TMP/app/保存测试.app/test.action?method=downFileInfo"
                }).then((result) => {
                    // console.log(result);
                    // let a = window.URL.createObjectURL(result);
                    // console.log(a);
                });
            },
            /**
			 * 20220217 tuzw
			 * 网络交易监管增加批量导出文件（截图、凭证、交易、登记表）
			 * @description 地址：https://jira.succez.com/browse/CSTM-17763
			 * 
			 * 20221028 tuzw
			 * 导出前需要先创建文件, 此过程耗时, 需要在前端增加等待样式, 故将导出和创建文件夹分开处理
			 */
			down_zip_file: (event: InterActionEvent) => {
				let choseImportData = event.component.getComponent("list1").getCheckedDataRows();
				if (choseImportData.length == 0) {
					showWarningMessage("请勾选需要导出的数据");
				}
				let importSabaCodes: string[] = [];
				let importPks: string[] = [];
				for (let i = 0; i < choseImportData.length; i++) {
					let sabaCode = choseImportData[i]["SYH"];
					let pk = choseImportData[i]["XSBH"];
					if (!!sabaCode) {
						importSabaCodes.push(sabaCode);
					}
					if (!!pk) {
						importPks.push(pk);
					}
				}
				if (importSabaCodes.length == 0 || importPks.length == 0) {
					showWarningMessage("没有需要导出的文件");
					return;
				}
				let zipRequestUrl: string = `/ZHJG/app/home.app/custom.action?method=zipMonitorIlegalInfoFiles&importSabaCodes=${importSabaCodes.join(",")}`;
				if (zipRequestUrl.length > 2038) { // URL长度最大2083字节, 携带参数个数需要增加限制
					showWarningMessage(`批量导出个数超过限制, 每次最多导出100个`);
					return;
				}
				let createFilePromise = rc<void>({ // 创建导出文件 
					url: "/ZHJG/app/home.app/custom.action?method=createMonitorIllegalWordFile",
					data: {
						importSabaCodes: importSabaCodes.join(","),
						importPks: importPks.join(",")
					}
				});
				return showWaiting(createFilePromise).then((createResult => {
					if (!createResult.result) {
						showWarningMessage(createResult.message);
						return;
					}
					downloadFile(zipRequestUrl); // 导出，由于压缩文件已经创建完成, 压缩文件耗时
				}));
			},
			/**
			 * 20221027 tuzw
			 * 网络交易检测批量导出网络交易监测违法信息登记表
			 * @description 地址：https://jira.succez.com/browse/CSTM-20971
			 * @param taskId 监测编码信息-任务编号
			 */
			button_import_monitorInfoTableFiles: (event: InterActionEvent) => {
				let params = event.params;
				let taskId: string = params.taskId;
				if (isEmpty(taskId)) {
					showWarningMessage("任务编码为空, 没有需要导出的文件");
					return;
				}
				let createFilePromise = rc<void>({
					url: "/ZHJG/app/home.app/custom.action?method=createImportIllegalFiles",
					data: {
						taskId: taskId
					}
				});
				showWarningMessage("批量下载耗时较久，请耐心等待！");
				return showWaiting(createFilePromise).then((createResult => {
					if (!createResult.result) {
						showWarningMessage(createResult.message);
						return;
					}
					let tempSaveImportFileDir: string = createResult.tempSaveImportFileDir;
					downloadFile(`/ZHJG/app/home.app/custom.action?method=zipSignDirAndimport&tempSaveImportFileDir=${tempSaveImportFileDir}`);
				}));
			}
        }
    },
	"/ZHJG/app/home.app/jgrw/ZFBA/zfbaxsfk.spg": {
		CustomActions: {
			/**
			 * 案件线索登记处理表批量下载功能
			 * @see https://jira.succez.com/browse/CSTM-23120
			 * @param pollingListIds 导出的线索编号
			 * @param zipFileName 压缩包名称, 默认: 案件线索登记处置表
			 */
			downloadPollingListWords: (event: InterActionEvent) => {
				let params = event.params;
				let pollingListIds: string[] | string = params.pollingListIds;
				if (isEmpty(pollingListIds)) {
					showWarningMessage(`列表未勾选导出行`);
					return;
				}
				if (typeof pollingListIds == "string") {
					pollingListIds = pollingListIds.split(",");
				}
				if (pollingListIds.length == 0) {
					showWarningMessage(`列表未勾选导出行`);
					return;
				}
				let zipFileName = params.zipFileName;
				if (isEmpty(zipFileName)) {
					zipFileName = "案件线索登记处置表";
				}
				let zipCheckedXmlAttach: Promise<any> = rc({
					url: "/ZHJG/app/home.app/custom.action?method=zipRegistrationFrom",
					data: {
						pollingListIds: pollingListIds
					}
				});
				showWarningMessage("批量下载耗时较久，请耐心等待！");
				showWaiting(zipCheckedXmlAttach).then((resultInfo => {
					if (!resultInfo.result) {
						showWarningMessage(resultInfo.message);
						return;
					}
					let zipPath: string = resultInfo.zipPath;
					let errorSabaIds: string = resultInfo.errorSabaIds;
					if (errorSabaIds != "") {
						showErrorMessage(`导出失败的线索编号[${errorSabaIds}][${resultInfo.message}]`, 5000);
					}
					downloadFile(`/ZHJG/app/home.app/custom.action?method=importSignZipFile&zipFilePath=${zipPath}&isDeleteZip=true&zipFileName=${zipFileName}`);
				}));
			}
		}
	},
}