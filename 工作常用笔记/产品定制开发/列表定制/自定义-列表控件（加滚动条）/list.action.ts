import { getCurrentUser } from 'svr-api/security';
import dw from 'svr-api/dw';
import db from 'svr-api/db';
import metadata from 'svr-api/metadata';
import utils from "svr-api/utils";
import etl from "svr-api/etl";
const Timestamp = Java.type('java.sql.Timestamp');


/**
 * 转换一下得到的字段列表
 */
function conversColums(colums: TableFieldMetadata[]) {
	let result = [];
	for (let i = 0; i < colums.length; i++) {
		let col = colums[i];
		result.push({
			name: col.name,
			desc: col.desc,
			dataType: col.dataType,
			length: col.length,
			decimal: col.decimal,
			nullable: col.nullable,
			index: col.index
		})
	}
	return result;
}

/**
 * 数据库配置信息
 */
interface DB_CONFIG_INFO {
	/** 【数据接入数据表配置信息表】主键值 */
	UUID?: string;
	/** 表名 */
	TABLE_NAME?: string;
	/** 更新策略 */
	UPDATE_STRATEGY?: string;
	/** 增量依据时间字段 */
	ADD_CONDITION?: Array<string>,
	/** 当前表所有时间字段 */
	ALL_TIME_FIELDS?: Array<string>;
	/** 当前表的主键字段 */
	PRIMARY_KEY?: Array<string>;
	/** 当前表所有字段 */
	TABLE_ALL_COLUMN?: Array<string>;
	/** 是否根据主键去重 */
	PRIMARY_KEY_DEL_REPEAT?: number;
}

/**
 * 20211209 tuzw
 * 获取对应数据源下的所有表主键 以及 时间字段
 * @param dataBase：数据源名
 * @param schema
 * @param choseData：列表行数据集
 */
export function getPrimaryKeysAndTimesColumns(request: HttpServletRequest, response: HttpServletResponse, params: { dataBase: string, schema: string, choseData: Array<DB_CONFIG_INFO> }): any {
	console.debug(`--getPrimaryKeysAndTimesColumns，获取对应数据源下的所有表主键 以及 时间字段--start`);
	let ds = db.getDataSource(params.dataBase);
	let schema: string = params.schema;
	/** 数据库配置对象，即使入参，也是返回数组 */
	let currentChoseData: Array<DB_CONFIG_INFO> = params.choseData;
	if (currentChoseData == null) {
		return;
	}
	for (let i = 0; i < currentChoseData.length; i++) {
		let rowTableName = currentChoseData[i].TABLE_NAME;
		if (rowTableName == null) {
			continue;
		}
		let dsMeta: TableMetaData = null;
		if (!!schema) {
			dsMeta = ds.getTableMetaData(rowTableName, schema);
		} else {
			dsMeta = ds.getTableMetaData(rowTableName);
		}
		let fields: TableFieldMetadata[] = dsMeta.getColumns();
		if (fields != null) {
			let colums = conversColums(fields);
			currentChoseData[i].TABLE_ALL_COLUMN = [];
			currentChoseData[i].ALL_TIME_FIELDS = [];
			for (let j = 0; j < colums.length; j++) {
				// 获取当前表的时间字段
				if ([FieldDataType.D, FieldDataType.P, FieldDataType.T].includes(colums[j].dataType)) {
					/** 系统字段不用显示出来 */
					if (colums[j].name.toUpperCase().indexOf("SYS_SDI_UPDATE_TIME") != -1 || colums[j].name.toUpperCase().indexOf("SYS_SDI_DATASOURCE") != -1
						|| colums[j].name.toUpperCase().indexOf("SYS_UPDATE_TIME") != -1 || colums[j].name.toUpperCase().indexOf("SYS_DATASOURCE") != -1) {
						continue;
					}
					currentChoseData[i].ALL_TIME_FIELDS.push(colums[j].name);
				}
				currentChoseData[i].TABLE_ALL_COLUMN.push(colums[j].name);
			}
		}
	}
	console.debug(`--getPrimaryKeysAndTimesColumns，获取对应数据源下的所有表主键 以及 时间字段--end`);
	return currentChoseData;
}