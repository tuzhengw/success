/**
 * ========================================================
 * 作者：tuzw
 * 创建日期：2022-11-08
 * 脚本用途： 处理项目内的报表、仪表板、应用、表单应用加载
 * ========================================================
 */
import { IMetaFileCustomJS, InterActionEvent } from "metadata/metadata-script-api";
import { DashboardData } from "ana/dashboard/dashboarddatamgr";
import { isEmpty } from "sys/sys";

/**
 * 20220715 tuzw
 * 所有仪表板通用脚本
 */
export class DashCustomJs {
    public CustomExpFunctions: any;
    public CustomActions: any;

    constructor() {
        this.CustomActions = {
            button_addDialogHeaderBackColor: this.button_addDialogHeaderBackColor.bind(this)
        };
    }

    /**
     * 当元数据文件信息、内容、或其他的一些元数据信息加载完毕后调用。
     */
    public onRender(event): void {
        let fileInfo = event.page.getFileInfo();
        let pageName: string = fileInfo && fileInfo.name;
        this.addIncludeDetailTableDashClassName(event, pageName);
    }

    /**
     * 2022-11-08 tuzw 
     * 给指定门户中的明细表添加一个定制class名
     * 门户地址：http://192.168.9.200:8158/whscjg/dsjfx/ana?:locate=%2Fdsjfx%2Fana%2F01+%E5%B8%82%E7%9B%91%E4%B8%80%E5%A4%A9%2F%E6%98%8E%E7%BB%86
     */
    private addIncludeDetailTableDashClassName(event: InterActionEvent, dashName: string): void {
        let signDashNames: string[] = [
            "医疗器械.dash", "地理标志企业明细.dash", "小作坊企业明细.dash", "小餐饮企业明细.dash", "明细/尚在严重违法失信企业.dash",
            "尚在异常名录.dash", "市场主体.dash", "有效注册商标企业明细.dash", "本年移出严重违法失信企业.dash", "案件办结数量.dash",
            "特种设备明细.dash", "移出异常名录企业.dash", "立案数量.dash", "药品经营.dash", "重点监控设备明细.dash", "食品生产企业明细.dash",
            "食品经营企业明细.dash", "驰名商标明细.dash", "案件线索量明细.dash", "尚在严重违法失信企业.dash", "XKLQCQYJ_MX.dash", "SPXKZS_MX.dash"
        ];
        if (signDashNames.includes(dashName)) {
            let comp = event.component;
            let compId: string = comp?.getId();
            if (["groupTable1", "columnTable1"].includes(compId)) {
                let groupTable = comp?.component;
                let domBase = groupTable.domBase;
                domBase.classList.add("custom-dash-group-table");
            }
        }
    }

    /**
     * 2022-11-15 tuzw
     * 给通过按钮打开的对话框-对话框表头所在面板-更改背景样式
     * @description 按钮打开的对话框, 无法通过page.getComponent获取, 改为document获取
     */
    private button_addDialogHeaderBackColor(event: InterActionEvent): void {
        let dashboardData = event.renderer.getData() as DashboardData;
        let dialogDom = document.getElementById("drillPageDialog");
        if (!!dialogDom) {
            dialogDom.style.backgroundColor = "#196e8dcc";
        }
        let dialog = dialogDom?.szobject;
        let dialogHeader: HTMLElement = dialog.domHeader;
        if (!!dialogHeader) {
            dialogHeader.style.backgroundColor = 'rgb(34,120,134)';
            dialogHeader.style.opacity = "0.7";
            dialogHeader.style.fontWeight = "bold";
        }
        let dialogCaption: HTMLElement = dialog.domCaption;
        if (!!dialogCaption) {
            dialogCaption.style.color = "rgb(255, 255, 255)";
        }
        /**
         * 2023-01-05 tuzw
         * 问题：触发脚本交互，若组件被选中，点击时无法触发
         * 解决办法：
         *  1) 产品内部部分组件有设置一个状态值来判断是否选中, 对校验是否选中的代码逻辑进行调整
         *  2) 产品在 anabrower.ts中的isFieldValuesSelected()方法中判断组件是否选中
         *  3) 这里脚本将isFieldValuesSelected方法内的actionSelect变量设置为: null, 使其取消选中状态
         * 
         * 待改进: 
         *  1) 若外部传入的actionEvent.value为空, 则会视为选中状态
         *  2) actionEvent.value暂时没有办法设置, 故调用此脚本, 最好保证点击的组件是有值的
         */
        let hbarIds: string[] = ["hbar1", "hbar6"];
        for (let i = 0; i < hbarIds.length; i++) {
            let hbarId: string = hbarIds[i];
            let hbar = dashboardData.getComponent(hbarId);
            if (isEmpty(hbar)) {
                continue;
            }
            hbar.setProperty(AnaStatusDataKey.ActionSelect, null);
        }
    }
}

export const CustomJS: { [type: string]: IMetaFileCustomJS } = {
    dash: new DashCustomJs()
}