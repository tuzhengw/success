import db from "svr-api/db"
import utils from "svr-api/utils";
import bean from "svr-api/bean";
import metadata from 'svr-api/metadata';
import fs from "svr-api/fs";
import sys from "svr-api/sys";
import security from "svr-api/security";
import dw from "svr-api/dw";

import JavaFile = java.io.File;
import ZipOutputStream = java.util.zip.ZipOutputStream;
import ZipEntry = java.util.zip.ZipEntry;
import ArrayUtils = org.apache.commons.lang3.ArrayUtils
import FileOutputStream = java.io.FileOutputStream;
import FileInputStream = java.io.FileInputStream;
import IOUtils = org.apache.commons.io.IOUtils;
import ZipFile = java.util.zip.ZipFile;
import StandardCharsets = java.nio.charset.StandardCharsets;
import JavaString = java.lang.String;
import FileUtils = org.apache.commons.io.FileUtils;
import ZipInputStream = java.util.zip.ZipInputStream;
import BufferedInputStream = java.io.BufferedInputStream;
import ByteArrayOutputStream = java.io.ByteArrayOutputStream;
import StringUtils = org.apache.commons.lang3.StringUtils;
import Byte = java.lang.Byte;

import MetaFileContentImpl = com.succez.metadata.service.impl.entity.MetaFileContentImpl;


/**
 * 不支持更改已存在的元数据ID
 */
function testWriteMetaFile(): void {
    let project = "/sysdata/test/textId.txt";
    let metaServiceRepository = bean.getBean("com.succez.metadata.service.MetaServiceRepository");

    metaServiceRepository.forceCreateOrUpdateFile(project, null, { // 若文件不存在, 内部会自己创建
        "id": "WPiahojlyFLMCJqISUrhtD", "creator": "system", "createTime": 1670340112567, "modifier": "system", "modifyTime": 1670374767616, "option": {}, "customOption": {}
    });
}


/**
 * 获取磁盘的一个文件内容, 并将其写入到环境对应的元数据文件中
 * <p>
 *  1 直接获取字符串来修改文件, 无法处理图片
 *   IOUtils.toString(zipFile.getInputStream(zipFile.getEntry(zipEntryName)), StandardCharsets.UTF_8); // 获取文件内容()
 *  2 通过产品对外提供的元数据仓库对象, 调用其创建方法修改元数据文件
 *   metaServiceRepository.forceCreateOrUpdateFile(路径, 内容, 文件属性配置)：
 *   (1) 可将内存字节流写入到元数据文件中,
 *   (2) 文件属性配置中的ID若在当前环境已存在, 不支持更改已存在的元数据ID
 * </p>
 */
function testWriteMetaFile(): void {
    let project = "/sysdata/script/Slice 23.png";
    let metaServiceRepository = bean.getBean("com.succez.metadata.service.MetaServiceRepository");

    let path: string = "D:/success/workspace4.0/workdir/clusters-share/file-storage/relativePath/Slice 23.zip";
    let backupFile = new JavaFile(path);
    let zipEntry;
    let zipInputStream: ZipInputStream = null;
    /** byte数组的缓冲区 */
    let byteArrayOutputStream: ByteArrayOutputStream = null;
    try {
        zipInputStream = new ZipInputStream(new BufferedInputStream(new FileInputStream(backupFile)));
        byteArrayOutputStream = new ByteArrayOutputStream(8 * 1024);
        let zipFile = new ZipFile(backupFile);

        let buf: number[] = [new Byte(8 * 1024)];
        while ((zipEntry = zipInputStream.getNextEntry()) != null) {
            let zipEntryName: string = zipEntry.getName();
            let isFolder = StringUtils.endsWith(zipEntryName, "/");
            if (isFolder) {
                continue;
            }
            if (zipEntryName != "Slice 23.png") {
                continue;
            }
            byteArrayOutputStream.reset(); // 重置此流(即，它将删除此流中所有当前消耗的输出，并将变量计数重置为0)
            IOUtils.copyLarge(zipFile.getInputStream(zipEntry), byteArrayOutputStream, buf);
            let newContent = new MetaFileContentImpl(byteArrayOutputStream.toByteArray(), false);
            metaServiceRepository.forceCreateOrUpdateFile(project, newContent, { // 若文件不存在, 内部会自己创建
                "id": "WPiahojlyFLMCJqISUrhtD", "creator": "system", "createTime": 1670340112567, "modifier": "system", "modifyTime": 1670374767616, "option": {}, "customOption": {}
            });
        }
    } catch (e) {
        logger.addLog(`恢复项目失败----error`);
        logger.addLog(e);
    } finally {
        zipInputStream && zipInputStream.close();
        byteArrayOutputStream && byteArrayOutputStream.close();
    }
}