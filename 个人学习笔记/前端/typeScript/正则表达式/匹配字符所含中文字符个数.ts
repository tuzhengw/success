

function main() {
	/** 中文匹配规则 */
    let matchRule = new RegExp(/[\u4e00-\u9fa5]/g);
    print("（农药残留）有机磷和氨基甲酸酯类".match(matchRule)); // ["农", "药", "残", "留", "有", "机", "磷", "和", "氨", "基", "甲", "酸", "酯", "类"]
    print("matchRule".match(matchRule)); // null
}


/**
 * 求字符串中【中文】和【英文】个数
 * @param v：需要处理的字符串
 */
 function dealStr(v: string): void {
	let chineseMatchRule = new RegExp(/[\u4e00-\u9fa5]/g);
	let chinese: string[] = (v + '').match(chineseMatchRule);
	let chineseNums: number = !!chinese // 中文所占字节大小
		? chinese.length * 2
		: 0;
	let englishNums: number = !!chinese && (v + '').length - chinese.length > 0    // 其他英文字符所占字节大小
		? (v + '').length - chinese.length
		: (v + '').length;
	
	print(`${chineseNums}、${englishNums}、${chineseNums + englishNums}`);
 }