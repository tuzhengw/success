


/**
 * 获取数据加工/模型查询的SQL
 * 20220401 tuzw
 * 检测的模型包含：数据加工，其不包含提取的物理表，仅一个视图表，故：无法通过指定物理表来查询数据
 * 解决办法：通过产品的getQuerySql()获取生成模型/数据加工的主键值临时表，将随机查询范围定位到这个临时表
 * @param modelResid 检测模型的resid值
 * @param filterConditions 随机的范围过滤条件（检测模型共有字段EXP），eg："outDate='20220202'"
 * eg：SELECT pk.流水号 FROM  (select distinct t0.LSH as 流水号 from baglxt.jcb_jbqk t0 left join baglxt.lsh_ba_ztb t1 on .... 
 */
function getViewModelQuerySQL(modelResid: string, filterConditions?: string): string {
    let pk = getViewTablePrimaryKeys(modelResid)[0];
    let modelPath: string = getModelPath(modelResid);
    console.debug(`模型【${modelResid}】对应主键：${pk}，模型路径：${modelPath}，随机范围过滤条件：${filterConditions}`);
    if (!pk || !modelPath) {
        return "";
    }
    let filter: FilterInfo[] = [];
    if (!!filterConditions) {
        filter.push({
            exp: filterConditions
        });
    }
    let queryInfo: QueryInfo = {
        resid: modelResid,
        distinct: true,
        fields: [{ name: `${pk}`, exp: `model1.${pk}` }],
        filter: filter,
        sources: [{
            id: "model1",
            path: modelPath
        }]
    }
    let queryInfoSQL = dw.getQuerySql(queryInfo);
    console.debug(`获取模型【${modelResid}】数据加工/模型查询的SQL：${queryInfoSQL}`);
    return queryInfoSQL;
}


/**
 * 根据模型id获取模型的路径
 */
function getModelPath(modelResid: string): string {
    let model: MetaFileInfo = metadata.getFile(modelResid);
    if (!model) {
        return "";
    }
    let modelPath: string = model.path;
    return modelPath;
}


/**
 * 获得视图模型的主键集
 * @param modelResid 检测模型的resid值
 */
function getViewTablePrimaryKeys(modelResid: string): string[] {
    let model = dw.getDwTable(modelResid);
    if (!model) {
        return [];
    }
    let pk = model.properties && model.properties.primaryKeys;
    if (!pk) {
        return [];
    }
    console.debug(`getViewTablePrimaryKeys()，模型【${modelResid}】的主键为：${pk}`);
    return pk;
}
