

/**
 *	将查询的结果转化为下拉框支持的格式
 */
function converFilterResultData(data: (string | number | boolean)[][]): JSONObject[] {
	let results: JSONObject[] = [];
	for (let i = 0; i < data.length; i++) {
		let info = {
			caption: data[i][0],
			value: data[i][1]
		};
		results.push(info);
	}
	return results;
}