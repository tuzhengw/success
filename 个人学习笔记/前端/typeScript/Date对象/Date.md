---
typora-root-url: images
---

# Date

> 学习网址：https://www.cnblogs.com/zszxz/p/12255663.html

# 1 字符串是否符合日期格式

```ts
function main(){
	/**
	 * 创建一个：正规表达式对象
	 */
	let checkPatten = new RegExp(/^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}$/);
    let times = '2020-12-21 20:05:05'; // true
    let str = '2019-09-20 20:05'; // false
    if (checkPatten.test(times)) {
        print("格式正确!");
    } else {
        print('格式错误')
    }
}
```

# 2 比较字符串日期大小

```ts
/*
 * 注意：js日期的初始化的格式：yyyy/MM/dd HH:mm:ss
 * eg：
 * 		let time = new Date('2014/11/27 00:00:00')
 */
function main(){
	let startDate = '2019-3-15 12:25:36';
    let endDate = '2019-4-23 12:36:45';
	/**
     * Date.parse() 方法解析一个表示某个日期的字符串
     		并返回从1970-1-1 00:00:00 UTC 到该日期对象（该日期对象的UTC时间）的毫秒数
     * 如果该字符串无法识别，或者一些情况下，包含了不合法的日期数值（如：2015-02-31）
     		则返回值为NaN
     * 2015-02-31 ————2015/02/31（转换后为本地时间）
     */
    let result = Date.parse(endDate.trim().replace(/-/g, '/')) 
    				> 
        Date.parse(startDate.trim().replace(/-/g, '/'));
    if (result) { 
        print('endDate 大于 startDate');
    } else {
        print('endDate 小于 startDate');
    }
}
```

> **测试：不使用`replace`也可以比较**

# 3 时间按指定格式显示

> **方法一：**<button>正则表达式</button>
>
> **缺陷：**` print(dateFormat("2021-09-09 00:00:00"));` ——`2021-9-9 00:00:00`，若月份和日小于10，则仅显示一份，忽略了0

```ts
function dateFormat(dateString: string): string {
    if (dateString == null) {
        return "";
    }
    let date = new Date(Date.parse(dateString));
    let timesting = date.toString().match(/\d+:\d+:\d+/);
    let nowDay: string = 
        date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + " " + timesting;
    return nowDay;
}
```

---

> **方法二：**使用Date对象的内置方法 + 三目运算符
>
> `print(formatDate("2021-09-09 00:00:00"));` ——`2021-09-09 00:00:00`
>
> **注意：**
>
> ​	时间戳字符串，由于浏览器之间的差异，<button>不推荐使用Date构造函数</button>来解析日期字符串

```ts
/**
 * 格式化时间戳
 * @parmas dateString：时间戳字符串
 */
function formatDate(dateString: string): string {
    /**
     * 时间戳字符串：
     *   由于浏览器之间的差异，不推荐使用Date构造函数来解析日期字符串
     */
    let dates = new Date(dateString);
    let Year: number = dates.getFullYear();
    let Months = (dates.getMonth() + 1) < 10 
				? '0' + (dates.getMonth() + 1) 
				: (dates.getMonth() + 1);
    let Day = dates.getDate() < 10 
    			? '0' + dates.getDate() 
    			: dates.getDate();
    let Hours = dates.getHours() < 10 
    			? '0' + dates.getHours() 
    			: dates.getHours();
    let Minutes = dates.getMinutes() < 10 
    			? '0' + dates.getMinutes() 
    			: dates.getMinutes();
    let Seconds = dates.getSeconds() < 10 
    			? '0' + dates.getSeconds() 
    			: dates.getSeconds();
    return Year + '-' + Months + '-' + Day + ' ' + Hours + ':' + Minutes + ':' + Seconds;
}
```

> **方法三：**利用JAVA的format类，见：5

# 4 日期字符串—时间戳

```ts
/**
 * 注意：
 *	若数据库中某字段为时间戳，不用转换为指定格式后，再存入数据库，直接存入时间戳，数据库内部会自己转换
 */
Date.parse("时间戳字符串");
Date.parse("2021-08-01 00:00:00");
```

# 5 JAVA的`format`方法格式化：日期—字符串

<button>【**依赖可以利用【IDEA】工具，查看【JAVA】对象源码**】</button>

> 时间戳——`LocalDateTime`——格式化
>
> `LocalDateTime`——时间戳
>
> 字符串日期——`LocalDateTime`——date
>
> `LocalDateTime`——字符串日期（格式化）

## 1 `LocalDate`—格式化

> **导入报错不影响**

```ts
// 用法跟JAVA类似，先导入相关包文件
import DateTimeFormatter = java.time.format.DateTimeFormatter;
const yearMonthFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
// LocalDate类，内容包含：now()：现在时间等方法
import LocalDate = java.time.LocalDate;
```

> **用法一**：<button>当前时间格式化</button>

```java
// 日期.format(yearMonthFormatter);
let now = LocalDate.now().format(yearMonthFormatter);
print(now);//2021-08-18
```

> **注意：**`LocalDate`类中没有获取：时、分、秒的方法

```ts
const yearMonthFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:ss:mm");
import LocalDate = java.time.LocalDate;

let now = LocalDate.now().format(yearMonthFormatter);
```

> <button>报错：</button> `LocalDate`不包含：时分秒

----

> **用法二：**<button>自定义字符串日期——转换日期——格式化字符串</button>

> 《第一步》：处理日期字符串，将其：年、月、日、时、分、秒单独拆分出来
>
> **注意：**`LocalDate`没有时分秒

```ts
 let dateString: string = "2021-01-09";
```

```ts
let year = dateString.substring(0, 4);
let month = dateString.substring(5,7);
let dayOfMonth = dateString.substring(8,10);
```

> 《第二步》：利用`LocalDate`构造方法构建Date对象

```ts
let date = LocalDate.of(year, month, dayOfMonth);
```

> 《第三步》：格式化日期
>
> **注意：**
>
> 【字符串本身就已经是一种格式化的日期字符串，若需要<button>格式其他样式</button>则可以继续格式化，否则不需要】

```ts
print(date.format(yearMonthFormatter)); 
```

## 2 `LocalDateTime`—格式化

> 用法一：<button>当前时间格式化</button>

```ts
// 导入依赖
import DateTimeFormatter = java.time.format.DateTimeFormatter;
const yearMonthFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:ss:mm");
import LocalDateTime = java.time.LocalDateTime;
```

```ts
// 处理当前时间
let now = LocalDateTime.now().format(yearMonthFormatter);
print(now);//2021-08-18 00:00:00
```

> 用法二：<button>自定义字符串日期——转换`LocalDateTime`日期——格式化字符串</button>

```ts
let dateString: string = "2021-01-09 00:00:00";
// 拆分年、月、日、时、分、秒
let year = dateString.substring(0, 4);
let month = dateString.substring(5,7);
let dayOfMonth = dateString.substring(8,10);
let hour = dateString.substring(11,13);
let minute = dateString.substring(14, 16);
let second = dateString.substring(17,19);
// 转换为LocalDateTime对象
let date = LocalDateTime.of(year, month, dayOfMonth, hour, minute, second);
// 格式化日期
print(date.format(yearMonthFormatter)); 
```

> 用法三：将指定格式字符串日期转换为`LocalDateTime`对象，并又按指定格式显示

```ts
const yearMonthFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:ss:mm");
import LocalDateTime = java.time.LocalDateTime;
/**
 * 使用特定的格式化LocalDateTime从文本字符串获取LocalDateTime的实例
 * 		注意：日期字符串必须跟给定的格式 一致，否则不报：某下标找不到
 */
let dateFormat = LocalDateTime.parse("2021-01-01 00:00:00", yearMonthFormatter);
print(dateFormat.format(yearMonthFromatter));
```

## 3 `LocalDateTime`—时间戳

> 学习网址（**语言：JAVA**）：https://www.cnblogs.com/zszxz/p/12255663.html

```ts
// TS导入依赖
import ZoneOffset = java.time.ZoneOffset;
import LocalDateTime = java.time.LocalDateTime;
```

> 获得<button>秒级别</button>的时间戳

```ts
let localDateTime = LocalDateTime.now();
// 将当前时间转为时间戳
long second = localDateTime.toEpochSecond(ZoneOffset.ofHours(8));
// 1580706475
print(second); // 内部还是调用的console.log();
```

> 获得<button>毫秒级</button>的时间戳

```ts
let localDateTime = LocalDateTime.now();
// 将当前时间转为时间戳
let second = localDateTime.toInstant(ZoneOffset.ofHours(8)).toEpochMilli();
// 1.629280319637E12
print(second);
```

## 4 时间戳—`LocalDateTime`

> `java.sql.Timestamp;`

> 导入依赖

```ts
// 依赖：可以利用【IDEA】工具，查看JAVA相关对象的源码
import ZoneOffset = java.time.ZoneOffset;
import LocalDateTime = java.time.LocalDateTime;
const yearMonthFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:ss:mm");
```

> <button>毫秒级</button>时间戳转换Date

```ts
// let dateString: string = "2021-01-09 00:01:00";
// let remindTime = Date.parse(dateString);
let remindTime: number = 1562983881098;
/**
 * 将得到的时间戳转换为：LocalDateTime对象
 */
let dateTime = LocalDateTime.ofEpochSecond(remindTime, 0, ZoneOffset.ofHours(8));
print(dateTime); 
```

---

> <BUTTON>秒级，默认：毫秒级，转换使用：秒级</BUTTON>时间戳转换Date，毫秒级/1000

```ts
// let dateString: string = "2021-01-09 00:01:00";
// let remindTime = Date.parse(dateString);
let remindTime: number = 1562983881098;
// 时间戳是从1970.1.1八点开始
let dateTime = LocalDateTime.ofEpochSecond(remindTime/1000, 0, ZoneOffset.ofHours(8));
print(dateTime); // 2021-01-09T00:01
```

## 4 字符串—`LocalDateTime`

> 导入依赖

```ts
import ZoneOffset = java.time.ZoneOffset;
import LocalDateTime = java.time.LocalDateTime;
import DateTimeFormatter = java.time.format.DateTimeFormatter;
const yearMonthFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dsss:mm");
```

```ts
let dateString: string = "2021-01-09 00:01:00";
/**
 * 将指定日期字符串解析为：LocalDateTime对象
 *  注意：字符串日期要跟：格式化一致，例如：2021-01-09 00:01:00 yyyy-MM-dd HH:ss:mm
 */
let dateFormat: LocalDateTime = LocalDateTime.parse(dateString, yearMonthFormatter);
// print(dateFormat.format(yearMonthFormatter)); // 2021-01-09 00:01:00
```

## 5 `ZoneOffset.ofHours(8)`

​	**原因：**时间戳是从1970.1.1<button>**八点**</button>开始

## 6 12小时和24小时制

> `LocalDateTime`获取服务器当前时间----24小时与12小时

```ts
// h：12小时
String time = 
    LocalDateTime.now().format(DateTimeFormatter.ofpattern("yyyy-MM-dd HH:mm:ss")
```

```ts
// H: 24小时
String time = 
    LocalDateTime.now().format(DateTimeFormatter.ofpattern("yyyy-MM-dd hh:mm:ss")
```

----

# 6 `Date.parse`存在时差问题

> new Date()，参数：`dateString`
>
> ​		由于浏览器之间的差异与不一致，不推荐使用`Date`构造函数来解析日期字符串 （或使用与其等价的`Date.parse`）。
>
> ​		对`RFC 2822`格式的日期仅有约定俗称的支持。
>
> ​		对`ISO 8601`格式的支持中，<button>仅有日期的串</button>（例如"1970-01-01"）会被处理为`UTC（世界标准时间）`，而<button>不是本地时间</button>，与其他格式的串的处理不同。

---

> 测试：

```ts
print(new Date('2021-05-09')); // 2021-05-09 08:00:00
print(new Date('2021-5-9')); // 2021-05-09 00:00:00

// 将“-”替换成“/”也可以解析成：本地时间
print(new Date('2021/05/09')); // 2021-05-09 00:00:00
print(new Date('2021/5/9')); // 2021-05-09 00:00:00
// 替换公式：日期字符串.trim().replace(/-/g, "/");

// 将整个日期写全，也可以按照本地转换
print(new Date('2021-05-09 00:00:00')); // 2021-05-09 00:00:00
```

> 分析

（1）`new Date('2021-05-09')`：被当做了`UTC（世界标准时间）`进行处理，即：<button>英国格林威治</button>时间` 5/9 00:00 `（此时，对应<button>北京时间</button>是 `5/9 08:00`)， 而Date 对象<button>获取本地时区</button>，得知此时开发人员位于东八区，便将 2021-05-09 解析为东八区的时间，即北京时间` 5/9 08:00`。

（2）`new Date('2021-5-9')`：被认定为本地时间，直接解析后输出。

---

> <button>推荐</button>：`LocalDateTime.parse`和`dateFormat`解析日期字符串

```ts
const yearMonthFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:ss:mm");
import LocalDateTime = java.time.LocalDateTime;
/**
 * 使用特定的格式化LocalDateTime从文本字符串获取LocalDateTime的实例
 * 		注意：日期字符串必须跟给定的格式 一致，否则不报：某下标找不到
 */
let dateFormat = LocalDateTime.parse("2021-01-01 00:00:00", yearMonthFormatter);
print(dateFormat);
```

# 7 日期没有0月0日

> 若无法识别，会返回：`NaN`

```ts
print(Date.parse("2018/00/00 12:30:22")); // NaN
```

> **注意：**若日期字符串为：`2018/00/00 12:30:22`，即月和日为00，则无法解析
>
> **原因：**月份和日最小值为：1

# 8 时区

> 网址：https://blog.csdn.net/halfclear/article/details/77573956

> ​		全球分为24个时区，相邻时区时间相差1个小时。比如北京处于东八时区，东京处于东九时区，北京时间比东京时间晚1个小时，而英国伦敦时间比北京晚7个小时（英国采用夏令时时，8月英国处于夏令时）。比如此刻北京时间是2017年8月24日`11:17:10`，则东京时间是2017年8月24日`12:17:10`，伦敦时间是2017年8月24日`04:17:10`。

​	使用Date构建的日期，无时区的说法（不推荐，不推荐Date构建）。

```ts
Date date = new Date(1503544630000L);  // 对应的北京时间是2017-08-24 11:17:10
SimpleDateFormat bjSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  // 北京
bjSdf.setTimeZone(TimeZone.getTimeZone("Asia/Shanghai"));  // 设置北京时区
print("毫秒数:" + date.getTime() + ", 北京时间:" + bjSdf.format(date));
```

# 9 TS中查看JAVA类型

```ts
typeof(timestamp); // 除了基本类型，其他都是：Object
```

```ts
print(timestamp.getClass()); // 跟Java一样，getClass获取
```

# 10 时间戳与`Timestamp`类型

> 第一种

```js
Date.now() —— 1629455693168
```

> 第二种

```
java.sql.Timestamp
```

> 总结：上面两种类型属于不同类型

---

# 11 `TS`中使用`format`

> 将数据库的时间戳`format`为指定格式字符串
>
> 步骤：`Timestamp`————>number————>`LocalDateTime`————>string

```ts
import { getDataSource } from "svr-api/db"; //数据库相关API
import DateTimeFormatter = java.time.format.DateTimeFormatter;
import ZoneOffset = java.time.ZoneOffset;
const yearMonthFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:ss:mm");
import LocalDateTime = java.time.LocalDateTime;
// import Simpledate = java.text.SimpleDateFormat;
```

```ts
function main() {
    batchUpdateUserNames();
}
```

​	**注意：**`interface`只是给变量增加：标记，没有实际意义，<button>不会真正改变原有变量类型</button>

```ts
interface Patient {
    /**
     * CYSJ：即是：查询结果，又是返回值
     * 
     * 查询的结果类型为：java.util.Timestamp
     * 
     * 返回的类型为：string
     * 
     * 《注意》
     * interface只是给变量添加了类型，没有实际作用，更没有强制转换原有类型
     * 
     * eg：查询的变量为：a: TimeStamp
     * 
     * interface给a封装为：string，虽然显示a为string类型，但a实际还是为：TimeStamp类型
     */

    /**
     * 出院时间
     */
    CYSJ: Date | string;
}
```

```ts
/**
 * 目标数据库
 */
const DEFAULT_TABLE = "default";
let ds = getDataSource(DEFAULT_TABLE);
/**
 * 患者信息表
 */
const PATIENT_INFORMATION_TABLE = "fact_hzsqb";
/**
 * 数据库查出来的时间戳或者日期，怎么按照指定格式显示
 * 步骤：timestamp————>number————>LocalDateTime————>string
 */
function batchUpdateUserNames(): void {
    let querySQL = `select CYSJ from fact_hzsqb where HZID=? limit 1`
    try {
        let patientTable = ds.openTableData(PATIENT_INFORMATION_TABLE);
        let queryResult = patientTable.executeQuery(querySQL, [1]) 
        						as unknown as ArrayLike<Patient>;
        // let date = {
        //     /**
        //      * 若字段为时间戳格式，查询出来的值为：时间毫秒数
        //      * eg："times": 1632474491000
        //      * print() 打印出来的值为：yyyy-mm-dd ... 内部做了处理
        //      */
        //     times: queryResult[0].CYSJ
        // };
        /**
         * queryResult[0].CYSJ.getTime() 报错，是因为ts类型检查问题，不影响运行结果
         * 解决办法：
         * （1）// @ts-ignore
         * （2）(queryResult[0].CYSJ as Date).getTime() 将变量暂时转换为：Date
         */
        let outHosiptalTimes = queryResult[0].CYSJ 
       	 	? (queryResult[0].CYSJ as Date).getTime()  // 获取时间毫秒数
        	: 0;
        // 将其格式化后的值，重新赋值
        queryResult[0].CYSJ = formatDate(outHosiptalTimes);
        /**
         * print({ a: queryResult[0].CYSJ });
         * {
             "a": "2021-09-24 17:11:08"
            }
         */
        // print(outHosiptalTimes.getClass());  java.util.Date
        // 校验是否为某个类型或者某类型的子集
        // print(outHosiptalTimes instanceof java.util.Date);   
        // print(outHosiptalTimes.toString()); // 获取时间毫秒数转换为字符串
    } catch (e) {
        console.debug(e);
    }
}
```

​	格式化：

```ts
/**
 * 格式化日期字符串
 * @params outHosiptalTimes：出院时间毫秒数
 * @retuen 返回格式化的日期格式
 * <pre>
 *      formatDate(queryResult[0].CYSJ.getTime()); —————— 2021-09-24 17:11:08
 *		formatDate(new Date().getTime())
 * </pre>
 */
function formatDate(outHosiptalTimes: number): string {
    if (outHosiptalTimes == 0) {
        return "";
    }
    let dateTime: LocalDateTime = 
        LocalDateTime.ofEpochSecond(outHosiptalTimes / 1000, 0, ZoneOffset.ofHours(8));
    let dateFormat: string = dateTime.format(yearMonthFormatter);
    print(dateFormat); // 2021-09-24 17:11:08
    return dateFormat;
}
```





---

# - print方法

```ts
function print(...args: any[]){
  console.log(...args)
}
```