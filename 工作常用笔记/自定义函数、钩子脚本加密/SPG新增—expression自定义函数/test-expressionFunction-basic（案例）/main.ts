import {
	IExpEvalDataProvider
} from 'commons/exp/expeval';
import { getCurrentUser } from 'metadata/metadata';

export function TEST_USER_EXTPROPERTY_SYNC(context: IExpEvalDataProvider, userIdOrProperty: string, property?: string) {
	if (!property) {
		property = userIdOrProperty;
		userIdOrProperty = getCurrentUser().userId;
	}
	return userIdOrProperty + ' ' + property + '属性值';
}

export function TEST_USER_EXTPROPERTY_ASYNC(context: IExpEvalDataProvider, userIdOrProperty: string, property?: string) {
	if (!property) {
		property = userIdOrProperty;
		userIdOrProperty = getCurrentUser().userId;
	}
	return Promise.resolve(userIdOrProperty + ' ' + property + '属性值');
}