```md
# **ENCRYP_QYXX(codeInfo)**

解析扫描二维码获取的加密编码值，返回解密后实际的值。

## 参数
***codeInfo***：扫描的二维码信息加密编码值

## 示例
1. `ENCRYP_QYXX('')` "未传递参数
2. `ENCRYP_QYXX('KXkTRmzrg8co%2BLgK...')` 解析的结果代码
3. `ENCRYP_QYXX('11')`  '解密失败'

```

