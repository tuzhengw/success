import { SDILog, LogType, isEmpty, ResultInfo } from "/sysdata/app/zt.app/commons/commons.action";
import dw from "svr-api/dw";
import utils from "svr-api/utils";
import excel from "svr-api/excel";
import db from "svr-api/db";
import sys from "svr-api/sys";
import fs from "svr-api/fs";
import DocumentFactory = org.dom4j.DocumentFactory;
import DOM4J_Element = org.dom4j.Element;
import DocumentHelper = org.dom4j.DocumentHelper;
import DefaultElement = org.dom4j.tree.DefaultElement;
import DefaultAttribute = org.dom4j.tree.DefaultAttribute;
import DefaultDocument = org.dom4j.tree.DefaultDocument;


const Timestamp = Java.type('java.sql.Timestamp');
let logger = new SDILog({ name: "custom.xml" });

/**
 * 测试XML生成
 */
function testGenerateXML(): void {
    let xml = new XMLTestService();
    xml.testGenerateXml();
}

/**
 * XML生成测试类
 */
export class XMLTestService {

    /**
   * 生成测试的XML
   * @description XML文档只能有一个顶层元素 Root。如果你多个并列的元素放在一起的话是不合XML规范的
   * @return 
   * clinicalDocument.add(new DefaultAttribute("xmlns:a", "urn:h17-org:v3"));
   */
    public testGenerateXml(): string {
        let factory = DocumentFactory.getInstance();
        let document = factory.createDocument();
        let clinicalDocument: DefaultElement = document.addElement("ClinicalDocument")
            .addAttribute("xmlns", "urn:h17-org:v3")
            .addAttribute("mif", "urn:h17-org:v3/mif")
            .addAttribute("schemaLocation", "urn:h17-org:v3...\sdschemas\SDA.xsd");

        clinicalDocument.addElement("realmCode").addAttribute("code", "CN");
        clinicalDocument.addElement("realmCode").addAttribute("code", "CN");

        let typeElement = clinicalDocument.addElement("typeId")
            .addAttribute("root", "CN")
            .addAttribute("extension", "CN");
        clinicalDocument.addCDATA("文档模板编号");

        clinicalDocument.addComment("添加注释"); // 添加注释, 同级的得再父节点添加

        let childrenType = typeElement.addElement("versionNumber");
        childrenType.addText("版本1");
        childrenType.setText("版本2"); // 修改指定节点的value值

        typeElement.setAttributeValue("root", "ZN"); // 修改指定节点的属性

        this.listXmlChildrenNodeProperties(clinicalDocument);
        let xml = document.asXML();
        print(utils.formatXML(xml));
        return xml;
    }

    /**
     * 遍历指定XML节点下所有属性
     * @description 支持深度遍历（子节点内的子节点可遍历）
     * @description 参考：https://blog.csdn.net/u010310183/article/details/44746743
     */
    private listXmlChildrenNodeProperties(dealNode: DefaultElement): void {
        let nodeName: string = dealNode.getName();
        print(`节点名: ${nodeName}`);
        let nodeProperties = dealNode.attributes();
        for (let i = 0; i < nodeProperties.size(); i++) { // 打印当前节点的属性值
            let properties = nodeProperties.get(i);
            print(`text: ${properties.getText()}, attName: ${properties.getName()}, attValue: ${properties.getValue()}`);
        }
        /** 当前节点下面子节点迭代器 */
        let elementIterator = dealNode.elementIterator();
        while (elementIterator.hasNext()) {
            let e = elementIterator.next();
            this.listXmlChildrenNodeProperties(e);
        }
    }
}