
import { DatasetDataPackageRowInfo, IDataset, InterActionEvent, IVComponent } from "metadata/metadata-script-api";
import { Component, isEmpty, SZEvent, throwInfo, rc, message, uuid, showProgressDialog, showWaiting, rc_task, assign, showWarningMessage, LogItemInfo, ServiceTaskPromise, showDialog } from "sys/sys";
import { List, ListItem, Tree, TreeGrid, TreeGridRow, TreeItem } from "commons/tree";

/** 选择列表 */
export class SPG_SelecterList {
    /** 15天的时间毫秒数 */
    private FifteenDateTime: number = 15 * 24 * 60 * 60 * 1000;

    /**
     * 点击下拉框展开也会触发onRender方法
     */
    public onRender(event: InterActionEvent) {
        let comp = event.component;
        if (!isEmpty(comp) && comp.getId() == "treeSelector1") {
            let treeSelector: TreeGrid | Tree = comp.component.getInnerComoponent();
            let signClassName: string = "dip-custom-selector-list"; // 方便更改样式，这里增加有个特定class
            if (!treeSelector.domBase.classList.contains(signClassName)) {
                treeSelector.domBase.classList.add(signClassName);
            }
            this.initSelectorListRowIcon(treeSelector);
        }
    }

    /**
     * 给指定选择列表行增加行图标样式 
     * @param treeSelector 选择列表组件对象
     * 注意：
     * 1）多选列表包含两个内置类：TreeGrid（树多列列表） 和 Tree
     * 2）加载数据会将起数据写入到第一个DOM中，若此步createElement("span")后，将其放置第一位，则数据会被写入到图标所在DOM中
     * 解决办法：由于图标可依附于已存在的DOM，此步不创建新的<span>来放置图标，而是直接将其图标的Class放入已有的数据DOM中：item-caption-text
     * 
     * 3）考虑到部分DOM的数值加载可能失败，导致页面满足条件的日期，部分加锁，部分不加锁
     * 解决办法：一开始将所有日期都加锁，然后将满足条件的DOM，去除加锁图标
     */
    private initSelectorListRowIcon(treeSelector: TreeGrid | Tree): void {
        let customSelectorListIcon: string = "custom-selector-list-icon";
        let customSelectorListNoIcon: string = "custom-selector-list-no-icon";
        if (treeSelector.domBaseClassName == "treegrid-base") { // 校验是否为多列树
            /**
             * (treeSelector as TreeGrid).createItem = (data: any) => {
                    let treeGridRow: TreeGridRow = new TreeGridRow(treeSelector as TreeGrid, data);
                    let oldCreateDom = treeGridRow._createDom;
                    treeGridRow._createDom = (accept?: boolean) => {
                        let rowDom: HTMLElement = oldCreateDom.call(treeGridRow, accept);
                        rowDom.szobject.domCaption.classList.add("custom-selector-list-icon");
                        return rowDom;
                    }
                    return treeGridRow; 
                }
             */
            (treeSelector as TreeGrid).onrenderitem = (item: ListItem) => {
                let itemCaption: string = item.getCaption();
                let itemValue: string = item.getValue();
                /**
                 * 注意：
                 * 1）多选列表创建的行DOM个数仅当前页面可显示的，不包括隐藏的
                 * 2）上下滚动，会将隐藏的值赋值到已存在的行DOM上（仅改变原行DOM值和DOM的ID值，而不会重新创建行DOM），导致一个行DOM可能被修改多次
                 * 3）从（2）得到：值变，而DOM不变，就会出现：之前不满足条件的DOM，后续值的改变，会被满足，从而赋值图标样式
                 * 4）解决办法：每次处理单个行DOM时，就删除其添加的样式
                 */
                item.domCaption.classList.remove(customSelectorListIcon);
                item.domCaption.classList.remove(customSelectorListNoIcon);

                if (itemCaption.indexOf("日") == -1) { // 若当前item为顶层节点（仅年月），则不做处理
                    return;
                }
                if (!this.checkDateTimeIsLegal(itemValue)) {
                    item.domCaption.classList.add(customSelectorListIcon);
                } else {
                    item.domCaption.classList.add(customSelectorListNoIcon);
                }
            }
        }
    }

    /**
     * 校验日期是否有效
     * 校验规则：若日期值与当前日期值对比，相差15天及以上，则失效
     * @param dateStr 日期字符串，eg：20220700
     * @return true | false
     */
    private checkDateTimeIsLegal(dateStr: string): boolean {
        if (isEmpty(dateStr)) {
            return true;
        }
        let isLegal: boolean = true;
        dateStr = `${dateStr.replace(/^(\d{4})(\d{2})(\d{2})$/, '$1-$2-$3')} 00:00:00`; // 将其日期格式转换为：2022-07-00
        let currentNow: number = Date.now();
        let compareTime: number = new Date(dateStr).getTime();
        if ((currentNow - compareTime) > this.FifteenDateTime) { // 若两者之差大于15天，则判断时间为锁定状态
            isLegal = false;
        }
        return isLegal;
    }
}