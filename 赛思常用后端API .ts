/**
 * =====================================================================
 * 作者：tuzhengwei
 * 创建日期：20220215
 * 脚本用途：
 * 		记录常见的后端API方法
 * ====================================================================
 */
import { getCachedValue, putCachedValue } from "svr-api/memcache";
import db from 'svr-api/db';
/**
 * 1. 分组查询
 */
function queryDataFlowLastEtlStatus(dataFlowIds: string[]): { [resid: string]: number } {
    let queryInfo: QueryInfo = {
        sources: [{
            id: "model1",
            path: "/sysdata/data/tables/dw/DW_EXTRACT_LOG.tbl"
        }],
        fields: [{
            name: "RESID", group: true, exp: "model1.RESID"
        }, {
            name: "STATE", group: false, exp: "model1.STATE"
        }, {
            name: "ID", group: false, exp: "model1.ID"
        }],
        // distinct: true,
        // select: false,
        filter: [{
            exp: `model1.RESID = param1`
        }],
        having: [{
            exp: `ROW_NUMBER(ORDERBY(model1.START_TIME,'desc'), model1.RESID) = 1`
        }],
        options: {
            limit: 3
        },
        params: [{
            name: "param1", value: dataFlowIds
        }],
        sort: [{
            exp: "model1.START_TIME", type: "desc"
        }]
    }

    print(dw.getQuerySql(queryInfo));


    let queryDatas = dw.queryData(queryInfo).data;
    if (queryDatas.length == 0) {
        return {};
    }

    print("----");
    print(queryDatas);

    let dataFlowInfo: { [resid: string]: number } = {};
    for (let i = 0; i < queryDatas.length; i++) {
        let datas = queryDatas[i];
        dataFlowInfo[datas[0] as string] = datas[1] as number;
    }
    return dataFlowInfo;
}
 
 /**
  * 2. SQL过滤
  */
ds.formatConstValue
table.update(item, `HZDJH='${ds.formatConstValue(item.HZDJH)}' AND YYID='${ds.formatConstValue(item.YYID)}' AND IS_DELETE IS NULL`);

/**
 * 3. 获取平台某个文件（java.io.File）
 */
 import metadata from "svr-api/metadata";
 function getFileIo(): void {
	 let content = metadata.runDocx({
        // path: "http://192.168.3.100:5055/zhjg/ZHJG/app/home.app/jgrw/WLJYJG/新建 DOCX 文档.d.docx?:viewSource=false&id=0-211223-wx-61619-1&:export=网络交易监管信息登记表.docx"
        /** 元数据路径 */
        path: "/ZHJG/app/home.app/jgrw/WLJYJG/新建 DOCX 文档.d.docx",
        params: {
            // ?: 是什么意思
            ":viewSource": "false",
            "id": "0-211223-wx-61619-1",
            ":export": "网络交易监管信息登记表.docx"
        }
    });
	
	let workDir = getWorkDir();
    let temp = getFile(`${workDir}/2022-01-04/tempSaveZipFile/`);

    let fileIn = null;
    let fileOut = null;
    try {
        fileIn = new FileInputStream(content); // 读文件二进制流
        let newFile = new java.io.File(`${workDir}/2022-01-04/tempSaveZipFile/网络交易监管信息登记表.docx`);
        if (!newFile.exists()) {
            newFile.createNewFile();
        }
        fileOut = new FileOutputStream(newFile);
        IOUtils.copy(fileIn, fileOut);

        print(temp.listDirs());
        print(temp.listFiles());
        if (newFile.exists()) {
            newFile.delete();
        }
        print(temp.listDirs());
        print(temp.listFiles());
    } catch (e) {
        print(e);
    } finally {
        fileOut && fileOut.close();
        fileIn && fileIn.close();
    }
 }
 
 
/**
 * 4. 脚本执行数据加工
 */
 import etl from "svr-api/etl";
 etl.runETL(resid); // 参数：模型id 或 path
 
/**
 * 5. 获取数据加工/模型查询的SQL
 * 20220401 tuzw
 * 检测的模型包含：数据加工，其不包含提取的物理表，仅一个视图表，故：无法通过指定物理表来查询数据
 * 解决办法：通过产品的getQuerySql()获取生成模型/数据加工的主键值临时表，将随机查询范围定位到这个临时表
 * @param modelResid 检测模型的resid值
 * @param filterConditions 随机的范围过滤条件（检测模型共有字段EXP），eg："outDate='20220202'"
 * eg：SELECT pk.流水号 FROM  (select distinct t0.LSH as 流水号 from baglxt.jcb_jbqk t0 left join baglxt.lsh_ba_ztb t1 on .... 
 */
function getViewModelQuerySQL(modelResid: string, filterConditions?: string): string {
    let pk = getViewTablePrimaryKeys(modelResid)[0];
    let modelPath: string = getModelPath(modelResid);
    console.debug(`模型【${modelResid}】对应主键：${pk}，模型路径：${modelPath}，随机范围过滤条件：${filterConditions}`);
    if (!pk || !modelPath) {
        return "";
    }
    let filter: FilterInfo[] = [];
    if (!!filterConditions) {
        filter.push({
            exp: filterConditions
        });
    }
    let queryInfo: QueryInfo = {
        resid: modelResid,
        distinct: true,
        fields: [{ name: `${pk}`, exp: `model1.${pk}` }],
        filter: filter,
        sources: [{
            id: "model1",
            path: modelPath
        }]
    }
    let queryInfoSQL = dw.getQuerySql(queryInfo);
    console.debug(`获取模型【${modelResid}】数据加工/模型查询的SQL：${queryInfoSQL}`);
    return queryInfoSQL;
}


/**
 * 6. 根据模型id获取模型的路径
 */
function getModelPath(modelResid: string): string {
    let model: MetaFileInfo = metadata.getFile(modelResid);
    if (!model) {
        return "";
    }
    let modelPath: string = model.path;
    return modelPath;
}

/**
 * 7. 获得视图模型的主键集
 * @param modelResid 检测模型的resid值
 */
function getViewTablePrimaryKeys(modelResid: string): string[] {
    let model = dw.getDwTable(modelResid);
    if (!model) {
        return [];
    }
    let pk = model.properties && model.properties.primaryKeys;
    if (!pk) {
        return [];
    }
    console.debug(`getViewTablePrimaryKeys()，模型【${modelResid}】的主键为：${pk}`);
    return pk;
}

/**
 * 8. 系统日志
 */
sys.fireEvent({
	name:"test.script",
	logLevel:"IMPT",
	operationState:"OK"
});

/**
 * 9. 自定义异常
 * 注意：部分环境，需要：print(e.toString())
 */
throw new Error("测试异常");

/**
 * 10. 清理后端缓存
 * 注意：部分环境，需要：print(e.toString())
 */
import { getDwTable, refreshState } from "svr-api/dw";
refreshState(modelPath); // 清理后端缓存数据


/**
 * 11. HashMap 转 String
 */
import JAVA_JSON = com.alibaba.fastjson.JSON;
params = JAVA_JSON.toJSONString(params);


/**
 * 12. 随机获取表数据
 */
import dw from "svr-api/dw";
function main() {
    let a = getModelRandomDatas("/HBAGL/data/tables/etlFile/JCB-ZK/JBQK.tbl", ["LSH"], 4, "model1.FCYDATE = '202107'");
    print(a);
}
/**
 * 获取某表随机N条数据
 * @param modelPath 数据源名
 * @param pkFieldName 查询的主键字段
 * @param randomNumber 随机查询的条数
 * @param filterCondtionExp 随机的范围过滤条件，eg："model1.ID='360825'"
 * @return 
 */
function getModelRandomDatas(modelPath: string, queryFields: string[], randomNumber: number, filterCondtionExp?: string): (string | number | boolean)[][] {
    let queryFieldInfos: QueryFieldInfo[] = [];
    for (let i = 0; i < queryFields.length; i++) {
        queryFieldInfos.push({
            name: queryFields[i],
            exp: `model1.${queryFields[i]}`
        });
    }
    let fileter: FilterInfo[] = [];
    if (filterCondtionExp != undefined) {
        fileter.push({
            exp: filterCondtionExp
        });
    }
    let queryInfo: QueryInfo = {
        sources: [{
            id: "model1",
            path: modelPath
        }],
        fields: queryFieldInfos,
        options: {
            limit: randomNumber
        },
        filter: fileter,
        sort: [{
            exp: "RAWSQL_STR('dbms_random.random()')"
        }]
    }
    return dw.queryData(queryInfo).data;
}


/**
 * 遍历Map
 */
/** 当前会话配置项 */
let configuration = tableEnv.getConfig().getConfiguration();
let map = configuration.toMap();
let iterator = map.keySet().iterator();
while (iterator.hasNext()) {
	let key = iterator.next();
	this.logger.addLog(`属性名[${key}], 属性值[${map.get(key)}]`);
}

/**
 * 9. 将元数据文件内容转换为MAP对象
 */
import SzObjectMapper = com.fasterxml.jackson.databind.ObjectMapper;
let mapper = new SzObjectMapper();
let customFile = metadata.getFile(modelPath);
let input = null;
try {
	input = customFile.getContentAsInputStream();
	let reader = new java.io.InputStreamReader(input, "utf-8");
	let javaType = mapper.getTypeFactory().constructParametricType(java.util.LinkedHashMap.class, java.lang.String.class, java.lang.Object.class);
	let map = mapper.readValue(reader, javaType);
} catch (e) {
	print(e.toString())
	print(e);
} finally {
	input && input.close();
}

