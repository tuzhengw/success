/**
 * =====================================================
 * 作者：tuzw
 * 创建日期:2022-06-29
 * 功能描述：将JSON转换为XML格式
 * 借鉴代码：https://jira.succez.com/browse/CSTM-18816?focusedCommentId=197180&page=com.atlassian.jira.plugin.system.issuetabpanels%3Acomment-tabpanel
 * 案例：
 * JSON -> XML
    <?xml version="1.0" encoding="UTF-8"?>
    <REQUEST>
        <REQUEST_HEAD>
            <DATA_TIME>20220629163259</DATA_TIME> // 日期
            <DATACOUNT>1</DATACOUNT> // 数据总量
            <DATA_TABLE>JG_CHECK</DATA_TABLE> // 表名
        </REQUEST_HEAD>
        <DATA> // XML数据内容
            <CHECK_DATAS>
                <CHECK_DATA>
                    <NO>30</NO>
                    <ENTNAME/>
                    <UNISCID/>
                    <LINK_TEL/>
                    <ADDR/>
                    <CHECK_CONTENT/>
                    <CHECK_QUESTION/>
                    <CHECK_DATE/>
                    <CHECK_UNIT/>
                    <CHECK_INFO/>
                    <IF_FORCE/>
                    <IF_AJ_RESOURCE/>
                    <YC_NAME/>
                    <YC_SOURCE/>
                    <IF_TRANSFER/>
                    <CHECK_RESULT/>
                    <N_LINK_ADDR/>
                    <QJ_SERIOUS/>
                    <REMAKE/>
                    <IF_PUBLIC/>
                    <CHKANTHUNISCID/>
                    <REC_OPER_TYPE/>
                    <DATADEPT_ID/>
                </CHECK_DATA>
            </CHECK_DATAS>
        </DATA>
    </REQUEST>
    -----------------------------------------------------
    XML -> JSON

    {
    "REQUEST": {
        "DATA": {
            "CHECK_DATAS": {
                "CHECK_DATA": ""
            }
        },
        "REQUEST_HEAD": {
            "DATA_TIME": "20220629175123",
            "DATACOUNT": "1",
            "DATA_TABLE": "JG_CHECK"
        }
    }
}
 * =====================================================
 */
import { getDataSource } from "svr-api/db";
import { request } from "svr-api/http";
import { sleep } from "svr-api/sys";
import { uuid } from "svr-api/utils";
import Base64 = java.util.Base64;
import DateTimeFormatter = java.time.format.DateTimeFormatter;
import LocalDateTime = java.time.LocalDateTime;
import DocumentFactory = org.dom4j.DocumentFactory;
import Timestamp = java.sql.Timestamp;
import SimpleDateFormat = java.text.SimpleDateFormat;
const formatter = new SimpleDateFormat("yyyy-MM-dd");
import DOM4J_Element = org.dom4j.Element;
import JSONArray = com.alibaba.fastjson.JSONArray;
import FastjsonJSONObject = com.alibaba.fastjson.JSONObject;
import DocumentHelper = org.dom4j.DocumentHelper;

/**
 * 程序入口
 */
function main() {
    let xml = xmlToJsonUtils.generateXml([{
        NO: 30
    }]);
    print(xml);
    print(xmlToJsonUtils.xmlToJsonArr(xml));
}

/**
 * 字段信息
 */
interface FieldInfo {
    /** 名称 */
    name: string;
    /** 类型 */
    type?: "string" | "number" | "date";
}

/** 
 * XML每条数据所包含的字段信息
 * 注意：若查询是基于dw模块，则需要跟query的field区分
 */
const fieldInfos: FieldInfo[] = [
    { name: "NO" },
    { name: "ENTNAME" },
    { name: "UNISCID" },
    { name: "LINK_TEL" },
    { name: "ADDR" },
    { name: "CHECK_CONTENT" },
    { name: "CHECK_QUESTION" },
    { name: "CHECK_DATE", type: 'date' },
    { name: "CHECK_UNIT" },
    { name: "CHECK_INFO" },
    { name: "IF_FORCE" },
    { name: "IF_AJ_RESOURCE" },
    { name: "YC_NAME" },
    { name: "YC_SOURCE" },
    { name: "IF_TRANSFER" },
    { name: "CHECK_RESULT" },
    { name: "N_LINK_ADDR" },
    { name: "QJ_SERIOUS" },
    { name: "REMAKE" },
    { name: "IF_PUBLIC" },
    { name: "CHKANTHUNISCID" },
    { name: "REC_OPER_TYPE" },
    { name: "DATADEPT_ID" }
];


/** XML与JSON互转工具类 */
export class XmlToJsonUtils {

    /** XML每条数据所包含的字段信息 */
    public fieldInfos: FieldInfo[];

    constructor(args) {
        this.fieldInfos = !!args.fieldInfos ? args.fieldInfos : [];
    }

    /**
     * 生成XML
     * @param data 转换为XML的JSON数组
     * @return 
     */
    public generateXml(data: ArrayLike<{ [p: string]: any; }>): string {
        let factory = DocumentFactory.getInstance();
        let document = factory.createDocument();
        let request = document.addElement("REQUEST");
        let requestHead = request.addElement("REQUEST_HEAD");
        requestHead.addElement("DATA_TIME").addText(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")));
        requestHead.addElement("DATACOUNT").addText(`${data.length}`);
        requestHead.addElement("DATA_TABLE").addText("JG_CHECK");
        let permitDates = request.addElement("DATA").addElement("CHECK_DATAS");

        for (let i = 0; i < data.length; i++) { // 循环增添XML数据内容
            let permitDate = permitDates.addElement("CHECK_DATA");
            let item = data[i];
            this.addData(item, permitDate);
        }
        let xml = document.asXML();
        return xml;
    }

    /**
     * 往XML的DATA中增添一条数据
     * @param data 单条JSON数据
     * @param element XML元素对象
     */
    private addData(data: { [fieldName: string]: any; }, element: DOM4J_Element): void {
        for (const field of this.fieldInfos) { // 仅处理指定字段数组内的key
            const value = data[field.name];
            this.addElement(element, field, value);
        }
    }

    /**
     * 创建字段标签，eg：age => <age>12</age>
     * @param element XML元素对象
     * @param field 字段信息，eg：{ "field": "age", "type": "number" }
     * @param value 字段值
     */
    private addElement(element: DOM4J_Element, field: FieldInfo, value: any): void {
        const addedElement = element.addElement(field.name);
        if (value == null) return;
        if (field.type === "string" || field.type == null || field.type == 'number') {
            addedElement.addText(`${value}`);
        } else if (field.type === "date") {
            const text = formatter.format(value);
            addedElement.addText(text);
        }
    }

    /**
     * XML转JSON数组
     * 参考：https://blog.csdn.net/qq_37312838/article/details/124205618
     * @param xml 需要处理的XML内容
     * @return 
     */
    public xmlToJsonArr(xml: string): FastjsonJSONObject {
        let jsonObject = new FastjsonJSONObject();
        let document = DocumentHelper.parseText(xml);
        let root = document.getRootElement();  // 获取根节点元素对象
        this.iterateNodes(root, jsonObject);
        return jsonObject;
    }

    /**
     * 遍历元素
     * @param node 元素
     * @param json 将元素遍历完成之后放的JSON对象
     */
    private iterateNodes(node: DOM4J_Element, json: FastjsonJSONObject) {
        /** 获取当前元素的名称 */
        let nodeName = node.getName();
        if (json.containsKey(nodeName)) { // 判断已遍历的JSON中是否已经有了该元素的名称
            let nodeObject = json.get(nodeName); // 该元素在同级下有多个
            let array: JSONArray = null;
            if (nodeObject instanceof JSONArray) {
                array = nodeObject as JSONArray;
            } else {
                array = new JSONArray();
                array.add(nodeObject);
            }
            /** 该元素下所有子元素 */
            let listElement: DOM4J_Element[] = node.elements();
            if (listElement.isEmpty()) { // 该元素无子元素，获取元素的值
                let nodeValue: string = node.getTextTrim();
                array.add(nodeValue);
                json.put(nodeName, array);
                return;
            }
            /** 子元素 */
            let newJson = new FastjsonJSONObject();
            for (let i = 0; i < listElement.size(); i++) { //递归遍历所有子元素
                this.iterateNodes(listElement.get(i), newJson);
            }
            array.add(newJson);
            json.put(nodeName, array);
        } else {   // 该元素同级下第一次遍历
            /** 获取该元素下所有子元素 */
            let listElement: DOM4J_Element[] = node.elements();
            if (listElement.isEmpty()) { // 该元素无子元素，获取元素的值
                let nodeValue = node.getTextTrim();
                json.put(nodeName, nodeValue);
                return;
            }
            let childrenNodes = new FastjsonJSONObject(); // 有子节点，新建一个JSONObject来存储该节点下子节点的值
            for (let i = 0; i < listElement.size(); i++) { // 递归遍历所有一级子节点
                this.iterateNodes(listElement.get(i), childrenNodes);
            }
            json.put(nodeName, childrenNodes);
        }
    }
}
const xmlToJsonUtils = new XmlToJsonUtils(fieldInfos);