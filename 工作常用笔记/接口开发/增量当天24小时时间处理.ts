
import utils from "svr-api/utils";
import http from "svr-api/http";
import { getDataSource } from "svr-api/db"; //数据库相关API
import { getJSON, getString, getDwTable, modifyFile } from 'svr-api/metadata';
import { queryDwTableData, queryData, openDwTableData } from "svr-api/dw";
import LocalDateTime = java.time.LocalDateTime;
import LocalTime = java.time.LocalTime
import ZoneOffset = java.time.ZoneOffset;
import sys from "svr-api/sys";
import metadata from "svr-api/metadata";

/** 
 * 获取查询的过滤条件
 * @param sendType 推送类型【全量 | 增量】
 * @param filterColumn 增量字段名
 * @reutnr { result: true, data: [{exp: "model1.CREATE_DATE" > "2021-12-12 23:55:00"}] }
 */
public getQueryFilterCondition(sendType: string, filterColumn: string): ResultInfo {
	if (sendType == "增量") {
		let now = LocalDateTime.now();
		let currentDay = LocalDateTime.of(now.toLocalDate(), LocalTime.MIN);
		let times = currentDay.toInstant(ZoneOffset.of("+8")).toEpochMilli();
		let sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		let lastSendTime: string = sdf.format(new java.util.Date(times - 24 * 60 * 60 * 1000 - 5 * 60 * 1000)); // eg："2021-12-12 23:55:00"
	}
	return { result: true, data: [] };
}
	


/** 
 * 获取查询的过滤条件
 * @param sendType 推送类型【全量 | 增量】
 * @param filterColumn 增量字段名
 * @reutnr { result: true, data: [{exp: "model1.CREATE_DATE" > "2021-12-12 23:55:00"}] }
 */
public getQueryFilterCondition(sendType: string, filterColumn: string): ResultInfo {
	if (sendType == "增量") {
		let now: number = new Date().setHours(0, 0, 0);
		let yearterDay: number = now - 24 * 60 * 60 * 1000 - 5 * 60 * 1000;
		let lastSendTime: string = utils.formatDate("yyyy-MM-dd HH:mm:ss", yearterDay);
		print(`当前请求方式是：增量请求，过滤表达式：model1.${filterColumn} > '${lastSendTime}'`);
		return { result: true, data: [{ exp: `model1.${filterColumn} > '${lastSendTime}'` }] };
	}
	return { result: true, data: [] };
}