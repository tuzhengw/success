import { IMetaFileCustomJS, InterActionEvent, API } from "metadata/metadata-script-api";
import { rc, showWarningMessage, showSuccessMessage, uuid, SZEvent, showConfirmDialog } from "sys/sys"
import { updateDwTableDatas, exportQuery } from 'dw/dwapi';
import { Dialog, showFormDialog } from "commons/dialog";
import { getQueryManager } from 'dw/dwapi';

/** 自定义配置：系统短路径 */
const VIEWSOURCEURL = "https://gsbi.succez.com/HBZHSCJG/pcspzs/";
/** 领码记录表路径 */
const SZ_APPLY_CODE_PATH = "/SPZS/data/tables/FACT/MGL/SZ_APPLY_CODE.tbl";

export const CustomJS: { [type: string]: any } = {
    spg: {
        onDidLoadFile: (event) => {
            return import("./LodopFuncs.js");
        },
        CustomActions: {
            /**
             * 打印码信息 20211123
             * 根据【领码记录ID】过滤【领码记录表】数据
             */
            printOnlineQRcode: (event) => {
				let userAgent = navigator.userAgent.toLowerCase();
                // 校验当前系统是否Mac
                if (userAgent.indexOf('Mac') > -1) {
                    showMessageBox({
                        content: "打印码暂时不支持Mac系统",
                        type: MessageBoxType.INFO,
                        top: 200
                    });
                    return;
                }
                let dataRow = event.dataRow;
                /** 领码记录ID */
                let applyId: string = event.params.applyId;
                let queryInfo: QueryInfo = {
                    sources: [{
                        id: "model1",
                        path: `${SZ_APPLY_CODE_PATH}`
                    }],
                    fields: [
                        { name: "CODE", exp: "model1.CODE" },
                        { name: "PRODUCT_TYPE", exp: "model1.PRODUCT_TYPE" }
                    ],
                    filter: [{
                        exp: `model1.APPLY_ID ='${applyId}'`
                    }]
                }
                getQueryManager().queryData(queryInfo, uuid()).then((result) => {
                    let querydata = result.data;
                    import("./LodopFuncs.js").then(m => {
                        try {
                            /** 码的宽度 */
                            let width = event.params.barCodeStyle == "style1" ? 700 : "50mm";
                            /** 码的高度 */
                            let height = event.params.barCodeStyle == "style1" ? 500 : "30mm";

                            /** 获取LODOP对象 */
                            let LODOP = getLodop();
                            if (LODOP && querydata.length > 0) {
                                LODOP.PRINT_INIT("");
                                LODOP.SET_PRINT_PAGESIZE(2, width, height, "5mm");
                                LODOP.SET_PRINT_STYLE("FontSize", 10);
                                LODOP.SET_PRINT_STYLE("Bold", 1);
                                LODOP.SET_PRINT_MODE("POS_BASEON_PAPER", !0);

                                // LODOP.SET_PRINT_MODE("FULL_WIDTH_FOR_OVERFLOW", true);
                                // LODOP.SET_PRINT_MODE("FULL_HEIGHT_FOR_OVERFLOW", true);

                                // 生成多个二维码
                                querydata.forEach(data => {
                                    LODOP.NewPage();
                                    let url = "".concat(VIEWSOURCEURL).concat(data[0]);
                                    data[1]
                                        ? LODOP.ADD_PRINT_TEXT("4%", 0, "100%", "10%", "鄂食安")
                                        : LODOP.ADD_PRINT_TEXT("10%", 0, "100%", "10%", "鄂食安");
                                    width == 700
                                        ? LODOP.SET_PRINT_STYLEA(0, "FontSize", 16)
                                        : LODOP.SET_PRINT_STYLEA(0, "FontSize", 11);

                                    LODOP.SET_PRINT_STYLEA(0, "Alignment", 2);
                                    // 给生成的二维码添加序号
                                    // LODOP.ADD_PRINT_TEXT("70%", 0, "100%", "10%", data[1]);
                                    // LODOP.SET_PRINT_STYLEA(0, "Alignment", 2);
                                    // LODOP.SET_PRINT_STYLEA(0, "TextNeatRow", !0);

                                    width == 700
                                        ? LODOP.ADD_PRINT_BARCODE("20%", "11%", "20%", "30%", "QRCode", url)
                                        // : LODOP.ADD_PRINT_BARCODE(40, "2mm", 120, 120, "QRCode", url);
                                        : LODOP.ADD_PRINT_BARCODE(40, 14, 100, 100, "QRCode", url);
									
									// 注意：码的大小跟二维码的内容（url）有关系，这里需要调整一下（注意版本）
                                    LODOP.SET_PRINT_STYLEA(0, "QRCodeVersion", 7);
                                    //LODOP.ADD_PRINT_TEXT("28%", "75%", "30%", "", "一张网");
                                    width == 700
                                        ? LODOP.ADD_PRINT_TEXT("80%", 0, "100%", "", data[0])
                                        : LODOP.ADD_PRINT_TEXT("80%", 0, "100%", "", data[0]);

                                    LODOP.SET_PRINT_STYLEA(0, "FontSize", width == 700 ? 8 : 6);
                                    LODOP.SET_PRINT_STYLEA(0, "Alignment", 2);
                                    LODOP.SET_PRINT_STYLEA(0, "TextNeatRow", true);
                                });

                                //LODOP.PRINT_DESIGN();
                                LODOP.PREVIEW();
                            } else {
                                // 若LODOP=false，并且导出数据不为空，则提示：未安装Lodop插件
                                if (querydata.length > 0) {
                                    showFormDialog({
                                        id: "custom.not.install",
                                        caption: "尚未安装Lodop插件",
                                        content: {
                                            items: [{
                                                id: "message",
                                                formItemType: "text",
                                                value: "Web打印服务CLodop未安装启动，请点击下方下载按钮进行安装（若此前已安装过，可点击刷新按钮直接再次启动），成功后请刷新本页面",
                                                captionVisible: false,
                                                formItemCaptionVisible: false
                                            }]
                                        },
                                        buttons: [{
                                            id: "download",
                                            caption: "下载",
                                            layoutTheme: "defbtn",
                                            onclick: (event: SZEvent, dialog: Dialog, item: any) => {
                                                // 存放插件的位置
                                                window.location.href = "/SPZS/public/打印插件/CLodop_Setup_for_Win32NT.exe";
                                            }
                                        }, {
                                            id: "refresh",
                                            caption: "刷新",
                                            layoutTheme: "defbtn",
                                            onclick: (event: SZEvent, dialog: Dialog, item: any) => {
                                                window.location.href = "CLodop.protocol:setup";
                                            }
                                        }]
                                    });
                                } else {
                                    showConfirmDialog({
                                        caption: "提示",
                                        message: `未获取到此次领码相关二维码数据`,
                                    })
                                }
                            }
                        } catch (err) {
                            alert(err);
                        }
                    })
                })
            },
            /**
             * 下载码信息 20211123 
             * 根据【领码记录ID】过滤【领码记录表】数据，并将其过滤后的数据导出
             */
            exportExcel: (event) => {
                let dataRow = event.dataRow;
                /** 领码记录ID */
                let applyId = event.params.applyId;
                let queryInfo: QueryInfo = {
                    sources: [{
                        id: "model1",
                        path: `${SZ_APPLY_CODE_PATH}`
                    }],
                    fields: [
                        { name: "二维码", exp: `CONCAT('${VIEWSOURCEURL}',model1.CODE)` },
                        { name: "溯源码", exp: "model1.CODE" },
                        { name: "产品类别", exp: "model1.PRODUCT_TYPE" }
                    ],
                    filter: [{
                        exp: `model1.APPLY_ID='${applyId}'`
                    }],
                    sort: [{
                        exp: "model1.CODE",
                        type: "asc"
                    }],
                    options: {
                        offset: 0,
                        needCodeDesc: true
                    }
                };
                exportQuery({
                    resid: "H2PCcJPVQ1ypGQ9v1L6KG",
                    query: queryInfo,
                    fileFormat: DataFormat.xlsx,
                    fileName: '领取空码(商品二维码)'
                });
            }
        }
    }
}