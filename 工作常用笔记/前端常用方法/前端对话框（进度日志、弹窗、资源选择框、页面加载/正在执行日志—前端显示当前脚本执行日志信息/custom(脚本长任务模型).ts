import { IMetaFileViewer, MetaFileViewerArgs } from "metadata/metadata";
import { BaseTemplatePage } from "app/templatepage";
import { MobileFramePageManager } from "app/mobilepage";
import {
    rc_task, ServiceTaskInfo, ctx, SZEvent, Component, throwInfo, showConfirmDialog, showFormDialog, showErrorMessage, showMenu, isEmpty, deepAssign,
    uuid, message, quoteExpField, showDialog, showSuccessMessage, showProgressDialog, rc, downloadFile, waitRender, showWaiting, assign, IDataProvider, formatDateFriendly, tryWait, rc_get, showWarningMessage, wait
} from 'sys/sys';
import { IMetaFileCustomJS, InterActionEvent, IAppCustomJS, IDwCustomJS, FAppInterActionEvent, IFApp } from "metadata/metadata-script-api";
import { InitializeService } from "./initialize/initializa.js";

/** 初始化执行状态记录表 */
interface INIT_EXECUTE_STATUS_TABLES {
    /** 任务序号 */
    TASK_ID: string;
    /** 任务名称 */
    TASK_NAME: string;
    /** 当前是否正在执行 */
    IS_RUNING: number;
    /** 是否已初始化过 */
    IS_INITEN: number;
    /** 上次执行状态 */
    LAST_TASK_STATUS: string;
    /** 当前执行生成的日志序号 */
    LOG_UUID: string;
}

export const CustomJS: { [key: string]: IMetaFileCustomJS } = {
    spg: {
        CustomActions: {
            /**
             * 测试脚本日志显示到前端
             * 案例地址：https://jira.succez.com/browse/BI-41963
             */
            stepTask: (event: InterActionEvent) => {
                return rc({
                    url: `/sysdata/app/sdi_initialize.app/process.action?method=getInitExecuteStatusTableInfos`,
                    method: "get"
                }).then((result: INIT_EXECUTE_STATUS_TABLES) => {
                    if (!result) {
                        showWarningMessage(`获取初始化状态表信息失败`);
                    }
                    let isRuning: number = result.IS_RUNING;
                    if (isRuning == 0) {
                        let taskId: string = uuid();
                        showProgressDialog({
                            url: '/sysdata/app/sdi_initialize.app/process.action?method=showProcess&:rcuuid=f21a544446e54a0f93717fc232b58854',
                            caption: "测试进度条",
                            data: {
                                taskId: taskId
                            },
                            uuid: taskId,
                            /**
                             * 问题：由于原有【关闭逻辑】会关闭【轮询请求】日志信息，导致后续重新打开，日志不会轮询请求
                             * 改进：重写关闭逻辑，关闭仅关闭对话框，不关闭轮询
                             */
                            onclose: (e, dialog) => {
                                dialog.close();
                                return false;
                            },
							// logsVisible: false, // 可设置不显示日志, 仅显示进度条
							onfinish: (e: SZEvent, component: Component, item: any) => {
								let responseResult = e.data; // 后端脚本执行后返回的结果
							}
                        });
                    } else {
                        let logId: string = result.LOG_UUID;
                        showProgressDialog({
                            caption: "程序正在执行中",
                            uuid: logId,
                            startPoll: true,
                            onclose: (e, dialog) => {
                                dialog.close();
                                return false;
                            }
                        });
                    }
                });
            }
        }
    },
    "initializaSpg.spg": new InitializeService()
};