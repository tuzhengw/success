


/**
 * 共享文档查询
 */
export class SpgRegisterWordCx {
	public CustomActions: any;
	constructor() {
		this.CustomActions = {
			button_batchImportXmlWordFiles: this.button_batchImportXmlWordFiles.bind(this)
		}
	}

	/**
	 * 批量导出列表勾选的XML文档文件
	 * @param xmlAttachInfos 列表勾选的xml文档附件信息, eg: workdir:2022122717480669101_1.病历概要.xml,...
	 */
	public button_batchImportXmlWordFiles(event: InterActionEvent): void {
		let params = event.params;
		let xmlAttachInfos: string[] = params?.xmlAttachInfos;
		if (isEmpty(xmlAttachInfos)) {
			showWarningMessage(`列表未勾选导出行`);
			return;
		}
		if (!Array.isArray(xmlAttachInfos)) {
			xmlAttachInfos = xmlAttachInfos.split(",");
		}
		let zipCheckedXmlAttach: Promise<any> = rc({
			url: "/xml/xmlBusinessScript/zipSignXmlWordFiles",
			data: {
				xmlAttachInfos: xmlAttachInfos
			}
		});
		showWaiting(zipCheckedXmlAttach).then((resultInfo => {
			if (!resultInfo.result) {
				showWarningMessage(resultInfo.message);
				return;
			}
			let zipPath: string = resultInfo.zipPath;
			downloadFile(`/HLHT/app/shareWord.app/API/xmlBusinessScript.action?method=importSignZipFile&zipFilePath=${zipPath}&isDeleteZip=true`);
		}));
	}
}