

/**
 * 获取选中的文件夹下的所有子文件和选择的文件
 * @param dirPathOrFile 当前勾选的目录路径或者文件
 * @param choseResourceId 选择的资源ID（模型、文件、样式等）
 */
public getChoseModelId(dirPathOrFiles, choseResourceId: string[]): void {
	dirPathOrFiles.forEach(item => {
		if (item.isFolder && item?.children?.length > 0) {
			let children = item.children;
			this.getChoseModelId(children, choseResourceId);
		} else if (!item.isFolder) {
			let id: string = item.id;
			choseResourceId.push(id);
		}
	})
}


/**
 * 产品的metaData.searchFiles 遍历
 * 支持递归，默认：false
 *
 * 获取APP下所有需要校验的SPG文件路径
 * @param listDirPath 遍历的父路径
 * @return eg: ["/sysdata/app/zt.app/data-access/test_pre_db_mgr.spg"]
 */
private getSignAppAllSpgPaths(listDirPath: string): string[] {
	if (isEmpty(listDirPath) || metadata.getFolder(listDirPath) == null) {
		logger.addLog(`路径: ${listDirPath} 不存在`);
		return [];
	}
	let saveSpgFilePaths: string[] = [];
	let metaFiles: MetaFileInfo[] = metadata.searchFiles({
		parentDir: listDirPath,
		recur: true, // 是否支持递归
		type: "spg"
	});
	for(let i = 0; i < metaFiles.length; i++) {
		saveSpgFilePaths.push(metaFiles[i].path);
	}
	return saveSpgFilePaths;
}