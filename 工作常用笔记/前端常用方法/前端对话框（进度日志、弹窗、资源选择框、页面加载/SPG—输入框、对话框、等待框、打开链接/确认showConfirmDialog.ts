import { ctx, SZEvent, Component, throwInfo, throwError, showMessage,
showConfirmDialog, showFormDialog, showMenu, 
isEmpty, deepAssign, uuid, message, quoteExpField, showDialog, 
showSuccessMessage,showErrorMessage,
showProgressDialog, rc, downloadFile, waitRender, 
showWaiting, assign, IDataProvider, formatDateFriendly, parseDate, formatDate, tryWait, 
rc_get, showWarningMessage, timerv, showWaitingMessage, hideMessage, DataChangeEvent,
 EventListenerManager, GroupInfo, IFindArgs, ServiceTaskPromise, ServiceTaskInfo, ServiceTaskState } from 'sys/sys';


/** 仅一种选择，用于提示 */
showConfirmDialog({
		caption: '提示',
		message: `资源或规则表达式书写错误，请检测资源和表达式`,
		buttons: ["ok"]
});

/** 确认后执行onok后的内容 */
showConfirmDialog({
	caption: "数据打标",
	message: `将要从对指定条件的数据进行打标，是否确定？`,
	onok: () => {
		labelData({ labelCode: label_code, labelLibCode: label_lib_code, filters: filters }).then(() => {
			throwInfo("保存成功！")
			return getDwTableDataManager().getLabelLib(label_lib_code);
		}).then(lib => {
			lib && refreshModelState(lib.mainTableId);
		});
	}
})
		
		

/**设置交换任务的状态 */
button_exchange_handleApply: (event: InterActionEvent) => {
	let dataRow = event.dataRow;
	let datasets = event.page.getDatasets();
	let task_id = dataRow.EX_TASK_ID;
	let buttonName = event.component.getTxt();
	let type = 5;
	showConfirmDialog({
		caption: `使用结束`,
		message: `确定"${dataRow.EX_APPLY_NAME}"已使用结束吗？确认之后${dataRow.DX_ORG_DESC}将不能再使用`,
		onok: () => {
			rc({
				url: getAppPath("/data-exchange/exApplyMgr.action?method=handleApply"),
				method: "POST",
				data: {
					task_id: task_id,
					type: type
				}
			}).then(() => {
				throwInfo(`结束使用交换任务成功`);
				datasets.forEach(d => {
					d.refresh({ notifyChange: true, force: true });
				})
			})
		}
	})
}
