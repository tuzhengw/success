# 私有接口方法

​	在 Java 8之前，接口可以有常量变量和抽象方法，而且方法必须是：public

---

## Maven 指定`JDK`

```properties
<properties>
    <maven.compiler.source>9</maven.compiler.source>
    <maven.compiler.target>9</maven.compiler.target>
</properties>
```

## * `Tester.java`

```java
public class Tester {
    public static void main(String[] args) {
        LogOracle logoracle = new LogOracle();
        logoracle.logInfo("info");
        LogMySql logmysql = new LogMySql();
        logmysql.logInfo("info");
        /**
         * Open
         * Log Message : info
         * Close
         * Open
         * Log Message : INFO
         * Close
         */
    }
}
```

## `JDK 8之前`

​	**接口：**可定义<button>常量变量和抽象方法</button>，方法必须：public

- **常量**
- **抽象方法**

### 1 `Logging.interface`

```java
public interface Logging {
    /**
     * JDK 8之前可定：常量变量和抽象方法
     */
    String ORACLE = "Oracle_Database";
    String MYSQL = "MySql_Database";

    void logInfo(String message);

    void getConnection();
    void closeConnection();
}
```

### 2 `LogMySql.java`

```java
public class LogMySql implements Logging{
    @Override
    public void logInfo(String message) {
        getConnection();
        System.out.println("Log Message : " + "INFO");
        closeConnection();
    }

    @Override
    public void getConnection() {
        System.out.println("Open");
    }

    @Override
    public void closeConnection() {
        System.out.println("Close");
    }

```

### 3 `LogOracle.java`

```java
public class LogOracle implements Logging {
    @Override
    public void logInfo(String message) {
        getConnection();
        System.out.println("Log Message : " + message);
        closeConnection();
    }

    @Override
    public void getConnection() {
        System.out.println("Open");
    }

    @Override
    public void closeConnection() {
        System.out.println("Close");
    }
}
```

## `JDK 8`--默认方法和静态方法

​	在 Java 8 接口引入了一些新功能——<button>默认方法和静态方法</button>。我们可以在Java SE 8的接口中编写方法实现，仅仅需要使用 <button>**default** 关键字</button>来定义它们。

在 Java 8 中，一个接口中能定义如下几种变量/方法：

- **常量**
- **抽象方法**
- **默认方法**
- **静态方法**

## 实例

### 1 `Logging.interface`

```java
public interface Logging {
    /**
     * JDK 8之前可定：常量变量和抽象方法
     */
    String ORACLE = "Oracle_Database";
    String MYSQL = "MySql_Database";

    /**
     * JDK 8 新增：默认方法
     */
    default void logInfo(String message){
        getConnection();
        System.out.println("Log Message : " + message);
        closeConnection();
    }

    /**
     * JDK 8 新增：静态方法
     */
    static void getConnection(){
        System.out.println("Open");
    }
    static void closeConnection(){
        System.out.println("Close");
    }
}
```

### 2 `LogMySql.java`

```java
public class LogMySql implements Logging{
}
```

### 3 `LogOracle.java`

```java
public class LogOracle implements Logging {
}
```

## `JDK 9--私有方法和私有静态方法`

​	Java 9 不仅像 Java 8 一样支持接口默认方法，同时还支持<button>私有方法</button>。

​	在 Java 9 中，一个接口中能定义如下几种变量/方法：

- **常量**
- **抽象方法**
- **默认方法**
- **静态方法**
- **私有方法**
- **私有静态方法**

---

## 实例

### 1 `Logging.interface`

```java
public interface Logging {
    /**
     * JDK 8之前可定：常量变量和抽象方法
     */
    String ORACLE = "Oracle_Database";
    String MYSQL = "MySql_Database";
    /**
     * JDK 9 提供了：私有方法
     */
    private void log(String message){
        getConnection();
        System.out.println("Log Message : " + message);
        closeConnection();
    }
    /**
     * JDK 8 新增：默认方法
     */
    default void logInfo(String message){
        log(message);
    }
    /**
     * JDK 9 新增：私有静态方法
     */
    private static void getConnection(){
        System.out.println("Open");
    }
    private static void closeConnection(){
        System.out.println("Close");
    }
}
```

### 2 `LogMySql.java`

```java
public class LogMySql implements Logging{
}
```

### 3 `LogOracle.java`

```java
public class LogOracle implements Logging {
}
```

