
import { updateDwTableData } from "svr-api/dw";


/** 产品记录数据源状态路径 */
const DATASOURCESTATE_PATH = "/sysdata/data/tables/sys/DATASOURCESTATE.tbl";
/** 产品记录数据源状态表字段 */
const DATASOURCESTATE_FIELD = ["DATASOURCE_NAME", "DBTYPE", "HOST", "URL", "USER", "MAXPOOLSIZE", "DBVERSION", "JDBCVERSION", "STATE", "LOG", "LAST_UPDATE_TIME"];

/**
 * 将校验的数据源状态记录到模型
 * @param dataSourceStateInfos 写入的数据源状态信息数组
 * @param dataRecordTableName 状态信息数据记录表路径
 * @param writeFileds 写入数据的字段名数组【与二维数组一一对应】
 * 
 * 20220623 tuzw
 * 考虑到产品创建的数据库状态表部分字段Oracle关键字重名，不使用Db的insert模块，改为dw模块
 */
function insertDbSourceStateInfoToModel(dbSourceStateInfos: string[][], dataRecordTablePath: string, writeFileds: string[]): void {
    print(`当前需要写入：${dbSourceStateInfos.length} 条数据表状态信息`);
    let dataPackage: CommonTableSubmitDataPackage = {};
    dataPackage = {
        addRows: [{
            fieldNames: writeFileds,
            rows: dbSourceStateInfos
        }]
    }
    updateDwTableData(dataRecordTablePath, dataPackage);
    print("将校验的数据源状态记录到指定表中-执行结束");
}


function emerger(): void {
	 let dataPackage: CommonTableSubmitDataPackage = {};
        dataPackage = {
            mergeIntoRows: [{
                fieldNames: ["ID", "SCHEDULE_ID", "RESID", "TYPE", "ENABLE"],
                rows: [[ // 脚本ID设置为任务ID, 方便更新计划任务
                    scriptId, scheduleId, scriptId, "script", Number(autoBackupEnable)
                ]]
            }]
        }
	dw.updateDwTableData("/sysdata/data/tables/sys/SCHEDULETASK.tbl", dataPackage);
}


const DwDataValidateFailHandleEnum = com.succez.dw.data.submit.DwDataValidateFailHandleEnum;

/**
 * 将处理好的excel数据写入指定模型  https://jira.succez.com/browse/BI-47751
 * @param data 写入的数据
 */
private writeDataToModel(data: FieldValue[][]): void {
    let dataPackage: CommonTableSubmitDataPackage = {};
    dataPackage = {
        addRows: [{
            fieldNames: [xxx],
            rows: data
        }],
        // @ts-ignore
        updateConfig: {
            /**
             * updateDwTableData方法走的是提交表单交互, 若校验失败则无法写入, 此处修改写入模式。
             * writeAll: 插入所有数据，如果模型上有设置检验状态和校验结果描述字段，则进行记录)
             */
            validateFail: DwDataValidateFailHandleEnum.values()[2],
            checkUpdateResult: "none" // 关闭更新行校验
        }
    }
    dw.updateDwTableData("xxx/xxx.tbl", dataPackage);
}