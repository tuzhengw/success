
import bean from "svr-api/bean";

/**
 * 2022-04-12 liuyz
 * https://jira.succez.com/browse/CSTM-18436
 * 因为产品获取所有物理表时，获取注释时单独进行了一次连接，这导致获取时速度很慢
 * 此处根据数据源列表界面获取物理表信息的实现来获取物理表信息
 * @param dataSource
 * @param schema
 * @param type
 */
function getAllTables(dataSource: string, schema: string, type: string[]) {
    let dsService = bean.getBean("com.succez.dw.services.impl.DwServiceDataSourcesServiceImpl");

    let result;
    try {
        result = dsService.queryDbTables("sdi", dataSource, schema);
    } catch(e) {
        print(e);
    }
    let tableInfos = [];
    for (let i = 0; i < result.length; i++) {
        let info = result[i];
        let tableType: string = info.type;
        if (type.includes(tableType.toLocaleLowerCase())) {
            tableInfos.push({
                name: info.name,
                comment: info.comment
            })
        }
    }
    return tableInfos;
}
