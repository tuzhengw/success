
import { MMP_Data } from "dw/dwmgr";
import { UrlInfo, assign } from "sys/sys";
import { MetaModulePageArgs, MetaFilesPageArgs } from "metadata/metamgr";
import { ToolbarArgs } from "commons/basic";

/**
 * 将产品原生数据模块界面嵌入到前台门户
 * export default：门户显示
 * export ：JS组件显示
 */
export class BDS_MMP_Data extends MMP_Data {

    /**
     * openPath 默认打开模型路径, eg: "/SJBS/data/tables/TJZB_KS/KSXXB.tbl"
     */
    constructor(args: MetaModulePageArgs) {
        args.projectName = "SJBS";
        super(args);

        let openPath: string = args.openPath;
        if (!openPath) {
            openPath = "/SJBS/data"
        }
        this.showUrl(assign({
            path: openPath.trim(),
        }));
    }

    public _init(args: MetaFilesPageArgs): HTMLElement {
        let domBase = super._init(args);
        domBase.style.height = "100%";
        domBase.style.width = "100%";
        domBase.style.background = "#fff";
        domBase.classList.add("bds-dataMgr");
        return domBase;
    }

    protected _getToolbarArguments(): JSONObject {
        let json = <ToolbarArgs>super._getToolbarArguments();
        json.items && (json.items = json.items.filter(item => item.id != "switchview"));
        return json;
    }

    /**
     * 路由到到某个模块页面去
     */
    public navigate(urlInfo: UrlInfo): Promise<any> {
        return this.showUrl(urlInfo).then(() => {
            return this;
        });
    }

    public getTitle(): string {
        return '公共数据';
    }
}