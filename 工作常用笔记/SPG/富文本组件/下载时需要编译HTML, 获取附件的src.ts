/**
 * ============================================================
 * 作者: tuzw
 * 创建日期：2022-12-22
 * 功能描述：该脚本主要是PC端笔记页面管理
 * ============================================================
 */
import { ctx, rc, isEmpty, showWarningMessage, downloadFile } from "sys/sys";
import { InterActionEvent } from "metadata/metadata-script-api";
import { ListMultipageCheckedRows, getAppPath } from "../commons/commons.js?v=20220606712142148";
import {
    url2pdf, ExportReusltType, ExportSizeOptionsInfo, createExportPDFIFrame, PdfBuilder, ExportTextInfo, ExportTextCompInfo, ExportCompType,
    html2pdf
} from "commons/export";
import { MetaFileViewer, showDrillPage, SuccRichtextRender } from "metadata/metadata";

/**
 * 笔记文件管理
 */
export class SpgNoteFileMgr {

    public CustomActions: any;

    constructor() {
        this.CustomActions = {
            button_editSuccRichtextToSpg: this.editSuccRichtextToSpg.bind(this),
            button_batchImportNoteFiles: this.batchImportNoteFiles.bind(this)
        };
    }


    /**
     * 编辑富文本的HTML内容, 并跳转到指定SPG展示 
     * 
     * @param noteFileName 笔记文件名
     * @param noteHtmlContent 笔记HTML字符串内容
     * 
     * <p>
     *  1 编译HTML内容后, 让其HTML的附件可获取到对应的src值
     *  <img width="279" height="220" 
     *       data-file-id="33e23337c1fda85a4ea185ab4e274e1d2fed" // 富文本对应的HTML值仅记录附件ID
     *       src="/api/dw/services/d.......mp;queryId=model1&amp;fileField=ATTACHMENT_INFO"> // 编译后可获取src值, 用于请求附件文件
     *  
     *  2 富文本控件可直接存储图片, 不勾选将图片作为附件存储, 故此步可省略
     * </p>
     */
    private editSuccRichtextToSpg(event: InterActionEvent): Promise<any> | void {
        let page = event.page;
        let params = event.params;
        let noteHtmlContent: string = params?.noteHtmlContent;
        if (isEmpty(noteHtmlContent)) {
            showWarningMessage(`文件内容为空, 无法下载`);
            return;
        }
        let noteFileName: string = params?.noteFileName;

        let fileInfo = page.getFileInfo();
        let spgResid: string = fileInfo.id;

        let deserializeHtml = SuccRichtextRender.deserializeHtml({ // 编译HTML, 获取附件的src值
            html: noteHtmlContent,
            constraintId: "model1",
            fileField: "ATTACHMENT_INFO",
            referredResId: spgResid
        });
        let domFileViewer = document.getElementsByClassName('metafileviewer-base');
        let fileViewer: MetaFileViewer = domFileViewer[1].szobject as MetaFileViewer;
        return fileViewer.showDrillPage({ // 用元数据对象去打开SPG, 才会显示面包屑路径
            url: {
                path: getAppPath(`/notePcMgr/noteFileShow.spg`),
                params: {
                    noteFileContent: deserializeHtml,
                    noteFileName: noteFileName
                }
            },
            title: 'note笔记查看',
            breadcrumb: true,
            target: ActionDisplayType.Container,
        });
    }
	
	
	/**
     * 导出富文本笔记(PDF)
     * 注意：直接导出DOM，若HTML格式混乱，则导出的内容也是混乱的
     * 
     * @param importFileName 导出文件名
     * 
     * <p>
     *  1 富文本组件所存的HTML内容中的附件仅记录附件ID, 加载HTML时会对HTML进行编译, 根据附件ID生成超链接请求附件, 直接导出的HTML是不会加载附件图片
     *  2 将HTML内容进行编译, 让其附件DOM都记录附件src值, 并将src值存入到库中, 然后页面借助HTML组件显示编译后的HTML内容（可展示附件）
     *  3 调用html2pdf方法, 导出HTML组件DOM内容(导出PDF可展示附件图片)
     * </p>
     */
    private downLoadSingalNoteFile(event: InterActionEvent): void {
        let page = event.page;
        let params = event.params;
        let importFileName: string = params?.importFileName;
        if (isEmpty(importFileName)) {
            importFileName = "note笔记";
        }
        let fileInfo = page.getFileInfo();
        let spgResid: string = fileInfo.id;
        let htmlDom = page.getComponent("html1");
        let domBase = htmlDom.component.domBase;
        html2pdf(spgResid, domBase, importFileName, ExportReusltType.DOWNLOAD);
    }
}
