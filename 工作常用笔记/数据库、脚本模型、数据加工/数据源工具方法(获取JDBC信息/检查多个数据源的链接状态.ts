/**
 * ============================================================
 * 作者: tuzhengw
 * 审核人员：liuyongz
 * 创建日期：20220610
 * 项目：江苏数据中台
 * 功能描述：
 *      数据归集监控界面，需要监控数据源的链接状态，但是产品不会主动刷新改状态，因此需要写一个处理脚本，能定时检测一下，
 * 检测的结果写入到系统的数据源状态表中， 暂时可以设置为每天运行一次。
 * 
 * 帖子地址：https://jira.succez.com/browse/CSTM-19128?page=com.atlassian.jira.plugin.system.issuetabpanels%3Aall-tabpanel
 * ============================================================
 */
function main() {
    checkDbSourceStates();
}

import { getString } from 'svr-api/metadata';
import { getDataSources, getDataSource, getDefaultDataSource } from 'svr-api/db';
import { updateDwTableData } from "svr-api/dw";

const Timestamp = Java.type('java.sql.Timestamp');

/** 数据中台数据源状态表监控表 */
const SDI_MONITOR_DATASOURCESTATE = "SDI_MONITOR_DATASOURCESTATE";
/**
 * 检查当前项目所有数据源状态
 */
export function checkDbSourceStates(): void {
    print(`开始——检查当前环境所有数据源连接状态`);
    let startTime: number = Date.now();
    let dbSourceNames: string[] = getDataSources();
    print(`当前环境共有：${dbSourceNames.length} 个数据源 `);
    let dbSourceStateInfos: SDI_MONITOR_DATASOURCESTATE[] = [];
    for (let i = 0; i < dbSourceNames.length; i++) {
        let dbSourceName: string = dbSourceNames[i];
        print(`开始校验数据源：${dbSourceName} 连接状态，还剩：${dbSourceNames.length - i - 1} 个数据源待校验`);
        let checkStateInfo = checkDbState(dbSourceName);
        if (!checkStateInfo) {
            continue;
        }
        let dbSourceJdbcInfos: DbConnectionInfo = getDbSourceJdbcInfos([dbSourceName])[0];
        if (!dbSourceJdbcInfos) {
            continue;
        }
        let stateInfo: SDI_MONITOR_DATASOURCESTATE = {
            DATASOURCE_NAME: dbSourceName,
            DBTYPE: dbSourceJdbcInfos.dbType,
            HOST: dbSourceJdbcInfos.host,
            URL: dbSourceJdbcInfos.url,
            DB_USER: dbSourceJdbcInfos.user,
            MAXPOOLSIZE: !dbSourceJdbcInfos.poolSize ? 0 : dbSourceJdbcInfos.poolSize,
            DBVERSION: checkStateInfo.dataSourceVersion,
            JDBCVERSION: checkStateInfo.jdbcVersion,
            STATE: checkStateInfo.dbSourceConnState ? DATA_SOURCE_STATE.WORKING : DATA_SOURCE_STATE.FALL,
            CHECK_LOG: checkStateInfo.errorLog,
            CREATE_TIME: new Timestamp(new Date().getTime())
        };
        dbSourceStateInfos.push(stateInfo);
    }
    insertDbSourceStateInfoToDbTable(dbSourceStateInfos, SDI_MONITOR_DATASOURCESTATE);
    print(`结束——检查当前环境所有数据源连接状态完成，共耗时：${(Date.now() - startTime) / 1000} s`);
}

/** 产品记录数据源状态路径 */
const DATASOURCESTATE_PATH = "/sysdata/data/tables/sys/DATASOURCESTATE.tbl";
/** 产品记录数据源状态表字段 */
const DATASOURCESTATE_FIELD = ["DATASOURCE_NAME", "DBTYPE", "HOST", "URL", "USER", "MAXPOOLSIZE", "DBVERSION", "JDBCVERSION", "STATE", "LOG", "LAST_UPDATE_TIME"];
/**
 * 校验【指定数据源】是否链接正常
 * @param dbSourceNames 校验的数据源名数组
 * 
 * 20220623 tuzw
 * 问题：由于产品校验数据源状态链接是否正常，并将结果写入到：数据源状态表（DATASOURCESTATE）的触发条件：数据源从正常-失败，失败-正常，触发条件不灵活
 * 改进：脚本去校验当前数据源状态，并将其结果写入到数据源状态表
 */
export function checkDesignDbSourceState(dbSourceNames: string[]): void {
    if (dbSourceNames.length == 0) {
        print(`当前需要校验的数据源为空，结束执行`);
        return;
    }
    let startTime: number = Date.now();
    print(`当前需要校验：${dbSourceNames.length} 个数据源 `);
    let dbSourceStateInfos: string[][] = [];
    for (let i = 0; i < dbSourceNames.length; i++) {
        let dbSourceName: string = dbSourceNames[i];
        print(`开始校验数据源：${dbSourceName} 连接状态，还剩：${dbSourceNames.length - i - 1} 个数据源待校验`);
        let checkStateInfo = checkDbState(dbSourceName);
        if (!checkStateInfo) {
            continue;
        }
        let dbSourceJdbcInfos: DbConnectionInfo = getDbSourceJdbcInfos([dbSourceName])[0];
        if (!dbSourceJdbcInfos) {
            continue;
        }
        let stateInfo: string[] = [
            dbSourceName,
            dbSourceJdbcInfos.dbType as string,
            dbSourceJdbcInfos.host as string,
            dbSourceJdbcInfos.url as string,
            dbSourceJdbcInfos.user as string,
            !dbSourceJdbcInfos.poolSize ? 0 : dbSourceJdbcInfos.poolSize,
            checkStateInfo.dataSourceVersion as string,
            checkStateInfo.jdbcVersion as string,
            checkStateInfo.dbSourceConnState ? DATA_SOURCE_STATE.WORKING : DATA_SOURCE_STATE.FALL,
            checkStateInfo.errorLog,
            new Timestamp(new Date().getTime())
        ]
        dbSourceStateInfos.push(stateInfo);
    }
    insertDbSourceStateInfoToModel(dbSourceStateInfos, DATASOURCESTATE_PATH, DATASOURCESTATE_FIELD);
    print(`结束——校验数据源连接状态完成，共耗时：${(Date.now() - startTime) / 1000} s`);
}

/**
 * 将校验的数据源状态记录到模型
 * @param dataSourceStateInfos 写入的数据源状态信息数组
 * @param dataRecordTableName 状态信息数据记录表路径
 * 
 * 20220623 tuzw
 * 考虑到产品创建的数据库状态表部分字段Oracle关键字重名，不使用Db的insert模块，改为dw模块
 */
function insertDbSourceStateInfoToModel(dbSourceStateInfos: string[][], dataRecordTablePath: string, writeFileds: string[]): void {
    print(`当前需要写入：${dbSourceStateInfos.length} 条数据表状态信息`);
    let dataPackage: CommonTableSubmitDataPackage = {};
    dataPackage = {
        addRows: [{
            fieldNames: writeFileds,
            rows: dbSourceStateInfos
        }]
    }
    updateDwTableData(dataRecordTablePath, dataPackage);
    print("将校验的数据源状态记录到指定表中-执行结束");
}

/**
 * 校验当前数据源状态是否连接成功
 * @param dataSourceName 数据源名
 * @return 
 */
function checkDbState(dataSourceName: string): CHECK_RESULT_INFO | void {
    if (!dataSourceName) {
        print(`校验当前数据源状态是否有效，参数为空`);
        return;
    }
    let checkResultInfo: CHECK_RESULT_INFO = { dbSourceConnState: true };
    let dataSource = getDataSource(dataSourceName);
    if (!dataSource) {
        return { dbSourceConnState: false, errorLog: `当前环境不存在数据源：${dataSourceName}` };
    }
    let conn;
    try {
        conn = dataSource.getConnection();
        let metaData = conn.getMetaData();
        checkResultInfo.jdbcVersion = metaData.getDriverVersion();
        checkResultInfo.dataSourceVersion = metaData.getDatabaseProductVersion();
        print(`数据源：${dataSourceName} 连接状态：${DATA_SOURCE_STATE.WORKING}`);
    } catch (e) {
        checkResultInfo.dbSourceConnState = false;
        checkResultInfo.errorLog = e.toString();
        print(`数据源：${dataSourceName} 连接状态：${DATA_SOURCE_STATE.FALL}`);
    } finally {
        conn && conn.close();
    }
    return checkResultInfo;
}

/**
 * 获取指定的数据源JDBC信息
 * @param dbSourceNames 数据源名数组
 * 注意：由于getJdbc方法只能获取链接成功的数据源信息，这里改为获取数据源元数据信息
 * @return 
 */
function getDbSourceJdbcInfos(dbSourceNames: string[]): DbConnectionInfo[] {
    if (dbSourceNames.length == 0) {
        print(`getDbSourceJdbcInfos()，参数为空`);
        return [];
    }
    print("开始获取以下的数据源JDBC信息");
    print(dbSourceNames);
    let dbSourceInfo: DbConnectionInfo[] = [];
    let dbMetadataRootPath: string = "/sysdata/data/sources";
    for (let i = 0; i < dbSourceNames.length; i++) {
        let dbSourcePath: string = `${dbMetadataRootPath}/${dbSourceNames[i]}.jdbc`;
        let metadataFileContent: string = getString(dbSourcePath);
        if (!!metadataFileContent) {
            try {
                dbSourceInfo.push(JSON.parse(metadataFileContent));
            } catch (e) {
                print(`获取数据源 ${dbSourceNames[i]} JDBC信息失败`);
                print(e.toString());
            }
        }
    }
    print("获取指定的数据源JDBC信息完成");
    return dbSourceInfo;
}

/** 
 * 将校验的数据源状态记录到数据库表中
 * @param dataSourceStateInfos 写入的数据源状态信息数组
 * @param dataRecordTableName 状态信息数据记录表名
 */
function insertDbSourceStateInfoToDbTable(dbSourceStateInfos: SDI_MONITOR_DATASOURCESTATE[], dataRecordTableName: string): void {
    if (dbSourceStateInfos.length == 0) {
        print("当前没有数据源信息，不执行写入操作");
        return;
    }
    let defaultDb: Datasource = getDefaultDataSource();
    let defaultSchema: string = defaultDb.getDefaultSchema();
    let sdiDataSourceTable = defaultDb.openTableData(dataRecordTableName, defaultSchema);
    try {
        let successInsertNums: number = sdiDataSourceTable.insert(dbSourceStateInfos);
        print(`insertDbSourceStateInfos()，成功写入：${successInsertNums} 条数据源状态信息`);
    } catch (e) {
        print(`insertDbSourceStateInfos()，将校验的数据源状态记录到表中失败`);
        print(e);
    }
}

/** 检查结果信息 */
interface CHECK_RESULT_INFO {
    /** 数据库连接状态 */
    dbSourceConnState?: boolean;
    /** JDBC版本 */
    jdbcVersion?: string;
    /** 数据库版本 */
    dataSourceVersion?: string;
    /** 连接失败错误日志 */
    errorLog?: string;
}

/** 数据源状态对象 */
interface SDI_MONITOR_DATASOURCESTATE {
    /** 数据源名称 */
    DATASOURCE_NAME?: string;
    /** 数据库类型 */
    DBTYPE?: string;
    /** 数据库服务IP */
    HOST?: string;
    /** 数据库URL */
    URL?: string;
    /** 数据库用户名 */
    USER?: string;
    /** 数据库用户名 */
    DB_USER?: string;
    /** 连接池最大连接数 */
    MAXPOOLSIZE?: number;
    /** 数据库版本 */
    DBVERSION?: string;
    /** Jdbc驱动版本 */
    JDBCVERSION?: string;
    /** 状态 */
    STATE?: string;
    /** 日志 */
    LOG?: string;
    /** 日志 */
    CHECK_LOG?: string;
    /** 最后修改时间 */
    LAST_UPDATE_TIME?: number | any;
    /** 创建时间 */
    CREATE_TIME?: number | any;
}

/** 数据源状态 */
enum DATA_SOURCE_STATE {
    /** 失败 */
    FALL = "FAIL",
    /** 连接中 */
    WORKING = "WORKING"
}