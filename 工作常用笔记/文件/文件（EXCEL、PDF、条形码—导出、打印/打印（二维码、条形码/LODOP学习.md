# `LODOP`

> 学习网址：https://www.cnblogs.com/shaobog/p/11649169.html

```ts
// 见LodopFuncs.ts文件
let LODOP = getLodop();
```

# 0 功能函数

​		`Lodop`的功能函数不多，但参数比较复杂。全部函数分“基本函数”和“扩展函数”两类，两类函数有类似性，基本函数使用简单，达不到要求时请使用扩展函数，二者无本质区别。

---

1

| 函数                                                         | 作用             |
| :----------------------------------------------------------- | ---------------- |
| `PRINT_INIT(strTaskName)`                                    | 初始化运行环境   |
| `SET_PRINT_PAGESIZE(intOrient, PageWidth, PageHeight, strPageName)` | 设定纸张大小     |
| `ADD_PRINT_HTM(Top,Left,Width,Height,strHtmlContent)`        | 增加超文本打印项 |
| `ADD_PRINT_TABLE(Top, Left, Width, Height, strHtml)`         | 增加表格打印项   |
| `ADD_PRINT_URL(Top,Left,Width,Height,strURL)`                | 打印URL          |
| `ADD_PRINT_TEXT(Top,Left,Width,Height,strContent)`           | 增加纯文本打印项 |
| `ADD_PRINT_IMAGE(Top,Left,Width,Height,strHtmlContent)`      | 增加图片打印项   |

2

| 函数                                                         | 作用           |
| ------------------------------------------------------------ | -------------- |
| `ADD_PRINT_RECT(Top, Left, Width, Height,intLineStyle, intLineWidth)` | 增加矩形线     |
| `ADD_PRINT_ELLIPSE(Top, Left,Width, Height, intLineStyle, intLineWidth)` | 增加椭圆线     |
| `ADD_PRINT_LINE(Top1,Left1, Top2, Left2,intLineStyle, intLineWidth)` | 增加直线       |
| `ADD_PRINT_BARCODE(Top, Left,Width, Height, CodeType, CodeValue)` | 增加条形码     |
| `ADD_PRINT_CHART(Top, Left,Width, Height, ChartType, strHtml)` | 增加图表       |
| `SET_PRINT_STYLE(strStyleName,varStyleValue)`                | 设置打印项风格 |

3

| 函数                  | 作用             |
| --------------------- | ---------------- |
| `NEWPAGE()`           | 强制分页         |
| `PREVIEW()`           | 打印预览         |
| `PRINT()`             | 直接打印         |
| `PRINT_SETUP()`       | 打印维护         |
| `PRINT_DESIGN ()`     | 打印设计         |
| `GET_PRINTER_COUNT()` | 获得打印设备个数 |
| `GET_PRINTER_NAME`    | 获得打印设备名称 |



# 1 基本函数

## 1.1 version-版本

​		名称：获得软件版本号

​		格式：VERSION

​		结果：返回字符型结果

​		版本号有四个数字组成，样式为：`X.X.X.X`

## 1.2  `print_init`-打印初始化

​		名称：<button>打印初始化</button>

​		功能：初始化运行环境，清理异常打印遗留的系统资源，设定打印任务名

​		结果：返回逻辑值

​		格式：`PRINT_INIT(strTaskName)`

---

> （1）`strTaskName`：打印任务名，字符型参数，由开发者自主设定，未限制长度，字符要求符合Windows文件起名规则，`Lodop`会根据该名记忆相关的打印设置、打印维护信息
>
> （2） 若`strTaskName`空，控件则不保存本地化信息，打印全部由页面程序控制

```ts
LODOP.PRINT_INIT("");
```

## 1.3 `set_print_pagesize`

​		名称：<button>设定纸张大小</button>

​		功能：设定打印纸张为固定纸张或自适应内容高，并设定相关大小值或纸张名及打印方向

​		格式：`SET_PRINT_PAGESIZE(intOrient, PageWidth, PageHeight, strPageName)`

---

>  （1）`intOrient`：打印方向及纸张类型，数字型
>
> ​        * 1：纵(正)向打印，固定纸张
>
> ​        * 2：横向打印，固定纸张
>
> ​        * 3：纵(正)向打印，宽度固定，高度按打印内容的高度自适应
>
> ​        \* 0（或其它）：打印方向由操作者自行选择或按打印机缺省设置
>
>  （2）`PageWidth`：设定自定义纸张宽度，整数或字符型
>
> ​          * 整数时缺省长度单位为0.1毫米
>
> ​          * 字符型时可包含单位名：in(英寸)、cm(厘米)、mm(毫米)、pt(磅)、px(1/96英寸)，如“10mm”表示10毫米
>
> （3）`PageHeight`：固定纸张时设定纸张高；高度自适应时设定纸张底边的空白高
>
> （4）`strPageName`：所选纸张类型名，字符型
>
> ​          * 注意：高小于等于0时`strPageName`才起作用
>
> ​          * 不同打印机所支持的纸张可能不一样，这里的名称同操作系统内打印机属性中的纸张名称，支持操作系统内的自定义纸张
>
> ​          * 关键字“`CreateCustomPage`”会按以上宽度和高度自动建立一个自定义纸张，所建立的纸张名固定为“`LodopCustomPage`”，多次建立则刷新该纸张的大小值

```ts
LODOP.SET_PRINT_PAGESIZE(2, width, height, "5mm");
```

## 1.4 `add_print_table`

​		名称：<button>增加表格打印项</button>（超文本模式）

​		功能：用超文本增加一个表格打印项，设定该表格在每个纸张内的位置和区域大小。打印时只输出首个页面元素table的显示内容，当table内包含`thead`或`tfoot`时，一旦表格被分页，则每个打印页都输出表头(`thead`)或表尾(`tfoot`)

​		格式：`ADD_PRINT_TABLE(Top, Left, Width, Height, strHtml)`

|   参数    | 作用                                                         |
| :-------: | ------------------------------------------------------------ |
|    top    | 表格数据头(页头`thead`)在纸张内的上边距，整数或字符型        |
|   left    | 表格数据头(页头`thead`)在纸张内的左边距，整数或字符型        |
|   width   | 打印区域的宽度，整数或字符型                                 |
|  height   | 表格数据体(`tbody`)区域的高度，整数或字符型                  |
| `strHtml` | 超文本代码内容，字符型，未限制长度。<br />可以是一个完整的页面超文本代码，或者是一个代码段落 |

> 特别说明：本函数能识别的超文本专有元素属性`有tdata、format、tclass、tindex`等四个，它们主要用来实现分页小计、分类合计等统计功能

## 1.6 `add_print_text`

​		名称：<button>增加纯文本打印项</button>

​		功能：增加纯文本打印项，设定该打印项在纸张内的<button>位置和区域大小</button>，文本内容在该区域内自动折行，当内容超出区域高度时，如果对象被设为“多页文档”则会自动分页继续打印，否则内容被截取

​		格式：`ADD_PRINT_TEXT(Top,Left,Width,Height,strContent)`

|     参数     | 作用                                 |
| :----------: | ------------------------------------ |
|     top      | 打印项在纸张内的上边距，整数或字符型 |
|     left     | 打印项在纸张内的左边距，整数或字符型 |
|    width     | 打印区域的宽度，整数或字符型         |
|    height    | 打印区域的高度，整数或字符型         |
| `setContent` | 纯文本内容，字符型，未限制长度       |

```ts
LODOP.ADD_PRINT_TEXT("70%", 0, "100%", "10%", data[1]);
```

## 1.7 `set_print_style`

​		名称：<button>设置打印项风格</button>

​		功能：设置打印项的输出风格，成功执行该函数，此后再增加的打印项按此风格输出。

​		格式：`SET_PRINT_STYLE(strStyleName,varStyleValue)`

```ts
// 第二个参数的值，取决于：第一个参数所选风格 
LODOP.SET_PRINT_STYLEA("Alignment", 2);
```

<button>`strStyleName`</button>：打印风格名，字符串

|   风格名称    | 含义                                 |
| :-----------: | ------------------------------------ |
|  `FontName`   | 设定纯文本打印项的：字体名称         |
|  `FontSize`   | 设定纯文本打印项的：字体大小         |
|  `FontColor`  | 设定纯文本打印项的：字体颜色         |
|    `Bold`     | 设定纯文本打印项是否粗体             |
|   `Italic`    | 设定纯文本打印项是否斜体             |
|  `Underline`  | 设定纯文本打印项是否下滑线           |
|  `Alignment`  | 设定纯文本打印项的内容左右靠齐方式   |
|     Angle     | 设定纯文本打印项的旋转角度           |
|  `ItemType`   | 设定打印项的基本属                   |
|   `HOrient`   | 设定打印项在纸张内的水平位置锁定方式 |
|   `VOrient`   | 设定打印项在纸张内的垂直位置锁定方式 |
|  `PenWidth`   | 线条宽度                             |
|  `PenStyle`   | 线条风格                             |
|    Stretch    | 图片截取缩放模式                     |
| `PreviewOnly` | 内容仅仅用来预览                     |
|  `ReadOnly`   | 纯文本内容在打印维护时，是否禁止修改 |

<button>`varStyleValue`</button>：打印风格值，<button>取决于：`strStyleName`所选风格</button>

```ts
LODOP.SET_PRINT_STYLEA("FontName", "黑体");
// 全局文字大小
LODOP.SET_PRINT_STYLEA("FontSize", 10pt);

LODOP.ADD_PRINT_TEXT("4%", 0, "100%", "10%", "鄂食安")
// 设置上一步输入的文字【鄂食安】大小
LODOP.SET_PRINT_STYLEA(0, "FontSize", 10pt);

LODOP.ADD_PRINT_TEXT("4%", 0, "100%", "10%", "第二个文字")
// 设置上一步输入的文字【第二个文字】大小
LODOP.SET_PRINT_STYLEA(0, "FontSize", 10pt);
```

|      风格值       |                                                              |
| :---------------: | ------------------------------------------------------------ |
|  `FontName的值`   | 字符型，与操作系统字体名一致，缺省是“宋体”                   |
|  `FontSize的值`   | 数值型，单位是pt，缺省值是9，可以含小数，如13.5              |
|  `FontColor的值`  | 整数或字符型，整数时是颜色的十进制`RGB`值<br />字符时是超文本颜色值，可以是“#”加三色，16进制值组合，也可以是英文颜色名 |
|    `Bold的值`     | 数字型，1：代表粗体，0：代表非粗体，缺省值是0                |
|   `Italic的值`    | 数字型，1：代表斜体，0：代表非斜体，缺省值是0                |
|  `Underline的值`  | 数字型，1：代表有下划线，0：代表无下划线，缺省值是0          |
|  `Alignment的值`  | 数字型，1：左靠齐；2：居中； 3：右靠齐，缺省值是1            |
|    `Angle的值`    | 数字型，逆时针旋转角度数，单位是度，0度表示不旋              |
|  `ItemType的值`   | 数字型，0：普通项；1：页眉页脚；2：页号项；3：页数项；4：多页项<br />缺省（不调用本函数时）值0<br />普通项只打印一次；页眉页脚项则每页都在固定位置重复打印；<br />页号项和页数项是特殊的页眉页脚项，其内容包含当前页号和全部页数；<br />多页项每页都打印，直到把内容打印完毕，打印时在每页上的位置和区域大小固定一样（多页项只对纯文本有效）<br/>在页号或页数对象的文本中，有两个特殊控制字符：“#”特指“页号”，“&”特指“页数” |
|   `HOrient的值`   | 数字型，0–左边距锁定 1–右边距锁定 2–水平方向居中 3–左边距和右边距同时锁定（中间拉伸），缺省值是0 |
|   `VOrient的值`   | 数字型，0–上边距锁定 1–下边距锁定 2–垂直方向居中 3–上边距和下边距同时锁定（中间拉伸），缺省值是0 |
|  `PenWidth的值`   | 整数型，单位是(打印)像素，缺省值是1，非实线的线条宽也是0     |
|  `PenStyle的值`   | 数字型，0–实线 1–破折线 2–点线 3–点划线 4–双点划线<br/>缺省值是0 |
|   `Stretch的值`   | 数字型，0–截取图片 1–扩展（可变形）缩放 2–按原图长和宽比例（不变形）缩放<br />缺省值是0 |
| `PreviewOnly的值` | 字符或数字型，1或“true”代表仅预览，否则为正常内容            |
|  `ReadOnly的值`   | 字符或数字型，1或“true”代表“是”，其它表示“否”，缺省值为“是”，即缺省情况下，纯文本内容在打印维护时是禁止修改的 |



## 1.8 `set_print_stylea`

​		名称：<button>（扩展型）设置打印项风格A</button>

​		功能：类似函数SET_PRINT_ STYLE的功能

​				  二者的区别是<button>本函数只对某打印项</button>有效

​		格式：`SET_PRINT_STYLEA(varItemNameID, strStyleName,varStyleValue)`

```ts
LODOP.SET_PRINT_STYLEA(0, "Alignment", 2);
```

| 参数            | 作用                                       |
| --------------- | ------------------------------------------ |
| `varItemNameID` | 要设置的目标项序号或项目名，数字型或字符型 |
| `strStyleName`  | 见：set_print_style                        |
| `varStyleValue` | 见：set_print_style                        |

<button>`varItemNameID`</button>

​	（1）数字型时：表示是序号，以其增加的先后自然顺序为准，从1开始，<button>所有打印对象都参与排序</button>，包括超文本、纯文本、图片、图线、图表、条码等

> 如果序号为0，代表当前（最后加入的那个）数据项；如果序号是负数，代表前面加入的数据项，该值为前移个数偏移量

​	（2）字符型时，是对象的类名或代表部分对象的关键字，关键字有如下几种：

​			*	Selected：代表在设计界面上用鼠标所选的所有对象；

​			*	`unSelected`：代表在设计界面上所有没有被鼠标选择的对象；

​			*	All：代表所有正常对象；

​			*	First：代表第一个正常对象；

​			*	Last：代表最后一个正常对象；

> 不是关键字的字符一般是类名，类名可以在`ADD_PRINT_TEXTA`
> 加入纯文本时用`strItemName`来声明，也可以在设计界面用菜单功能（Set TEXT Item Name）来设置。

----

## 1.9 `add_print_barcode`

​		名称：<button>增加条形码</button>

​		功能：增加条形码打印项，设定该条形码在纸张内的位置和大小，指定条形码的类型和条码值，控件在打印机上直接绘制条码图

​		格式：`ADD_PRINT_BARCODE(Top, Left,Width, Height, CodeType, CodeValue)`

> 字符型时可包含单位名：in(英寸)、cm(厘米)、mm(毫米)、pt(磅)、`px`(1/96英寸)、%(百分比)
>
> 如“`10mm`”表示10毫米

```ts
// "QRCode"：二维码
LODOP.ADD_PRINT_BARCODE("20%", "11%", "30%", "30%", "QRCode", url)
```

| 参数        | 作用                     |
| ----------- | ------------------------ |
| top         | 该条码图在纸张内的上边距 |
| left        | 该条码图在纸张内的左边距 |
| width       | 该条码图的总宽度         |
| height      | 该条码图的高度           |
| `cadeType`  | 条码类型，字符型         |
| `codeValue` | 条码值                   |

<button>`cadeType`</button>

目前支持的类型（条码规制）主要是一维条码，有如下几种：
`128A，128B，128C，EAN8，EAN13，EAN128A，EAN128B，EAN128C，Code39，
39Extended，2_5interleaved，2_5industrial，2_5matrix，UPC_A，UPC_E0，UPC_E1，UPCsupp2，UPCsupp5，Code93，93Extended，MSI，PostNet，Codaba，QRCode`

> <button>其中`QRCode`二维码，其它为一维码</button>
>
> 默认情况下`QRCode`的版本会根据宽度和高度自动调整，页面程序也可以直接设置具体版本（有1、3、7、14四个简约版本可选），版本固定时会按宽度和高度自动缩放条码大小

## 2.0 二维码扫描出乱码

更改其他编码试试：

```ts
LODOP.SET_PRINT_STYLEA(0,"DataCharset","UTF-8");
```

如图：打印设计——选中该条码打印项——右键属性——条码属性——下方修改编码



