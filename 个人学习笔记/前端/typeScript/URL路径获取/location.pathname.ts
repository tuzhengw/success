/**
 * 20220218 tuzw
 * 问题来源：江苏数据中台
 
 * 技术点：location.pathname 获取当前URL地址（不包含：?后面的部分）
 
 * 原逻辑：if (location.pathname.endsWith(keys) && frame.headbar) {...
 * 校验时通过读取当前URL路径，使用endsWith()与配置文件中的属性进行匹配，来动态创建配置文件中指定下拉框菜单，eg：
 * 		url：http://172.23.126.151:8080/zt/data-govern/index.tpg?id=校验规则
 * 		keys：index_structure.tpg
 * 		location.pathname：http://172.23.126.151:8080/zt/data-govern/index_structure.tpg
 * 故：原逻辑的结果为：true，可以成功创建下拉框菜单
 * 
 * 问题：若通过【短路径】访问，则location.pathname获取的值为："/"，eg：
 * 		url：http://172.23.126.151:8080/?id=归集看板
 * 		keys：index_structure.tpg
 * 		location.pathname：/
 * 原逻辑为：false，则不会创建下拉框菜单
 * 
 * 改进：根据门户的元数据信息来判断
 */