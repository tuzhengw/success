/**
 * 20221221 zhangd
 * 将产品原生数据源管理界面嵌入到前台门户
 */

import { MetaFilesPageArgs } from "metadata/metamgr";
import { MMP_Datasources } from "datasources/datasourcesmgr";
import { DEFAULT_DATASOURCE, getDataSourceManager, SYS_PROJECT_NAME } from "datasources/datasourcesdata";
import { formatDateFriendly, isEmpty, rc } from "sys/sys";
import { DataProvider } from "commons/basic";
import { getMetaRepository } from "metadata/metadata";
import { PROJECT_NAME_CORE } from '../sys';

/**
 * 数据源管理页面
 */
export default class BDS_MMP_Datasources extends MMP_Datasources {

    domBase: HTMLElement;

    /** 数据源缓存 */
    availableDatasources: string[];

    public _init(args: MetaFilesPageArgs): HTMLElement {
        let domBase = super._init(args);
        domBase.style.height = "100%";
        domBase.style.background = "#fff";
        return domBase;
    }

    public getTitle(): string {
        return '数据源';
    }


    /**
     * 刷新数据源列表
     */
    public refresh(): Promise<any> {
        return getDataSourceManager().fetchDataSources(SYS_PROJECT_NAME).then(infos => {
            if (!this.domBase) {//测试用例中，会出现已经销毁，仍然执行到这里的情况
                return;
            }
            if (isEmpty(infos)) {
                infos = [];
            }
            infos = infos.filter(info => info?.datasource !== 'default');
            let availableDatasources: string[] = [];
            let rows: JSONObject[] = [];
            infos.forEach((info, i) => {
                let row: JSONObject = {};
                row.number = ++i;
                let datasource = row.name = info.datasource;
                datasource && availableDatasources.push(datasource);
                let databaseName = info.databaseName || info.sid;
                /**
                 * 数据源的“连接到”是 ip:port/databaseName 拼的
                 * 默认数据源配置里可能没有ip，port，databaseName这些参数，所以使用的是url。
                 * 这里可以尽量保持一致，如果默认数据源没有这些参数就用url解析；可能url中也解析不出，那就还是展示url
                 * 
                 * 配置不正确的的数据源也可能没有ip，port，databaseName这些参数
                 */
                if (info.host && info.port && databaseName) {
                    row.connectTo = info.host + ":" + info.port + "/" + databaseName;
                }
                else {
                    row.connectTo = info.url || "";
                }
                row.operate = datasource === DEFAULT_DATASOURCE ? this['getOpeHtml'](["edit", "refresh"]) : this['getOpeHtml'](["edit", "delete", "refresh"]);
                if (info.type) {
                    //type可能不存在，比如高版本支持的数据库，低版本启动就没有。
                    row.icon = "icon-ds-" + info.type.toString().toLowerCase();
                }
                row.decorateIcon = info.state === DataSourceState.FAIL ? "icon-decorate-unableconnect" : "";
                row.desc = info.desc;
                row.type = info.type;
                row.user = info.user;
                let createTime = info.createTime;
                if (createTime) {
                    row.createTime = formatDateFriendly(new Date(createTime));
                }
                row.creator = info.creator;
                rows.push(row);
            });
            return this['grid'].setDataProvider(new DataProvider({
                data: rows
            })).then(() => {
                if (isEmpty(this.availableDatasources)) {
                    this.availableDatasources = availableDatasources;
                    return;
                }
                if (this.availableDatasources.length === availableDatasources.length
                    && this.availableDatasources.every(ds => availableDatasources.indexOf(ds) !== -1)) {
                    return;
                }
                return getMetaRepository().getPublicProjectSettings(PROJECT_NAME_CORE, 'data').then(info => {
                    this.availableDatasources = availableDatasources;
                    info['availableDatasources'] = availableDatasources;
                    this.saveSettings('data', info);
                });
            });

        });
    }


    /**
     * 保存系统设置
     * @param module
     * @param data
     */
    public saveSettings(module: string, settings: JSONObject): Promise<void> {
        let data: JSONObject = {};
        data[module] = settings;
        return rc({
            url: `/api/meta/project/modifyProjectConfiguration`,
            data: {
                projectName: PROJECT_NAME_CORE,
                settings: data
            }
        }).then(() => {
            getMetaRepository().refreshProjectModuleSettings(this.projectName, module, settings);
        });
    }
}
