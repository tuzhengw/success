    /**
	 * 格式化类型jpg
	 */
	public final static String	FORMAT_NAME		= "jpg";
 
	/**
	 * 网络图片下载
	 * @param imageUrl 图片url
	 * @param formatName 文件格式名称
	 * @param localFile 下载到本地文件
	 * @return 下载是否成功
	 */
	public boolean downloadImage(String imageUrl, String formatName, File localFile) {
		boolean isSuccess = false;
		URL url = null;
		try {
			url = new URL(imageUrl);
			isSuccess = ImageIO.write(ImageIO.read(url), formatName, localFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isSuccess;
	}
 
 
	/**
	 * base64解密图片资源并解码上传
	 * @param imageBase64 base64编码的图片资源
	 * @param formatName 文件格式名称
	 * @param localFile 下载到本地文件
	 * @return 下载是否成功
	 */
	public boolean decodeBase64ImageUpload(String imageBase64, String formatName, File localFile) {
		boolean isSuccess = false;
		ByteArrayInputStream bis = null;
		byte[] imageByte;
		try {
			imageByte = Base64.getDecoder().decode(imageBase64);
			bis = new ByteArrayInputStream(imageByte);
			isSuccess = ImageIO.write(ImageIO.read(bis), formatName, localFile);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if(bis!=null)
					bis.close();
			}catch (Exception e){
				log.info("decode base64  bis close fail！！！ excetion{}",e);
			}
		}
		return isSuccess;
	}
 
 
	/**
	 * base64转化成 inputStream
	 *
	 * @param base64
	 * @return
	 */
	public static InputStream base64ToInputStream(String base64) {
		ByteArrayInputStream stream = null;
		try {
			byte[] bytes = Base64.getDecoder().decode(base64);
			stream = new ByteArrayInputStream(bytes);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return stream;
	}