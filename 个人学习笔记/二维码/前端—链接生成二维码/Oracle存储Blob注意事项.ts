/**
 * 更新出仓管理-备案号二维码内容
 * 注意：
 * 1. Blob类型不能直接更新
 * （1）MySQL 可直接更新
 * （2）Oracle 要先取出来再写入
 * @param warehouseOutInfos 包含二维码内容的出仓信息数组
 */
function updateWarehouseOutQrcode(warehouseOutInfos: DW_JGC_WAREHOUSINGOUT[]): void {
    if (warehouseOutInfos == null || warehouseOutInfos.length == 0) {
        console.debug(`updateWarehouseOutQrcode()，更新出仓管理-备案号二维码内容，更新内容：NULL`);
        return;
    }
    let dataSource = getDataSource(DATA_BASE_NAME);
    let conn = dataSource.getConnection();
    let preparestatement = null;
    try {
        print(warehouseOutInfos);
        conn.setAutoCommit(false);
        for (let i = 0; i < 1; i++) {
            let updateSQL: string = `update ${DW_JGC_WAREHOUSINGOUT} set QRCODEPATH=?,QRCODECREATETIME=? where ID=? and OUTBOUNDBATCH=?`; // 锁行
            preparestatement = conn.prepareStatement(updateSQL);
			
            let bytesBlob = conn.createBlob();
            preparestatement.setBlob(1, bytesBlob);
            preparestatement.setBytes(1, warehouseOutInfos[i].QRCODEPATH);
			
            preparestatement.setTimestamp(2, new java.sql.Timestamp(new Date().getTime()));
			
            preparestatement.setString(3, warehouseOutInfos[i].ID);
            preparestatement.setString(4, warehouseOutInfos[i].OUTBOUNDBATCH);
            preparestatement.addBatch();
        }
        let successNums = preparestatement.executeBatch();
        print(`成功执行的条数为：${successNums.length}`)
        conn.commit();
    } catch (e) {
        console.error(`更新出仓管理-备案号二维码内容：error`);
        console.error(e);
    } finally {
        if (preparestatement != null) {
            preparestatement.clearBatch();
            preparestatement.close();
        }
        conn.close();
    }
}