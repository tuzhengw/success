---
typora-root-url: images
---

# succIDE

# 1 安装

​	打开`VSCode`，在插件应用商店搜索`SuccIDE`，点击`Install`安装。

# 2 界面和初配置

## 1 界面介绍

![](../images/页面初始化.png)

界面主要包括如下功能板块：

（1）**资源区**：用于存放脚本、数据表、元数据等类型文件，右键菜单提供创建扩展、与服务器同步等功能

（2）**服务区**：用于展示服务器上的扩展、项目、数据源等，工具栏提供新增服务器、切换服务器、刷新服务器等功能，右侧菜单提供下载功能

（3）**同步区**：用于展示同步比较后的文件差异树，工具栏提供实时同步、刷新等功能，右侧菜单则提供上传、下载功能

（4）**比较区**：用于展示单个文件服务器与本地的内容差异，可手工编辑合并内容

（5）**日志区**：用于显示插件操作过程中的日志

## 2 初始化

​	安装好插件后还不能直接使用，需初始化开发环境，只需要初始化一次，可执行命令`Init IDE`或使用快捷键`Ctrl + F1 (win)`、`Command + F1 (mac)`进行初始化。

​	<button>初始化方法：`ctrl + shift + p`， 搜索并选中：`succIDE初始化`</button>

> **若安装后，初始化无效，检查：是否禁止`succIDE`插件**

​	初始化后的会生成如下目录结构，并出现两个新的视图：同步区和服务区。

-----

- .`vscode`

  目录，存放系统文件

  - `backup` 目录，存放了同步的备份文件，以压缩包形式存在
  - `logs` 目录，存放插件的日志文件
  - `settings.json` 文件，工作区`VSCode`设置文件

- `api` 目录，存放了bi系统提供的二次开发接口

- `datasources` 目录，存放数据表数据，以`csv`形式存在

- `extensions` 目录，存放了开发的扩展

- `projects` 目录，存放下载的项目

- `.gitignore` 文件，git忽略的配置文件

- `package.json` 文件，包含项目的相关配置，见[插件配置](https://docs.succbi.com/dev/ide/ui/#插件配置)

- `tsconfig.json` 文件，ts编译的配置文件

---

**注意：**

- 在使用此命令前，你需要用`vscode`打开想要进行开发工作的工程目录，应该是一个<button>空目录</button>，在这个目录下执行初始化。
- 初始化命令只需执行一次，下次启动可直接使用，不用再次执行。
- 个人开发时，这个命令是你在`vscode`中安装好插件后，开始使用插件功能时执行的第一个命令，不执行将无法使用插件其他功能。
- 团队开发时，应该指定一个人使用本命令后，将对应代码提交到git，其他人无需执行本命令，直接fetch代码后，在对应目录进行开发。

## 3 设置

> `SuccIDE`提供：自动备份、自动编译、自动格式化功能，默认均开启，可根据场景自行选择开启与关闭。

![](../images/设置.png)

---

# 3 同步测试环境和开发环境

## 1 概念

​	通常项目开发中会涉及到多个环境，一般至少有两个环境：<button>测试环境</button>、<button>生产环境</button>。

​	测试环境用于：<button>开发新功能</button>，然后**周期性**将测试环境开发的功能发布至生产环境，这就涉及到多个环境间元数据的同步与迁移，常用的方案有以下三种：

- **迁移数据库表**：可用数据库工具或者`SuccBI`的备份恢复，好处是批量文件恢复快，缺点：全部覆盖。适用于环境初始迁移或很久未用环境的同步。
- **导入导出**：使用`SuccBI`的元数据导入导出功能，好处是只同步需要的文件，操作简便，缺点：全部覆盖，无法比对差异，再者对于需要同步的文件若分布很零散，操作不便。适用于集中式新增功能模块迁移。
- `SuccIDE`**同步**：使用`SuccIDE`的同步功能，好处是可查看文件的差异，可选择需要的文件进行同步，缺点是依赖于的`vscode`使用，有一定难度，另外差异若是太多，同步比较耗时会很长。适用于定期同步升级的场景。

## 2 总体流程

![](../images/连接流程.png)

## 3 同步服务器

![](../images/开始同步.gif)

### 3.1 更改`package.json`

​	插件的配置信息都在工作目录下的`package.json`文件中，以下内容由`SuccIDE`操作时<button>自动产生</button>，用户也可以进行<button>手动修改</button>，该文件的结构如下：

```json
{
	"project": "SuccIDE",
	"dependencies": {
		"@types/d3": "^5.7.2",
		"@types/jquery": "^3.3.30",
		"@types/leaflet": "^1.5.1",
		"@types/mapbox-gl": "^0.53.0",
		"@types/marked": "^0.7.0",
		"@types/three": "^0.92.2",
		"@types/vis": "^4.21.18",
		"d3": "^5.9.7",
		"echarts": "^4.2.1",
		"echarts-wordcloud": "^1.1.3",
		"hammerjs": "^2.0.8",
		"html2canvas": "^1.0.0-rc.3",
		"jquery": "^3.4.1",
		"keycharm": "^0.3.1",
		"leaflet": "^1.5.1",
		"three": "^0.92.0",
		"mapbox-gl": "^1.2.0",
		"marked": "^0.7.0",
		"quill": "^1.3.6",
		"vis": "^4.21.0",
		"vis-data": "^6.2.2",
		"vis-network": "^6.4.0",
		"vis-timeline": "^6.2.0",
		"vue": "^2.6.10",
		"xlsx": "^0.15.0"
	},
	"serverConfigs": [
		{
			"serverName": "http://localhost:8081",
			"serverUrl": "http://localhost:8081"
		},
		{
			"serverName": "http://localhost:8081/",
			"serverUrl": "http://localhost:8081"
		}
	],
	"syncConfig": {
		"currentServer": "http://localhost:8081",
		"currentDevelopment": null
	},
	// 同步忽略文件，用于配置忽略不需要同步的文件;文件格式：元数据路径即可
	"projects.include": [
		"/DEMO/ana",
		"/DEMO/data",
		"/DEMO/fapp",
		"/sysdata/app/default.app",
		"/sysdata/settings/themes"
	],
	"projects.exclude": [],
	// 数据源配置文件，用于配置哪些数据表需要下载数据内容
	// 文件格式：/{数据源名称}/{数据表名称}
	"datasources.include": [
		"/default/cn_adcode",
		"/succbi/countriesgeo",
		"/succbidw/country",
		"/succbiyw/dw_field_roles"
	]
}
```

### 3.2 输入服务器`URL`

![](../images/链接服务器.png)

```
http://localhost:8081
```

### 3.3 输入账户和密码

![](../images/输入用户名和密码.png)

## 4 下载项目

![](../images/下载文件.png)

# 4 扩展开发

​	开发环境准备好后，就可以使用`SuccIDE`创建一个新的扩展，具体步骤如下：

![](../images/新建扩展.gif)

# 5 可能遇到的问题

## 5.1 缺少同步配置

![](../images/根目录.png)

---

![](../images/缺少同步配置.png)

---

解决办法：

![](../images/选中指定的项目.png)

## 5.2 链接服务器的URL注意问题

![](../images/连接URL问题.png)

例如：

```
https://app.scwjxx.cn/DZPHZ/app/DZPHZ.app?:edit=true
```

```
https://app.scwjxx.cn/DZPHZ
```

**以上写法都是错误的**，必须要是首页，正确用法：

```
https://app.scwjxx.cn
```

# 