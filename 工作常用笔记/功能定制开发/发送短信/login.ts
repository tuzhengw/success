<!DOCTYPE html>
<html>

<head>
	<title>“旗舰领航”信息化服务平台</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge，chrome=1">
	<link rel="stylesheet" href="login.css?v=8" type="text/css">
	<script>
		//根据contextPath指定登录页
		var search = window.location.search;
		if(search.indexOf('redirect_url')<0){
			location.href='/zhjg/sysdata/public/login/qjlh/login.html?redirect_url=/';
		}
	</script>
</head>

<body>
	<noscript>
		<style>
			.page {
				display: block;
			}
		</style>
	</noscript>
	<div id="basepage" class="page">
		<div class="page-content">
			<div class="header">
				<div class="logo"></div>
				<div class="logo-caption"></div>
				<div class="logo-caption-phone">SuccBI</div>
			</div>
			<div class="content">
				<div class="login-content">
					<div class="login-box">
						<div class="login-box-header account-active  inner-active">
							<div class="login-title" i18n-key="login.page.title">用户登录</div>
							<div class="login-tab-header switch-login-type">
								<div class="header-wrapper">
									<li class="tab-item item-account" id="tab-account"
										i18n-key="login.page.passwordLogin">账号登录</li>
									<li class="tab-item item-qrcode" id="tab-qrcode" i18n-key="login.page.qrcodeLogin">
										扫码登录</li>
									<li class="tab-item item-sms" id="tab-sms" i18n-key="login.page.smsLogin">手机号登录</li>
								</div>
							</div>
							<div class="login-tab-header switch-login-user">
								<div class="header-wrapper">
									<li class="tab-item item-inner" id="inner" login.page.sysUser>专班人员</li>
									<li class="tab-item item-outer" id="outer" login.page.externalUser>企业用户</li>
								</div>
							</div>
						</div>
						<div class="login-box-body">
							<div class="login-account" id="login-account">
								<div class="login-status" id="status">
									<noscript>
										您的浏览器未启用 JavaScript，您需要启用 JavaScript 才能进行登录
										<br> your browser disabled JavaScript, you need to enable JavaScript before trying to login
									</noscript>
								</div>
								<div class="row-user">
									<input id="user" name="user" placeholder="用户名" type="text" autocapitalize="off" class="input-user" i18n-key="login.page.user" />
								</div>
								<div class="row-password">
									<input id="password" name="password" placeholder="请输入密码" type="password" class="input-password" i18n-key="login.page.password" />
								</div>
								<div class="row-captcha" id="captcha-container">
									<input id="captcha" name="captcha" placeholder="验证码" type="text" class="input-captcha" i18n-key="login.page.captcha">
									<img class="captcha-img" id="captcha-img">
								</div>
								<div class="row-remember">
									<span class="login-remember" id="remember" i18n-key="login.page.remember">记住我的登录状态</span>
									<a class="forget-pwd" href="#">忘记密码</a>
									<a class="login-sign"
										href="/qjlh/ZHJG/app/QJLH.app/public/qyd/sign_in/提交单行数据.spg?:newData=true">用户注册</a>
								</div>
								<div class="login-btn" id="login-btn-account" i18n-key="login.page.login">登&nbsp;&nbsp;录
								</div>
								<div class="login-options">
									<span class="login-option switch-qrcode" id="account-to-qrcode" i18n-key="login.page.qrcodeLogin">扫码登录</span>
									<span class="login-option switch-phone" id="account-to-phone" i18n-key="login.page.smsLogin">手机号登录</span>
								</div>
							</div>
							<div class="login-qrcode" id="login-qrcode">
								<div class="qrcode-body"></div>
								<div class="login-options-qrcode">
									<span class="login-option switch-phone" id="qrcode-to-phone" i18n-key="login.page.smsLogin">手机号登录</span>
									<span class="login-option switch-account" id="qrcode-to-account" i18n-key="login.page.passwordLogin">账号登录</span>
								</div>
							</div>
							<div class="login-phone" id="login-phone">
								<div class="login-status" id="status-phone"></div>
								<div class="row-phone">
									<input id="phone" class="input-phone" name="phone" type="tel" placeholder="请输入手机号" i18n-key="login.page.phone">
								</div>
								<div class="row-captcha" id="captcha-container">
									<input id="phone-captcha" name="captcha" placeholder="验证码" type="text" class="input-captcha" i18n-key="login.page.captcha">
									<img class="captcha-img" id="phone-captcha-img">
								</div>
								<div class="row-code" id="code-container">
									<input id="code" name="code" placeholder="短信验证码" type="text" class="input-code" i18n-key="login.page.code">
									<input id="send-code" name="send-code" type="button" class="input-sendcode" value="获取验证码" i18n-key="login.page.sendcode">
								</div>
								<div class="login-btn" id="login-btn-sms" i18n-key="login.page.login">登&nbsp;&nbsp;录
								</div>
								<div class="login-options-phone">
									<span class="login-option switch-qrcode" id="phone-to-qrcode" i18n-key="login.page.qrcodeLogin">扫码登录</span>
									<span class="login-option switch-account" id="phone-to-account" i18n-key="login.page.passwordLogin">账号登录</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="foot-bar">
					<div class="foot-title" i18n-key="login.page.thirdPartyLogin">第三方登录</div>
					<div class="foot-content" id="third-party-logins">
					</div>
				</div>
				<div class="version"></div>
			</div>
		</div>
	</div>
	<script>
		requirejs(['sys/sys'], function(sys) {
			sys.ready('security/login').then(function(login) {
				login.initLoginPageTemplate();

				/*
				login.initLoginPageTemplate({page:'phone'}).then(function (loginPage) { // 改成首页显示手机登录
					var submit_old = loginPage.submit;
					loginPage.submit = function (data, sms) {
						sys.ready(sys.ctx('/sysdata/public/imageJigsaw/client.js')).then(function (m) {
							m.showVerifyImage({
								onsuccess: (token) => {
									data.token = token;
									submit_old.call(loginPage, data, sms);
								}
							})
						});
					}
				})
				*/
			});
		});
	</script>
</body>

</html>