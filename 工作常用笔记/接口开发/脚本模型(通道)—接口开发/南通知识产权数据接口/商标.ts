import { getDataSource } from "svr-api/db"; //数据库相关API
import { EventOperationState, ServerEvent, fireEvent } from "svr-api/sys";
import { post, request } from "svr-api/http";
import { requestData, getToken, getMaxTime, checkTableIsContainsData } from "/sdi/script/requestExternalInterface.action";

/**
 * 返回脚本节点字段结构。
 * 使用场景：
 * 1. 通过脚本爬取数据到数据仓库，需要生成定义好的字段结构。
 * 2. 通过脚本解析json数据，需要预解析几行数据生成字段。
 */
function onProcessFields(context: IDataFlowScriptNodeContext): DbFieldInfo[] {
    return outputFields;
}

/** 表类型 */
const requestTyle = "商标";
/** 数据源名 */
const dataBase: string = "sjzt_ods";
/** 脚本提取的目标表 */
const targetTableName: string = "ODS_ZSCQ_SB";
/**
 * 返回脚本节点的数据结构
 * 注意：该表过滤采用的是：es查询，参考：https://ip.jsipp.cn/ZSCQ/app/zscq.app/DSJJSFX/ZLJSYFX/ZLJSBZ.spg
 * eg：申请日期=[20180101 to 20200201]
 */
function onProcessData(context: IDataFlowScriptNodeContext): DbTableInfo | any[][] {
    let rows: Array<Array<string>> = [];
	
	let isPreview = context.isPreview; // 是否为预览数据
    if (isPreview) {
        print(`--【商标】脚本，当前状态【预览】，不执行脚本`);
        return rows;
    }
	
    let token: string = "";
    let requestTokenResult = getToken();
    if (!requestTokenResult.success) {
        console.debug(`--【${targetTableName}】 获取凭证token失败，详情：${JSON.stringify(requestTokenResult)}--`);
        return rows;
    }
    token = requestTokenResult.access_token;

    let isContainData: boolean = checkTableIsContainsData(dataBase, targetTableName);
    if (isContainData) { // true：没有数据，全量加载
        console.debug(`--【${targetTableName}】表没有数据，首次加载，请求所有【南通数据中台】的数据--start`);
        let filter = "申请人行政区划 = 3206??";
        rows = requestData(requestTyle, targetTableName, token, filter);
        console.debug(`--【${targetTableName}】初始化【南通数据中台】的数据完成--end`);
    } else {
        let lastRequestMaxTime: string = getMaxTime(dataBase, targetTableName, "APP_DATE");
        console.debug(`--【${targetTableName}】表增量请求，过滤条件：根据上次请求的【导入月份】最大值【${lastRequestMaxTime}】-至今，来过滤数据--start`);
        let filter = `申请人行政区划 = 3206?? and 申请日期 > ${lastRequestMaxTime}`;
        console.debug(`----过滤条件：${filter}`);
        rows = requestData(requestTyle, targetTableName, token, filter);
        console.debug(`--【${targetTableName}】表增量请求--end`);
    }
    return rows;
}

const outputFields = [
    { name: "专用期开始日期", dbfield: "PROPERTY_BGN_DATE", dataType: FieldDataType.P },
    { name: "专用期结束日期", dbfield: "PROPERTY_END_DATE", dataType: FieldDataType.P },
    { name: "产业", dbfield: "CY", dataType: FieldDataType.C, length: 3 },
    { name: "代理人中文名称", dbfield: "NAME", dataType: FieldDataType.C, length: 1501 },
    { name: "代理人中文地址", dbfield: "AGENT_ADDR", dataType: FieldDataType.C, length: 1501 },
    { name: "代理人代码", dbfield: "AGENT_ID", dataType: FieldDataType.C, length: 481 },
    { name: "代理人手机号码", dbfield: "AGENT_CELLPHONE_NUM", dataType: FieldDataType.C, length: 151 },
    { name: "代理人状态", dbfield: "AGENT_STATE", dataType: FieldDataType.C, length: 16 },
    { name: "代理人类型", dbfield: "AGENT_TYPE", dataType: FieldDataType.C, length: 151 },
    { name: "代理人经济性质", dbfield: "AGENT_ECONOMY_TYPE", dataType: FieldDataType.C, length: 1501 },

    { name: "代理人联系email", dbfield: "AGENT_CONTACT_EMAIL", dataType: FieldDataType.C, length: 151 },
    { name: "代理人联系人", dbfield: "AGENT_CONTACT_NAME", dataType: FieldDataType.C, length: 1501 },
    { name: "代理人联系传真", dbfield: "AGENT_CONTACT_FAX", dataType: FieldDataType.C, length: 151 },
    { name: "代理人联系地址", dbfield: "AGENT_CONTACT_ADDR", dataType: FieldDataType.C, length: 1501 },
    { name: "代理人联系电话", dbfield: "AGENT_CONTACT_TEL", dataType: FieldDataType.C, length: 151 },
    { name: "代理人行政区划编码", dbfield: "AGENT_REGIONALISM_ID", dataType: FieldDataType.C, length: 49 },
    { name: "代理人证件名称", dbfield: "AGENT_CERTIFICATE_NAME", dataType: FieldDataType.C, length: 151 },
    { name: "代理人证件编码", dbfield: "AGENT_CERTIFICATE_ID", dataType: FieldDataType.C, length: 49 },
    { name: "代理人邮政编码", dbfield: "AGENT_CONTACT_ZIP", dataType: FieldDataType.C, length: 31 },
    { name: "内部序号", dbfield: "NBXH", dataType: FieldDataType.C, length: 50 },

    { name: "初审公告日期", dbfield: "FIRST_ANNC_DATE", dataType: FieldDataType.P },
    { name: "初审公告期号", dbfield: "FIRST_ANNC_ISSUE", dataType: FieldDataType.C, length: 151 },
    { name: "商标名称", dbfield: "TM_NAME", dataType: FieldDataType.M },
    { name: "商标名称意译", dbfield: "TM_NAME_TRANSLATE", dataType: FieldDataType.M },
    { name: "商标形式类型", dbfield: "TM_FORM_TYPE", dataType: FieldDataType.C, length: 151 },
    { name: "商标类型", dbfield: "TM_TYPE", dataType: FieldDataType.C, length: 31 },
    { name: "商标颜色说明", dbfield: "TM_COLOUR_DESC", dataType: FieldDataType.M },
    { name: "国家编码", dbfield: "REG_CTRY_ID", dataType: FieldDataType.C, length: 49 },
    { name: "地理标志信息（停用）", dbfield: "LANDMARK_INFO", dataType: FieldDataType.C, length: 16 },
    { name: "尼斯分类", dbfield: "INT_CLS", dataType: FieldDataType.M },

    { name: "放弃专用权说明", dbfield: "ABANDON_PROPERTY_DESC", dataType: FieldDataType.M },
    { name: "是否共享商标", dbfield: "IF_SHARE_TM", dataType: FieldDataType.C, length: 16 },
    { name: "是否具有优先权", dbfield: "YXQ", dataType: FieldDataType.C, length: 3 },
    { name: "是否立体商标", dbfield: "IF_SOLID_TM", dataType: FieldDataType.C, length: 16 },
    { name: "是否驰名", dbfield: "IF_CM_TM", dataType: FieldDataType.C, length: 16 },
    { name: "注册人中文名称", dbfield: "REG_CN_NAME", dataType: FieldDataType.C, length: 1501 },
    { name: "注册人中文地址", dbfield: "REG_CN_ADDR", dataType: FieldDataType.C, length: 1501 },
    { name: "注册人外文名称", dbfield: "REG_EN_NAME", dataType: FieldDataType.C, length: 1501 },
    { name: "注册人外文地址", dbfield: "REG_EN_ADDR", dataType: FieldDataType.C, length: 1501 },
    { name: "注册人姓名", dbfield: "ZCR_XM", dataType: FieldDataType.C, length: 200 },

    { name: "注册人行政区划编码", dbfield: "REG_REGIONALISM_ID", dataType: FieldDataType.C, length: 49 },
    { name: "注册人证据号码", dbfield: "ZCR_ZJHM", dataType: FieldDataType.C, length: 200 },
    { name: "注册公告日期", dbfield: "REG_ANNC_DATE", dataType: FieldDataType.P },
    { name: "注册公告期号", dbfield: "REG_ANNC_ISSUE", dataType: FieldDataType.C, length: 151 },
    { name: "注册号", dbfield: "REG_NUM", dataType: FieldDataType.C, length: 211 },
    { name: "申请日期", dbfield: "APP_DATE", dataType: FieldDataType.P },
    { name: "相似群", dbfield: "SIMILAR_CODE", dataType: FieldDataType.C, length: 1000 },
    { name: "相似群名称", dbfield: "GOODS_CN_NAME_QUE", dataType: FieldDataType.M },
    { name: "经济户口类别", dbfield: "JJHKLB", dataType: FieldDataType.C, length: 7 },
    { name: "英文名称", dbfield: "EN_NAME", dataType: FieldDataType.C, length: 1501 },

    { name: "英文地址", dbfield: "AGENT_EN_ADDR", dataType: FieldDataType.C, length: 1501 },
    { name: "行业代码", dbfield: "HYDM", dataType: FieldDataType.C, length: 21 },
    { name: "颜色标志信息", dbfield: "COLOUR_SIGN", dataType: FieldDataType.C, length: 751 }
]