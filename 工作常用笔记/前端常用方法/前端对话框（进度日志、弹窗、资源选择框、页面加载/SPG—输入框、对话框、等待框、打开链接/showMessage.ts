import { ListArgs, List, TreeGridArgs, ListItem } from 'commons/tree';
import { ICustomJS, IFAppCustomJS, IFApp, FAppInterActionEvent, InterActionEvent } from 'metadata/metadata-script-api';
import { SZEvent, showConfirmDialog, rc, Component, rc_task, uuid } from 'sys/sys';
import { MetaFileViewer, getMetaRepository, getCurrentUser } from 'metadata/metadata';
import { DwTableModelFieldProvider } from 'dw/dwdps';
import { queryDwTablesState, queryLabelLibs, saveLabelLib, updateDwTableDatas } from 'dw/dwapi';
import { showResourceDialog } from 'metadata/metamgr';

/**
 * 当前项目的根路径
 */
const CURENT_SYSTEM_PATH = '/QYFWPT/app/label.app/';

const fappCustomJS: IFAppCustomJS = {
	onSubmitData: (event: FAppInterActionEvent): boolean | Promise<boolean> => {
		let app = event.app;
		let items = app.dataList.dataList.getItems();
		// 获取当前【选择表】路径，eg：/QYFWPT/data/tables/DIM_XGTSFD.tbl'
		// let currentChoseModelTablePath = app.formsDataMgr.getFormsData().getFormData("请选择需要打标的数据表").getData(2).value;

		/** 当前选择表的resid */
		let currentChoseModelTableResid = app.formsDataMgr.getFormsData().getFormData("请选择需要打标的数据表").getValue("customResselector1".toUpperCase());
		if (!currentChoseModelTableResid) {
			return true;
		}
		
		// 校验当前表是否有主键
		let isKeysExists = rc({
			url: `${CURENT_SYSTEM_PATH}data-label/dataLabelMgr.action?method=hasPrimaryKey`,
			method: "POST",
			data: {
				currentChoseModelTableResid: currentChoseModelTableResid
			}
		});
		// 若当前表不存在主键字段，则提示
		if(!isKeysExists) {
			app.showMessage('该标签表不存在主键字段，请重新选择', 'error');
			return false;
		}
		return true;
	}
}