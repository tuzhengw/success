/**
 * 文件资源表
 */
const FACT_FILERESOURCE = "fact_fileresource";
/**
 * 查询文件信息SQL
 */
const QUERY_FILE_INFO_SQL = `select URL,FILE_NAME from fact_fileresource where FILE_ID=? limit 1`;
/**
 * 20210914
 * 通过文件id获取资源表中的路径，然后获取内容的base64字符串
 * @params id：文件ID
 * @return base64字符串
 */
function fileToBase64(id: string): string {
    if (!id) {
        return "";
    }
    let table = ds.openTableData(FACT_FILERESOURCE);
    let fileData = table.executeQuery(QUERY_FILE_INFO_SQL, [id]);

    if (fileData == null || fileData.length == 0) {
        return "文件不存在";
    }

    let workDir = getWorkDir();
    // 定义一下附件存储的时候的数据信息，集群环境下，存到磁盘指定的clusters-share，这个是共享磁盘，
    let sharePath = "/clusters-share/attachments/";
    let path = workDir + sharePath + fileData[0].URL;
    let file = getFile(path);

    // /u01/succezsoft/workdir/clusters-share/attachments/FACT_FILERESOURCE/URL/b4e7c6025177a2abcd422aa802ca4d61
    if (file == null || !file.exists()) {
        console.debug("--文件路径不存在--");
        return "文件路径不存在";
    }

    let fileType: string = getFileType(fileData[0].FILE_NAME);
    if (fileType == null) {
        console.debug("--文件类型错误--");
        return "文件类型错误";
    }
    /**
     * 给base64 新增前缀，例如：data:image/图片类型;base64,.....
     */
    let prefixBase64 = `data:image/${fileType.toLowerCase()};base64,`;
    let base64String = prefixBase64 + file.readBase64();
    return base64String;
}

/**
 * 获取文件的后缀
 * @fileName：文件名.类型
 * @return 文件的后缀名
 * <pre>
 *     "file.txt" —— txt
 * </pre>
 */
function getFileType(fileName: string): string {
    return fileName.lastIndexOf('.') < 1
        ? null
        : fileName.split(".").slice(-1).toString();
}