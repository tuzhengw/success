/**
 * =====================================================
 * 作者：tuzw
 * 审核人员：liuyz
 * 创建日期: 2022-08-16
 * 功能描述：系统管理脚本
 * =====================================================
 */
import { isEmpty, showWarningMessage, showDialog } from "sys/sys";
import { InterActionEvent } from "metadata/metadata-script-api";
import { Dialog, ShowDialogArgs } from "commons/dialog";
import { ProgressLogs } from "commons/progress";

/**
 * 中台日志管理类
 */
export class SdiLogInfoMgr {

    public CustomActions: any;
    constructor() {
        this.CustomActions = {
            button_showDetailLogInfo: this.button_showDetailLogInfo.bind(this)
        }
    }

    /**
     * 将传入的日志信息以进度形式展示
     * @param showLogInfo 展示的日志信息，eg：'{"logs":[
     *      {"time":1.657854575385E12,"log":"开始执行【sdi.ac.deleteDataSource】方法"},
     *      {"time":1.657854575388E12,"log":"删除数据源，dbName：esa_1"},
     * ]}'
     */
    public button_showDetailLogInfo(event: InterActionEvent): Promise<any> {
        let params = event.params;
        let showLogInfo: string = params?.showLogInfo;
        if (isEmpty(showLogInfo)) {
            showWarningMessage(`当前没有日志信息`);
            return Promise.resolve(false);
        }
        let logInfos = JSON.parse(showLogInfo)['logs'];
        let showLogDialogArgs: ShowDialogArgs = {
            caption: "日志详情",
            content: { implClass: ProgressLogs },
            minHeight: 200,
            minWidth: 500,
            maxWidth: 1000,
            height: 500,
            flexible: "true",
            overflow: "auto"
        }
        return showDialog(showLogDialogArgs).then((diglog: Dialog) => {
            let logs: ProgressLogs = diglog.content;
            logs.clear();
            logs.addLogs(logInfos);
            return Promise.resolve(diglog);
        });
    }
}