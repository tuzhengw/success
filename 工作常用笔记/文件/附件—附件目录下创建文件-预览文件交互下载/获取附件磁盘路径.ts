
import fs from "svr-api/fs";
import sys from "svr-api/sys";


/**
 * 根据指定附件ID获取磁盘路径
 * <p>
 *  产品源码参考: filestorage/impl/WorkDirFileStorageServiceAbstract.java
 * </p>
 * @param attachId 附件ID
 *        1) 存储模式default, eg: default:e6243167dd7d43558d8039f1ebb3a0c1
 *        2) 存储模式工作区, eg: workdir:33e2fa4876f5e02744dd8047a7af9b600538-将描述加入到页面.png
 * @return
 */
export function getFileStorageFilePath(attachId: string): string {
    let arr = attachId.split(":");
    let attachMode: string = arr[0];
    let attachName: string = arr[1];
    let attachPath: string = `${sys.getClusterShareDir()}/file-storage`;
    switch (attachMode) {
        case "default":
            let parentFoldName: string = attachName.substring(0, 2); // 前两位作为附件的父目录
            attachPath = `${attachPath}/default/${parentFoldName}/${attachName}`;
            break;
        case "workdir":
            attachPath = `${attachPath}/relativePath/${attachName}`;
            break;
        default:
            attachPath = "";
            console.info(`附件: [${attachId}] 存储模式[${attachMode}] 未定义`);
    }
    console.info(`附件[${attachId}]存储磁盘路径: ${attachPath}`);
    return attachPath;
}