# limit

​	如果是一百页以内，就使用最基本的分页方式，大于一百页，则使用子查询的分页方式

```mysql
# offset：起始位（偏移一位）
# rows：分页的行数，例如：10,5，则从第11页起，显示5页
SELECT * FROM table LIMIT [offset,] rows | rows OFFSET offset
```

## 基本分页

​	基本用法：

```mysql
# 检索记录行 6-15 
SELECT * FROM table LIMIT 5,10; 
```

​	从某一个偏移量到记录集的结束所有的记录行，可以指定第二个参数为 ：《-1》

```mysql
# 检索记录行 96-last
SELECT * FROM table LIMIT 95,-1;  
```

​	只给定一个参数，它表示返回：最大的记录行数目

```mysql
# 索前 5 个记录行
SELECT * FROM table LIMIT 5;
```

​	换句话说，`LIMIT n `等价于` LIMIT 0, n`

---

## 子查询分页

​	**最基本的分页方式：**

```mysql
SELECT ... FROM ... WHERE ... ORDER BY ... LIMIT ...  
```

​	案例：

```mysql
SELECT * FROM articles 
	WHERE category_id = 123 
	ORDER BY id DESC
	LIMIT 10000, 10
```

​	随着数据量的增加，页数会越来越多，越往后分页，LIMIT语句的偏移量就会越大，速度也会明显变慢

```mysql
SELECT * FROM articles 
	WHERE id >=
		# 获取子查询结果的第10000条所在的id位置
 		(SELECT id FROM articles  
         	WHERE category_id = 123
         	ORDER BY id 
         	LIMIT 10000, 1) 
    LIMIT 10
```

## join-在分页

```mysql
SELECT * FROM content AS t1    
	JOIN 
		( SELECT id FROM content 
     		ORDER BY id desc 
     		LIMIT 
     			".($page-1)*$pagesize.", 1) AS t2 )   
	WHERE 
		t1.id <= t2.id 
	ORDER BY t1.id desc 
	LIMIT $pagesize;   
```

​	join分页和子查询分页的效率基本在一个等级上，消耗的时间也基本一致