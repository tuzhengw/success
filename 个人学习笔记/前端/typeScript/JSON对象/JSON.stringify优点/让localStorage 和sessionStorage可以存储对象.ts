/**
	localStorage/sessionStorage默认只能存储字符串，
而实际开发中，我们往往需要存储的数据多为对象类型，
那么这里我们就可以在存储时利用json.stringify()将对象转为字符串，
而在取缓存时，只需配合json.parse()转回对象即可。
*/
function main(){
	//测试
	setLocalStorage('demo',[1,2,3]);
	let  a = getLocalStorage('demo');//[1,2,3]
}

//存
function setLocalStorage(key,val){
	window.localStorage.setItem(key,JSON.stringify(val));
};

//取
function getLocalStorage(key){
	let val = JSON.parse(window.localStorage.getItem(key));
	return val;
};