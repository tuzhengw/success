import { Combobox, Button, showUploadDialog, Toolbar, ToolbarItemAlign, ComboboxArgs, DataProvider, ButtonArgs, TXT_FILETYPE, IMAGE_FILETYPE } from 'commons/basic';

/** 通过导入excel进行硬标签打标 */
button_label_excel: (event: InterActionEvent) => {
	let builder = (<any>event.page).builder;
	let urlParams = event.page.getUrlParams();
	let resId: string = urlParams.table;
	let lib: string = urlParams.lib;
	let label: string = urlParams.label;
	let filterExp: string; // 匹配列表的过滤条件
	let unmatchFilter: string; // 不匹配列表的过滤条件
	let rowCount: number;//匹配列表条数
	let unmatchQuery: QueryInfo;//不匹配query
	let fileName: string;
	showUploadDialog({
		fileTypes: ["xls", "xlsx"], //只上传excel文档
		multiple: false,       //上传单个文档
		maxSize: 10 * 1024 * 1024, //限制大小10M
		onchange: (e) => {
			let file = e.value[0];
			let excelResult: ImportDataResult;
			// 导入excel到临时模型
			importData({
				fileInfo: {
					fileId: file.id,
					fileType: file.fileType,
					headsStart: 1,
					headsEnd: 1
				},
				importSameFields: true,
				resIdOrPath: EXCEL_MODEL_PATH + resId + ".tbl",
				importUnmatchedFields: true,
				fieldValues: [{
					name: "UUID",
					valueType: "const",
					value: file.id
				}]
			}).then((result) => {
				excelResult = result;
				return getMetaRepository().getFileBussinessObject(resId);
			}).then((bo: DwTableModelFieldProvider) => {
				let promise = new Promise<JSONObject>((resolve) => {
					// 计算默认的匹配字段
					let targetField;
					let fileField = excelResult.fileFields.find((field) => {
						let fieldInfo = bo.getField(field) || bo.getFieldByDbField(field);
						if (fieldInfo && (fieldInfo.dataType == FieldDataType.C || fieldInfo.dataType == FieldDataType.I)) {
							targetField = fieldInfo.dbfield;
							return true;
						}
					})
					showFormDialog({
						id: "spg.dialog.labelExcel",
						caption: "匹配字段",
						buttons: ["ok"],
						content: {
							items: [{
								id: "label-excel-fileField",
								caption: "文件字段",
								desc: "选择用于匹配数据的文件字段",
								formItemType: "combobox",
								items: excelResult.fileFields,
								value: fileField,
								validRegex: (v: string) => {
									if (!v) {
										return { result: true };
									}
								}
							}, {
								id: "label-excel-targetField",
								caption: "目标表字段",
								formItemType: "combobox",
								type: "tree",
								dataProvider: bo,
								value: targetField,
								desc: "选择用于匹配数据的目标表字段",
								searchBoxVisible: true,
								itemDataConf: { valueField: "id" },
								validRegex: (v: string) => {
									if (!v) {
										return { result: true };
									}
								}
							}]
						},
						onshow: (event: SZEvent, dialog: Dialog) => {
							let form = <Form>dialog.content;
							form.waitInit().then(() => {
								form.getFormItem('label-excel-fileField').setValue(fileField);
								targetField && form.getFormItem('label-excel-targetField').setValue(targetField);
								form.getFormItem('label-excel-fileField').comp.setItems(excelResult.fileFields);
								form.getFormItem('label-excel-targetField').setDataProvider(bo);
							})
						},
						onok: (sze, dialog, btn, form) => {
							let fileField = form.getFormItem("label-excel-fileField").getValue();
							let dbField = excelResult.fields[excelResult.fileFields.indexOf(fileField)];
							resolve({
								bo: bo,
								fileField: dbField,
								targetField: form.getFormItem("label-excel-targetField").getValue(),
							});
						}
					});
				});
				return promise;
			}).then((result: JSONObject) => {
				// 根据导入结果生成关联匹配和不匹配的过滤条件
				let bo: DwTableModelFieldProvider = result.bo;
				let key = bo.getPrimaryKeys()[0];
				let matchField = result.fileField;
				fileName = bo.getFileInfo().desc;
				let filter = `model1.${result.targetField}=model2.${matchField}`;
				/*
				let matchFields = excelResult.targetFields;
				excelResult.fields.forEach((field, i) => {
					if (matchFields[i]) {
						let fieldInfo = bo.getField(field) || bo.getFieldByDbField(field);
						if (fieldInfo && (fieldInfo.dataType == FieldDataType.C || fieldInfo.dataType == FieldDataType.I)) {
							if (filter.length) {
								filter += ' and ';
							}
							filter += `model1.${fieldInfo.dbfield}=model2.${matchFields[i]}`
						}
					}
				});
				*/
				if (filter.length) {
					unmatchFilter = `not exists select(model1, ${filter}) and model2.uuid='${file.id}'`;
					filter += ` and model2.uuid='${file.id}'`;
					filterExp = `exists select(model2, ${filter})`;
				}
				else {
					unmatchFilter = `model2.uuid='${file.id}'`;
					filterExp = '1>2';
				}
				hard_label_filter = { exp: filterExp };
				// 查询匹配数量
				let queryInfo: QueryInfo = {
					fields: [{ name: "count", exp: `count(model1.${result.targetField})` }],
					filter: [{ exp: filterExp }],
					sources: [{
						id: "model1",
						path: bo.getFileInfo().path
					}, {
						id: "model2",
						path: EXCEL_MODEL_PATH + resId + ".tbl"
					}]
				};
				//获取匹配主键
				/*label_match_primaryKey = {
					fields: [{ name: `${result.targetField}`, exp: `model1.${result.targetField}` }],
					filter: [{ exp: filterExp }],
					sources: [{
						id: "model1",
						path: bo.getFileInfo().path
					}, {
						id: "model2",
						path: EXCEL_MODEL_PATH + resId + ".tbl"
					}]
				};*/
				//未匹配数量
				unmatchQuery = {
					fields: [{ name: "count", exp: `count(model2.${matchField})` }],
					filter: [{ exp: unmatchFilter }],
					sources: [{
						id: "model1",
						path: bo.getFileInfo().path
					}, {
						id: "model2",
						path: EXCEL_MODEL_PATH + resId + ".tbl"
					}]
				};
				return getQueryManager().queryData(queryInfo, uuid());
			}).then((result: QueryDataResultInfo) => {
				rowCount = <number>result.data[0][0];
				return getQueryManager().queryData(unmatchQuery, uuid());
			}).then((result: QueryDataResultInfo) => {
				let unMatchCount = <number>result.data[0][0];
				showConfirmDialog({
					caption: "提示",
					message: `本次Excel共导入${excelResult.rowCount}条数据；其中${excelResult.rowCount - unMatchCount}条数据共匹配到${fileName}中${rowCount}条记录，剩余${unMatchCount}条数据未匹配到信息`,
					onok: () => {
						// 进入到导入excel结果界面
						let domFileViewer = builder.getRenderer().domBase.closest('.metafileviewer-base');
						if (!domFileViewer) {
							return;
						}
						let fileViewer: MetaFileViewer = domFileViewer.szobject as MetaFileViewer;
						return fileViewer.showDrillPage({
							url: {
								path: getAppPath(`/data-label/数据打标/导入excel.spg`),
								params: {
									':runtimecompile': true,
									'label': label, 'lib': lib, 'table': resId, 'filterExp': filterExp, 'unmatchFilter': unmatchFilter, targetFields: excelResult.fields, fileFields: excelResult.fileFields
								}
							},
							breadcrumb: true,
							target: ActionDisplayType.Container,
							title: `导入Excel数据`
						});
					}
				});
			})
		}
	})
}