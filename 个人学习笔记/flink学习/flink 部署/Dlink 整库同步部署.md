---
typora-root-url: images
---

主要用于：批量同步某个库下多张表数据时。

# 整库同步

# 一 Dinky在Apache Doris的实时整库同步与模式演变

地址：https://www.bilibili.com/video/BV1dG4y197ca/?spm_id_from=333.788&vd_source=fa512aa9a0db6f5d99c69c6f94fa4e69

## 1 需求





![](/整库同步-需求.png)

## 2 现状

![](/整库同步-问题.png)

## 3 CDC Source实现思路

![](/CDC Source思路.png)

## 4 多源合并

### 4.1 优势

​	合并同一个作业中的source，确保只有一个连接数，实现整库的读取，降低数据库的压力。

​	根据元数据信息来构建特殊处流，对上一步多源合并变动的数据进行过滤分流，从核心源码看，首先需要遍历所有元数据的表，并通过正则进行分库分表，转换为汇总库表名，来提前构建好对应的`OutPutTag<Map>`。

![](/多源合并优势.png)

### 4.2 实现解读

​	`Flink`通过`dataStream API` 来构建`MySQL` source 和 builder，其使用到的参数：数据库连接配置、采集库表。

![](/多源合并解读实现.png)

​	`OutPutTag`

### ![](/多源合并-构建outputtag.png)

### 4.3 分库分表的测输出流构建

![分库分表侧输出流构建](/分库分表侧输出流构建.png)

### 4.4 构建`Sink SQL`

![](/构建SQL Sink.png)

### 4.5 `FlatMap`构建`DataStream Row`

![](/FlatMap构建流行.png)

​	源码

![](/FlatMap构建Row源码.png)

### 4.6 构建`DataStream Sink`

![](/构建DataStream Sink.png)



# 二 基于Dinky整库同步(flink 1.13.6)

官方网址：

http://www.dlink.top/docs/next/data_integration_guide/cdcsource_statements/#application-%E6%A8%A1%E5%BC%8F%E6%8F%90%E4%BA%A4

## 1 打包Dinky jar包

​	由于 CDCSOURCE 是 Dinky 封装的新功能，Apache Flink 源码不包含，非 Application 模式提交需要在远程 Flink 集群所使用的依赖里添加一下依赖：

```
# 将下面 Dinky根目录下 整库同步依赖包放置 $FLINK_HOME/lib下
lib/dlink-client-base-${version}.jar
lib/dlink-common-${version}.jar
plugins/flink-${flink-version}/dlink-client-${version}.jar
```

### 1.1 打包问题

​	dinky是一个整体，内部部分子项目都是依赖其他子项目，不能单独打包单个子项目，否则会出现：

![](/dinky jar包生成问题.png)

​	异常：找不到项目内依赖的子模块

### 1.2 打包问题解决办法

​	直接编译整个dinky项目。

![](/dinky打包文档.png)

![](/编译dinkyjar包.png)

## 2 Flink 放置jar包

​	flink-connector-mysql-cdc-1.1.0.jar 是阿里老版本CDCjar包，但dlink整库同步仅基于之前历史版本写的，需要支持，Flink服务器lib可放置多个flink-connector-mysql-cdc版本，已测试可正常运行。

​	若缺失flink-connector-mysql-cdc-1.1.0.jar包则会提示找到此类：Lorg/apache/kafka/connect/json/JsonConverter。

​	下载地址：https://mvnrepository.com/artifact/com.alibaba.ververica/flink-connector-mysql-cdc

![](/Flink整库同步jar包放置.png)

## 3 CDC SOURCE语法

参考：http://www.dlink.top/docs/next/data_integration_guide/cdcsource_statements/#%E6%95%B4%E5%BA%93%E5%90%8C%E6%AD%A5%E5%88%B0-mysql



## 4 应用模型开发CDC SOURCE

### 4.1 jar包准备

​	flink相关jar包则根据自己的flink版本准备，此处主要是dinky整库同步相关jar包。

```
dlink-client-1.13-0.6.5.jar
dlink-client-base-0.6.5.jar
dlink-metadata-mysql-0.6.5.jar
dlink-metadata-base-0.6.5.jar
dlink-common-0.6.5.jar
dlink-executor-0.6.5.jar // 内部数据库连接逻辑已更改, 可见本章节 5.3
reflections-0.10.2.jar
flink-json-1.11.0.jar
druid-1.2.8.jar
```

### 4.1 IDEA执行

![](/IDEA添加dinky相关jar包.png)

```java
package com.flink.code;

import com.dlink.assertion.Asserts;
import com.dlink.executor.EnvironmentSetting;
import com.dlink.executor.Executor;
import com.dlink.executor.ExecutorSetting;
import com.dlink.executor.RemoteStreamExecutor;
import org.apache.flink.api.common.RuntimeExecutionMode;
import org.apache.flink.core.execution.JobClient;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.TableResult;

import java.util.HashMap;

public class CdcSource {
    public static void main(String[] args) throws Exception {
        /**
         * 执行的整库同步SQL
         * 注意:
         * 1. 每行后面需要紧跟: 逗号;
         * 2. 自动创建语法必须保证库下表不存在时指定。
         */
        String executeSql = "EXECUTE CDCSOURCE cdc_mysql WITH ("
                + "'connector' = 'mysql-cdc',"
                + "'hostname' = '192.168.144.129',"
                + "'port' = '3306',"
                + "'username' = 'root',"
                + "'password' = '123456',"
                + "'checkpoint' = '3000',"
                + "'scan.startup.mode' = 'initial',"
                + "'parallelism' = '1',"
                + "'table-name' = 'test1\\.TEACHER,test1\\.DOCTOR',"
                + "'sink.connector' = 'jdbc',"
                + "'sink.url' = 'jdbc:mysql://192.168.144.129:3306/test2?serverTimezone=GMT%2B8&characterEncoding=utf-8&useSSL=false',"
                + "'sink.username' = 'root',"
                + "'sink.password' = '123456',"
                + "'sink.sink.db' = 'test2',"
                + "'sink.table.upper' = 'true',"
                + "'sink.table-name' = '${tableName}',"
                + "'sink.driver' = 'com.mysql.cj.jdbc.Driver',"
//                + "'sink.driver' = 'com.mysql.jdbc.Driver',"
                + "'sink.sink.buffer-flush.interval' = '2s',"
                + "'sink.sink.buffer-flush.max-rows' = '100',"
                + "'sink.sink.max-retries' = '5'"
                + ")";
        EnvironmentSetting eEnvironmentSetting = new EnvironmentSetting("192.168.144.129", 8081);
        HashMap<String, String> tableConf = new HashMap<>();
        ExecutorSetting executorSetting = new ExecutorSetting(3000, 1, false, false, false, "file:\\\\tmp\\flink\\checkpoints", "整库同步", tableConf);
        Executor executor = new RemoteStreamExecutor(eEnvironmentSetting, executorSetting);
        executor.executeSql(executeSql);
        JobClient jobClient = executor.executeAsync("整库同步");
        System.out.println(jobClient.getJobID());
    }
}
```

### 4.2 bi环境执行

```ts
/**
 * 基于[dinky]提交整库同步
 * 
 * 1. 提交过程中, 内部会对sink的表创建临时视图表注意事项:
 * - 会给涉及的表增加 sink, [dink-client-1.14 -> com.dlink.cdc.sql.SQLSinkBuilder -> addTableSink];
 * - [addTableSink]方法中会给涉及的表创建临时视图表, 若临时表已经存在则会抛出: [
 * org.apache.flink.table.api.ValidationException: Temporary table '`default_catalog`.`default_database`.`VIEW_test1_DOCTOR`' already exists], 
 * 此异常是[org.apache.flink.table.api.internal.flink->createTemporaryView]方法通过[ignoreIfExists]控制的,
 * 内部值静态: false, 此异常不影响正常执行。 
 */
function dinkyCdcSourceTest(): void {
    let statement: string = "EXECUTE CDCSOURCE cdc_mysql WITH ("
        + "'connector' = 'mysql-cdc',"
        + "'hostname' = '192.168.144.129',"
        + "'port' = '3306',"
        + "'username' = 'root',"
        + "'password' = '123456',"
        + "'checkpoint' = '3000',"
        + "'scan.startup.mode' = 'initial',"
        + "'parallelism' = '1',"
        + "'table-name' = 'test1\\.TEACHER,test1\\.DOCTOR',"
        + "'sink.connector' = 'jdbc',"
        + "'sink.url' = 'jdbc:mysql://192.168.144.129:3306/test2?serverTimezone=GMT%2B8&characterEncoding=utf-8&useSSL=false',"
        + "'sink.username' = 'root',"
        + "'sink.password' = '123456',"
        + "'sink.sink.db' = 'test2',"
        + "'sink.table.upper' = 'true',"
        + "'sink.table-name' = '${tableName}',"
        + "'sink.driver' = 'com.mysql.cj.jdbc.Driver',"
        + "'sink.sink.buffer-flush.interval' = '2s',"
        + "'sink.sink.buffer-flush.max-rows' = '100',"
        + "'sink.sink.max-retries' = '5'"
        + ")";
    let eEnvironmentSetting = new EnvironmentSetting("192.168.144.129", 8081);
    let tableConf = new java.util.HashMap();
    let executorSetting = new ExecutorSetting(3000, 1, false, false, false, "file:\\\\tmp\\flink\\checkpoints", "整库同步", tableConf);
    print(executorSetting.toString());
    let executor = new RemoteStreamExecutor(eEnvironmentSetting, executorSetting);
    /**
     * 		内部会进行数据库连接, 若数据库连接失败, 内部异常被捕获不会抛出, 
     * 但执行[executeAsync]时内部会抛出异常(无法获取执行图)。
     * 		注意: 内部捕获异常是[dinky]提供的jar处理的。
     */
    executor.executeSql(statement);
    let jobClient = executor.executeAsync("整库同步");
    print(jobClient.getJobID());
}
```



## 5 注意事项

### 5.1 打的jar内部的源码不一定是真实的代码

​	下面展示的代码不是真实的代码，是被混淆后的，需要自行从dinky仓库fetch代码查看。

​	例如下列源码：https://github.com/DataLinkDC/dinky/blob/0.6.5/dlink-executor/src/main/java/com/dlink/trans/ddl/CreateCDCSourceOperation.java

![](/dinky jar包源码错误.png)

### 5.2 bi环境查看dinky日志方式

​	注意：添加的package路径必须是logger所在的父类（有成员属性为: logger）的路径。

![](/父类logger路径.png)

![](/bi查看dinky指定类日志.png)



### 5.3 bi环境数据库连接注意事项

​		dinky内部数据库连接方法不适合bi环境使用, 需要**重写其内部源代码**，更改获取数据库连接方式。

![](/重写数据库connect方法.png)

​	bi 环境测试数据库连接代码：

```ts
/**
 * [dinky]提供的[cdc source]进行整库同步数据库连接注意事项: 
 * - 内部会自行解析SQL, 获取来源和目标库连接信息;
 * - 连接内部采用的是JDK自带的[java.sql.DriverManager]类提供的[getConnection]方法;
 * - 在部分环境上不能直接通过[Class.forName]来加载驱动, [Class.forName]加载驱动是按当前环境所配置驱动顺序加载的,
 * 若环境存在多个数据库驱动, 加载的驱动可能不是指定的;
 * - 为了保证加载驱动的正确性, 修改[dinky]对应[com.dlink.metadata.driver.AbstractJdbcDriver]中的[connect]方法,
 * 将其数据库连接方式改成: 反射方式连接。
 */
function connectDriver(): void {
    // @ts-ignore
    let conn: Connection = null;
    try {
        let driverClass = java.lang.Class.forName("com.mysql.cj.jdbc.Driver");
        let constru = driverClass.getConstructor();
        let driver = constru.newInstance();
        let method = driverClass.getMethod("connect", java.lang.String.class, java.util.Properties.class);
        let property = new java.util.Properties();
        property.put("user", "root");
        property.put("password", "123456");
        conn = method.invoke(driver, "jdbc:mysql://192.168.144.129:3306/test1", property);
        print(conn)
    } catch (e) {
        print(e);
    } finally {
        conn && conn.close();
    }
}
```





# 三 MySQL查看整库同步状态

## 1 查看当前主库正在使用的连接数

```
命令：show processlist;
如果是root帐号，你能看到所有用户的当前连接。如果是其它普通帐号，只能看到自己占用的连接。
show processlist命令只列出前100条，如果想全列出请使用: show full processlist;
mysql> show processlist;
```

![](/MySQL主库正在连接数.png)

- MySQL主从同步的实现中，从库连接到主库，并向主库发送一个COM_BINLOG_DUMP命令，主库会启动一个专门的线程为其服务，也就是Binglog Dump线程。 ...
- Binglog Dump线程按照从库请求的binlog名字和pos找到对应的binlog文件，然后**读取binlog的envent不断的发往从库**，当主库处于空闲状态时，binlog dump线程会在一个信号量（update_cond即主库的binlog更新状态）上等待。
- 在主库端一旦有新的日志产生后，立刻会发送一次广播，Binglog Dump线程在收到广播后，则会读取二进制日志并通过网络向备库传输日志，所以这是一个主库向备库不断推送的过程。
- 



# 四 踩坑

## 1  mysql cdc source -> map -> (filter -> flat white

​	提交的cdc source SQL缺失Sink： 'sink.table-name' = '${tableName}',

​	参考：https://github.com/DataLinkDC/dinky/discussions/1714



## 2 register mbean error

```
com.alibaba.druid.stat.DruidDataSourceStatManager.addDataSource(DruidDataSourceStatManager.java:154): register mbean error
javax.management.MalformedObjectNameException: Invalid character '=' in value part of property
```



## 3 not find class MysqlJsonDebeziumDeserializationSchema

​	解决办法：flink平台和应用环境都需要放置对于的jar包，并且版本一致。

​	提示：此类是dinky 0.7.2 版本需要的，0.7.2版本依赖的flink版本必须是1.14.6以上，否则会导致很多jar依赖存在问题，比较麻烦，建议：使用dinky 0.6.5版本。



## 4 Cannot instantiate user function.

​	此异常是dinky 0.7.2 版本内部jar包版本不一致导致的，跟flink版本也有关系，通过网上资料搜索得到需要flink换到1.14.6以上，此处没有测试，不一定能通过，建议使用dinky 0.6.5 版本。

```
org.apache.flink.streaming.runtime.tasks.StreamTaskException: Cannot instantiate user function.
	at org.apache.flink.streaming.api.graph.StreamConfig.getStreamOperatorFactory(StreamConfig.java:338)
	at org.apache.flink.streaming.runtime.tasks.OperatorChain.<init>(OperatorChain.java:159)
	at org.apache.flink.streaming.runtime.tasks.StreamTask.executeRestore(StreamTask.java:551)
	at org.apache.flink.streaming.runtime.tasks.StreamTask.runWithCleanUpOnFail(StreamTask.java:650)
	at org.apache.flink.streaming.runtime.tasks.StreamTask.restore(StreamTask.java:540)
	at org.apache.flink.runtime.taskmanager.Task.doRun(Task.java:759)
	at org.apache.flink.runtime.taskmanager.Task.run(Task.java:566)
	at java.base/java.lang.Thread.run(Thread.java:834)
Caused by: java.io.InvalidClassException: org.apache.flink.streaming.api.operators.AbstractStreamOperatorFactory; local class incompatible: stream classdesc serialVersionUID = -5116106217318483983, local class serialVersionUID = 9054245229149847006
	at java.base/java.io.ObjectStreamClass.initNonProxy(ObjectStreamClass.java:689)
	at java.base/java.io.ObjectInputStream.readNonProxyDesc(ObjectInputStream.java:1903)
	at java.base/java.io.ObjectInputStream.readClassDesc(ObjectInputStream.java:1772)
	at java.base/java.io.ObjectInputStream.readNonProxyDesc(ObjectInputStream.java:1903)
	at java.base/java.io.ObjectInputStream.readClassDesc(ObjectInputStream.java:1772)
	at java.base/java.io.ObjectInputStream.readOrdinaryObject(ObjectInputStream.java:2060)
	at java.base/java.io.ObjectInputStream.readObject0(ObjectInputStream.java:1594)
	at java.base/java.io.ObjectInputStream.readObject(ObjectInputStream.java:430)
	at org.apache.flink.util.InstantiationUtil.deserializeObject(InstantiationUtil.java:615)
	at org.apache.flink.util.InstantiationUtil.deserializeObject(InstantiationUtil.java:600)
	at org.apache.flink.util.InstantiationUtil.deserializeObject(InstantiationUtil.java:587)
	at org.apache.flink.util.InstantiationUtil.readObjectFromConfig(InstantiationUtil.java:541)
	at org.apache.flink.streaming.api.graph.StreamConfig.getStreamOperatorFactory(StreamConfig.java:322)
	... 7 more
```



## 5 addSink - > Temporary table xxx already exists

org.apache.flink.table.api.ValidationException: Temporary table '`default_catalog`.`default_database`.`VIEW_test1_DOCTOR`' already exists。

![](/临时视图表已存在.png)

​	此异常属于产品内部**固定好抛出**的异常，若是待需创建的视图表已经存在则将异常抛出，不过此异常不影响正常执行。

![](/dinky添加sink.png)

![](/视图已存在固定抛出异常.png)

## 6 The MySQL server has a timezone offset

​		时区相关问题，比如说时间显示错误、时区不是东八区、程序取得的时间和数据库存储的时间不一致等等问题。其实，这些问题都与数据库时区设置有关，本篇文章将从数据库参数入手，逐步介绍时区相关内容；

​		**来源库和目标库都需要将时区改成中国大陆**。

```
2023-08-15 16:48:25
org.apache.flink.util.FlinkException: Global failure triggered by OperatorCoordinator for 'Source: TableSourceScan(table=[[default_catalog, default_database, MyTable1]], fields=[id, account, password, name]) -> DropUpdateBefore -> NotNullEnforcer(fields=[id]) -> Sink: Sink(table=[default_catalog.default_database.MyTable2], fields=[id, account, password, name])' (operator cbc357ccb763df2852fee8c4fc7d55f2).
	at org.apache.flink.runtime.operators.coordination.OperatorCoordinatorHolder$LazyInitializedCoordinatorContext.failJob(OperatorCoordinatorHolder.java:553)
	at org.apache.flink.runtime.operators.coordination.RecreateOnResetOperatorCoordinator$QuiesceableContext.failJob(RecreateOnResetOperatorCoordinator.java:223)
	at org.apache.flink.runtime.source.coordinator.SourceCoordinatorContext.failJob(SourceCoordinatorContext.java:285)
	at org.apache.flink.runtime.source.coordinator.SourceCoordinator.start(SourceCoordinator.java:133)
	at org.apache.flink.runtime.operators.coordination.RecreateOnResetOperatorCoordinator$DeferrableCoordinator.resetAndStart(RecreateOnResetOperatorCoordinator.java:381)
	at org.apache.flink.runtime.operators.coordination.RecreateOnResetOperatorCoordinator.lambda$resetToCheckpoint$6(RecreateOnResetOperatorCoordinator.java:136)
	at java.base/java.util.concurrent.CompletableFuture$UniRun.tryFire(CompletableFuture.java:783)
	at java.base/java.util.concurrent.CompletableFuture.postComplete(CompletableFuture.java:506)
	at java.base/java.util.concurrent.CompletableFuture.complete(CompletableFuture.java:2073)
	at org.apache.flink.runtime.operators.coordination.ComponentClosingUtils.lambda$closeAsyncWithTimeout$0(ComponentClosingUtils.java:71)
	at java.base/java.lang.Thread.run(Thread.java:834)
Caused by: org.apache.flink.table.api.ValidationException: The MySQL server has a timezone offset (0 seconds ahead of UTC) which does not match the configured timezone Asia/Shanghai. Specify the right server-time-zone to avoid inconsistencies for time-related fields.
	at com.ververica.cdc.connectors.mysql.MySqlValidator.checkTimeZone(MySqlValidator.java:191)
	at com.ververica.cdc.connectors.mysql.MySqlValidator.validate(MySqlValidator.java:81)
	at com.ververica.cdc.connectors.mysql.source.MySqlSource.createEnumerator(MySqlSource.java:170)
	at org.apache.flink.runtime.source.coordinator.SourceCoordinator.start(SourceCoordinator.java:129)
	... 7 more		
```

​	解决办法：更改数据库时区，若是docker部署的数据库，最好删除已部署的容器，重新部署。

​	注意：docker部署的数据库，不能通过set语句修改时区，必须要直接更改容器。

```cmd
[root@localhost /]# sudo docker run -p 3306:3306 --name mysql \
-v /usr/local/docker/mysql/mysql-files:/var/lib/mysql-files \
-v /usr/local/docker/mysql/conf:/etc/mysql \
-v /usr/local/docker/mysql/logs:/var/log/mysql \
-v /usr/local/docker/mysql/data:/var/lib/mysql \
-e TZ=Asia/Shanghai  \
-e MYSQL_ROOT_PASSWORD=root  \
-d mysql:8.0.20 \
--default-time_zone='+8:00' 
```

​		重新部署后可查看数据库的时区，若system_time_zone的结果是：CST，则正确。

```
show variables like '%time_zone%'
```

​		更改时区后，还需要在数据库内执行： set global time_zone='+8:00'; 





# end 

