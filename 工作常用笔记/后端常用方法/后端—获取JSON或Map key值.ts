
import JavaHashMap = java.util.HashMap;
import JavaIterator = java.util.Iterator;
import JavaMap = java.util.Map;

/**
 * 批处理脚本模版
 */
function main() {

    let map = new JavaHashMap();
    map.put("1", "张三");
    map.put("2", "李四");
    map.put("3", "王五");
    map.put("4", "赵六");
    print(getMapOrJsonKeys(map));

    // let a = ["", "1"].filter(item =>{
    //     return !isEmpty(item);
    // })
    // print(a);
}

/**
 * 获取JSON或者Map的key
 * @param dealData JSON 或者 JAVA hashmap对象
 * @return 
 * 
 * @description 前端传递后端的参数都被JAVA转换过, 其JSON对象实际类型为: HashMap
 */
export function getMapOrJsonKeys(dealData: JSONObject | java.util.HashMap): string[] {
    if (!dealData) {
        return [];
    }
    let keys: string[] = [];
    if (typeof dealData == 'object') {
        try {
            keys = Object.keys(dealData);
        } catch (e) {
            console.error(`当前数据类型不为JSON`);
            console.error(e);
        }
        if (dealData.getClass != undefined) {
            let javaType: string = dealData.getClass();
            if (javaType == "class java.util.HashMap") {
                console.info(`当前类型为: ${javaType}`);
                let iterator = dealData.entrySet().iterator();
                while (iterator.hasNext()) {
                    let next = iterator.next();
                    keys.push(next.getKey());
                }
            } else {
                console.info(`当前数据类型即不是: JSON, 也不是java.util.HashMap, 无法获取其key值`);
            }
        }
    }
    return keys;
}
