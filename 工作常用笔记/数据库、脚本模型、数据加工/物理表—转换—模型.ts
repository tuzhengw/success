/**
 * 作者：tuzw
 * 审核人员：liuyz
 * 创建日期：2022-10-14
 * 脚本用途：将数据源中的记录的物理表转换为模型
 * 地址：https://jira.succez.com/browse/CSTM-20740
 */
import { SDILog, LogType, isEmpty } from "/sysdata/app/zt.app/commons/commons.action";
import dw from "svr-api/dw";
import db from "svr-api/db";
import metadata from "svr-api/metadata";

let logger = new SDILog({ name: "custom.xml" });

function main() {
    convertPhyTableToModel();
}

/**
 * 将物理表转换为模型
 */
function convertPhyTableToModel(): void {
    let changeRegisterPhyTableToModel = new ChangeRegisterPhyTableToModel({
        dbSouceName: "SJZX_HLHT", // 物理表所在数据源
        schema: "SJZX_HLHT",
        targetFolder: "/HLHT/data/tables/xml/registerTable",
        projectName: "HLHT"
    });
    changeRegisterPhyTableToModel.execute();
}

/**
 * 互联互通将登记的物理表转换为模型功能
 */
export class ChangeRegisterPhyTableToModel {

    /** 数据源名 */
    private dbSouceName: string;
    /** 转换物理表所在schema名 */
    private schemaName: string;
    /** 当前操作模块，默认：sys，eg：sdi */
    private projectName: string;
    /** 转换的模型所存根目录地址 */
    private targetFolder: string;

    constructor(args: {
        dbSouceName: string,
        schema: string,
        targetFolder: string,
        projectName?: string
    }) {
        this.dbSouceName = args.dbSouceName;
        this.schemaName = args.schema;
        this.targetFolder = args.targetFolder;
        this.projectName = !isEmpty(args.projectName) ? args.projectName as string : "sys";
    }

    /**
     * 转换入口
     */
    public execute(): void {
        logger.setName("sdi.xml.ChangeRegisterPhyTableToModel.execute");
        logger.addLog(`开始执行物理表转换为模型`);
        let startTime: number = Date.now();
        let dealPhyTableInfos: DealPhyTableInfos[] = this.queryDealPhyTableNInfos();
        let saveNeedConvertPhyInfo = this.formatPhyTableInfos(dealPhyTableInfos) as JSONObject;
        if (isEmpty(saveNeedConvertPhyInfo)) {
            return;
        }
        try {
            let dealParentNames: string[] = Object.keys(saveNeedConvertPhyInfo);
            for (let i = 0; i < dealParentNames.length; i++) {
                let parentName: string = dealParentNames[i];
                let convertPhyTableInfo: ConvertModelTableParam = saveNeedConvertPhyInfo[parentName];
                let needDealTables: ConvertPhyTableInfo[] = convertPhyTableInfo.tables;
                logger.addLog(`开始将目录:[${parentName}]下[${needDealTables.length}]个物理表 转换为模型`);
                if (needDealTables.length == 0) {
                    logger.addLog(`目录下:[${parentName}]没有需要转换的物理表信息`);
                    continue;
                }
                metadata.importDWModelFromDbTables(convertPhyTableInfo);
            }
        } catch (e) {
            logger.addLog(`物理表转换为模型执行失败---error`);
            logger.addLog(e);
        }
        logger.addLog(`执行结束物理表转换为模型, 耗时: [${(Date.now() - startTime) / 1000} s]`);
        logger.fireEvent();
    }

    /**
     * 格式化需要转换模型的物理表信息
     * @param DealPhyTableInfos 处理的物理表信息
     * @return  记录需要转换的物理表信息（根据模块分类批次转换），eg: {
     *     "产后访视服务信息": {
     *          projectName: "",
     *          tables: [{...}], // 转换的物理表信息
     *          schema?: "",
     *          targetFolder: "",
     *          connectionType: "",
     *          schedule?: ""
     *      }
     * }
     */
    private formatPhyTableInfos(dealPhyTableInfos: DealPhyTableInfos[]): JSONObject | void {
        logger.addLog(`开始执行【formatPhyTableInfos】, 格式化需要转换模型的物理表信息`);
        let saveNeedConvertPhyInfo: JSONObject = {};
        let dbSouce: Datasource = db.getDataSource(this.dbSouceName);
        if (dbSouce == null) {
            logger.addLog(`-数据源:[${this.dbSouceName}] 不存在, 执行结束`);
            return;
        }
        for (let i = 0; i < dealPhyTableInfos.length; i++) {
            let tableInfo: DealPhyTableInfos = dealPhyTableInfos[i];
            let phyTableName: string = tableInfo.phyTableName;
            let tableExist: boolean = dbSouce.isTableExists(`${this.schemaName}.${phyTableName}`);
            if (!tableExist) {
                logger.addLog(`-物理表名:[${phyTableName}]不存在, 越过`);
                continue;
            }
            let parentFoldName: string = tableInfo.parentFoldName;
            let tableDesc: string = tableInfo.tableDesc;
            let saveParentFoldPath: string = `${this.targetFolder}/${parentFoldName}`;
            if (metadata.getFolder(saveParentFoldPath) == null) { // 若所存父目录不存在，则创建
                try {
                    metadata.remove(saveParentFoldPath); // 产品问题，文件存在，但页面不显示，这里不管存不存在，都先删除，再创建
                } catch (e) { }
                metadata.mkdirs(saveParentFoldPath);
            }
            /** 物理表转换模型所在绝对路径 */
            let convertModelPath: string = `${saveParentFoldPath}/${phyTableName}.tbl`;
            if (metadata.getFile(convertModelPath) != null) { // 若模型已经被转换过，将已有的删除
                metadata.remove(convertModelPath);
            }
            if (!saveNeedConvertPhyInfo[parentFoldName]) {
                saveNeedConvertPhyInfo[parentFoldName] = {
                    projectName: this.projectName,
                    tables: [],
                    targetDatasource: this.dbSouceName,
                    schema: this.schemaName,
                    targetFolder: `${this.targetFolder}/${parentFoldName}`,
                    connectionType: ConnectionType.Readwrite
                } as ConvertModelTableParam;
            }
            saveNeedConvertPhyInfo[parentFoldName].tables.push({
                datasource: this.dbSouceName,
                schema: this.schemaName,
                dbTable: phyTableName,
                name: phyTableName,
                desc: tableDesc
            } as ConvertPhyTableInfo);
        }
        logger.addLog(`执行结束【formatPhyTableInfos】, 格式化需要转换模型的物理表信息`);
        return saveNeedConvertPhyInfo;
    }

    /**
     * 查询需要转换的物理表
     */
    private queryDealPhyTableNInfos(): DealPhyTableInfos[] {
        let dealPhyTableInfos: DealPhyTableInfos[] = [];
        let query: QueryInfo = {
            sources: [{
                id: "model1",
                path: "/HLHT/data/tables/xml/ETL/获取数据元中所有表名.tbl"
            }],
            fields: [{
                name: "LY", exp: "model1.LY"
            }, {
                name: "MK", exp: "model1.MK"
            }, {
                name: "MS", exp: "model1.MS"
            }]
        }
        try {
            let queryResult = dw.queryData(query).data as string[][];
            for (let i = 0; i < queryResult.length; i++) {
                dealPhyTableInfos.push({
                    phyTableName: queryResult[i][0],
                    parentFoldName: queryResult[i][1],
                    tableDesc: queryResult[i][2]
                });
            }
        } catch (e) {
            logger.addLog(`查询需要转换的物理表失败---error`);
            logger.addLog(e);
        }
        logger.addLog(`当前需要处理物理表个数:[${dealPhyTableInfos.length}]`);
        return dealPhyTableInfos;
    }
}

/** 转换模型后的链接类型 */
enum ConnectionType {
    /** 实时连接（只读模式，用于实时数据分析） */
    Readonly = "readonly",
    /** 实时连接（读写模式，系统可能会更新数据或者表结构） */
    Readwrite = "readwrite",
    /** 提取数据（只读模式，将业务系统数据提取到指定数据源，默认为分析主数据库） */
    Extract = "extract"
}

/** 转换模型的物理表信息 */
interface ConvertPhyTableInfo {
    /** 来源数据表所在的数据源 */
    datasource: string,
    /** 来源数据表所在的schema */
    schema: string,
    /** 来源数据表的表名 */
    dbTable: string,
    /** 导入后的模型的名称 */
    name?: string,
    /** 导入后模型的描述信息 */
    desc?: string
}

/** 转换模型表配置参数  */
interface ConvertModelTableParam {
    /** 项目模块名，eg: sys、sdi */
    projectName: string;
    /** 转换模型的物理表信息 */
    tables: ConvertPhyTableInfo[];
    /** 转换模型所在数据源名 */
    targetDatasource: string;
    schema?: string;
    /** 转换模型所在位置（目录必须存在，否则导入失败） */
    targetFolder: string;
    /** 转换模型后的链接类型 */
    connectionType: ConnectionType;
    schedule?: string;
}

/** 数据元查询映射表  */
interface DealPhyTableInfos {
    /** 物理表名 */
    phyTableName: string;
    /** 父目录名 */
    parentFoldName: string;
    /** 表描述 */
    tableDesc: string;
}