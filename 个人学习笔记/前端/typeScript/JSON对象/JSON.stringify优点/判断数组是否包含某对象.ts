/**
 *  判断数组是否包含某对象，或者判断对象是否相等
 */
function main() {
    var data = [
            {name: 'echo'},
            {name: '听风是风'},
            {name: '天子笑'},
        ];
    var val = {
        name: '天子笑'
    }
    // true
    console.log(JSON.stringify(data).indexOf(JSON.stringify(val)) !== -1);
    // false
    console.log(data.indexOf(val) !== -1)
}