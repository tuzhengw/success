
import { queryData, getDwTable } from "svr-api/dw";
import metadata from 'svr-api/metadata';
import { getDataSource } from "svr-api/db"; //数据库相关API

/**
 * 获取某模型表【查询的字段】信息
 * 
 * @description 通过dw模块获取
 * @param modelPath 模型表路径
 * @param dbPhysicalFields 【记录】模型【物理字段名】数组，用于数组转换为JSON
 * @param dbPirmaryKey 【记录】表物理名主键
 * @param ignoredFields 忽略的【字段名】数组
 * @return 查询字段格式，eg：
 * [{ 
 *      name: "企业名称", 
 *      exp: "model1.ENTERPRISE_NAME" 
 * }]
 */
export function getQueryModelFileds(modelPath: string, dbPhysicalFields?: Array<string>, dbPirmaryKey?: Array<string>, ignoredFields?: Array<string>): QueryFieldInfo[] {
    if (!modelPath) {
        console.debug(`getTableFieldInfo()，获取模型【${modelPath}】物理表字段失败，原因：modelPath参数错误`);
        return [];
    }
    let modelMetaInfo: string = metadata.getString(modelPath);
    if (!modelMetaInfo) {
        return []
    }
    let modelInfos: DwTableInfo = JSON.parse(modelMetaInfo);
    let modelProperties: DwTablePropertiesInfo = modelInfos.properties;
    let modelPrimary: string[] = modelProperties.primaryKeys; // class java.util.ArrayList
    if (!modelPrimary) {
        print(`-- -【${modelPath}】该模型没有主键，无法推送`);
        return [];
    }
    let dimensions: FieldInfo[] = modelInfos.dimensions as FieldInfo[];
    let queryFields: QueryFieldInfo[] = [];
    let recordFieldDescName: string[] = [];
    try {
        for (let i = 0; i < dimensions.length; i++) {
            let modelFieldInfo = dimensions[i];
            let dbfield: string = modelFieldInfo.dbfield;
            if (modelFieldInfo.name == '时间戳') {
                continue;
            }
            if (!!dbPirmaryKey && !!modelPrimary && modelPrimary.includes(modelFieldInfo.name)) {
                dbPirmaryKey.push(dbfield);
            }
            if (!!dbPhysicalFields) {
                dbPhysicalFields.push(dbfield);
            }
            let fieldDescName: string = modelFieldInfo.name;
            if (recordFieldDescName.includes(fieldDescName)) { // 校验描述是否重复
                fieldDescName = `${fieldDescName}_${i}`;
            }
            let queryField: QueryFieldInfo = {
                name: fieldDescName,
                exp: `model1.${dbfield}`
            }
            recordFieldDescName.push(fieldDescName);
            queryFields.push(queryField);
        }
    } catch (e) {
        print(`通过模型路径【${modelPath}】获取物理表字段信息失败--error`);
        print(e.toString());
    }
    return queryFields;
}

/** 字段信息 */
interface FieldInfo {
    /** 字段描述 */
    name: string;
    /** 字段类型 */
    dataType: string;
    /** 字段长度 */
    length: number;
    /** 是否维项 */
    isDimension: boolean;
    /** 字段物理名 */
    dbfield: string;
}