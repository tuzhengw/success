import db from "svr-api/db"; //数据库相关API
import utils from "svr-api/utils";
import sys from "svr-api/sys";
import security from "svr-api/security";

/**
 * 此脚本用于扩展系统的WebAPI功能。系统在`/api/...`路径上提供了丰富的webAPI，当某些个性化需求需要扩展心的API（如`/api/trustedapps/my-custom-api`）时，可以使用此脚本文件。
 * 
 * 1. 此钩子脚本全局唯一，在`sysdata`项目中，位于`/sysdata/settings/hooks/webapi.action`。
 * 2. 可以直接编辑`webapi.action`文件，也可以在SuccIDE或元数据资源管理界面中通过脚本编辑器编辑ts语法的脚本文件`webapi.action.ts`，编辑器会自动编译并生成`webapi.action`。
 */

/**
 * 当系统收到/api路径下面的请求时会优先执行系统内置的api，如果不存在系统内置的api，那么会调用这里的脚本函数。
 * @returns 返回的额内容将直接输出给客户端
 */
function onCustomWebAPI(request: HttpServletRequest, response: HttpServletResponse): any | void {
	let uri = request.getRequestURI();
	let contextPath = request.getContextPath();
	let index = uri && uri.indexOf(contextPath);
	if (index > -1) {
		uri = uri.substring(index + contextPath.length);
	}
	return doDZPHZInterface_YLJG(request);
}

const DS_NAME = 'default';
const SCHEMA = 'scdzphz';

/**
 * 是否输入控制台日志
 */
const IS_DEBUG = true;

/**
 * 失败日志等级代码
 */
const LOGLEVEL_FAIL = 'FAIL';

/**
 * 日志重要程度
 */
const LOGLEVEL_FAIL_IMPT = 'IMPT';

/**
 * 审核通过申请陪护接口地址
 */
const API_approveSQPH_URL = '/api/v1/approveSQPH';

/**
 * 获取病区列表接口地址
 */
const API_getBqInfo_URL = '/api/v1/listWardInfo';

/**
 * 申请表
 */
const TABLE_SQPHZ = 'FACT_DZPHZSQB';

/**
 * 申请操作表
 */
const TABLE_SQCZB = 'FACT_SQCZB';

let ds = db.getDataSource(DS_NAME);

/**
 * 医疗机构调用相关接口
 * 用于查询，审核，打回等操作
 */
export function doDZPHZInterface_YLJG(request): ResultInfo {
	IS_DEBUG && print(`--doDZPHZInterface_YLJG--,医疗机构调用相关接口-------------start`);
	let uri = request.getRequestURI();
	let contextPath = request.getContextPath();
	let index = uri && uri.indexOf(contextPath);
	if (index > -1) {
		uri = uri.substring(index + contextPath.length);
	}
	let resultInfo: ResultInfo = { result: true };
	let params;
	try {
		params = getParams(request);
	} catch (e) {
		print(e);
		resultInfo = {
			result: false,
			errorCode: `invalidParams`,
			message: `请求参数解析失败，必须是JSON参数，请检查参数格式！`
		}
	}
	if (!resultInfo.result) {
		return resultInfo;
	}
	if (!params) {
		params = {};
	}
	if (!params) {
		IS_DEBUG && print(`--doDZPHZInterface_YLJG--,没有参数！`);
		let event: any = {
			name: 'doDZPHZInterface_YLJG',
			logLevel: LOGLEVEL_FAIL_IMPT,
			detailInfo: {
				params: params,
				result: resultInfo
			}
		}
		resultInfo = {
			result: false,
			errorCode: 'noparams',
			message: `没有任何参数！${uri}`
		}
		// sys.fireEvent(event);
	} else {
		if (uri === API_approveSQPH_URL) {
			resultInfo = approveSQPH(params);
		}
		else{
			if (uri === API_getBqInfo_URL){
				resultInfo = getBqInfo(params);
			}
			else {
				resultInfo.result = false;
				resultInfo.errorCode = 'invalidUrl';
				resultInfo.message = `无法识别的接口：${uri}`;
			}
		}
		
	}
	IS_DEBUG && print(`--doHSInterface_JCJG--,医疗机构调用相关接口-------------end`);
	return resultInfo;
}

/**
 * 审核通过申请陪护
 */
function approveSQPH(params) {
	IS_DEBUG && print(`--approveSQPH--,审核通过申请陪护-------------start`);
	let start_time = new Date();
	IS_DEBUG && print(`--approveSQPH--,开始时间：${start_time.toLocaleDateString() + ' ' + start_time.toLocaleTimeString()}`);
	let appId = params[APP_ID];
	let appSecret = params[APP_SECRET];
	let resultInfo: ResultInfo = { result: true, message: '操作成功' };
	let event: any = {
		name: 'trustedapp.approveSQPH',
		logLevel: LOGLEVEL_FAIL_IMPT,
		option1: appId,
		detailInfo: {
			appId: appId,
			result: resultInfo
		}
	}
	// let appSecret = params[APP_SECRET];
	let appInfo = getAppInfo(appId);
	let result = checkApiWhiteList(appInfo, appSecret);
	if (!result.result) {
		IS_DEBUG && print(`--approveSQPH--,没有权限：${JSON.stringify(result)}`);
		resultInfo = result;
		event.operationState = LOGLEVEL_FAIL;
		event.detailInfo.result = resultInfo;
	} else {
		let sqid = params["SQID"];
		event.detailInfo['sqid'] = sqid;
		if (!sqid) {
			resultInfo = {
				result: false,
				errorCode: 'missParam',
				message: `缺失参数SQID`
			}
			event.operationState = LOGLEVEL_FAIL;
			event.detailInfo.result = resultInfo;
		} else {
			let conn = ds.getConnection();
			try {
				conn.setAutoCommit(false);
				let table_sq = ds.openTableData(TABLE_SQPHZ, SCHEMA, conn);
				let result_sq = table_sq.selectRows(['SQZT'], { SQID: sqid });
				if (!result_sq || result_sq.length == 0) {
					resultInfo.message = `操作失败！该申请ID【${sqid}】对应的申请不存在，请检查SQID是否正确`
				} else {
					let zt = result_sq[0][0];
					if (zt != 1) {
						//todo 考虑状态不是待审核的情况，比如用户已取消，或者其他护士已经审核通过了,提示明确信息
						resultInfo.result = false;
						resultInfo.errorCode = 'notToAudit';
						resultInfo.message = `操作失败！该申请ID【${sqid}】对应的申请状态不是待审核`;
					} else {
						//更新申请表的状态，并往申请操作表增加一条记录
						table_sq.update({ SQZT: 3 }, { SQID: sqid })
						let table = ds.openTableData(TABLE_SQCZB, SCHEMA, conn);
						let jgid = appInfo.jgid;
						//往操作表插入一条数据
						table.insert({
							SQID: sqid,
							SHYY: jgid,
							SQCZ: 3,//通过
							CZSJ: getNowTime()
						});
						conn.commit();
					}
				}

			} catch (e) {
				conn && conn.rollback();
				resultInfo.result = false;
				resultInfo.errorCode = 'systemError';
				resultInfo.message = `系统错误，请联系管理员！`;
				resultInfo.detailMessage = e;
				event.operationState = LOGLEVEL_FAIL;
				event.detailInfo.result = resultInfo;
				let exception = getBaseException(e);
				event.exception = exception;
				print(exception);
			}
			finally {
				conn && conn.close();
			}
		}
	}
	// sys.fireEvent(event);
	IS_DEBUG && print(`---- ${JSON.stringify(resultInfo)}`);
	let end_time = new Date();
	IS_DEBUG && print(`--approveSQPH--,结束时间：${end_time.toLocaleDateString() + ' ' + end_time.toLocaleTimeString()}`);
	let spendTime = (end_time.getTime() - start_time.getTime());
	IS_DEBUG && print(`--approveSQPH--,花费时间：${spendTime / 1000}秒`);
	IS_DEBUG && print(`--approveSQPH--,审核通过申请陪护-------------end`);
	if (resultInfo.result) {
		return {
			result: true,
			message: resultInfo.message
		}
	} else {
		return resultInfo;
	}
}

let Timestamp;
function getNowTime() {
	if (!Timestamp) {
		Timestamp = java.sql.Timestamp;
		// Timestamp = java.type('java.sql.Timestamp');
	}
	return new Timestamp(new Date().getTime());
}

/**
 * 获取请求中的参数
 * @param request
 */
function getParams(request: HttpServletRequest) {
	let params = request.getRequestBody();
	IS_DEBUG && print(`--获取请求参数：`);
	// print(params);
	if (!params || params == "" || params == null) {
		return null;
	} else {
		if (typeof params === "string") {
			IS_DEBUG && print(`--是string：`);
			return JSON.parse(params);
		} else {
			return params;
		}
	}
}

function getBaseException(args) {
	return new com.succez.exception.business.BaseException(args);
}


/**
 * 返回结果格式
 */
interface ResultInfo {
    /**
     * 结果
     */
	result: string | boolean;

    /**
     * 错误码
     */
	errorCode?: string;

    /**
     * 错误提示
     */
	message?: string;

    /**
     * 返回数据
     */
	data?: any;

	detailMessage?: any;

	errorData?: any;
}

const APP_ID = 'app_id';
const APP_SECRET = 'app_secret';
const TABLE_TRUSTED_APPS = 'SZSYS_4_TRUSTED_APPS';
const ERROR_APP_NOT_FOUND = 'appNotFound';
const ERROR_SECRET_MISMATCH = 'secretMismatch';
const ERROR_PERMISSION_DENIED = 'permissionDenied';
const MESSAGE_APP_NOT_FOUND = 'app_id在系统中不存在。正式环境的app_id见卫健委下发的文件，测试环境的app_id见在线API接口文档。';
const MESSAGE_SECRET_MISMATCH = 'app_secret错误，正式环境请从参照卫健委下发的文件，测试环境见在线API接口文档';
const MESSAGE_PERMISSION_DENIED = '无权限调用';

var defaultDs = db.getDefaultDataSource();

/**
 * 同{@link TrustedApp}
 */
interface AppInfo {
	appId: string;
	appName?: string;
	appSecret: string;
	group?: string;
	enabled: boolean;
	type: 'code' | 'auto' | 'ip' | 'cert';
	ip?: string[];
	domain?: string[];
	apis?: string[];
	depts?: string[];
	label?: string;
	jgid: string;
	call_num?: number;
	last_call_time?: Date;
}

/**
 * 判断一个应用是否有权限访问一项API
 *
 *  <p>这个方法会判断进行如下判断：<ol>
 * <li> app 是否存在/可用</li>
 * <li> 应用秘钥是否正确</li>
 * <li>api白名单中是否存在被调用的 api</li>
 * </ol>
 * @param appInfo 授信应用
 * @param appSecret 授信应用秘钥
 * @param api api的完整路径，比如 /api/oauth2/verifyUser
 * @return 返回校验结果， 包括 errorCode 错误码，以及 message 错误文本提示（国际化文本）
 */
function checkApiWhiteList(appInfo: AppInfo, appSecret: string, api?: string): ResultInfo {
	let type = appInfo && appInfo.type;
	if (appInfo == null || !appInfo.enabled || (type != 'code' && type != 'auto' && type != 'ip')) {
		return { result: false, errorCode: ERROR_APP_NOT_FOUND, message: MESSAGE_APP_NOT_FOUND };
	}

	if (appInfo.appSecret != appSecret) {
		return { result: false, errorCode: ERROR_SECRET_MISMATCH, message: MESSAGE_SECRET_MISMATCH };
	}

	if (appInfo.apis.indexOf(api) == -1) {
		//暂时去掉API的判断 
		// return { result: false, errorCode: ERROR_PERMISSION_DENIED, message: MESSAGE_PERMISSION_DENIED };
	}
	if (!appInfo.jgid) {
		return { result: false, errorCode: ERROR_PERMISSION_DENIED, message: MESSAGE_PERMISSION_DENIED };
	}

	return { result: true, data: appInfo };
}

/**
 * 根据appId获取对应应用信息，没有则返回null
 */
function getAppInfo(appId: string): AppInfo {
	let tableData = defaultDs.openTableData(TABLE_TRUSTED_APPS);
	let rs = tableData.selectFirst('*', { 'APP_ID': appId });
	if (rs == null || rs == {}) {
		return null;
	}
	let appInfo: AppInfo = {
		appId: rs.APP_ID,
		appName: rs.APP_NAME,
		appSecret: rs.APP_SECRET,
		type: rs.TYPE,
		group: rs.APP_GROUP,
		enabled: parseInt(rs.ENABLED) == 1,
		ip: split(rs.IP),
		domain: split(rs.DOMAIN),
		apis: split(rs.API_WHITE_LIST),
		depts: split(rs.DEPTS),
		label: rs.LABEL,
		jgid: rs.JGID,
		call_num: rs.CALL_NUM,
		last_call_time: rs.LAST_CALL_TIME || rs.last_call_time
	}
	return appInfo;
}

function split(s: string): string[] {
	if (s == null || s == '') {
		return [];
	}
	return s.split(',');
}


// 病区表
const BZTABLEDATE = "dim_bq";
/**
 * 查询医院病区列表的接口
 * @parm params（JSON格式）：包含机构ID
 * @return { "result": 是否执行成功, "data": 查询结果集};
 * <pre>
 * url：https://app.scwjxx.cn/api/v1/listWardInfo
 *  {
		"app_id": "test_succez_0420",
		"app_secret": "test_succez_0420_secret",
		"jgID": "510000000764"
	}
 * </pre>
 */
function getBqInfo(params): ResultInfo {
	IS_DEBUG && print(`--getBqInfo--，查询某个医院病区列表-------------start`);
	let start_time = new Date();
	IS_DEBUG && print(`--getBqInfo--，开始时间${start_time.toLocaleDateString() + '' + start_time.toLocaleTimeString}`)

	let appId = params[APP_ID];
	let appSecret = params[APP_SECRET];
	let resultInfo: ResultInfo = { result: true, message: '操作成功' };

	let event: any = {
		name: 'trustedapp.getBqInfo',
		LogLevel: LOGLEVEL_FAIL_IMPT,
		option1: appId,
		detailInfo: {
			appId: appId,
			result: resultInfo
		}
	}
	/**
	 * 1. 根据app_id获取对应App信息
	 * 2. 检查app对象
	 */
	let appInfo = getAppInfo(appId);
	let result = checkApiWhiteList(appInfo, appSecret);
	if (!result.result) {
		IS_DEBUG && print(`--getBqInfo--，没有权限：${JSON.stringify(result)}`);
		resultInfo = result;
		event.operationState = LOGLEVEL_FAIL;
		event.detailInfo.result = resultInfo;
	} else {
		/**
		 * 1. 获取机构ID，并作为新成员变量添加到detailInfo中
		 * 2. 判定机构ID是否为NULL
		 */
		let jgID = params["jgID"];
		event.detailInfo['jgid'] = jgID;
		if (!jgID) {
			resultInfo = {
				result: false,
				errorCode: 'missParam',
				message: `缺失参数jgID`
			}
			event.operationState = LOGLEVEL_FAIL;
			event.detailInfo.result = resultInfo;
		} else {
			let defaultData = db.getDataSource(DS_NAME);
			let bqTable = defaultData.openTableData(BZTABLEDATE);
			try {
				/**
				 * 1. 根据机构ID（jgID）获取病区表中当前机构的所有病区信息
				 * 2. 根据查询返回的结果，判断结果是否为：NULL或者不存在
				 */
				let result_sql = bqTable.select(["BQID", "BQMC", "QY"], { "SSJG": jgID });
				if (!result_sql || result_sql.length == 0) {
					resultInfo.message = `操作失败！该申请ID【${jgID}】不存在当前机构中`;
				} else {
					resultInfo.data = result_sql;
					event.operationState = "操作成功";
					event.detailInfo.result = resultInfo;
				}
			} catch (e) {
				resultInfo.result = false;
				resultInfo.errorCode = 'systemError';
				resultInfo.message = `系统错误，请联系管理员!`;
				resultInfo.detailMessage = e;
				event.operationStatel = LOGLEVEL_FAIL;
				event.detailInfo.result = resultInfo;
				let exception = getBaseException(e);
				event.exception = exception;
				print(exception);
			}
		}
	}
	IS_DEBUG && print(`---${JSON.stringify(resultInfo)}`);
	let end_time = new Date();
	IS_DEBUG && print(`--getBqInfo--，结束时间：${end_time.toLocaleDateString + ' ' + end_time.toLocaleTimeString}`)
	let spendTime = (end_time.getTime() - start_time.getTime());
	IS_DEBUG && print(`--getBqInfo--，花费时间：${spendTime / 1000}秒`);
	IS_DEBUG && print(`--getBqInfo--，查询完成的时间------------end`);
	return resultInfo;
}