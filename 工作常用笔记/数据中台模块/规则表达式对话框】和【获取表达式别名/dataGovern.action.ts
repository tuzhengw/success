
import dw from "svr-api/dw";
import db from "svr-api/db";
import metadata from "svr-api/metadata";
import utils from 'svr-api/utils';
import security from 'svr-api/security';
import util from "svr-api/utils";
import { getDbTableNameMaxLength, SDILog, LogType } from "/sysdata/app/zt.app/commons/commons.action";

/**最开始的目录，用于规定需要导入几个层级 */
const STARTPATH = '/sdi/data/tables/domains';
const RES_CAT = '/sysdata/data/tables/sdi/data-govern/dims/DIM_GOVERN_RES_CAT.tbl';
const CHECK_TASK_RES_TABLE = "SZSYS_4_DG_CHECK_TASK_TABLES";
const CHECK_TASK_TABLE = "SZSYS_4_DG_CHECK_TASKS";
const SCHEDULETASK = 'SZSYS_4_SCHEDULETASK';
const SCHEDULE_TABLE = "SZSYS_4_SCHEDULE";
const Timestamp = Java.type('java.sql.Timestamp');

/** 存储检测任务合法表根目录 */
const LEGAL_TABLE_ROOT_PATH: string = "/sdi/data/tables/sys/data-govern";

let logger: SDILog = new SDILog({ name: "sdi.dg" });

/**
 * 获取待治理表ID和对应的别名，提供给前端表达式对话框使用
 * @param params.checkTableType 获取检测表类型，默认获取检测规则表字段信息
 * 
 * 20220823 tuzw
 * 改进：新增数据标准检测模块，需根据checkTableType来获取对应的检测表字段信息
 * 地址：https://jira.succez.com/browse/CSTM-20164
 */
export function getRuleTableInfos(request: HttpServletRequest, response: HttpServletResponse, params: {
	checkTableType?: CheckTableType
	standardLibId?: string
}): string[][] {
	logger.setName("sdi.dataGovern.getRuleTableInfos");
	logger.addLog('start----【getRuleTableInfos】获取待治理表ID和对应的别名，提供给前端表达式对话框使用');
	logger.addLog(params);

	let checkTableType: CheckTableType = params.checkTableType as CheckTableType;
	if (!checkTableType) {
		checkTableType = CheckTableType.DataGovernCheck;
	}
	let checkTableInfos: string[][] = [];
	let ds = db.getDefaultDataSource();
	let querySQL: string = "";
	switch (checkTableType) {
		case "governCheck":
			querySQL = 'select t0.MAIN_TABLE_ID as MAIN_TABLE_ID,t0.ALIAS as BM,t0.RULES_LIB_NAME as RULES_LIB_NAME from SDI2.SZSYS_4_DG_RULES_LIB t0';
			checkTableInfos = ds.executeQueryRows(querySQL);
			break;
		case "standardCheck":
			let standardLibId: string = params.standardLibId as string;
			logger.addLog(`获取标准库：${standardLibId} 的检测表信息`);
			querySQL = `SELECT t1.STD_TABLE_ID,t1.STD_TABLE_NAME,t1.STD_TABLE_COMMENT FROM SDI2.SZSYS_4_DG_STD_TABLES t1 WHERE t1.STD_LIB_ID = ?`;
			checkTableInfos = ds.executeQueryRows(querySQL, [standardLibId]);
			break;
	}
	logger.addLog(checkTableInfos);
	logger.addLog('end----【getRuleTableInfos】获取待治理表ID和对应的别名，提供给前端表达式对话框使用执行结束');
	logger.fireEvent();
	return checkTableInfos;
}

/**
 * 获取指定模型的所有字段信息，返回给表达式对话框使用
 * @param checkTableType 检测表类型
 * @param tableId 表resid
 * @param isDimension 是否维项
 * @param standardTableId 标准表ID
 */
export function getFieldInfos(request: HttpServletRequest, response: HttpServletResponse, params: {
	checkTableType?: CheckTableType,
	tableId?: string,
	isDimension?: boolean,
	standardTableId?: string
}) {
	let checkTableType: CheckTableType = params.checkTableType as CheckTableType;
	if (!checkTableType) {
		checkTableType = CheckTableType.DataGovernCheck;
	}
	let fieldResultInfo;
	switch (checkTableType) {
		case "governCheck":
			let tableId: string = params.tableId as string;
			let isDimension: boolean = params.isDimension as boolean;
			let tableInfo = dw.getDwTable(tableId);
			if (isDimension) { // 拼接表的维度信息和度量信息
				fieldResultInfo = tableInfo.dimensions;
			} else {
				fieldResultInfo = tableInfo.measures
			}
			break;
		case "standardCheck":
			let standardTableId: string = params.standardTableId as string;
			let ds = db.getDefaultDataSource();
			fieldResultInfo = getStandardFieldInfo(ds, standardTableId).dimensions;
			break;
	}
	return fieldResultInfo;
}

/**
 * 获取所有检测字段的字段信息
 *
 * @param signTableResid 资源表ID值 | 标准表ID
 * @param tableAlias 资源表别名
 * @param checkTableType? 获取检测表类型，默认获取检测规则表字段信息
 * 
 * 20220706 tuzw
 * 由于一张表多次引入，加载全部字段信息，会混淆，导致重复表字段可能引用错乱
 * 解决办法：单表仅加载自己的字段信息
 * 地址：https://jira.succez.com/browse/CSTM-19484
 *
 * 20220823 tuzw
 * 改进：后端获取检测表所有字段信息，原仅获取数据检测的所有检测表字段信息，现在需要根据前端传入的checkTableType来获取对应的检测表字段信息
 * 地址：https://jira.succez.com/browse/CSTM-20164
 */
export function getCheckTableFieldInfos(request: HttpServletRequest, response: HttpServletResponse, params: {
	tableAlias?: string,
	signTableResid?: string,
	checkTableType?: CheckTableType
}) {
	logger.setName("sdi.dataGovern.getCheckTableFieldInfos");
	logger.addLog('start----【getCheckTableFieldInfos】获取所有检测字段的字段信息');
	print(params);

	let checkTableType: CheckTableType = params?.checkTableType as CheckTableType;
	if (!checkTableType) {
		checkTableType = CheckTableType.DataGovernCheck;
	}
	let checkTableInfos: JSONObject = {};
	let signTableResid: string = params?.signTableResid as string;
	if (!!signTableResid) { // 校验是否获取指定表的字段信息
		logger.addLog(`获取指定表：${signTableResid} 表的字段信息`);
		let tableInfo = dw.getDwTable(signTableResid);
		let tableAlias: string = params?.tableAlias as string;
		switch (checkTableType) {
			case "governCheck":
				checkTableInfos[tableAlias] = { dimensions: tableInfo.dimensions, measures: tableInfo.measures };
				break;
			case "standardCheck":
				let ds = db.getDefaultDataSource();
				checkTableInfos[tableAlias] = getStandardFieldInfo(ds, signTableResid);
				break;
		}
	} else {
		logger.addLog(`获取所有表的检测字段信息`);
		switch (checkTableType) {
			case "governCheck": checkTableInfos = getDgAllCheckTableInfos();
				break;
			case "standardCheck": checkTableInfos = getStdAllCheckTableInfos();
				break;
			default:
				logger.addLog(`获取检测表类型不存在：${checkTableType}`);
		}
	}
	logger.addLog('end----【getCheckTableFieldInfos】获取所有检测字段的字段信息执行结束');
	logger.fireEvent();
	return checkTableInfos;
}

/**
 * 获取检测规则表所有检测表ID和别名
 */
function getDgAllCheckTableInfos() {
	logger.addLog('获取【检测规则表】所有检测表ID和别名');
	let checkTableInfos: JSONObject = {};
	let ds = db.getDefaultDataSource();
	let querySQL: string = 'select t0.MAIN_TABLE_ID as MAIN_TABLE_ID,t0.ALIAS as ALIAS from SDI2.SZSYS_4_DG_RULES_LIB t0';
	let datas = ds.executeQuery(querySQL);
	for (let i = 0; i < datas.length; i++) {
		let data: string[] = datas[i] as string[];
		let tableId: string = data[0] as string;
		let alias: string = data[1] as string;
		let tableInfo: DwTableInfo = dw.getDwTable(tableId);
		if (!tableInfo) {
			logger.addLog(`表：${tableId} 不存在，无法获取字段信息`);
			continue;
		}
		checkTableInfos[alias] = { dimensions: tableInfo.dimensions, measures: tableInfo.measures };
	}
	return checkTableInfos;
}

/**
 * 获取数据标准所有检测表ID和别名
 */
function getStdAllCheckTableInfos() {
	logger.addLog(' 获取【数据标准】所有检测表ID和别名');
	let checkTableInfos: JSONObject = {};
	let ds = db.getDefaultDataSource();
	let querySQL: string = `SELECT t1.STD_TABLE_ID,t1.STD_TABLE_NAME FROM SDI2.SZSYS_4_DG_STD_TABLES t1`;
	let queryData = ds.executeQuery(querySQL);
	for (let i = 0; i < queryData.length; i++) {
		let data = queryData[i];
		let tableAlias: string = data.STD_TABLE_NAME;
		let standardId: string = data.STD_TABLE_ID;
		checkTableInfos[tableAlias] = getStandardFieldInfo(ds, standardId);
	}
	return checkTableInfos;
}

/**
 * 查询标准表字段信息
 * 由于部分标准表不存在实际的表，需要从【标准字段表】查询字段信息
 * @param ds 
 * @param standardId 标准表ID
 * @return 
 */
function getStandardFieldInfo(ds: Datasource, standardId: string): CheckFieldInfo {
	let fieldInfo: CheckFieldInfo = {
		dimensions: [],
		measures: []
	}
	let querySQL: string = `SELECT STD_FIELD_NAME,STD_FIELD_DESC,DATA_TYPE,DATA_LENGTH,DATA_DECIMAL FROM SDI2.SZSYS_4_DG_STD_FIELDS WHERE STD_TABLE_ID = ?`;
	let queryData = ds.executeQuery(querySQL, standardId);
	for (let i = 0; i < queryData.length; i++) {
		let data = queryData[i];
		let dimension: DwTableFieldItemInfo = {
			name: data.STD_FIELD_DESC,
			decimal: data.DATA_DECIMAL,
			dataType: data.DATA_TYPE,
			dbFieldName: data.STD_FIELD_NAME,
			innerWrappedObject: {
				name: data.STD_FIELD_DESC,
				dimension: true
			}
		};
		fieldInfo.dimensions.push(dimension);
	}
	return fieldInfo;
}

/** 检测字段信息 */
interface CheckFieldInfo {
	/** 字段维项信息 */
	dimensions: DwTableFieldItemInfo[];
	/** 字段度量信息 */
	measures: (DwTableFieldInfo | DwTableFieldFolderInfo)[];
}

/** 获取检测表类型 */
enum CheckTableType {
	/** 数据检测 */
	DataGovernCheck = 'governCheck',
	/** 数据标准 */
	StandardCheck = 'standardCheck'
}