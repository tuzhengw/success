/**
 * 代码位于：web/static-file/metadata/sysettings/syssettings-securityConf.ts
 
 * 安全策略设置界面，包含子面板：密码安全，登录安全，安全等保，阈值设置，回归测试，数据脱敏，数据加密
 */
export class SettingPage_securityConf extends SettingTabPages {
	constructor(args: ComponentArgs) {
		super(args);
	}

	protected _init(args: ComponentArgs): HTMLElement {
		let domBase = super._init(args);
		this.toolbar?.setEnabled(true);
		return domBase;
	}

	/**@override */
	protected getPageImportComponentInfo(id: string): IImportComponentInfo<Component> {
		return SecurityConf_Page_ImportComponentInfos[id];
	}

	/**@override */
	protected getTabbarArgs(): TabbarArgs {
		return {
			layoutTheme: "texttabset",
			className: "syssettings-web-tabbar",
			keyPrefix: "sys.syssettings.securityConf",
			items: [{
				id: 'passwordSettings', // 密码安全
			}, {
				id: 'loginSettings', // 登录安全
			}, {
				id: 'securityLevelProtection', // 安全等保
			}, {
				id: 'thresholds', // 阈值设置
			}, {
				id: 'regressionTesting' // 回归测试
			}, {
				id: 'log' // 回归测试
			}, {
				id: 'masking' // 数据脱敏
			}, {
				id: 'algorithms' // 数据加密
			}],
			value: 'passwordSettings',
		};
	}

	/** @override */
	protected getManURL(): string {
		return "sys/settings/securityconf";
	}
}

/**
 * 会根据ID来分别构建内部的N个页面
 */
const SecurityConf_Page_ImportComponentInfos: { [pageId: string]: IImportComponentInfo<Component> } = {
	passwordSettings: {
		implClass: SettingFormPage_securityConf 
	},
	loginSettings: {
		implClass: SettingFormPage_securityConf
	},
	securityLevelProtection: {
		implClass: SettingFormPage_securityConf
	},
	thresholds: {
		implClass: SettingFormPage_securityConf
	},
	regressionTesting: {
		implClass: SettingFormPage_securityConf
	},
	masking: {
		implClass: SettingPage_masking
	},
	algorithms: {
		implClass: SettingPage_algorithms
	},
	log: {
		implClass: SettingPage_log
	}
}
