
/**
 * 1 ORACLE 查询正在执行的SQL
 */
 SELECT t0.schemaname "SCHEMA" ,
		to_char(substr(t1.SQL_FULLTEXT, 1, 4000)) SQL_TEXT ,
		t0.USERNAME ,
		t0.SQL_EXEC_START START_TIME ,
		(Sysdate - t0.SQL_EXEC_START) * 24 * 60 * 60 COST_TIME ,
		t0.EVENT ,
		t0.SID SESSION_ID ,
		t0.SERIAL# SERIAL ,
		t0.LOGON_TIME ,
		t0.STATUS SESSION_STATUS ,
		t2.SPID DB_OS_PID ,
		t0.OSUSER CLIENT_OSUSER ,
		t0.MACHINE CLIENT_HOSTNAME ,
		Null CLIENT_IP ,
		t0.PROGRAM PROGRAM
FROM v$session t0
LEFT JOIN v$sql t1
	ON t0.sql_id = t1.SQL_ID
		AND t0.SQL_CHILD_NUMBER = t1.CHILD_NUMBER
LEFT JOIN v$process t2
	ON t0.PADDR = t2.ADDR
WHERE 1 = 1
		AND t0.STATUS = 'ACTIVE'
		AND t0.username Is NOT Null
		AND t0.sql_id Is NOT NULL
		
---------------------------------------------------------
oracel删除某个SQL：ALTER SYSTEM KILL SESSION 'SID,SERIAL#';
--------------
oracle查看当前所有被锁住的资源
SELECT l.session_id, o.owner, o.object_name
FROM v$locked_object l, dba_objects o
WHERE l.object_id = o.object_id;


SELECT s.username, s.sid, s.serial#, s.logon_time
FROM v$locked_object l, v$session s
WHERE l.session_id = s.sid
ORDER BY s.logon_time; 通过查询sid和serial
---------------------------------------------------------
vitical：

SELECT *
FROM v_monitor.query_requests
WHERE is_executing = true;
---------------------------------------------------------Oceanbase 获取正在执行的SQL语句的一些信息 
SELECT ID, USER, DB, INFO
FROM information_schema.PROCESSLIST
WHERE COMMAND = 'Query';


数据库标识符：例如：oracle，字段和关键字一样，则需要增加：单引号
不同数据库特殊对象标识符是不一样的，获取到dialect后，调用getOpenQuote()+name+getCloseQuote()
let ds = db.getDataSource("111_1");
let dialect = ds.getDialect();
print(dialect.getOpenQuote());
注意：普通字段增加quote也可以正常查询。
---------------------------------------------------------


/**
 * 2 dw模块和db模块都支持exp写法, eg: model1.ID IN() 或者 userGroupTable.select(["ID"], "ID IN()")
 */
testTable.select(["ID"], "ID IN ('123')");

 /**
  * 3 日期范围, eg: [FCYDATE].[年月] = "[20230114~20230131]", 产品会自己识别, 并生成SQL
  * 注意：范围日期值要被：[] 包含
  */
SELECT *
	FROM ( SELECT t0.FID AS FID,
		t0.FPRN AS FPRN,
		t0.FZDLX AS FZDLX,
		t0.FICDM AS FICDM,
		t0.FJBNAME AS FJBNAME,
		t0.FZLJGBH AS FZLJGBH,
		t0.FZLJG AS FZLJG,
		t0.FRYBQBH AS FRYBQBH,
		t0.FRYBQ AS FRYBQ,
		t0.FCYDATE AS FCYDATE,
		t0.LSH AS LSH,
		t0.FICDVERSION AS FICDVERSION,
		t0.FPX AS 排序,
		t0.FTIMES AS FTIMES,
		t0.PX AS PX
	FROM BAGLXT.SBA_ZDXX t0
	WHERE (t0.FCYDATE>=to_date('202301','yyyymm')
			AND t0.FCYDATE<to_date('202302','yyyymm')) )
WHERE rownum <= 10000