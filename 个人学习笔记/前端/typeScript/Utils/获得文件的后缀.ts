/**
 * 获取文件的后缀
 * @fileName：文件名.类型
 * @return 文件的后缀名（限制最多3个字符）
 * <pre>
 *     "file.txt" —— txt
 *	   "file"—— null
 * </pre>
 */
function getFileType(fileName: string): string {
	return fileName.lastIndexOf('.') < 1
		? null
		: fileName.split(".").slice(-1).toString();
}


/**
 * 获取文件的后缀
 * @fileName：文件名.类型
 * @return 文件的后缀名
 * <pre>
 *     "file.txt" —— txt
 *     "file"—— file
 * </pre>
 */
function getExt(fileName: string): string {
    return fileName.match(/(?:.+..+[^\/]+$)/ig) != null
        ? fileName.split('.').slice(-1).toString()
        : null;
}


/**
 * 案例：判定一个文件是否为图片格式
 * @params  imageType：图片后缀名
 * @return 是否是图片
 */
function isImageType(imageType: string): boolean {
    return ["png", "jpg", "jpeg", "bmp", "gif", "webp", "psd", "svg", "tiff", "img"].indexOf(imageType.toLowerCase()) != -1;
}