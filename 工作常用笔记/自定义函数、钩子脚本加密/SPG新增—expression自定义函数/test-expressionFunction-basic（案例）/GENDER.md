---
permalink: /exp/func/gender
sidebarDepth: 0
---
# **GENDER([*p_id*,] property)**

根据身份证号获取性别

**获取性别**

## 参数

***p_id***：必须，居民18位身份证号。

## 示例

1. `GENDER("420100********1112")` 返回 男，表示男性
