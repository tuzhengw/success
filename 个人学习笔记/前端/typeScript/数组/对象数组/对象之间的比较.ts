interface Student {
    /**
     * 姓名
     */
    name: string;
    /**
     * 年龄
     */
    age: number;
}

/**
 * 对象数组的处理
 */
function main() {
    var s1: Student = {
        name: "张三",
        age: 12
    }
    var s2: Student = {
        name: "张三",
        age: 12
    }
	// 不相等  s1 == s2
	// 相等  JSON.stringify(s1) == JSON.stringify(s2)
    objEquals(s1, s2);

    var a = {
        num: "1",
        arr: ["1", "2", "3"],
        hello() {
            var i = 1;
            console.log(i);
        }
    };
    var b = {
        num: "1",
        arr: ["1", "2", "3"],
        hello() {
            var i = 1;
        }
    };
	// 不相等
	// 相等
    objEquals(a, b);
}


/**
 * 对象之间比大小
 * 仅适合：对象成员为基本类型
 */
function objEquals(s1: any, s2: any): void {
    if (s1 == s2) {
        console.log("相等");
    } else {
        console.log("不相等");
    }
    /**
     * 结果：{"num":"1","arr":["1","2","3"]}
     * 说明：对象内的成员方法不会被JSON.stringify识别
     * 注意：JSON.stringify 不能用于内部循环引入的对象，eg：
     * var a = { b }; b.parent = a;
     */
    console.log(JSON.stringify(s1));
    try{
        if (JSON.stringify(s1) == JSON.stringify(s2)) {
            console.log("相等");
        } else {
            console.log("不相等");
        }
    }catch (e) {
        console.log(e);
        console.log("参数不是JSON格式或者内部属性存在循环引入");
    }
}

main();

