---
permalink: /exp/func/test_user_extproperty_sync
sidebarDepth: 0
---
# **TEST_USER_EXTPROPERTY_SYNC([*user_id*,] property)**

获取用户的扩展属性。

**测试扩展函数，外部不要使用!**

## 参数

***user_id***：可选，表示要获取信息的用户的ID，不传递时表示当前用户。

***property***：要获取的属性名

## 示例

1. `TEST_USER_EXTPROPERTY_SYNC("role")` 获取当前用户扩展的`role`属性
2. `TEST_USER_EXTPROPERTY_SYNC("hb01,"role")` 获取`hb01`用户扩展的`role`属性
