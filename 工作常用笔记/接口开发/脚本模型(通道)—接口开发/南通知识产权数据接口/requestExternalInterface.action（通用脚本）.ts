import { getDataSource } from "svr-api/db"; //数据库相关API
import { EventOperationState, ServerEvent, fireEvent, getThreadProcessMonitor } from "svr-api/sys";
import { post, request } from "svr-api/http";
import { sleep } from "svr-api/sys";

/** 数据源名 */
const dataBase: string = "sjzt_ods";
/** 日志重要程度 */
const LOGLEVEL_FAIL_IMPT = 'IMPT';
/**
 * 20211222 tuzw
 * 请求第三方接口，获取【南通】数据
 * @param tableType 表注释类型【 有效专利 | 专利授权 】
 * @param tableName 模型表名
 * @parma token 请求接口凭证
 * @parma filter 过滤条件，eg：[{ "SS_XZQH": { "val": "3206", "oper": "contains" } }]
 * @return rows[][]
 */
export function requestData(tableType: string, tableName: string, token: string, filter): Array<Array<string>> {
    // return [];
    console.debug(`--requestData，请求【南通市场监管局】-【${tableType}】数据查询------------start`);
    let start_time = new Date();
    let event: ServerEvent = {
        name: 'requestData',
        logLevel: LOGLEVEL_FAIL_IMPT,
        detailInfo: {
            requestParam: {
                tableType: tableType,
                tableName: tableName,
                token: token,
                filter: filter
            }
        }
    }
    let rows: Array<Array<string>> = [];
    let queryFields = getTableFields(dataBase, tableName);
    let requestDataResult: ResultInfo = requestExternalInterface(tableType, token, filter, queryFields, 0, 10); // 先发一次请求，获取【南通数据中台】的【总数据量：totalRowCount 】
    if (requestDataResult.success) {
        /** 满足过滤条件的【数据数量】 */
        let totalRowCount: number = requestDataResult.result.totalRowCount;
        /** 根据【总数】获取【总分页数】【1页：100条】 */
        let sumDividePage: number = Math.ceil(totalRowCount / 100);
        console.debug(`-- 总条数为：${totalRowCount}，总页数：${sumDividePage} --`);
        for (let pageNumber = 0; pageNumber <= sumDividePage; pageNumber++) {

            if (pageNumber == 0 || (pageNumber + 1) % 10 == 0) {
                console.debug(`--【${tableType}】当前为第${pageNumber + 1}次请求，总请求次数为：${sumDividePage}，每次请求100条--`);
            }
            requestDataResult = requestExternalInterface(tableType, token, filter, queryFields, pageNumber, 100); // 每次请求100条
            if (requestDataResult.code == 101) {
                token = getToken().access_token;
                requestDataResult = requestExternalInterface(tableType, token, filter, queryFields, pageNumber, 100);
                console.debug(`--【${tableName}】请求数据过程中，Token失效，重新请求的token为：【${token}】--`);
            }
            /**
             * 20220106 tuzw
             * 由于对方数据库性能问题，每次请求不能超过5秒，调用频率不要太频繁，若报：103，则休眠5秒
             */
            if (requestDataResult.code == 103) {
                console.debug(`--【${tableName}】每次请求需要间隔【5】秒，本次请求冲突了，休眠后5秒后，重新请求，【${JSON.stringify(requestDataResult)}】--`);
                sleep(5000);
                requestDataResult = requestExternalInterface(tableType, token, filter, queryFields, pageNumber, 100);
            }
            if (requestDataResult.success) {
                let data: Array<IntellectualPropertyRight> = requestDataResult.result.data;
                for (let i = 0; i < data.length; i++) {
                    let row: Array<string> = [];
                    let keys: Array<string> = Object.keys(data[i]);
                    for (let k = 0; k < keys.length; k++) {
                        row.push(data[i][`${keys[k]}`])
                    }
                    rows.push(row);
                }
            } else {
                // 其他问题终止，记录日志
                console.debug(`--南通市场监管局接口接口，当前请求表：${tableName}，第${pageNumber + 1}次请求失败：${JSON.stringify(requestDataResult)}，终止程序--`);
                event.detailInfo.requestResult = requestDataResult;
                rows = [];
                break;
            }
        }
    } else {
        console.debug(requestDataResult);
    }
    let end_time = new Date();
    let spendTime = end_time.getTime() - start_time.getTime();
    console.debug(`--requestData，请求【南通市场监管局】-【${tableType}】数据查询，花费时间：${spendTime / 1000}秒------------end`);
    fireEvent(event);
    return rows;
}


/** 
 * 请求第三方接口【南通市场监管局接口】，获取凭证：Token值
 * java.net.UnknownHostException 异常：无法解析该域名，可以将域名对应的ip写入到hosts文件中保存
 * @return eg，{ result: true, access_token: xxx }
 */
export function getToken(): ResultInfo {
    let requestResult: ResultInfo = { success: true };
    try {
        // 参数单独提出，因为部分环境直接传入参数，内部无法将map转换为JSON
        let paramJson = {
            url: "https://ip.jsipp.cn/api/oauth2/token",
            data: {
                "app_id": "5069c008268642ea9e1ac5cdc886f543",
                "app_secret": "0ed2c6e02c254236ab949dc32521eb67",
                "grant_type": "ip",
                "userid": "eac1757b52ef4c918652bfc933cbbfc1"
            },
            headers: {
                "Content-Type": "application/json"
            }
        };
        let httpRequestResult: HttpResponseResult = post(paramJson);
        if (httpRequestResult.httpCode == 200) {
            requestResult = JSON.parse(httpRequestResult.responseText);
            requestResult.success = true;
            console.debug(`获取token成功，token：【${requestResult.access_token}】`);
        } else {
            requestResult.success = false;
            requestResult.message = `--请求错误--`;
            console.debug(httpRequestResult);
        }
    } catch (e) {
        requestResult.success = false;
        requestResult.message = `南通市场监管局接口，获取token失败`;
        console.debug(`--南通市场监管局接口，获取token失败--`);
        console.error(e);
    }
    return requestResult;
}

/**
 * 20211221 tuzw
 * 请求【南通市场监管局接口】，获取知识产权数据
 * @parma requestTableType 请求表类型【专利授权 | 有效专利】
 * @param token 调用凭证信息
 * @param filters 查询的过滤条件数组，[ "SS_XZQH": { "val": "3206", "oper": "contains" } ]
 * @param returnField 查询的字段集
 * @param pageNum 从第几页开始查询
 * @param pageSize 指定的查询页面大小，条数范围：[10-100]
 * @return eg: { success: true, result: { "totalRowCount": 281238,  "data": [{...}] } }
 */
export function requestExternalInterface(requestTableType: string, token: string, filters, returnField: string, pageNum: number, pageSize: number): ResultInfo {
    let queryResult: ResultInfo = {};
    try {
        let parmaJson = {
            url: `${getRequestUrl(requestTableType)}?access_token=${token}`,
            method: "POST",
            data: {
                "returnField": returnField,
                "pageNum": pageNum,
                "pageSize": pageSize
            },
            headers: {
                "Content-Type": "application/json"
            }
        }
        if (requestTableType == "商标") { //商标过滤条件是：exp
            parmaJson.data["exp"] = filters;
        } else {
            parmaJson.data["filters"] = filters;
        }
        let httpRequestResult: HttpResponseResult = request(parmaJson);
        if (httpRequestResult.httpCode == 200) {
            queryResult = JSON.parse(httpRequestResult.responseText);
            if (queryResult.result != undefined) { // 若请求结果包含：查询数据，则成功
                queryResult.success = true;
            } else {
                queryResult.success = false;
            }
        } else {
            queryResult.success = false;
            queryResult.message = `--请求错误--`;
            console.debug(httpRequestResult);
        }
    } catch (e) {
        queryResult.success = false;
        console.debug(`--requestExternalInterface()，请求【南通市场监管局】数据接口失败--`);
        console.error(e);
    }
    return queryResult;
}

/** 
 * 获取对应物理表的物理字段
 * @param dataBase 数据源
 * @param tableName 物理表名
 * @return eg：PROPERTY_BGN_DATE,PROPERTY_END_DATE,CY,NAME,AGENT_ADDR,AGENT_ID,....
 */
function getTableFields(dataBase: string, tableName: string, schema?: string): string {
    if (dataBase == null || tableName == null) {
        return ""
    }
    let ds = getDataSource(dataBase);
    try {
        if (!schema) {
            schema = ds.getDefaultSchema();
        }
        let dsMeta = ds.getTableMetaData(tableName, schema);
        let fields = dsMeta.getColumns();
        let colums = conversColums(fields);
        return colums.join(",");
    } catch (e) {
        console.error(`-----南通请求接口，获取对应物理表的物理字段，error`);
        console.error(e);
    }
    return "";
}

/**
 * 转换一下得到的字段列表
 */
function conversColums(colums: TableFieldMetadata[]) {
    let result = [];
    for (let i = 0; i < colums.length; i++) {
        let col = colums[i];
        result.push(col.name)
    }
    return result;
}



/** ODS_ZSCQ_ZLSQ：【专利授权】查询列 */
const ODS_ZSCQ_ZLSQ_QUERY_COLUMNS: string = "ZLQRMC,ZLQRMC_NEW,ZLQRMC_YW,ZLRQDZ,ZLQRLX,ZLQRYB,ZLLX,ZFLH,ID,AGENCYCODE,DLJGDM,DLJGMC,QYLX,QYGM,NBXH,FMMC,CSMC,INPUTCODE,BELONGAREA,BELONGCITY,BELONGPROVINCE,BELONGTOWN,SQRKR_CHAR,SQRKR,SFZLQY,SFGSQY,SFGXQY,SFLTQY,SQH,SQR_CHAR,SQR,SFMC,XIAN,SHI,SHENG,JJHKLB,JYZT,SS_XZQH";
/** ODS_ZSCQ_YXZL：【有效专利】查询列 */
const ODS_ZSCQ_YXZL_QUERY_COLUMNS = "ZLQRMC,ZLQRMC_NEW,ZLQRMC_YW,ZLRQDZ,ZLQRLX,ZLQRYB,ZLLX,ZFLH,ID,AGENCYCODE,DLJGDM,DLJGMC,QYGM,NBXH,FMMC,CSMC,INPUTCODE,BELONGAREA,BELONGCITY,BELONGPROVINCE,BELONGTOWN,SQRKR,SQRKR_CHAR,SFZLQY,SFGSQY,SFGXQY,SFLTQY,SQH,APPLYCODE,SQR_CHAR,SQR,SFMC,XIAN,SHI,SHENG,JJHKLB,SJ_HYDM,SS_XZQH";

/**
 * 根据表类型【专利授权 | 有效专利】，返回查询的列
 * @param requestTableType 请求表类型【专利授权 | 有效专利】
 * @return 
 */
function getQueryColumn(requestTableType: string): string {
    switch (requestTableType) {
        case "专利授权": return ODS_ZSCQ_ZLSQ_QUERY_COLUMNS;
        case "有效专利": return ODS_ZSCQ_YXZL_QUERY_COLUMNS;
        default:
            console.debug(`--目前表的类型仅【专利授权 | 有效专利】，请检查参数值--`);
    }
    return "";
}

/**
 * 根据请求接口类型，获取URL路径
 * @param requestUrlType 请求URL类型，eg："专利授权"
 * @return url路径
 */
function getRequestUrl(requestUrlType: string): string {
    switch (requestUrlType) {
        case "专利授权": return "https://ip.jsipp.cn/api/dx/8fa9d5ca9361441abdfd566ef519848e/queryData";
        case "有效专利": return "https://ip.jsipp.cn/api/dx/3e06fc7fa6294ce183d089ffb14af446/queryData";
        case "商标": return "https://ip.jsipp.cn/api/dx/89ee765d17bf4295a390d2990f89418b/queryData";
        default:
            return "";
    }
}

/** 
 * 校验某数据源下的数据库【某表】是否为：空表
 * @param dataBase 数据源名
 * @param tableName 表名
 * @parma schem
 * @return true | false
 */
export function checkTableIsContainsData(dataBase: string, tableName: string, schem?: string): boolean {
    let dataSource = getDataSource(dataBase);
    let conn = dataSource.getConnection();
    let tableIsNull: boolean = false;
    try {
        let table: TableData;
        table = dataSource.openTableData(tableName, schem, conn);
        let checkContentIsNullSql: string = `select count(1) as COUNT from ${tableName}`;
        let queryData = table.executeQuery(checkContentIsNullSql);
        if (Number(queryData[0]["COUNT"]) == 0) {
            tableIsNull = true;
        }
    } catch (e) {
        tableIsNull = undefined;
        console.debug(`--校验数据库【${tableName}】是否为：空表--error--`);
        console.error(e);
    } finally {
        conn.close();
    }
    return tableIsNull;
}

/**
 * 获取某表指定【时间字段】最大的时间
 * @param dataBase 数据源名
 * @param tableName 表名
 * @param timeColumName 时间字段名
 * @parma schem
 * @return 2020-02-01 | 20200201
 */
export function getMaxTime(dataBase: string, tableName: string, timeColumName: string, schem?: string): string {
    let dataSource = getDataSource(dataBase);
    let conn = dataSource.getConnection();
    let maxDateString: string = "";
    try {
        let table: TableData;
        if (!!schem) {
            table = dataSource.openTableData(tableName, schem, conn);
        } else {
            table = dataSource.openTableData(tableName, "", conn);
        }
        let selectMaxDateSQL: string = "";
        if (tableName == "ODS_ZSCQ_SB") { // 商标过滤时间是：日期类型
            selectMaxDateSQL = `select MAX(${timeColumName}) as MAXDATE from ${tableName} where ${timeColumName} < sysdate`;
        } else {
            selectMaxDateSQL = `select MAX(${timeColumName}) as MAXDATE from ${tableName} where ${timeColumName} < to_char(sysdate,'yyyy-MM')`;
        }
        print(`获取某表指定【时间字段】最大的时间SQL：${selectMaxDateSQL}`);
        let maxDate = table.executeQuery(selectMaxDateSQL)[0].MAXDATE;
        if (!!maxDate) {
            if (typeof maxDate == "string") {
                maxDate = new Date(maxDate);
            }
            if (tableName == "ODS_ZSCQ_SB") {
                let sdf = new java.text.SimpleDateFormat("yyyyMMdd");
                maxDateString = sdf.format(new java.util.Date(maxDate.getTime()));
                return maxDateString;
            } else {
                let sdf = new java.text.SimpleDateFormat("yyyy-MM");
                maxDateString = sdf.format(new java.util.Date(maxDate.getTime()));
            }
        }
    } catch (e) {
        console.error(`--数据库获取【${tableName}】指定【时间字段】最大的时间--error--`);
        console.error(e);
    } finally {
        conn.close();
    }
    return dateFormat(maxDateString);
}

/**
 * 格式化时间
 * @param date 需要格式化的时间
 * @return 格式化后的时间【20210705】
 */
function dateFormat(dateTime): string {
    if (!dateTime) {
        return "";
    }
    let dates = new Date(dateTime);
    let year: number = dates.getFullYear();
    let months = (dates.getMonth() + 1) < 10
        ? '0' + (dates.getMonth() + 1)
        : (dates.getMonth() + 1);
    let day = dates.getDate() < 10
        ? '0' + dates.getDate()
        : dates.getDate();
    let nowDay: string = `${year}${months}${day}`;
    return nowDay;
}


/** 返回值 */
export interface ResultInfo {
    /** 返回结果 */
    success?: boolean;
    /** 错误码 */
    errorCode?: string;
    /** 消息 */
    message?: string;
    /** 错误数据 */
    errorData?: any;
    /** 过期时间，单位为秒，每次时间为10分钟 */
    expires_id?: number,
    /** token值 */
    access_token?: string;
    /** 状态码 */
    code?: number;
    /** 请求接口返回的数据 */
    result?: QueryResult;
    [propname: string]: any;
}

/** 请求返回的结果 */
export interface QueryResult {
    /** 满足过滤条件的所有数据【总数】 */
    totalRowCount?: number;
    /** 查询的数据集【查询个数与指定的pageSize数量有关】 */
    data?: Array<IntellectualPropertyRight>;
    [propname: string]: any;
}

/** 知识产权对象 */
export interface IntellectualPropertyRight {
    /** 专利权人名称 */
    ZLQRMC?: string;
    /** 专利权人名称（新） */
    ZLQRMC_NEW?: string;
    /** 专利权人名称（英文） */
    ZLQRMC_YW?: string;
    /** 专利权人地址 */
    ZLRQDZ?: string;
    /** 专利权人类型  */
    ZLQRLX?: string;
    /** 专利权人邮编 */
    ZLQRYB?: string;
    /** 专利类型 */
    ZLLX?: string;
    /** 主分类号 */
    ZFLH?: string;
    /** 主键 */
    ID?: string;
    /** 代理机构代码 */
    AGENCYCODE?: string;
    /** 代理机构代码(旧) */
    DLJGDM?: string;
    /** 代理机构名称 */
    DLJGMC?: string;
    /** 企业类型 */
    QYLX?: string;
    /** 企业规模 */
    QYGM?: string;
    /** 内部序号  */
    NBXH?: string;
    /** 发明名称 */
    FMMC?: string;
    /** 城市名称 */
    CSMC?: string;
    /** 导入月份 */
    INPUTCODE?: string;
    /** 所属区县（旧） */
    BELONGAREA?: string;
    /** 所属市（旧） */
    BELONGCITY?: string;
    /** 所属省（旧） */
    BELONGPROVINCE?: string;
    /** 所属镇 */
    BELONGTOWN?: string;
    /** 授权入库日  */
    SQRKR_CHAR?: string;
    /** 授权入库日日期型 */
    SQRKR?: Date;
    /** 是否战略企业 */
    SFZLQY?: string;
    /** 是否规上企业 */
    SFGSQY?: string;
    /** 是否高新企业 */
    SFGXQY?: string;
    /** 是否龙头企业 */
    SFLTQY?: string;
    /** 申请号 */
    SQH?: string;
    /** 申请号（非空） */
    APPLYCODE?: string;
    /** 申请日 */
    SQR_CHAR?: string;
    /** 申请日日期型 */
    SQR?: Date;
    /** 省份名称 */
    SFMC?: string;
    /** 系统划分区县 */
    XIAN?: string;
    /** 系统划分城市 */
    SHI?: string;
    /** 系统划分省份 */
    SHENG?: string;
    /** 经济户口类别 */
    JJHKLB?: string;
    /** 经营状态 */
    JYZT?: string;
    /** 行政区划 */
    SS_XZQH?: string;
    [propname: string]: any;
}