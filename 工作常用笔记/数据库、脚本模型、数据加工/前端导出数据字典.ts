import { ctx, SZEvent, Component, throwInfo, showConfirmDialog, showFormDialog, showErrorMessage, showMenu, isEmpty, deepAssign, uuid, message, quoteExpField, showDialog, showSuccessMessage, showProgressDialog, rc, downloadFile, waitRender, showWaiting, assign, IDataProvider, formatDateFriendly, tryWait, rc_get, showWarningMessage, wait } from 'sys/sys';
import { IMetaFileCustomJS, InterActionEvent, IAppCustomJS, IDwCustomJS, FAppInterActionEvent, IFApp } from "metadata/metadata-script-api";
import { showDrillPage, getMetaRepository, MetaFileViewer, getCurrentUser, getSysSettings } from 'metadata/metadata';
import { getDwTableDataManager, refreshModelState, exportQuery, getQueryManager } from 'dw/dwapi';
import { DwTableModelFieldProvider } from 'dw/dwdps';
import { Combobox, Button, showUploadDialog, Toolbar, ToolbarItemAlign, ComboboxArgs, DataProvider, ButtonArgs, TXT_FILETYPE, IMAGE_FILETYPE, Edit } from 'commons/basic';
import { Tree } from 'commons/tree';
import { Menu, MenuItem } from "commons/menu";
import { SuperPageBuilder } from "app/superpage/superpagebuilder";
import { SuperPage } from 'app/superpage/superpage';
import { IDwTableEditorPanel, DwTableEditor } from 'dw/dwtable';
import { showLabelMgrDialog } from 'dw/dwdialog';
import { Form } from "commons/form";
import { ExDTable } from "commons/dtable";
import { Dialog } from 'commons/dialog';
import { PortalModulePageArgs, PortalHeadTitle, PortalHeadToolbar, PortalHeadTabbar, ITemplatePagePartRenderer } from 'app/templatepage';
import { EasyFilter } from 'commons/extra';
import { ExpVar, IExpDataProvider } from 'commons/exp/expcompiler';
import { MetaFileRevisionView } from 'metadata/metamgr';
import { showDbTableSelectDialog } from 'dw/dwdialog';
import { IFieldsDataProvider } from 'commons/exp/expdialog';

import { modifyClass, getAppPath } from './commons/commons.js';
import { Spg_Data_Access, Spg_Data_Access_Create, Spg_Data_Access_Ds, Spg_Data_Access_View } from "./data-access/data-access-mgr.js";
import { SpgDataGovernBasic, SpgDataGovernCreateTask, SpgDataGovernRules, SpgDataGovernTaskList } from './data-govern/data-govern-mgr.js?v=20220420';
import { Fapp_Data_Exchange_Res, Spg_Data_Exchange_API, Spg_Data_Exchange_List, Spg_Data_Exchange_Order, Spg_Data_Exchange_Push } from './data-exchange/data-exchange-mgr.js';
import { Spg_Data_Label_Hard, Spg_Data_Label_Soft } from './data-label/data-label-mgr.js';
import { Spg_Meta_Data_Mgr } from './meta-data/meta-data-mgr.js?v=1647848366227';
import { Spg_Mon_Mgr } from './data-monitor/mon-mgr.js';



/**
 * CSTM-11820 导出数据字典，传入需要导出的模型表id，支持输入文件名称、选择文件类型（csv、xlsx、xls）
 * @params resids
 */
function exportDataDictionary(resids: Array<string>): void {
	let dataDictionaryTablePath = "/sysdata/data/tables/sdi/data-asset/SDI_DATA_DICTIONARY.tbl";
	let exp = "\'" + resids.join("\',\'") + "\'";

	let exportFile = (fileName: string, format: DataFormat): Promise<void> => {
		return getMetaRepository().getFileBussinessObject<DwTableModelFieldProvider>(dataDictionaryTablePath).then(dwbo => {
			if (!dwbo) {
				showErrorMessage("数据字典模型表不存在");
				return;
			}
			let resid = dwbo.getFileInfo().id;
			let queryFields: QueryFieldInfo[] = dwbo.getFields().filter(field => field.dbfield != "ID").map(field => {
				return {
					name: field.name,
					exp: `[t0].${quoteExpField(field.name)}`
				}
			});
			let filters: FilterInfo[] = [
				{
					"matchAll": true,
					"enabled": true,
					"clauses": [{
						"exp": `t0.id in (${exp})`
					}]
				}];
			let queryInfo = {
				fields: queryFields,
				sources: [{
					id: "t0",
					path: dataDictionaryTablePath
				}],
				filter: filters,
				sort: [],
				options: {
					limit: null,
					offset: null,
					queryTotalRowCount: true
				}
			};

			return exportQuery({
				resid: resid,
				query: queryInfo,
				fileFormat: format,
				fileName: fileName
			})
		})
	}

	showFormDialog({
		id: 'spg.exportdatadictionary',
		caption: '导出数据字典',
		content: {
			items: [{
				id: 'fileName',
				caption: '文件名称',
				formItemType: 'edit',
				captionPosition: 'left'
			}, {
				id: 'format',
				caption: '文件类型',
				formItemType: 'selectpanel',
				captionPosition: 'left',
				compArgs: {
					items: [{
						value: 'xlsx',
						caption: 'xlsx'
					}, {
						value: 'xls',
						caption: 'xls'
					}, {
						value: 'csv',
						caption: 'csv'
					}]
				}
			}],
		},
		onshow: (event: SZEvent, dialog: Dialog) => {
			let form = <Form>dialog.content;
			form.loadData({ fileName: '数据字典', format: 'xlsx' });
		},
		buttons: [{
			id: "ok",
			layoutTheme: "defbtn",
			onclick: (event: SZEvent, dialog: Dialog, item: any) => {
				let data = (<any>dialog).form.getData();
				let format = data.format;
				let fileName = data.fileName;
				let range = data.range;
				dialog.waitingClose(exportFile(fileName, format));
			}
		}, 'cancel']
	});
}