
const EncryptionTool = com.topnet.utils.EncryptionTool;

const keys = "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAMJeMN6juzR9VKvzmQH8+eZXorXE8eU" +
    "Jvuqq+f35uHsTDr9wWOZel/YhLcZaKSXi1xmPL/fqICerAV1KBEUVbcNoWTq43Rbf/S/kY Ihu81S2o2DpffszIwTAN2pFxOgn9/" +
    "1kUe6sgJV6tJBzYo/YMwqnGE3NDqTf/VgD7UAK84zbAgMBAAECgYBCpBTRYQXgm1AanKzLV7c+2LE67Umao9rHjtOj1qj/2armz" +
    "FhOtAQaKYoORf9xs/LYGpXxxF49+42+NJB+Nzioe3aG4+TCyIJOVV1xVY8Ef5IVqAfSIdVwYleP26bTyb5LWgLvp/WbmWfckF3b+" +
    "eilsHK8XPOS5A6kMePZvcnawQJBAPId6psZpjVRoRvow0K6e7safza18+3NSk+rvJcgSPOPfjISWqv21gIAZlVQG8p9rPzLqGTiDe" +
    "JTEUZg3rp96VECQQDNg1up+CLiQWNyrmz60XHBbIWefCwS9cYzHYdpKTJlNvh1pVgKxwBmXgLgoumKXPvbAsmj38HmqBvZ6pTWYIh" +
    "rAkEA1mXrhCD4b6qzUfWSQxrDynGySQ5izSfHMDnRrLnoH9XnWAuswy63X9pexBlGIs+bSdhGa99JkCsi1wGc+ePZoQJBALL7F7Wre" +
    "RX7RJT3+8lk5uFBlg00r13D1l1l9ixLyKwFd9VdrpOURHDipzbkKuhpmp5eiyXBFam/9qGOfxlE8QkCQDlyoA6yk94Rcf2I9aochNu" +
    "p8EPcjA+XYt6i//i3emH5wjDa3sPfuXbBuBXp7MIMvAhGQiPjS4Kx4c5BkjdC/TI=";

/**
 * 20211202 tuzw
 * 将扫描的二维码信息加密编码 进行解密
 * @param encryptUrlInfo 加密的URL信息
 * @return 解密后的实际内容，eg：{ 'UNISCID':'91420704MA49PY9C7P' }
 */
export function dealWithCode(request: HttpServletRequest, response: HttpServletResponse, params: { decryptUrlCodeInfo: string }): string {
    if (!params.decryptUrlCodeInfo) {
        return "未传递参数";
    }
    try {
        EncryptionTool.setPrivateKey(keys);
        let decodeResult: string = EncryptionTool.decrypt(params.decryptUrlCodeInfo);
        return decodeResult;
    } catch (e) {
        console.error('--扩展表达式succ-expressionFucntion-decode，解析出现错误--');
        return '解密失败';
    }
}
