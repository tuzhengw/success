import db from "svr-api/db"
import sys from "svr-api/sys"

/**
 * 是否输入控制台日志
 */
const IS_DEBUG = true;
/**
 * 失败日志等级代码
 */
const LOGLEVEL_FAIL = 'FAIL';
/**
 * 日志重要程度
 */
const LOGLEVEL_FAIL_IMPT = 'IMPT';

// 错误码
const ERROR_BQ_NOEXIST = "noExist_patientArea";

// APP信息表以及错误码
const APP_ID = 'app_id';
const APP_SECRET = 'app_secret';
const TABLE_TRUSTED_APPS = 'SZSYS_4_TRUSTED_APPS';
const ERROR_APP_NOT_FOUND = 'appNotFound';
const ERROR_SECRET_MISMATCH = 'secretMismatch';
const ERROR_PERMISSION_DENIED = 'permissionDenied';
const MESSAGE_APP_NOT_FOUND = 'app_id在系统中不存在。正式环境的app_id见卫健委下发的文件，测试环境的app_id见在线API接口文档。';
const MESSAGE_SECRET_MISMATCH = 'app_secret错误，正式环境请从参照卫健委下发的文件，测试环境见在线API接口文档';
const MESSAGE_PERMISSION_DENIED = '无权限调用';

/**
 * 批处理脚本模版
 */
function main() {
    let temp = {
        "app_id": "test_succez_0420",
        "app_secret": "test_succez_0420_secret",
        "jgID": "510000000764"
    }
    print(getBqInfo(temp));
}

/**
 * 返回结果格式
 */
interface ResultInfo {
    /**
     * 结果
     */
    result: string | boolean;
    /**
     * 错误码
     */
    errorCode?: string;
    /**
     * 错误提示
     */
    message?: string;
    /**
     * 返回数据
     */
    data?: any;
    detailMessage?: any;
    errorData?: any;
}

/**
 * 同{@link TrustedApp}
 */
interface AppInfo {
    appId: string;
    appName?: string;
    appSecret: string;
    group?: string;
    enabled: boolean;
    type: 'code' | 'auto' | 'ip' | 'cert';
    ip?: string[];
    domain?: string[];
    apis?: string[];
    depts?: string[];
    label?: string;
    jgid: string;
    call_num?: number;
    last_call_time?: Date;
}

// 目标数据库
const DS_NAME = "default";
// 病区表
const bzTableData = "dim_bq";

/**
 * 查询医院病区列表的接口
 * @parm params（JSON格式）：包含机构ID
 * @return { "result": 是否执行成功, "data": 查询结果集};
 * <pre>
 * let temp = {
        'app_id': 'test_succez_0420',
        'app_secret': 'test_succez_0420_secret',
        'jgID': '510000000764'
    }
    print(getBqInfo(temp));
 * </pre>
 */
function getBqInfo(params): ResultInfo {
    IS_DEBUG && print(`--getBqInfo--，查询某个医院病区列表-------------start`);
    let start_time = new Date();
    IS_DEBUG && print(`--getBqInfo--，开始时间${start_time.toLocaleDateString() + '' + start_time.toLocaleTimeString}`)

    let appId = params[APP_ID];
    let appSecret = params[APP_SECRET];
    let resultInfo: ResultInfo = { result: true, message: '操作成功' };

    let event: any = {
        name: 'trustedapp.getBqInfo',
        LogLevel: LOGLEVEL_FAIL_IMPT,
        option1: appId,
        detailInfo: {
            appId: appId,
            result: resultInfo
        }
    }
    /**
     * 1. 根据app_id获取对应App信息
     * 2. 检查app对象
     */
    let appInfo = getAppInfo(appId);
    let result = checkApiWhiteList(appInfo, appSecret);
    if (!result.result) {
        IS_DEBUG && print(`--getBqInfo--，没有权限：${JSON.stringify(result)}`);
        resultInfo = result;
        event.operationState = LOGLEVEL_FAIL;
        event.detailInfo.result = resultInfo;
    } else {
        /**
         * 1. 获取机构ID，并作为新成员变量添加到detailInfo中
         * 2. 判定机构ID是否为NULL
         */
        let jgID = params["jgID"];
        event.detailInfo['jgid'] = jgID;
        if (!jgID) {
            resultInfo = {
                result: false,
                errorCode: 'missParam',
                message: `缺失参数jgID`
            }
            event.operationState = LOGLEVEL_FAIL;
            event.detailInfo.result = resultInfo;
        } else {
            let defaultData = db.getDataSource(DS_NAME);
            let bqTable = defaultData.openTableData(bzTableData);
            try {
                /**
                 * 1. 根据机构ID（jgID）获取病区表中当前机构的所有病区信息
                 * 2. 根据查询返回的结果，判断结果是否为：NULL或者不存在
                 */
                let result_sql = bqTable.select(["BQID", "BQMC", "QY"], { "SSJG": jgID });
                if (!result_sql || result_sql.length == 0) {
                    resultInfo.message = `操作失败！该申请ID【${jgID}】不存在当前机构中`;
                } else {
                    resultInfo.data = result_sql;
                    event.operationState = "操作成功";
                    event.detailInfo.result = resultInfo;
                }
            } catch (e) {
                resultInfo.result = false;
                resultInfo.errorCode = 'systemError';
                resultInfo.message = `系统错误，请联系管理员!`;
                resultInfo.detailMessage = e;
                event.operationStatel = LOGLEVEL_FAIL;
                event.detailInfo.result = resultInfo;
                let exception = getBaseException(e);
                event.exception = exception;
                print(exception);
            }
        }
    }
    IS_DEBUG && print(`---${JSON.stringify(resultInfo)}`);
    let end_time = new Date();
    IS_DEBUG && print(`--getBqInfo--，结束时间：${end_time.toLocaleDateString + ' ' + end_time.toLocaleTimeString}`)
    let spendTime = (end_time.getTime() - start_time.getTime());
    IS_DEBUG && print(`--getBqInfo--，花费时间：${spendTime / 1000}秒`);
    IS_DEBUG && print(`--getBqInfo--，查询完成的时间------------end`);
    return resultInfo;
}
/**
 * 处理异常
 */
function getBaseException(args) {
    return new com.succez.exception.business.BaseException(args);
}

/**
 * 20210716 tuzhengwie
 * 根据appId获取对应应用信息，没有则返回：NULL
 */
function getAppInfo(appId: string): AppInfo {
    let defaultData = db.getDataSource(DS_NAME);
    let tableData = defaultData.openTableData(TABLE_TRUSTED_APPS);
    let rs = tableData.selectFirst('*', { 'APP_ID': appId });
    if (rs == null || rs == {}) {
        return null;
    }
    let appInfo: AppInfo = {
        appId: rs.APP_ID,
        appName: rs.APP_NAME,
        appSecret: rs.APP_SECRET,
        type: rs.TYPE,
        group: rs.APP_GROUP,
        enabled: parseInt(rs.ENABLED) == 1,
        ip: split(rs.IP),
        domain: split(rs.DOMAIN),
        apis: split(rs.API_WHITE_LIST),
        depts: split(rs.DEPTS),
        label: rs.LABEL,
        jgid: rs.JGID,
        call_num: rs.CALL_NUM,
        last_call_time: rs.LAST_CALL_TIME || rs.last_call_time
    }
    return appInfo;
}

/**
 * 20210617
 * 拆分字符串
 */
function split(s: string): string[] {
    if (s == null || s == '') {
        return [];
    }
    return s.split(',');
}

/**
 * 20210716 tuzhengwei
 * 判断一个引用是否有权限访问一项：API
 * 流程：
 * 1. app是否存在/可用
 * 2. 应用秘钥（secret）是否可用
 * 3. api白名单中是否存在被调用的api
 * @param appInfo：授信应用
 * @param appSecret：授信应用秘钥
 * @param api（可选）：api的完整路径，例如：/api/oauth2/verifyUser
 * @return 返回校验结果（ResultInfo对象）
 */
function checkApiWhiteList(appInfo: AppInfo, appSecret: string, api?: string): ResultInfo {
    let type = appInfo && appInfo.type;
    if (appInfo == null || !appInfo.enabled || (type != 'code' && type != 'auto' && type != 'ip')) {
        return { result: false, errorCode: ERROR_APP_NOT_FOUND, message: MESSAGE_APP_NOT_FOUND };
    }
    if (appInfo.appSecret != appSecret) {
        return { result: false, errorCode: ERROR_SECRET_MISMATCH, message: MESSAGE_SECRET_MISMATCH };
    }
    if (appInfo.apis.indexOf(api) == -1) {
        //暂时去掉API的判断 
        // return { result: false, errorCode: ERROR_PERMISSION_DENIED, message: MESSAGE_PERMISSION_DENIED };
    }
    if (!appInfo.jgid) {
        return { result: false, errorCode: ERROR_PERMISSION_DENIED, message: MESSAGE_PERMISSION_DENIED };
    }
    return { result: true, data: appInfo };
}

// 患者表
const hzTableData = "dim_hz";
/**
 * 检查当前患者所在病区是否是当前机构的病区
 * @param jgID：机构ID
 * @param hzId：患者ID
 * @return ture或false
 * <pre>
 *      print(checkPatientIsArea("510000000764", "1"));
 * </pre>
 * 流程：
 * 1. 传入当前所选机构ID和患者ID
 * 2. 根据所传入机构ID，筛选病区表中当前机构的所有病区ID
 * 3. 获取患者的病区ID
 * 4. 判断患者病区ID是否在当前机构病区ID集合中
 */
function checkPatientIsArea(jgID: string, hzId: string): boolean {
    let bool: boolean = false;
    let bqData = db.getDataSource(DS_NAME);
    let bqTable = bqData.openTableData(bzTableData);
    // jgSelectResult：当前机构所拥有的病区ID结果集
    let jgSelectResult = bqTable.select("BQID", { "SSJG": jgID });
    if (jgSelectResult.length == 0) {
        // 
    }
    let hzTable = bqData.openTableData(hzTableData);
    // hzBqIDResult：获取患者病区的ID
    let hzBqIDResult = hzTable.select("HZBQID", { "HZID": hzId });
    if (hzBqIDResult.length == 0) {
        //
    }
    // 判断患者ID是否在当前机构病区ID集合中
    for (let i = 0; i < jgSelectResult.length; i++) {
        if (jgSelectResult[i].BQID == hzBqIDResult[0].HZBQID) {
            bool = true;
        }
    }
    return bool;
}