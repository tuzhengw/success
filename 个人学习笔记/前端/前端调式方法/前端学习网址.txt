前端文档速查：https://developer.mozilla.org/zh-CN/
——————————————————————————————
pro git：http://git-scm.com/book/zh/v2
——————————————————————————————


廖雪峰：https://www.liaoxuefeng.com/
公司后端脚本网址：https://docs.succbi.com/dev/server-script-api/security/#object
JS箭头函数：https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Functions/Arrow_functions
async和await：https://www.jianshu.com/p/b4fd76c61dc9
后端培训：https://wiki.succez.com/pages/viewpage.action?pageId=252182660
北京理工大学：https://blog.csdn.net/qq_26460841/category_8335084.html
福听云：https://doc.foxitcloud.cn/