// @see https://jira.succez.com/browse/SSSH-1 票据合并打印
	public mergePdfPrint(sze) {
		let params = sze.params;
		console.log(params);
		let fjs = params.fjs;
		fjs = fjs instanceof Array?fjs:[fjs]
		return showWaiting(rc({
			method: 'POST',
			url: '/szhfpgl/app/szhfpgl.app/scripts/mergepdfprint.action?method=getMergePdf',
			data: { fjs }
		}).then(result => {
			if (result && result.status == 'failed') {
				return showAlertDialog({ desc: result.msg })
			}
			window.open(`${ctx()}/szhfpgl/app/szhfpgl.app/scripts/mergepdfprint.action?mergePath=${result.data}`)
		}))
	}