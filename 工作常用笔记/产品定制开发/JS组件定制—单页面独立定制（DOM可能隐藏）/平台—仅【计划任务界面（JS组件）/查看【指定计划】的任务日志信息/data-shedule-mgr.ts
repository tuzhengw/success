/**
 * ================================================
 * 作者：liuyz
 * 审核员：
 * 创建日期：2022-01-04
 * 脚本用途：
 *      数据治理前端脚本，数据治理相关方法都存在这个文件中
 * ================================================
 */
import { showConfirmDialog } from "commons/dialog";
import "css!./data-govern-mgr.css";
import { DatasetDataPackageRowInfo, IDataset, InterActionEvent, IVComponent } from "metadata/metadata-script-api";
import { ExpDialog, ExpDialogValueInfo, IFieldsDataProvider, showExpDialog } from 'commons/exp/expdialog';
import { Component, isEmpty, SZEvent, throwInfo, rc, message, uuid, showProgressDialog, showWaiting, rc_task, assign, ServiceTaskPromise, showWarningMessage, showDialog, LogItemInfo, ServiceTaskInfo } from "sys/sys";
import { CustomJs_List, CustomJs_List_Head, getAppPath, showScheduleDialog } from "../commons/commons.js?v=202204121404-1486bcd5-2401677267720097";
import { ExpVar, IExpDataProvider } from "commons/exp/expcompiler";
import { checkCstmRule, CheckCstmRuleArgs, runCheckTask } from "dg/dgapi";
import { getDwTableDataManager, getQueryManager } from "dw/dwapi";
import { Dialog } from "commons/dialog";
import { showDrillPage, getMetaRepository, MetaFileViewer, getCurrentUser, getSysSettings } from 'metadata/metadata';
import { ExpContentType } from 'commons/exp/exped';
import { ProgressLogs } from "commons/progress"
import { getScheduleMgr, ScheduleMgr, ScheduleLogInfo, ScheduleLogsPage, TaskLogInfo } from "schedule/schedulemgr";

/** 有前缀表名的自定义规则字段的dp */
let dgRuleFieldDp: CustomRuleFieldDp;

/**
 * 数据检测基本类，处理执行检测任务
 */
export class SpgDataGovernBasic {
	public CustomActions: any;
	constructor() {
		this.CustomActions = {
			executeCheckTask: this.executeCheckTask.bind(this),
			judgeSheduleIsEnd: this.judgeSheduleIsEnd.bind(this)
		};
	}
	
	/**
	 * 问题：数据表执行检测任务后，会在后台执行已定好的计划任务，生成一张合法表，若生成过程未完成，用户点击查看，则会提示文件不存在
	 * 解决办法：点击后，先判断当前执行的计划任务是否结束，若没有结束，则展示当前执行的进度
	 * @param currentExecuteSchedulId  当前正在执行的计划任务ID
	 * @param toUrlCheckTaskId 跳转Url参数，[数据检测任务列表].[任务编码]
	 */
	public judgeSheduleIsEnd(event: InterActionEvent): Promise<void> {
		let params = event?.params;
		let currentExecuteSchedulId = params.currentExecuteSchedulId;
		let toUrlCheckTaskId = params.toUrlCheckTaskId;
		if (currentExecuteSchedulId == "") {
			showWarningMessage(`未指定要查询的计划任务序号`);
			return;
		}
		let sheduleMgr: ScheduleMgr = getScheduleMgr();
		return sheduleMgr.getSchedule(currentExecuteSchedulId).then((sheduleInfo: ScheduleInfo) => {
			/** 最近一次的执行状态 */
			let lastState = sheduleInfo.lastState;
			if (lastState != 9) {
				showDrillPage({
					url: `/sdi/data?:locate=/sdi/data/tables/sys/data-govern/${toUrlCheckTaskId}`,
					title: "打开链接 -查看执行计划生成的合法表",
					target: ActionDisplayType.Self
				});
				return;
			}
			showWarningMessage(`执行计划【${currentExecuteSchedulId}】正在执行，请稍后查看`);
			let scheduleLogs: Promise<ScheduleLogInfo[]> = sheduleMgr.queryScheduleLogs(currentExecuteSchedulId);
			return scheduleLogs.then((logResult: ScheduleLogInfo[]) => {
				let logId = logResult[0].scheduleLogId; // 默认取第一个
				return sheduleMgr.queryScheduleTaskLogs(currentExecuteSchedulId, logId).then((logs: TaskLogInfo[]) => {
					let log = logs[0];
					let state = log.state;
					if (state == 9) { // 若正在运行，则【轮询】获取当前最新的日志信息
						/** 任务运行的集群节点信息 */
						let lastClusterNode = log.lastClusterNode;
						/**	任务运行的长任务id */
						let lastRuntimeId = log.lastRuntimeId;
						this.showLogsDialog([], state, lastRuntimeId, lastClusterNode);
					} else { // 若已经执行完成，则直接获取logs信息
						let logInfos = !!log.logs ? JSON.parse(log.logs).logs : "计划已经执行完成";
						this.showLogsDialog(logInfos);
					}
				});
			});
		});
	}
	
	/**
	 * 显示日志对话框
	 * @param logs 日志信息
	 */
	public showLogsDialog(logs: (string | LogItemInfo)[], state?: DW_RESOURCE_STATE, lastRuntimeId?: string, lastClusterNode?: string) {
		// 任务正在执行，则根据长任务id向服务器请求日志
		if (state === DW_RESOURCE_STATE.EXECUTING && !isEmpty(lastRuntimeId)) {
			let taskPromise = new ServiceTaskPromise({
				uuid: lastRuntimeId,
				clusterNode: lastClusterNode,
				startPoll: false // 创建时会自动启动轮询，这里不启用，而是等进度对话框显示后再轮询，否则可能导致进度对话框中的日志丢失前面一部分
			});
			return new Promise((resolve, reject) => {
				showProgressDialog({
					caption: message("schedulemgr.tasklog.dilaog.viewlog"),
					uuid: lastRuntimeId,
					clusterNode: lastClusterNode,
					promise: taskPromise,
					buttons: ["close"],
					onclose: () => {
						taskPromise.stopPoll();
					}
				}).then(() => {
					taskPromise.startPoll();
				});
			})
		}
		return new Promise((resolve, reject) => {
			showDialog({
				id: "schedulemgr-dialog-progresslogs",
				className: "schedulemgr-dialog-progresslogs",
				caption: message("schedulemgr.tasklog.dilaog.viewlog"),
				maxHeight: 810,
				maxWidth: 1440,
				height: 600,
				width: 1080,
				minWidth: 600,
				minHeight: 340,
				maxIconVisible: true,
				content: {
					implClass: ProgressLogs,
					className: "dialog-viewprogresslogs",
					autoWrap: true,
					format: true,
				},
				onshow: (e: SZEvent, dialog: Dialog) => {
					let content = <ProgressLogs>dialog.content;
					content.clear();
					content.addLogs(logs);
				}
			})
		})
	}

	/**
	 * 执行计划任务检测
	 * @param event
	 * @returns
	 */
	public executeCheckTask(event) {
		let params = event.params;
		let taskId = params?.taskId;
		return taskId && showConfirmDialog({
			message: "确认要执行选择的检测任务吗？",
			onok: (event, dialog) => {
				this.currentExecuteSchedulId = taskId;

				let promise = runCheckTask(taskId, uuid());
				showProgressDialog({
					caption: "任务执行日志",
					uuid: taskId,
					promise: promise,
					buttons: [{
						id: "cancel",
						caption: "关闭"
					}],
					logsVisible: true,
					width: 700,
					height: 500
				});
				return showWaiting(promise).then(() => {
					throwInfo("检测完毕");
					getScheduleMgr().runSchedules([taskId]);
				})
			}
		})
	}
}