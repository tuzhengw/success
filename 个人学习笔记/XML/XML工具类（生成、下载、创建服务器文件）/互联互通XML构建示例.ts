/**
 * 作者：liuyz,tuzhengw,dingy
 * 创建时间:2022-09-27
 * 功能描述：
 * 	1.根据数据表数据生成xml配置类
 * 	2.根据解析后的xml配置生成xml
 * 	3.根据已经上传好的xml，校验结构和数据是否正确，并且将正确数据入库
 * 相关帖子：
 * 	https://jira.succez.com/browse/CSTM-20635
 */

import { SDILog, LogType, isEmpty, ResultInfo } from "/sysdata/app/zt.app/commons/commons.action";
import dw from "svr-api/dw";
import db from "svr-api/db";
import utils from "svr-api/utils";
import sys from "svr-api/sys";
import fs from "svr-api/fs";
import security from "svr-api/security";
import bean from "svr-api/bean";
import metadata from "svr-api/metadata";
import util from "svr-api/utils";

let logger = new SDILog({ name: "custom.xml" });
let ds = db.getDefaultDataSource();
import DocumentFactory = org.dom4j.DocumentFactory;
import DefaultElement = org.dom4j.tree.DefaultElement;
import DefaultDocument = org.dom4j.tree.DefaultDocument;


const Timestamp = Java.type('java.sql.Timestamp');

function main() {
	let { wordId, requestParams } = { "wordId": "ywHZekkkxoGBnivEYbxPnC", requestParams: { 'EMPIID': 'E99119945D47692AE053A10AA8C04140' } };
	if (wordId == null || requestParams == null) {
		return { result: false, message: "缺失参数" }
	}
	let queryInfo: QueryInfo = {
		sources: [{
			id: "model1",
			path: "/HLHT/data/tables/xml/SDI_HLHT_WORD_INFO.tbl"
		}],
		fields: [{
			name: "WORD_NAME", exp: "model1.WORD_NAME"
		}, {
			name: "MAIN_TABLE", exp: "model1.MAIN_TABLE"
		}, {
			name: "TABLE_DIR_PATH", exp: "model1.TABLE_DIR_PATH"
		}],
		filter: [{
			exp: "model1.WORD_ID=param1"
		}],
		params: [{
			name: "param1", value: wordId
		}]
	}
	let queryResult = dw.queryData(queryInfo);
	let rows = queryResult.data;
	if (rows.length == 0) {
		return { result: false, message: "查询不到当前文档信息" }
	}
	let wordInfo: WordInfo = {
		wordId,
		wordName: rows[0][0] as string,
		mainDbTable: rows[0][1] as string,
		tableDirPath: rows[0][2] as string
	}
	logger.addLog(`查询到文档信息为${JSON.stringify(wordInfo)}`);
	let wordStructureService = new WordStructureService({ wordId });
	print(wordStructureService.getWordNodes().length);
	let cdaMetaService = new CDAMetaService({ wordId });
	let nodesDataService = new NodesDataService({
		wordInfo, wordStructureService, requestParams, metaData: cdaMetaService.getMetaData()
	});
	try {
		nodesDataService.init();
	} catch (e) {
		throw e;
		logger.addLog("节点信息初始化出现异常");
		logger.addLog(e);
		logger.fireEvent();
		return {
			result: false, message: "初始化节点信息失败"
		}
	}
	logger.addLog(wordStructureService.getWordNodes())
	let xmlConstitueService = new XmlConstitueService({
		wordInfo: wordInfo,
		nodesDataservice: nodesDataService,
		logger: logger, wordStructureService
	});
	let xmlGenerateResult: ResultInfo = xmlConstitueService.generateXml();
	print(xmlGenerateResult)
	if (!xmlGenerateResult.result) {
		return xmlGenerateResult;
	}
	let xmlStr: string = xmlGenerateResult.data;
	if (xmlStr == null) {
		return { result: false, message: "生成xml失败" };
	}
	logger.fireEvent();
	return { result: true, xmlStr: xmlStr };
}

/**
 * 将数据集转为xml总入口
 * @param request
 * @param response
 * @param params
 */
export function execute(request: HttpServletRequest, response: HttpServletResponse, params: {
	wordId: string, requestParams: JSONObject
}) {
	let { wordId, requestParams } = params;
	if (wordId == null || requestParams == null) {
		return { result: false, message: "缺失参数" }
	}
	let queryInfo: QueryInfo = {
		sources: [{
			id: "model1",
			path: "/HLHT/data/tables/xml/SDI_HLHT_WORD_INFO.tbl"
		}],
		fields: [{
			name: "WORD_NAME", exp: "model1.WORD_NAME"
		}, {
			name: "MAIN_TABLE", exp: "model1.MAIN_TABLE"
		}, {
			name: "TABLE_DIR_PATH", exp: "model1.TABLE_DIR_PATH"
		}],
		filter: [{
			exp: "model1.WORD_ID=param1"
		}],
		params: [{
			name: "param1", value: wordId
		}]
	}
	let queryResult = dw.queryData(queryInfo);
	let rows = queryResult.data;
	if (rows.length == 0) {
		return { result: false, message: "查询不到当前文档信息" }
	}
	let wordInfo: WordInfo = {
		wordId,
		wordName: rows[0][0] as string,
		mainDbTable: rows[0][1] as string,
		tableDirPath: rows[0][2] as string
	}
	logger.addLog(`查询到文档信息为${JSON.stringify(wordInfo)}`);
	let wordStructureService = new WordStructureService({ wordId });
	let cdaMetaService = new CDAMetaService({ wordId });
	let nodesDataService = new NodesDataService({
		wordInfo, wordStructureService, requestParams, metaData: cdaMetaService.getMetaData()
	});
	try {
		nodesDataService.init();
	} catch (e) {
		logger.addLog("节点信息初始化出现异常");
		logger.addLog(e);
		logger.fireEvent();
		return {
			result: false, message: "初始化节点信息失败"
		}
	}
	let xmlConstitueService = new XmlConstitueService({
		wordInfo, wordNodeDatas: nodesDataService,
		logger,
		wordStructureService
	})
	let xmlGenerateResult: ResultInfo = xmlConstitueService.generateXml();
	if (!xmlGenerateResult.result) {
		return xmlGenerateResult;
	}
	let xmlStr: string = xmlGenerateResult.data;
	if (xmlStr == null) {
		return { result: false, message: "生成xml失败" };
	}
	logger.fireEvent();
	return { result: true, xmlStr: xmlStr };
}

/**
 * 2022-11-16 丁煜
 * https://jira.succez.com/browse/CSTM-21261
 * 增加互联互通初始化xml界面
 */
export function initializeXml(request: HttpServletRequest, response: HttpServletResponse, params: { fileInfoArr }) {
	const fileInfoArr = params.fileInfoArr;
	let list = [];
	let result = { result: true, message: "xml文档初始化成功" };
	for (let i = 0; i < fileInfoArr.length; i++) {
		let fileId = fileInfoArr[i].id;
		let fileName = fileInfoArr[i].name.split(".xml")[0];
		let file = getUploadFile(fileId);
		let xmlString = file.readString();
		let wordId = fileId.split("-")[0]
		let xml = new XMLutil(xmlString, wordId);
		let isSucces = xml.xmlAnalyze();
		if (isSucces == false) {
			result = { result: false, message: "xml文档初始化过程中部分节点属性有误，详细信息请查看SDI_HLHT_WORD_ERROR_LOG表" };
		}
		let insertRow = [wordId, fileName];
		list.push(insertRow);
	}
	let ds = db.getDefaultDataSource();
	let table = ds.openTableData("SDI_HLHT_WORD_INFO");
	table.insert(list, ["WORD_ID", "WORD_CODE"]);
	return result;
}

/**
 * 2022-10-17 丁煜
 * https://jira.succez.com/browse/CSTM-20823
 * 在spg中实现xml上传功能，并且在后端脚本中获取到上传的文件
 */
export function getFileCheckXml(request: HttpServletRequest, response: HttpServletResponse, params: { fileId: string }) {
	const fileId = params.fileId;
	let file = getUploadFile(fileId);
	let xml = new UploadXml();
	let xmlString = file.readString()
	let result = xml.xmlCheck(xmlString, file);
	return result;
}

/**
 * 获取上传的文件
 */
function getUploadFile(fileId: string) {
	const user = security.getCurrentUser();
	let userId = user?.userInfo.userId;
	if (!userId) {
		userId = "admin"
	}
	const tempUploadFileService = bean.getBean("com.succez.commons.service.impl.io.TempUploadFileServiceImpl");
	const fileObject = tempUploadFileService.getUploadFile(userId, fileId);
	const filePath = fileObject.getPath();
	let file = fs.getFile(filePath);
	return file;
}

/**
 * 遍历xml并储存节点信息
 */
class XMLutil {
	constructor(xml, fileId) {
		this.xml = xml;
		this.wordId = fileId;
		this.proCheck();
	}
	/**互联互通节点值 */
	private dissimilarityNodeValue = "hlhtNodeValue";
	/**xml对象 */
	private xml;
	/**需要写入文档结构表的对象数组 */
	private structureAddArr: any[] = [];
	/**需要写入节点信息表的对象数组 */
	private nodeInfoAddArr: any[] = [];
	/**文档ID */
	private wordId: string;
	/**是否为叶子结点 */
	private isLeaf: number = 0;
	/**当前用户 */
	private creator = security.getCurrentUser()?.userInfo.userId;
	/**排序字段 */
	private compositor: number = 0;
	/**物理表名 */
	private tableName = null;
	/**物理字段名 */
	private filedName = null;
	/**内部标识符 */
	private internalIdentifier;
	/**错误日志数组 */
	private errorLog: any[] = [];
	/**章节名称 */
	private chapterName = null;

	/**
	 * 预先检查
	 */
	private proCheck() {
		if (typeof this.xml == "string") {
			this.xml = util.parseXML(this.xml);
		}
	}

	/**
	 * 递归遍历XML所有子节点
	 */
	private recursion(xmlNodeArr, parentId: string, level: number, levelIdArr) {
		for (let i = 0; i < xmlNodeArr.length; i++) {
			let xmlNode = xmlNodeArr.item(i)
			let nodeName = xmlNode.getNodeName();
			let nodeValue = xmlNode.getNodeValue();
			let nodeId = util.uuid();
			if (nodeName == "#text" || nodeName == "#comment") {
				let value = nodeValue.replace(/\s/g, "")
				if (nodeName == "#text" && value != "" && nodeValue != ">\n" && nodeValue != "\n\n\t") {
					let result = this.checkNodeValue(nodeValue, nodeId);
					this.tableName = result['tableName'];
					this.filedName = result['filedName'];
					this.nodeInfoAddArr.push({
						"NODE_ID": parentId,
						"NAME": this.dissimilarityNodeValue,
						"WORD_ID": this.wordId,
						"DEFAULT_VALUE": nodeValue,
						"CREATOR": this.creator,
						"CREATE_TIME": new java.util.Date(),
						"DB_TABLE": this.tableName,
						"DB_FIELD": this.filedName,
						"INTER_IDENTIFITER": this.internalIdentifier
					})
				} else if (nodeName == "#comment") {
					this.chapterName = nodeValue
				}
				continue;
			}
			levelIdArr.push(nodeId);
			let childNodeArr = xmlNode.getChildNodes();
			if (childNodeArr.length == 0) {
				this.isLeaf = 1;
			} else {
				this.isLeaf = 0;
			}
			if (!this.creator) {
				this.creator = "admin"
			}
			this.compositor = this.compositor + 1
			let data = {
				"NODE_ID": nodeId,
				"P_NODE_ID": parentId,
				"NODE_NAME": nodeName,
				"WORD_ID": this.wordId,
				"SZ_LEVEL": level,
				"SZ_LEAF": this.isLeaf,
				"CREATOR": this.creator,
				"CREATE_TIME": new java.util.Date(),
				"SORT_FIELD": this.compositor,
				"SECTION_NAME": this.chapterName
			};
			let maxLevel = metadata.getJSON("/HLHT/data/tables/xml/SDI_HLHT_WORD_STRUCTURE.tbl").hierarchies[0].hierarchyFields.length;
			for (let k = 0; k < maxLevel; k++) {
				if (k > levelIdArr.length) {
					data[`SZ_PID${k}`] = null
				} else {
					data[`SZ_PID${k}`] = levelIdArr[k];
				}
			}
			this.structureAddArr.push(data);
			if (this.chapterName != null) {
				this.chapterName = null;
			}
			let attributesArr = xmlNode.getAttributes();
			if (attributesArr) {
				for (let o = 0; o < attributesArr.length; o++) {
					let attributes = attributesArr.item(o)
					let result = this.checkNodeValue(attributes.value, nodeId);
					this.tableName = result['tableName'];
					this.filedName = result['filedName'];
					this.nodeInfoAddArr.push({
						"NODE_ID": nodeId,
						"NAME": attributes.name,
						"WORD_ID": this.wordId,
						"DEFAULT_VALUE": attributes.value,
						"CREATOR": this.creator,
						"CREATE_TIME": new java.util.Date(),
						"DB_TABLE": this.tableName,
						"DB_FIELD": this.filedName,
						"INTER_IDENTIFITER": this.internalIdentifier
					})
				}
			}
			if (childNodeArr.length != 0) {
				this.recursion(childNodeArr, nodeId, level + 1, levelIdArr);
			}
			levelIdArr.pop();
		}
	}

	/**
	 * 校验是否包含$
	 */
	private checkNodeValue(node: string, nodeId: string) {
		if (node && node.indexOf("$") != -1) {
			if (node.indexOf(".") == -1) {
				this.internalIdentifier = "error";
				this.errorLog.push({ "NODE_ID": nodeId, "MESSAGE": "节点值或节点属性值有误", "NODE": node, "WORD_ID": this.wordId });
				return { "tableName": null, "filedName": null }
			}
			this.internalIdentifier = "!";
			let result = {};
			let nodeArr = node.split(".");
			result["tableName"] = nodeArr[0].split("{")[1];
			let value = nodeArr[1]
			if (value.indexOf("!") != -1) {
				result["filedName"] = value.split("!")[0];
			} else if (value.indexOf("?") != -1) {
				result["filedName"] = value.split("?")[0];
			} else {
				result["filedName"] = value.split("}")[0];
			}
			return result
		} else {
			this.internalIdentifier = null;
		}
		return { "tableName": null, "filedName": null }
	}

	/**解析xml并获取节点属性与信息入库 */
	public xmlAnalyze() {
		let levelArr = [];
		this.recursion(this.xml.getChildNodes(), null, 0, levelArr);
		let result = this.writeTable();
		return result
	}

	/**
	 * 写入指定表中
	 */
	private writeTable() {
		let ds = db.getDefaultDataSource();
		let nodeInfo = ds.openTableData("SDI_HLHT_NODE_INFO");
		let structure = ds.openTableData("SDI_HLHT_WORD_STRUCTURE");
		nodeInfo.insert(this.nodeInfoAddArr);
		structure.insert(this.structureAddArr)
		if (this.errorLog.length > 0) {
			let error = ds.openTableData("SDI_ERROR_COPY");
			error.insert(this.errorLog);
			return false;
		}
		return true;
	}
}

/**
 * XML信息处理
 */
export class MgrXmlContentService {

	/** 文档信息 */
	private wordInfo: WordInfo;
	/** 附件主键ID */
	private attachmentPkId: string;
	/** 构成XML的所有文档节点数据 */
	private nodesDataservice: NodesDataService;
	/** 文档创作者标识符 */
	private wordCreateId: string;
	/** 文档创作者姓名 */
	private wordCreateAuthor: string;
	/** 医疗机构编码 */
	private hospitalId: string;
	/** 医疗机构名称 */
	private hospitalName: string;

	/** XML构建类 */
	private xmlConstitueService: XmlConstitueService;
	/**
	 * XML附件信息, eg: {
	 * 		"主键ID1": {
	 * 			WORD_ID: "",
				EMPIID: "",
				WORD_INDEX: "",
				WORD_SERIAL_NUMBER: "",
				VERSION_NUMBER: "",
				FILE_PATH: "",
				CONTENT_MD5: ""
	 * 		}, {
	 * 		  "主键ID2": {
	 * 			...
	 * 		}
	 * }
	 */
	private xmlAttachmentInfos: XMLAttachmentInfo;
	/** 操作的数据源 */
	private optionDb: Datasource;
	/** 日志对象 */
	private logger: SDILog;

	constructor(args: {
		wordInfo?: WordInfo,
		attachmentPkId?: string,
		nodesDataservice: NodesDataService,
		wordCreateId: string,
		wordCreateAuthor: string,
		hospitalId: string,
		hospitalName: string,
		logger: SDILog
	}) {
		this.wordInfo = args.wordInfo as WordInfo;
		this.attachmentPkId = args.attachmentPkId as string;
		this.nodesDataservice = args.nodesDataservice;
		this.wordCreateId = args.wordCreateId;
		this.wordCreateAuthor = args.wordCreateAuthor;
		this.hospitalId = args.hospitalId;
		this.hospitalName = args.hospitalName;
		this.logger = args.logger;
		this.initParams();
	}

	/**
	 * 初始化参数
	 */
	private initParams(): void {
		let wordStructureService = new WordStructureService({ wordId: this.wordInfo.wordId })
		this.xmlConstitueService = new XmlConstitueService({
			wordInfo: this.wordInfo,
			nodesDataservice: this.nodesDataservice,
			logger: this.logger,
			wordStructureService: wordStructureService
		});
		this.optionDb = db.getDataSource("HLHT");
		this.xmlAttachmentInfos = {};
	}

	/**
	 * 处理XML附件信息, 并将其生成的XML存储到磁盘目录下
	 * @description 操作结束后, 会将附件信息返回, 不执行入库操作
	 */
	public dealXmlAttachmentInfo(): ResultInfo {
		this.logger.addLog(`开始执行【dealXmlAttachmentInfo】处理XML附件信息`);
		let preResult: ResultInfo = this.preCheck();
		if (!preResult.result) {
			logger.addLog("未通过预检查, 执行结束");
			return preResult;
		}
		let xmlCreateResult: ResultInfo = this.xmlConstitueService.generateXml();
		if (!xmlCreateResult.result) {
			return xmlCreateResult;
		}
		if (Object.keys(this.xmlAttachmentInfos).length == 0) {
			this.queryXMLAttachmentInfo();
		}
		let xmlContent: string = xmlCreateResult.data;

		let resultInfo: ResultInfo = { result: true };
		let wordInfo: WordInfo = this.wordInfo;
		let attachmentOid: string = this.getXMLAttachmentOid();
		let xmlFileName: string = `${attachmentOid}_${wordInfo.wordName}.xml`;
		this.logger.addLog(`当前处理的XML文件名为: [${xmlFileName}]`);
		let saveXmlFileResult: ResultInfo = this.createXMLFile(xmlContent, xmlFileName);
		if (!saveXmlFileResult.result) {
			return saveXmlFileResult;
		}
		let xmlAttachmentInfo: XMLAttachmentInfo = this.xmlAttachmentInfos[this.attachmentPkId]; // 获取当前附件ID对应的附件信息
		/** xml内容加密后的标记值, 用于对比每次生成后的差异 */
		let xmlMd5Mark: string = utils.md5(xmlContent);
		/** xml文件存储信息, eg: default:文件名.xml */
		let saveFileName: string = `workdir:${xmlFileName}`; // 附件前缀根模型对应的附件字段设置有关, 此处设置: 存储工作空间, 故: workdir

		if (!xmlAttachmentInfo || Object.keys(xmlAttachmentInfo).length == 0) {
			this.logger.addLog(`当前XML附件信息未生成, 开始初始化附件信息`);
			let newAddAttachment: XMLAttachmentInfo = {
				WORD_ID: this.wordInfo.wordId,
				EMPIID: this.attachmentPkId,
				WORD_INDEX: utils.uuid(),
				WORD_SERIAL_NUMBER: attachmentOid,
				VERSION_NUMBER: "1.0",
				CREATE_TIME: new Timestamp(Date.now()),
				FILE_PATH: saveFileName,
				CONTENT_MD5: xmlMd5Mark,
				AUTHOR_ID: this.wordCreateId,
				AUTHOR_NAME: this.wordCreateAuthor,
				ORGANIZATION_ID: this.hospitalId,
				ORGANIZATION_NAME: this.hospitalName,
				CUSTODIAN_ID: "0000030001",
				CUSTODIAN_NAME: "上海市卫生健康委员会"
			}
			resultInfo.newAddAttachment = newAddAttachment;
		} else {
			let beforeXmlMd5Mark = xmlAttachmentInfo.CONTENT_MD5 as string;
			let versionNumber = xmlAttachmentInfo.VERSION_NUMBER as string;
			this.logger.addLog("开始比较新旧xml文档")
			if (beforeXmlMd5Mark != xmlMd5Mark) {
				this.logger.addLog(`当前XML内容存在更新, 更新附件信息`);
				let newVersion: string = (Number(versionNumber) + 1).toString();
				this.logger.addLog(`当前XML内容存在更新, 更新附件信息, 原版本号: ${versionNumber} , 新版本号: ${newVersion}`);
				let updateXMLAttachment: XMLAttachmentInfo = {
					WORD_ID: this.wordInfo.wordId,
					EMPIID: this.attachmentPkId,
					VERSION_NUMBER: newVersion,
					CONTENT_MD5: xmlMd5Mark,
					WORD_SERIAL_NUMBER: attachmentOid,
					FILE_PATH: saveFileName,
					MODIFY_TIME: new Timestamp(Date.now())
				};
				resultInfo.updateXMLAttachment = updateXMLAttachment;
				let xmlRootNode = this.xmlConstitueService.getXmlRootNode();
				this.xmlConstitueService.updateSignNodeValue(xmlRootNode, "versionNumber", newVersion); // 修改XML节点版本
			} else {
				this.logger.addLog(`当前XML内容没有发生改变`);
			}
		}
		// this.logger.addLog(resultInfo);
		this.logger.addLog(`执行结束【dealXmlAttachmentInfo】处理XML附件信息`);
		return resultInfo;
	}

	/**
	 * 预先检查
	 */
	private preCheck(): ResultInfo {
		if (isEmpty(this.wordInfo)) {
			return { result: false, message: "参数文档信息未传入" };
		}
		if (isEmpty(this.attachmentPkId)) {
			return { result: false, message: "参数附件主键ID未传入" };
		}
		if (isEmpty(this.wordInfo.wordId)) {
			return { result: false, message: "文档ID未传入" };
		}
		if (!this.nodesDataservice) {
			return { result: false, message: "构成XML的所有文档节点数据对象为空" };
		}
		return { result: true };
	}

	/**
	 * 在指定磁盘路径下创建XML文件
	 * @param xmlContent 字符串XML内容
	 * @param xmlFileName xml文件名: (附件流水号+文档名).xml
	 * @return
	 *
	 * @description 若文件已存在, 则覆盖
	 * @description 文件放置附件目录下, 可通过SPG交互：下载文件 或者 预览文件
	 */
	private createXMLFile(xmlContent: string, xmlFileName: string): ResultInfo {
		if (isEmpty(xmlContent)) {
			return { result: false, message: `存储的XML内容为:[${xmlContent}], 不符合, 保存磁盘失败` };
		}
		if (isEmpty(xmlFileName)) {
			return { result: false, message: `XML文件名为空, 创建XML文件失败` };
		}
		let resultInfo: ResultInfo = { result: true };
		let xmlJavaStr = new java.lang.String(xmlContent);
		let xmlByteContent: number[] = xmlJavaStr.getBytes();

		let xmlFilePath: string = this.getXMLSavePath(xmlFileName);

		let xmlFile = new java.io.File(xmlFilePath);
		if (!xmlFile.exists()) {
			let parentFile = xmlFile.getParentFile();
			if (!parentFile.exists()) {
				parentFile.mkdirs(); // 若文件父目录不存在，则创建文件以及父文件目录
			}
			xmlFile.createNewFile();
		}
		xmlFile = fs.getFile(xmlFilePath);
		let outStream;
		try {
			this.createAttachmentMetaFile(xmlFilePath, xmlFileName, xmlByteContent.length);
			outStream = xmlFile.openOutputStream();
			outStream.write(xmlByteContent);
			this.logger.addLog(`XML文件: [${xmlFilePath}] 创建成功`);
		} catch (e) {
			// xmlFile.delete();
			resultInfo.result = false;
			resultInfo.message = `创建XML文件: [${xmlFilePath}] 失败`;
			this.logger.addLog(`创建XML文件: [${xmlFilePath}] 失败----error`);
			this.logger.addLog(e);
		} finally {
			outStream && outStream.close();
		}
		return resultInfo;
	}

	/**
     * 获取XML在磁盘的存储路径
     * <p>
     *  1 附件存储格式要选择：工作目录路径
     * </p>
     * @param xmlFileName XML文件名, eg: test.xml
     * @return 存储的绝对路径, eg: /opt/workdir/clusters-share/file-storage/relativePath/2022110214454335601_1.个人基本健康信息登记.xml
     */
    public getXMLSavePath(xmlFileName: string): string {
        let shareDir: string = sys.getClusterShareDir();
        let attachParentDir: string = `${shareDir}/file-storage/relativePath`;
        let xmlFilePath: string = `${attachParentDir}/${xmlFileName}`;
        return xmlFilePath;
    }

	/**
	 * 创建配置文件(txt.xml.meta)以供预览时读取文件对应数据
	 * @param xmlFilePath XML文件所存路径, eg: /afa/ata/text.xml
	 * @param xmlFileName XML文件名
	 * @param xmlFileSize XML文件大小
	 *
	 * @description 存储文件的附件信息（文件名、文件大小、文件路径）
	 */
	private createAttachmentMetaFile(xmlFilePath: string, xmlFileName: string, xmlFileSize: number): void {
		let metaFilePath: string = `${xmlFilePath}.meta`;
		let metaFile = new java.io.File(metaFilePath);
		if (!metaFile.exists()) {
			let createResult: boolean = metaFile.createNewFile();
			this.logger.addLog(`文件[${metaFilePath}] 创建状态: ${createResult}`);
		}
		metaFile = fs.getFile(metaFilePath);
		metaFile.writeJson({
			name: xmlFileName,
			size: xmlFileSize,
			lastModifyTime: new Timestamp(Date.now())
		});
		this.logger.addLog("创建配置文件(txt.xml.meta)以供预览时读取文件对应数据--完成");
	}

	/**
	 * 插入XML附件信息
	 * @param xmlAttachmentInfo XML文件附件信息
	 * @return
	 */
	public insertXMLAttachementInfo(xmlAttachmentInfo: XMLAttachmentInfo[]): ResultInfo {
		if (xmlAttachmentInfo.length == 0) {
			return { result: false, message: "插入XML附件信息为空" };
		}
		this.logger.addLog(`当前需要插入: [${xmlAttachmentInfo.length}] 条数据`);
		let resultInfo: ResultInfo = { result: true };
		let attachmentTable = this.optionDb.openTableData("SDI_HLHT_XML_ATTACH");
		attachmentTable.setAutoCommit(false);
		try {
			let insertNums: number = attachmentTable.insert(xmlAttachmentInfo);
			this.logger.addLog(`XML附件信息插入: [${insertNums}] 条`);
			attachmentTable.commit();
		} catch (e) {
			attachmentTable.rollback();
			resultInfo.result = false;
			resultInfo.message = "写入XML附件信息失败";
			this.logger.addLog(`插入XML附件信息失败----error`);
			this.logger.addLog(e);
		}
		return resultInfo;
	}

	/**
	 * 更新对应的XML附件信息
	 * @param updateXMLAttachments 更新的XML附件信息
	 *
	 * @description 每次生成的XML名都是唯一的(携带时间), 生成后会用加工将当前信息记录, 方便跟踪历史信息
	 * @description 同一个事务下提交更新
	 */
	public updateXMLAttachementInfo(updateXMLAttachments: XMLAttachmentInfo[]): ResultInfo {
		if (updateXMLAttachments.length == 0) {
			return { result: false, message: "没有需要更新的数据" };
		}
		this.logger.addLog(`当前需要更新: [${updateXMLAttachments.length}] 条附件信息`);
		let resultInfo: ResultInfo = { result: true };
		let attachmentTable = this.optionDb.openTableData("SDI_HLHT_XML_ATTACH");
		attachmentTable.setAutoCommit(false);
		let updateNums: number = 0;
		try {
			for (let i = 0; i < updateXMLAttachments.length; i++) {
				let updateXMLAttachment = updateXMLAttachments[i];
				let updateNum = attachmentTable.update({
					VERSION_NUMBER: updateXMLAttachment.VERSION_NUMBER,
					WORD_SERIAL_NUMBER: updateXMLAttachment.WORD_SERIAL_NUMBER,
					CONTENT_MD5: updateXMLAttachment.CONTENT_MD5,
					FILE_PATH: updateXMLAttachment.FILE_PATH,
					MODIFY_TIME: updateXMLAttachment.MODIFY_TIME
				}, {
					WORD_ID: updateXMLAttachment.WORD_ID,
					EMPIID: updateXMLAttachment.EMPIID,
				});
				updateNums = updateNums + updateNum;
			}
			attachmentTable.commit();
			this.logger.addLog(`成功更新XML附件信息: [${updateNums}] 条`);
		} catch (e) {
			attachmentTable.rollback();
			resultInfo.result = false;
			resultInfo.message = "更新XML附件信息失败";
			this.logger.addLog(`更新XML附件信息失败----error`);
			this.logger.addLog(e);
		}
		return resultInfo;
	}

	/**
	 * 根据文档ID获取对应XML附件信息
	 */
	private queryXMLAttachmentInfo(): void {
		try {
			let querySQL: string = "select * from SDI_HLHT_XML_ATTACH where WORD_ID = ? ";
			let queryData = this.optionDb.executeQuery(querySQL, [this.wordInfo.wordId]);
			this.logger.addLog("开始查询附件信息");
			for (let i = 0; i < queryData.length; i++) {
				let data: XMLAttachmentInfo = queryData[i];
				let primaryKeyId = data.EMPIID as string;
				this.xmlAttachmentInfos[primaryKeyId] = { // 数据表主键由: 文档ID和主键ID组合, 同文档ID下的主键ID不存在重复的
					WORD_ID: data.WORD_ID,
					EMPIID: data.EMPIID,
					WORD_INDEX: data.WORD_INDEX,
					WORD_SERIAL_NUMBER: data.WORD_SERIAL_NUMBER,
					VERSION_NUMBER: data.VERSION_NUMBER,
					FILE_PATH: data.FILE_PATH,
					CONTENT_MD5: data.CONTENT_MD5
				};
			}
		} catch (e) {
			this.logger.addLog(`文档ID获取对应XML附件信息失败----error`);
			this.logger.addLog(e);
		}
	}

	/**
	 * 更新记录已存在的XML附件信息
	 * @param newAddAttachment 新增的附件信息
	 * @description 由于新增附件信息是已知的, 直接记录到已存在附件变量中, 避免重新查库
	 */
	public updateXMLAttachmentInfo(newAddAttachment: XMLAttachmentInfo[]): void {
		for (let i = 0; i < newAddAttachment.length; i++) {
			let attachement: XMLAttachmentInfo = newAddAttachment[i];
			let primaryKeyId = attachement.EMPIID as string;
			this.xmlAttachmentInfos[primaryKeyId] = {
				WORD_ID: attachement.WORD_ID,
				EMPIID: primaryKeyId,
				WORD_INDEX: attachement.WORD_INDEX,
				WORD_SERIAL_NUMBER: attachement.WORD_SERIAL_NUMBER,
				VERSION_NUMBER: attachement.VERSION_NUMBER,
				FILE_PATH: attachement.FILE_PATH,
				CONTENT_MD5: attachement.CONTENT_MD5
			};
		}
	}

	/**
	 * 获取文档流水号
	 * @description 流水号 = 当前时间（精确毫秒）+ 自增长的两位序号
	 */
	private getXMLAttachmentOid(): string {
		let attachmentOid: string = "";
		let nowDate: string = utils.formatDate("yyyyMMddHHmmssSS");
		let sequence: NumberSequencer = db.getNumberSequencer(nowDate);
		let sequenceNums: number = sequence.next();
		if (sequence.next() < 10) {
			attachmentOid = `${nowDate}0${sequenceNums}`;
		} else {
			attachmentOid = `${nowDate}${sequenceNums}`;
		}
		return attachmentOid;
	}

	/**
	 * 重新设置主键值
	 * @param attachmentPkId 附件ID
	 */
	public setAttachPkId(attachmentPkId: string): void {
		this.attachmentPkId = attachmentPkId;
	}

	/**
	 * 重新设置XML构建类
	 * @param wordInfo 文档信息
	 * @param nodesDataservice 构成XML的所有文档节点数据对象
	 * @param wordStructureService 文档结构对象
	 */
	public setXmlConstitueService(wordInfo: WordInfo, nodesDataservice: NodesDataService, wordStructureService: WordStructureService): void {
		this.wordInfo = wordInfo;
		this.nodesDataservice = nodesDataservice;

		this.xmlAttachmentInfos = {};
		this.queryXMLAttachmentInfo(); // 更新当前设置文档的附件信息

		this.xmlConstitueService = new XmlConstitueService({
			wordInfo: this.wordInfo,
			nodesDataservice: this.nodesDataservice,
			logger: this.logger,
			wordStructureService: wordStructureService
		});
	}
}

/**
 * XML节点数据校验类
 * <p>
 *  1 tuzw 2022-10-12
 *   地址: https://jira.succez.com/browse/CSTM-20748
 *
 *  2 tuzw 2022-12-06
 *   问题：互联互通中获取维表数据方式修改
 *   地址：https://jira.succez.com/browse/CSTM-21490
 * </p>
 */
export class XmlNodeDataCheck {

	/** 默认数据源 */
	private defualtDb: Datasource;
	/** 文档ID */
	private wordId: string;
	/** 维表是否从db模块获取(默认: true), false则认为维表数据来自模型, 从模型查询维表数据匹配 */
	private isDbTable: boolean;

	/** 文档中文描述 */
	private wordName: string;
	/**
	 * 记录已查询的维表数据（查询的维表字段名一致, eg:{
	 *     DM_HLHT_SFFSDMB: [{
	 *          Z: "1",
	 *          ZHY: "",
	 *          SM: ""
	 *     }],
	 *     "HLHT_ETSCFYPJJGDMB": [{
	 *          ...
	 *      }]
	 * }
	 */
	private cacheAllDimTableDatas: JSONObject;
	/**
	 * 记录当前文档ID对应的所有元数据信息, eg: {
	 *     "HDSD00.01.366(内部标识符)": { // 重复以首次出现为主
	 *          metaDataIdentify: "元数据标识符",
	 *          metaDataName: "元数据名称",
	 *          metaDataType: "元数据类型",
	 *          expressForm: "表示格式",
	 *          dimTableName: "维表名"
	 *      },
	 *      {
	 *          ...
	 *      }
	 * }
	 */
	private cacheWordNodeMetaDataInfos: { [interIdentifiter: string]: MetaDataMappingTable };

	constructor(args: {
		wordId: string,
		metaDataService: CDAMetaService,
		isDbTable?: boolean
	}) {
		this.wordName = args.metaDataService?.getWordName();
		this.cacheAllDimTableDatas = {};
		this.cacheWordNodeMetaDataInfos = args.metaDataService?.getMetaData();
		this.defualtDb = db.getDefaultDataSource();
		this.isDbTable = args.isDbTable == undefined ? true : args.isDbTable;
	}

	/**
	 * 获取内部标识符对应维表数据
	 */
	public getIdentifierDimData(identifier: string) {
		if (isEmpty(identifier)) {
			return { result: true, message: "未传入内部标识符值" };
		}
		if (Object.keys(this.cacheWordNodeMetaDataInfos).length == 0) {
			return { result: false, message: `[${this.wordName}] 不存在对应的元数据信息, 请检测章节中文名是否正确` };
		}
		let metaDataDefineInfo: MetaDataMappingTable = this.cacheWordNodeMetaDataInfos[identifier.trim()];
		if (isEmpty(metaDataDefineInfo)) {
			return { result: false, message: `没有找到[${this.wordName}] 对应的内部标识符: [${identifier}]` };
		}
		let dimTableName: string = metaDataDefineInfo.dimTableName;
		if (isEmpty(this.cacheAllDimTableDatas[dimTableName])) { // 判断当前维表数据是否又被查询过
			if (this.isDbTable) {
				this.querySignDimTableData(dimTableName);
			} else {
				this.querySignModelData(dimTableName);
			}
		}
		let dimTableDatas: DimTableProperties[] = this.cacheAllDimTableDatas[dimTableName];
		return { result: true, dimTableDatas };
	}

	/**
	 * 校验当前值是否存在于内部标识符对应的维表描述字段内
	 * @param identifier
	 * @param fieldValue
	 */
	public checkValueIsInDimDesc(identifier: string, fieldValue: string): ResultInfo {
		let dimDataResult = this.getIdentifierDimData(identifier);
		let isExist = false;
		if (dimDataResult.result) {
			let dimData = dimDataResult.dimTableDatas;
			print(identifier)
			for (let i = 0; i < dimData.length; i++) {
				let data = dimData[i];
				if (data["SM"] == fieldValue) {
					isExist = true;
					break;
				}
			}
			if (!isExist) {
				let metaDataDefineInfo: MetaDataMappingTable = this.cacheWordNodeMetaDataInfos[identifier.trim()];
				let dimTableName: string = metaDataDefineInfo?.dimTableName;
				return {
					result: false, message: `当前值[${fieldValue}]不存在于指定维表${dimTableName}的描述字段内`
				}
			} else {
				return { result: true }
			}
		} else {
			return dimDataResult;
		}
	}

	/**
	 * 校验指定内部标识符对应的字段值 是否符合数据元标准（字段长度，字段类型，关联维表值）
	 * @param identifier 内部标识符值
	 * @param fieldValue 字段值（通过物理表名和字段名查询）
	 *
	 * @description
	 * 1) 根据文档名和内部标识符查询：数据源查询映射表（可开始将对应文档数据查询出来）
	 * 2）根据内部标识符 + 模块（文档中文名）查到需要校验的行数据（多条，取第一个）
	 *
	 * @return {
	 *      result: false,
	 *      checkResult: {
	 *          formCheckResult: {
	 *              result: false,
	 *              message: ""
	 *          },
	 *          typeCheckResult: {
	 *              result: false,
	 *              message: ""
	 *          }
	 *      }
	 * }
	 */
	public executeCheckFieldValueIsLegal(identifier: string, fieldValue: string | number): ResultInfo {
		if (isEmpty(identifier)) {
			return { result: true, message: "未传入内部标识符值" };
		}
		if (!fieldValue) {
			return { result: true, message: "校验值为空" };
		}
		if (Object.keys(this.cacheWordNodeMetaDataInfos).length == 0) {
			return { result: false, message: `[${this.wordName}] 不存在对应的元数据信息, 无法进行校验, 请检测章节中文名是否正确` };
		}
		let metaDataDefineInfo: MetaDataMappingTable = this.cacheWordNodeMetaDataInfos[identifier.trim()];
		if (isEmpty(metaDataDefineInfo)) {
			return { result: false, message: `没有找到[${this.wordName}] 对应的内部标识符: [${identifier}], 无法校验` };
		}
		let resultInfo: ResultInfo = { result: true };
		let expressForm: string = metaDataDefineInfo.expressForm;
		let formCheckResult: ResultInfo = this.checkExpressFormIsLegal(expressForm, fieldValue as string);

		let dimTableName: string = metaDataDefineInfo.dimTableName;
		let metaDataType: string = metaDataDefineInfo.metaDataType;
		let typeCheckResult: ResultInfo = this.checkValueTypeIsLegal(metaDataType, dimTableName, fieldValue);

		if (!formCheckResult.result || !typeCheckResult.result) {
			resultInfo.result = false;
			resultInfo.checkResult = {
				formCheckResult: formCheckResult,
				typeCheckResult: typeCheckResult
			}
		}
		return resultInfo;
	}

	/**
	 * 根据内部标识符对应的元数据定义，校验【值格式】是否符合
	 * @param format 格式, eg: AN..70
	 * @param fieldValue 检测的值
	 * @description 例如：AN..70 ： 长度 0 - 70 、AN,70 ： 固定长度 70
	 * @return
	 */
	private checkExpressFormIsLegal(format: string, fieldValue: string): ResultInfo {
		if (isEmpty(format)) {
			return { result: false, message: "表示格式值不存在, 无法校验" };
		}
		let actualFormatName: string = this.getActualFormatValue(format)?.replaceAll(" ", ""); // N
		let fieldValueType: string = typeof fieldValue;
		if (fieldValueType == "object") {
			fieldValueType = fieldValue?.getClass();
		}
		// 除去整型, 其他类型都值都是以字符串形式传入
		if (actualFormatName != "N" && !["string", "class java.lang.String"].includes(fieldValueType)) {
			return { result: false, message: `元数据定义类型: [${actualFormatName}], 当前值类型: [${fieldValueType}], 值: [${fieldValue}]` };
		}

		let checkResult: ResultInfo = { result: true };
		switch (actualFormatName) {
			case XmlNodeMetaDataFormat.A:
				checkResult = this.checkDataLenAndTypeIsLegal(format, actualFormatName, fieldValue);
				break;
			case XmlNodeMetaDataFormat.N:
				if (isEmpty(Number(fieldValue))) {  // 考虑到外部整型也是字符串形式，这里通过转换整型是否成功来判断是否整型
					checkResult = { result: false, message: `元数据定义类型: [${actualFormatName}], 当前值类型: [${fieldValueType}], 值:[${fieldValue}]` };
				} else {
					checkResult = this.checkDataLenAndTypeIsLegal(format, actualFormatName, fieldValue);
				}
				break;
			case XmlNodeMetaDataFormat.T:
				if (fieldValueType != "boolean" && !["false", "true"].includes(fieldValue)) {
					checkResult.result = false;
					checkResult.message = `值:[${fieldValue}]不为布尔类型`;
				}
				break;
			case XmlNodeMetaDataFormat.AN:
				checkResult = this.checkDataLenAndTypeIsLegal(format, actualFormatName, fieldValue);
				break;
			case XmlNodeMetaDataFormat.D8:
				checkResult = this.checkSignDateFormat("YYYYMMDD", fieldValue);
				break;
			case XmlNodeMetaDataFormat.T6:
				checkResult = this.checkSignDateFormat("hhmmss", fieldValue);
				break;
			case XmlNodeMetaDataFormat.DT15:
				checkResult = this.checkSignDateFormat("YYYYMMDD\'T\'hhmmss", fieldValue);
				break;
			default:
				checkResult.result = false;
				checkResult.message = `格式:[${format}]不存在文档表示格式中`;
		}
		return checkResult;
	}

	/**
	 * 根据内部标识符对应的元数据定义，校验【值类型】是否符合
	 * @param metaDataType 元数据类型
	 * @param dimTableName 维表名
	 * @param fieldValue 检测的值
	 * @return
	 */
	private checkValueTypeIsLegal(metaDataType: string, dimTableName: string, fieldValue: string | number): ResultInfo {
		if (isEmpty(metaDataType)) {
			return { result: false, message: "元数据类型为空, 无法校验" };
		}
		let resultInfo: ResultInfo = { result: true };
		let valueType: string = typeof fieldValue;
		switch (metaDataType) {
			case XmlNodeMetaDataType.S1:
				if (valueType != "string" && fieldValue?.getClass() != "class java.lang.String") {
					return { result: false, message: `值: [${fieldValue}] 类型不是: S1-字符型` };
				}
				break;
			case XmlNodeMetaDataType.S2:
				resultInfo = this.checkDimValueIsLegal(dimTableName, fieldValue as string);
				break;
			case XmlNodeMetaDataType.S3:
				resultInfo = this.checkDimValueIsLegal(dimTableName, fieldValue as string);
				break;
			case XmlNodeMetaDataType.L:
				if (valueType != "boolean" && !["false", "true"].includes(fieldValue)) {
					return { result: false, message: `[${fieldValue}]类型不是: L-布尔型` };
				}
				break;
			case XmlNodeMetaDataType.N:
				if (isEmpty(Number(fieldValue))) {
					return { result: false, message: `值: [${fieldValue}]类型不是: N-数值型` };
				}
				break;
			// 日期类型在校验格式已经处理过，此处不重复处理；二进制类型不存在，不处理
			case XmlNodeMetaDataType.D:
				break;
			case XmlNodeMetaDataType.DT:
				break;
			case XmlNodeMetaDataType.T:
				break;
			case XmlNodeMetaDataType.BY:
				break;
			default:
				resultInfo.result = false;
				resultInfo.message = `类型[${metaDataType}]不存在文档定义类型中`;
		}
		return resultInfo;
	}

	/**
	 * 根据内部标识符对应的元数据定义，校验值是否【存在与维表】中
	 * @param dimTableNameOrId 查询的维表名 或 模型ID
	 * @param dimIdValue 维表ID值（考虑到维表ID都为字符串类型, 在校验前, 将值转换为字符串）
	 * @return
	 */
	public checkDimValueIsLegal(dimTableNameOrId: string, dimIdValue: string): ResultInfo {
		let checkResult: ResultInfo = { result: true };
		dimIdValue = dimIdValue.toString();

		if (isEmpty(this.cacheAllDimTableDatas[dimTableNameOrId])) { // 判断当前维表数据是否又被查询过
			if (this.isDbTable) {
				this.querySignDimTableData(dimTableNameOrId);
			} else {
				this.querySignModelData(dimTableNameOrId);
			}
		}
		let dimTableDatas: DimTableProperties[] = this.cacheAllDimTableDatas[dimTableNameOrId];
		if (isEmpty(dimTableDatas) || dimTableDatas.length == 0) {
			// return { result: false, message: `[${dimTableNameOrId}]没有对应的数据信息` }; // 2022-12-07 为空合法
			return { result: true };
		}
		let existDimValues: string[] = [];
		for (let i = 0; i < dimTableDatas.length; i++) {
			let data: DimTableProperties = dimTableDatas[i];
			let dimId: string = data.Z?.toString(); // 产品查询出来的数据为JAVA类型, 这里转换一下
			if (!!dimId) {
				existDimValues.push(dimId);
			}
		}
		if (!existDimValues.includes(dimIdValue)) {
			return { result: false, message: `[${dimIdValue}]不在维表 [${dimTableNameOrId}]中` };
		}
		return checkResult;
	}

	/**
	 * 查询指定模型的维表数据
	 * @param modelId 模型ID
	 * <p>
	 * 	1 考虑到模型可以是一个视图, 采用dw模块查询
	 *  2 查询出来的数据转换为与db模块匹配维表结构一致, 维表匹配值对应的key, {
	 *     Z: "主键值",
	 *     ZHY: "",
	 * 	   SM: "描述"
	 *  }
	 * </P>
	 */
	private querySignModelData(modelId: string): void {
		if (!modelId) {
			return;
		}
		let { modelPath, modelPkField, modelPkTextField } = queryModelPathAndPkField(modelId);
		if (modelPath == "" || modelPkField == "") {
			return;
		}
		let query: QueryInfo = {
			sources: [{
				id: "model1",
				path: modelPath
			}],
			fields: [{
				name: modelPkField, exp: `model1.${modelPkField}`
			}]
		}
		if (modelPkTextField != null && modelPkTextField != "") {
			query.fields.push({
				name: modelPkTextField, exp: `model1.${modelPkTextField}`
			})
		}
		let queryData = dw.queryData(query).data;
		let modelDimValues: JSONObject[] = [];
		for (let i = 0; i < queryData.length; i++) {
			let data = queryData[i];
			modelDimValues.push({
				Z: data[0],
				ZHY: "",
				SM: data.length > 1 ? data[1] : ""
			});
		}
		this.cacheAllDimTableDatas[modelId] = modelDimValues;
	}

	/**
	 * 查询指定维表的全部数据
	 * @param dimTableName 查询的维表名
	 * <p>
	 *  1 从db模块查询对应的物理表维表数据
	 * </p>
	 */
	private querySignDimTableData(dimTableName: string): void {
		if (isEmpty(dimTableName)) {
			return;
		}
		let querySQL: string = `select * from ${dimTableName}`;
		try {
			let queryData = this.defualtDb.executeQuery(querySQL);
			this.cacheAllDimTableDatas[dimTableName] = queryData;
		} catch (e) {
			console.error(`查询维表: ${dimTableName} 数据失败----error`);
			console.error(e);
		}
	}

	/**
	 * 去除格式携带的说明部分，获取实际的格式值
	 * @param format 原记录的格式和说明，eg: N..4
	 * @return
	 *
	 * @description 考虑到格式存在: N..4 情况，从首字母匹配，去除说明，保留实际的格式值
	 */
	private getActualFormatValue(format: string): string {
		if (format == "T/F") { // 判断格式是否为布尔格式
			return "T/F";
		}
		let actualFormat: string = format;
		let formatFirstChar: string = format.substring(0, 1); // 获取格式的第一个字母
		if (["A", "N"].includes(formatFirstChar)) { //
			actualFormat = "";
			for (let i = 0; i < format.length; i++) {
				let char: number = format.charCodeAt(i);
				if (char < 65 || char > 97) { // 若遇到不为大写字母的符号，则结束
					break;
				}
				actualFormat = `${actualFormat}${format.charAt(i)}`;
			}
		}
		return actualFormat;
	}

	/**
	 * 根据定义的格式，校验【字符型和整形】值是否符合要求
	 * @param defineFormat 格式名+格式要求, eg: N...4
	 * @param actualFormatName 实际的格式名, eg: N
	 * @param fieldValue 检测的值
	 * @return
	 */
	public checkDataLenAndTypeIsLegal(defineFormat: string, actualFormatName: string, fieldValue: string | number): ResultInfo {
		let formatContent: string = defineFormat.substring(actualFormatName.length); // 去掉格式字符，获取格式要求, eg: 1..4
		let contents: string[] = formatContent.split(".."); // eg: ["4", "8,2"]
		if (contents.length == 0) {
			return { result: true, message: "当前字符串长度没有要求" };
		}
		fieldValue = fieldValue.toString();

		let actualSmallNumber: number = 0;
		let actualLen: number = this.getStrLen(fieldValue);
		/** 小数点位置 */
		let smallNumberIndex: number = fieldValue.indexOf(".");
		if (actualFormatName == "N" && smallNumberIndex != -1) { // 若当前类型为整形，则判断当前是否是浮点型数字
			actualSmallNumber = fieldValue.substring(smallNumberIndex + 1).length;
			actualLen = fieldValue.substring(0, smallNumberIndex).length;
		}
		/** 定义的小数位数 */
		let defineSmallNumber: number = -1;
		if (contents.length == 1) { // 若长度为1, 则为固定长度, eg: 4
			let parseResult = this.getDefineFormatMaxLenAndSmallNumber(contents[0], actualFormatName);// contents[01]: 4
			if (!parseResult.result) {
				return parseResult;
			}
			let maxLen: number = parseResult.data.maxLen;
			defineSmallNumber = parseResult.data.defineSmallNumber;

			if ((actualFormatName == "N" && actualLen <= maxLen) && (actualFormatName != "N" && actualLen != maxLen)) {
				return { result: false, message: `值:[${fieldValue}],实际长度:[${actualLen}]与定义长度:[${maxLen}]不相等` };
			}
		} else { // 若长度不为1，则允许动态长度, eg: 4..8,2
			let parseResult = this.getDefineFormatMaxLenAndSmallNumber(contents[1], actualFormatName); // contents[1]: 8,2
			if (!parseResult.result) {
				return parseResult;
			}
			let minLen: number = Number(contents[0]);
			let maxLen: number = parseResult.data.maxLen;
			defineSmallNumber = parseResult.data.defineSmallNumber;
			/**
			 * 2022-12-26 liuyz
			 * 对于数字可能会出现定义长度范围3-5，但是实际值为60，从实际来看这个是符合逻辑的，这里校验只对字符型生效
			 */
			if ((actualFormatName != "N" && actualLen < minLen) || actualLen > maxLen) {
				return { result: false, message: `实际长度:[${actualLen}],不在定义长度:[${minLen},${maxLen}]` };
			}
		}
		/**
		 * 若格式为整形,并且定义了小数位数,则校验小数位数是否符合
		 * 2022-12-07 tuzw
		 * 浮点型: 123.000 转换为字符串后：123, 小数位丢失了, 更改: 实际长度小于等于定义长度也符合
		 */
		if (actualFormatName == "N" && defineSmallNumber != -1 && actualSmallNumber > defineSmallNumber) {
			return { result: false, message: `值小数位数不符合,定义小数位数:[${defineSmallNumber}],实际小数位数:[${actualSmallNumber}]` };
		}
		return { result: true };
	}

	/**
	 * 解析设置的格式，获取限制的最大字符长度和允许的小数位数
	 * @param defineFormat 定义长度格式要求（此处的格式已在上一步被处理过）, eg: 8,2、4
	 * @param actualFormatName 实际的格式名, eg: N
	 * @return
	 */
	private getDefineFormatMaxLenAndSmallNumber(defineFormat: string, actualFormatName: string): ResultInfo {
		if (defineFormat.indexOf(".") != -1) { // eg: 5.1 、 .4
			return { result: false, message: `格式[${defineFormat}]不符合文档定义` };
		}
		let defineSmallNumber: number = -1; // 记录定义的小数位数，默认值为: -1，表示小数位数没有要求
		let maxLen: number = 0;
		let limitSmallNumber: string[] = defineFormat.split(","); // 以逗号划分, 判断是否有设置小数位数，字符型也走此逻辑，但划分数组长度一定为：1 eg: ["8", "2"]
		if (limitSmallNumber.length > 1) {
			defineSmallNumber = Number(limitSmallNumber[1]);  // 获取限制的小数位数
		}
		if (actualFormatName == "A") { // 若当前格式为A，则需要进一步判断是否允许多行
			let isAllRows: string[] = limitSmallNumber[0].split("X");
			if (isAllRows.length > 1) { // 若划分后长度大于1个，则允许多行，将定义的最大长度*可允许多行的数量
				maxLen = Number[isAllRows[0]] * Number[isAllRows[1]];
			} else {
				maxLen = Number(limitSmallNumber[0]);
			}
		} else {
			maxLen = Number(limitSmallNumber[0]);
		}
		return { result: true, data: { defineSmallNumber: defineSmallNumber, maxLen: maxLen } };
	}

	/**
	 * 获取字符串实际长度（区分中英文）
	 * @param str 获取的字符串值
	 * @return
	 */
	private getStrLen(str: string): number {
		let realLength = 0, len = str.length, charCode = -1;
		for (let i = 0; i < len; i++) {
			let charCode: number = str.charCodeAt(i);
			if (charCode >= 0 && charCode <= 128) {
				realLength += 1;
			} else {
				realLength += 2;
			}
		}
		return realLength;
	}

	/**
	 * 校验日期字符串值，是否符合指定格式
	 * @param dateFormat 格式化的日期格式，eg：yyyy-MM-dd HH:mm:ss
	 * @param data 日期字符串
	 * @return
	 */
	private checkSignDateFormat(dateFormat: string, data: string): ResultInfo {
		let checkResult: ResultInfo = { result: true };
		try {
			data = data.toString().replace("\u00a0", " "); // 日期中间的空格可能为：补换行空格，这里需要将其替换为普通空格
			let SimpleDateFormat = Java.type('java.text.SimpleDateFormat');
			let dateTimeStamp = new SimpleDateFormat(dateFormat);
			data = dateTimeStamp.parse(data); // 内部是java进行转换，这里若转换失败，则格式错误
		} catch (e) {
			checkResult.result = false;
			checkResult.message = `日期:[${data}]不符合格式: ${dateFormat}`;
			console.error(`日期:[${data}]不符合格式: ${dateFormat}`);
			console.error(e.toString());
		}
		return checkResult;
	}
}

/**
 * cda数据元服务类
 */
export class CDAMetaService {
	/** 文档ID */
	private wordId: string;
	/** 文档中文描述 */
	private wordName: string;

	private metaData: { [interIdentifiter: string]: MetaDataMappingTable };
	constructor(args: {
		wordId: string
	}) {
		this.wordId = args.wordId;
		this.wordName = this.parseActualWordName();
		this.metaData = {};
		this.querySignWordModuleInfos();
	}

	/**
	 * 根据文档ID解析实际的章节名称
	 * @description /HLHT/data/tables/xml/registerTable/门诊摘要信息 ——> 门诊摘要信息
	 */
	private parseActualWordName(): string {
		let queryInfo: QueryInfo = {
			sources: [{
				id: "model1",
				path: "/HLHT/data/tables/xml/SDI_HLHT_WORD_INFO.tbl"
			}],
			fields: [{
				name: "MODULE", exp: "model1.MODULE"
			}],
			filter: [{
				exp: `model1.WORD_ID = '${this.wordId}'`
			}]
		}
		let wordName: string = "";
		let queryDatas: (string | number | boolean)[][] = dw.queryData(queryInfo).data;
		if (queryDatas.length != 0) {
			wordName = queryDatas[0][0] as string;
		}
		return wordName;
	}

	/**
	 * 查询指定文档的所有数据源查询映射表信息
	 */
	private querySignWordModuleInfos(): void {
		let query: QueryInfo = {
			sources: [{
				id: "model1",
				path: "/HLHT/data/tables/xml/ODS_HLHT_YSJ.tbl"
			}],
			fields: [{
				name: "NBBSF", exp: "model1.NBBSF"
			}, {
				name: "YSJBSF", exp: "model1.YSJBSF"
			}, {
				name: "YSJMC", exp: "model1.YSJMC"
			}, {
				name: "YSJLX", exp: "model1.YSJLX"
			}, {
				name: "BSGS", exp: "model1.BSGS"
			}, {
				name: "TEXT1", exp: "model1.TEXT1"
			}],
			filter: [{
				exp: `model1.MK = '${this.wordName}'`
			}]
		}
		let queryDatas = dw.queryData(query).data;
		for (let i = 0; i < queryDatas.length; i++) {
			let data = queryDatas[i] as string[];
			let interIdentifiter: string = data[0];
			if (!isEmpty(this.metaData[interIdentifiter])) { // 校验当前内部标识符是否已经记录过
				continue;
			}
			let wordNodeMetaDataInfo: MetaDataMappingTable = {
				interIdentifiter,
				metaDataIdentify: data[1],
				metaDataName: data[2],
				metaDataType: data[3],
				expressForm: data[4],
				dimTableName: data[5]
			}
			this.metaData[interIdentifiter] = wordNodeMetaDataInfo;
		}
		console.info(`当前文档: 【${this.wordName}】 存在: 【${queryDatas.length}】 条数据元映射信息`);
	}

	public getMetaData() {
		return this.metaData;
	}

	public getWordName() {
		return this.wordName;
	}
}

/**
 * XML构建类
 * 根据结构配置和节点数据信息构建xml
 */
export class XmlConstitueService {

	/** 文档信息 */
	private wordInfo: WordInfo;
	/** 文档ID */
	private wordId: string;
	/** 根节点 */
	private xmlRootNode: DefaultElement;
	/** 文档结构信息 */
	private wordStructureService: WordStructureService;
	/** XML节点数据格式校验类 */
	private xmlNodeDataCheck: XmlNodeDataCheck;
	/** 构成XML的所有文档节点数据 */
	private nodesDataservice: NodesDataService;
	/**
	 * 记录整个XML节点属性存在多值的节点数据，eg：{
	 *      // 记录属性值为多个XML节点的共有的父节点ID（文档结构节点: 数据多不多类型, 多个父节点, 取最近）
	 *      // 父节点ID用于循环处理多属性时，每循环处理一个值，则复制一份父节点
	 *      "父节点ID(多对多)": {
	 *          parentNodeLevel: 1, // 当前父节点所在层级，处理后，改为：-1，避免重复处理
	 *          multiValueLen: 1, // 记录多个XML节点中多值属性值数量最少的个数，用于循环遍历
	 *          multiValueNodes: {
	 *              xmlNodeName（XML节点名）: [{
	 *                  nodeId: 文档结构节点ID,
	 *                  name: XML属性名,
	 *                  value: XML节点属性值，若为数组，则视为：多值，eg: ["1", "2"]
	 *              },{
	 *                  ...
	 *                  value: ["3"]
	 *              }],
	 *              "xxx": [{...}]
	 *          }
	 *      },
	 *      "XXX2": {...}
	 * }
	 */
	private multiPropertyValueXmlNodes: JSONObject;
	/** 日志对象 */
	private logger: SDILog;

	constructor(args: {
		wordInfo: WordInfo,
		nodesDataservice: NodesDataService,
		logger: SDILog,
		wordStructureService: WordStructureService
	}) {
		this.wordInfo = args.wordInfo;
		this.nodesDataservice = args.nodesDataservice;
		this.wordId = this.wordInfo.wordId;
		this.logger = args.logger;
		this.wordStructureService = args.wordStructureService;

		let cDAMetaService = new CDAMetaService({
			wordId: this.wordId
		})
		this.xmlNodeDataCheck = new XmlNodeDataCheck({
			wordId: this.wordId,
			metaDataService: cDAMetaService,
			isDbTable: false
		});
		this.multiPropertyValueXmlNodes = {};
	}

	/**
	 * 生成XML
	 * @description XML文档只能有一个顶层元素 Root。如果你多个并列的元素放在一起的话是不合XML规范的
	 * @description
	 * 1）节点可重复，属性不可重复
	 * 2）顶层XML节点属性名可必须包含：冒号，其余节点属性不可包含
	 * @return
	 */
	public generateXml(): ResultInfo {
		this.logger.addLog(`开始执行【generateXml】, 生成XML`);
		let checkDataFormatResult: ResultInfo = this.checkXmlNodeDataValidity();
		if (!checkDataFormatResult.result) {
			return checkDataFormatResult;
		}
		let wordRootNodeId: string = this.wordStructureService.getRootNodeId();
		if (!wordRootNodeId) {
			return { result: false, message: "XML顶层节点只有一个，上传的构造节点中存在多个根节点，不符合" };
		}
		this.logger.addLog(`文档节点：${this.wordId} 对应根节点：${wordRootNodeId}`);
		let factory = DocumentFactory.getInstance();
		let document: DefaultDocument = factory.createDocument();

		let rootNodeInfo: WordStructure = this.wordStructureService.getNodeInfo(wordRootNodeId) as WordStructure;
		if (isEmpty(rootNodeInfo)) {
			this.logger.addLog(`没有查询到节点：${wordRootNodeId} 的信息，越过`);
			return { result: false, message: `根节点: [${wordRootNodeId}]没有节点信息` };
		}
		let rootNodeName: string = rootNodeInfo.nodeName;
		let rootNode: DefaultElement = document.addElement(rootNodeName);
		this.xmlRootNode = rootNode;
		this.createXmlTree(rootNodeInfo, rootNode, 0); // 根节点
		this.logger.addLog("XML构建完成");
		this.dealMutilValueXmlNodeProperties();

		let xml = document.asXML();
		//this.logger.addLog(utils.formatXML(xml));
		this.logger.addLog(`执行结束【generateXml】, 生成XML`);
		return { result: true, data: xml };
	}

	/**
	 * 创建XML结构树
	 * @param nodeInfo 节点文档结构信息
	 * @param xmlNode 节点
	 * @param level 层级，第一层：0
	 */
	private createXmlTree(nodeInfo: WordStructure, xmlNode: DefaultElement, level: number): void {
		let xmlNodeName: string = xmlNode.getName();
		let nodeId: string = nodeInfo.nodeId;
		let nodePropertys: NodeData[] = this.nodesDataservice.getNodeProperty(nodeId);
		if (!nodePropertys || nodePropertys.length == 0) { // 校验当前节点是否有设置属性
			nodePropertys = [];
		}
		for (let propertyIndex = 0; propertyIndex < nodePropertys.length; propertyIndex++) {
			let nodeProperty: NodeData = nodePropertys[propertyIndex];
			let propertyName: string = nodeProperty.name;
			let propValueFromNode: string = nodeProperty.valueFromNode;
			let propetiesValue: string[] | string = nodeProperty.value;
			if (propetiesValue.length == 0) {
				propetiesValue = nodeProperty.defaultValue;
			}
			/**
			 * 2022-12-20 liuyz
			 * 如果一个属性设置了valueFromNode，那么要获取指定属性的值对应维表的描述
			 */
			if (propValueFromNode != null && propValueFromNode != "") {
				nodePropertys.forEach(prop => {
					if (prop.name == propValueFromNode) {
						propetiesValue = this.getNodeValueDescFromDim(prop.interIdentifiter, prop.value);
						return;
					}
				})
			}
			if (propetiesValue == null || isEmpty(propetiesValue) || propetiesValue.length == 0) { // 若节点属性值为空, 则不创建属性和设置值
				continue;
			}
			/**
			 * 校验节点属性是否存在多属性值，eg：
			 *   属性1：1,2,3  属性2：4,5  （以长度最少的作为循环次数，找到他们共有的1..*的父节点作为XML节点）
			 *   首次处理：1,4，后续循环处理：2,5  （每次循环除多值不同，单值也需重复创建）
			 */
			if (typeof propetiesValue != 'string') { // 若value为数组，则记录，用于后续循环重新生成节点（节点属性相同-复制，多值属性-值不同）
				let nodePropertyMultiValueNode: XmlNodeMutilValueProperties = {
					nodeId: nodeId,
					name: propertyName,
					values: propetiesValue
				}
				this.statisticsXmlMutilPropertiesValueNode(nodeId, xmlNodeName, nodePropertyMultiValueNode, level);
				propetiesValue = propetiesValue[0]; // 若节点属性存在多值，初次将首个元素作为属性value值，等XML构建完成后，根据记录整个XML节点属性存在多值的节点数据处理多属性值
			}
			if (propertyName == 'hlhtNodeValue') { // 若为互联互通节点名，则视为节点的文本值
				if (propetiesValue == null) { // 写入文本值不可为NULL, 若为NULL, 则写入空字符串
					propetiesValue = "";
				}
				xmlNode.addText(propetiesValue);
			} else {
				xmlNode.addAttribute(propertyName, propetiesValue);  // xmlNode.add(new DefaultAttribute(propertyName, propetiesValue));
			}
		}
		let childrenNodes: WordStructure[] = this.wordStructureService.getChildernNodeInfo(nodeId);
		for (let childrenIndex = 0; childrenIndex < childrenNodes.length; childrenIndex++) { // 递归构建子节点（从根节点依次往下构建: 高——>低）
			let childrenNode: WordStructure = childrenNodes[childrenIndex];
			let childrenNodeName: string = childrenNode.nodeName;
			if (childrenNode.hasComment) { // 判断当前节点是否有注释信息, 若存在, 则不处理其下孩子节点
				!childrenNode.allowNull && xmlNode.addComment(childrenNode.comment); // 设置注释
				continue;
			}

			let childrenXmlNode = xmlNode.addElement(childrenNodeName);
			this.createXmlTree(childrenNode, childrenXmlNode, level + 1);
		}
		this.wordStructureService.setNodeXmlNode(nodeId, xmlNode); // 等待内部子节点构建完成后，设置当前文档结构节点对应生成的XML节点
	}

	/**
	 * 统计每个XML节点属性存在多值的节点信息
	 * @description 顶层和第二层不统计，顶层节点固定: 1..1, 第二层节点多值默认取：首元素
	 *
	 * @param nodeId 文档结构ID
	 * @param xmlNodeName xml节点名称
	 * @param nodePropertyMultiValueNode 当前XML节点多值属性信息
	 * @param level 层级
	 */
	private statisticsXmlMutilPropertiesValueNode(
		nodeId: string,
		xmlNodeName: string,
		nodePropertyMultiValueNode: XmlNodeMutilValueProperties,
		level: number
	): void {
		if (level < 2) {
			return;
		}
		let parentNodeInfo = this.wordStructureService.querySignBaseParentNodeInfo(nodeId) as WordStructure;
		if (!parentNodeInfo) {
			this.logger.addLog(`节点: ${nodeId} 不存在多不多的父节点, 不记录其XML多值属性`);
			return;
		}
		let parentNodeId = parentNodeInfo.nodeId as string;
		if (isEmpty(parentNodeId)) { // 若父节点为空，则越过
			this.logger.addLog(`节点: ${nodeId} 父节点为空`);
			return;
		}
		let valueLen: number = nodePropertyMultiValueNode.values.length;
		if (!this.multiPropertyValueXmlNodes[parentNodeId]) { // 根据共有的父节点来记录 [nodePropertyMultiValueNode]
			this.multiPropertyValueXmlNodes[parentNodeId] = {
				parentNodeLevel: parentNodeInfo.level,
				multiValueLen: valueLen,
				multiValueNodes: {}
			};
		}
		let saveValueLen: number = this.multiPropertyValueXmlNodes[parentNodeId]["multiValueLen"];
		if (valueLen < saveValueLen) { // 记录共有的最小多值长度
			this.multiPropertyValueXmlNodes[parentNodeId]["multiValueLen"] = valueLen;
		}
		if (!this.multiPropertyValueXmlNodes[parentNodeId]['multiValueNodes'][xmlNodeName]) { // 根据XML节点名来归类
			this.multiPropertyValueXmlNodes[parentNodeId]['multiValueNodes'][xmlNodeName] = [nodePropertyMultiValueNode];
		} else {
			this.multiPropertyValueXmlNodes[parentNodeId]['multiValueNodes'][xmlNodeName].push(nodePropertyMultiValueNode);
		}
	}

	/**
	 * 等待XML生成完成后，根据记录整个XML节点属性存在多值的节点数据进一步构建XML
	 * @description 将属性值含有多个XML节点的父XML节点（类型：多不多）复制，循环依次将属性值赋予对应的节点属性，再将复制的节点添加到其父节点的父节点下
	 * @description 处理必须先处理最底层含有多个属性的XML节点（下——>上），保证每次复制的XML父节点，其内部子节点都已经处理完成（包含属性多值情况）
	 * @description xml节点都是引用指向的，根据记录的文档结构ID，获取对应的XML节点即可
	 */
	private dealMutilValueXmlNodeProperties(): void {
		let multiPropertyValueXmlNodes = this.multiPropertyValueXmlNodes;
		let mutilValueNodeKeys: string[] = Object.keys(multiPropertyValueXmlNodes);
		for (let keyIndex = 0; keyIndex < mutilValueNodeKeys.length; keyIndex++) {
			let dealXmlNode: JSONObject = {};
			let parentId: string = "";
			let lowLevelIndex: number = 9999; // 默认给定个最小值
			for (let i = 0; i < mutilValueNodeKeys.length; i++) { // 找到层级最小的
				let inKey: string = mutilValueNodeKeys[i];
				let nodes = multiPropertyValueXmlNodes[inKey];
				let level: number = nodes['parentNodeLevel'];
				if (level == -1) {
					continue;
				}
				if (level < lowLevelIndex) {
					lowLevelIndex = level;
					parentId = inKey;
					dealXmlNode = nodes;
				}
			}
			if (lowLevelIndex == 9999) {
				this.logger.addLog(`多值属性XML节点已经处理完成----end`);
				break;
			}
			multiPropertyValueXmlNodes[parentId]['parentNodeLevel'] = -1; // 因为是对象，相互都是指针指向，这里直接修改其层级为-1，标识处理过
			let parentNode = this.wordStructureService.getNodeInfo(parentId) as WordStructure;
			if (isEmpty(parentNode)) {
				this.logger.addLog(`多值XML属性节点对应的文档结构父节点: ${parentId} 不存在，越过`);
				continue;
			}
			let grandFatherNode = this.wordStructureService.getNodeInfo(parentNode.pNodeId as string) as WordStructure;
			if (isEmpty(grandFatherNode)) { // 此处处理的XML节点都是2层以上的，其父节点一定存在，若不存在，则越过
				this.logger.addLog(`多值XML属性节点对应的文档结构父节点的父节点不存在，越过`);
				continue;
			}
			let grandFatherXmlNode: DefaultElement = grandFatherNode.xmlNode;
			let parentXmlNode: DefaultElement = parentNode.xmlNode; // XML节点多值属性节点对应的父节点

			let multiValueMixLen: number = dealXmlNode['multiValueLen'];
			let mutilValueIndex: number = 1; // 由于XML生成时，已取首元素，此处依次从第二个元素开始赋值
			let multiValueNodes = dealXmlNode['multiValueNodes'];
			let xmlNodeNames: string[] = Object.keys(multiValueNodes);

			while (mutilValueIndex < multiValueMixLen) { // 根据需要处理的属性值个数循环赋值XML节点
				let cloneXmlNode: DefaultElement = parentXmlNode.createCopy(); // 考虑到对象之间都是引用关系，不能直接操作原本对象，这里深度复制节点
				for (let nameIndex = 0; nameIndex < xmlNodeNames.length; nameIndex++) {
					let updateXmlNodeName: string = xmlNodeNames[nameIndex];
					/** 指定XML节点的多值属性 */
					let updateChildrenProperties: XmlNodeMutilValueProperties[] = multiValueNodes[updateXmlNodeName];
					this.updateSignXmlNodeProperty(cloneXmlNode, updateXmlNodeName, updateChildrenProperties, mutilValueIndex);
				}
				grandFatherXmlNode.add(cloneXmlNode); // 将其复制的节点添加到其父节点下
				mutilValueIndex++;
			}
		}
	}

	/**
	 * 修改指定XML节点下指定叶子节点的属性value
	 * @param xmlNode 修改节点的上级根节点（不一定是直属根节点）
	 * @param updateXmlNodeName 修改的子节点名
	 * @param updateChildrenProperties 修改的子节点属性
	 * @param mutilValueIndex 取多值数组的下标位
	 */
	private updateSignXmlNodeProperty(
		xmlNode: DefaultElement,
		updateChildrenNodeName: string,
		updateChildrenProperties: XmlNodeMutilValueProperties[],
		mutilValueIndex: number
	): void {
		let nodeName: string = xmlNode.getName();
		if (nodeName == updateChildrenNodeName) { // 判断当前子节点是否为修改的子节点
			for (let propertyIndex = 0; propertyIndex < updateChildrenProperties.length; propertyIndex++) {
				let childrenProperties: XmlNodeMutilValueProperties = updateChildrenProperties[propertyIndex];
				let updatePropertyName = childrenProperties.name;
				let propertyValue: string[] = childrenProperties.values;
				xmlNode.setAttributeValue(updatePropertyName, propertyValue[mutilValueIndex]); // 修改属性值
			}
			return; // 修改完后，结束递归
		}
		let elementIterator = xmlNode.elementIterator();
		while (elementIterator.hasNext()) { // 当前节点下面子节点迭代器
			let childrenNode = elementIterator.next();
			this.updateSignXmlNodeProperty(childrenNode, updateChildrenNodeName, updateChildrenProperties, mutilValueIndex);
		}
	}

	/**
	 * 修改XML中指定节点的值
	 * @param xmlRootNode xml根节点
	 * @param updateNodeName 修改节点的名
	 * @param nodeValue 修改的值
	 */
	public updateSignNodeValue(xmlRootNode: DefaultElement, updateNodeName: string, nodeValue: string): void {
		if (!xmlRootNode) {
			this.logger.addLog(`XML根节点为空, 请先生成XML信息`);
			return;
		}
		let nodeName: string = xmlRootNode.getName();
		if (nodeName == updateNodeName) {
			if (nodeValue == null) {
				nodeValue = "";
			}
			xmlRootNode.setText(nodeValue);
			this.logger.addLog(`修改XML节点: ${updateNodeName} 的值为: ${nodeValue} 成功`);
			return;
		}
		let elementIterator = xmlRootNode.elementIterator();
		while (elementIterator.hasNext()) {
			let childrenNode: DefaultElement = elementIterator.next();
			this.updateSignNodeValue(childrenNode, updateNodeName, nodeValue);
		}
	}

	/**
	 * 校验生成XML的节点数据是否满足格式
	 * @return eg: {
			"result": false,
			"checkErrorResult": [{
				"wordNodeId": "OSVkzRXMdoCmhoqMfpSN5F",
				"nodeName": "code",
				"identifier": "HDSD00.01.036",
				"phytableName": "ODS_HLHT_JBXX_CJS",
				"fieldName": "HDSD00_01_036",
				"errorMessage": ["值: 1, 实际长度: 1 与定义长度: 2 不相等", "1 不在维表 [DM_HLHT_CJQKDMB]中"]
			}]
		}
	 */
	private checkXmlNodeDataValidity(): ResultInfo {
		this.logger.addLog(`开始执行【checkXmlNodeDataValidity】校验生成XML的节点数据是否满足格式`);
		let resultInfo: ResultInfo = { result: true };
		let checkErrorResults: JSONObject[] = [];
		let wordNodeDatas = this.nodesDataservice.getNodesData();
		let dealNodeIds: string[] = Object.keys(wordNodeDatas);
		for (let i = 0; i < dealNodeIds.length; i++) {
			let wordNodeId: string = dealNodeIds[i];
			let nodeDatas: NodeData[] = wordNodeDatas[wordNodeId];
			for (let nodeIndex = 0; nodeIndex < nodeDatas.length; nodeIndex++) {
				let nodeInfo: NodeData = nodeDatas[nodeIndex];
				let nodeName: string = nodeInfo.name;
				let identifier: string = nodeInfo.interIdentifiter;
				let fieldValues: string[] = nodeInfo.value;
				if (isEmpty(fieldValues)) {
					fieldValues = nodeInfo.defaultValue;
				}
				if (isEmpty(fieldValues)) { // 若节点值和默认值都为空, 则越过
					continue;
				}
				if (typeof fieldValues == 'string') { // 考虑到值存在多个，这里统一转换为数组形式
					fieldValues = [fieldValues];
				}
				for (let valueIndex = 0; valueIndex < fieldValues.length; valueIndex++) {
					let fieldValue: string = fieldValues[valueIndex];
					let executeResult: ResultInfo = this.xmlNodeDataCheck.executeCheckFieldValueIsLegal(identifier, fieldValue);
					if (!executeResult.result) {
						let errorMessages: string[] = [];
						let checkResult: ResultInfo = executeResult.checkResult;
						if (!isEmpty(checkResult)) {
							let formCheckResult = checkResult.formCheckResult;
							if (!formCheckResult.result) {
								errorMessages.push(formCheckResult.message);
							}
							let typeCheckResult = checkResult.typeCheckResult;
							if (!typeCheckResult.result) {
								errorMessages.push(typeCheckResult.message);
							}
						} else {
							errorMessages.push(executeResult.message);
						}
						checkErrorResults.push({
							wordNodeId: wordNodeId,
							nodeName: nodeName,
							identifier: identifier,
							phytableName: nodeInfo.dbTable,
							fieldName: nodeInfo.dbField,
							errorMessage: errorMessages
						});
					}
				}
			}
		}
		if (checkErrorResults.length != 0) {
			resultInfo.result = false;
			resultInfo.checkErrorResult = checkErrorResults;
		}
		return resultInfo;
	}

	/**
	 * 获取XML根节点
	 */
	public getXmlRootNode(): DefaultElement {
		return this.xmlRootNode;
	}

	/**
	 * 获取值对应维表的描述值
	 */
	private getNodeValueDescFromDim(identifier: string, value: string | string[]): string | string[] {
		let dimResult = this.xmlNodeDataCheck.getIdentifierDimData(identifier);
		if (!dimResult.result) {
			return null;
		}
		let dimData = dimResult.dimTableDatas;
		if (typeof value == "string") {
			for (let i = 0; i < dimData.length; i++) {
				let data = dimData[i];
				if (data["Z"] == value) {
					return data["SM"];
				}
			}
		} else {
			let descs: string[] = [];
			for (let i = 0; i < value.length; i++) {
				let isExist = false;
				for (let j = 0; j < dimData.length; j++) {
					let data = dimData[j];
					if (data["Z"] == value[i]) {
						descs.push(data["SM"]);
						isExist = true;
						break;
					}
				}
				if (!isExist) {
					descs.push(null);
				}
			}
			return descs;
		}
	}
}

/**
 * 文档结构构造类
 * 内部查询指定文档id的数据，并且将数据构造成树形
 */
export class WordStructureService {

	/** 文档ID */
	private wordId: string;
	/** 文档结构节点 */
	private wordNodes: WordStructure[];
	/** 基数返回为[0~n, 1~n]条的节点信息 */
	private mutilpleNodes: WordStructure[];
	/** 基数返回为[0~1, 1~1]条的节点信息 */
	private singleNodes: WordStructure[];
	/** 文档ID所对应的所有根节点 */
	private rootNodeId: string;

	constructor(args: {
		wordId: string
	}) {
		this.wordId = args.wordId;
		this.wordNodes = [];
		this.createWorkNodes();

		this.mutilpleNodes = [];
		this.singleNodes = [];
		this.filterWordNodes();
	}

	/**
	 * 根据节点id查询节点信息
	 * @param nodeId
	 */
	public getNodeInfo(nodeId: string): WordStructure | void {
		if (!nodeId) {
			console.debug(`未指定需要查询子节点的父节点ID`);
			return;
		}
		let nodeInfo: WordStructure;
		let workNodes: WordStructure[] = this.wordNodes;
		for (let i = 0; i < workNodes.length; i++) {
			let node = workNodes[i];
			if (node.nodeId == nodeId) {
				nodeInfo = node;
				break;
			}
		}
		return nodeInfo;
	}

	/**
	 * 查询子节点
	 * @param nodeId 节点ID
	 * @return
	 */
	public getChildernNodeInfo(nodeId: string): WordStructure[] {
		if (!nodeId) {
			logger.addLog(`未指定需要查询子节点的父节点ID`);
			return [];
		}
		let childrenNodes: WordStructure[] = [];
		let workNodes: WordStructure[] = this.wordNodes;
		for (let i = 0; i < workNodes.length; i++) {
			let node = workNodes[i];
			if (node?.pNodeId == nodeId) { // 校验节点的父节点ID是否为当前父节点ID（顶层节点无父节点，需要判空）
				childrenNodes.push(node);
			}
		}
		return childrenNodes;
	}

	/**
	 * 查询所有返回多条数据结果
	 */
	public getMutilpleNode(): WordStructure[] {
		return this.mutilpleNodes;
	}

	/**
	 * 查询返回单条数据结果
	 */
	public getSingleNode(): WordStructure[] {
		return this.singleNodes;
	}

	/**
	 * 获取当前文档ID所设计的根节点
	 * @description 一个文档对应一个根节点
	 */
	public getRootNodeId(): string {
		return this.rootNodeId;
	}

	/**
	 * 递归寻找指定节点ID的父节点基数为[0..*, 1..*]的节点信息（最近原则）
	 * @param nodeInfo 节点信息
	 * @return
	 */
	public querySignBaseParentNodeInfo(nodeId: string): WordStructure | void {
		let nodeInfo = this.getNodeInfo(nodeId);
		if (!nodeInfo) {
			return;
		}
		if (['0..*', '1..*'].includes(nodeInfo.base)) {// 校验当前基数是否多不多情况
			return nodeInfo;
		}
		let parentId: string = nodeInfo.pNodeId as string;
		if (!parentId) {
			return;
		}
		return this.querySignBaseParentNodeInfo(parentId);
	}

	/**
	 * 设置某个节点对应的XML节点
	 * @param nodeId 节点ID
	 * @param xmlNode xml节点信息
	 */
	public setNodeXmlNode(nodeId: string, xmlNode: DefaultElement): void {
		if (!nodeId || !xmlNode) {
			return;
		}
		for (let i = 0; i < this.wordNodes.length; i++) {
			if (this.wordNodes[i].nodeId == nodeId) {
				this.wordNodes[i].xmlNode = xmlNode;
				break;
			}
		}
	}

	/**
	 * 根据基数区分数据返回数据类型
	 *
	 * @description 若当前节点类型不为多不多, 但它上级存在多不多的节点（最近原则）, 则将其也视为多不多节点
	 */
	private filterWordNodes(): void {
		let workNodes: WordStructure[] = this.wordNodes;
		if (!workNodes) {
			print(`文档结构节点数据为空`);
			return;
		}
		for (let i = 0; i < workNodes.length; i++) {
			let node = workNodes[i];
			let nodeId: string = node.nodeId;
			let base: string = node.base;
			/** 查询当前节点是否存在多不多的父节点 */
			let baseParentNode = this.querySignBaseParentNodeInfo(nodeId);
			if (["0..*", "1..*"].includes(base) || !!baseParentNode) {
				this.mutilpleNodes.push(node);
			} else {
				if (["0..1", "1..1", null].includes(base)) {
					this.singleNodes.push(node);
				}
			}
		}
	}

	/**
	* 查询文档结构表，生成结构节点信息
	*/
	private createWorkNodes(): void {
		if (!this.wordId) {
			console.debug("文档ID未给定，生成结构节点信息失败");
			return;
		}
		let queryInfo: QueryInfo = {
			sources: [{
				id: "model1",
				path: "/HLHT/data/tables/xml/SDI_HLHT_WORD_STRUCTURE.tbl"
			}],
			fields: [{
				name: "NODE_ID", exp: "model1.NODE_ID"
			}, {
				name: "P_NODE_ID", exp: "model1.P_NODE_ID"
			}, {
				name: "NODE_NAME", exp: "model1.NODE_NAME"
			}, {
				name: "SECTION_NAME", exp: "model1.SECTION_NAME"
			}, {
				name: "BASE", exp: "model1.BASE"
			}, {
				name: "DESC", exp: "model1.DESC"
			}, {
				name: "WORD_ID", exp: "model1.WORD_ID"
			}, {
				name: "CONSTRAINT", exp: "model1.CONSTRAINT"
			}, {
				name: "SORT_FIELD", exp: "model1.SORT_FIELD"
			}, {
				name: "SZ_LEVEL", exp: "model1.SZ_LEVEL"
			}, {
				name: "SZ_LEAF", exp: "model1.SZ_LEAF"
			}],
			filter: [{
				exp: `model1.WORD_ID = '${this.wordId}'`
			}],
			sort: [{
				exp: "model1.SORT_FIELD"
			}]
		}
		let isXmlContent = false;
		let workNodeDatas: (string | number | boolean)[][] = dw.queryData(queryInfo).data;
		for (let i = 0; i < workNodeDatas.length; i++) {
			let data = workNodeDatas[i] as any[];
			let nodeId: string = data[0];
			let base: string = data[4];
			let level: number = data[9];
			if (data[2] == "component" && !isXmlContent) {
				isXmlContent = true;
			}
			let allowNull = ["0..1", "0..*", null].includes(base);
			let workNode: WordStructure = {
				nodeId: nodeId,
				pNodeId: data[1],
				nodeName: data[2],
				sectionName: data[3],
				base: base,
				constraint: data[7],
				allowNull: allowNull,
				allowMultiple: ["1..1", "1..*"].includes(base),
				level: level,
				isLeaf: data[10] == 1 ? true : false,
				sort: data[8],
				hasComment: allowNull ? false : ((level >= 3 && isXmlContent) && true),
				comment: data[3] != null ? `${data[3]}章节数据为空` : `${data[2]}数据为空`
			};
			this.wordNodes.push(workNode);
			if (level == 0) {
				this.rootNodeId = nodeId;
			}
		}
	}

	/**
	 * 获取当前文档ID下的所有文档节点信息
	 */
	public getWordNodes(): WordStructure[] {
		return this.wordNodes;
	}
}

/**
 * 节点数据服务类
 * 查询每个节点每个属性的值
 * 当一个节点的所有子节点都查询不出来数据，那么这个节点应该隐藏，并且给出注释。
 */
export class NodesDataService {
	private wordTableDirPath: string;
	private wordId: string;
	private wordStructureService: WordStructureService;
	private mainDbTable: string;
	private requestParams: any;
	private nodesData: { [nodeId: string]: NodeData[] };
	private metaData: { [interIdentifiter: string]: MetaDataMappingTable };
	private queryDbQueryFields: { signle: { [dbTable: string]: { nodeIds: string[], fields: string[] } }, mutlpie: { [dbTable: string]: { nodeIds: string[], fields: string[] } } };
	constructor(args: { wordInfo: WordInfo, wordStructureService: WordStructureService, requestParams: any, metaData: JSONObject }) {
		this.wordId = args.wordInfo.wordId;
		this.wordStructureService = args.wordStructureService;
		this.requestParams = args.requestParams;
		this.mainDbTable = args.wordInfo.mainDbTable;
		this.wordTableDirPath = args.wordInfo.tableDirPath;
		this.nodesData = {};
		this.queryDbQueryFields = { signle: {}, mutlpie: {} };
		this.metaData = args.metaData;
	}

	public init(notQueryValue?: boolean) {
		logger.addLog(`开始查询所有节点属性对应的数据集信息`)
		this.queryNodeInfo();
		if (!notQueryValue) {
			this.queryNodeValue();
		}
	}

	/**
	 * 获取节点属性信息
	 */
	public getNodeProperty(nodeId: string) {
		return this.nodesData[nodeId];
	}

	/**
	 * 查询节点信息
	 */
	private queryNodeInfo() {
		let queryInfo: QueryInfo = {
			sources: [{
				id: "model1",
				path: "/HLHT/data/tables/xml/SDI_HLHT_NODE_INFO.tbl"
			}, {
				id: "model2",
				path: "/HLHT/data/tables/xml/ODS_HLHT_YSJ.tbl"
			}],
			fields: [{
				name: "NODE_ID", exp: "model1.NODE_ID"
			}, {
				name: "NAME", exp: "model1.NAME"
			}, {
				name: "DEFAULT_VALUE", exp: "model1.DEFAULT_VALUE"
			}, {
				name: "INTER_IDENTIFITER", exp: "model1.INTER_IDENTIFITER"
			}, {
				name: "DB_TABLE", exp: "model1.DB_TABLE"
			}, {
				name: "ZDM", exp: "model1.DB_FIELD"
			}, {
				name: "DATA_ELEMENT", exp: "model1.DATA_ELEMENT"
			}, {
				name: "DESC_FROM_NODE", exp: "model1.DESC_FROM_NODE"
			}],
			filter: [{
				exp: "model1.WORD_ID=param1"
			}],
			params: [{
				name: "param1", value: this.wordId
			}],
			joinConditions: [{
				leftTable: "model1",
				rightTable: "model2",
				joinType: SQLJoinType.LeftJoin,
				exp: "model1.INTER_IDENTIFITER=model2.NBBSF AND model1.DB_TABLE=model2.ZJ"
			}]
		}
		let queryResult = dw.queryData(queryInfo);
		let rows = queryResult.data;
		for (let i = 0; i < rows.length; i++) {
			const row = rows[i];
			let nodeId = row[0] as string;
			let nodeName = row[1] as string;
			let defaultValue = row[2];
			let interIdentifiter = row[3] as string;
			let dbTable = row[4] as string;
			let dbField = row[5] as string;
			let elementId = row[6] as string;
			let valueFromNode = row[7] as string;
			if (interIdentifiter == 'HDSB01.01.039') {//对于生成时间特殊处理
				dbTable = "SDI_HLHT_XML_ATTACH";
				dbField = "CREATE_TIME"
			}
			let nodeData: NodeData = {
				name: nodeName,
				defaultValue,
				interIdentifiter,
				dbTable, dbField, value: [],
				elementId: elementId,
				valueFromNode
			}
			this.nodesData[nodeId] == null ? this.nodesData[nodeId] = [nodeData] : this.nodesData[nodeId].push(nodeData);
			if (defaultValue != null && defaultValue != "") {
				let nodeInfo = this.wordStructureService.getNodeInfo(nodeId);
				nodeInfo && (nodeInfo.hasComment = false);
			}
		}
		logger.addLog(`节点信息查询完毕，共查询到${Object.keys(this.nodesData).length}节点信息`)
	}

	/**
	 * 查询所有节点需要的字段值
	 * 查询时要进行分类，根据返回是单行还是多行分开查询，
	 */
	private queryNodeValue() {
		logger.addLog("开始查询节点对应字段的信息")
		let wordStructureService = this.wordStructureService;
		let singleNodes = wordStructureService.getSingleNode();
		let multipleNodes = wordStructureService.getMutilpleNode();
		this.getDbQueryFields(singleNodes, "signle");
		this.getDbQueryFields(multipleNodes, "mutlpie");
		this.queryDbData(this.queryDbQueryFields["signle"], false);
		this.queryDbData(this.queryDbQueryFields["mutlpie"], true);
	}

	/**
	 * 查询某个节点要查询的物理表和字段，将其按照查询还是多行进行归类
	 * 如果某个节点的父节点要查询多行，那么这个节点也要查询多行
	 * @param nodes
	 * @param type
	 * @returns
	 */
	private getDbQueryFields(nodes: WordStructure[], type: "signle" | "mutlpie") {
		let queryDbQueryFields = this.queryDbQueryFields;
		for (let i = 0; i < nodes.length; i++) {
			let setType = type;
			const nodeInfo = nodes[i];
			let nodeId = nodeInfo.nodeId;
			if (type == "signle") {
				let pNodeId = this.wordStructureService.querySignBaseParentNodeInfo(nodeId);
				if (pNodeId != null) {
					setType = "mutlpie";
				}
			}
			let nodeData = this.nodesData[nodeId];
			for (let j = 0; nodeData && j < nodeData.length; j++) {
				const nodeAttr = nodeData[j];
				let dbTable = nodeAttr.dbTable;
				let dbField = nodeAttr.dbField;
				if (dbTable != null && dbField != null) {
					if (queryDbQueryFields[setType][dbTable] == null) {
						queryDbQueryFields[setType][dbTable] = { nodeIds: [nodeId], fields: [dbField] }
					} else {
						queryDbQueryFields[setType][dbTable].nodeIds.push(nodeId);
						queryDbQueryFields[setType][dbTable].fields.push(dbField);
					}
				}
			}
		}
	}

	/**
	 * 查询当前结构中物理表所有需要的字段
	 * 如果参数不是一个值，那么就要考虑将主表和从表关联查询
	 *
	 * //TODO 之后考虑实现如何关联查询指定值吧，先实现单表查询
	 * @param queryDbField
	 * @param isMutliple
	 */
	private queryDbData(queryDbField: { [tableName: string]: { nodeIds: string[], fields: string[] } }, isMutliple: boolean) {
		let requestParams = this.requestParams;
		let reqFieldNames = Object.keys(requestParams);
		let tableList = Object.keys(queryDbField);
		let params = [];
		for (let i = 0; i < reqFieldNames.length; i++) {
			let fieldName = reqFieldNames[i];
			params.push({ name: fieldName, value: requestParams[fieldName] });
		}
		for (let i = 0; i < tableList.length; i++) {
			let table = tableList[i];
			let tablePath = this.wordTableDirPath + "/" + table + ".tbl";

			let filters: FilterInfo[] = [];//TODO如果过滤条件为空，要返回错误
			reqFieldNames.forEach(fieldName => {
				let isExist = this.isExistField(tablePath, fieldName);
				if (isExist) {
					filters.push({ exp: `${table}.${fieldName}=${fieldName}` });
				}
			});
			let queryFields = [];
			let dbFields = queryDbField[table].fields;
			let nodeIds = queryDbField[table].nodeIds;
			let existField = {};
			for (let j = 0; j < dbFields.length; j++) {
				let fieldName = dbFields[j];
				if (existField[fieldName] == null) {
					existField[fieldName] = 1;
				} else {
					existField[fieldName]++;
				}
				queryFields.push({ name: fieldName + existField[fieldName], exp: `${table}.${fieldName}` })
			}
			if (table == "SDI_HLHT_XML_ATTACH") {
				tablePath = "/HLHT/data/tables/xml/SDI_HLHT_XML_ATTACH.tbl";
				filters = [{
					exp: "model1.WORD_ID=param1 and model1.EMPIID=EMPIID"
				}]
			}
			let queryInfo: QueryInfo = {
				sources: [{
					id: table,
					path: tablePath
				}],
				filter: filters,
				fields: queryFields,
				params,
				options: {
					limit: isMutliple == true ? 1 : null
				}
			};
			let queryResult = dw.queryData(queryInfo);
			let rows = queryResult.data;
			let fields = queryResult.fields;

			for (let index = 0; isMutliple ? index < rows.length : (index < 1 && index < rows.length); index++) {
				const row = rows[index];
				for (let pIndex = 0; pIndex < dbFields.length; pIndex++) {
					let propValue = row[pIndex];
					const field = dbFields[pIndex];
					const nodeId = nodeIds[pIndex];
					this.nodesData[nodeId].forEach(data => {
						if (data.dbField == field) {
							let newValue = this.foramteValue(propValue, data.interIdentifiter);
							if (newValue != null && isMutliple) {
								data.value == null ? data.value = [newValue] : data.value.push(newValue)
							} else if (newValue != null) {
								data.value = [newValue];
							}
							if (newValue == null) {
								let nodeInfo = this.wordStructureService.getNodeInfo(nodeId);
								nodeInfo && (nodeInfo.hasComment = true);
							}

							if (newValue != null && newValue != "") {
								this.setParentNodeComment(nodeId);
							}
						}
					})
				}
			}
		}
	}

	/**
	 * 设置xml文档体节点中所有父节点的hasComment属性为false
	 * @param nodeId
	 */
	private setParentNodeComment(nodeId: string) {
		let nodeInfo = this.wordStructureService.getNodeInfo(nodeId);
		if (nodeInfo && nodeInfo.level < 3) {
			return;
		} else if (nodeInfo) {
			nodeInfo.hasComment = false;
			return this.setParentNodeComment(nodeInfo.pNodeId);
		}
		return;
	}

	/**
	 * 格式化值内容
	 * 目前只处理时间类型的值，将时间类型的值转为字符串，如果本身已经是字符串那边不做任何处理直接返回
	 */
	private foramteValue(value: any, identifier: string) {
		let metaData = this.metaData;
		let metaInfo = metaData[identifier];
		if (metaInfo != null) {
			let type = metaInfo.metaDataType;
			if ((type == "D" || type == "DT") && value && value.getClass() == "class java.sql.Timestamp") {
				let formate = metaInfo.expressForm.replaceAll(" ", "");
				let formateStr: string = null;
				switch (formate) {
					case XmlNodeMetaDataFormat.D8:
						formateStr = "yyyyMMdd";
						break;
					case XmlNodeMetaDataFormat.T6:
						formateStr = "hhmmss";
						break;
					case XmlNodeMetaDataFormat.DT15:
						formateStr = "YYYYMMDD\'T\'hhmmss";
						break;
				}
				if (formateStr != null) {
					let SimpleDateFormat = Java.type('java.text.SimpleDateFormat');
					let dateTimeStamp = new SimpleDateFormat(formateStr);
					return dateTimeStamp.format(value);
				}
			} else if (type == "L") {
				switch (value) {
					case "1": return "true";
					case "0": return "false";
				}
			}
		}
		return value;
	}

	/**
	 * 判断字符是否存在于对应的模型中
	 */
	private isExistField(tablePath: string, fieldName: string) {
		let table = dw.getDwTable(tablePath);
		let dimensions = table && table.dimensions;
		let measures = table && table.measures;
		for (let i = 0; dimensions && i < dimensions.length; i++) {
			let field = dimensions[i];
			if (field.name == fieldName || field.getDbFieldName() == fieldName) {
				return true;
			}
		}
		for (let i = 0; measures && i < measures.length; i++) {
			let field = measures[i];
			if (field.name == fieldName || field.getDbFieldName() == fieldName) {
				return true;
			}
		}
		return false;
	}

	public getNodesData() {
		return this.nodesData;
	}
}

/**
 * 2022-10-13 丁煜
 * https://jira.succez.com/browse/CSTM-20750
 * 上传XML校验类
 * 校验上传的XML是否符合格式
 */
export class UploadXml {
	/**默认数据库对象 */
	private ds = db.getDefaultDataSource();
	/**xml字符串 */
	private xmlString: string
	/**xml文件对象 */
	private xmlFile: fs.File
	/**xml对象 */
	private xml;
	/**需要写入物理表中数据 */
	private needAddTableArr: {}[] = [];
	/**校验失败的物理表 */
	private checkFailArr: string[] = [];
	/**文档对象 */
	private word: WordStructureService;
	/**文档ID */
	private wordId: string;
	/**校验类对象 */
	private checkClass: XmlNodeDataCheck;
	/**节点表对象 */
	private nodeTable;
	/**错误节点信息数组 */
	private errorInfoArr = [];
	/**批次ID */
	private taskId = utils.uuid();
	/**是否校验默认值 */
	private isCheckDefaultValue = false;
	/**日志对象 */
	private logger = new SDILog({ name: "XMLCheck" });
	/**数据元服务类对象 */
	private metaDataService: CDAMetaService;
	/**文档对应的所有数据元信息 */
	private metaDataInfo;
	/**文档流水号 */
	private documentserial;
	/**版本号 */
	private version;
	/**所有需要写入的物理表名 */
	private needInsertTableName = [];
	/**物理表写入过程中的失败信息 */
	private insertFailInfo: string[] = [];
	/**默认数据源Connection对象 */
	private conn;
	/**默认数据源schema对象 */
	private defaultSchema;
	/**互联互通数据源 */
	private hlht;
	/**互联互通数据源默认数据源Connection对象 */
	private hlhtConn;
	/**互联互通数据源schema对象 */
	private hlhtSchema;
	/**节点属性信息类对象 */
	private nodeDataInfo: NodesDataService;

	/**
	 * xml校验
	 * @param xmlString xml格式字符串
	 * xml:xml文件对象
	 */
	public xmlCheck(xmlString: string, xmlFile: fs.File) {
		this.xmlString = xmlString;
		this.xmlFile = xmlFile;
		this.xml = utils.parseXML(this.xmlString);
		let result = this.configuringNodes();
		return result;
	}

	/**
	 * 配置根节点以及读取文档结构表生成文档对象
	 * 查出所有需要查询的数据,首先需要查出xml格式属于那个文档结构，查出文档ID后通过new WordStructureService类来获取文档结构对象
	 */
	private configuringNodes() {
		let xmlNode = this.xml.getChildNodes().item(0);
		let xmlChildrenArr = xmlNode.getChildNodes();
		this.hlht = db.getDataSource("HLHT");
		let hasTitle = false;
		let wordName;
		for (let i = 0; i < xmlChildrenArr.length; i++) {
			let xmlChildren = xmlChildrenArr.item(i);
			let xmlChildrenName = xmlChildren.getNodeName();
			if (xmlChildrenName == "title") {
				hasTitle = true;
				wordName = "%" + xmlChildren.getChildNodes().item(0).getNodeValue() + "%";
			}
			if (xmlChildrenName == "id") {
				this.documentserial = this.getValue("extension", xmlChildren);
			}
			if (xmlChildrenName == "versionNumber") {
				this.version = this.getValue("value", xmlChildren);
			}
		}
		if (hasTitle) {
			let wordTable = this.hlht.openTableData("SDI_HLHT_WORD_INFO");
			let wordQuery = wordTable.executeQuery(`select WORD_ID from SDI_HLHT_WORD_INFO where WORD_NAME like ?`, [wordName]);
			if (wordQuery && wordQuery.length > 0) {
				this.wordId = wordQuery[0]?.WORD_ID;
			}
			if (!wordQuery || !this.wordId) {
				this.logger.addLog(`xml格式不正确，标准文档中不包含此xml,当前文档名称为：${wordName}`);
				return { result: false, message: "xml格式不正确，标准文档中不包含此xml" }
			}
			this.metaDataService = new CDAMetaService({
				wordId: this.wordId
			})
			this.metaDataInfo = this.metaDataService.getMetaData();
			this.checkClass = new XmlNodeDataCheck({
				wordId: this.wordId,
				metaDataService: this.metaDataService,
				isDbTable: false
			});
			this.word = new WordStructureService({ wordId: this.wordId });
		} else {
			this.logger.addLog(`xml中未找到title标签，无法获取到所属文档名称`);
			return { result: false, message: "xml中未找到title标签，无法获取到所属文档名称" }
		}
		let nodeId = this.word.getRootNodeId();
		let nodeName = this.word.getNodeInfo(nodeId)['nodeName'];
		if (nodeName != xmlNode.getNodeName()) {
			this.logger.addLog(`xml格式不正确,根节点名不符,当前根节点名为：${xmlNode.getNodeName()},规范中根节点应为：${nodeName}`);
			return { result: false, message: "xml格式不正确,根节点名不符" }
		}
		let wordInfo: WordInfo = {
			wordId: this.wordId
		}
		this.nodeDataInfo = new NodesDataService({ wordInfo: wordInfo, wordStructureService: this.word, requestParams: null, metaData: {} });
		this.nodeDataInfo.init(true);
		let result = this.compareXML(nodeId, xmlNode);
		this.hlhtConn = this.hlht.getConnection();
		this.hlhtSchema = this.hlht.getDefaultSchema();
		this.hlhtConn.setAutoCommit(false);
		let cdaErrorTable = this.hlht.openTableData("SDI_HLHT_CDA_ERROR", this.hlhtSchema, this.hlhtConn);
		if (result['result']) {
			if (this.errorInfoArr.length > 0) {
				try {
					cdaErrorTable.insert({
						"WORD_ID": this.wordId,
						"PRIMARYKESY": this.taskId,
						"TASK_ID": this.taskId,
						"TYPE": "import",
						"CREATE_TIME": new java.util.Date(),
						"IS_CREATED": false,
						"ERROR": "节点属性校验不通过"
					})
					let cdaErrorLogTable = this.hlht.openTableData("SDI_HLHT_CDA_ERROR_LOG", this.hlhtSchema, this.hlhtConn);
					cdaErrorLogTable.insert(this.errorInfoArr);
				} catch (e) {
					this.insertFailInfo.push(e)
				}
			}
			if (this.checkFailArr.length == 0) {//如果没有校验失败的物理表则像ATTACH表中写入一条数据 将xml作为附件储存
				this.filterNeedAddTableArr();
				let xmlFileName = this.xmlFile.getName();
				let shareDir;
				let isCluster: boolean = sys.isCluster();
				if (isCluster) {
					shareDir = sys.getClusterShareDir();
				} else {
					shareDir = sys.getWorkDir();
				}
				let attachParentDir: string = `${shareDir}/file-storage/relativePath`;
				let xmlFilePath: string = `${attachParentDir}/${xmlFileName}`;
				let filePath = `workdir:${utils.formatDate("yyyyMMddHHmmssSS")}_${xmlFileName}`
				let newFile = fs.getFile(xmlFilePath);
				newFile.copy(this.xmlFile);
				let md5 = utils.md5(this.xmlString);
				let attachTable = this.hlht.openTableData("SDI_HLHT_XML_ATTACH", this.hlhtSchema, this.hlhtConn);
				try {
					attachTable.insert({
						WORD_ID: this.wordId,
						EMPIID: this.taskId,
						WORD_INDEX: utils.uuid(),
						WORD_SERIAL_NUMBER: this.documentserial,
						VERSION_NUMBER: this.version,
						CREATE_TIME: new java.util.Date(),
						FILE_PATH: filePath,
						CONTENT_MD5: md5
					})
				} catch (e) {
					this.insertFailInfo.push(e)
				}
			} else {
				result = {
					result: true,
					message: `xml结构正确但有部分属性校验未通过,点击以下按钮可以进行查看`,
					wordId: this.wordId,
					taskId: this.taskId
				}
			}
		} else {
			try {
				cdaErrorTable.insert({
					"WORD_ID": this.wordId,
					"PRIMARYKESY": this.taskId,
					"TASK_ID": this.taskId,
					"TYPE": "import",
					"CREATE_TIME": new java.util.Date(),
					"IS_CREATED": false,
					"ERROR": result['message']
				})
			} catch (e) {
				this.insertFailInfo.push(e)
			}
		}
		try {
			if (this.insertFailInfo.length == 0) {//如果没有写入失败的数据则提交所有写入数据
				this.hlhtConn.commit();
			} else {
				this.logger.addLog(`上传xml写入物理表过程出现异常,异常详情：${this.insertFailInfo}`);
				return {
					result: false, message: `上传xml写入物理表过程出现异常`
				}
			}
		} catch (e) {//若出现异常则执行回滚并抛出异常
			this.logger.addLog(`数据提交过程中出现异常,异常详情：${e}`);
			this.hlhtConn.rollback();
			throw e;
		} finally {
			this.hlhtConn && this.hlhtConn.close();
		}
		return result
	}

	/**
	 * 从xml指定节点属性中取出需要的值
	 */
	private getValue(propertyName: string, xmlNode) {
		let xmlPropertyArr = xmlNode.getAttributes();
		for (let i = 0; i < xmlPropertyArr.length; i++) {
			let xmlProperty = xmlPropertyArr.item(i);
			let xmlPropertyName = xmlProperty.name;
			let xmlPropertyValue = xmlProperty.value;
			if (xmlPropertyName == propertyName) {
				return xmlPropertyValue;
			}
		}
	}

	/**
	 * 递归对比获取传入的标准节点的子节点和上传xml节点的子节点,将两者进行一一比对
	 * 对比时需要判断不同基数情况下的不同情况
	 * 其中基数为0..1或0..*的情况允许不存在当前节点，当前结构表节点需与下级节点进行比较
	 * 若基数为1..*或0..*的情况则允许连续出现重复的相同节点，若节点名为注释的情况且约束为R和基数为1..1或1..*的情况允许不存在此节点且当前结构表节点也不做对比，默认当前数据情况合法继续校验流程
	 * @param nodeId 节点ID
	 * @xmlNode xml节点对象
	 */
	private compareXML(nodeId: string, xmlNode) {
		let currentNode = 0;//当前节点在结构表子节点数组中的位置，由于xml中可能存在连续重复且合法的节点，所以需要此变量来控制使用哪一个结构表节点来与xml节点进行对比
		let xmlChildrenArr = xmlNode.getChildNodes();
		let nodeChildrenArr = this.word.getChildernNodeInfo(nodeId);
		let count: number = this.queryXmlChilrendNode(nodeChildrenArr);
		if (xmlChildrenArr.length < count) {//如果xml的子节点比结构表中少则返回格式不正确
			let lackNodeName = this.returnNodeName(xmlChildrenArr, nodeChildrenArr);
			this.logger.addLog(`xml格式不正确,xml中${lackNodeName.toString()}子节点缺失，当前结构表根节点为${nodeId},xml根节点名为：${xmlNode.getNodeName()}`);
			return { result: false, message: `xml格式不正确,xml中${lackNodeName.toString()}子节点缺失` }
		}
		for (let i = 0; i < xmlChildrenArr.length; i++) {
			let xmlChildren = xmlChildrenArr.item(i);
			let xmlChildrenName = xmlChildren.getNodeName();
			let xmlChildrenValue = xmlChildren.getNodeValue();
			if (xmlChildrenName == "#text") {
				continue;
			}
			if (currentNode == nodeChildrenArr.length) {
				let overflowNodeArr: string[] = [];
				for (let o = i; o < xmlChildrenArr.length; o++) {
					let overflowNodeName = xmlChildrenArr.item(o).getNodeName();
					if (overflowNodeName == "#text") {
						continue;
					}
					overflowNodeArr.push(overflowNodeName);
				}
				this.logger.addLog(`xml格式不正确,上传xml中【${xmlNode.getNodeName()}】的子节点数量多于标准文档下子节点数量，当前标准文档节点ID为${nodeId}，上传xml中多出的节点有：${overflowNodeArr},当前结构表根节点为${nodeId},xml根节点名为：${xmlNode.getNodeName()}`);
				return { result: false, message: `xml格式不正确,上传xml中【${xmlNode.getNodeName()}】的子节点数量多于标准文档下子节点数量，当前标准文档节点ID为${nodeId}，上传xml中多出的节点有：${overflowNodeArr}` }
			}
			let nodeChildren = nodeChildrenArr[currentNode];
			let nodeChildrenName = nodeChildren['nodeName'];
			let nodeChildrenId = nodeChildren['nodeId'];
			let base = nodeChildren['base'];
			let constraint = nodeChildren['constraint'];
			if (xmlChildrenName == "#comment") {//如果当前xml节点名为注释的情况则判断其约束是否为R以及基数是否为1开头，若是的情况则说明允许为空，跳过此层校验，结构表和xml同时进行下一个节点校验，若不是则还是用当前结构表节点校验下层xml节点
				//if (constraint == 'R') {
				if (base == "1..1" || base == "1..*") {
					currentNode += 1;
				}
				//}
				continue;
			}
			// print(xmlChildrenName+"======"+nodeChildrenName)
			if (xmlChildrenName == nodeChildrenName) {
				if (base != "1..1" && base != "1..*" && i != xmlChildrenArr.length - 2) {//因为xml都是成双成对出现的，所以长度-2的情况下是最后一个节点，最后一个节点不需要判断是否为期望节点
					let result = this.compareComponent(xmlChildren, nodeChildrenId);
					if (result == false) {
						currentNode += 1;
						i = i - 1;
						if (currentNode == nodeChildrenArr.length) {

						}
						continue;
					}
				}
				let result = this.compareProperty(nodeChildren, xmlChildren);
				if (!result['result']) {
					return result
				}
				if (base == "0..*" || base == "1..*") {
					if (i + 1 != xmlChildrenArr.length) {
						let nextNodeName = xmlChildrenArr.item(i + 1).getNodeName();
						if (nextNodeName != xmlChildrenName) {//若基数尾数为*的情况只有下层节点名与当前节点名不同的情况，结构表的下标才会加一
							currentNode += 1;
						}
					}
				} else {
					currentNode += 1;
				}
			} else if (base == "0..*" || base == "0..1" || base == null) {//如果基数为0且当前匹配不符合的情况则用当前xml节点与结构表中下一节点进行比较
				currentNode += 1;
				i = i - 1;
				if (currentNode == nodeChildrenArr.length) {//如果结构表没有下级则代表当前数据不符合规范
					this.logger.addLog(`xml格式不正确，不应存在当前xml节点${xmlChildrenName}，当前结构表根节点为${nodeId},xml根节点名为：${xmlNode.getNodeName()}`);
					return { result: false, message: `xml格式不正确，不应存在当前xml节点${xmlChildrenName}` }
				}
				continue;
			} else {
				this.logger.addLog(`xml格式不正确，xml结构有误，当前xml节点名应为：${nodeChildrenName},xml实际节点名为${xmlChildrenName}，当前结构表根节点为${nodeId},xml根节点名为：${xmlNode.getNodeName()}`);
				return { result: false, message: `xml格式不正确，xml结构有误，xml节点名为${xmlChildrenName}` }
			}
			let result = this.compareXML(nodeChildrenId, xmlChildren);
			if (!result['result']) {
				return result
			}
		}
		return { result: true, message: "xml校验通过" }
	}

	/**
	 * 对比当前章节是否为期望章节
	 * xml中有些章节可以允许为空，若当前约束不为R且基数不为1..1或1..R的情况下则需要对其内部子节点进行比对
	 * 递归寻找到第一个存在属性的子节点，对比其属性是否一致，若一致则认为是期望章节，若不一致则跳过此章节
	 *
	 * 2022-12-12 liuyz
	 * 判断一个节点是否为期望节点时需要比较下级节点的code值是否符合期望
	 */
	private compareComponent(xmlNode, nodeId): boolean | void {
		let currentNode = 0;//当前文档结构表中节点下标
		let xmlChildrenArr = xmlNode.getChildNodes();
		let nodeChildrenArr = this.word.getChildernNodeInfo(nodeId);
		let result;
		let count: number = this.queryXmlChilrendNode(nodeChildrenArr)
		if (xmlChildrenArr.length < count) {//如果xml的子节点比结构表中少则一定不是期望章节
			return false;
		}
		for (let i = 0; xmlChildrenArr && i < xmlChildrenArr.length; i++) {
			let xmlChildren = xmlChildrenArr.item(i);
			let xmlChildrenName = xmlChildren.getNodeName();
			if (xmlChildrenName == "#text") {
				continue;
			}
			if (currentNode == nodeChildrenArr.length) {//如果当前文档结构表节点下标等于结构表节点数组长度则一定不是期望章节
				print("遍历到最后，无法匹配上");
				print(xmlChildrenArr)
				print("当前xml节点为" + xmlChildrenName);
				print("xml根节点名为：" + xmlNode.getNodeName())
				print("nodeId为：" + nodeId)
				print(nodeChildrenArr.length)
				return false
			}
			let nodeChildren = nodeChildrenArr[currentNode];
			let nodeChildrenName = nodeChildren['nodeName'];
			let nodeChildrenId = nodeChildren['nodeId'];
			let base = nodeChildren['base'];
			/**
			 * 2022-12-12 liuyz
			 * 在比较某个节点子节点时，如果子节点为必填，但是xml中为注释，那么可以忽略此节点，继续比较下一个节点
			 */
			if (xmlChildrenName == "#comment" && !nodeChildren.allowNull) {
				currentNode += 1;
				continue;
			}
			if (xmlChildrenName == nodeChildrenName) {//名称相等，尝试比较内部属性是否一致
				if (xmlChildrenName == "value" && nodeChildrenName == "value") {
					currentNode += 1;
					continue;
				}
				let xmlNodeInfoArr = xmlChildren.getAttributes();
				if (xmlNodeInfoArr && xmlNodeInfoArr.length > 0) {
					let nodeInfoArr: NodeData[] = this.nodeDataInfo.getNodeProperty(nodeChildrenId);
					for (let k = 0; nodeInfoArr && k < nodeInfoArr.length; k++) {
						let nodeInfo: NodeData = nodeInfoArr[k];
						let nodePropertyName = nodeInfo['name'];
						let defaultValue = nodeInfo['defaultValue'];
						let isExist = false;//判断上传xml中是否有对应属性 若没有则一定不是期望章节
						for (let o = 0; o < xmlNodeInfoArr.length; o++) {
							let xmlNodeProperty = xmlNodeInfoArr.item(o);
							let xmlNodePropertyName = xmlNodeProperty.name;
							let xmlNodePropertyValue = xmlNodeProperty.value;
							if (nodePropertyName == xmlNodePropertyName) {
								if (defaultValue != null && defaultValue != xmlNodePropertyValue) {//只有标准文档中有默认值的时候才进行比对
									print("当前标准节点为" + nodeChildrenName + "，xml节点为" + xmlChildrenName);
									print("当前标准节点ID为：" + nodeChildrenId)
									print(defaultValue);
									print(xmlNodePropertyValue)
									print("默认值不符合报错");
									return false;
								}
								isExist = true;
								break;
							}
						}
						if (isExist == false) {
							print("当前标准节点为" + nodeChildrenName + "，xml节点为" + xmlChildrenName);
							print("不存在属性");
							print(xmlNodeInfoArr.item(0).name);
							print(nodeInfo)
							return false;
						}
					}
				}
				if (base == "0..*" || base == "1..*") {
					if (i + 1 != xmlChildrenArr.length) {
						let nextNodeName = xmlChildrenArr.item(i + 1).getNodeName();
						if (nextNodeName != xmlChildrenName) {//若基数尾数为*的情况只有下层节点名与当前节点名不同的情况，结构表的下标才会加一
							currentNode += 1;
						}
					}
				} else {
					currentNode += 1;
				}
			} else if (nodeChildren.allowNull) {//此时标准节点和xml节点名称不一致，并且当前标准节点可以为空，那么跳过当前标准节点，继续比较下一个节点
				currentNode += 1;
				i = i - 1;
				continue;
			} else {//名称不相等，标准节点也不可以被忽略，此时比对补上，那么认为该节点缺失返回false
				print("当前标准节点为" + nodeChildrenName + "，xml节点为" + xmlChildrenName);
				print("不存在节点");
				return false;
			}
			result = this.compareComponent(xmlChildren, nodeChildrenId);
			if (!result) {//当返回了false的时候才中止，不然继续比较后续节点
				return result;
			}
		}
		if (isEmpty(result) || result == true) {
			return true;
		}
	}


	/**
	 * 比对#text节点默认值是否符合期望
	 */
	private compareText() {

	}

	/**
	 * 查询当前上传xml中至少需要有多少个子节点
	 * 因为标准文档中子节点有一些为0开头的情况下允许上传xml中没有这些子节点，但是compareComponent和compareXML方法中循环次数是根据上传的xml中子节点数量来决定的
	 * 若不判断可能会出现xml中子节点缺失但却校验通过的情况
	 */
	private queryXmlChilrendNode(nodeArr) {
		let count = 0;
		for (let i = 0; i < nodeArr.length; i++) {
			let node = nodeArr[i];
			let base = node['base'];
			if (base == "0..*" || base == "0..1" || base == null) {
				count += 1;
			}
		}
		return nodeArr.length - count;
	}

	/**
	 * 对比属性是否符合结构
	 * 首先是查询文档节点中有哪些属性，若xml节点中不包含这些属性则直接返回xml不符合规范
	 * 其次遍历判断文档中节点属性有无数据元标识，有的情况下进行校验，判断xml中数据的值是否都符合数据元标准，如果都符合，那么要将数据存入到指定表中，若不符合则记录信息
	 * 若不存在数据元标识则寻找文档和xml中节点属性有无默认值，有的情况下判断是否启用默认值校验，若启用比对他们是否一致，若不通过则记录信息
	 * 若以上情况全部通过的情况下将xml节点属性的属性值和物理表还有物理表字段一同作为JSON存入needAddTableArr数组中，若以上情况有一个失败则将物理表名记录进checkFailArr数组中
	 * 例如：结构表中有内部标识符字段值的时候就通过校验类来校验xml节点值是否符合要求，若没有内部标识符则再判断有无默认值，有默认值且启用默认值校验的情况对比默认值与xml节点值是否一致，若既没有内部标识符也没有默认值则不做校验
	 */
	private compareProperty(node, xmlNode) {
		let nodeId = node['nodeId'];
		let nodeInfoArr: NodeData[] = this.nodeDataInfo.getNodeProperty(nodeId);
		if (!nodeInfoArr) {//原逻辑使用sql进行查询过于耗费性能，调用节点信息类中获取节点属性数组因为是从json中取所以会出现undefined的情况，需要特殊处理一下将其置为空数组
			nodeInfoArr = [];
		}
		let xmlNodeInfoArr = xmlNode.getAttributes();
		for (let i = 0; i < nodeInfoArr.length; i++) {
			let nodeInfo: NodeData = nodeInfoArr[i];
			let nodePropertyName = nodeInfo.name;
			let identifiter = nodeInfo.interIdentifiter?.trim();
			let isExistXMLProperty = false;
			let tableName = nodeInfo.dbTable;
			let tableField = nodeInfo.dbField;
			let defaultValue = nodeInfo.defaultValue;
			let valueFromNode = nodeInfo.valueFromNode;
			if (nodePropertyName == "hlhtNodeValue") {//如果结构表中属性为hlhtNodeValue情况则对比当前xml节点下有无#text节点 若有则进行比对 没有则返回格式有误
				let nodeValue = xmlNode.getTextContent();
				if (nodeValue) {
					if (valueFromNode != null && valueFromNode != "") {
						nodeInfoArr.forEach(prop => {
							if (prop.name == valueFromNode) {
								this.checkPropValueInDim(prop.interIdentifiter, nodeValue, nodeInfo, nodeId + "/" + valueFromNode);
								return;
							}
						})
					} else {
						this.comparePropertyValue(identifiter, defaultValue, tableName, tableField, nodeValue, nodeId);
					}
				}
				continue;
			}
			for (let o = 0; o < xmlNodeInfoArr.length; o++) {
				let xmlNodeInfo = xmlNodeInfoArr.item(o);
				let xmlNodePropertyName = xmlNodeInfo.name;
				let xmlNodePropertyValue = xmlNodeInfo.value;

				if (xmlNodePropertyName == nodePropertyName) {
					isExistXMLProperty = true;
					if (valueFromNode != null && valueFromNode != "") {
						nodeInfoArr.forEach(prop => {
							if (prop.name == valueFromNode) {
								this.checkPropValueInDim(prop.interIdentifiter, xmlNodePropertyValue, nodeInfo, nodeId + "/" + valueFromNode);
								return;
							}
						})
					} else {
						this.comparePropertyValue(identifiter, defaultValue, tableName, tableField, xmlNodePropertyValue, nodeId);
					}
					break;
				}
			}
			if (isExistXMLProperty == false) {
				this.logger.addLog(`xml格式不正确，上传xml中没有${nodePropertyName}此节点属性,当前结构表节点为${nodeId},xml当前节点名为：${xmlNode.getNodeName()}`);
				this.logger.addLog(`xml当前节点属性为${xmlNode.toString()}`)
				return { result: false, message: `xml格式不正确，上传xml中没有${nodePropertyName}此节点属性,xml当前节点名为：${xmlNode.getNodeName()}` }
			}
		}
		return { result: true, message: `xml格式正确` }
	}

	/**
	 * 2022-12-27 liuyz
	 * 获取xml节点字符串
	 * eg：<value type="123" code="123"></value>
	 * 为了方便实施人员定位问题，将当前节点前后节点一并输出
	 */
	private getNodeXmlString(){

	}

	/**
	 * 对比属性方法
	 * @param identifiter 内部标识符
	 * @param defaultValue 默认值
	 * @param tableName 物理表名
	 * @param tableField 物理表字段
	 * @param xmlNodeValue 上传xml的节点值
	 * @param nodeId 结构表节点ID
	 * @param nodeName 结构表节点名
	 */
	private comparePropertyValue(identifiter, defaultValue, tableName, tableField, xmlNodeValue, nodeId) {
		if (identifiter) {
			let chekcResult = this.checkClass.executeCheckFieldValueIsLegal(identifiter, xmlNodeValue);
			if (chekcResult['result']) {//若校验通过且存在物理表和物理表字段值的时候将其放入needAddTableArr数组
				this.insertNeedAddTableArr(tableName, tableField, xmlNodeValue, identifiter);
			} else {
				let message = chekcResult['message'];
				if (chekcResult['checkResult']) {
					if (!chekcResult['checkResult']['formCheckResult']['result']) {
						message = chekcResult['checkResult']['formCheckResult']['message'];
					} else if (!chekcResult['checkResult']['typeCheckResult']['result']) {
						message = chekcResult['checkResult']['typeCheckResult']['message'];
					}
				}
				this.logger.addLog(`属性数据元标识校验不通过，结果为：${message}，当前数据源标识符为：${identifiter}，节点ID为：${nodeId}，xml值为${xmlNodeValue}`)
				this.checkFailArr.push(tableName);
				this.errorInfoArr.push({
					"TASK_ID": this.taskId,
					"CREATE_TIME": new java.util.Date(),
					"DB_TABLE": tableName,
					"DB_FIELD": tableField,
					"INTER_IDENTIFITER": identifiter,
					"WORD_ID": this.wordId,
					"DETAIL": message,
					"NODE_ID": nodeId,
					"CHECK_PK_VALUE": "null",
				})
			}
		} else if (defaultValue && this.isCheckDefaultValue) {
			this.logger.addLog(`当前默认值为：${defaultValue}，xml值为${xmlNodeValue}，是否启动默认值校验：${this.isCheckDefaultValue}`)
			if (defaultValue != xmlNodeValue) {
				//this.checkFailArr.push(tableName);
				this.errorInfoArr.push({
					"TASK_ID": this.taskId,
					"CREATE_TIME": new java.util.Date(),
					"DB_TABLE": tableName,
					"DB_FIELD": tableField,
					"INTER_IDENTIFITER": identifiter,
					"WORD_ID": this.wordId,
					"DETAIL": "上传xml中节点属性默认值校验不通过",
					"NODE_ID": nodeId,
					"CHECK_PK_VALUE": "null",
				})
			}
		}
	}

	/**
	 * 校验当前值是否存在于指定内部标识符对应维表的描述内
	 * https://jira.succez.com/browse/CSTM-21569
	 * @param identifiter
	 * @param xmlNodeValue
	 * @param nodeInfo
	 * @param nodeId
	 */
	private checkPropValueInDim(identifiter: string, xmlNodeValue: string, nodeInfo: NodeData, nodeId: string) {
		let result = this.checkClass.checkValueIsInDimDesc(identifiter, xmlNodeValue);
		if (!result.result) {
			let message = result.message;
			this.logger.addLog(`属性数据元标识校验不通过，结果为：${message}，当前数据源标识符为：${identifiter}，节点ID为：${nodeId}，xml值为${xmlNodeValue}`)
			this.checkFailArr.push(nodeInfo.dbTable);
			this.errorInfoArr.push({
				"TASK_ID": this.taskId,
				"CREATE_TIME": new java.util.Date(),
				"DB_TABLE": nodeInfo.dbField,
				"DB_FIELD": nodeInfo.dbField,
				"INTER_IDENTIFITER": identifiter,
				"WORD_ID": this.wordId,
				"DETAIL": message,
				"NODE_ID": nodeId,
				"CHECK_PK_VALUE": "null",
			})
		}
	}

	/**
	 * 若xml中孩子节点少于结构表中孩子节点则返回缺少的节点信息
	 */
	private returnNodeName(xmlChildrenArr, nodeChildrenArr) {
		let nodeNameArr: any[] = [];
		for (let i = 0; i < nodeChildrenArr.length; i++) {
			let nodeChildrenName = nodeChildrenArr[i]['nodeName'];
			let isExist = false;
			for (let k = 0; k < xmlChildrenArr.length; k++) {
				let xmlChildrenName = xmlChildrenArr.item(k).getNodeName();
				if (xmlChildrenName == nodeChildrenName) {
					isExist = true;
				}
			}
			if (isExist == false) {
				nodeNameArr.push(nodeChildrenName)
			}
		}
		return nodeNameArr;
	}

	/**
	 * 将数据构建成json写入needAddTableArr数组中
	 */
	private insertNeedAddTableArr(tableName, tableField, xmlNodeValue, identifiter) {
		if (tableName && tableField) {
			let metaDataType = this.metaDataInfo[identifiter]['metaDataType'];
			if (metaDataType == 'D' || metaDataType == 'T' || metaDataType == "DT" || metaDataType == 'D8' || metaDataType == 'T6' || metaDataType == 'DT15') {
				let sdf;
				switch (metaDataType) {
					case "D": sdf = new java.text.SimpleDateFormat("YYYYMMDD");
						break;
					case "D8": sdf = new java.text.SimpleDateFormat("YYYYMMDD");
						break;
					case "T": sdf = new java.text.SimpleDateFormat("hhmmss");
						break;
					case "T6": sdf = new java.text.SimpleDateFormat("hhmmss");
						break;
					case "DT": sdf = new java.text.SimpleDateFormat("YYYYMMDD\'T\'hhmmss");
						break;
					case "DT15": sdf = new java.text.SimpleDateFormat("YYYYMMDD\'T\'hhmmss");
						break;
				}
				let date = new java.util.Date();
				try {
					date = sdf.parse(xmlNodeValue);
				} catch (e) {
					this.checkFailArr.push(tableName);
					this.logger.addLog(`字符串转换时间类型出现错误，当前待写入物理表名：${tableName},xml节点值：${xmlNodeValue},内部标识符：${identifiter},错误信息${e}`);
				}
				xmlNodeValue = date;
			} else if (metaDataType == 'L') {
				if (xmlNodeValue == 'true') {
					xmlNodeValue = '1';
				} else if (xmlNodeValue == 'false') {
					xmlNodeValue = '0';
				} else {
					this.checkFailArr.push(tableName);
					this.logger.addLog(`当前xml节点值不符合标准，此处xml节点值因为true或false，数据可能有误，当前待写入物理表名：${tableName},xml节点值：${xmlNodeValue},内部标识符：${identifiter}`);
				}
			}
			this.needAddTableArr.push({
				"nodeValue": xmlNodeValue,
				"tableName": tableName,
				"tableField": tableField
			})
		}
	}

	/**
	 * 过滤加工需要写入物理表的数据
	 * 过滤之后将数据重新构建好数据进行写表操作
	 *
	 * TODO 目前仅处理了每张物理表写入一条数据的情况，多条数据的情况由于暂时没有数据不便测试，目前逻辑下可能需要重构写入数据部分，数据方面也不好构建，等有了具体业务需求与测试数据再做完成，此处仅记录一下实现思路
	 * 将需要写入的字段值与字段名构建存为数组，若按现有逻辑进行修改需要在处理完filedNameArr数组后将判断将重复的字段值作为数组存入同一个字段名下，然后遍历得出数组长度最大的长度，根据长度最大的数组来决定遍历写入几条数据
	 * 内部再写一个循环根据字段名来遍历 将与字段名一一对应的字段值作为数组存入待写入的二维数组中，字段值有多值的情况根据外部循环次数作为数组下标取出对应字段值存入数组中，例如：
	 * for(let i=0;i<max.length;i++){
	 * 		for(let k=0;k<filedNameArr.length;k++){
	 * 				needInsertArr.push([filedA,filedB,filed[i]])
	 * 		}
	 * }
	 */
	private filterNeedAddTableArr() {
		let needAdd = {};
		for (let i = 0; i < this.needAddTableArr.length; i++) {
			let needAddTable = this.needAddTableArr[i];
			if (this.checkFailArr.indexOf(needAddTable['tableName']) != -1) {
				continue;
			}
			if (!needAdd[needAddTable['tableName']]) {
				needAdd[needAddTable['tableName']] = [];
			}
			let data = {};
			if (Array.isArray(needAddTable['tableField'])) {
				for (let o = 0; o < needAddTable['tableField'].length; o++) {
					data[needAddTable['tableField'][o]] = needAddTable['nodeValue'][o];
				}
			} else {
				data[needAddTable['tableField']] = needAddTable['nodeValue'];
			}
			needAdd[needAddTable['tableName']].push(data);
		}
		try {
			this.insertTable(needAdd);
		} catch (e) {
			this.insertFailInfo.push(e)
		}
	}

	/**
	 * 将构建好的数据写入指定物理表中
	 */
	private insertTable(needAdd) {
		let keys = Object.keys(needAdd);
		for (let i = 0; i < keys.length; i++) {
			let filedNameArr: string[] = [];
			let filedValueArr: string[] = [];
			let tableName = keys[i];
			needAdd[tableName].push({ PAT_INDEX_NO: this.taskId });//TODO 主键要单独获取
			let valueArr = needAdd[tableName];
			for (let o = 0; o < valueArr.length; o++) {
				let value = valueArr[o];
				let filed = Object.keys(value);
				for (let k = 0; k < filed.length; k++) {
					if (filedNameArr.indexOf(filed[k]) != -1) {
						this.logger.addLog(`待写入物理表字段名重复，此处需构建多条数据写入物理表，重复字段名为${filed[k]},当前物理表名为:${tableName}，重复的字段值为：${value[filed[k]]}`);
						continue;
					}
					filedNameArr.push(filed[k]);
					filedValueArr.push(value[filed[k]])
				}
			}
			let table = this.hlht.openTableData(tableName, this.hlhtSchema, this.hlhtConn);
			try {
				table.insert([filedValueArr], filedNameArr)
			} catch (e) {
				this.logger.addLog(`写入物理表过程中出现错误，写入字段名数组为${filedNameArr},当前物理表名为:${tableName}，写入的字段值数组为：${filedValueArr}`);
				this.insertFailInfo.push(e);
			}
		}
	}
}


/**
 * 根据模型ID获取模型路径和物理主键名
 * @param modelId 模型ID
 * @return
 */
function queryModelPathAndPkField(modelId: string): { modelPath: string, modelPkField: string, modelPkTextField?: string } {
	if (!modelId) {
		return { modelPath: "", modelPkField: "" };
	}
	let modelFile = metadata.getFile(modelId);
	if (modelFile == null) {
		console.info(`模型[${modelId}] 不存在, 无法查询维表数据`);
		return { modelPath: "", modelPkField: "" };
	}
	let modelPath: string = modelFile.path;
	let attachModelStr = metadata.getString(modelPath);
	let modelInfo = JSON.parse(attachModelStr);
	let pkFieldDescs: string[] = modelInfo['properties'].primaryKeys;
	if (!pkFieldDescs || pkFieldDescs.length == 0 || pkFieldDescs.length > 1) {
		console.info(`模型[${modelId}] 没有设置主键, 无法查询维表数据`);
		return { modelPath: "", modelPkField: "" };
	}
	let pkDbField: string = "";
	let pkTextField: string = "";
	let modelFields: DwTableFieldItemInfo[] = modelInfo['dimensions'];
	for (let i = 0; i < modelFields.length; i++) {
		let fieldInfo: DwTableFieldItemInfo = modelFields[i];
		let fieldDesc: string = fieldInfo["name"];
		if (pkFieldDescs.includes(fieldDesc)) {
			let dbField: string = fieldInfo["dbfield"];
			pkDbField = dbField;
			pkTextField = fieldInfo['textField'];
			break;
		}
	}
	return { modelPath: modelPath, modelPkField: pkDbField, modelPkTextField: pkTextField };
}


export interface WordInfo {
	/** 文档id */
	wordId: string;
	/** 文档名称 */
	wordName: string;
	/** 查询主表 */
	mainDbTable: string;
	/**  查询模型所在的根目录*/
	tableDirPath: string;
}

/** 结构信息 */
export interface WordStructure {
	/** 节点id */
	nodeId: string;
	/** 节点名称 */
	nodeName: string;
	/** 父节点id */
	pNodeId?: string;
	/** 章节名称 */
	sectionName: string;
	/** 基数 */
	base: string;
	/** 约束 */
	constraint: string;
	/** 是否允许为空，默认为false */
	allowNull: boolean;
	/** 是否允许查询多行,默认为false */
	allowMultiple?: boolean;
	/** 层级 */
	level: number;
	/** 是否叶子节点 */
	isLeaf: boolean;
	sort: number;
	/** XML节点 */
	xmlNode?: DefaultElement;
	/** 是否有备注信息 */
	hasComment: boolean;
	comment?: string;
}

/** 节点属性 */
export interface NodeData {
	/** 属性名称（命名规范：不包含冒号，否则产品解析XML失败） */
	name: string;
	/** 默认值 */
	defaultValue: any;
	/** 数据元ID */
	elementId?: string;
	/** 内部标识符 */
	interIdentifiter: string;
	/** 物理表 */
	dbTable: string;
	/** 查询物理表 */
	dbField: string;
	/** 节点值 */
	value: any;
	/** 节点取值来源属性 */
	valueFromNode?: string;
}

/** XML属性存在多值配置 */
export interface XmlNodeMutilValueProperties {
	/** 文档结构节点序号 */
	nodeId: string;
	/** 属性名 */
	name: string;
	/** 属性值 */
	values: string[]
}

/** 数据元映射表所涉及维表属性（字段一致） */
export interface DimTableProperties {
	/** ID */
	Z: string;
	/** 中文值 */
	ZHY: string;
	/** 说明 */
	SM: string;
}

/** 数据元映射表属性 */
export interface MetaDataMappingTable {
	/** 内部标识符 */
	interIdentifiter?: string;
	/** 元数据标识符 */
	metaDataIdentify: string;
	/** 元数据名称 */
	metaDataName: string;
	/** 元数据类型 */
	metaDataType: string;
	/** 表示格式 */
	expressForm: string;
	/** 维表名 */
	dimTableName: string;
}

/** XML节点元数据类型 */
export enum XmlNodeMetaDataType {
	/** 字符型 */
	S1 = "S1",
	/** 枚举值（维表） */
	S2 = "S2",
	/** 代码表（维表） */
	S3 = "S3",
	/** 布尔型 */
	L = "L",
	/** 数值型 */
	N = "N",
	/** 日期型 */
	D = "D",
	/** 日期时间型 */
	DT = "DT",
	/** 时间型 */
	T = "T",
	/** 二进制 */
	BY = "BY"
}

/** XML节点元数据表示格式 */
export enum XmlNodeMetaDataFormat {
	/** 字符 */
	A = "A",
	/** 数字 */
	N = "N",
	/** 字母或数字 */
	AN = "AN",
	/** 布尔类型 */
	T = "T/F",
	/** 日期型 */
	D8 = "D8",
	/** 时间型 */
	T6 = "T6",
	/** 日期时间型 */
	DT15 = "DT15"
}

/** XML文件附件信息 */
export interface XMLAttachmentInfo {
	/** 文档ID */
	WORD_ID?: string;
	/** 主键ID */
	EMPIID?: string;
	/** 文档索引号 */
	WORD_INDEX?: string;
	/** 文档流水号 */
	WORD_SERIAL_NUMBER?: string;
	/** 版本号 */
	VERSION_NUMBER?: string;
	/** 生成时间 */
	CREATE_TIME?: string | Date;
	/** 修改时间 */
	MODIFY_TIME?: string | Date;
	/** 文档存储路径, eg: default：文件名 */
	FILE_PATH?: string;
	/** XML MD5加密内容 */
	CONTENT_MD5?: string;
	/** 文档创作者标识符 */
	AUTHOR_ID?: string;
	/** 文档创作者姓名 */
	AUTHOR_NAME?: string;
	/** 医疗机构编码 */
	ORGANIZATION_ID?: string;
	/** 医疗机构名称 */
	ORGANIZATION_NAME?: string;
	/** 文档保管机构编码 */
	CUSTODIAN_ID?: string;
	/** 文档报关机构名称 */
	CUSTODIAN_NAME?: string;
}