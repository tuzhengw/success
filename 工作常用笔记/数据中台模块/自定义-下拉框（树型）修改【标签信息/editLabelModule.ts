/**
 * ============================================================
 * 作者: tuzhengw
 * 审核人员：liuyongz
 * 创建日期：2022-06-07
 * 功能描述：
 *      该脚本主要是：通过下拉框形式展示某表的标签信息，通过勾选状态来修改标签
 * 问题：https://jira.succez.com/browse/CSTM-19030
 * ============================================================
 */
import { Component, ComponentArgs, ctx, message, SZEvent, showWarningMessage, wait, showSuccessMessage } from 'sys/sys';
import 'css!./editLabelModule.css';
import { getLabels, labelData, LabelDataArgs, LabelDataResult } from "dw/dwapi";
import { Tree, TreeArgs, TreeItem } from "commons/tree";
import { getQueryManager } from "dw/dwapi";
import { uuid } from "sys/sys";

/**
 * 标签编辑下拉框对象（树形）
 */
export class EditLabelModule extends Tree {

    /** 树节点渲染数据来自的模型表ID或路径 */
    public treeBindModelTableResidOrPath: string;
    /** 查询的行主键值 */
    public rowPrimaryKey: string;

    /** 当前表-所有硬标签信息（不包含隐藏的） */
    public allLabelDatas: DwDataLabel[];

    /** 标签分组-所有标签分组信息 */
    public labelGroupInfos: JSONObject;
    /** 当前表—标签分组编码集（不包含隐藏的分组ID） */
    public tableLabelGroupIds: string[];

    /** 当前模型-标签库ID */
    public libCodeId: string;
    /** 当前模型-标签内容信息集 */
    public dataLabelInfos: DW_DATA_LABELS[];

    /** 隐藏的标签分组编码集，eg：['NlyVQIR7IXDdrxDx1YQmEB', 'VjFkbr0qPkHL676eHquPEF'] */
    public noDisplayLabelGroupIds: string[];
    /** 隐藏的标签编码集，eg：['4200008221_yanhan_001', '4200008221_yanhan_002'] */
    public noDisplayLableIds: string[];
    /** 是否初始化展开全部节点 */
    public isExpandAll: boolean;

    /** 初始记录标签编码数组 */
    public initLabelIds: string[];

    /** 是否停止初始化 */
    public isStopInit: boolean;
    /** 是否正在执行reInit方法 */
    public isExcuteReInit: boolean;
    /** 是否首次加载 */
    public isFristInit: boolean;

    /**
     * @param checkBoxVisible：是否显示勾选框，默认：false
     */
    constructor(args: TreeArgs) {
        super(args);
    }

    /**
     * @param treeBindModelTableResidOrPath 树节点渲染数据来自的模型表ID或路径
     * @param rowPrimaryKey 某行主键值
     * @param noDisplayLabelGroupIds? 隐藏的标签分组编码集，eg：'NlyVQIR7IXDdrxDx1YQmEB', 'VjFkbr0qPkHL676eHquPEF'
     * @param noDisplayLableIds? 隐藏的标签编码集，eg：'4200008221_yanhan_001', '4200008221_yanhan_002'
     * @param isExpandAll? 是否初始全部展开节点
     */
    protected _init(args: TreeArgs): HTMLElement {
        this.treeBindModelTableResidOrPath = args.treeBindModelTableResidOrPath;
        if (!this.treeBindModelTableResidOrPath) {
            this.isStopInit = true;
            showWarningMessage("未获取到传入的模型resid或者路径");
            return;
        }
        this.rowPrimaryKey = args.rowPrimaryKey;
        if (!this.rowPrimaryKey) {
            this.isStopInit = true;
            showWarningMessage("未指定要查看的行主键值");
            return;
        }
        this.noDisplayLabelGroupIds = !!args.noDisplayLabelGroupIds ? args.noDisplayLabelGroupIds.split(",") : [];
        this.noDisplayLableIds = !!args.noDisplayLableIds ? args.noDisplayLableIds.split(",") : [];
        this.isExpandAll = !!args.isExpandAll ? true : false;

        this.initLabelIds = [];
        this.tableLabelGroupIds = [];
        this.dataLabelInfos = [];
        this.labelGroupInfos = [];
        this.allLabelDatas = [];
        this.isStopInit = false;
        this.isExcuteReInit = false;
        this.isFristInit = true;

        let domBase = super._init(args);
        return domBase;
    }

    /**
     * 重新打开渲染数据方法
     * 1）由于初始化init()方法时，也会执行此方法，避免重复加载多次，增加一个变量来控制
     * 2）由于每次渲染，会创建一个DP对象，后续重新渲染会调用dp对象获取，而不会使用自定义的树数据渲染，故_reInit()内将已创建好的DP对象置NULL
     */
    protected _reInit(args: TreeArgs): void {
        if (this.isExcuteReInit) {
            return;
        }
        if (this.isFristInit) {
            this.isFristInit = false;
            return;
        }
        this.isExcuteReInit = true;

        super._reInit(args);
        this.rowPrimaryKey = args.rowPrimaryKey;
        if (!this.rowPrimaryKey) {
            this.isStopInit = true;
            showWarningMessage("未指定要查看的行主键值");
            return;
        }
        this.removeAll();// 清空树数据
        this.dataProvider = null;

        this._init_data(args).then(() => {
            this.isExcuteReInit = false;
        });
    }

    /**
     * 重写初始化数据方法
     * 由于原本树是根据dp查询数据来渲染树，现需求是不需要通过dp查询，而是直接根据某表的标签数据构建树节点，故重写init_data方法，给定树节点数据items
     * @return 
     */
    protected _init_data(args: TreeArgs): Promise<void> {
        if (this.isStopInit) {
            return;
        }
        return this.getCatelogInfos().then(() => {
            return this.getLables(this.treeBindModelTableResidOrPath).then((allLabelDatas: DwDataLabel[]) => {
                if (!allLabelDatas || allLabelDatas.length == 0) {
                    return <Promise<void>>super._init_data(args);
                }
                this.allLabelDatas = allLabelDatas;
                return this.getLables(this.treeBindModelTableResidOrPath, this.rowPrimaryKey).then((rowLabelDatas: DwDataLabel[]) => { // 加载当前表某行的标签数据（勾选）
                    args.items = this.dealTreeData(rowLabelDatas);
                    return <Promise<void>>super._init_data(args).then(() => {
                        return this.expandAll(this.isExpandAll);  // 展开节点必须等待树控件加载完成后执行
                    });
                });
            });
        });
    }

    /**
     * 构造整颗树的数据（总两层）
     * @param rowLabelDatas 当前表某行的标签数据集
     * @return 
     */
    public dealTreeData(rowLabelDatas: DwDataLabel[]): TREE_NODE[] {
        let treeItems: TREE_NODE[] = [];
        for (let groupId = 0; groupId < this.tableLabelGroupIds.length; groupId++) { // 最外层-标签分组信息
            let parentCategoryId: string = this.tableLabelGroupIds[groupId];
            let parentCaption: string = this.labelGroupInfos[parentCategoryId];
            let parent: TREE_NODE = {
                "caption": parentCaption,
                "checked": false,
                "className": "custom_Tree-row",
                "id": parentCategoryId,
                "value": parentCategoryId,
                "items": [],
                "level": 0
            }
            let childrenItem: TREE_NODE[] = [];
            for (let libId = 0; libId < this.allLabelDatas.length; libId++) { // 内层-分组内标签信息
                let childrencategoryId: string = this.allLabelDatas[libId].category;
                let codeId: string = this.allLabelDatas[libId].code;

                let libCaption: string = this.allLabelDatas[libId].name;
                if (childrencategoryId == parentCategoryId) {
                    let children: TREE_NODE = {
                        "caption": libCaption,
                        "checked": false,
                        "className": "custom_Tree-row",
                        "id": codeId,
                        "value": codeId,
                        "level": 1
                    }
                    let isChecked = this.checkLabelIsRowLabel(codeId, rowLabelDatas);
                    if (isChecked) { // 判断当前标签是否为当前表-指定行的标签，若是，则勾选
                        children['checked'] = true;
                    }
                    childrenItem.push(children);
                }
            }
            parent.items.pushAll(childrenItem);
            treeItems.push(parent);
        }
        return treeItems;
    }

    /**
     * 判断标签是否为指定行的标签信息（用于判断是否自动勾选此标签）
     * @param codeId 标签编码
     * @param rowLabelDatas 某行标签信息数组
     * @return true | false
     */
    private checkLabelIsRowLabel(codeId: string, rowLabelDatas: DwDataLabel[]): boolean {
        let isRowLabel: boolean = false;
        for (let i = 0; i < rowLabelDatas.length; i++) {
            if (rowLabelDatas[i].code == codeId) {
                isRowLabel = true;
                break;
            }
        }
        return isRowLabel;
    }

    /**
     * 获取某表 或者 某表某行数据-硬标签信息
     * 标签表：/sysdata/data/tables/dw/DW_DATA_LABELS_LIB.tbl
     * @param modelResidOrPath 模型Resid 或 路径
     * @param rowKey? 行主键值
     * return 
     */
    public getLables(modelResidOrPath: string, rowKey?: string): Promise<any> {
        if (!modelResidOrPath) {
            showWarningMessage("参数为空，请检查参数设置");
            return Promise.resolve(false);
        }
        let labelParam = {
            path: modelResidOrPath
        }
        if (!!rowKey) {
            labelParam["key"] = rowKey;
        }
        /** 硬标签数据 */
        let hardLabelDatas = [];
        return getLabels(labelParam).then(data => {
            if (data.length == 0) {
                showWarningMessage("没有标签信息");
                return [];
            }
            for (let i = 0; i < data.length; i++) {
                if (data[i].labelType == 2) { // 校验是否硬标签
                    if (!this.libCodeId) { // 记录当前模型的标签库ID值，仅记录一次
                        this.libCodeId = data[i].libCode;
                    }
                    let labelGroupId: string = data[i].category;
                    if (this.noDisplayLabelGroupIds.length != 0 && this.noDisplayLabelGroupIds.includes(labelGroupId)) { // 若顶层分组被隐藏，则内部标签都不处理
                        continue;
                    }
                    let labelCode: string = data[i].code;
                    if (this.noDisplayLableIds.length != 0 && this.noDisplayLableIds.includes(labelCode)) { // 校验当前标签是否隐藏
                        continue;
                    }
                    if (!this.tableLabelGroupIds.includes(labelGroupId)) {
                        this.tableLabelGroupIds.push(labelGroupId); // 避免重复记录分组信息
                    }
                    if (!!rowKey) { // 记录没有被隐藏的标签编码
                        this.initLabelIds.push(labelCode);
                    }
                    hardLabelDatas.push(data[i]);
                }
            }
            return hardLabelDatas;
        });
    }

    /**
     * 获取全部的标签分组信息
     * @return eg：{
     *    "c497669e7fb240c397475dd6d608ac7e" : "业务标签"
     * }
     */
    public getCatelogInfos(): Promise<void> {
        let queryInfo: QueryInfo = {
            sources: [{
                id: "model1",
                path: "/sysdata/data/tables/sdi/data-label/dims/DIM_LABEL_CATEGORY.tbl"
            }],
            fields: [{
                name: "CODE", exp: "model1.CODE"
            }, {
                name: "CAPTION", exp: "model1.CAPTION"
            }],
            filter: [],
            select: true
        }
        return getQueryManager().queryData(queryInfo, uuid()).then(result => {
            let data = result.data;
            let labelGroupInfos = {};
            for (let i = 0; i < data.length; i++) {
                labelGroupInfos[data[i][0]] = data[i][1];
            }
            this.labelGroupInfos = labelGroupInfos;
        });
    }

    /**
     * 根据勾选差异，更新当前数据行的标签信息
     */
    public adjustLabelInfo(): Promise<void> {
        let { newAddLableIds, deleteLableIds } = this.getAddAndDeleteLabelIds();
        let labelDataArgs: LabelDataArgs;
        let labelPromises: Promise<any>[] = [];
        for (let i = 0; i < newAddLableIds.length; i++) { // 新增标签
            labelDataArgs = {
                labelCode: newAddLableIds[i],
                labelLibCode: this.libCodeId,
                labelKeys: [this.rowPrimaryKey],
                overwriteMode: "append"
            }
            let promise: Promise<LabelDataResult> = labelData(labelDataArgs);
            labelPromises.push(promise);
        }
        for (let i = 0; i < deleteLableIds.length; i++) { // 删除标签
            labelDataArgs = {
                labelCode: deleteLableIds[i],
                labelLibCode: this.libCodeId,
                delabelKeys: [this.rowPrimaryKey],
                overwriteMode: "append"
            }
            let promise: Promise<LabelDataResult> = labelData(labelDataArgs);
            labelPromises.push(promise);
        }
        return Promise.all(labelPromises).then((values) => {
            console.log(values);
            showSuccessMessage("调整完成");
            this.initLabelIds = this.getCheckedNodeIds(); // 更新完成后，重新赋值-初始记录的ID集
        });
    }

    /**
     * 对比-初始记录标签编码数组，根据勾选差异，获取新增、删除的标签编码集
     * 注意：根据差异更新标签信息后，根据当前勾选状态，重新赋值-初始记录的编码集
     * @return 
     */
    public getAddAndDeleteLabelIds(): {
        /** 新增的标签编码ID集 */
        newAddLableIds: string[],
        /** 删除的标签编码ID集 */
        deleteLableIds: string[]
    } {
        let currentCheckedNodeIds: string[] = this.getCheckedNodeIds();
        let newAddLableIds: string[] = [];
        let deleteLableIds: string[] = [];
        for (let i = 0; i < this.initLabelIds.length; i++) {
            if (!currentCheckedNodeIds.includes(this.initLabelIds[i])) { // 若节点不存在最新的勾选ID中，则视为删除
                deleteLableIds.push(this.initLabelIds[i]);
            }
        }
        for (let i = 0; i < currentCheckedNodeIds.length; i++) {
            if (!this.initLabelIds.includes(currentCheckedNodeIds[i])) { // 若节点不存在之前记录的勾选ID集中，则视为新增
                newAddLableIds.push(currentCheckedNodeIds[i]);
            }
        }
        return { newAddLableIds: newAddLableIds, deleteLableIds: deleteLableIds };
    }

    /**
     * 获取当前最新选中的节点编码集（标签编码）
     * @return 
     */
    public getCheckedNodeIds(): string[] {
        /** 当前展开的全部节点信息 */
        let allItems: TreeItem[] = this.getItems() as TreeItem[];
        /** 当前页面勾选的最新节点ID集 */
        let currentCheckedNodeIds: string[] = [];
        for (let i = 0; i < allItems.length; i++) {
            if (allItems[i].getChecked() && allItems[i].getLevel() != 0) {
                currentCheckedNodeIds.push(allItems[i].getId());
            }
        }
        return currentCheckedNodeIds;
    }
}

/** 树节点 */
interface TREE_NODE {
    /** 节点标题 */
    caption: string;
    /** 是否勾选 */
    checked: boolean;
    /** 对话框中展示，得给定css样式，否则，宽度为0 */
    className?: string;
    /** 节点ID */
    id: string;
    /** 节点值 */
    value: string;
    /** 节点孩子节点集 */
    items?: TREE_NODE[];
    /** 当前节点层级，第一层：0 */
    level: number;
}

/** 数据内容标签表 */
interface DW_DATA_LABELS {
    /** 标签编码 */
    LABEL_CODE: string;
    /** 标签名称 */
    LABEL_NAME: string;
}