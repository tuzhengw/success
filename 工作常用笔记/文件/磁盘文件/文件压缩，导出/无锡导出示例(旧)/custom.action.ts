import security from "svr-api/security";
import etl from "svr-api/etl";
import { getDataSource } from "svr-api/db";
import Files_ = java.nio.file.Files;
import Paths_ = java.nio.file.Paths;
import sys from "svr-api/sys";
import dw from "svr-api/dw";
import utils from "svr-api/utils";
import fs from "svr-api/fs";
import metadata from "svr-api/metadata";

import HttpClients = org.apache.http.impl.client.HttpClients;
import EntityUtils = org.apache.http.util.EntityUtils;
import HttpGet = org.apache.http.client.methods.HttpGet;
import JavaFile = java.io.File;

import ZipOutputStream = java.util.zip.ZipOutputStream;
import ZipEntry = java.util.zip.ZipEntry;
import ArrayUtils = org.apache.commons.lang3.ArrayUtils

import FileOutputStream = java.io.FileOutputStream;
import FileInputStream = java.io.FileInputStream;
import IOUtils = org.apache.commons.io.IOUtils;

/** 系统工作目录路径 */
let workDir = sys.getWorkDir();
/** 导出压缩包暂存父路径 */
const importZipParentPath: string = `${workDir}/wljy_fjcc/tempSaveZipFile/`
/** 导出网络交易监管信息登记表所在位置 */
const importAttachWordPath: string = '/ZHJG/app/home.app/jgrw/WLJYJG/网络交易监测违法信息登记表.d.docx';
/** 导出文件父路径 */
const importFileParentPath: string = `${workDir}/wljy_fjcc/2022-02-22/`;

/**
 * 压缩指定线索号对应的: 截图文件、存证文件、违法点截图, 并导出
 * @author tuzw
 * @createDate 2022-01-24
 * @see https://jira.succez.com/browse/CSTM-17763
 * @param importSabaCodes 线索号（导出文件夹名），eg："2022012420057934, 2022020720016376"
 * <p>
 *  1 由于压缩的文件已经创建完成（除检测违法信息表未创建）, 压缩读取字节流耗时;
 *  2 在线索清单页面增加批量导出功能，用户勾选要导出的线索，点击批量导出后，将每条线索的截图文件、存证文件、违法点截图打包成一个文件下载下来，每个线索单独一个文件;
 *  3 考虑到请求此方法是通过URL方式, URL携带参数最大长度: 2083字节, 超过则不会被请求, 为了尽可能导出多个文件, 参数不传递压缩文件夹路径, 改为线索号, 内部获取压缩路径;
 *  4  后端请求依赖方法 {@link #createMonitorIllegalWordFile};
 *  5 页面地址：http://58.215.18.230:8280/zhjg/ZHJG/app/home.app?:edit=true&:file=wljyjgxsqd.spg
 * </p>
 */
export function zipMonitorIlegalInfoFiles(request: HttpServletRequest, response: HttpServletResponse, params: {
    importSabaCodes: string
}): void {
    print(`开始执行[zipMonitorIlegalInfoFiles], 压缩指定线索号对应的: 截图文件、存证文件、违法点截图, 并导出`);
    print(params);

    if (!params.importSabaCodes) {
        print(`传入线索号`);
        return;
    }
    let startTime: number = Date.now();
    let importSabaCodes: string[] = params.importSabaCodes.split(",");
    let importFileDirs: string[] = []; // 记录服务器所存 截图文件、存证文件、违法点截图文件 父路径
    for (let i = 0; i < importSabaCodes.length; i++) {
        let tempPath: string = `${importFileParentPath}${importSabaCodes[i]}/`;
        importFileDirs.push(tempPath);
    }
    let zipName: string = utils.uuid();
    let zipSavePath: string = `${importZipParentPath}${zipName}.zip`;
    zipUtils.zipDirs(importFileDirs, zipSavePath, true);

    let sendFile = new java.io.File(zipSavePath);
    let fileBytes = Files_.readAllBytes(Paths_.get(sendFile.getAbsolutePath())); // 通过绝对路径获取文件字节流

    response.reset();
    let importZipName: string = java.net.URLEncoder.encode("网络交易监管线索信息.zip", "utf-8");
    response.setHeader("Content-Disposition", "attachment; filename=" + importZipName); // 给定【文件下载】请求头
    response.setContentType("application/octet-stream");

    response.setContentLength(fileBytes.length);
    let out: OutputStream = null;
    try {
        out = response.getOutputStream();
        out.write(fileBytes);
        out.flush();
    } catch (e) {
        print(`线索信息文件压缩导出失败-error`);
        print(e);
    } finally {
        out && out.close();
        let zipIsdelete: boolean = sendFile.delete(); // 将导出的内容写入response后，导出zip文件进行销毁
        print(`导出临时生成的压缩包删除情况：${zipIsdelete}`);
        for (let i = 0; i < importFileDirs.length; i++) { // 删除临时加入的附件word文件
            let tempFile = fs.getFile(`${importFileDirs[i]}网络交易监管信息登记表.docx`);
            if (tempFile.exists()) {
                let deleteStatus: boolean = tempFile.delete();
                print(`附件：${importFileDirs[i]}网络交易监管信息登记表.docx 删除: ${deleteStatus}`);
            }
        }
    }
    print(`执行结束[zipMonitorIlegalInfoFiles], 共耗时: ${(Date.now() - startTime) / 1000} s`);
}

/**
 * 创建线索对应的-网络交易监测违法信息登记表word文件, 并放在对应凭证等文件夹下
 * @param importSabaCodes 线索号（文件夹父路径名），eg："2022012420057934, 2022020720016376"
 * @param importPks 线索编号（处理附件文档），eg："0-220118-wx-80337-1, 0-220118-wx-80403-1"
 * @return 
 * <p>
 *  1 创建的word文件存储到线索对应的凭证文件所存放目录下, 方便和凭证等文件一起压缩导出;
 *  2 创建和导出分开, 用于给前端新增一个【等待样式】。
 * </p> 
 */
export function createMonitorIllegalWordFile(request: HttpServletRequest, response: HttpServletResponse, params: {
    importSabaCodes: string,
    importPks: string
}): ResultInfo {
    print(`开始执行[createMonitorIllegalWordFile], 创建线索对应的-网络交易监测违法信息登记表word文件, 并放在对应凭证等文件夹下`);
    print(params);

    if (!params.importSabaCodes) {
        print(`将指定的文件夹打包（ZIP），并导出：参数错误，params：${params}`);
        return { result: false, message: "参数不合法" };
    }
    let startTime: number = Date.now();
    let importSabaCodes: string[] = params.importSabaCodes.split(",");
    let importPks: string[] = [];
    if (!!params.importPks) {
        importPks = params.importPks.split(",");
    }
    let resultInfo: ResultInfo = { result: true };
    let importFileDirs: string[] = []; // 记录服务器所存 截图文件、存证文件、违法点截图文件 父路径
    try {
        for (let i = 0; i < importSabaCodes.length; i++) {
            let tempPath: string = `${importFileParentPath}${importSabaCodes[i]}/`;
            importFileDirs.push(tempPath);
            if (!!importPks[i]) {
                let tempWordFile = metadata.runDocx({
                    // path: http://58.215.18.230:8280/zhjg/ZHJG/app/home.app/jgrw/WLJYJG/网络交易监测违法信息登记表.d.docx?:viewSource=false&id=0-220505-wx-140861-1&:export=网络交易监管信息登记表.docx
                    path: importAttachWordPath,
                    params: {
                        ":viewSource": "false",
                        "id": importPks[i],
                        ":export": "网络交易监管信息登记表.docx"
                    }
                });
                fileUtils.copySignFileToDir(tempPath, tempWordFile, "网络交易监管信息登记表.docx"); // 将生成的元数据文件 - 放入当前线索的截图等文件目录下
            }
        }
    } catch (e) {
        resultInfo.result = false;
        resultInfo.message = "创建网络交易监测违法信息登记表word文件失败";
        for (let i = 0; i < importFileDirs.length; i++) { // 删除临时加入的附件word文件
            let tempFile = fs.getFile(`${importFileDirs[i]}网络交易监管信息登记表.docx`);
            if (tempFile.exists()) {
                let deleteStatus: boolean = tempFile.delete();
                print(`附件：${importFileDirs[i]}网络交易监管信息登记表.docx 删除: ${deleteStatus}`);
            }
        }
        print('执行失败, 创建线索对应的-网络交易监测违法信息登记表word文件, 并放在对应凭证等文件夹下');
        print(e);
    }
    print(`执行结束[createMonitorIllegalWordFile], 耗时: ${(Date.now() - startTime) / 1000} s`);
    return resultInfo;
}

/** 临时存放导出的word文件父路径 */
const tempSaveImportWordFileDir = `${workDir}/wljy_fjcc/tempSaveImportWordFile/`;
/**
 * 创建-网络交易监测违法信息登记表文件, 并存放到服务器指定临时文件夹
 * @see https://jira.succez.com/browse/CSTM-20971
 * @param taskId 线索店铺信息任务编号
 * @return 
 * <p>
 *  1 压缩的文件未创建, 创建耗时;
 *  2 仅导出-网络交易监测违法信息登记表文件, 创建后存放文件夹下不包括: 凭证等文件;
 *  3 考虑到创建文件和导出文件一起处理, 页面等待时间过长, 将其分开处理, 页面增加等待样式。
 * </p>
 */
export function createImportIllegalFiles(request: HttpServletRequest, response: HttpServletResponse, params: {
    taskId: string
}): ResultInfo {
    print(`开始执行[createImportIllegalFiles], 创建需要导出的文件-网络交易监测违法信息登记表文件`);
    print(params);

    let taskId: string = params.taskId;
    let storeInfos: string[][] = queryImportStoreInfos(taskId);
    print(`需要创建: ${storeInfos.length} 个导出文件`);

    let resultInfo: ResultInfo = { result: true };
    let startTime: number = Date.now();
    /** 当前导出操作生成一个临时目录文件夹, 存储导出的word信息, 支持多人操作不冲突 */
    let uuidDirName: string = utils.uuid();
    let tempSaveImportFileDir: string = `${tempSaveImportWordFileDir}${uuidDirName}/`;
    try {
        let saveDir = new java.io.File(tempSaveImportFileDir);
        if (!saveDir.exists()) {
            saveDir.mkdirs();    // 若文件父目录不存在，则创建文件以及父文件目录
        }
        for (let i = 0; i < storeInfos.length; i++) { // 根据线索编号, 循环生成word文档, 存放到指定目录下
            let storeInfo: string[] = storeInfos[i];
            /** 索引号 */
            let indexId: string = storeInfo[0];
            /** 线索编号 */
            let importPk: string = storeInfo[1];
            let fileName: string = `${indexId}_网络交易监测违法信息登记表.docx`;
            let tempWordFile = metadata.runDocx({
                path: importAttachWordPath,
                params: {
                    ":viewSource": "false",
                    "id": importPk,
                    ":export": fileName
                }
            });
            fileUtils.copySignFileToDir(tempSaveImportFileDir, tempWordFile, fileName);
            print(`开始创建: ${i + 1} 个文件`);
        }
        resultInfo.tempSaveImportFileDir = tempSaveImportFileDir;
    } catch (e) {
        resultInfo.result = false;
        resultInfo.message = "创建需要导出的文件-网络交易监测违法信息登记表文件失败";
        print(`创建需要导出的文件-网络交易监测违法信息登记表文件失败, 删除已经创建的导出文件----error`);
        fs.deleteFile(tempSaveImportFileDir); // 创建文件失败, 则删除当前临时存放导出的文件夹和文件
        print(e);
    }
    print(`执行结束[createImportIllegalFiles], 共耗时: ${(Date.now() - startTime) / 1000} s`);
    return resultInfo;
}

/**
 * 根据任务编码查询需要导出的线索店铺信息
 * @param taskId 任务编码
 * @return 
 */
function queryImportStoreInfos(taskId: string): string[][] {
    let query: QueryInfo = {
        sources: [{
            id: "model1",
            path: "/ZHJG/data/tables/FACT/JGRW/07WLJYJG/WF_ZHJG_WLJYJG_XSDPXX.tbl"
        }],
        fields: [{
            name: "SYH", exp: "model1.SYH"
        }, {
            name: "XSBH", exp: "model1.XSBH"
        }],
        filter: [{
            exp: `model1.RWBH = '${taskId}' and model1.CZZT != '6'`
        }]
    }
    let queryData = dw.queryData(query).data as string[][];
    return queryData;
}

/**
 * 压缩指定【文件夹-支持多个】成压缩包, 并导出
 * @author tuzw
 * @createDate 2022-10-26
 * @param tempSaveImportFileDir 导出的文件夹路径, eg: /zhjg/workdir/wljy_fjcc/tempSave1/test1/,/zhjg/workdir/wljy_fjcc/tempSave1/test1/
 * <p>
 *  1 通过URL传递参数, 需注意URL最大长度为: 2083字节, 超过则不会发起请求;
 *  2 临时文件夹被压缩后, 导出后, 导出的文件夹和文件会被删除掉;
 *  3 示例, 先创建导出文件, 再使用此方法压缩zip文件并导出 #link{createImportIllegalFiles}
 * </p>
 */
export function zipSignDirAndimport(request: HttpServletRequest, response: HttpServletResponse, params: {
    tempSaveImportFileDir: string
}): void {
    print(`开始执行[zipSignDirAndimport], 批量导出 网络交易监测违法信息登记表文件`);
    print(params);

    let startTime: number = Date.now();
    /** 导出文件夹路径 */
    let tempSaveImportFileDirs: string[] = [];
    if (!!params.tempSaveImportFileDir) {
        tempSaveImportFileDirs = params.tempSaveImportFileDir.split(",");
    }
    for (let i = 0; i < tempSaveImportFileDirs.length; i++) { // 判断导出文件夹是否都是临时文件夹（tempSave开头的文件夹名）
        let dirPath: string = tempSaveImportFileDirs[i];
        if (dirPath.indexOf(`${workDir}/wljy_fjcc/tempSave`) == -1) { // 因为导出后会删除导出路径下所有文件, 校验, 避免删除其他文件夹
            print(`-导出文件夹路径: [${dirPath}] 不在临时文件夹中, 必须存放在临时文件夹中, eg: [${tempSaveImportWordFileDir}]`);
            return;
        }
    }

    /** 压缩包文件名 */
    let zipName: string = utils.uuid();
    let zipSavePath: string = `${importZipParentPath}${zipName}.zip`;
    zipUtils.zipDirs(tempSaveImportFileDirs, zipSavePath, true);

    let zipFile = new java.io.File(zipSavePath);
    let fileBytes = Files_.readAllBytes(Paths_.get(zipFile.getAbsolutePath()));

    response.reset();
    let importZipName: string = java.net.URLEncoder.encode("网络交易监测违法信息登记表.zip", "utf-8");
    response.setHeader("Content-Disposition", "attachment; filename=" + importZipName);
    response.setContentType("application/octet-stream");

    response.setContentLength(fileBytes.length);
    let out: OutputStream = null;
    try {
        out = response.getOutputStream();
        out.write(fileBytes);
        out.flush();
    } catch (e) {
        print(`-批量导出 网络交易监测违法信息登记表文件失败-error`);
        print(e);
    } finally {
        out && out.close();
        let zipIsdelete: boolean = zipFile.delete(); // 将导出的内容写入response后，导出zip文件进行销毁(delete方法仅删除文件或者空目录)
        for (let i = 0; i < tempSaveImportFileDirs.length; i++) {
            let dirPath: string = tempSaveImportFileDirs[i];
            fs.deleteFile(dirPath); // 将当前临时生成所存导出文件的文件夹以及导出文件一起删除
            print(`-临时存放导出文件夹: [${dirPath}] 已被删除`);
        }
        print(`-导出临时生成的压缩包删除情况：${zipIsdelete}`);
    }
    print(`执行结束[zipSignDirAndimport], 共耗时: ${(Date.now() - startTime) / 1000} s`);
}

/**
 * 压缩案件线索登记处理表文档(批量)
 * @author tuzw
 * @createDate 2023-06-09
 * @see https://jira.succez.com/browse/CSTM-23120
 * <p>
 *  考虑到处理的文件过多, 在磁盘临时文件下创建一个压缩包文件, 将其文档放入其中, 实际导出由#link{importSignZipFile}
 * </p>
 * @param params.pollingListIds 线索UUID字段值
 * @return {
 *   result: true,
 *   zipPath: "压缩包路径",
 *   errorSabaIds: "xxx001,xxx002"
 * }
 */
export function zipRegistrationFrom(request: HttpServletRequest, response: HttpServletResponse, params: {
    pollingListIds: string[]
}): ResultInfo {
    print(`执行[zipRegistrationFrom], 压缩案件线索登记处理表文档`);
    // print(params);

    let pollingListIds = params.pollingListIds;
    if (!pollingListIds || pollingListIds.length == 0) {
        return { result: false, message: "没有要导出的登记表信息" };
    }
    let sabaIdInfos: string[][] = queryZipSabaInfos(pollingListIds);
    let resultInfo: ResultInfo = { result: true };
    let nowDateFolder: string = fileUtils.formatDate();
    let tempZipPath: string = `${sys.getWorkDir()}/temp/${nowDateFolder}/register`;
    let tempZipName: string = `登记表_${utils.uuid()}`; // 名称后缀增加uuid(), 保证文件名唯一
    let tempZipFilePath: string = `${tempZipPath}/${tempZipName}.zip`;
    print(`当前存放登记表压缩包路径: [${tempZipFilePath}]`);

    let errorSabaIds: string[] = [];
    let zipFile: JavaFile = fileUtils.createZipFile(tempZipFilePath);
    let fileInputStream: FileInputStream = null;
    let zipOutputStream: ZipOutputStream = null;
    try {
        zipOutputStream = new ZipOutputStream(new FileOutputStream(zipFile));
        for (let i = 0; i < sabaIdInfos.length; i++) {
            let sabaIdInfo: string[] = sabaIdInfos[i];
            let sabaId: string = sabaIdInfo[0];
            let sabaUuid: string = sabaIdInfo[1];
            if (!sabaUuid) {
                continue;
            }
            let wordFielName: string = `${sabaId}案件线索登记（处置）表.docx`;
            let tempWordFile;
            try {
                tempWordFile = metadata.runDocx({
                    path: "/ZHJG/app/home.app/jgrw/ZFBA/案件线索登记（处置）表.d.docx",
                    cache: false,
                    params: {
                        ":viewSource": "false",
                        "id": sabaUuid,
                        ":export": wordFielName
                    }
                });
            } catch (e) {
                print(`压缩线索[${sabaId}]文档失败[${e.toString()}]`);
                resultInfo.message = `部分线索编号失败[${e.toString()}]`;
                errorSabaIds.push(sabaId);
                continue;
            }
            fileInputStream = new FileInputStream(tempWordFile);
            let ztNoteFileEntry = new ZipEntry(wordFielName);
            zipOutputStream.putNextEntry(ztNoteFileEntry);
            IOUtils.copy(fileInputStream, zipOutputStream);
            zipOutputStream.closeEntry();
            fileInputStream.close();
            fileInputStream = null; // 重新更改指针指向
        }
        resultInfo.zipPath = tempZipFilePath; // 记录压缩包路径
        resultInfo.errorSabaIds = errorSabaIds.join(","); // 记录失败的线索编号
    } catch (e) {
        resultInfo.result = false;
        resultInfo.message = `压缩登记表文件失败[${e.toString()}]`;
        print(`压缩登记表文件执行失败[${e.toString()}], 删除临时压缩包文件: ${fs.deleteFile(tempZipFilePath)}`);
        print(e);
    } finally {
        fileInputStream && fileInputStream.close();
        zipOutputStream && zipOutputStream.close();
    }
    return resultInfo;
}


/**
 * 获取导出的线索信息
 * @param pollingListIds 线索UUID值
 * @return [
 *   ["线索编号", "线索UUID值"]
 * ]
 */
function queryZipSabaInfos(pollingListIds: string[]): string[][] {
    let querySabaIds: string[] = [];
    for (let i = 0; i < pollingListIds.length; i++) { // 避免为集合类型, 无法进行in查询, 这里转换一下
        querySabaIds.push(pollingListIds[i]);
    }
    let queryInfo: QueryInfo = {
        sources: [{
            id: "model1",
            path: "/ZHJG/data/tables/FACT/JGRW/08ZFBA/WF_ZHJG_ZFBA_WSSCNR.tbl"
        }],
        fields: [{
            name: "XSBH", exp: "model1.XSBH"
        }, {
            name: "XSUUID", exp: "model1.XSUUID"
        }],
        filter: [{
            exp: `model1.XSUUID in ('${querySabaIds.join("','")}')`
        }]
    }
    let queryData = dw.queryData(queryInfo).data as string[][];
    return queryData;
}

/**
 * 导出指定路径压缩包文件, 导出后可选择是否删除压缩包
 * @param params.zipFilePath 压缩包路径, eg: /opt/workdir/clusters-share/file-storage/relativePath/XML文档_20221230135637.zip
 * @param params.isDeleteZip 是否导出后删除zip文件, 默认: 不删除
 * @param params.zipFileName 导出压缩包名称, 默认: 导出压缩包名
 */
export function importSignZipFile(request: HttpServletRequest, response: HttpServletResponse, params: {
    zipFilePath: string,
    isDeleteZip?: boolean,
    zipFileName?: string
}): void {
    print(`开始执行[importSignZipFile], 导出指定路径压缩包文件, 导出后删除压缩包`);
    print(params);

    let zipFilePath: string = params.zipFilePath;
    if (!zipFilePath) {
        return;
    }
    let isDeleteZip: boolean = params.isDeleteZip != undefined ? params.isDeleteZip : false;
    let tempZipName: string = params.zipFileName;
    if (!tempZipName) {
        tempZipName = zipFilePath.substring(zipFilePath.lastIndexOf("/") + 1);
    }
    if (tempZipName.lastIndexOf(".zip") == -1) {
        tempZipName = `${tempZipName}.zip`;
    }
    let outPut = response.getOutputStream();
    let zipFile: JavaFile = new JavaFile(zipFilePath);
    try {
        let fileBytes = Files_.readAllBytes(Paths_.get(zipFile.getAbsolutePath()));
        response.setContentType("application/zip");
        response.addHeader("Content-Disposition", `attachment; filename=${java.net.URLEncoder.encode(tempZipName, "UTF-8")}`);
        response.addHeader("Content-transfer-Encoding", "binary");
        outPut.write(fileBytes);
        print(`${zipFilePath} 压缩包导出成功`);
    } catch (e) {
        print(`[importSignZipFile], 导出指定路径压缩包文件失败`);
        print(e);
    } finally {
        outPut && outPut.close();
        if (isDeleteZip) {
            print(`删除压缩包文件, 删除状态: ${zipFile.delete()}`);
        }
    }
}

/** 
 * 文件压缩工具类
 * @author tuzw
 * @createDate 2022-02-09
 */
class ZipUtils {

    /**
     * 功能：压缩多个文件（可是ZIP文件）成一个zip文件
     * @param files       压缩的文件绝对路径集，eg：["/zhjg/workdir/wljy_fjcc/", "/zhjg/workdir/wljy_fjcc/t.txt"]
     * @param zipFilePath 压缩后的文件路径（绝对路径），eg：.../tempSaveZipFile/0-220119-wx-80908-1.zip
     * eg：
     *  let files: string[] = ["/zhjg/workdir/0-220119-wx-80908-1/1.png", "/zhjg/workdir/0-220119-wx-80908-1/2.pdf"];
        let zipFilePath: string = `${workDir}/2022-01-04/tempSaveZipFile/0-220119-wx-80908-1.zip`;
        zipFiles(files, zipFilePath);
     */
    public zipFiles(files: string[], zipFilePath: string): void {
        print("zipFiles()，开始压缩导出文件");
        if (files == null || files.length == 0 || !zipFilePath) {
            return;
        }
        let zipFile = new java.io.File(zipFilePath);
        if (!zipFile.exists()) {
            print("压缩文件不存在，已自动创建");
            zipFile.createNewFile();
        }
        let out: ZipOutputStream = null;
        try {
            out = new ZipOutputStream(new FileOutputStream(zipFile));
            for (let i = 0; i < files.length; i++) { // 
                let sendFile = new JavaFile(files[i]);
                if (!sendFile.exists()) {
                    print(`文件【${files[i]}】不存在服务器上`);
                    continue;
                }
                let fileBytes = Files_.readAllBytes(Paths_.get(sendFile.getAbsolutePath())); // 通过绝对路径获取文件字节流
                out.putNextEntry(new ZipEntry(sendFile.getName()));
                out.write(fileBytes);
            }
        } catch (e) {
            print(`zipFiles()，开始压缩导出文件--error`);
            print(e);
        } finally {
            if (out != null) {
                out.close();
            }
        }
    }

    /**
     * 压缩多个文件夹 或 文件
     * @param dirs           要压缩的文件夹 或 文件，eg：[]
     * @param zipFile        压缩后的文件路径（绝对路径），eg：.../tempSaveZipFile/0-220119-wx-80908-1.zip
     * @param includeBaseDir 是否将内部文件夹一起压缩
     */
    public zipDirs(dirs: string[], zipFilePath: string, includeBaseDir: boolean): void {
        print("zipDirs()，压缩多个文件夹开始");
        if (dirs == null || dirs.length == 0 || zipFilePath == null || zipFilePath == "") {
            return;
        }
        let zipOut: ZipOutputStream = null;
        let zipFile = new java.io.File(zipFilePath);
        if (!zipFile.exists()) {
            let parentFile = zipFile.getParentFile();
            if (!parentFile.exists()) {
                parentFile.mkdirs();    // 若文件父目录不存在，则创建文件以及父文件目录
            }
            zipFile.createNewFile(); // 创建真实的文件
        }
        try {
            /**
             * 导出多个文件夹
             * 思路：压缩后直接将其文件流写入：response
             * zipOut = new ZipOutputStream(new FileOutputStream(response.getOutputStream()));
             * 
             * 参考网址：https://juejin.cn/post/6994374601329344548#heading-5
             */
            zipOut = new ZipOutputStream(new FileOutputStream(zipFile));
            for (let i = 0; i < dirs.length; i++) {
                print(`当前压缩的文件为：${dirs[i]}`);
                let fileDir = new JavaFile(dirs[i]);
                let baseDir: string = ""; // 最外层目录
                if (includeBaseDir) {
                    baseDir = fileDir.getName();
                }
                this.compress(zipOut, fileDir, baseDir);
            }
        } catch (e) {
            print("压缩多个文件夹错误")
            print(e);
        } finally {
            if (zipOut != null) {
                zipOut.close();
            }
        }
        print("zipDirs()，压缩多个文件夹结束");
    }

    /**
     * 采用递归的方式，将文件夹下的文件放入到指定的压缩包中
     * @param zipOut    压缩对象
     * @param folderOrFile   文件夹 或 文件对象, 类型: JavaFile
     * @param baseDir   文件夹 或 文件名
     */
    private compress(zipOut: ZipOutputStream, folderOrFile, baseDir: string): void {
        if (!folderOrFile.exists()) {
            print(`当前压缩的目录不存在，${folderOrFile.getAbsolutePath()}`);
            return;
        }
        if (folderOrFile.isDirectory()) {
            baseDir = baseDir.length == 0
                ? ""
                : baseDir + JavaFile.separator;
            let childFiles: JavaFile = folderOrFile.listFiles();
            if (ArrayUtils.isEmpty(childFiles)) {
                return;
            }
            for (let i = 0; i < childFiles.length; i++) {
                this.compress(zipOut, childFiles[i], baseDir + childFiles[i].getName());
            }
        } else {
            zipOut.putNextEntry(new ZipEntry(baseDir));
            let fileIn: FileInputStream = null;
            try {
                fileIn = new FileInputStream(folderOrFile);
                IOUtils.copy(fileIn, zipOut);
            } catch (e) {
                print("文件夹下的文件放入到指定的压缩包中失败");
                print(e);
            } finally {
                if (fileIn != null) {
                    fileIn.close();
                }
            }
        }
    }
}
const zipUtils = new ZipUtils();

/**
 * 文件工具类
 * @author tuzw
 * @createDate 2022-02-09
 */
class FileUtils {

    /**
    * 复制一个已存在的文件到指定目录
    * @param dirPath 存放复制后的目录路径，eg：/tempSaveZipFile/
    * @param copyFileOrFile 复制的文件路径 或者 文件对象，eg：/tempSaveZipFile/t.doc
    * @param newFileName 复制后的文件名.类型，eg：copy_t.doc
    * @return true | false
    */
    public copySignFileToDir(
        dirPath: string,
        copyFileOrFile: string | JavaFile,
        newFileName: string
    ): ResultInfo {
        if (!copyFileOrFile || !copyFileOrFile) {
            return { result: false, message: "复制文件操作, 参数不合法" };
        }
        if (typeof copyFileOrFile == 'string') {
            copyFileOrFile = new java.io.File(copyFileOrFile);
        }
        if (!copyFileOrFile.exists()) {
            return { result: false, message: "复制的文件不存在" };
        }
        if (!copyFileOrFile.isFile()) {
            return { result: false, message: "复制的文件不是文件" };
        }
        let saveFiledir = new java.io.File(dirPath);
        if (!saveFiledir.exists()) {
            return { result: false, message: "复制后的文件所存放目录不存在" };
        }
        if (!saveFiledir.isDirectory()) {
            return { result: false, message: `dirPath参数值不是一个目录` };
        }
        if (!newFileName) {
            newFileName = copyFileOrFile.getName(); // eg：test.txt
        }
        if (`${copyFileOrFile.getParent()}/` == dirPath) { // 若同处于一个目录下，则增加前缀：copy_
            newFileName = `copy_${newFileName}`;
        }
        let newFile = new java.io.File(`${dirPath}${newFileName}`);
        if (!newFile.exists()) {
            newFile.createNewFile();
        }
        let resultInfo: ResultInfo = { result: true };
        let fileOut: FileOutputStream = null;
        let fileIn: FileInputStream = null;
        try {
            fileOut = new FileOutputStream(newFile);
            fileIn = new FileInputStream(copyFileOrFile);
            IOUtils.copy(fileIn, fileOut);
        } catch (e) {
            resultInfo.result = false;
            resultInfo.message = "复制一个已存在的文件到指定目录失败";
            print(e);
        } finally {
            if (fileOut != null) {
                fileOut.close();
            }
            if (fileIn != null) {
                fileIn.close();
            }
        }
        return resultInfo;
    }

    /**
     * 读取url文件二进制流，创建指定类型文件
     * @param fileName 绝对路径，eg：/tempSaveZipFile/t.doc
     * @param fileType 文件类型，eg：txt、doc
     * @return true | false
     */
    public createFile(filePath: string, fileType: string, fileIoByte: number[]): boolean {
        if (!filePath || !fileType) {
            return false;
        }
        let isSuccess: boolean = true;
        let file = new java.io.File(filePath);
        if (!file.exists()) {
            let parentFile = file.getParentFile();
            if (!parentFile.exists()) {
                parentFile.mkdirs();    // 若文件父目录不存在，则创建文件以及父文件目录
            }
            file.createNewFile(); // 创建真实的文件
        }
        file = fs.getFile(filePath); // 读取文件
        let outStream = file.openOutputStream();
        try {
            outStream.write(fileIoByte as number[]);
        } catch (e) {
            file.delete();
            isSuccess = false;
            print(`--写入文件${filePath}内容失败---`);
            print(e);
        } finally {
            outStream.close();
        }
        return isSuccess;
    }

    /**
     * 读取【下载链接】文件内容 和 文件名.类型
     * @param url 文件下载链接
     * @return eg: { fileIo: [文件二进制流数组], "test.png" }
     */
    public getFileIo(url: string): { result: boolean, fileIo?: ArrayLike<number> | undefined | string, fileName?: string } {
        print("读取【下载链接】文件内容 和 文件名.类型");

        let resultInfo = { result: false, fileIo: [], fileName: "" };
        let client = HttpClients.createDefault();
        let response;
        try {
            let httpGet = new HttpGet(url);
            response = client.execute(httpGet);
            /**
             * 20220927 tuzw
             * 获取文件名，不可使用getValuegetValue来获取，改为直接使用toString()方法将其对象转换为字符串
             * let fileName = response.getFirstHeader("Content-Disposition").getValuegetValue().split(';')[1].split('=')[1];
             * class org.apache.http.message.BufferedHeader ——> Content-Disposition: inline; filename="926/0-220925-wx-207109-1.jpg"
             */
            let contentDisposition = response.getFirstHeader("Content-Disposition").toString();
            let fileName = contentDisposition.split(';')[1].split('=')[1];

            let entity = response.getEntity();
            let contentType = response.getFirstHeader("Content-type");
            let fileType: string = "";
            if (!!contentType) { // 考虑到zip文件，不包含：Content-type，此处需要进行空校验，若不包含，则直接根据文件名来匹配文件类型
                fileType = contentType.getValue();
            } else {
                fileType = fileName; // 通过indexOf来匹配文件名所包含文件类型，故此处直接将文件名赋值给文件类型
            }
            if (fileType.indexOf("image") !== -1 || fileType.indexOf("pdf") !== -1 ||
                fileType.indexOf("jpg") !== -1 || fileType.indexOf("png") !== -1 || fileType.indexOf("mp4") !== -1
                || fileType.indexOf("jpeg") !== -1 || fileType.indexOf("zip") !== -1 || fileType.indexOf("bmp") !== -1) {
                /**
                 * 20220208 tuzw 
                 * 对方文件夹含有大量的中文名，直接获取是一串乱码，需要将其转换为utf-8
                 */
                let encodeFileName = new java.lang.String(fileName.getBytes("ISO-8859-1"), "utf-8");
                resultInfo = {
                    result: true,
                    fileIo: EntityUtils.toByteArray(entity),
                    fileName: encodeFileName.replace(/\"/g, "") // 去除字符串左右‘双引号’
                }
            } else {
                let content = EntityUtils.toString(entity);
                if (content.indexOf("认证失败") !== -1) {
                    print("认证失败");
                    print(content);
                }
            }
        }
        finally {
            response && response.close();
            client && client.close();
        }
        return resultInfo;
    }

    /**
     * 删除指定文件 或者 空文件夹
     * @description delete方法仅删除文件或者空目录
     * @description 删除文件夹以及内部所有子文件夹, fs.deleteFile("") 会将当前文件夹也一起删除
     */
    public deleteSignFileOrDir(deleteFileOrDirPath: string): ResultInfo {
        let deleteFile = new java.io.File(deleteFileOrDirPath);
        if (!deleteFile.exists()) {
            return { result: true, message: "文件不存在" };
        }
        if (deleteFile.isFile()) {
            return deleteFile.delete();
        }
        if (deleteFile.isDirectory()) {
            let listChildrenFiles = deleteFile.listFiles();
            if (listChildrenFiles.length != 0) {
                return { result: false, message: "文件夹内存在子文件, 无法删除" };
            }
            return deleteFile.delete();
        }
        return { result: true };
    }

    /**
     * 显示指定目录下的子文件和文件夹情况
     * @param dirPath 目录路径，eg：/tempSaveZipFile/
     */
    public showFolderOrFile(dirPath: string): ResultInfo {
        if (!dirPath) {
            return { result: false, message: "显示目录为空" };
        }
        let dir = new java.io.File(dirPath);
        if (!dir.isDirectory()) {
            return { result: false, message: "传入的路径不是一个目录" };
        }
        print("当前目录下的子文件和文件夹情况：");
        let childFolders: JavaFile[] = dir.listFiles();
        if (childFolders.length > 0) {
            for (let i = 0; i < childFolders.length; i++) {
                print(childFolders[i].getAbsolutePath());
            }
        }
        return { result: true };
    }

    /**
     * 格式化时间戳
     * @parmas dates 需要格式化的时间, 默认当前时间
     * @return 格式化后的日期，2021-12-12
     */
    public formatDate(dates?: Date): string {
        if (!dates) {
            dates = new Date();
        }
        let Year: number = dates.getFullYear();
        let Months = (dates.getMonth() + 1) < 10
            ? '0' + (dates.getMonth() + 1)
            : (dates.getMonth() + 1);
        let Day = dates.getDate() < 10
            ? '0' + dates.getDate()
            : dates.getDate();
        return Year + '-' + Months + '-' + Day;
    }

    /**
     * 创建压缩zip文件
     * @param filePath 文件路径, eg: .../file-storage/relativePath/noteFileZip.zip
     * @return 
     */
    public createZipFile(filePath: string): JavaFile {
        let zipFile = new JavaFile(filePath);
        let parentFile: JavaFile = zipFile.getParentFile();
        if (!parentFile.exists()) {
            parentFile.mkdirs(); // 递归创建所有不存在的父级目录
        }
        if (!zipFile.exists()) {
            zipFile.createNewFile();
        }
        return zipFile;
    }
}
const fileUtils = new FileUtils();

/** 返回信息 */
interface ResultInfo {
    /** 返回结果 */
    result: boolean;
    /** 返回消息 */
    message?: string;
    /** 错误数据 */
    errorData?: any;
    /** 成功数据 */
    successData?: any;
    [propname: string]: any;
}