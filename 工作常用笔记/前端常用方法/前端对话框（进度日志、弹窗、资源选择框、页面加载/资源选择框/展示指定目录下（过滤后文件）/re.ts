


/**
 * @description 交换资源页面相关脚本
 */
export class Spg_Data_Exchange_Res {
	public CustomActions: any;
	constructor() {
		this.CustomActions = {
			button_ex_openResourceDialog: this.button_ex_openResourceDialog.bind(this)
		}
	}
	
	/**
	 * 打开数据交换资源对话框
	 * @param rootPath 资源选择器根目录
	 * @param valueBindCompId 选择资源ID值所存控件ID
	 * @param descBindCompId? 选择资源的描述所存控件ID
	 * @param multiple? 是否允许多选，默认：false
	 *
	 * 问题：showResourceDialog内部的cancel不能返回promise，导致重新打开交互时，内部会判断交互正处于运行状态，从而无法触发交互
	 * 解决办法：取消返回值
	 */
	private button_ex_openResourceDialog(event: InterActionEvent): void {
		let params = event.params;
		let page = event.page;
		let rootPath: string = params?.rootPath;
		if (isEmpty(rootPath)) {
			rootPath = '/sdi/data/tables';
		}
		let multiple: boolean = params?.multiple;
		if (isEmpty(multiple)) {
			multiple = false;
		}
		let valueBindCompId: string = params?.valueBindCompId;
		let descBindCompId: string = params?.descBindCompId;
		let valueBindComp = page.getComponent(valueBindCompId);
		let sourceDialogValue: string[] = valueBindComp.getValue()?.split(",");

		rc({
			url: "/zt/dataExchange/getCanShowExchangeFileIds"
		}).then((result) => {
			if (!result.result) {
				showWarningMessage(result.message);
				return;
			}
			let showFileIds: string[] = result.showFileIds;
			let checkIsShowFileFunc = (file: MetaFileInfo): boolean => { // 在指定根目录下的文件都会进入此方法进行校验
				let fileId: string = file.id;
				if (showFileIds.includes(fileId)) {
					return true;
				}
				return false;
			}
			let dp = new ResourceTreeDataProvider({ filterFiles: checkIsShowFileFunc });
			showResourceDialog({
				id: "dataExchage-sourceDialog",
				rootPath: rootPath,
				caption: "数据交换资源选择",
				returnTypes: ["tbl"],
				checkBoxVisible: true,
				multiple: multiple,
				multipleCheckable: true,
				dataprovider: dp,
				value: sourceDialogValue,
				enableEmptySelect: true,
				onok: function(event1: SZEvent, component?: Component, items?: any) {
					let choseFileDesc: string[] = [];
					let choseFileIds: string[] = [];
					for (let i = 0; i < items.length; i++) {
						choseFileIds.push(items[i].id);
						choseFileDesc.push(items[i].desc);
					}
					valueBindComp.setValue(choseFileIds.join(","));
					if (!isEmpty(descBindCompId)) {
						let descBindComp = page.getComponent(descBindCompId);
						descBindComp.setValue(choseFileDesc.join(","));
					}
				}
			});
		});
	}
}