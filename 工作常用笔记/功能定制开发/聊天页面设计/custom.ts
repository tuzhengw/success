import "css!./custom.css";
import { message, wait, formatDate, Component, rc, decodeBase64, ctxIf, encodeBase64, showInfoDialog, ComponentArgs, showWaiting, SZEvent, deepAssign, waitRender } from "sys/sys";
import { IMetaFileViewer, showDrillPage, renderMetaFile } from "metadata/metadata";
import { getDwTableDataManager, getLabels, getQueryManager } from "dw/dwapi";
import { SuperPageData } from "app/superpage/superpagedatamgr";
import { TouchEasy, TouchState } from "commons/mobile/basic";
//20211110 增加鹰眼功能
import { IAppCustomJS, IMetaFileCustomJS, InterActionEvent } from "metadata/metadata-script-api";
import { SuperPage } from 'app/superpage/superpage';
import { GisMap_Amap } from 'gismap/gismap';
import { HomeTemplatePage } from 'app/templatepage';
import { getCurrentUser, MetaFileViewer } from 'metadata/metadata';
import { Button, showUploadDialog } from 'commons/basic';
import { AnaNodeData } from "ana/datamgr";
import { sleep } from "svr-api/sys";


export const CustomJS: { [type: string]: IMetaFileCustomJS; } = {
	'/QYFWPT/app/pc.app/hqzc/zczx/zczx.spg': { // 政策咨询
		onRender: (event: InterActionEvent): Promise<void> => {
			customMessagelist(event);
			return Promise.resolve();
		}
	}
}


/**
 * 刷新消息数据
 * @param mlist 定制对话框对象
 * @param delay 延迟的毫秒数
 * @returns
 */
function refreshMessageData(mlist, delay) {
	let lastId = null;
	let lastTime = null;
	if (!mlist || !mlist.isDisplay()) {
		return delay ? wait(delay) : Promise.resolve();
	}
	if (mlist && mlist.items && mlist.items.length) {
		const domItems = mlist.domContent.querySelectorAll('.mlistitem-base');
		const last = domItems[domItems.length - 1].szobject;
		const lastData = last && last.data;
		lastId = lastData && lastData.id;
		lastTime = lastData && lastData.time;
	}
	if (!lastTime) {
		lastTime = formatDate(mlist.createTime, 'yyyy-MM-dd HH:mm:ss');
	}
	return rc({
		url: '/QYFWPT/app/pc.app/hqzc/intelligence-server.action?method=getNewMessage',
		data: {
			chatUser: mlist.chatUser,
			currentUser: mlist.currentUser,
			lastTime
		}
	}).then((data) => {
		const rs = data.filter(d => d.id != lastId);
		let lastRefreshData = mlist.lastRefreshData || [];
		// if (!!rs && rs.length != 0 && JSON.stringify(lastRefreshData) !== JSON.stringify(rs) && mlist.isExecuteDisplayData) {
		if (!!rs && rs.length != 0 && JSON.stringify(lastRefreshData) !== JSON.stringify(rs)) {
			mlist.hideNoDataPlaceholder();
			let mlistData = mlist.data.filter(d => {
				return d.id == rs[0].id;
			});
			let newSendMessage = []; // 当前新发送的消息
			/**
			 * 20220113 tuzw
			 * rs：包含历史已经发送过的数据，由于没有设置【是否已读】来过滤数据，这里脚本过滤
			 * 循环去掉【历史数据】已经在页面显示的数据
			 * 注意：后端返回的caption属性要跟前端一直，区别：中英文
			 */
			for (let rsIndex = 0; rsIndex < rs.length; rsIndex++) {
				let isExists: boolean = false;
				for (let dataIndex = 0; dataIndex < mlist.data.length; dataIndex++) {
					if (rs[rsIndex]["id"] == mlist.data[dataIndex]["id"]) {
						isExists = true;
						break;
					}
				}
				if (!isExists) {
					newSendMessage.push(rs[rsIndex]);
				}
			}
			if (newSendMessage.length > 0 && mlistData.length == 0) {
				mlist.addData(newSendMessage);
				mlist.data.pushAll(newSendMessage);
			}
			// if (rs.length > 0 && mlistData.length == 0) {
			// 	mlist.addData(rs); // 发送当前输入的消息
			// 	mlist.data.pushAll(rs); // 将发送过的消息保存
			// }
		}
		lastRefreshData = [];
		lastRefreshData.pushAll(rs);
		mlist.lastRefreshData = lastRefreshData;
		return delay ? wait(delay) : Promise.resolve();
	})
};

/**
 * 时时获取当前最新的聊天消息
 * @param mlist 定制对话框对象
 */
function autoRefreshMessageData(mlist) {
	refreshMessageData(mlist, 1000).then(() => {
		autoRefreshMessageData(mlist);
	});
}

/**
 * 定制对话框样式的聊天组件
 * 1. 【聊天头像】来自产品内置：
 * （1）通过用户的id值来访问（'/api/co/avatar/用户id值'）
 * （2）点击用户个人中心设置
 * （3）AI账户名：AI
 * 2. SPG导入的【消息】模型得加【过滤条件】，保证显示的是当前用户发的消息
 * 3. 【发送消息】按钮---交互不能有---刷新页面的交互，eg：插入、刷新等
 */
function customMessagelist(event) {
	let page = event.renderer;
	let list = page.getComponent('mobileList1');
	if (!list || list.setCstm) {
		return;
	};
	list.setCstm = true;
	let chatUser: string = "AI";
	let currentUser = getCurrentUser().userInfo.userId;
	/**
	 * 20220114 
	 * 由于是AI自动回复，不存在两个实际的用户相互发送，将其值写死，不用动态校验
	 	let params = event.page.getParameterNames();
		params.map(p => {
			if (p.name == "zjid") {
				chatUser = p.value;
			}
			if (p.name == "zjzh") {
				currentUser = p.value;
			}
		})
	 */
	let createTime = new Date();
	let builder = page && page.builder;
	let compBuilder = builder.getComponent(event.component.getId());
	let fileInfo = builder.getFileInfo();
	let spgid = fileInfo.name.toLowerCase().replace('.', '-');

	if (!list.old_getMListArgs) {
		list.old_getMListArgs = list.getMListArgs;
		list.getMListArgs = function () {
			let rs = list.old_getMListArgs();
			rs.pullToRefreshEnabled = false;
			rs.scrollLoadable = true;
			rs.onfetchdata = function (obj, args) {
				return obj.component.hasMore == null ? obj.data.reverse() : obj.data;
			};
			return rs;
		};
	}

	let refresh = function (incData): Promise<void> {
		!incData && this.reset();
		if (!this.dataProvider) {
			this.domPullupInfo && (this.domPullupInfo.textContent = '');
			return Promise.resolve();
		}
		if (this._promiseRefresh) return this._promiseRefresh;
		let fetchCount = this.incLoadRowCount;
		!this.startIndex && (fetchCount = this.initLoadRowCount);
		let startIndex = this.startIndex * fetchCount;
		let _showPullupInfo = () => {
			if (this.domPullupInfo) {
				wait(500).then(() => {
					if (this.isDisposed()) {
						return;
					}
					this.domPullupInfo.textContent = this.hasMore ? '' : this.nomoreDataPlaceholder
				});
			}
		};
		return this._promiseRefresh = Promise.resolve(this.dataProvider.fetchData({
			fetchCount,
			startIndex
		})).then((ds: any[]) => {
			delete this._promiseRefresh;
			if (this.isDisposed()) return;
			let datas = this.onfetchdata ? this.onfetchdata({
				component: this,
				data: ds
			} as SZEvent, this, ds) : ds;
			let eq = this.data === datas;
			this.hasMore = !eq && datas.length > 0;
			if (eq) return _showPullupInfo();
			this.data = datas;
			let _datas = [];
			if (datas.length > this.initPageRowCount) {
				_datas = datas.slice(0, this.initPageRowCount);
				this.cachePageRowCount = _datas.length;
			} else {
				_datas = datas;
			}
			if (!this.startIndex || !incData) {
				this.startIndex = 0;
				this.setData(_datas);
				datas.length ? this.hideNoDataPlaceholder() : this.showNoDataPlaceholder();
				wait(500).then(() => {
					this.domContent.scrollTo({ top: this.domContent.scrollHeight, behavior: 'smooth' });
				});
			} else {
				_showPullupInfo();
				this.appendData(_datas);
			}
			this.hasMore && (this.startIndex += 1);

			this.hideAllPanButtons();
			if (this.getDataReadyState() === 'nosearchresult') {
				this.showSearchInfo(message('commons.mobile.mlist.nosearchresult'));
			} else {
				this.hideSearchInfo();
			}
		}).catch(e => {
			delete this._promiseRefresh;
			throw e;
		});
	};

	let requestLoadMore = function () {
		this.scrollTimer && clearTimeout(this.scrollTimer);
		this.scrollTimer = setTimeout(() => {
			let domContent = this.domContent;
			let loadMore = domContent.scrollTop == 0;

			let domPullupInfo = this.domPullupInfo;
			if (!domPullupInfo) {
				domPullupInfo = this.domPullupInfo = document.createElement('div');
				domPullupInfo.classList.add('mlist-pullup-info');
				this.domContent.insertBefore(domPullupInfo, this.domContent.querySelector('.mlistitem-base'));
			}

			if (loadMore && this.hasMore !== false) {
				this.domPullupInfo.innerHTML = `<span class="mlist-pullup-loading"></span>${message('commons.mobile.mlist.loading')}`;
				this.isRefreshing = true;
				this.refresh(true).then(() => {
					this.isRefreshing = false;
				}).catch((e) => {
					this.isRefreshing = false;
					throw e;
				});
			}
		}, 300);
	};

	let appendData = function (datas) {
		if (!datas || !datas.length) return;
		const before = this.domContent.scrollHeight;
		this.domContent.style.overflowY = 'hidden';
		datas.forEach(data => {
			let item = this.createItem(data);
			if (this.items.length) {
				this.domContent.insertBefore(item.domBase, this.domContent.querySelector('.mlistitem-base'));
			} else {
				this.domContent.appendChild(item.domBase);
			}
			this.items.push(item);
		});
		wait(200).then(() => {
			this.domContent.scrollTo(0, this.domContent.scrollHeight - before);
			wait(100).then(() => { this.domContent.style.overflowY = 'auto'; });
		});
	};

	/**
	 * 发送一条信息
	 */
	let addData = function (datas) {
		if (!datas || !datas.length) {
			return;
		};
		getCurrentUser().getUserInfo().then(userInfo => {
			datas.forEach(data => {
				let item = this.createItem(typeof data == 'string'
					? {
						'model2.CONTENT': data, // model2：对应消息表
						attributeCaptions: [''],
						attributes: ['model2.CONTENT'],
						dataCount: '1',
						time: formatDate(new java.sql.Timestamp(new Date()), 'yyyy-MM-dd HH:mm:ss'),
						caption: userInfo.userName
					}
					: data);
				this.domContent.appendChild(item.domBase);
				this.items.push(item);
			});
			wait(50).then(() => {
				this.domContent.scrollTo({ top: this.domContent.scrollHeight, behavior: 'smooth' });
			});
		});
	};

	let checkMlistTimer;
	let checkMlist = function (callback) {
		checkMlistTimer && clearTimeout(checkMlistTimer);
		checkMlistTimer = setTimeout(() => {
			let mlist = list.getInnerComoponent();
			mlist.chatUser = chatUser;
			mlist.currentUser = currentUser;
			mlist.createTime = createTime;
			if (!mlist) {
				checkMlist(callback);
			} else {
				checkMlistTimer && clearTimeout(checkMlistTimer);
				callback && callback(mlist);
			}
		}, 10);
	};
	checkMlist((mlist) => {
		mlist.domBase.classList.add('custom-mlist-base', compBuilder.getId(), spgid);
		mlist.noDataPlaceholder = `&nbsp;`;
		if (!mlist.old_createItem) {
			mlist.old_createItem = mlist.createItem;
			mlist.createItem = function (data) {
				data.attributes = ['model2.CONTENT'];
				if (data.dataCount == 1) {
					data.icon = `/api/co/avatar/${currentUser}`; // 设置用户头像
				}
				if (data.dataCount == 0) {
					data.icon = `/api/co/avatar/AI`; // 设置AI智能头像
				}
				let rs = mlist.old_createItem(data);
				data.dataCount == 1 // 通过样式来设置是否：自己发的消息
					? rs.domBase.classList.add('current')
					: rs.domBase.classList.remove('current');
				/**
				 * 20220114 
				 * 将按钮加到内容区域内
					if (rs.domButtons && rs.domButtons.children) {
						for (let i = 0, n = rs.domButtons.children.length; i < n; i++) {
							rs.domAttrs.querySelector('.mlistitem-attr').appendChild(rs.domButtons.children[i]).onclick = (e) => {
								let domFileViewer = rs.domBase.closest('.metafileviewer-base');
								if (!domFileViewer) return;
								let fileViewer: MetaFileViewer = domFileViewer.szobject as MetaFileViewer;
								return fileViewer.showDrillPage({
									url: {
										path: `/sqb/app/csb.app/risk/fx_bg_new.spg`,
										params: {
											'rwid': data['model2.CONTENT']
										}
									},
									breadcrumb: false,
									target: ActionDisplayType.Blank,
								});
							};
						}
					}
				 */
				return rs;
			};
		}
		mlist.addData = function (datas) {
			addData.call(mlist, datas);
		};
		mlist.refresh = function (incData) {
			return refresh.call(mlist, incData);
		};
		mlist.appendData = function (data) {
			appendData.call(mlist, data);
		};
		mlist.requestLoadMore = function () {
			requestLoadMore.call(mlist);
		};
		autoRefreshMessageData(mlist);
	});
}
