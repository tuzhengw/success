/**显示pdf搜索框 */
button_std_file_showFindbar: (event: InterActionEvent) => {
	let builder = (<any>event.page).getBuilder();
	let local = event.page.getUrlParams().local;
	let comp = event.page.getComponent('document1');
	let embed = comp.getUIComponent().embed;
	let pdf_view = embed.currentView;
	pdf_view.showFindbar().then((findbar) => {
		findbar.keyword = local;
		findbar.findValueSearch.setValue(local);
		findbar.doFind();
	})
}