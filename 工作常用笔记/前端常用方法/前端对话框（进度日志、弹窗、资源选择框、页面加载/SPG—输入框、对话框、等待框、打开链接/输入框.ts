
import { InputDialog, showInputDialog, ShowInputDialogArgs } from 'commons/dialog';


showInputDialog({
	caption: "设置过滤条件",
	placeholder: "请求输入过滤条件",
	/**
	 * e
	 * dialog：当前输入框对象（InputDialog）
	 * btn：当前确认按钮
	 */
	onok: (e?: SZEvent, dialog?: Dialog, btn?: Button) => {
		dialog.getValue();
	}
} as ShowInputDialogArgs);
				
				


/**
 * 显示一个输入对话框
 * 注意：《默认的inputDialog》目前带的是ok和close按钮
 * @param args
 */
let inputDialog: InputDialog;
export function showInputDialog(args: ShowInputDialogArgs): Promise<Dialog> {
	//这里移动端和PC端共用了showInputDialog方法，所以只会有一个inputDialog，对实际应用不会有影响，但对移动端和pc端来回切换调试的时候会有影响。记录一下，暂不解决。
	if (!inputDialog) {
		inputDialog = new InputDialog(args);
	}

	inputDialog.onshow = (sze, dialog) => {
		//重置对话框
		args.caption && inputDialog.setCaption(args.caption);
		inputDialog.setDesc(args.desc);
		inputDialog.onok = args.onok;
		inputDialog.oncancel = args.oncancel;
		inputDialog.refreshInput(args);
		// 《默认的inputDialog》目前带的是ok和close按钮，因此onclose事件也需要更新
		inputDialog.onclose = args.onclose;
	}

	return inputDialog.show();

}

/**
 * {@link showInputDialogArgs}的构造参数。
 */
export interface ShowInputDialogArgs extends DialogArgs {

	/**
	 * 对话框的标题。默认取国际化字段`keyPrefix`。
	 */
	caption?: string;

	/**
	 * 输入框上方的描述文本。默认取国际化字段`keyPrefix + ".desc"`。
	 */
	desc?: string;

	/**
	 * 输入框的placeholder。默认取国际化字段`keyPrefix + ".placeholder"`。
	 */
	placeholder?: string;

	/**
	 * 显示输入对话框时输入框的默认值。
	 */
	value?: string;

	/**
	 * 确定按钮事件。
	 */
	onok?: DialogEventCallback;

	/**
	 * 取消按钮事件。
	 */
	oncancel?: DialogEventCallback;

}
