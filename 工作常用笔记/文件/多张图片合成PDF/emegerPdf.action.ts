import db from "svr-api/db"; //数据库相关API
import meta from "svr-api/metadata"; //元数据、模型表相关API
import utils from "svr-api/utils"; //通用工具函数
import bean from "svr-api/bean"; //spring bean相关api
import fs from "svr-api/fs";
import sys from "svr-api/sys";
const File = Java.type("java.io.File");
const FileInputStream = Java.type("java.io.FileInputStream")
const HashMap = Java.type("java.util.HashMap");
const Map = Java.type("java.util.Map");

const ByteArrayOutputStream = Java.type("org.apache.commons.io.output.ByteArrayOutputStream");
const Overlay = Java.type("org.apache.pdfbox.multipdf.Overlay");
const PDDocument = Java.type("org.apache.pdfbox.pdmodel.PDDocument");
const PDPage = Java.type("org.apache.pdfbox.pdmodel.PDPage");
const PDRectangle = Java.type("org.apache.pdfbox.pdmodel.common.PDRectangle");
const PDActionGoTo = Java.type("org.apache.pdfbox.pdmodel.interactive.action.PDActionGoTo");
const PDPageXYZDestination = Java.type("org.apache.pdfbox.pdmodel.interactive.documentnavigation.destination.PDPageXYZDestination");
const PDFMergerUtility = Java.type('org.apache.pdfbox.multipdf.PDFMergerUtility');
const IOUtils = Java.type("org.apache.commons.io.IOUtils");

/**
 * action开发规范：
 * 1. 前端可以通过url或ajax请求访问到action，action会根据情况执行内部的脚本函数
 * 2. 默认将执行`execute`函数，可以通过指定参数`method`指定执行的函数，所有前端能访问到的函数都必须导出（export）
 * 3. 每个函数接受2个参数`request: HttpServletRequest, response: HttpServletResponse`
 * 4. 函数可以执行重定向、forward、返回json、返回文本、下载文件、上传文件……
 */

/**
 * 地址：https://jira.succez.com/browse/SSSH-1
 * main方法主要用于：
 * 1. 定时调度脚本执行入口
 * 2. 脚本开发界面的执行入口，用于调试，或执行其他批处理业务
 */
export function main(args: string[]) {
    print(sys.getClusterShareDir())
    let fjs = [
        "workdir:08a057ab4e3829604506b3233cc22f455e00-【T3出行-25.26元-1个行程】高德打车电子发票.pdf",
        "workdir:08a037b7ab52f6e24e63a7290d8d3938c3a2-【及时用车-21.82元-1个行程】高德打车电子发票.pdf",
        "workdir:08a016d39407bab44a33b4b128adecbea6b0-【风韵出行-24.16元-1个行程】高德打车电子发票.pdf"
    ]
    //let filePaths = getMergePdf(null, null, { fjs });
    let info = parsePdf(getFilePath('default:89b995cfe37f458ba24b9a66a5415026'))
    print(info)
    //print(filePaths)
}

export function test(request: HttpServletRequest, response: HttpServletResponse, params: any) {
    let fjs = [
        'default:89b995cfe37f458ba24b9a66a5415026',
        "workdir:08a057ab4e3829604506b3233cc22f455e00-【T3出行-25.26元-1个行程】高德打车电子发票.pdf",
        "workdir:08a037b7ab52f6e24e63a7290d8d3938c3a2-【及时用车-21.82元-1个行程】高德打车电子发票.pdf",
        "workdir:08a016d39407bab44a33b4b128adecbea6b0-【风韵出行-24.16元-1个行程】高德打车电子发票.pdf"
    ]

    let mergePath= getMergePdf(null, null, { fjs }).data;
    response.setContentType('application/pdf');
    IOUtils.copy(new FileInputStream(new File(mergePath)), response.getOutputStream())
}

/**
 * 通过浏览器访问，当url中没有通过method指定执行的函数时，默认执行此函数。
 * 
 * @param request `HttpServletRequest`
 * @param response `HttpServletResponse`
 * @param params 请求参数，通常是一个json对象：
 *  1. params.param1 等价于 `request.getParameter('param1')`
 *  2. 当请求的`Contect-Type`==`application/json`（前端通过`rc`函数进行的调用）时，那么返回请求体的json对象, 等价于`request.getRequestBody()`
 */
export function execute(request: HttpServletRequest, response: HttpServletResponse, params: any) {
    let mergePath = params.mergePath;
    response.setContentType('application/pdf');
    IOUtils.copy(new FileInputStream(new File(mergePath)), response.getOutputStream())

}
export function getMergePdf(request: HttpServletRequest, response: HttpServletResponse, params: any) {
    let fjs = params.fjs;
    if (!fjs || !fjs.length) {
        return {
            status: 'failed',
            msg: '缺少附件参数!'
        }
    }
    let fjCount = fjs.length;
    if (fjCount < 2) {
        return {
            status: 'success',
            data: combine2(getFilePath(fjs[0]), '').path
        }
    }
    let filePaths = [];
    for (let i = 0; i < fjs.length; i++) {
        let path1 = getFilePath(fjs[i]);
        if (i == fjs.length - 1) {
            filePaths.push(combine2(path1, '').path);
            continue;
        }
        let path2 = getFilePath(fjs[i + 1]);
        print(path2);
        let combineResult = combine2(path1, path2);
        filePaths.push(combineResult.path);
        if (combineResult.canComb) {
            i++;
        }
    }

    return {
        status: 'success',
        data: mergePDF(filePaths, '发票')
    }

}
/**
 * 获取单个发票附件路径
 */
function getFilePath(fj) {
    let fjPath = '';
    if (fj.startsWith('workdir:')) {
        fjPath = `${sys.getClusterShareDir()}/file-storage/relativePath/${fj.substr(8)}`;
    }
    if (fj.startsWith('default:')) {
        ///opt/workdir/clusters-share/file-storage/default/89/89b995cfe37f458ba24b9a66a5415026
        fjPath = `${sys.getClusterShareDir()}/file-storage/default/${fj.substr(8, 2)}/${fj.substr(8)}`;
    }
    return fjPath;
}

/**
 * 拼接发票
 * @param file1 
 * @param file2
 * @return
 */
function combine2(file1: string, file2?: string) {
    let targetDoc;
    try {
        let pdfResult1 = parsePdf(file1, 'top');
        let pdfResult2 = parsePdf(file2, 'buttom');
        print({ pdfResult1, pdfResult2 })

        //重叠文件1，文件2,组成新pdf页面
        let overLay = new Overlay();
        overLay.setInputPDF(PDDocument.load(pdfResult1.data.toByteArray()));
        if (pdfResult1.canComb && pdfResult2.canComb) {
            overLay.setDefaultOverlayPDF(PDDocument.load(pdfResult2.data.toByteArray()));
        }
        let specificPageOverlayFile = new HashMap();//<Integer, String>();
        targetDoc = overLay.overlay(specificPageOverlayFile);

        //let dest2 = new PDPageXYZDestination();
        //dest2.setPage(targetDoc.getPage(0));
        //dest2.setZoom(-1);

        //dest2.setLeft(PDRectangle.A4.getWidth());
        //dest2.setTop(PDRectangle.A4.getHeight());
        //let action2 = new PDActionGoTo();
        //action2.setDestination(dest2);
        //targetDoc.getDocumentCatalog().setOpenAction(action2);

        let targetFile = fs.createTempFile(`${utils.randomString()}.pdf`);
        let targetPath = targetFile.getPath()
        targetDoc.save(targetPath);
        return {
            path: targetPath,
            canComb: pdfResult1.canComb && pdfResult2.canComb
        }
    } catch (e) {
        print(e);
    } finally {
        targetDoc && targetDoc.close();
    }

}
/**
 * 发票转为A4大小
 * @param filePath 发票路径
 * @position 返回的格式 1.top 2.bottom
 */
function parsePdf(filePath, position = 'top') {
    print({ filePath })
    let canComb = false;
    if (!filePath) {
        return {
            canComb
        }
    }
    let A4_HEIGHT = PDRectangle.A4.getHeight();
    let A4_WIDTH = PDRectangle.A4.getWidth();
    let _baos = new ByteArrayOutputStream();
    let doc;
    try {
        doc = PDDocument.load(new File(filePath));
        let _page = doc.getPage(0);
        let _cropBox = _page.getCropBox();
        let h = _cropBox.getHeight();
        let w = _cropBox.getWidth();
        //print('oh:' + h + ',a4:' + A4_HEIGHT);
        print({ h, w, A4_HEIGHT, A4_WIDTH });
        canComb = A4_HEIGHT / 2 > h;
        if(!canComb){
            doc.save(_baos);
            return {
                data: _baos,
                canComb
            }
        }

        _cropBox.setUpperRightX(A4_WIDTH);
        _cropBox.setLowerLeftY(position == 'top' ? (-A4_HEIGHT / 2) : (-A4_HEIGHT * 1.5));
        _page.setMediaBox(_cropBox);
        _page.setCropBox(_cropBox);

        doc.save(_baos);
        return {
            data: _baos,
            canComb
        }

    } catch (e) {
        print('error')
        print(e);
    } finally {
        doc && doc.close();
    }
}


/**
 * pdf 合并
 */
function mergePDF(mergeFilePaths, fileName = 'mergefile') {
    var PDFMerger = new PDFMergerUtility();
    if (mergeFilePaths.length == 1) return mergeFilePaths[0]
    var targetFile = fs.createTempFile(`${fileName}.pdf`);
    for (var i = 0; i < mergeFilePaths.length; i++) {
        PDFMerger.addSource(mergeFilePaths[i]);
    }
    PDFMerger.setDestinationFileName(targetFile.getPath());
    PDFMerger.mergeDocuments();

    return targetFile.getPath();
}
