
import { ExportReusltType, html2pdf } from "commons/export";

/**
 * 导出指定控件的DOM
 * <p>
 *  2023-02-13 tuzw
 *  地址: https://jira.succez.com/browse/CSTM-22005
 * </p>
 * @param importFileName 导出文件
 * @param importCompId 导出组件, 用于计算PDF宽度, 避免页面宽度大于PDF宽度,则将内容拆分了多页PDF
 * @param patientNumber 病案号
 * @return 
 */
private exportMedicalRecord(event: InterActionEvent): Promise<any> | void {
	let page = event.page;
	let params = event.params;
	let importFileName: string = params?.importFileName;
	if (isEmpty(importFileName)) {
		importFileName = `${formatDate(new Date(), "yyyyMMdd")}医院病案信息`;
	}
	let importCompId: string = params?.importCompId;
	if (isEmpty(importCompId)) {
		showWarningMessage(`未获取到需要导出的控件ID`);
		return;
	}
	let redener = event.renderer;
	let fileInfo = page.getFileInfo();
	let resid = fileInfo.id;
	let importComp = redener?.getComponent(importCompId);
	if (isEmpty(importComp)) {
		showWarningMessage(`导出控件不存在`);
		return;
	}
	let domBase = importComp?.getDomBase() as HTMLElement;
	let patientNumber: string = params?.patientNumber;
	if (isEmpty(patientNumber)) {
		showWarningMessage(`未传入病案号`);
		return;
	}
	
	let exportSpgAddress: string = ctx("/HBAGL/app/SY.app/ruleCheck/highFile/medicalRecordPage.spg"); // 给URL增加上下文父目录
	let importUrl: string = `${exportSpgAddress}?patientNumber=${patientNumber}`
	let importPromise = url2pdf(exportSpgAddress, importUrl, importFileName, ExportReusltType.DOWNLOAD, {
		width: (domBase.widthNoPadding + 140),
		margin: {
			all: 30
		}
	}).then((result => {
		return;
	}));
	return showWaiting(importPromise, domBase);
}