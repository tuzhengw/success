private converFieldType(type) {
	let newType;
	switch (type) {
		case "String":
			newType = "C"; break;
		case "Number":
		case "Int":
			newType = "I"; break;
		case "Float":
			newType = "N"; break;
		case "Date":
			newType = "D"; break;
		case "Time":
			newType = "T"; break;
		case "Timestamp":
			newType = "P"; break;
		default:
			newType = type; break;
	}
	return newType;
}