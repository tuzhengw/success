/**
 * 新增计划任务信息
 * 1）后端直接插入一条计划任务信息，不走产品的api新增计划，会导致新增计划无法按照计划频率自动执行
 * 2）后端调用此前端方法，走产品的api写入计划任务
 * 
 * @param schedule 新增计划任务信息
 */
export function addScheduleInfos(schedule: string): Promise<string> {
	let addSchedule: ScheduleInfo = JSON.parse(schedule);
	/**
	 * 注意：不要直接调用前端开放的方法，由于是后端调用，可能出现：前端部分模块无法加载，eg：document is undefined
	 * 解决部分: 引入前端sys模块的ready()，等待国际化后，在执行
	 */
	ready().then(() =>{
		return rc({ // 
			url: "/api/schedule/services/addSchedule",
			data: addSchedule
		}).then((scheduleInfo) => {
			return JSON.stringify(scheduleInfo);
		});
	});
}