
import db from "svr-api/db";
import dw from "svr-api/dw";
import metadata from "svr-api/metadata";
import bean from "svr-api/bean";
import { sdiModelUtils } from "/sysdata/app/zt.app/commons/sdiUtils/sdiModelUtils.action";

/**
 * 校验模型数据主键字段唯一性是否合法
 * - 存在模型设置主键, 但物理表没有设置主键的情况
 */
function checkModelPk(modelFileId: string): ResultInfo {
    let modelFile: MetaFileInfo = metadata.getFile(modelFileId);
    if (!modelFileId || modelFile == null) {
        return { result: false, message: "缺失参数模型文件ID" };
    }
    let modelInfo: DwTableInfo = JSON.parse(metadata.getString(modelFileId));
    let pkFields: string[] = sdiModelUtils.getModelPkFields(modelInfo);
    let instModelPath: string = modelFile.path;
    let pkFieldName: string = pkFields[0];
    let queryInfo: QueryInfo = {
        sources: [{ // 主键不能直接进行分组查询, 产品内部会识别若当前分组仅主键字段, 不会生成分组的SQL，因为产品默认主键本身就是不重复的, 不需要分组
            id: "model1",
            path: instModelPath
        }],
        fields: [{
            name: pkFieldName, exp: `model1.${pkFieldName}`
        }]
    };
	
    let { datasource, dbSchema } = modelInfo.properties;
    if (!datasource) {
        datasource = "default";
    }
    if (datasource == "defdw") {
        datasource = sdiModelUtils.getActualDefaultDbName(instModelPath);
    }
    if (!dbSchema) {
        let dbSource = db.getDataSource(datasource);
        dbSchema = dbSource.getDefaultSchema();
    }
    let querySQL = dw.getQuerySql(queryInfo);
    let queryInfo1: QueryInfo = {
        sources: [{
            id: "model1",
            datasource: datasource,
            dbSchema: dbSchema,
            dbSql: querySQL // 包一层，使其支持主键分组查询
        }],
        fields: [{
            name: pkFieldName, exp: `model1.${pkFieldName}`, group: true
        }, {
            name: "COUNTNUMS", exp: `COUNT()`
        }],
        options: {
            queryTotalRowCount: true,
            limit: 1
        },
        having: [{
            exp: `COUNTNUMS > 1`
        }]
    };
    print(dw.getQuerySql(queryInfo1));
    // select t0.XH as XH, count(*) as COUNTNUMS from(select t0.XH as XH from sdi2_1.test_student t0) t0 group by t0.XH having count(*) > 1 limit 1
}
