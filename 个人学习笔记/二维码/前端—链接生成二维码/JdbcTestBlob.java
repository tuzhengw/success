import oracle.jdbc.driver.OracleDriver;
import oracle.sql.BLOB;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.*;

/**
 * 文件存储在数据库中
 */
public class JdbcTest {
    public static final String url = "jdbc:oracle:thin:@127.0.0.1:1521:test";
    public static final String user = "system";
    public static final String pass = "system";
    //入库文件
    public static final String inputFilePath = "F:\\input.txt";
    //出库文件
    public static final String outputFilePath = "F:\\output.txt";
	//文件字段名
    public static final String fileColumnName = "USERFILE";
    public static final String tableName = "MY_TEST";


    static {
        Driver driver = new OracleDriver();
        try {
            DriverManager.deregisterDriver(driver);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void download() {
        Connection connect=null;
        Statement statement=null;
        ResultSet resultSet=null;
        try {
            connect=DriverManager.getConnection(url, user, pass);
            statement=connect.createStatement();
            String sql = "select * from " + tableName + " where IID=1";
            resultSet = statement.executeQuery(sql);
            if (resultSet.next()) {
                BLOB blob = (BLOB) resultSet.getBlob(fileColumnName);
                InputStream in = blob.getBinaryStream();
                FileOutputStream out = new FileOutputStream(outputFilePath);
                byte[] data = new byte[1024];
                int len = 0;
                while ((len = in.read(data)) > 0) {
                    out.write(data, 0, len);
                }
                in.close();
                out.close();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            close(connect, statement, resultSet);
        }

    }



    @Test
    public void upload() {
        Connection connect=null;
        Statement statement=null;
        ResultSet resultSet=null;
        try  {
            connect=DriverManager.getConnection(url, user, pass);
            statement=connect.createStatement();
            connect.setAutoCommit(false);

            String sql = "insert into " + tableName + " values (1,empty_blob())";
            int num=statement.executeUpdate(sql);

            sql = "select * from " + tableName + " where IID=1 for update";
            resultSet = statement.executeQuery(sql);
            if (resultSet.next()) {
                BLOB blob = (BLOB) resultSet.getBlob(fileColumnName);
                OutputStream out = blob.getBinaryOutputStream();
                FileInputStream in = new FileInputStream(inputFilePath);
                byte buffer[] = new byte[1024];
                int len = 0;
                while ((len = in.read(buffer)) > 0) {
                    out.write(buffer, 0, len);
                }
                in.close();
                out.close();
            }
            connect.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            close(connect, statement, resultSet);
        }
    }
    private void close(Connection connect, Statement statement, ResultSet resultSet) {
        try {
            if (resultSet != null) {
                resultSet.close();
            }
            if (statement != null) {
                statement.close();
            }
            if (connect != null) {
                connect.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
