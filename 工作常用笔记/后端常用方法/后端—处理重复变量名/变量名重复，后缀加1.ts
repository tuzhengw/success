/**
 * 测试变量名重复
 */
function testVariableRepeat(): void {
    let names: string[] = ["name", "age", "name", "age"];
    let repeatVariableNames: RepeatName[] = []; // 记录之前处理的名字

    print(dealRepeatNames(['name'], repeatVariableNames));
	
    print(dealRepeatNames(names)); // names_1
}

/**
 * 功能：校验字符串数组重复元素，并给重复元素增添后缀
 *
 * @param names 校验的变量名集合，eg：['student']
 * @param recordNames? 对比的变量数组，eg：[{ name: 'student', repeatNums: 2}, { name: "teacher", repeatNums: 2} ]
 * @return eg：['student_3']
 * 
 * @description 注意校验的变量，会再处理前增加后缀：_1
 */
public dealRepeatNames(names: string[], recordNames: RepeatName[]): string[] {
	let newNames: string[] = [];
	for (let nameIndex = 0; nameIndex < names.length; nameIndex++) {
		let name: string = names[nameIndex];
		/** 是否重复出现 */
		let isRepeat: boolean = false;
		for (let i = 0; i < recordNames.length; i++) {
			if (recordNames[i].name == name) {
				recordNames[i].repeatNums++;
				name = `${recordNames[i].name}_${recordNames[i].repeatNums}`;
				isRepeat = true;
				break;
			}
		}
		if (!isRepeat) { // 若当前变量没有重复，则记录出现次数为1
			recordNames.push({
				name: name,
				repeatNums: 1
			});
		}
		newNames.push(name);
	}
	return newNames;
}
	

/** 重复名对象 */
interface RepeatName {
    /** 变量名 */
    name: string;
    /** 变量出现次数，起始：1 */
    repeatNums: number;
}