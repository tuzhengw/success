# 描述

此表记录一个数据检测任务的资源信息。

## 路径

/sysdata/data/tables/dg/DG_CHECK_TASK_TABLES.tbl

## 物理表名

SZSYS_4_DG_CHECK_TASK_TABLES。

## 表结构

| 字段名 | 主键 | 字段类型 | 字段描述 | 说明
| --- | :----: | :---- | :------ | :-----
| CHECK_TASK_ID | 是 | VARCHAR(32) | 任务编码 | 来自`DG_CHECK_TASKS`的`CHECK_TASK_ID`
| DW_TABLE_ID | 是 | VARCHAR(256) | 检测资源元数据代码 |
| DATA_FILTER      |       | VARCHAR(2048) | 数据范围     | 可以只检测一部分数据 |
| DATA_FILTER_DESC |       | VARCHAR(2048) | 数据范围描述 ||
| IS_INCREMENT	   |       | INT | 是否增量检测 | 0为全量检测，1为增量检测 |
| LAST_CHECK_TIME |       | TIMESTAMP | 最后成功检测时间 |该表上一次成功检测的时间，在检测完成时修改该值，用于增量检测范围过滤|
| LAST_CHECK_TIME_EXP |       | VARCHAR(2048) | 最后检测时间表达式 |用于修改后台使用的LAST_CHECK_TIME，以便以处理检测粒度，如每天检测任务时间是8点，但却需要以天为单位检测，则表达式可以写addDate(LAST_CHECK_TIME,-8,'h')|

/**
 * 注意存放错误结果的模型，物理表不能再default下面，必须在其他数据源下
 */
| ERR_DATAIL_TABLE_PATH |       | VARCHAR(2048) | 错误明细表路径 |配置该路径后，该表的检测结果将会写入到配置的模型上，若不配置，则写入到默认路径，默认路径为/sysdata/data/tables/dg/confs，表名为数据源名称|
| FIELD_VALUES |       | VARCHAR(2048) | 扩展字段 | 设置的扩展字段将会写入到错误详情表的对应字段中，该字段内容格式见：`数据检测设计.md`的 `扩展字段` |
