/**
 * 在连接查询过程中，
	（1）以左表为主表，右表若不存在，则置：null
	（2）在结果集中保留《左表中的全部记录》及右表中《满足匹配条件的记录》
 */
SELECT 
	insane_emp.ename,
	insane_job.jobname 
FROM insane_emp LEFT OUTER JOIN insane_job 
ON 
	insane_emp.jobno = insane_job.jobno;