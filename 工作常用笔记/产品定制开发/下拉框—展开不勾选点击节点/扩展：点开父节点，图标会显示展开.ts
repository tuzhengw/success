import { encodeBase64, rc_get, waitAnimationFrame, ctx, uuid, clone, SZEvent, Component, showWarningMessage, showConfirmDialog, rc, browser, showInfoDialog, showWaiting, downloadFile, showMenu, deepEqual, showSuccessMessage, showErrorMessage, BASIC_EVENT, isEmpty, showManual, message, showError } from 'sys/sys';
import { ICustomJS, IMetaFileCustomJS, InterActionEvent, IVPage, IAppCustomJS, DatasetDataPackageRowInfo, IVComponent, InputDataChangeInfo } from "metadata/metadata-script-api";
import { ITemplatePagePartRenderer, TemplatePagePartArgs, ResourcesTree } from 'app/templatepage'
import { showMetaFileDialog, showDrillPage, getCurrentUser, IMetaFileViewer, CurrentUser, MetaFileViewer } from 'metadata/metadata';
import { exportPDFByPaths, AnaActionEvent, AnaObjectRenderer, TriggerActionsArgs } from 'ana/anabrowser';
import { RuntimePropertyInfo } from 'ana/compiler';
import { CrossQuery, FloatQuery } from 'ana/builder';
import { getDwTableDataManager, DwDataChangeEvent, exportQuery } from "dw/dwapi";
import { SuperPage, SpgComponent, showBackConfirmPanel } from 'app/superpage/superpage';
import { SuperPageBuilder, SpgComponentBuilder } from "app/superpage/superpagebuilder";
import { AnaDataset, AnaNodeData, AnaDatasetRow, AnaObjectData } from 'ana/datamgr';
import { EditMode, importTableInlineComponent, isImportedTableInlineComponent } from 'commons/table';
import { GroupTableCellHeader, TableBase } from 'ana/dashboard/components/tables';
import { DashboardComponentBuilder, DashboardDataDefinitionBuilder } from 'ana/dashboard/dashboardbuilder';
import { DataProvider, showUploadDialog, Button, ComboboxFloatPanel, Combobox } from "commons/basic";
import { UploadArgs, SZUploadEvent, ThumbUpload } from "commons/upload";
import { genAttachmentUrl, refreshModelState } from 'dw/dwapi';
import { modifyClass } from './public/commons.js';

import { SpgTreeSelector } from "app/superpage/components/inputs";
import { Dialog, DialogEventCallback, IDialogButtonArgs } from "commons/dialog";
import { SpgDialog } from 'app/superpage/components/embed';
import { MessageChangeType } from 'me/smessage';

import { Tree, List, TreeItem } from "commons/tree";
import { IExpEvalDataProvider } from "commons/exp/expeval";

export const CustomJS: { [type: string]: IMetaFileCustomJS } = {
	/**
	 * 20210913
	 *  spg源：https://jira.succez.com/browse/CSTM-16425
	 * 针对下拉框增强的一个功能点，展开时，父节点图标会显示展开
	 */
	"/ZHJG/app/home.app/qyd/wjbsgl/text.spg": {
		onRender: (event: InterActionEvent): Promise<void> => {
			let id = event.component && event.component.getId();
			/**
			 * 校验当前操作组件是否下拉框
			 */
			if (id == "combobox6") {
				console.log(`==onRender:`);
				let combobox = event.component.component && event.component.component.innerComponent;
				let tree: Tree = combobox.panel && combobox.panel.component;
				/**
				 * 获得combobox的孩子属性：ComboboxFloatPanel对象
				 */
				let comboboxFloatPanel: ComboboxFloatPanel = combobox.panel;
				let owner: Combobox = comboboxFloatPanel.owner;
				let multipleSelect: boolean = comboboxFloatPanel.multipleSelect;
				if (tree) {
					/**
					 * 保留原来tree的onwillcheck方法
					 * let onwillcheck_old = tree.onwillcheck;
					 * 不能勾选父节点时，换为勾选所有叶子节点
					 */
					tree.onwillcheck = (sze: SZEvent, list: List, item: TreeItem) => {
						let result: boolean;
						if (owner.isTree && owner.selectLeafOnly && owner.checkboxVisible && multipleSelect && !item.isLeaf()) {
							result = false;
							// 特殊节点点击不全选
							let noChooseItemsArray: Array<string> = ["320200000000", "320205000000", "320206000000", "320211000000", "320213000000", "320214000000",
								"320281000000", "320282000000", "320292000000"];
							/**
							 * 若当前item为指定对象，则走重写的逻辑，反之，则走原来的逻辑
							 */
							let currentItemId: string = item.id;
							if (noChooseItemsArray.indexOf(currentItemId) != -1) {
								// 设置节点的勾选状态
								if (item.getChecked() === false) {
									// 设置节点的勾选状态，childonly-part：展开状态，false：关闭
									item.setChecked("childonly-part");
									comboboxFloatPanel.doCheckItem();
								} else {
									item.setChecked(false);
									comboboxFloatPanel.doCheckItem();
								}
								/**
								 * comboboxFloatPanel.doCheckItem(); 等同于：this.doCheckItem()
								 * doCheckItem：统一处理勾选选项的事件，包括全选，allCheckedClicked? ：是否点击了全选勾选框
								 */
							} else {
								// 返回当前勾选框的状态值（点击前没有勾选，则设置勾选，若勾选，则取消勾选）
								if (item.getChecked() === false) {
									// 加载所有子节点
									item.loadChildren(true).then(childrens => {
										childrens.filter(e => e.isLeaf() && e.setChecked(true));
										comboboxFloatPanel.doCheckItem();
									});
								} else {
									item.setChecked(false);
									comboboxFloatPanel.doCheckItem();
								}
							}
						}
						return result;
					}
				}
			}
			return Promise.resolve();
		}
	}
}