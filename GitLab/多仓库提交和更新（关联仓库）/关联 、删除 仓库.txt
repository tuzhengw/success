只需要通过git remote add <refs> <addr>即可关联多个仓库 
refs 指向远程仓库, 默认的就是origin, addr 就是仓库地址了
---------------------------------------------------------------------------------
$ git remote add 关联仓库别名 git@git.coding.net:mbinary/netease-cached-music.git
注意：此操作是在已存在的本地仓库里面操作，而不是git clone操作.
---------------------------------------------------------------------------------
异常：fatal: remote origin already exists.
原因：远程仓库的refs相同的原因, 可以换个名字
---------------------------------------------------------------------------------
《正确用法》
$ git remote add 关联仓库别名 git@git.coding.net:mbinary/netease-cached-music.git 

【注意】：新增后，先git push 到自己的仓库，改变当前仓库默认推送仓库，然后再使用VScode的推送，不然默认推送到origin
---------------------------------------------------------------------------------
可以通过git remote -v 【查看】,
$ git remote -v

localcode       git@gitlab.succez.com:tuzhengw_succez/SCDZPHZ.git (fetch)
localcode       git@gitlab.succez.com:tuzhengw_succez/SCDZPHZ.git (push)
origin  git@gitlab.succez.com:customer/SCDZPHZ.git (fetch)
origin  git@gitlab.succez.com:customer/SCDZPHZ.git (push)
origin  git@gitlab.succez.com:tuzhengw_succez/SCDZPHZ.git (push)
---------------------------------------------------------------------------------
取消：
$ git remote rm 关联仓库别名
eg：
git remote rm localcode ; 
git remote rm  tuzw（名字）
---------------------------------------------------------------------------------
看项目是从git的那个分支上拉下来的,可以在命令行中输入:  git remote show origi





