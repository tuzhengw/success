---
typora-root-url: images
---

# 部署Flink环境-standalone

​	参考：https://juejin.cn/post/7010599971233890312

​	下载地址：https://flink.apache.org/zh/downloads/

​					   https://archive.apache.org/dist/flink/

​	flink官方文档: https://nightlies.apache.org/flink/flink-docs-release-1.8/monitoring/debugging_classloading.html

## 1 环境说明

```cmd
# 创建存放压缩包文件文件夹
/usr/home/zipfile 
# 创建存放安装（解压后）文件文件夹，环境变量也是使用此目录
/usr/local/install 
```

## 2 上传到虚拟机指定目录下

![](/上传flink.png)

## 3 解压并移动到指定目录

​	**解压文件**: 

```cmd
[root@localhost zipfile]# tar -zxvf flink-1.16.1-bin-scala_2.12.tgz
flink-1.16.1/
flink-1.16.1/licenses/
flink-1.16.1/licenses/LICENSE.jaxb
flink-1.16.1/licenses/LICENSE.jsr166y
......

查看解压文件
[root@localhost zipfile]# ls
flink-1.16.1  flink-1.16.1-bin-scala_2.12.tgz  jdk-11_linux-x64_bin.tar.gz
```

​	**移动到指定目录**:

```cmd
[root@localhost zipfile]# mv flink-1.16.1 /usr/local/install

[root@localhost usr]# cd local/install
[root@localhost install]# ls
flink-1.16.1  jdk-11
```

## 4 配置环境变量

​	若环境上有多个Flink 环境，默认读取第一个flink环境配置。

```
# 1、进入目录下(此处我们是提取将其放置到/usr/local/install下, 路径为: /usr/local/install/flink-1.16.1)
cd flink-1.16.1/

# 2、查看完整classpsth，然后复制
pwd 

# 3、编辑系统变量
sudo  vim  /etc/profile

#4、配置变量环境( FLINK_HOME 起名可自定义, 不要包含数字)
export FLINK_HOME=/usr/local/install/flink-1.16.1
export PATH=$PATH:$FLINK_HOME/bin
```

```cmd
[root@localhost install]# sudo vim /etc/profile
[root@localhost install]# source /etc/profile #5 刷新系统变量环境
[root@localhost install]# $FLINK_HOME  #6 查看是否配置成功
bash: /usr/local/install/flink-1.16.1: Is a directory
[root@localhost install]# 
```

## 5 配置Flink conf文件

​	进入到flink-1.16.1/conf目录下

```cmd
[root@localhost /]# cd /usr/local/install/flink-1.16.1/conf
[root@localhost conf]# ls
flink-conf.yaml           log4j.properties          logback-session.xml  workers
log4j-cli.properties      log4j-session.properties  logback.xml          zoo.cfg
log4j-console.properties  logback-console.xml       masters
```

### 5.1 配置flink-conf.yaml

```cmd
[admin@localhost flink-1.16.1]$ cd conf
[admin@localhost conf]$ vim flink-conf.yaml
```

```cmd
# taskmanager不要改，如果要改则要更改 /etc/hosts配置, 否则无法根据IP找到对应的地址
```

```python
#1. 配置jobmanager rpc 地址
jobmanager.rpc.address: 192.168.144.128 (可通过ifconfig 查看)

#2. 修改taskmanager内存大小，可改可不改
taskmanager.memory.process.size: 2048m

#3. 修改一个taskmanager中对于的taskslot个数，可改可不改
# 本地最好设置一个, 避免关闭的时候无法一次性全部关闭, 导致后续启动与上次没有正常关闭的冲突, 无法访问UI页面
taskmanager.numberOfTaskSlots: 1 

# 修改并行度，可改可不改
parallelism.default: 1
```

![](/修改finkConfi文件.png)

### 5.2 配置master

​	修改主节点ip地址

```cmd
[admin@localhost flink-1.16.1]$ cd conf
[root@localhost conf]# vim masters
```

![](/配置master节点.png)

### 5.3) 配置work

​	修改从节点ip，因为是standalone，所有主从一样(192.168.144.128,  默认值: localhost)

```cmd
[admin@localhost flink-1.16.1]$ cd conf
[root@localhost conf]# vim workers
```

### 5.4) 配置zoo

​	新建snapshot存放的目录，在flink-1.13.2目录下建zookeeper文件夹

```cmd
[root@localhost conf]# cd ..
[root@localhost flink-1.16.1]# mkdir tmp
[root@localhost flink-1.16.1]# cd tmp
[root@localhost tmp]# mkdir zookeeper
[root@localhost tmp]# ls
zookeeper
```

​	修改conf下zoo.cfg配置

```cmd
[admin@localhost flink-1.16.1]$ cd conf
[root@localhost conf]$ vim zoo.cfg
```

​	**原值**

```cmd
# The number of milliseconds of each tick
tickTime=2000

# The number of ticks that the initial  synchronization phase can take
initLimit=10

# The number of ticks that can pass between  sending a request and getting an acknowledgement
syncLimit=5

# The directory where the snapshot is stored.
# dataDir=/usr/local/install/flink-1.16.1/tmp/zookeeper

# The port at which the clients will connect
clientPort=2181

# ZooKeeper quorum peers
server.1=localhost:2888:3888
# server.2=host:peer-port:leader-port
```

​	**修改后**

```cmd
# The number of milliseconds of each tick
tickTime=2000

# The number of ticks that the initial  synchronization phase can take
initLimit=10

# The number of ticks that can pass between  sending a request and getting an acknowledgement
syncLimit=5

# The directory where the snapshot is stored.
dataDir=/usr/local/install/flink-1.16.1/tmp/zookeeper

# The port at which the clients will connect
clientPort=2181

# ZooKeeper quorum peers
server.1=192.168.144.128:2888:3888
# server.2=host:peer-port:leader-port
```

## 6 添加cdc和数据库jar

​	Flink_CDC搭建及简单使用: https://blog.csdn.net/weixin_43914798/article/details/121361949

​	jar包下载地址：

​		1） https://mvnrepository.com/artifact/mysql/mysql-connector-java/8.0.13

​		2） https://www.cnblogs.com/Springmoon-venn/p/16327247.html

![](/上传cdc和数据库连接jar.png)

## 7 Flink启动和关闭

​	进入flink-1.13.2/bin目录下

```cmd
[root@localhost /]# cd /usr/local/install/flink-1.16.1
[root@localhost flink-1.16.1]# cd bin
[root@localhost bin]# ls
bash-java-utils.jar  kubernetes-jobmanager.sh   start-zookeeper-quorum.sh
config.sh            kubernetes-session.sh      stop-cluster.sh
find-flink-home.sh   kubernetes-taskmanager.sh  stop-zookeeper-quorum.sh
flink                pyflink-shell.sh           taskmanager.sh
flink-console.sh     sql-client.sh              yarn-session.sh
flink-daemon.sh      sql-gateway.sh             zookeeper.sh
historyserver.sh     standalone-job.sh
jobmanager.sh        start-cluster.sh
```

### 7.1 启动

​	密码: admin 

​	注意: 若虚拟机IP地址更换了, 需要重新走一遍第5步。

​    启动后注意slot数量是否为0，若为0，则检查conf.yaml配置文件

```cmd
[root@localhost bin]# ./start-cluster.sh（./ 当前目录, 不加可能不是当前目录）    
# 若报不是一个目录或者启动失败, 则输入: start-cluster.sh
# [root@localhost bin]# ./taskmanager.sh start  启动任务管理器（start-cluster.sh会携带这个命令）

Starting cluster.
Starting standalonesession daemon on host localhost.localdomain.
The authenticity of host '192.168.144.128 (192.168.144.128)' can't be established.
ECDSA key fingerprint is SHA256:XKfuzXxSDSZLjBegdzswL5osThqxRzOS1sVyrEGLrYk.
ECDSA key fingerprint is MD5:8c:99:24:89:bc:8e:03:da:54:5d:3a:3b:19:b3:d2:60.
Are you sure you want to continue connecting (yes/no)? yes // 无法识别主机真实性, 是否继续启动
Warning: Permanently added '192.168.144.128' (ECDSA) to the list of known hosts.
root@192.168.144.128's password:   // 直接输入回车即可, 密码不可见
Permission denied, please try again.
root@192.168.144.128's password: 
Starting taskexecutor daemon on host localhost.localdomain.


[root@localhost bin]# jps
62598 TaskManagerRunner
62678 Jps
62234 StandaloneSessionClusterEntrypoint
```

​	访问地址：http://192.168.144.128:8081/#/overview

​	效果图：

![](/启动效果图.png)

### 7.2 关闭

```cmd
[root@localhost bin]# ./stop-cluster.sh  # 启动失败则尝试: stop-cluster.sh
# [root@localhost bin]# ./taskmanager.sh stop 
```

## 8 常见问题

### 8.1 浏览器连不上 Flink WebUI 8081 端口

​	参考地址: https://blog.csdn.net/qq_17685725/article/details/126412706

​	问题排查：

​	1）检查是否有flink以外的任务占用8081端口

​	2）检查防火墙是否已关

​	解决办法: 

​	1）打开 flink-1.15.1/conf/flink-conf.yaml 文件，修改为：rest.bind-address: 0.0.0.0，如果布置了集群，记得分发该文件

```cmd
[root@localhost /]# cd /usr/local/install/flink-1.16.1

[root@localhost bin]# cd ..
[root@localhost flink-1.16.1]# cd conf
[root@localhost conf]# ls
flink-conf.yaml           log4j.properties          logback-session.xml  workers
log4j-cli.properties      log4j-session.properties  logback.xml          zoo.cfg
log4j-console.properties  logback-console.xml       masters
[root@localhost conf]# vim flink-conf.yaml
```

![](/修改restbind地址.png)

​	2）重启 flink

​	3）浏览器正常连上 Flink WebUI 的 8081 端口

### 8.2 flink无法关闭问题

​	原因：pid丢失，无法关闭服务

```
[root@localhost bin]# jps
62598 TaskManagerRunner
62678 Jps
62234 StandaloneSessionClusterEntrypoint

直接kill杀死线程
```

```cmd
[root@localhost bin]# stop-cluster.sh
root@192.168.144.129's password: 
No taskexecutor daemon to stop on host localhost.localdomain.
No standalonesession daemon to stop on host localhost.localdomain.
```

​		参考：https://blog.csdn.net/qq_37135484/article/details/102474087

​		解决办法：修改存储pid文件路径，默认: /tmp

![](/自定义flink存储pid目录.png)

### 8.3 一台服务器多个Flink版本

​	先关闭上个版本的`Flink`环境，然后在bin目录下输入:

```
 ./start-cluster.sh
```

### 8.4 `Slot`任务数显示0

​	如果是rpc通信失败的错误大概率是jobmanager.bind-host: localhost 这个配置项引起的，改为jobmanager.bind-host: 0.0.0.0 放开flink的rpc通信权限，等心跳包发送到jobmanager则注册成功，页面显示正确的数量即部署成功；
​	如果webUI无法外机访问把rest.bind-address: 0.0.0.0 这个设置放开权限即可。



# end 