

/**
 * 20220108 tuzw
 * 获取【数据模型】组件页面内的【多字段过滤】值
 * 参考地址：
 * （1）数据模型参数：https://docs.succbi.com/superpage/component/embeddatamodel/#param
 * （2）https://docs.succbi.com/dev/sys-urls/#model
 * 
 * 注意：【数据模型】上的【多字段过滤】，必须点击【查询】才会存储到指定对象中【EasyFilter】
 * 
 * 优化：
 * （1）用于点击确定后，又重新打开页面，页面上是否还保留刚刚设置的过滤条件
 * （2）过滤条件是否显示【中文】，但实际过滤条件是【英文】，eg：部门号==1001 departId==1001
 */
button_dg_get_dataModel_filterCondition: (event: InterActionEvent) => {
	let page = event.page;
	let dataModel = page.getComponent('embeddatamodel1');
	let dataModelinnerComponent = dataModel.getUIComponent().getInnerComoponent();
	/**
	 * 20220110
	 * dwTable：数据模型的相关参数设置，例如：过滤条件
	 * let fieldFirlter = dataModelinnerComponent.dwTable.getEasyFilter()[0];
	 * 上述获取方式，必须要点击【查询】【数据模型】后，才可以获取到设置的【过滤条件】
	 */
	let fieldFirlter = dataModelinnerComponent.getPanels().getCurrentPanel().getComponent("easyFilter").getValue()[0];
	let filterCondition: string = "";
	if (!!fieldFirlter) {
		let filterContent = fieldFirlter["clauses"];
		let connectionOperator: string = "and";
		if (fieldFirlter["matchAll"] != undefined && !fieldFirlter["matchAll"]) {
			connectionOperator = "or";
		}
		for (let i = 0; i < filterContent.length; i++) {
			let leftExp: string = filterContent[i]["leftExp"];
			let operator: string = filterContent[i]["operator"];
			let rightValue: string = filterContent[i]["rightValue"];
			if (i > 0) {
				// NBXH == '1022JS114308430' and QYMC == '江苏亿舟文化发展有限公司'
				filterCondition = `${filterCondition} ${connectionOperator} ${leftExp} ${operator} '${rightValue}'`;
				continue;
			}
			filterCondition = `${leftExp} ${operator} '${rightValue}'`;
		}
	} else {
		showWarningMessage(`--当前未设置过滤条件!!!`);
	}
	// 将【过滤条件】设置到【SPG】指定【文本组件】的值
	let textComponent = page.getComponent('multipleinput4');
	textComponent.setValue(filterCondition);
},