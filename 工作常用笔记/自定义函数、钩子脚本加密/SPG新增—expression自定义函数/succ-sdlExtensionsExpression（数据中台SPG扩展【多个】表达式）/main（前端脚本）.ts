import { IExpEvalDataProvider } from "commons/exp/expeval";
/**
 * 获取指定数据库允许的最大表名长度
 * @param dataBaseType 数据库类型
 * @return 当前数据库所允许的最大表名长度
 */
export function GET_DB_TABLE_LEN(context: IExpEvalDataProvider, dataBaseType: string): number {
    return extensionExpressionUtils.getDataBaseSupportTableNameLen(dataBaseType);
}

/**
 * 解析某数值，返回时间段信息
 * @param context：SPG对象
 * @param numbers 处理的数值（必须为number，且不小于0）
 * @return eg: 13475527 ———— 4小时44分35秒
 */
export function GET_NUMBERS_DATA_DURATION(context: IExpEvalDataProvider, numbers: string): string {
    if (!extensionExpressionUtils.isNumber(numbers)) {
        return "参数不是数值型";
    }
    return extensionExpressionUtils.getFriendlyDuration(parseFloat(numbers));
}

/**
 * 根据选择【前后缀】信息，修改表的前后缀
 * @param  tablePrefix 前后缀和操作信息，eg："delPre.123:addPre.ODS_"
 *         源表前后缀是否删除 + "." + 源表前后缀名 + ":" + 目标表前后缀是否新增 + "." + 目标表前后缀名
 * @param  tableName 处理的表名
 * @param  ignoreCase 比较是否忽略大小写，默认不忽略
 * @return 调整前后缀信息的表名
 */
export function ADJUST_TABLE_PRE_OR_SUF(context: IExpEvalDataProvider, tablePrefix: string, tableName: string, ignoreCase?: boolean): string {
    return extensionExpressionUtils.parseTablePrefix(tablePrefix, tableName, ignoreCase);
}


/**
 * 扩展表达式工具方法
 */
class ExtensionExpressionsUtils {

    /**
     * 获取指定数据库允许的最大表名长度
     * @param dataBaseName 数据库的名称
     * @return 当前数据库所允许的最大表名长度
     */
    public getDataBaseSupportTableNameLen(dataBaseType: string): number {
        if (dataBaseType == null) {
            return -1;
        }
        let supportMaxLen: number = 0;
        dataBaseType = dataBaseType.toLocaleLowerCase();
        switch (dataBaseType) {
            case 'oracle': supportMaxLen = 30;
                break;
            case 'mysql': supportMaxLen = 64;
                break;
            case 'vertica': supportMaxLen = 32;
                break;
            case 'sqlsever': supportMaxLen = 64;
                break;
            default: supportMaxLen = 64;
        }
        return supportMaxLen;
    }

    /**
     * 将某毫秒数转换为时间段信息
     * @param numbers 处理的数值（毫秒）
     * @return eg: 13475527 ———— 4小时44分
     */
    public getFriendlyDuration(numbers: number): string {
        if (typeof numbers != "number" || numbers < 0) {
            return "typeError";
        }
        // 若1秒都没有，则保留2位小数
        if (numbers < 1000) {
            return `${(numbers / 1000.0).toFixed(2)}秒`;
        }
        numbers = Math.floor(numbers / 1000); /** 转换为秒 */
        let hour = Math.floor(numbers / 3600);
        let minute = Math.floor(numbers % 3600 / 60); /** numbers % 3600 去除"时"，获取剩下的"分" */
        let second = numbers % 60;
        if (numbers < 60) {
            return `${second}秒`;
        }
        if (numbers < 60 * 60) {
            return `${minute}分${second}秒`;
        }
        return `${hour}时${minute}分${second}秒`;
    }

    /**
     * 校验字符串是否为数字
     */
    public isNumber(val) {
        let regPos = /^\d+(\.\d+)?$/; //非负浮点数
        let regNeg = /^(-(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*)))$/; //负浮点数
        if (regPos.test(val) || regNeg.test(val)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 根据选择【前后缀】信息，修改表的前后缀
     * @param tablePrefix 前后缀和操作信息，eg："delPre.123:addPre.ODS_"
     *      源表前后缀是否删除 + "." + 源表前后缀名 + ":" + 目标表前后缀是否新增 + "." + 目标表前后缀名
     * @param tableName 处理的表名
     * @param ignoreCase 比较是否忽略大小写，默认不忽略
     * <pre>
     *      parseTablePrefix("delPre.ODS_:addPre.NEW_", "ODS_TABLE_NAME") ——> NEW_TABLE_NAME
     * </pre>
     */
    public parseTablePrefix(tablePrefix: string, tableName: string, ignoreCase?: boolean): string {
        if (!tablePrefix) {
            return tableName;
        }
        if (!tableName) {
            return "";
        }
        let newTableName: string = tableName.trim();
        let dealStr: string[] = tablePrefix.split(":");
        let sourceTableStr: string[] = dealStr[0].split(".");

        let sourceTableOption: string = sourceTableStr[0];
        let sourceTableAdjustStr: string = sourceTableStr[1];
        if (!!sourceTableOption && !!sourceTableAdjustStr) { // 处理源表信息
            switch (sourceTableOption) {
                case "delPre": newTableName = this.removeStart(newTableName, sourceTableAdjustStr, ignoreCase);
                    break;
                case "delSuf": newTableName = this.removeEnd(newTableName, sourceTableAdjustStr, ignoreCase);
                    break;
            }
        }
        let targetTableStr = dealStr[1].split(".");
        let targetTableOption: string = targetTableStr[0];
        let targetTableAdjustStr: string = targetTableStr[1];
        if (!!targetTableOption && !!targetTableAdjustStr) { // 处理目标表信息
            switch (targetTableOption) {
                case "addPre": newTableName = this.prependCase(newTableName, targetTableAdjustStr, ignoreCase);
                    break;
                case "addSuf": newTableName = this.appendCase(newTableName, targetTableAdjustStr, ignoreCase);
                    break;
            }
        }
        return newTableName;
    }

    /**
     * 校验str是否以字符串prefix开头，若是，则删除头部prefix
     * @param str 用于验证的字符串
     * @param prefix 匹配开头的字符串
     * @param ignoreCase 比较是否忽略大小写，默认不忽略
     * @return
     */
    public removeStart(str: string, prefix: string, ignoreCase?: boolean): string {
        if (!prefix) {
            return str;
        }
        if (!str) {
            return str;
        }
        if (str.length < prefix.length) {
            return str;
        }
        if (!!ignoreCase) {
            str = str.toUpperCase();
            prefix = prefix.toUpperCase();
        }
        /** 是否指定字母开头 */
        let isOppontLetterStart: boolean = true;
        for (let i = 0; i < prefix.length && isOppontLetterStart; i++) {
            if (str[i] == prefix[i]) {
                continue;
            } else {
                isOppontLetterStart = false;
            }
        }
        if (isOppontLetterStart) {
            str = str.substring(prefix.length);
        }
        return str;
    }

    /**
     * 校验str是否以字符串prefix结尾，若是，则删除尾部prefix
     * @param str 用于验证的字符串
     * @param prefix 匹配结尾的字符串
     * @param ignoreCase 比较是否忽略大小写，默认不忽略
     * @return 
     */
    public removeEnd(str: string, prefix: string, ignoreCase?: boolean): string {
        if (!prefix) {
            return str;
        }
        if (!str) {
            return str;
        }
        if (str.length < prefix.length) {
            return str;
        }
        if (!!ignoreCase) {
            str = str.toUpperCase();
            prefix = prefix.toUpperCase();
        }
        /** 是否指定字母结尾 */
        let isOppontLetterEnd: boolean = true;
        let strIndex: number = str.length - 1;
        for (let i = prefix.length - 1; i >= 0 && isOppontLetterEnd; i--) {
            if (str[strIndex] == prefix[i]) {
                strIndex--;
                continue;
            } else {
                isOppontLetterEnd = false;
            }
        }
        if (isOppontLetterEnd) {
            str = str.substring(0, strIndex + 1);
        }
        return str;
    }

    /**
     * 确保字符串str以字符串prefix开头，返回处理之后的字符串
     * @param str 用于验证的字符串
     * @param prefix 匹配结尾的字符串
     * @param ignoreCase 比较是否忽略大小写，默认不忽略
     * return 
     */
    public prependCase(str: string, prefix: string, ignoreCase?: boolean): string {
        if (!prefix && !str) {
            return str;
        }
        if (!str && !!prefix) {
            return prefix;
        }
        if (!!str && !prefix) {
            return str;
        }
        if (!!ignoreCase) {
            str = str.toUpperCase();
            prefix = prefix.toUpperCase();
        }
        /** 是否指定字母开头 */
        let isOppontLetterStart: boolean = true;
        for (let i = 0; i < prefix.length && isOppontLetterStart; i++) {
            if (str[i] == prefix[i]) {
                continue;
            } else {
                isOppontLetterStart = false;
            }
        }
        if (!isOppontLetterStart) {
            str = `${prefix}${str}`;
        }
        return str;
    }

    /**
     * 确保字符串str以字符串suffix结尾或者以params中任意一个字符串结尾，返回处理之后的字符串
     * @param str 用于验证的字符串
     * @param prefix 匹配结尾的字符串
     * @param ignoreCase 比较是否忽略大小写，默认不忽略
     * return 
     */
    public appendCase(str: string, prefix: string, ignoreCase?: boolean): string {
        if (!prefix && !str) {
            return str;
        }
        if (!str && !!prefix) {
            return prefix;
        }
        if (!!str && !prefix) {
            return str;
        }
        if (!!ignoreCase) {
            str = str.toUpperCase();
            prefix = prefix.toUpperCase();
        }
        /** 是否指定字母结尾 */
        let isOppontLetterEnd: boolean = true;
        let strIndex: number = str.length - 1;
        for (let i = prefix.length - 1; i >= 0 && isOppontLetterEnd; i--) {
            if (str[strIndex] == prefix[i]) {
                strIndex--;
                continue;
            } else {
                isOppontLetterEnd = false;
            }
        }
        if (!isOppontLetterEnd) {
            str = `${str}${prefix}`;
        }
        return str;
    }
}
/** 扩展表达式工具方法示例 */
const extensionExpressionUtils = new ExtensionExpressionsUtils();
