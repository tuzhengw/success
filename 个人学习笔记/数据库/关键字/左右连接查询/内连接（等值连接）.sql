
/**
 * 等值连接（内连接）：进行多表查询时，按照匹配条件，在结果集中只保留各个表中满足匹配条件的记录
 *（包含：重复项）
 */
SELECT 
	insane_emp.ename, 
	insane_job.jobname 
FROM 
	insane_emp, insane_job 
WHERE 
	insane_emp.jobno = insane_job.jobno; 
	
	
SELECT 
	insane_emp.ename,
	insane_job.jobname 

FROMinsane_emp 	INNER JOIN 	insane_job 
ON 
	insane_emp.jobno=insane_job.jobno;