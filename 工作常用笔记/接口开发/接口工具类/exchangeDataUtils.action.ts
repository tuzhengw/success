/**
 * ============================================================
 * 作者: tuzhengw
 * 审核人员：liuyongz
 * 创建日期：20220530
 * 功能描述：
 *      此次数据对接通用工具方法
 * ============================================================
 */
import { getDataSource } from "svr-api/db";
import fs from "svr-api/fs";
import { getDwTable, queryData, updateDwTableData } from "svr-api/dw";
import { getBean } from "svr-api/bean";
import { uuid } from "svr-api/utils";
import utils from "svr-api/utils";

/**
 * 数据交换工具类
 */
export class DATA_EXCHANGE_UTILS {

    public constructor() { }

    /**
     * 获取某表所含最大数据总数
     * @param physicalTableName 物理表名
     * @param dataSource 
     * @param schem
     * return 
     */
    public getTableMaxNums(physicalTableName: string, dataSource: Datasource, schem: string): number {
        if (!physicalTableName) {
            return 0;
        }
        let maxNums: number = 0;
        let querySQL: string = `select count(*) as maxNums from ${schem}.${physicalTableName}`;
        maxNums = dataSource.executeQuery(querySQL)[0]['maxNums'];
        console.info(`获取表【${physicalTableName}】所含最大数据总数: ${maxNums}`);
        return maxNums;
    }

    /**
     * 获取某模型表【查询的字段】信息
     * @param modelPath 模型表路径
     * @param dbPhysicalFields 【记录】模型【物理字段名】数组, 用于数组转换为JSON
     * @param dbPirmaryKey 【记录】表物理名主键
     * @param ignoredFields 忽略的【字段名】数组
     * @return 查询字段格式, eg: 
     * [{ 
     *      name: "企业名称", 
     *      exp: "model1.ENTERPRISE_NAME" 
     * }]
     */
    public getDwQueydataFieldInfos(modelPath: string, dbPhysicalFields?: Array<string>, dbPirmaryKey?: Array<string>, ignoredFields?: Array<string>): QueryFieldInfo[] {
        if (!modelPath) {
            console.debug(`getTableFieldInfo(), 获取模型【${modelPath}】物理表字段失败, 原因: modelPath参数错误`);
            return [];
        }
        let modelProperties: DwTablePropertiesInfo = getDwTable(modelPath).properties;
        let modelPrimary: string[] = [];
        let primaryKeys: string[] = modelProperties.primaryKeys as string[]; // class java.util.ArrayList
        if (!!primaryKeys) {
            for (let i = 0; i < primaryKeys.size(); i++) {
                modelPrimary.push(primaryKeys.get(i));
            }
        } else {
            console.info(`【${modelPath}】该模型没有主键`);
        }
        let dataBase: string = modelProperties.datasource as string;
        if (dataBase == "defdw") { // 默认数据源需要在系统页面设置, 不一定是default库
            dataBase = "default";
        }
        let tableName: string = modelProperties.dbTableName as string;
        let schema: string = modelProperties.dbSchema as string;
        let queryFields: QueryFieldInfo[] = [];
        try {
            let ds: Datasource = getDataSource(dataBase);
            if (!schema) {
                schema = ds.getDefaultSchema();
            }
            console.info(`获取: ${dataBase} 数据源下的Schema为: ${schema}, 表名为: ${tableName}`);
            let dsMeta = ds.getTableMetaData(tableName, schema);
            let fields: TableFieldMetadata[] = dsMeta.getColumns();
            for (let i = 0; i < fields.length; i++) {
                let modelFields = fields[i];
                let dbfield: string = modelFields.name;
                let fieldDesc: string = modelFields.desc;
                if (!fieldDesc) {
                    fieldDesc = dbfield;
                }
                if (!!dbPirmaryKey && !!modelPrimary && modelPrimary.includes(fieldDesc)) {
                    dbPirmaryKey.push(dbfield);
                }
                if (!!dbPhysicalFields) {
                    dbPhysicalFields.push(dbfield);
                }
                let queryField: QueryFieldInfo = {
                    name: `${fieldDesc}`,
                    exp: `model1.${dbfield}`
                }
                queryFields.push(queryField);
            }
        } catch (e) {
            console.info(`通过模型路径【${modelPath}】获取物理表字段信息失败--error`);
            console.info(e.toString());
        }
        return queryFields;
    }

    /** 
     * 获取查询的过滤条件（当天获取昨天的数据）
     * @param sendType 推送类型【全量 | 增量】
     * @param filterColumn 增量字段名
     * @param isPushNowDayData 是否增量范围为当天数据, 默认: 昨天到执行时间
     * @reutnr { result: true, data: [{exp: "model1.CREATE_DATE" > "2021-12-12 23:55:00"}] }
     */
    public getQueryFilterCondition(sendType: string, filterColumn: string, isPushNowDayData: boolean): ResultInfo {
        let sdfTime = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        if (sendType == "增量") {
            let now: number = new Date().setHours(0, 0, 0);
            let filterNums: number = 0;
            if (isPushNowDayData) {
                console.info(`增量: 获取当天0点至当前数据`);
                filterNums = now - 1 * 60 * 1000;
            } else {
                console.info(`增量: 获取昨天0点至当前数据`);
                filterNums = now - 24 * 60 * 60 * 1000 - 1 * 60 * 1000;
            }
            let lastSendTime: string = sdfTime.format(new java.util.Date(filterNums));
            console.info(`当前请求方式是: 增量请求, 过滤表达式: model1.${filterColumn} > '${lastSendTime}'`);
            return { result: true, data: [{ exp: `model1.${filterColumn} > '${lastSendTime}'` }] };
        }
        return { result: true, data: [] };
    }

    /**
     * 将二维数组转换为JSON数组（注: 数据与物理字段对应）
     * @param queryData 查询模型表数据返回的【二维数组】
     * @param modelDbFields 模型物理字段集
     * @return JSON数组, eg: [{"age": "13"}]
        */
    public arrToJsonArrs(queryData: (string | number | boolean)[][], modelDbFields: Array<string>): unknown {
        if (!queryData || !modelDbFields) {
            console.info(`二维数组转换为JSON数组, 【参数错误】`);
            return [];
        }
        if (queryData[0].length != modelDbFields.length) {
            console.info(`二维数组转换为JSON数组, 【行数据个数 与 物理字段不是一一对应】`);
            return [];
        }
        let resultData = [];
        for (let i = 0; i < queryData.length; i++) {
            let temp = {};
            for (let j = 0; j < queryData[i].length; j++) {
                if (queryData[i][j] == null || queryData[i][j] == undefined) { // 若列数据没有值, 则跳过
                    continue;
                }
                temp[`${modelDbFields[j]}`] = queryData[i][j];
            }
            resultData.push(temp);
        }
        return resultData;
    }

    /**
     * 校验表是否为空表
     * @param dataBase 数据源名
     * @param tableName 校验物理表名
     * @param schem
     * @return true | false
     */
    public checkTableIsNull(dataBase: string, tableName: string, schem?: string): boolean {
        let dataSource = getDataSource(dataBase);
        let conn = dataSource.getConnection();
        let tableIsNull: boolean = false;
        if (!schem) {
            schem = dataSource.getDefaultSchema();
        }
        try {
            let table: TableData;
            table = dataSource.openTableData(tableName, schem, conn);
            let checkContentIsNullSql: string = `select count(1) as COUNT from ${tableName}`;
            let queryData = table.executeQuery(checkContentIsNullSql);
            if (Number(queryData[0]["COUNT"]) == 0) {
                tableIsNull = true;
            }
        } catch (e) {
            tableIsNull = false;
            console.info(`--校验数据库【${tableName}】是否为: 空表--error--`);
            console.info(e);
        } finally {
            conn.close();
        }
        return tableIsNull;
    }

    /**
     * 校验字符串是否由纯数字构成
     * @param checkStr 校验的字符串, eg: "123"
     * @return true | false
     */
    public checkStrIsNums(checkStr: string): boolean {
        if (checkStr == undefined) {
            return false;
        }
        let checkPatten = new RegExp(/^[0-9]*$/);
        if (checkPatten.test(checkStr)) {
            return true;
        }
        return false;
    }

    /**
    * dw模块记录推送失败的数据
    * 注意: 产品的insert方法不支持多数据库转换
    * @param tablePath 写入表模型绝对路径
    * @param errorData 错误推送数据, JSON对象
    * @param writeFileds 写入的字段数组
    */
    public recordErrorSendDatas(tablePath: string, errorData: string[][], writeFileds: string[]): void {
        let dataPackage: CommonTableSubmitDataPackage = {};
        dataPackage = {
            addRows: [{
                fieldNames: writeFileds,
                rows: errorData
            }]
        }
        updateDwTableData(tablePath, dataPackage);
    }
}
const dataExchangeUtils = new DATA_EXCHANGE_UTILS();


/** 返回值 */
export interface ResultInfo {
    /** 返回值 */
    result?: boolean;
    /** 编码 */
    code?: string;
    /** 错误编码 */
    errorCode?: string;
    /** 处理消息 */
    message?: string;
    /** 返回数据 */
    data?: any;
    /** 成功插入数据的主键集 */
    insertPks?: any;
    /** 成功插入的行数  */
    rowCount?: number;
    /** 上传数据数量 */
    dataLen?: number;
}