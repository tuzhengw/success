# Promise—异步操作

## 1 Promise状态

​		Promise 异步操作有三种状态：

​			pending（进行中）、fulfilled（已成功）、 rejected（已失败）

​		除了异步操作的结果，任何其他操作都无法改变这个状态

----

> Promise 对象只有：
>
> 从 pending ———— fulfilled  或  pending ———— rejected 
>
> 只要处于 fulfilled 和 rejected ，状态就不会再变了即 resolved（**已定型**）

---

```ts
new Promise(function(resolve,reject){
    resolve('success'); // 先调用“成功”（执行结束，即已定型，不会改变状态）
    reject('reject'); // 在调用“失败”（不会改变上一步的状态）
}).then(function(value){
    console.log(value);//success
});
```

## 2 then方法

​		then 方法接收两个函数作为参数

​		（1）第一个参数是 Promise 执行成功时的回调

​		（2）第二个参数是 Promise 执行失败时的回调

​		两个函数<button>只有一个被调用</button>，且第二个函数可以省略

---

> 两个函数（resolve，reject），和两个then()方法 不一样

---

```ts
new Promise(function(resolve,reject){
    resolve('success');
}).then(
    	function(value){ 
            console.log(value);  /*success*/
        },
        function(error){
            console.log(error);
        }
);
```

## 3 then链式调用

​		then 方法将**返回**一个 resolved 或 rejected 状态的 <button>Promise 对象</button>

​		可用于<button>链式调用</button>，且《 Promise 对象》的值就是《这个返回值》

---

> then方法：返回的是一个新的Promise实例（注意，<button>不是原来那个Promise实例</button>）。因此可以采用链式写法，即then方法后面再调用另一个then方法

---

```ts
new Promise(function(resolve,reject){
    resolve('success');
}).then(function(value){
    console.log(value);
    return "success2";
}).then(function(value){
    console.log(value);
}).then(function(value){
    console.log(value);
})
/**
success
success2
undefined
*/
```

​		依次指定了三个回调函数。第一个回调函数完成以后，会<button>将返回结果作为参数</button>，传入第二个回调函数，直到整体结束

## 4 注意

​		通过<button> `.then`</button> 形式添加的回调函数，<button>不论什么时候，都会被调用</button>



