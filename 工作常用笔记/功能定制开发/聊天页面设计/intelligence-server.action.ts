/**
 * ============================================================
 * 作者: tuzhengw、wangc
 * 审核人员：liuyongz
 * 创建日期：2021-01-12
 * 功能描述：
 *      该脚本主要是用于【政策咨询】智能客服聊天
 * ============================================================
 */
import { post, get } from 'svr-api/http';
import { md5, uuid } from 'svr-api/utils';
import { getQuerySql, queryData } from 'svr-api/dw';
import { getCurrentUser, } from 'svr-api/security';
import { getDefaultDataSource } from 'svr-api/db';
import { formatDate } from "svr-api/utils";
import { sleep } from "svr-api/sys";

let ds = getDefaultDataSource();

const APP_ID = '4.10037';
const APP_KEY = 'tUvIVknt0KlVgGy1'
const HOST = 'http://10.15.24.250:7777'
const API_GetAccessToken = '/tes/v3/getAccessToken';
const API_AiChatReq = '/tes/v3/aiChatReq';

/** 问题 */
const QUESETIONS = [
    '东湖高新区2021年新经济政策（互联网+、文科融合、人工智能）奖励补贴',
    '武汉市中国民营企业500强入选奖励',
    '2021年度国家文化和旅游科技创新工程项目政策的办理流程',
    '科技“小巨人”认定奖励政策的办理流程是',
    '政策级别'
]
/**
 * 地址：https://jira.succez.com/browse/BI-47039
 * spg地址：http://58.49.165.246:8090/QYFWPT/app/pc.app?:edit=true&:file=zczx.spg
 
 * 根据用户提的问题，请求第三方接口，获取回复的信息
 * @param question 问题，eg：'政策级别？'
 * @param serverTime 服务器当前的时间（注意：服务器和现实时间可能存在差异，以：服务器的为主），eg：{ question=1, serverTime=1642068305216 }
 * 注意：
 * （1）前端调用产品：refreshMessageData()方法进行时时刷新，SPG内不能调用：插入、刷新等交互，否则，会让页面属性顺序混乱
 */
export function getResponseMessage(request: HttpServletRequest, response: HttpServletResponse, params: { question: string, serverTime: string }): void {
    console.debug(`---根据用户提的问题：【${params}】，请求第三方接口，获取回复的信息---start`);
    let question: string = params.question.trim();
    let serverTime: string = params.serverTime;
    let currentUserId: string = getCurrentUser().userInfo.userId;

    insertAiResponseMessage(currentUserId, "AI", question, serverTime);

    let responseContent: string =
        "小策猜您可能会问以下问题：\n" +
        "1. 东湖高新区2021年新经济政策（互联网+、文科融合、人工智能）奖励补贴\n" +
        "2. 武汉市中国民营企业500强入选奖励\n" +
        "3. 2021年度国家文化和旅游科技创新工程项目政策的办理流程\n" +
        "4. 科技“小巨人”认定奖励政策的办理流程是？\n" +
        "5. 政策级别？";
    if (QUESETIONS.includes(question)) {
        let accessToken = getAccessToken();
        let requestParams = {
            accessToken: accessToken,
            appid: APP_ID,
            data: {
                uname: "user001",
                keytp: "cacc",
                content: '',
                type: 'sync'
            } as RequestParamData
        }
        requestParams.data.content = question;
        let responseResult = aiChatReq(requestParams);
        if (responseResult.result == 0) {
            responseContent = "网络存在问题，请稍后再试";
        } else {
            responseContent = responseResult.data['content'];
        }
    }
    insertAiResponseMessage("AI", currentUserId, responseContent, (Number(serverTime) + 1000).toString()); // AI是自动回复，比发送人晚1秒
    console.debug(`---根据用户提的问题：【${params}】，请求第三方接口，获取回复的信息：${responseContent}---end`);
}


/**
 * 将发送的消息插入到消息库
 * @param sends 发送者ID
 * @param accepts 接受者ID
 * @param speakContent AI回应的消息
 * @param serverTime 服务器时间毫秒数字符串，区分现实时间
 */
function insertAiResponseMessage(sends: string, accepts: string, speakContent: string, serverTime: string): void {
    let conn = ds.getConnection();
    try {
        conn.setAutoCommit(false);
        let table: TableData = ds.openTableData("FACT_ZXFKXXB", "SUCCEZ", conn);
        table.insert({
            "MESSAGE_ID": uuid(),
            "SENDS": sends,
            "ACCEPTS": accepts,
            "CONTENT": speakContent,
            "CREATE_TIME": new java.sql.Timestamp(new Date(Number(serverTime)))
        });
        conn.commit();
    } catch (e) {
        print(`--将AI发送的消息插入到消息库，error--`);
        print(e);
    } finally {
        conn.close();
    }
}

/** POST请求参数 */
interface RequestParamData {
    /** 用户唯一标识，填写规则参考：userLogin接口说明  */
    uname: string;
    /** cass：注册账户；guest：游客账户 */
    keytp: string;
    /** 用户询问的问题 */
    content: string;
    /** sync：同步方式聊天；不填或者填写非sync，异步方式聊天*/
    type?: string;
    /** 应答时返回该字段，内容保持不变 */
    seq?: string;
    [propname: string]: any;
}


/** 返回结果 */
interface ResultInfo {
    /** 返回结果 */
    result: number;
    /** 返回消息 */
    msg?: string;
    /** token值 */
    accessToken?: string;
    /** token有效时间（秒） */
    expire?: number;
    /** 返回数据 */
    data?: any;
    [propname: string]: any;
}

/**
 * 获取accessToken
 */
function getAccessToken(): string {
    const APP_ID = '4.10037';
    let data = {
        appid: APP_ID,
        sign: md5(APP_KEY)
    }
    let responseResult: string = get(`${HOST}${API_GetAccessToken}?${covertJsonToForm(data)}`);
    try {
        let res: ResultInfo = JSON.parse(responseResult);
        if (res.result == 1) {
            return res.accessToken;
        } else {
            throw Error(JSON.stringify(res));
        }
    } catch (e) {
        console.error(`---只能服务，获取accessToken，解析JSON失败---`);
        console.error(e);
    }
}

/**
 * AI聊天接口请求
 * @param accessToken 访问的Token值
 * @param appid 授信APP_ID
 * @param data 参数
 * @return { result: 1, data: json数组 }
 */
function aiChatReq(params: { accessToken: string, appid: string, data: RequestParamData }): ResultInfo {
    let resultInfo: ResultInfo = { result: 1 };
    let accessToken = params.accessToken;
    let appid = params.appid;
    let data = params.data
    let _params = {
        appid,
        accessToken
    }
    let responseResult = post({
        url: `${HOST}${API_AiChatReq}?${covertJsonToForm(_params)}`,
        data: data,
        headers: {
            'Content-Type': 'application/json'
        }
    });
    if (responseResult.httpCode != 200) {
        console.debug(`----AI聊天接口请求，aiChatReq()，请求返回的结果httpCode值为：${responseResult.httpCode}---error`);
        resultInfo.result = 0;
        resultInfo.msg = JSON.stringify(responseResult.responseText);
        return resultInfo;
    }
    resultInfo.data = JSON.parse(responseResult.responseText);
    return resultInfo;
}

/**
 * 把json格式转为 param1=value&param2=value2 格式的字符串
 */
function covertJsonToForm(obj): string {
    if (!obj || obj.lenght == 0) {
        return '';
    }
    let arr: string[] = [];
    for (const key in obj) {
        arr.push(`${key}=${obj[key]}`);
    }
    return arr.join('&');
}

/**
 * 返回用户最新的聊天信息
 * @param lastTime 最近一条聊天时间 yyyy-mm-dd hh:mi:ss
 * @param chatUser 聊天对象
 * @param currentUser 当前登录的用户
 */
export function getNewMessage(request: HttpServletRequest, response: HttpServletResponse, params: { lastTime: string, chatUser: string, currentUser: string }) {
    let lastTime = params.lastTime;
    let chatUser = params.chatUser;
    let currentUser = params.currentUser;
    let user = getCurrentUser();
    let userId = user.userInfo.userId;
    if (currentUser == null || currentUser == "") {
        currentUser = userId;
    }
    let queryInfo = builderQuery(currentUser, chatUser, lastTime);
    let result = queryData(queryInfo).data;
    let rows = [];
    for (let i = 0; i < result.length; i++) {
        let info = result[i];   // ["bnbnryCT9VLuw3f8H6EmoD", "AI", "tuzhengw", "政策级别？", 1642076110768, "智能客服", "涂正维", "2022年01月13日"]
        /**
         *  20220114 
         *  业务需求不需要：按钮
            if (info[8] == "BG") {
                buttons = [{ id: "operateButton1", caption: "检测报告", confirmCaption: null, disable: "false" }];
            }
         */
        let buttons = [""];
        let json: JSONObject = {
            id: info[0],
            caption: info[5], // 发送人【中文名】
            icon: `/api/co/avatar/${info[1]}`, // 头像
            dataCount: info[1] == "AI" ? 0 : 1, // 控制消息左右，由于"AI"始终是左边，这里写死
            // dataCount: info[1] == userId ? 1 : 0,
            buttons: buttons,
            attributeCaptions: [""],
            attributes: ["model2.CONTENT"], // model2： 对应消息表
            "model2.CREATE_TIME": formatDate('yyyy-MM-dd HH:mm:ss', info[4]),
            "model2.SENDS": info[1],
            "model2.CONTENT": info[3],
            "model2.CALCULATOR_2": info["CALCULATOR_2"],
            time: formatDate('yyyy-MM-dd HH:mm:ss', info[4]),
            rowIndex: i
        };
        rows.push(json);
    }
    return rows;
}
/**
 * 获取存储【消息】的表信息
 * @param currentUser 当前登录的用户ID
 * @param chatUser 聊天的用户ID
 * @param lastTime 最后聊天的时间
 */
function builderQuery(currentUser: string, chatUser: string, lastTime: string) {
    let queryInfo = {
        "select": true,
        "distinct": false,
        "fields": [{
            "name": "MESSAGE_ID",
            "group": false,
            "exp": "q0.消息序号"
        }, {
            "name": "SENDS",
            "group": false,
            "exp": "q0.发送人"
        }, {
            "name": "ACCEPTS",
            "group": false,
            "exp": "q0.接收人"
        }, {
            "name": "CONTENT",
            "group": false,
            "exp": "q0.正文"
        }, {
            "name": "CREATE_TIME",
            "group": false,
            "exp": "q0.发送时间"
        }],
        "filter": [{
            // "exp": `q0.OWNER_ID IN ('${currentUser}','${chatUser}') AND q0.PAIR_ID IN ('${currentUser}','${chatUser}') AND q0.SEND_STATE==1 AND q0.CREATE_TIME>'${lastTime}'`
            "exp": `q0.SENDS IN ('${currentUser}','${chatUser}') AND q0.ACCEPTS IN ('${currentUser}','${chatUser}') AND q0.CREATE_TIME>'${lastTime}'`
        }],
        /**
         * SPG采取【新建数据集】，将模型按【发送时间】降序，保证：用户发送消息的先后顺序
         */
        "sort": [{
            "exp": "q0.发送时间",
            "type": "ASC"
        }],
        "sources": [{
            "id": "q0",
            "type": 0,
            "path": "/QYFWPT/data/tables/FACT/ZXFK/FACT_ZXFKXXB.tbl",
        }],
        "options": {
            "needCodeDesc": true,
            "autoSort": true,
            "queryBlobFlag": true,
            "queryTotalRowCount": true,
            "needHighlightKeyword": true,
            "highlightPreTag": "<em class='sz-keyword'>",
            "highlightPostTag": "</em>",
            "needResultFilter": true,
            "nullAsZero": false,
            "denominatorZeroAsNull": true,
            "nullSortAsc": "first",
            "nullSortDesc": "last",
            "useJoin": true,
            "optDimExp": true
        }
    }
    return queryInfo;
}