# Blob字段操作

```ts
import db from "svr-api/db";
import dw from "svr-api/dw";
import fs from "svr-api/fs";
import utils from "svr-api/utils";
import { SDILog, LogType, isEmpty, ResultInfo } from "/sysdata/app/zt.app/commons/commons.action";

const Timestamp = Java.type('java.sql.Timestamp');

function main() {
    let attachInfos: XMLAttachmentInfo[] = [{
        HISTORY_ID: utils.uuid(),
        WORD_ID: "5cab39a498dd58ce4b25b8d75a9dccedd71b",
        EMPIID: "X31jQXKc6wHtxljNYKHrdC",
        WORD_INDEX: "likowAAQ7qKCrjvOTIoW2D",
        WORD_SERIAL_NUMBER: "2023010613524523501",
        VERSION_NUMBER: "1.0",
        XML_FILE_NAME: "2022123014562943201_1.病历概要.xml",
        FILE_BYTE_CONTENT: new java.lang.String("456").getBytes(),
        CONTENT_MD5: "",
        AUTHOR_ID: "",
        AUTHOR_NAME: "",
        ORGANIZATION_ID: "",
        ORGANIZATION_NAME: "",
        CUSTODIAN_ID: "0000030001",
        CUSTODIAN_NAME: "xx医院",
        CREATOR: "admin",
        CREATE_TIME: new Timestamp(Date.now()),
        MODIFIOR: "tuzw",
        MODIFY_TIME: new Timestamp(Date.now()),
    }];
    // insertXMLAttachementInfo(attachInfos);
    updateXMLAttachementInfo(attachInfos);

    let attackPkInfos: XMLAttachmentInfo[] = [{
        WORD_ID: "5cab39a498dd58ce4b25b8d75a9dccedd71b",
        EMPIID: "X31jQXKc6wHtxljNYKHrdC",
    }];
    queryXMLAttachmentInfo(attackPkInfos);
}
```

## 1 批量写入Blob字段值

```ts
/**
 * 写入附件信息
 * @param xmlAttachmentInfos 写入XML附件信息
 * @return 
 */
function insertXMLAttachementInfo(xmlAttachmentInfos: XMLAttachmentInfo[]) {
    let insertAttachNums: number = xmlAttachmentInfos?.length;
    if (insertAttachNums == 0) {
        return { result: true, message: "插入XML附件信息为空" };
    }
    let resultInfo: ResultInfo = { result: true };
    let optionDb = db.getDataSource("HLHT");
    let dbConn = optionDb.getConnection();
    // @ts-ignore
    let stmt: JdbcPreparedStatement = null;
    dbConn.setAutoCommit(false);
    try {
        let updateSQL: string = `
            INSERT INTO 
            SDI_HLHT_HISTORY_XML_ATTACH(
                HISTORY_ID, WORD_ID, EMPIID, WORD_INDEX, WORD_SERIAL_NUMBER, VERSION_NUMBER, CREATE_TIME, FILE_BYTE_CONTENT, CONTENT_MD5, AUTHOR_ID, AUTHOR_NAME, ORGANIZATION_ID, ORGANIZATION_NAME, CUSTODIAN_ID, CUSTODIAN_NAME, CREATOR) 
            VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`;
        stmt = dbConn.prepareStatement(updateSQL);
        for (let i = 0; i < xmlAttachmentInfos.length; i++) {
            let xmlAttachmentInfo = xmlAttachmentInfos[i];

            stmt.setString(1, xmlAttachmentInfo.HISTORY_ID as string);
            stmt.setString(2, xmlAttachmentInfo.WORD_ID as string);
            stmt.setString(3, xmlAttachmentInfo.EMPIID as string);
            stmt.setString(4, xmlAttachmentInfo.WORD_INDEX as string);
            stmt.setString(5, xmlAttachmentInfo.WORD_SERIAL_NUMBER as string);
            stmt.setString(6, xmlAttachmentInfo.VERSION_NUMBER as string);
            stmt.setTimestamp(7, xmlAttachmentInfo.CREATE_TIME as JdbcTimestamp);

            // @ts-ignore
            let bytesBlob: JdbcBlob = dbConn.createBlob();
            bytesBlob.setBytes(8, xmlAttachmentInfo.FILE_BYTE_CONTENT as number[]);
            stmt.setBlob(8, bytesBlob);

            stmt.setString(9, xmlAttachmentInfo.CONTENT_MD5 as string);
            stmt.setString(10, xmlAttachmentInfo.AUTHOR_ID as string);
            stmt.setString(11, xmlAttachmentInfo.AUTHOR_NAME as string);
            stmt.setString(12, xmlAttachmentInfo.ORGANIZATION_ID as string);
            stmt.setString(13, xmlAttachmentInfo.ORGANIZATION_NAME as string);
            stmt.setString(14, xmlAttachmentInfo.CUSTODIAN_ID as string);
            stmt.setString(15, xmlAttachmentInfo.CUSTODIAN_NAME as string);
            stmt.setString(16, xmlAttachmentInfo.CREATOR as string);
            stmt.addBatch();
        }
        let insertNums: number[] = stmt.executeBatch(); // 执行所有SQL语句
        console.info(insertNums);
        dbConn.commit();
        stmt.clearBatch(); // 清空batch命令
    } catch (e) {
        dbConn.rollback();
        resultInfo.result = false;
        resultInfo.message = "写入XML附件信息失败";
        console.info(`插入XML附件信息失败----error`);
        console.info(e);
    } finally {
        stmt && stmt.close();
        dbConn && dbConn.close();
    }
}

```

## 2 批量更新Blob字段值

```ts
/**
 * 更新附件信息
 * @param updateXMLAttachments 更新XML附件信息
 * @return 
 */
function updateXMLAttachementInfo(updateXMLAttachments: XMLAttachmentInfo[]): ResultInfo {
    let updateAttachNums: number = updateXMLAttachments?.length;
    if (updateAttachNums == 0) {
        return { result: true, message: "更新XML附件信息为空" };
    }
    let resultInfo: ResultInfo = { result: true };
    let optionDb = db.getDataSource("HLHT");
    let dbConn = optionDb.getConnection();
    // @ts-ignore
    let stmt: JdbcPreparedStatement = null;
    dbConn.setAutoCommit(false);
    try {
        let updateSQL: string = `
            UPDATE
            SDI_HLHT_HISTORY_XML_ATTACH
            SET
                MODIFIOR=?,MODIFY_TIME=?,VERSION_NUMBER=?,CONTENT_MD5=?,FILE_BYTE_CONTENT=?,WORD_SERIAL_NUMBER=?
            WHERE 
                WORD_ID=? AND EMPIID=?`;
        stmt = dbConn.prepareStatement(updateSQL);
        for (let i = 0; i < updateXMLAttachments.length; i++) {
            let xmlAttachmentInfo = updateXMLAttachments[i];

            stmt.setString(1, xmlAttachmentInfo.MODIFIOR as string);
            stmt.setTimestamp(2, xmlAttachmentInfo.MODIFY_TIME as JdbcTimestamp);
            stmt.setString(3, xmlAttachmentInfo.VERSION_NUMBER as string);
            stmt.setString(4, xmlAttachmentInfo.CONTENT_MD5 as string);
            // @ts-ignore
            let bytesBlob: JdbcBlob = dbConn.createBlob();
            bytesBlob.setBytes(5, xmlAttachmentInfo.FILE_BYTE_CONTENT as number[]);
            stmt.setBlob(5, bytesBlob);

            stmt.setString(6, xmlAttachmentInfo.WORD_SERIAL_NUMBER as string);

            stmt.setString(7, xmlAttachmentInfo.WORD_ID as string);
            stmt.setString(8, xmlAttachmentInfo.EMPIID as string);
            stmt.addBatch();
        }
        let insertNums: number[] = stmt.executeBatch();
        console.info(insertNums);
        dbConn.commit();
        stmt.clearBatch();
    } catch (e) {
        dbConn.rollback();
        resultInfo.result = false;
        resultInfo.message = "更新XML附件信息失败";
        console.info(`更新XML附件信息失败----error`);
        console.info(e);
    } finally {
        stmt && stmt.close();
        dbConn && dbConn.close();
    }
}
```

## 3 查询Blob字段值

```ts
/**
 * 查询附件信息
 * @param queryAttachmentInfos 查询指定主键的附件信息
 * @return 
 */
function queryXMLAttachmentInfo(attachPkInfos: XMLAttachmentInfo[]): ResultInfo {
    let resultInfo: ResultInfo = { result: true };
    let optionDb = db.getDataSource("HLHT");
    let dbConn = optionDb.getConnection();
    // @ts-ignore
    let stmt: JdbcPreparedStatement = null;
    // @ts-ignore
    let resultSet: JdbcResultSet = null;
    let queryAttachInfos: XMLAttachmentInfo[] = [];
    try {
        let querySQL: string = `
            SELECT WORD_ID, EMPIID, XML_FILE_NAME, FILE_BYTE_CONTENT FROM SDI_HLHT_HISTORY_XML_ATTACH WHERE WORD_ID = ? AND EMPIID = ?`;
        stmt = dbConn.prepareStatement(querySQL);
        for (let i = 0; i < attachPkInfos.length; i++) {
            let xmlAttach = attachPkInfos[i];
            stmt.setString(1, xmlAttach.WORD_ID as string);
            stmt.setString(2, xmlAttach.EMPIID as string);
            resultSet = stmt.executeQuery();
            while (resultSet.next()) {
                let wordId: string = resultSet.getString(1);
                let xmlPkValue: string = resultSet.getString(2);
                let xmlFileName: string = resultSet.getString(3);
                let xmlFileContent: number[] = resultSet.getBytes(4);
                queryAttachInfos.push({
                    WORD_ID: wordId,
                    EMPIID: xmlPkValue,
                    XML_FILE_NAME: xmlFileName,
                    FILE_BYTE_CONTENT: xmlFileContent
                });
            }
        }
    } catch (e) {
        resultInfo.result = false;
        resultInfo.message = "查询XML附件信息失败";
        console.info(`执行[queryXMLAttachmentInfo], 查询XML附件信息失败----error`);
        console.info(e);
    } finally {
        resultSet && resultSet.close();
        stmt && stmt.close();
        dbConn && dbConn.close();
    }
    return resultInfo;
}
```



```ts
/** XML文件附件信息 */
export interface XMLAttachmentInfo {
    /** 历史附件信息记录主键值 */
    HISTORY_ID?: string;
    /** 文档ID */
    WORD_ID?: string;
    /** 主键ID */
    EMPIID?: string;
    /** 文档索引号 */
    WORD_INDEX?: string;
    /** 文档流水号 */
    WORD_SERIAL_NUMBER?: string;
    /** 版本号 */
    VERSION_NUMBER?: string;
    /** 创建者 */
    CREATOR?: string
    /** 生成时间 */
    CREATE_TIME?: string | Date | JdbcTimestamp;
    /** 修改者 */
    MODIFIOR?: string;
    /** 修改时间 */
    MODIFY_TIME?: string | Date | JdbcTimestamp;
    /** XML文件名 */
    XML_FILE_NAME?: string;
    /** 文档二进制内容 */
    FILE_BYTE_CONTENT?: number[] | JdbcBlob;
    /** XML MD5加密内容 */
    CONTENT_MD5?: string;
    /** 文档创作者标识符 */
    AUTHOR_ID?: string;
    /** 文档创作者姓名 */
    AUTHOR_NAME?: string;
    /** 医疗机构编码 */
    ORGANIZATION_ID?: string;
    /** 医疗机构名称 */
    ORGANIZATION_NAME?: string;
    /** 文档保管机构编码 */
    CUSTODIAN_ID?: string;
    /** 文档报关机构名称 */
    CUSTODIAN_NAME?: string;
}
```

