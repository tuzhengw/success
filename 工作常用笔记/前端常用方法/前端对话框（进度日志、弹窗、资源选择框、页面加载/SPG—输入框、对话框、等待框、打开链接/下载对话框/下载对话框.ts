import { Combobox, Button, showUploadDialog, Toolbar, ToolbarItemAlign, ComboboxArgs, DataProvider, ButtonArgs, TXT_FILETYPE, IMAGE_FILETYPE } from 'commons/basic';

/**
 * 文件大类类型，在处理文件时相同大类文件可能会进行相同处理，例如文件显示的时候，文件压缩的时候等等
 */
export enum FileMajorType {
	Text = 'text', // 文本类型
	Image = 'image', //图片类型
	Audio = 'audio',//音频类型
	Video = 'video',//视频类型
	Compress = 'compress',//压缩类型
	Other = 'other'//其他类型
}

/**
 * 移动端上传图片菜单选项。
 */
export enum UploadActionType {
	/**可以选文件，图片，以及相机（fileTypes中包含图片或isImage=true时才有图片和相机） */
	All = 'all',
	/**直接打开到相机 */
	Camera = 'camera',
	/**qq内置+微信+firefox中没有相机菜单，其他都可以选文件，图片以及相机（fileTypes中包含图片或isImage=true时才有图片和相机） */
	File = 'file',
	/**chrome+微信会直接打开到相册中，顶部有相机的按钮；其他都是先弹出图片+相机的菜单 */
	Image = 'image'
}

/**
 * 上传文件对话框的参数
 */
export interface ShowUploadDialogArgs extends UploadArgs {
	/**
	 * 移动端上传图片时弹出的菜单，有两个菜单：1. 直接打开相机，2. 选择文件，3. 弹出菜单选择，4. 相册，默认为all
	 * PC端设置无效
	 * 
	 * all：可以选文件，图片，以及相机（fileTypes中包含图片或isImage=true时才有图片和相机）
	 * file：qq内置+微信+firefox中没有相机菜单，其他都可以选文件，图片以及相机（fileTypes中包含图片或isImage=true时才有图片和相机）
	 * image：chrome+微信会直接打开到相册中，顶部有相机的按钮；其他都是先弹出图片+相机的菜单
	 * camera：直接打开到相机
	 * 
	 * 参数优先级情况：actionType>fileTypes>isImage，例如actionType='camera'后就是打开相机拍照后选择的图片，此时fileTypes和isImage参数会被忽略
	 */
	actionType?: UploadActionType;

	/**
	 * 为true时，此对话框仅打开文件而不上传。
	 * 需要前端读文件但不需要上传文件到服务端场景下，可以传此参数为true。
	 * 使用场景：
	 * 1. Excel表单填报时，前端导入Excel文件数据时读Excel文件。
	 */
	onlyOpenFile?: boolean;
}

/**
 * 显示上传文件的对话框
 * 
 * 规则：
 * 1. PC端目前只弹出选择文件的对话框，不弹出相机，通过accept来限制选中的文件类型
 * 2. ios端各平台统一性非常好，界面效果也非常统一，所以用原生的菜单即可
 * 3. 安卓端个平台配置非常不统一，界面效果也不一样；qq内置+微信+firefox无法同时选文件+拍照，需要统一弹出自定义菜单，其他都走系统菜单（避免弹出二次菜单），
 *    qq内置和firefox还是无法避免弹出二次菜单
 * 
 * @example 弹出上传当个Word文档的对话框
 * showUploadDialog({
 *  fileTypes: ["doc","docx"], //只上传word文档
 *  multiple : false,       //上传单个文档
 *  maxSize : 10*1024*1024, //限制大小10M
 *  onchange: this.onchange.bind(this) //绑定change事件，上传完成后处理
 * });
 */
let globalUploadFileInput: HTMLInputElement;

export function showUploadDialog(args: ShowUploadDialogArgs): Promise<FileList> {
	return new Promise<FileList>((resolve, reject) => {
		args = Object.assign({
			multiple: true,
			maxSize: -1,
			actionType: UploadActionType.All
		}, args || {});

		let actionType = args.actionType;
		// BI-40192 移动端qq、微信、firefox这几个没法同时选文件和打开相机，其他都可以，这几个地方弹出自定义菜单，其他都走系统的，避免二次弹出菜单
		if (browser.android && actionType === UploadActionType.All && (browser.qq || browser.weixin || browser.wxapp || browser.firefox)) {
			showMobileUploadMenu(args);
			return;
		}

		// 这里生成上传文件的dom
		let domFiles = globalUploadFileInput;
		if (!domFiles) {
			/**
			 * 20190325 guob ISSUE:BI-25048 
			 * 问题原因：chrome最新版本对上传文件的限制更严了，iframe中input必须创建后才能上传
			 * 解决方案：直接用HTML5的方式上传
			 */
			domFiles = globalUploadFileInput = document.createElement('input');
			domFiles.type = "file";
			/**
			 * 2020628 guob
			 * 在win10以下的操作系统的IE浏览器中如果没有将input节点append到body下，那么无法domFiles.click()来打开文件选择对话框
			 */
			if (browser.msie && (browser.os === OperatingSystem.Win7 || browser.os === OperatingSystem.WinXP)) {
				domFiles.style.display = 'none';
				document.body.appendChild(domFiles);
			}
		}
		domFiles.value = "";
		domFiles.multiple = !!args.multiple;
		domFiles.removeAttribute('capture');

		/*
		 * https://jira.succez.com/browse/CSTM-13132 小程序中上传附件支持拍照和选取相册图片
		 *
		 * 
		 * 测试结果：
		 *  ios设备：
		 *  在ios设备通过`accept`属性就可以控制拍照、录像、选取相册图片等菜单，基本符合需求。
		 *  在ios设备指定`capture`属性为`camera`时，会直接打开相机而无法选取相册中的图片。
		 * 
		 *  安卓设备：
		 *  1. chrome浏览器
		 *     accept='image/*'+capture='camera' 直接进入到相机
		 *     其他情况下：相机+文件
		 * 
		 * 2. 微信浏览器：
		 *    accept='image/*'+capture='camera' 直接进入到相机
		 *    accept='image/*'  相机+文件
		 *    其他情况：只能选取文件
		 * 
		 * 3. 微信小程序：
		 *    accept='image/*'+capture='camera'+multiple='false' 直接进入到相机
		 *    accept='image/*'+capture='camera'+multiple='true' 相机+文件
		 *    其他情况：只能选取文件
		 * 
		 * 4. 从QQ打开需要设置capture='camera'，否则只能选取文件
		 * 
		 * 5. QQ浏览器，华为浏览器，firefox等浏览器设置capture='camera'后直接进入到相机，所以不能设置capture='camera'
		 * 
		 * 6. chrome和微信可以直接进入到相册外，其他都会弹出菜单：文件/图片+拍照/相机
		 * 
		 * 处理规则：
		 *  1. 在ios设备直接通过`accept`就可以弹出合理的菜单，不需要设置`capture`属性
		 *  2. 在安卓微信小程序环境和从QQ打开时，设置`accept`和`camera`
		 *  3. 在安卓微信浏览器环境，设置`accept`
		 *  4. 微信小程序中，设置accept='image/*'+capture='camera'+multiple='true'
		 * 
		 * 参考：https://blog.csdn.net/cvper/article/details/89792401
		 * 
		 * XXX 通过H5原生属性无法兼容所有设备，而微信小程序接口也不够完善，以后考虑组合使用。
		 */
		let getAcceptMimeTypes = (types: string[]) => {
			return types?.map(type => {
				type = type.toLowerCase();
				let m = MIME_TYPES[type];
				return m?.accept || m?.mime || type;
			}).distinct().join(',') || ''
		}
		let fileTypes = args.fileTypes;
		// 下面是针对移动端：文件+摄像头，直接进入到相机这两种情况的，如果只是选择文件，可以不用进行下面的设置，直接设置accept即可
		if (browser.mobile && actionType === UploadActionType.Camera) {// accept='image/*'+capture="camera"才能打开相机
			// TODO 目前只有从QQ直接打开时会弹出文件或相机的选择，其他基本上都会直接打开相机，需要处理从QQ直接打开的情况
			domFiles.accept = 'image/*';
			domFiles.setAttribute('capture', 'camera'); // accept='image/*'+capture='camera'后基本上都会直接进入到相机
			browser.wxapp && (domFiles.multiple = false); // 微信小程序在上面基础上还必须设置multiple='false'才能直接进入到相机
		} else if (browser.mobile && actionType === UploadActionType.Image) { // 直接打开图库的话，accept="image/*"会有选相册和相机的菜单，所以accept必须是'image/png,image/jpg'这样的，firefox下无法直接打开图库，会有二级菜单项
			domFiles.accept = getAcceptMimeTypes(isEmpty(fileTypes) ? IMAGE_FILETYPE : fileTypes.filter(type => IMAGE_FILETYPE.includes(type)));
		} else {// 1. pc， 2. ios弹出菜单，3. 安卓的qq,微信，firefox直接打开文件，其他弹出系统菜单
			let accept = domFiles.accept = getAcceptMimeTypes(fileTypes);
			// 如果没有设置fileType但是指定上传图片，此时需要设置accept='image/*'，否则所有文件都能上传
			isEmpty(accept) && args.isImage && (domFiles.accept = 'image/*');
		}

		/**
		 * 先触发的window的focus，后触发onchange。如果选中了图片，在触发window的focus时domFiles还没有文件内容，
		 * 此时没办法判断是否选中了文件，只有等待触发onchange后才知道。
		 * 在focus时设置一个Timeout并记录下timer，如果触发了onchange，那么clearTimeout，这样不触发oncancel，
		 * 如果等待500ms也没有clearTimeout，那么说明是取消了上传，此时触发oncancel
		 */
		let cancelTimer: number;
		domFiles.onchange = () => {
			let count = domFiles.files.length;
			if (browser.wxapp && !args.multiple && count > 1) {
				showMessageBox({ content: message('commons.upload.single') });
				return;
			}
			cancelTimer > 0 && count && clearTimeout(cancelTimer);
			cancelTimer = 0;
			args.onlyOpenFile ? resolve(domFiles.files) : import("commons/upload").then((m) => {
				m.bindUpload(domFiles, args);
				/**
				* 在上传文件时总是要清理数据参数，避免上次上传的参数影响到了本次上传
				* win10 IE11下，如果domFiles有onchange，因为清理参数将value清空，此时触发了domFiles的onchange（其他浏览器没有），
				* 选中文件并上传时value改变了，又触发了一次onchange，导致触发两次onchange，这样不妥，
				* 每次上传完成后都将onchange置空，避免在两次onchange的问题
				*/
				domFiles.onchange = null;
				resolve(domFiles.files)
			});
		};
		/**
		 * 目前只能在onclick中给window来添加focus事件，通过在focus中判断文件数据，如果数量为0，那么表示取消上传
		 */
		domFiles.onclick = args.oncancel ? () => {
			let checkCancelUploadHandler = (event: Event): void => {
				if (browser.ios) {// ios中不是即时触发的touchstart所以无需等待
					domFiles.files.length == 0 && args.oncancel && args.oncancel({ srcEvent: event });
					window.removeEventListener('touchstart', checkCancelUploadHandler);
				} else { // 非ios会即时触发window.focus，但是此时无法知道选中了文件，需要等待才知道
					/**
					 * 在win7上500ms有可能还是不够（极少情况），即先触发了oncancel，后又触发了oncchange，，所以这里还是轮询几次，
					 * 尽量等待onchange先触发，但是这里的等待时间不能太长，因为取消上传是不会触发onchange的，就会一直等待到轮询结束，
					 * 所以设置为2000ms也差不多了，如果2000ms也没法触发onchange，那么也真是没办法了。。。
					 */
					let count = 0;
					let loopCheckCancel = () => {
						cancelTimer = setTimeout(() => {
							if (cancelTimer > 0 && domFiles.files.length == 0) {
								++count > 3 ? args.oncancel({ srcEvent: event }) : loopCheckCancel();
							}
						}, 500);
					}
					loopCheckCancel();
					window.removeEventListener('focus', checkCancelUploadHandler);
				}
			};
			// ios中不会触发window.focus，所以只能触发window.touchstart来检查是否cancel
			window.addEventListener(browser.ios ? 'touchstart' : 'focus', checkCancelUploadHandler);
		} : null;
		domFiles.click();
	});
}
