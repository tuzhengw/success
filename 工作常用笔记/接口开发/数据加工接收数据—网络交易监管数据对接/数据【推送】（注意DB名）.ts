/**
 * ============================================================
 * 作者:tuzhengw
 * 审核人员：
 * 创建日期：2021-12-27
 * 功能描述：
 *      该脚本主要是用于【网络交易监管数据对接接口开发】
 * 
 * 20220808 tuzw
 * 问题：网络交易监测线索处置反馈信息推送
 * https://jira.succez.com/browse/CSTM-19957
 * 
 * IDEA反编译jar包：https://blog.csdn.net/weixin_30247781/article/details/101931298
 * Maven jar包下载：https://mvnrepository.com/artifact/com.alibaba/fastjson/1.2.59
 * ============================================================
 */
import HttpClients = org.apache.http.impl.client.HttpClients;
import EntityUtils = org.apache.http.util.EntityUtils;
import HttpGet = org.apache.http.client.methods.HttpGet;
import File_ = java.io.File; // 工具类不需要对象，直接import导入

const GdGenericRequest = com.gongdao.yuncourt.security.model.GdGenericRequest; // 通过jar包内部源码查看
const GdApiClient = com.gongdao.yuncourt.security.GdApiClient;
const GdGenericResponse = com.gongdao.yuncourt.security.model.GdGenericResponse;
const GdApiException = com.gongdao.yuncourt.security.exception.GdApiException;
const GDClientV2Impl = com.gongdao.yuncourt.security.impl.GDClientV2Impl;
const JSONObject = com.alibaba.fastjson.JSONObject;
const JSONArray = com.alibaba.fastjson.JSONArray;
const JSON_ = com.alibaba.fastjson.JSON;  // 避免与前端JSON对象冲突
import LocalDateTime = java.time.LocalDateTime;
import LocalTime = java.time.LocalTime
import ZoneOffset = java.time.ZoneOffset;

import metadata from 'svr-api/metadata';
import { queryData, getDwTable } from "svr-api/dw";
import { uuid } from "svr-api/utils";
import { getDataSource } from "svr-api/db"; //数据库相关API
import { getFile } from "svr-api/fs";
import { getWorkDir } from "svr-api/sys";


/** 每次发送数量【对方接口最大为：1000】 */
const SEND_NUMS: number = 1000;
/** 应用秘钥 */
const APP_KEY: string = "saba";
/** 正式环境标记秘钥 */
const SIGN_KEY: string = "xxx";
/** 正式环境数据秘钥 */
const DATA_KEY: string = "xxxx";
/** 正式环境租户秘钥 */
const SECURITY_TENANT_ID: string = "xxx";

/** 测试应用秘钥 */
const TEST_APP_KEY: string = "savo";
/** 测试环境标记秘钥 */
const TEST_SIGN_KEY: string = "xxxx";
/** 测试环境数据秘钥 */
const TEST_DATA_KEY: string = "xxx";
/** 测试环境租户秘钥 */
const TEST_SECURITY_TENANT_ID: string = "xxx";

/** 推送记录表 */
const SEND_RECORD = "SEND_RECORD";
/** 数据源名 */
const DATA_SOURCE_DATA = "sjzt_qzj";
const SCHEM = "SJCK_SJJH";
/** 数据源 */
const dataSource = getDataSource(DATA_SOURCE_DATA);

/**
 * 读取【推送配置表】信息，循环【推送配置的表】数据
 * 配置表路径：/ZHJG/data/tables/网络交易/SEND_RECORD.tbl
 */
export function sendConfigTable(): void {
    print(`start----读取【推送配置表】信息，循环【推送配置的表】数据`);
    let sendRecords: SendRecord[] = getSendRecords();
    if (!sendRecords || sendRecords.length == 0) {
        print(`end----推送配置信息表无数据，推送结束`);
        return;
    }
    print(`当前共需要推送：${sendRecords.length} 张表数据`);
    let startTime: number = new Date().getTime();
    for (let i = 0; i < sendRecords.length; i++) {
        print(`------------------------------------------------------------------------------------------------`);
        let isStop: string = metadata.getString("/sdi/script/sendWebExchangeData/isStopExecute.txt");
        if (isStop == "true") {
            print(`手动停止推送，当前处于第【${i + 1}】推送，共需要请求次数为：【${sendRecords.length}】`);
            break;
        }
        let sendRecord: SendRecord = sendRecords[i];
        let modelPath = sendRecord.MODEL_PATH.trim();
        print(`${i + 1}. 当前处理模型：${modelPath}，表名：${sendRecord.TABLE_NAME}，类型为：${sendRecord.SEND_TYPE}`);
        if (!modelPath) {
            print(`模型：${sendRecord.TABLE_NAME} 的模型路径不存在，推送结束--end`);
            break;
        }
        if (sendRecord.IS_RUN == 0) {
            updateRowValue(SEND_RECORD, "IS_RUN", 1, "string", "ID", sendRecord.ID); // 更新状态，开始推送
            let filterCondtionResult = getQueryFilterCondition({
                modelPath: sendRecord.MODEL_PATH,
                tableName: sendRecord.TABLE_NAME,
                sendType: sendRecord.SEND_TYPE,
                lastSendTime: sendRecord.LAST_DEAL_TIME,
                filterColumn: sendRecord.FILTER_COLUMN,
                pkValue: sendRecord.ID,
                customFilterCondition: sendRecord.CUSTOM_FILTER_CONDITION
            });
            if (!filterCondtionResult.result) {
                print(`获取查询过滤条件失败`);
                break;
            }
            let filterCondition: FilterInfo[] = filterCondtionResult.data;
            /** 记录当前处理模型表的物理字段名集 */
            let dbPhysicalFields: Array<string> = [];
            let dbPirmaryKey: Array<string> = [];
            try {
                let queryModelFields = getQueryModelFileds(`${modelPath}`, dbPhysicalFields, dbPirmaryKey);
                if (queryModelFields.length == 0) {
                    print(`当前${modelPath}查询的字段为NULL`);
                    break;
                }
                let queryInfo: QueryInfo = {
                    fields: queryModelFields,
                    select: true,
                    sources: [{
                        id: "model1",
                        path: `${modelPath}`
                    }],
                    filter: filterCondition,
                    options: {
                        limit: 10,
                        offset: 0,
                        queryTotalRowCount: true
                    }
                }
                let sendResult: boolean = sendData(queryInfo, sendRecord.DATA_TYPE, dbPhysicalFields, dbPirmaryKey);
                if (sendResult) {
                    print(`【${sendRecord.MODEL_PATH}】，发送成功`);
                }
            } catch (e) {
                print(`【${modelPath}】推送数据失败`);
                print(e);
            } finally {
                updateRowValue(SEND_RECORD, "IS_RUN", 0, "string", "ID", sendRecord.ID); // 更新状态【推送完成】
            }
            print(`第【${i + 1}】个模型推送完成：${modelPath}模型，表名：${sendRecord.TABLE_NAME}，描述为：${sendRecord.DESC}，类型为：${sendRecord.SEND_TYPE}`);
        }
    }
    let endTime: number = new Date().getTime();
    print(`end-----处理推送表配置，耗时：${(endTime - startTime) / 1000}S`);
}

/**
 * 数据推送
 * @param queryInfo Query查询过滤条件
 * @param sendDataType 推送数据类型
 * @param modelDbFields 查询表的物理字段名
 * @param dbPrimaryFields 查询表的物理表主键集
 * @return true | false
 */
function sendData(queryInfo: QueryInfo, sendDataType: number, modelDbFields: Array<string>, dbPrimaryFields: Array<string>): boolean {
    print(`start----sendData()，开始推送数据`);
    let totalRowCount: number = queryData(queryInfo).totalRowCount; // 先发一次请求，获取当前模型表【数据总量】
    queryInfo.options.queryTotalRowCount = false; // 后续不需要获取【数据总量】，设置为：false
    if (!totalRowCount) {
        print(`end-----数据为NULL，不需要发送`);
        return false;
    }
    let dealCount: number = Math.ceil(totalRowCount / SEND_NUMS);
    print(`数据总量为：${totalRowCount}条，需要处理${dealCount}次，一次处理${SEND_NUMS}条数据`);
    if (dealCount > 0) {
        queryInfo.options.limit = SEND_NUMS; // 分页：每页的数据行数
        for (let i = 0; i < dealCount; i++) {
            let isStop: string = metadata.getString("/sdi/script/sendWebExchangeData/isStopExecute.txt");
            if (isStop == "true") {
                print(`手动停止推送，当前推送信息`);
                print(queryInfo);
                break;
            }
            if (i == 0 || i % 10 == 0) {
                print(`（${i + 1}）开始第【${i + 1}】次请求，总请求次数为【${dealCount}】---`);
            }
            queryInfo.options.offset = i * SEND_NUMS; // 从第几行开始
            let data: (string | number | boolean)[][] = queryData(queryInfo).data;
            let legalJsonData = arrToJsonArrs(data, modelDbFields);
            let responseResult = requestInterfaceSendData(PUSH_INTERFACE, sendDataType, legalJsonData, dbPrimaryFields);
            if (!!responseResult && !responseResult["success"]) {
                print(`推送失败----Error`);
                break;
            }
            print(`---${JSON.stringify(responseResult)}`);
        }
    }
    print(`end----sendDat()，推送数据，一共推送${totalRowCount}条数据，共推送【${dealCount}】次`);
    return true;
}

/** 推送接口 */
const PUSH_INTERFACE = "/gdopenapi/saba/third_data/push";

/**
 * 调用第三方接口，将【数据推送】给第三方平台
 * @param requestPath 请求地址
 * @param dataType 推送数据类型
 * @param legalData 已经处理好的JSON数据【字段以我们平台为主】
 * @param primaryFileds 推送表的物理主键集
 * @return { "resultCode":"1000001011","resultMessage":"decrypt request error","success":false }
 * 
 *  let request = new JSONObject();
 *  request.put("securityTenantId", "71TfMh-ft1kt5xkJt0upyQ");
    request.put("type", "tag_subject_seller");
    request.put("source", "saisi");

    let sendDatas = new JSONArray();
    let singleData = new JSONObject();
    singleData.put("pk", "primaryKey");

    let dataCotent = new JSONObject();
    dataCotent.put("name", "name");
    singleData.put("content", JSON.toJSONString(dataCotent));
    sendDatas.add(singleData);

    request.put("data", sendDatas);
 */
function requestInterfaceSendData(requestPath: string, sendDataType: number, legalData, primaryFileds: Array<string>): unknown {
    if (!legalData) {
        return { result: true, message: "推送数据为NULL" };
    }
    let resultInfo: ResultInfo = { result: true };
    let request = new JSONObject();
    request.put("securityTenantId", SECURITY_TENANT_ID);
    request.put("type", `${sendDataType}`); // // request.put("type", "tag_subject_seller");
    request.put("source", "saisi");
    let sendDatas = new JSONArray();
    for (let i = 0; i < legalData.length; i++) { // 考虑到对方接口限制，这里需要使用fastJson来转换JSON数组
        let singleData = new JSONObject();
        singleData.put("pk", `${legalData[i][`${primaryFileds[0]}`]}`); // 考虑到对方接口限制，仅传递第一个主键
        delete legalData[i][`${primaryFileds[0]}`]; // 去掉主键字段

        let keys = Object.keys(legalData[i]); // 单条数据内容【除主键】
        let dataContent = new JSONObject();
        for (let key = 0; key < keys.length; key++) { // eg： dataCotent.put("name", "name");
            if (!legalData[i][keys[key]]) {
                continue;
            }
            dataContent.put(`${keys[key]}`, `${legalData[i][`${keys[key]}`]}`);
        }
        singleData.put("content", JSON_.toJSONString(dataContent));
        sendDatas.add(singleData);
    }
    request.put("data", sendDatas);

    let gdGenericRequest = new GdGenericRequest(requestPath, request);
    gdGenericRequest.setAppMessageId(uuid());

    let gdApiClient = new GdApiClient(APP_KEY, SIGN_KEY, DATA_KEY);
    gdApiClient.setHost(GdApiClient.ONLINE_HOST_URL); // ONLINE_HOST_URL | SANDBOX_HOST_URL
    gdApiClient.setEncryptRequest(true);
    gdApiClient.setDecryptResponse(true);

    let gdGenericResponse = new GdGenericResponse();
    gdGenericResponse = gdApiClient.execute(gdGenericRequest);
    return JSON.parse(gdGenericResponse);
}

/** 
 * 获取查询的过滤条件
 * @description 注意表所在的数据源和schema可能不一样
 * @param args.modelPath 模型路径
 * @param args.tableName 推送物理表名
 * @param args.sendType 推送类型【全量 | 增量】
 * @param args.lastSendTime 上次库中【增量字段】最大值
 * @param args.filterColumn 增量字段名
 * @param args.pkValue 【增量字段】对应主键值
 * @param args.customFilterCondition 自定义过滤条件，eg：model1.id = '123'
 * @reutnr { result: true, data: [{exp: "model1.CREATE_DATE" > "2021-12-12 00:00:00"}] }
 */
function getQueryFilterCondition(args: {
    modelPath: string,
    tableName: string,
    sendType: string,
    lastSendTime: Date,
    filterColumn: string,
    pkValue: string,
    customFilterCondition: string
}): ResultInfo {
    print(`start----getQueryFilterCondition()，获取查询的过滤条件`);
    let { dbSourceName, schemName } = getDbSourceAndSchemName(args.modelPath);
    if (dbSourceName == '') {
        print(`未获取到模型：${args.modelPath} 的数据源名，结束执行`);
        return { result: false };
    }
    let filterInfos: FilterInfo[] = [];
    if (args.sendType == "增量") {
        let lastMaxUpdateTime: string = getMaxTime(dbSourceName, args.tableName, args.filterColumn, schemName); // 更新当前库最大的时间
        if (lastMaxUpdateTime == "") {
            print(`当前获取的最大时间类型不支持`);
            return { result: false };
        }
        print(`【${args.tableName}】推送方式为：增量，上次库中最大【增量时间字段值】为：【${args.lastSendTime}】`);
        updateRowValue(SEND_RECORD, "LAST_DEAL_TIME", new Date(lastMaxUpdateTime), "Date", "ID", args.pkValue);
        if (!!args.lastSendTime) {
            if (!args.filterColumn) {
                print(`【${args.tableName}】未给定增量时间字段，请检查【配置表：${SEND_RECORD}】`);
                return { result: false };
            }
            filterInfos.push({ exp: `model1.${args.filterColumn} > '${args.lastSendTime}'` });
        }
    }
    if (!!args.customFilterCondition) { // 校验是否有指定自定义过滤条件
        print(`当前模型给定的自定义过滤条件为：${args.customFilterCondition}`);
        filterInfos.push({ exp: args.customFilterCondition });
    }
    print(`end----获取查询条件完成`);
    return { result: true, data: filterInfos };
}

/**
 * 获取推送信息
 * 模型地址：/sdi/data/tables/etl/数据推送/网络交易/SEND_RECORD.tbl
 * @param tableName 物理表名
 * @param schem
 * @return eg: { "1", "模型名", "模型路径", ... }
 */
export function getSendRecords(): SendRecord[] {
    let conn = dataSource.getConnection();
    let queryData: SendRecord[] = [];
    try {
        let table: TableData;
        let schem: string = dataSource.getDefaultSchema();
        let querySQL: string = `select * from SJCK.SEND_RECORD ORDER BY SEND_TYPE ASC`;
        queryData = dataSource.executeQuery(querySQL) as SendRecord[];
    } catch (e) {
        print(`获取推送表信息失败--error--`);
        print(e);
    } finally {
        conn.close();
    }
    return queryData;
}

/**
 * 根据主键更新某表【单个字段】信息
 * eg：updateRowValue("SEND_RECORD", "IS_RUN", 0, "ID", "1");
 * @param tableName 物理表名
 * @param updateColumnName 更新字段名
 * @param updateValue 更新值
 * @param fieldType 字段类型
 * @param primaryKey 主键字段
 * @return true | false
 */
function updateRowValue(tableName: string, updateColumnName: string, updateValue: unknown, fieldType: string, primaryKey: string, primaryKeyValue: unknown, schem?: string): boolean {
    let conn = dataSource.getConnection();
    try {
        conn.setAutoCommit(false);
        let table: TableData;
        if (!!schem) {
            table = dataSource.openTableData(tableName, schem, conn);
        } else {
            table = dataSource.openTableData(tableName, "", conn);
        }
        let updateSql = `UPDATE ${tableName} SET ${updateColumnName}=? WHERE ${primaryKey}=?`;
        if (fieldType == "Date") { // 若是格式化日期，则需要使用：to_date 
            /**
             * 更新调用时间
             * oracle插入数据报：not a valid month |  literal does not match format string
             * 解决办法一：UPDATE SEND_RECORD SET LAST_DEAL_TIME = to_date('2019-2-26 11:14:25', 'yyyy-mm-dd hh24:mi:ss') WHERE ID='1'
             * 解决办法二（推荐）：ew java.sql.Timestamp(Date.now())  
             */
            print(`---当前更新的字段为：时间字段，需要转换为：java.sql.Timestamp`);
            updateValue = new java.sql.Timestamp(updateValue);
        }
        table.executeUpdate(updateSql, [updateValue, primaryKeyValue]);
        conn.commit();
    } catch (e) {
        print(`--更新物理表【${tableName}】错误--error--`);
        print(e);
    } finally {
        conn.close();
    }
    return true;
}

/**
 * 获取某表指定【时间字段】最大的时间
 * @param dbSouceName 数据源名
 * @param tableName 表名
 * @param timeColumName 时间字段名
 * @parma schem
 * @return eg："2021-12-26 06:43:49"
 */
export function getMaxTime(dbSouceName: string, tableName: string, timeColumName: string, schem: string): string {
    let dbSource = getDataSource(dbSouceName);
    let conn = dbSource.getConnection();
    let maxTimes: string = "";
    try {
        let table: TableData;
        if (!schem) {
            schem = dbSource.getDefaultSchema();
        }
        table = dbSource.openTableData(tableName, schem, conn);
        let selectMaxDateSQL: string = `select MAX(${timeColumName}) as MAXDATE from ${schem}.${tableName}`;
        print(`获取表：${tableName} 指定【时间字段】最大的时间`);
        print(selectMaxDateSQL);
        let maxDate = table.executeQuery(selectMaxDateSQL);
        if (maxDate != null) {
            let queryDateType: string = maxDate[0]["MAXDATE"].getClass();   // 注意：版本不同，查出来的结果可能是：字符串 或 时间类型 class java.sql.Timestamp
            if (queryDateType == "class java.lang.String") {
                maxTimes = formatDate(new Date(maxDate[0]["MAXDATE"]));
            }
            if (queryDateType == "class java.sql.Timestamp") {
                maxTimes = formatDate(new Date(maxDate[0]["MAXDATE"]));
            }
            if (queryDateType == "class java.sql.Date") {
                maxTimes = formatDate(maxDate[0]["MAXDATE"]);
            }
            print(`-----当前【物理表】中数据最大时间字段${timeColumName}值为：${maxTimes}`);
        }
    } catch (e) {
        print(`--获取【${tableName}】指定【时间字段】最大的时间，error--`);
        print(e);
    } finally {
        conn.close();
    }
    return maxTimes;
}

/**
 * 根据模型路径获取对应的数据源名和schem
 * @param modelPath 模型路径
 * @return 
 */
export function getDbSourceAndSchemName(modelPath: string): { dbSourceName: string, schemName: string } {
    let model: DwTableInfo = getDwTable(modelPath);
    if (!model) {
        return { dbSourceName: "", schemName: "" };;
    }
    let dataSource: string = model.properties && model.properties.datasource as string;
    let schema: string = model.properties && model.properties.dbSchema as string;
    if (dataSource == "defdw") { // 默认数据源在配置文件中名字不同，需要校验
        dataSource = getDefaultDbSourceName();
    }
    print(`【${modelPath}】的所在数据源名为：${dataSource}，schema：${schema}`);
    return { dbSourceName: dataSource, schemName: schema };
}

/**
 * 获取某模型表【查询的字段】信息
 * 
 * @description 通过dw模块获取
 * @param modelPath 模型表路径
 * @param dbPhysicalFields 【记录】模型【物理字段名】数组，用于数组转换为JSON
 * @param dbPirmaryKey 【记录】表物理名主键
 * @param ignoredFields 忽略的【字段名】数组
 * @return 查询字段格式，eg：
 * [{ 
 *      name: "企业名称", 
 *      exp: "model1.ENTERPRISE_NAME" 
 * }]
 */
export function getQueryModelFileds(modelPath: string, dbPhysicalFields?: Array<string>, dbPirmaryKey?: Array<string>, ignoredFields?: Array<string>): QueryFieldInfo[] {
    if (!modelPath) {
        console.debug(`getTableFieldInfo()，获取模型【${modelPath}】物理表字段失败，原因：modelPath参数错误`);
        return [];
    }
    let modelMetaInfo: string = metadata.getString(modelPath);
    if (!modelMetaInfo) {
        return []
    }
    let modelInfos: DwTableInfo = JSON.parse(modelMetaInfo);
    let modelProperties: DwTablePropertiesInfo = modelInfos.properties;
    let modelPrimary: string[] = modelProperties.primaryKeys; // class java.util.ArrayList
    if (!modelPrimary) {
        print(`-- -【${modelPath}】该模型没有主键，无法推送`);
        return [];
    }
    let dimensions: FieldInfo[] = modelInfos.dimensions as FieldInfo[];
    let queryFields: QueryFieldInfo[] = [];
    let recordFieldDescName: string[] = [];
    try {
        for (let i = 0; i < dimensions.length; i++) {
            let modelFieldInfo = dimensions[i];
            let dbfield: string = modelFieldInfo.dbfield;
            if (modelFieldInfo.name == '时间戳') {
                continue;
            }
            if (!!dbPirmaryKey && !!modelPrimary && modelPrimary.includes(modelFieldInfo.name)) {
                dbPirmaryKey.push(dbfield);
            }
            if (!!dbPhysicalFields) {
                dbPhysicalFields.push(dbfield);
            }
            let fieldDescName: string = modelFieldInfo.name;
            if (recordFieldDescName.includes(fieldDescName)) { // 校验描述是否重复
                fieldDescName = `${fieldDescName}_${i}`;
            }
            let queryField: QueryFieldInfo = {
                name: fieldDescName,
                exp: `model1.${dbfield}`
            }
            recordFieldDescName.push(fieldDescName);
            queryFields.push(queryField);
        }
    } catch (e) {
        print(`通过模型路径【${modelPath}】获取物理表字段信息失败--error`);
        print(e.toString());
    }
    return queryFields;
}

/**
 * 获取当前项目设置的默认数据源名
 * @description defdw 为当前项目设置的默认数据源名
 */
function getDefaultDbSourceName(): string {
    print("getDefaultDbSourceName()：获取当前项目设置的默认数据源名");
    let settingPath: string = "/ZHJG/settings/settings.json";
    let projectSetting: string = metadata.getString(settingPath);
    let defalutDbName: string = JSON.parse(projectSetting).data.analysisDatasource;
    return defalutDbName;
}

/**
 * 格式化时间戳
 * @parmas dates 需要格式化的时间
 * @return 格式化后的日期，2021-12-12 00:00:00 
 */
function formatDate(dates: Date): string {
    let Year: number = dates.getFullYear();
    let Months = (dates.getMonth() + 1) < 10
        ? '0' + (dates.getMonth() + 1)
        : (dates.getMonth() + 1);
    let Day = dates.getDate() < 10
        ? '0' + dates.getDate()
        : dates.getDate();
    let Hours = dates.getHours() < 10
        ? '0' + dates.getHours()
        : dates.getHours();
    let Minutes = dates.getMinutes() < 10
        ? '0' + dates.getMinutes()
        : dates.getMinutes();
    let Seconds = dates.getSeconds() < 10
        ? '0' + dates.getSeconds()
        : dates.getSeconds();
    return Year + '-' + Months + '-' + Day + ' ' + Hours + ':' + Minutes + ':' + Seconds;
}

/**
 * 将二维数组转换为JSON数组（注：数据与物理字段对应）
 * @param queryData 查询模型表数据返回的【二维数组】
 * @param modelDbFields 模型物理字段集
 * @return JSON数组，eg：[{"age": "13"}]
 */
function arrToJsonArrs(queryData: (string | number | boolean)[][], modelDbFields: Array<string>): unknown {
    if (!queryData || !modelDbFields) {
        print(`-----arrToJsonArrs()：二维数组转换为JSON数组，【参数错误】`);
        return [];
    }
    if (queryData[0].length != modelDbFields.length) {
        print(`-----arrToJsonArrs()：二维数组转换为JSON数组，【行数据个数 与 物理字段不是一一对应】`);
        return [];
    }
    let resultData = [];
    for (let i = 0; i < queryData.length; i++) {
        let temp = {};
        for (let j = 0; j < queryData[i].length; j++) {
            if (queryData[i][j] == null || queryData[i][j] == undefined) { // 若列数据没有值，则跳过
                continue;
            }
            temp[`${modelDbFields[j]}`] = queryData[i][j];
        }
        resultData.push(temp);
    }
    return resultData;
}

/**
 * 校验是否为指定日期格式
 * @param times 格式化后的日期字符串
 * @return true | false
 */
function checkDateFormat(times: string): boolean {
    let checkPatten = new RegExp(/^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}$/);
    if (checkPatten.test(times)) {
        return true;
    } else {
        return false;
    }
}

/** 推送数据对象 */
interface SendFormat {
    /** 主键 */
    pk: string;
    /** 行数据（JSON格式字符串） */
    content: string;
}

/** 推送对象 */
interface SendRecord {
    /** 推送序号 */
    ID: string;
    /** 推送模型表名 */
    TABLE_NAME: string;
    /** 推送模型表路径 */
    MODEL_PATH: string;
    /** 上次推送时，库中最大【增量字段】值*/
    LAST_DEAL_TIME: Date;
    /** 数据类型 */
    DATA_TYPE: number;
    /** 数据类型描述 */
    DESC: string;
    /** 推送类型【全量 | 增量】 */
    SEND_TYPE: string;
    /** 是否正在推送，1：推送中，0：停用中 */
    IS_RUN: number;
    /** 增量时间字段 */
    FILTER_COLUMN: string;
    /** 自定义过滤条件 */
    CUSTOM_FILTER_CONDITION: string;
    [propname: string]: any;
}

/** 返回格式 */
interface ResultInfo {
    /** 执行结果 */
    result: boolean;
    /** 返回消息 */
    message?: string;
    /** 数据 */
    data?: any;
    /** 错误数据 */
    errorData?: any;
    [propname: string]: any;
}

/** 字段信息 */
interface FieldInfo {
    /** 字段描述 */
    name: string;
    /** 字段类型 */
    dataType: string;
    /** 字段长度 */
    length: number;
    /** 是否维项 */
    isDimension: boolean;
    /** 字段物理名 */
    dbfield: string;
}