import { IExpEvalDataProvider } from "commons/exp/expeval";
import { rc } from 'sys/sys';


/**
 * 将扫描的二维码信息加密编码 进行解密
 * @param context：SPG对象
 * @param encryptUrlInfo：RSA加密的URL信息
 * @return 解密信息
 */
export function ENCRYP_QYXX(context: IExpEvalDataProvider, encryptUrlInfo: string): Promise<any> | string {
    // 对URI 进行解码
    let decryptUrlInfo = decodeURI(encryptUrlInfo);
    // 去除解密中的特殊字符，eg：'%'
    let decryptUrlCodeInfo = decodeURIComponent(decryptUrlInfo);
    /**
     * 注意：URLEncode 中对 空格的编码 有 "+" 和 "%20" 两种
     * 参考网址：https://www.cnblogs.com/zhengxl5566/p/10783422.html
     */
    decryptUrlCodeInfo = decryptUrlCodeInfo.replaceAll(" ", "+");
    decryptUrlCodeInfo = decryptUrlCodeInfo.replaceAll("\\+", "%20");

    return rc({
        url: '/sysdata/extensions/succ-expressionFucntion-decode/main.action?method=dealWithCode',
        method: "POST",
        data: {
            decryptUrlCodeInfo: decryptUrlCodeInfo
        }
    }).then((result: string) => {
        if (result == '解密失败') {
            return result;
        }
        /**
         * result："{'UNISCID':'91420704MA49PY9C7P'}"
         * 因为不是一个合法的JSON字符串，先将单引号替换为双引号
         */
        let dealWithAfterResult = JSON.parse(result.replace(/'/g, '"'));
        return dealWithAfterResult['UNISCID'];
    })
}
