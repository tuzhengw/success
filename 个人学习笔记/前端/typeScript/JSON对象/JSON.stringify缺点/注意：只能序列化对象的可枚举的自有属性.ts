/**
 *      JSON.stringify()只能序列化对象的可枚举的自有属性，
 *      例如 如果obj中的对象是有构造函数生成的，则使用JSON.parse(JSON.stringify(obj))深拷贝后，
 * 会丢弃对象的constructor；
 */
function main() {
    var liai = new Person('liai');

    var test = {
        name: 'a',
        date: liai,
    };
    var copyed = JSON.parse(JSON.stringify(test));
    // { name: 'a', date: Person { name: 'liai' } }
    console.log(test);
    // { name: 'a', date: { name: 'liai' } }
    console.log(copyed);
}

function Person(name: any) {
    this.name = name;
    console.log(name)
}