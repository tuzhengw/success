/**
 * ============================================================
 * 作者: tuzhengw
 * 审核人员：liuyz
 * 创建日期：2021-12-21
 * 功能描述：
 *      该脚本主要是用于【根据配置的系统表JSON文件，同步当前平台系统表差异】
 * ============================================================
 */
import { getDataSource, getDefaultDataSource, getTableAlter, getTableCreator } from "svr-api/db";
import { getString, modifyFile } from 'svr-api/metadata';
import { getDwTable } from "svr-api/dw";
import { sleep } from "svr-api/sys";
const ds = getDefaultDataSource();

/** 项目根目录地址 */
let PROJECT_ROOT_PATH = '';
/**
 * 读取【表结构信息配置JSON】，同步表结构差异
 * @param projectName 当前项目的模块，eg：sysdata
 * @return { result: true }
 */
export function autoAdjustTableConfigInfo(request: HttpServletRequest, response: HttpServletResponse, params: { projectName: string }): ResultInfo {
	console.debug(`-------autoAdjustTableConfigInfo，读取【表结构信息配置JSON】，同步表结构差异，params：${params}------------start`);
	let start_time = new Date();
	let projectName: string = params.projectName;
	if (!projectName) {
		console.debug(`未获取当前的项目的模块名，初始化失败`);
		return { result: false, message: "未获取当前的项目的模块名，初始化失败" };
	}
	PROJECT_ROOT_PATH = `/${projectName}/app/label.app/`;

	let sysModelUpdateConf: Array<MetaConfigInfo> = JSON.parse(getString('./更新系统数据json.json'));
	if (!sysModelUpdateConf) {
		console.debug(`--autoAdjustTableConfigInfo，配置文件没有内容--`);
		return { result: false, message: `配置文件没有内容` };
	}
	let resultInfo: ResultInfo = { result: true };
	let modelCount: number = sysModelUpdateConf.length;
	let successModelPath: Array<string> = [];
	let noSuccessModelPath: Array<string> = [];

	for (let i = 0; i < modelCount; i++) {
		let modelPath = sysModelUpdateConf[i].tablePath;
		console.debug(`--------------------${i + 1}、开始处理【${modelPath}】表--------------------start`);
		if (!modelPath) {
			console.debug(`-------配置文件的第${i}个，没有指定模型表路径--`);
			continue;
		}
		let modelInfo: MetaConfigInfo = JSON.parse(getString(modelPath)); // 获取当前模型表的元数据信息
		if (!modelInfo) {
			successModelPath.push(modelPath);
			console.debug(`-------${modelPath}，不存在当前系统中--`);
			continue;
		}
		let modelInfoCopy: string = JSON.stringify(modelInfo); // 备份模型表信息，用于错误还原

		let needUpdateDbFields: Array<PhysicalColumn> = [];
		let needAddDbFeilds: Array<PhysicalColumn> = [];
		let filedIsChange: boolean = adjustModelFileds(modelInfo, sysModelUpdateConf[i].fields, needUpdateDbFields, needAddDbFeilds); // 同步最新字段信息

		let propertiesIsChange: boolean = adjustModelProperties(modelInfo, sysModelUpdateConf[i]); // 同步最新配置信息
		if (filedIsChange || propertiesIsChange) {
			if (updateModelAndDbStructure(modelPath, modelInfo, needUpdateDbFields, needAddDbFeilds, modelInfoCopy).result) {
				successModelPath.push(modelPath);
			} else {
				noSuccessModelPath.push(modelPath);
			}
		} else {
			console.debug(`-------【${modelPath}】表模型结构没有变化，无需更新【模型表结构】【物理表结构】--`);
			successModelPath.push(modelPath);
		}
	}
	if (noSuccessModelPath.length != 0) {
		resultInfo.result = false;
		resultInfo.message = `有部分数据存在问题，无法更新`;
		resultInfo.errorData = noSuccessModelPath;
	}
	resultInfo.successData = successModelPath;
	let end_time = new Date();
	let spendTime = end_time.getTime() - start_time.getTime();
	console.debug(`-------autoAdjustTableConfigInfo，读取【表结构信息配置JSON】，同步表结构差异，花费时间：${spendTime / 1000}秒------------end`);
	return resultInfo;
}

/**
 * 更新模型表和物理表
 * @parma modelPath 模型表路径
 * @param modelInfo 当前表的元数据信息对象
 * @param needUpdateDbFields 需要更新的物理表字段集
 * @param needAddDbFields 需要新增的物理字段集
 * @param modelInfoCopy 当前表的元数据信息对象【备份: 用于错误还原】
 * @return eg：{ result: true }
 */
function updateModelAndDbStructure(modelPath: string, modelInfo: MetaConfigInfo, needUpdateDbFields: Array<PhysicalColumn>, needAddDbFields: Array<PhysicalColumn>, modelInfoCopy: string): ResultInfo {
	let resultInfo: ResultInfo = { result: true };
	try {
		modifyFile(modelPath, { content: JSON.stringify(modelInfo) }); // 修改模型表信息
		if (needUpdateDbFields.length != 0 || needAddDbFields.length != 0) {
			let physicalTableName: string = modelInfo.properties.dbTableName;
			let schema: string = modelInfo.properties.dbSchema;
			let executeResult: ResultInfo = alterDbFields(physicalTableName, needUpdateDbFields, needAddDbFields, schema); // 修改物理表信息
			if (!executeResult.result) {
				console.debug(`-------物理表${physicalTableName}：更新错误，还原模型表--`);
				modifyFile(modelPath, { content: modelInfoCopy });
				resultInfo.result = false;
			}
		} else {
			console.debug(`-------【${modelPath}】表结构没有变化，无需更新【物理表结构】--`);
		}
		console.debug(`--------------------【${modelPath}】表结构【更新成功】--------------------end`);
	} catch (e) {
		resultInfo.result = false;
		console.error(`--【${modelPath}】，模型属性更新失败--`);
		console.error(e);
	}
	return resultInfo;
}

/**
 * 新增和修改模型表字段信息【dimensions】
 * @param dimTableInfo 当前表的元数据信息对象【直接对原对象修改】
 * @param fieldsInfo 传入的对应模型表的字段配置信息【ModelColumn】
 * @param needUpdateDbFields 【记录】需要更新的物理字段集
 * @param needAddDbFeilds 【记录】需要新增的物理字段集
 * @return 当前平台模型字段结构是否有更改【true
 */
function adjustModelFileds(dimTableInfo: MetaConfigInfo, fieldsInfo: Array<ModelColumn>, needUpdateDbFields: Array<PhysicalColumn>, needAddDbFeilds: Array<PhysicalColumn>): boolean {
	let isChange: boolean = false;
	if (!fieldsInfo) {
		print(`【${dimTableInfo.tablePath}】，没有配置字段【field】信息--`);
		return isChange;
	}
	let noCanAddField: Array<string> = [];
	for (let fieldsInfoIndex = 0; fieldsInfoIndex < fieldsInfo.length; fieldsInfoIndex++) {
		let fieldIsExist: boolean = false;
		let field: ModelColumn = fieldsInfo[fieldsInfoIndex];
		for (let mateInfoIndex = 0; mateInfoIndex < dimTableInfo.dimensions.length; mateInfoIndex++) {
			let dimension: ModelColumn = dimTableInfo.dimensions[mateInfoIndex];
			if (dimension.name == field.name) { // 比较【中文描述名】，若存在，则 以配置字段为主，更新已有的字段信息
				fieldIsExist = true;
				if (!field.exp // 计算字段不需要修改物理表
					&& (!!field.length && dimension.length != field.length) // 长度不一致
					|| (!!field.dbfield && field.dbfield != dimension.dbfield) // 物理字段名不一样
					|| (!!field.dataType && field.dataType != dimension.dataType) // 字段类型不一样
					|| (!!field.decimal && field.decimal != dimension.decimal) // 浮点数精度不一样
				) {
					let temp: PhysicalColumn = {
						oldFieldName: dimension.dbfield,
						fieldName: !!field.dbfield ? field.dbfield : dimension.dbfield,
						fieldLength: !!field.length ? field.length : dimension.length,
						fieldType: !!field.dataType ? field.dataType : dimension.dataType
					}
					if (field.dataType == 'N') { // 类型为浮点型才修改小数位数
						temp.fieldDic = !!field.fieldDic ? field.fieldDic : dimension.decimal
					} else {
						temp.fieldDic = null;
					}
					needUpdateDbFields.push(temp);
				}
				if (compareFieldInfoAndUpdate(dimension, field) && !isChange) { // 是否改变，仅记录一次
					isChange = true;
				}
				dimTableInfo.dimensions[mateInfoIndex] = dimension; // 将修改后的内容覆盖原来内容
				break;
			}
		}
		if (!fieldIsExist) { // 若字段不存在，则新增
			if (!!field.dbfield) {
				isChange = true;
				field.name = field.name.toLocaleUpperCase();
				dimTableInfo.dimensions.push(field);  // 新增字段信息

				if (!field.exp) {
					let temp: PhysicalColumn = {
						fieldName: field.dbfield.toLocaleUpperCase(),
						fieldType: field.dataType,
						fieldLength: field.length,
						fieldDic: !!field.fieldDic ? field.fieldDic : null,
						defvalue: !!field.defvalue ? field.defvalue : null,
						nullable: !!field.nullable ? field.nullable : true,
						fieldDesc: !!field.fieldDesc ? field.fieldDesc : null
					}
					needAddDbFeilds.push(temp);
				}
			} else {
				if (!!field.exp) { // 若是计算字段，则新增模型字段
					isChange = true;
					dimTableInfo.dimensions.push(field);
					print(`${field.name} 为计算字段`);
				} else {
					noCanAddField.push(field.name);
				}
			}
		}
	}
	if (noCanAddField.length != 0) { // 仅提示不合法的字段，符合的字段还是新增
		print(`字段：【${noCanAddField.join(",")}】没有包含dbfield属性，无法新增--`);
	}
	if (needUpdateDbFields.length != 0 || needAddDbFeilds.length != 0) {
		isChange = true;
	}
	return isChange;
}


/**
 * 对比某字段【新|旧】信息差异，并同步最新字段信息
 * @param oldFiled 原字段信息对象【直接对原对象修改】
 * @param newField 新字段信息对象
 * @return 字段是否有执行更新操作【true
 */
function compareFieldInfoAndUpdate(oldFiledInfo: ModelColumn, newFieldInfo: ModelColumn): boolean {
	let isChange: boolean = false;
	/** 获取需要更新的所有属性【key */
	let fieldPropertyName: Array<string> = Object.keys(newFieldInfo);
	let length: number = fieldPropertyName.length;
	for (let i = 0; i < length; i++) {
		let key: string = fieldPropertyName[i];
		if (oldFiledInfo[`${key}`] != newFieldInfo[`${key}`]) {
			isChange = true;
			oldFiledInfo[`${key}`] = newFieldInfo[`${key}`]; // eg：oldFiled.name = newField.name;
		}
	}
	return isChange;
}

/**
 * 新增和修改模型表的配置文件【properties】|【 hierarchies 】
 * @param dimTableInfo 当前表的元数据信息对象【直接对原对象修改】
 * @parma tableconf 最新的元数据信息对象
 * return 当前模型配置信息是否有更改【true
 */
function adjustModelProperties(modelInfo: MetaConfigInfo, tableConf: MetaConfigInfo): boolean {
	let isChange: boolean = false;
	if (!modelInfo.properties) {
		modelInfo.properties = {};
	}
	let needUpdateKeys: Array<string> = Object.keys(tableConf);  // ['field', 'primaryKeys', 'modelFilter', 'scriptMadelPath', ...]
	for (let i = 0; i < needUpdateKeys.length; i++) {
		let key = needUpdateKeys[i];
		if (JSON.stringify(modelInfo.properties[`${key}`]) != JSON.stringify(key)) { // 若新 | 旧属性不一致，则更新
			switch (key) {
				case "primaryKeys":
					modelInfo.properties.primaryKeys = tableConf[`${key}`];
					isChange = true;
					break;
				case "modelFilter":
					modelInfo.properties.dataFilter = tableConf[`${key}`];;
					isChange = true;
					break;
				case "scriptName":
					modelInfo.properties.assignScriptPath = true;
					modelInfo.properties.scriptName = tableConf[`${key}`];;
					modelInfo.properties.scriptType = getScriptType(tableConf[`${key}`]);;
					isChange = true;
					break;
				case "hierarchies":
					console.debug(`-------同步更新模型表【层级关系】--`);
					let hierarchies: Array<Hierarchies> = tableConf[`${key}`];
					if (!!hierarchies) {
						modelInfo.hierarchies = hierarchies; // 直接覆盖原有的层级配置即可
					}
					isChange = true;
					break;
			}
		}
	}
	return isChange;
}

/**
 * 根据【脚本路径】校验脚本文件是前端还是后端文件
 * @param scriptPath 脚本路径
 * @return 【browserScript | serverScript】
 */
function getScriptType(scriptPath: string): string {
	if (scriptPath == null) {
		return;
	}
	let scriptPathLen: number = scriptPath.length;
	if (scriptPath.substring(scriptPathLen - 10) == '.action.ts') {
		return "serverScript";
	}
	if (scriptPath.substring(scriptPathLen - 3) == '.ts') {
		return "browserScript";
	}
}

/**
 * 给default数据源下的物理表【新增 | 修改】字段
 * @param tableName 物理表名
 * @param updateColumns 需要修改的对象【仅修长度，eg[ { 英文字段名: XXX, 长度: XXX} ]
 * @param addColumns[] 需要新增的列对象
 * @param schema-可选
 */
function alterDbFields(tableName: string, updateColumns: Array<PhysicalColumn>, addColumns: Array<PhysicalColumn>, schema?: string): ResultInfo {
	if (!tableName) {
		return { result: false, message: `参数tableName：${tableName}` };
	}
	console.debug(`-------开始修改【${tableName}】表物理结构数据-------`);
	let resultInfo: ResultInfo = { result: true };
	let conn = ds.getConnection();
	try {
		let alterTable: AlterTable;
		let dsMeta: TableMetaData;
		if (!!schema) {
			alterTable = getTableAlter(conn, `${schema}.${tableName}`);
			dsMeta = ds.getTableMetaData(`${schema}.${tableName}`, schema);
		} else {
			alterTable = getTableAlter(conn, tableName);
			dsMeta = ds.getTableMetaData(tableName);
		}
		// 修改字段
		for (let i = 0; i < updateColumns.length; i++) { // 注意：由于所有修改是全部完成后，最后一起提交commit，修改依据还是按：旧物理表字段定位
			let updateColumn: PhysicalColumn = updateColumns[i];
			console.debug(`开始修改字段：`);
			console.debug(updateColumn);
			if (updateColumn.oldFieldName != updateColumn.fieldName) {
				alterTable.modifyColumnName(updateColumn.oldFieldName, updateColumn.fieldName); // 修改物理字段名
				console.debug(`字段名修改成功`);
			}
			alterTable.modifyColumn(updateColumn.oldFieldName, updateColumn.fieldType, updateColumn.fieldLength, updateColumn.fieldDic);
			console.debug(`字段类型、长度、精度修改成功`);
		}

		let currentDbFields: TableFieldMetadata[] = dsMeta.getColumns();
		let columnFiledNames: Array<string> = conversColums(currentDbFields);
		// 新增字段
		for (let i = 0; i < addColumns.length; i++) {
			if (columnFiledNames.includes(addColumns[i].fieldName)) { // 校验物理表是否已经存在此字段
				continue;
			}
			alterTable.addColumn(
				addColumns[i].fieldName,
				addColumns[i].fieldType,
				addColumns[i].fieldLength,
				addColumns[i].fieldDic,
				addColumns[i].defvalue,
				addColumns[i].nullable,
				addColumns[i].fieldDesc
			);
		}
		alterTable.commit();
		console.debug(`-------修改default数据源下的物理表【${tableName}】结构成功--`);
	} catch (e) {
		resultInfo.result = false;
		resultInfo.message = `修改default数据源下的物理表【${tableName}】结构失败-`;
		console.error(`-------修改default数据源下的物理表【${tableName}】结构失败--`);
		console.error(e);
	} finally {
		conn.close();
	}
	return resultInfo;
}


/**
 * 转换一下得到的字段列表
 * @param columns 当前表的列对象【TableFieldMetadata】
 * @return eg：["ID", "XHZWM", "SEX", "AGE", "SJ", "SJ1", "REMARK"]
 */
function conversColums(colums: TableFieldMetadata[]): Array<string> {
	let result: Array<string> = [];
	for (let i = 0; i < colums.length; i++) {
		let col = colums[i];
		result.push(col.name);
	}
	return result;
}

/**
 * 自动创建sdi目录下所有app模型对应的物理表
 */
export function createModelDbTable() {
	let metaFileTable = ds.openTableData("SZSYS_4_META_FILES");
	let tableInfos = metaFileTable.select("*", "PARENT_DIR like '%sysdata/data/tables/sdi%' and type='tbl' and SUB_FILE_TYPE='App'");
	console.debug("当前查询出来的模型共有" + tableInfos.length + "个");
	for (let i = 0; i < tableInfos.length; i++) {
		let tableInfo = tableInfos[i];
		let resid = tableInfo["ID"];
		let desc = tableInfo["DESC"];
		let dwTable = getDwTable(resid);
		let prop = dwTable.properties;
		let dbTableName = prop.dbTableName;
		let isExist = ds.isTableExists(dbTableName);
		if (!isExist) {
			let keys = prop.primaryKeys;
			let dims = dwTable.dimensions;
			let measures = dwTable.measures;
			let fields = [];
			fields.pushAll(getFieldInfo(dims));
			fields.pushAll(getFieldInfo(measures));
			let dbKeys = [];
			for (let i = 0; keys && i < keys.length; i++) {
				let key = keys[i];
				for (let j = 0; j < fields.length; j++) {
					if (key == fields[j].desc) {
						dbKeys.push(fields[j].name);
						break;
					}
				}
			}
			ds.createTable({
				tableName: dbTableName,
				primaryKeys: dbKeys,
				desc,
				fields
			});
			console.debug(`${tableInfo["NAME"]}模型对应物理表不存在，创建成功`)
		}
	}
}

function getFieldInfo(fields) {
	if (!fields) {
		return;
	}
	let list = [];
	for (let i = 0; fields && i < fields.length; i++) {
		let dim = fields[i];
		list.push({
			name: dim.getDbFieldName(),
			desc: dim.name,
			dataType: dim.dataType,
			length: dim.length
		})
	}
	return list;
}

/** 返回信息 */
interface ResultInfo {
	/** 返回结果 */
	result: boolean;
	/** 返回消息 */
	message?: string;
	/** 错误数据 */
	errorData?: any;
	/** 成功数据 */
	successData?: any;
	[propname: string]: any;
}

/** 层级对象【见dw-meta-type.d.ts】 */
interface Hierarchies {
	/** 层次id */
	id?: string;
	/** 层级名 */
	name?: string;
	/** 表示是层次，用于和其他类型区分 */
	isHierarchy?: boolean;
	/** 层级类型 */
	hierarchyType?: string;
	/** 层级字段集 */
	hierarchyFields?: Array<HierarchyFields>;
	/** 父字段 */
	parentField?: string;
	/** 子字段*/
	childField?: string;
	/** 层次字段。hierarchyType是pc时有效 */
	levelField?: string;
	/** 记录一行数据是否叶子节点的字段 */
	isLeafField?: string;
	/** 是否开启自动生成层级 */
	autoGenerateHierarchy?: boolean;
	[propname: string]: any;
}

/**  多字段层次字段 */
interface HierarchyFields {
	/** 层级字段描述 */
	desc: string;
	/** 字段名【支持表达式】 */
	field: string;
	[propname: string]: any;
}

/**  物理表字段对象 */
interface PhysicalColumn {
	/** 旧物理字段名 */
	oldFieldName?: string;
	/** 物理表字段名 */
	fieldName?: string;
	/** 字段类型 */
	fieldType?: string;
	/** 字段长度 */
	fieldLength?: number;
	/** 小数位数 */
	fieldDic?: number;
	/** 字段默认值 */
	defvalue?: string;
	/** 是否为空 */
	nullable?: boolean;
	/** 字段注释 */
	fieldDesc?: string;
}

/** 模型表元数据字段对象 */
interface ModelColumn {
	/** 描述 */
	name?: string;
	/** 字段文字描述 */
	textField?: string;
	/** 字段类型 */
	dataType?: string;
	/** 字段长度 */
	length?: number;
	/** 是否维表 */
	isDimension?: boolean;
	/** 英文字段名 */
	dbfield?: string;
	/** 关联维表路径 */
	dimensionPath?: string;
	/** 计算字段表达式 */
	exp?: string;
	/** 小数位数 */
	decimal?: number;
	[propname: string]: any;
}

/** 模型表配置信息，详细可见对应模型表-设置 */
interface Properties {
	modelDataType?: string,
	extractDataEnabled?: boolean,
	rebuildIndexAndKey?: boolean,
	modifyDbTableEnabled?: Array<string>,
	/** 所在数据源名 */
	datasource?: string,
	/** 物理表名 */
	dbTableName?: string,
	dbSchema?: string;
	/** 模型表主键集 */
	primaryKeys?: Array<string>;
	/** 脚本模型类型 */
	scriptType?: string;
	/** 是否设置脚本模型路径 */
	assignScriptPath?: boolean;
	/** 脚本模型路径 */
	scriptName?: string;
	/** 过滤条件 */
	dataFilter?: string,
	/** 业务键 */
	businessKeys?: Array<string>,
	/** 数据期类型 */
	periodType?: string,
	/** 数据校验结果标识: */
	checkErrorStateField?: string;
	[propname: string]: any;
}

/** 表元数据配置信息 */
interface MetaConfigInfo {
	/** 模型表路径 */
	tablePath?: string;
	/** 模型表配置信息 */
	properties?: Properties;
	/** 模型表字段信息集【来源传入的配置文件】 */
	fields: Array<ModelColumn>,
	/** 模型表字段信息集  */
	dimensions?: Array<ModelColumn>,
	/** 模型表层级配置数据集 */
	hierarchies?: Array<Hierarchies>;
	/** 模型表主键集【来源传入的配置文件】 */
	primaryKeys?: Array<string>;
	/** 脚本模型路径【来源传入的配置文件】 */
	scriptName?: string;
	/** 模型过滤条件【来源传入的配置文件】 */
	dataFilter?: string;
	[propname: string]: any;
}
