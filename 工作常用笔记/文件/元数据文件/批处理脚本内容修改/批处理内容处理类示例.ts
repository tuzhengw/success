
import db from "svr-api/db";
import sys from "svr-api/sys";
import dw from "svr-api/dw";
import metadata from 'svr-api/metadata';
import bean from "svr-api/bean";
import security from "svr-api/security";
import utils from "svr-api/utils";

/**
 * 共享任务数据推送业务批处理脚本处理类
 * @description 推送脚本内容包含指定：数据交换任务管理ID下[所有]推送表的推送计划
 */
class ShareTaskPushScriptMgr {

    /** 文件名(ts|js文件名一致) */
    private scriptName: string;
    /** 数据交换任务管理ID */
    private dxTaskMgrId: string;
    /**  推送计划ID(可多个或者为空) */
    private pushScheduleIds: string[];
    /** 日志对象 */
    private logger: SDILog;

    /** 推送脚本存放父路径 */
    private ScriptRootPath = '/sysdata/data/batch/sdi/dx/shareTaskPush';

    constructor(args: {
        scriptName: string,
        dxTaskMgrId: string,
        logger: SDILog
    }) {
        this.scriptName = args.scriptName;
        this.dxTaskMgrId = args.dxTaskMgrId;
        this.logger = args.logger;

        this.pushScheduleIds = [];
    }

    /**
     * 预检查
     */
    private preCheck(): ResultInfo {
        if (!this.scriptName) {
            return { result: false, message: "未获取到批处理脚本名" };
        }
        if (!this.dxTaskMgrId) {
            return { result: false, message: "未获取到数据交换任务管理ID" };
        }
        return { result: true };
    }

    /**
     * 处理推送资源批处理脚本
     */
    public dealPushBatchScript(): ResultInfo {
        let preResult = this.preCheck();
        if (!preResult.result) {
            return preResult;
        }
        this.queryPushScheduleIds();
        this.logger.addLog(`开始更新数据交换任务管理: [${this.dxTaskMgrId}] 下的推送脚本内容`);
        let { tsScriptContents, jsScriptContents } = this.builderScriptContent();
        let scriptId: string = this.adjustBatchScriptFile(tsScriptContents, jsScriptContents);
        return { result: true, data: scriptId };
    }

    /**
     * 构建推送脚本内容
     * @return
     */
    private builderScriptContent(): { tsScriptContents: string[], jsScriptContents: string[] } {
        let tsScriptContents: string[] = [];
        let jsScriptContents: string[] = [];
        let pushScheduleIds = this.pushScheduleIds;
        for (let i = 0; i < pushScheduleIds.length; i++) {
            let pushSchedule: string = pushScheduleIds[i];
            tsScriptContents.push(`dw.pushData({
                        schedule: '${pushSchedule}',
                        parallel: 5
                    });`);
            jsScriptContents.push(`dw_1.default.pushData({
                        schedule: '${pushSchedule}',
                        parallel: 5
                    });`);
        }
        return { tsScriptContents: tsScriptContents, jsScriptContents: jsScriptContents };
    }

    /**
     * 新增或者修改批处理脚本内容
     * @param fileContent 脚本内容, eg: [`{
                    schedule: 'Dr8iTswO0XCoPYWoT15qgB', // DX_RESOURCES中【推送计划】字段的值
                    parallel: 5 // 并发数
              }`]
       @return 批处理脚本ID
     *
     * @description 系统模型——任务模块——只能查看batch目录下批处理文件
     * @description 若想页面编辑查看，只能定制树目录
     */
    private adjustBatchScriptFile(
        tsfileContent: string[],
        jsFileContent: string[]
    ): string {
        let fileName: string = this.scriptName;
        let tsScriptPath: string = `${this.ScriptRootPath}/${fileName}.action.ts`;
        this.logger.addLog(`脚本路径: ${tsScriptPath}`);
        let jsScriptPath: string = `${this.ScriptRootPath}/${fileName}.action`;
        let content =
            `/**
			* 自动生成数据推送/回放脚本
			*/
			import dw from "svr-api/dw";
			export function main() {
				${tsfileContent.join("")}
			}`;
        let actionContent = `
			"use strict";
		   /**
    		* 自动生成数据推送/回放脚本
    		*/
			var dw_1 = require("svr-api/dw");
			function main() {
				${jsFileContent.join("")}
			}
			exports.main = main;`
        let scriptFile = metadata.getFile(tsScriptPath);
        let scriptId: string = scriptFile?.id;
        if (scriptFile == null) {
            this.logger.addLog(`脚本不存在, 开始创建脚本`);
            let tsFile = metadata.createFile(tsScriptPath, content);
            metadata.createFile(jsScriptPath, actionContent);
            scriptId = tsFile.id;
        } else {
            this.logger.addLog(`脚本已存在, 开始修改文件内容`);
            metadata.modifyFile(tsScriptPath, { content: content });
            metadata.modifyFile(jsScriptPath, { content: actionContent });
        }
        return scriptId;
    }

    /**
      * 查询数据交换任务管理ID下所有推送资源对应的推送计划ID
      */
    private queryPushScheduleIds(): void {
        let query: QueryInfo = {
            sources: [{
                id: "model1",
                path: "/sysdata/data/tables/sdi2/data-exchange/DX_TASK_RESOURCE.tbl"
            }],
            fields: [{
                name: "PUSH_SCHEDULE", exp: "model1.DX_RES_ID.PUSH_SCHEDULE"
            }],
            filter: [{
                exp: `model1.TASK_ID = '${this.dxTaskMgrId}'`
            }]
        }
        let queryData = dw.queryData(query).data;
        for (let i = 0; i < queryData.length; i++) {
            let data = queryData[i];
            let pushSchedule = data[0] as string;
            if (!!pushSchedule) {
                this.pushScheduleIds.push(pushSchedule);
            }
        }
        this.logger.addLog(`当前数据交换任务管理:[${this.dxTaskMgrId}] 推送计划: `);
        this.logger.addLog(this.pushScheduleIds);
    }
}