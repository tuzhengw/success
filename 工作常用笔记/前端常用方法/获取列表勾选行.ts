
/**
 * 将所勾选列表行部分列值set到定制下拉框组件中
 * @description 当前操作是对话框中
 * <p>
 *  20230330 tuzw
 *  地址: https://jira.succez.com/browse/CSTM-22470
 * </p>
 * @param jsComponentId 定制组件ID
 * @return 
 */
public setMetaDataComboboxValue(event: InterActionEvent): boolean | Promise<any> {
	let params = event.params;
	let jsComponentId: string = params?.jsComponentId;
	if (isEmpty(jsComponentId)) {
		showWarningMessage(`未获取定制组件ID`);
		return false;
	}
	let page = event.page;
	let jsComp = page.getComponent(jsComponentId);
	if (isEmpty(jsComp)) {
		showWarningMessage(`[${jsComponentId}]不存在当前页面`);
		return false;
	}
	let drillPageDialogDom = document.getElementById("drillPageDialog") as HTMLElement;
	if (isEmpty(drillPageDialogDom)) {
		showWarningMessage(`未获取到对话框组件`)
		return false;
	}
	let drillPageDialog: Dialog = drillPageDialogDom.szobject;
	let drillPage: SuperPage = drillPageDialog.content.getActivePage();
	let drillPageListComp = drillPage.getData().getComponent("list1");
	if (isEmpty(drillPageListComp)) {
		showWarningMessage(`页面没有列表组件`);
		return false;
	}
	let checkedRows = drillPageListComp.getCheckedDataRows() as MetaDataProperties[]; // 列表若多个重复列名, 默认取相同列最后一列数据
	if (checkedRows.length == 0) {
		return false;
	}
	let listRowData: MetaDataProperties = checkedRows[0]; // 暂且仅勾选一行
	let caption = listRowData.DESC as string;
	if (isEmpty(caption)) {
		caption = listRowData.NAME;
	}
	let customCombobox = jsComp.component.getInnerComoponent();
	let items = {
		id: listRowData.META_DATA_ID,
		value: listRowData.META_DATA_ID,
		caption: caption
	};
	customCombobox.setValue(items);
	showSuccessMessage(`操作结束`);
	return true;
}

/** 元数据数据属性 */
interface MetaDataProperties {
	/** 元数据ID */
	META_DATA_ID: string;
	/** 元数据名 */
	NAME: string;
	/** 元数据描述 */
	DESC?: string;
	[propname: string]: any,
}