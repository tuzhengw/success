import { Dialog, showConfirmDialog } from "commons/dialog";
import { DisplayFormat, ExpUtils, IExpEvalDataProvider, NumberFormatter } from "commons/exp/expeval";
import { ExportReusltType, url2pdf } from "commons/export";
import { Form } from "commons/form";
import { Grid, GridArgs, List, ListItem, TreeItem } from "commons/tree";
import { getDwTableChangeEvent, getDwTableDataManager, getQueryManager, updateDwTableDatas } from "dw/dwapi";
import { FAppDBD_IM } from "fapp/browser/fappbrowserdesktop";
import { FAppComponentData } from "fapp/form/fappdatamgr";
import { MetaFileViewer, showDrillPage } from "metadata/metadata";
import { FAppCommandRendererId, InterActionEvent } from "metadata/metadata-script-api";
import { showResourceDialog } from "metadata/metamgr";
import { parseMultiValue } from "sys/kvparser";
import {
	CommandItemInfo, Component, ctx, isEmpty, rc, showErrorMessage, showFormDialog,
	showMenu, showSuccessMessage, showWaiting, showWarningMessage, SZEvent, throwInfo, uuid
} from "sys/sys";
import { getAppPath, modifyClass } from "../commons/commons.js";
import { Tree } from 'commons/tree';
import { Checkbox, Combobox, DataProvider } from "commons/basic";
import { SuperPageBuilder } from "app/superpage/superpagebuilder";
import { SuperPage } from 'app/superpage/superpage';
import { ExpDialog, IFieldsDataProvider, showExpDialog } from 'commons/exp/expdialog';
import { CustomRuleFieldDp } from "../data-govern/data-govern-mgr.js";
import { ExpContentType } from 'commons/exp/exped';



/**
 * 标准表管理
 */
export class StandardTableMgr {

	/** 有前缀表名的自定义规则字段的dp */
	public dgRuleFieldDp: CustomRuleFieldDp;

	public CustomExpFunctions: any;
	public CustomActions: any;

	constructor() {
		if (this.dgRuleFieldDp == null) {
			this.dgRuleFieldDp = new CustomRuleFieldDp();
		}
		this.CustomActions = {
			button_std_setRuleExp: this.button_std_setRuleExp.bind(this)
		};
	}

	/**
	 * 设置自定义规则表达式
	 * @param standardLibId 标准库ID
	 * @param displayCompId 设置中文表达式显示的控件ID
	 * @param outPutComponentId 设置英文表达式值输出到那个组件中，eg：multipleinput1
	 * 
	 * @param tableAlias? 操作的数据源别名，eg：tableAlias：'EDU_F_XSSJ_1' —— resid：'D8OxNhGgaeCvTtUMICaMoE
	 * @param isNeedPrefix? 是否需要表达式前缀
	 * @param expType? 表达式类型，默认：expression，支持：auto、expression、macro、tex
	 */
	public button_std_setRuleExp(event: InterActionEvent) {
		let page = event.page;
		let params = event.params;
		let tableAlias: string = params?.tableAlias;
		let isNeedPrefix: string = params?.isNeedPrefix;
		let displayCompId: string = params?.displayCompId;
		let outPutComponentId: string = params?.outPutComponentId;
		let standardLibId: string = params?.standardLibId;

		if (isEmpty(displayCompId) || isEmpty(outPutComponentId)) {
			showWarningMessage(`未指定设置表达式输出控件ID`);
			return;
		}
		if (isEmpty(standardLibId)) {
			showWarningMessage(`标准库ID不为空`);
			return;
		}
		let expPrefixIsNeed: boolean = true;
		if (!isEmpty(isNeedPrefix) && isNeedPrefix == "false") {
			expPrefixIsNeed = false;
		}
		let expType: string = params?.expType;
		let self = this;
		function showExp() {
			let displayComp = page.getComponent(displayCompId);
			let filterInputComp = page.getComponent(outPutComponentId);
			showExpDialog({
				readonly: false,
				newborn: true,
				defaultTab: "fields",
				expRequired: true,
				buttons: ['ok'],
				expDataProvider: self.dgRuleFieldDp.getExpDp(),
				contentType: self.getExpContentType(expType),
				fieldPanelImpl: {
					depends: "dsn/datasourceeditor",
					implClass: "DataSourceEditor",
					sourceTheme: SourceTheme.Combobox
				},
				fieldsDataProvider: self.dgRuleFieldDp,
				caption: " 配置检查规则测试数据范围的过滤条件",
				value: {
					exp: filterInputComp.getValue()
				},
				needTransformValue: true,
				onok: function (e: SZEvent, dialog: ExpDialog) {
					filterInputComp.setValue(dialog.getValue().exp);
					displayComp.setValue(dialog.exped.originValue);
				}
			});
		}
		if (!isEmpty(tableAlias)) {
			this.dgRuleFieldDp.setAssginSource(tableAlias);
		}
		if (this.dgRuleFieldDp.getIsNeedTableName()) {
			this.dgRuleFieldDp.setIsNeedTableName(expPrefixIsNeed); // 设置表达式不需要前缀表别名	
		}
		this.dgRuleFieldDp.initTableAndFieldsInfo({
			initTableAlias: tableAlias,
			checkTableType: CheckTableType.StandardCheck,
			standardLibId: standardLibId
		}).then(() => { // 重新加载字段信息
			showExp();
		});
	}

	/**
	 * 获取【表达式编辑框的编辑内容或编辑的模式】
	 * @param expType 表达式类型
	 * @return ExpContentType
	 */
	public getExpContentType(expType: string): ExpContentType {
		switch (expType) {
			case "auto": return ExpContentType.AUTO;
				break;
			case "expression": return ExpContentType.EXPRESSION; // name=="张三"
				break;
			case "macro": return ExpContentType.MACRO; // ${name=="张三"}
				break;
			case "text": return ExpContentType.TEXT;
				break;
		}
		return ExpContentType.EXPRESSION;
	}
}