

export const CustomJS: { [type: string]: any } = {
    spg: {
        onRender: (event: InterActionEvent) => {
            let page = event.renderer;
            if (page.builder && page.builder.fileInfo && page.builder.fileInfo.path) {
                let path = page.builder.fileInfo.path;
                if (SPG_SY.indexOf(path) > -1) {
                    new MpgAddWatermark().onShow(event);
                }
            }
        }
    }
}

/**
=================================================
liuziy 2023-01-04
对所有spg和指定rpt添加页面水印功能
帖子：https://jira.succez.com/browse/CSTM-19823
================================================
*/
class MpgAddWatermark {
    constructor() {}

    /**
     * 有页面要准备显示时调用。
     *
     * 注：用于给页面添加水印
     *
     * 1. 门户内的某个子资源页面显示前调用
     * 2. 发生下钻时，下钻出的新页面显示前调用
     * @param component 将要显示的页面控件
     */
    public onShow(event: InterActionEvent): void {
        let component: IAnaObjectRenderer = event.renderer as IAnaObjectRenderer;
        //添加水印
        let dom: HTMLElement = component['watermark'];
        if (dom) {//已添加过水印了，不重复添加
            return;
        }
        dom = component['watermark'] = document.createElement('div');
        dom.className = 'watermark-bg-wraper';
        let txt = `${getCurrentUser().userId || 'anonymous'}${formatDate(new Date(), 'yyyyMMdd')}`;
        let watermarkInfo = {
            watermarkType: WatermarkType.Txt,
            watermarkFontSize: 24,
            watermarkFontFamily: '微软雅黑',
            watermarkTxt: txt,
            watermarkPosition: WatermarkPosition.LeftTop,
            watermarkRepeat: false,
            watermarkRotate: -45,
            printWatermark: true
        };
        let styleConvertor = new DefaultStyleConvertor();
        let watermarkDom1 = document.createElement('div');
        watermarkDom1.className = 'watermark-bg';
        renderDomWatermark(watermarkDom1, watermarkInfo, styleConvertor);
        watermarkDom1.style.backgroundPosition = 'center 550px';

        let watermarkDom2 = document.createElement('div');
        watermarkDom2.className = 'watermark-bg';
        renderDomWatermark(watermarkDom2, watermarkInfo, styleConvertor);
        watermarkDom2.style.backgroundPosition = 'center 355px';

        let watermarkDom3 = document.createElement('div');
        watermarkDom3.className = 'watermark-bg';
        renderDomWatermark(watermarkDom3, watermarkInfo, styleConvertor);
        watermarkDom3.style.backgroundPosition = 'center 137px';

        let watermarkDom4 = document.createElement('div');
        watermarkDom4.className = 'watermark-bg';
        renderDomWatermark(watermarkDom4, watermarkInfo, styleConvertor);
        watermarkDom4.style.backgroundPosition = 'center top';
        watermarkDom4.style.marginTop = '-4%';

        dom.appendChild(watermarkDom1);
        dom.appendChild(watermarkDom2);
        dom.appendChild(watermarkDom3);
        dom.appendChild(watermarkDom4);
        component.getDomParent().appendChild(dom);
    }
}



/** 水印样式 */
.watermark-bg-wrapper {
    position: absolute;
    pointer-events: none;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    width: 100%;
    height: 100%;
}

.watermark-bg {
    width: 100%;
    height: 100%;
    position: absolute;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    background-repeat: repeat-x !important;
}

.watermark.mark-repeat {
    width: 100%;
    height: 100%;
}