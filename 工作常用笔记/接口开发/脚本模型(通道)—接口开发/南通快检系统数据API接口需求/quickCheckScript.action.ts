/**
 * ============================================================
 * 作者: tuzhengw
 * 审核人员：liuyongz
 * 创建日期：2021-01-14
 * 功能描述：
 *      该脚本主要是用于南通快检系统数据API接口需求，获取部门表信息
 * ============================================================
 */
import { sumbit, submitData, ruleGenerator, getLimit } from "/sysdata/public/util/utils.action";
import { getDwTable } from "svr-api/dw";

/** API接口 */
const Requests = [
    "insertDepartData", // 部门表
    "insertSimpleData", // 采样表
    "insertDealData", // 处置表
    "insertRepeatCheckData", // 复核表
    "insertCheckData", // 检测表
    "insertQuickCheckData", // 快检室表
    "insertMarkData" // 市场表
]

/** 返回值 */
interface ResultInfo {
    /** 返回值 */
    result?: boolean;
    /** 编码 */
    code?: string;
    /** 错误编码 */
    errorCode?: string;
    /** 处理消息 */
    message?: string;
    /** 返回数据 */
    data?: any;
    /** 成功插入数据的主键集 */
    insertIds?: any;
    /** 成功插入的行数  */
    rowCount?: number;
}

/**
 * 模型调度对外提供的API支持
 * 需求：数据全部合法则插入，不合法，全部打回，并附带错误详细原因
 * @param dxcnl 通道信息
 * @param apiId 一个模型可以有多个api，外界可以指定调用的api
 * @param params 外部传递的参数，由脚本和外部调用者自己约定。有几个约定的参数：
 * @param output 数据输出的流或临时文件的路径，如不传递，那么将通过函数返回值返回，具体格式有脚本实现者和调用者协商。
 * @return true
 */
function onDXService(args?: { dxcnl: DXChannelInfo, apiId: string, params: JSONObject, output?: OutputStream | string, logs?: any }): DXResourceScriptAPIResult {
    let startTime = new Date().getTime();
    /** 参数自带的通道对象 */
    let dxObj: DXResourceScriptAPIResult = { rowCount: 0, data: [] };
    let resultInfo: ResultInfo = { result: true };
    let params = args.params;
    let logs = args.logs;
    let correspondenceObj = args.dxcnl;
    let datas = params && params.data; // JSON数组，记录插入的数据
    let apiId: string = args.apiId;
    console.debug(`onDXService()，南通快检系统数据API接口需求，获取部门表信息，参数：【${params}】--start`);
    if (Requests.indexOf(apiId) == -1) { // 检测接口名是否正确
        resultInfo.result = false;
        resultInfo.code = "500";
        resultInfo.errorCode = 'appNotExist';
        resultInfo.message = `当前接口不存在！`;
        dxObj.data = resultInfo;
        return dxObj;
    }
    if (!datas || !datas.length) {
        resultInfo.result = false;
        resultInfo.code = "500";
        resultInfo.errorCode = 'dataIsNull';
        resultInfo.message = `data不为NULL，并且必须是JSON数组`;
        dxObj.data = resultInfo;
        return dxObj;
    }
    let modelPath: string = correspondenceObj.getDwTable();
    let optionMaxLimit: number = getLimit(apiId, correspondenceObj);  // 插入行数限制，默认：100
    if (!optionMaxLimit) {
        optionMaxLimit = 100;
    }
    let fields = [];
    correspondenceObj.fields.forEach(field => { // 记录插入的字段列
        fields.push(field);
    });
    let checkRules = ruleGenerator(modelPath);
    let dataDealResult = submitData(modelPath, datas, 'insert', checkRules, fields, optionMaxLimit, true);
    if (dataDealResult['data']['result']) {
        resultInfo.message = `请求成功，上传${datas.length}条数据，成功插入${dataDealResult["rowCount"]}条数据`;
        resultInfo.rowCount = dataDealResult.rowCount;
        resultInfo.code = "200";
        resultInfo.errorCode = "success";
        resultInfo.insertIds = getSuccessInsertPks(datas, apiId, dataDealResult['data']['successIndexs']);
        let errorInfo = dataDealResult['data']['checkResult'];
        if (!!errorInfo) {
            resultInfo.data = errorInfo;
        }
    } else {
        let errorInfo = dataDealResult['data']['checkResult'];
        resultInfo.message = `数据全部不合法，插入失败`;
        resultInfo.result = false;
        resultInfo.code = "500";
        resultInfo.errorCode = "insertError";
        resultInfo.data = errorInfo;
        console.debug(`【${modelPath}】数据全部不合法，插入失败，【${JSON.stringify(errorInfo)}】--`);
    }
    logs.put('result', JSON.stringify(resultInfo));
    let endTime = new Date().getTime();
    console.debug(`onDXService()，南通快检系统数据API接口需求，获取部门表信息，花费时间为：${(endTime - startTime) / 1000}秒--end`);
    dxObj.data = resultInfo;
    return dxObj;
}


/**
 * 获取数据成功插入的主键值
 * @param data 插入成功的数据（JSON格式）
 * @param request 请求名
 * @return []
 */
function getSuccessInsertPks(data, request: string, successIndexs: string[]): string[] {
    console.debug(`getSuccessInsertPks()，获取数据成功插入的主键值`);
    if (!data || !successIndexs) {
        return [];
    }
    let primaryKeysFiledName: string = getPrimaryKey(request);
    if (primaryKeysFiledName == "") {
        return [];
    }
    let primaryKeys: string[] = [];
    for (let i = 0; i < successIndexs.length; i++) {
        let successIndex = Number(successIndexs[i]);
        primaryKeys.push(data[successIndex][`${primaryKeysFiledName}`]);
    }
    return primaryKeys;
}

/**
 * 根据请求的方法，获取对应表的主键字段
 * @parma request 请求名
 */
function getPrimaryKey(request: string) {
    let primaryKey: string = "";
    switch (request) {
        case "insertDepartData": primaryKey = "DEPTID"; // 部门表
            break;
        case "insertSimpleData": primaryKey = "ID"; // 采样表
            break;
        case "insertDealData": primaryKey = "ID"; // 处置表
            break;
        case "insertRepeatCheckData": primaryKey = "ID"; // 复核表
            break;
        case "insertCheckData": primaryKey = "DETECTID"; // 检测表
            break;
        case "insertQuickCheckData": primaryKey = "DEPTID"; // 快检室表
            break;
        case "insertMarkData": primaryKey = "MKTID"; // 市场表
            break;
    }
    return primaryKey;
}