
import metadata from 'svr-api/metadata';

/**
 * 根据脚本ID 或 路径，修改批处理脚本名
 * @param scriptId 脚本ID
 * @param newScriptName 新脚本名
 * 
 * @description 表所存的脚本ID，通常都是TS文件ID，通过表所存ID修改脚本时, 也需要一同修改对应JS文件
 */
export function modifySignBatchScriptName(scriptIdOrPath: string, newScriptName: string): void {
    if (!scriptIdOrPath) {
        return;
    }
    let batchFile = metadata.getFile(scriptIdOrPath);
    console.info(`开始修改批处理脚本: [${scriptIdOrPath}], 脚本是否存在情况: ${batchFile != null}`);
    if (batchFile != null) {
        let batchFileType = batchFile.type as string;
        if (batchFile?.type != "ts") {
            console.info(`当前修改的文件类型为: ${batchFileType}, 方法仅修改: ts 脚本文件, 不执行修改`);
            return;
        }
        let tsScriptPath: string = batchFile.path; // eg: /sysdata/data/batch/sdi/dx/shareTaskPush/每日凌晨定时推送.action.ts
        let jsScriptPath: string = tsScriptPath.substring(0, tsScriptPath.lastIndexOf(".ts")); // 去掉后缀，得到对应js文件路径
        metadata.modifyFile(tsScriptPath, {
            name: `${newScriptName}.action.ts`
        });
        if (metadata.getFile(jsScriptPath) != null) { // 进一步判断对应JS文件是否存在
            metadata.modifyFile(jsScriptPath, {
                name: `${newScriptName}.action`
            });
        }
        console.info(`批处理脚本(ts|js)[${scriptIdOrPath}] 修改成功`);
    }
}