/**
 * =====================================================
 * 作者：tuzw
 * 创建日期:2023-02-13
 * 功能描述
 *  此脚本是数据中台所有spg、表单、dash和门户定制功能入口，
 */
import { isEmpty, showWarningMessage, throwError, assign, showWaiting, formatDate, rc, downloadFile } from "sys/sys";
import { IMetaFileCustomJS, InterActionEvent } from "metadata/metadata";
import { ExportReusltType, url2pdf, html2pdf, PdfInfo } from "commons/export";


/**
 * 付款申请SPG脚本
 */
export class PaymentApplySpgJs {


    public CustomActions: any;
    constructor() {
        this.CustomActions = {
            button_importApplayPage: this.importApplayPage.bind(this),
        }
    }

    /**
     * 付款申请单导出为PDF
	 * 地址: https://jira.succez.com/browse/CSTM-21964
     * <p>
     *  导出的页面存在附件图片, 而附件展示使用的是产品: 附件上传组件, 当导出PDF后, 则无法查看。
     *  原因: 附件DOM存在一个短路径请求(请求服务器后台附件), 导出的PDF, 脱离了服务器, 无法通过短路径获取附件
     *  尝试解决办法:
     *   1 将导出的页面附件图片base64值转换为URL, 替换产品请求的短路经。
     *   2 URL.createObjectURL将BASE64关联到MediaSource对象上去, 从而得到一个可访问URL。
     *   3 缺陷:
     *     1) URL的生命仅存在于在被创建的这个文档里(页面刷新, URL则失效)。
     *     2) 若不在使用, 也可调用revokeObjectURL使这个潜在的对象回到原来的地方，允许平台在合适的时机进行垃圾收集。
     *     3) 由于需要脚本去调用释放, 导出后已经脱离了脚本, 此方法不可用。
     *  最终采取办法:
     *   1 将导出的PDF和页面附件存入压缩包一起导出。
     * </p>
     * @param importCompId 导出控件ID
     * @param importFileName? 文件名
     * @param paymentApplyId 付款申请ID
     * @param existAttachNums 申请单存在的附件数据量
     * @return
     */
    private importApplayPage(event: InterActionEvent): Promise<Blob | string | void | PdfInfo> | void {
        let page = event.page;
        let params = event.params;
        let importFileName: string = params?.importFileName;
        if (isEmpty(importFileName)) {
            importFileName = `${formatDate(new Date(), "yyyyMMdd")}_付款清单表`;
        }
        let importCompId: string = params?.importCompId;
        if (isEmpty(importCompId)) {
            showWarningMessage(`未获取到需要导出的控件ID`);
            return;
        }
        let redener = event.renderer;
        let fileInfo = page.getFileInfo();
        let resid = fileInfo.id;
        let importComp = redener?.getComponent(importCompId);
        if (isEmpty(importComp)) {
            showWarningMessage(`导出控件不存在`);
            return;
        }
        let paymentApplyId: string = params?.paymentApplyId;
        if (isEmpty(paymentApplyId)) {
            showWarningMessage(`未获取到付款申请ID`);
            return;
        }
        let domBase = importComp?.getDomBase() as HTMLElement;
        // domBase.getElementsByClassName("button-base smalldefbtn listupload-button icon-uploadt defaultLight")[0].style.display = "none";

        let existAttachNums: number = params?.existAttachNums;
        let exportResultType = isEmpty(existAttachNums) || existAttachNums == 0
            ? ExportReusltType.DOWNLOAD
            : ExportReusltType.BASE64; // 若清单无附件, 则直接下载
        let importPromise = html2pdf(resid, domBase, importFileName, exportResultType, {
            width: (domBase.widthNoPadding + 140),
            margin: { 
				all: 30  // 注意此处边距太高, 导出的PDF可能会存在中间空白页
			}
        }).then((convertResult => {
            // domBase.getElementsByClassName("button-base smalldefbtn listupload-button icon-uploadt defaultLight")[0].style.display = "";
            if (exportResultType == ExportReusltType.BASE64 && !isEmpty(convertResult)) {
                return this.dealApplyAttach(paymentApplyId, importFileName, convertResult as string);
            }
        }));
        return showWaiting(importPromise);
    }


    /**
     * 若导出的付款申请单包含附件, 则需要将附件和付款申请单一起打包导出
     * @param paymentApplyId 付款申请ID
     * @param importFileName? 文件名
     * @param applyBase64 付款申请单转换的BASE64值
     * @return
     */
    private dealApplyAttach(paymentApplyId: string, importFileName: string, applyBase64: string): Promise<boolean> {
        return rc({
            url: "/oa/server/arrangePaymentApply",
            data: {
                paymentApplyId: paymentApplyId,
                applyBase64: applyBase64,
                zipName: importFileName
            }
        }).then((arrangeResult => {
            if (!arrangeResult.result) {
                showWarningMessage(arrangeResult.message);
                return false;
            }
            let zipPath: string = arrangeResult.zipPath;
            downloadFile(`/bgxt/app/wdmh.app/commons/commons.action?method=importSignZipFile&zipFilePath=${zipPath}&isDeleteZip=true`);
            return true;
        }));
    }
}

export const CustomJS: { [file_OR_type_OR_resid: string]: IMetaFileCustomJS } = {
    "FKSQ.spg": new PaymentApplySpgJs()
}