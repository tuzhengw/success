
import security from "svr-api/security";
/**
 * 根据上传文件的ID获取上传文件对象
 * @param uploadFileId 上传文件ID(注意文件后缀), eg: 33e21b559036a5904ab298cf7947971cced1-sdi_2.0_20221208155857.zip
 * @return 
 */
function getUploadFile(uploadFileId: string): JavaFile {
	let user = security.getCurrentUser();
	let userId = user?.userInfo.userId;
	if (!userId) {
		userId = "admin"
	}
	let tempUploadFileService = bean.getBean("com.succez.commons.service.impl.io.TempUploadFileServiceImpl");
	let fileObject = tempUploadFileService.getUploadFile(userId, uploadFileId);
	let filePath = fileObject.getPath();
	let file = new JavaFile(filePath);
	return file;
}