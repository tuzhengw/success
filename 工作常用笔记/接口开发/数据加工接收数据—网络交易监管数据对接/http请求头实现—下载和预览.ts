
import Files_ = java.nio.file.Files;
import Paths_ = java.nio.file.Paths;

// const Paths_ = new java.nio.file.Paths;
// const Files_ = new java.nio.file.Files;

const attachmentPath = "";
/**
 * 获取服务器指定文件，返回给客户端，供用户：【下载】 | 【预览】
 * 
 * 下载和预览，可通过设置：【请求头】 处理
 * 
 * response 设置响应头 ：Content-Disposition 消息头指示回复的内容：该以何种形式展示，是以内联的形式（即网页或者页面的一部分），还是以附件的形式下载并保存到本地
 * @param fileName
 * @param 
 * /zhjg/workdir/clusters-share/internetDeal/test/certPath_101702956049248498D66E125BE4D6427920D70C96978159F.pdf
 */
export function downFileInfo1(request: HttpServletRequest, response: HttpServletResponse, params: any) {
    print(`---downFileInfo()，获取服务器指定文件，返回给客户端，供用户下载---start`);
    // png
    // let fileName = "filePath_10170175218714519snapshot.png";
    // let sendFile = new java.io.File(`/zhjg/workdir/clusters-share/internetDeal/test/${fileName}`);

    // pdf
    let fileName = "certPath_101702956049248498D66E125BE4D6427920D70C96978159F.pdf";
    let sendFile = new java.io.File(`/zhjg/workdir/clusters-share/internetDeal/test/${fileName}`);

    let fileBytes = Files_.readAllBytes(Paths_.get(sendFile.getAbsolutePath())); // 通过绝对路径获取文件字节流

    /**
     * Content-Disposition设置（下载）
     * （1）inline（浏览器打开）：如果浏览器支持该文件类型的【预览】，就会打开，而不是下载，eg：response.setHeader("Content-Disposition", "inline; filename=111.jpg");
     * （2）attachment（附件下载）：浏览器则直接进行【下载】，纵使他能够预览该类型的文件，eg：response.setHeader("Content-Disposition", "attachment; filename=111.jpg");
     */
    // response.setHeader("Content-Disposition", "inline; filename=" + fileName); // 给定【文件下载】请求头
    // response.setContentType("multipart/form-data");
    // response.setContentType("application/octet-stream");
    // response.setContentLength(fileBytes.length);

    /** 预览-png格式 */
    // response.setContentType("image/png");

    /** 预览-pdf格式 */
    response.setContentType("application/pdf");

    let out: OutputStream = null;
    try {
        out = response.getOutputStream();
        out.write(fileBytes);
        out.flush();
    } catch (e) {
        print(e);
    } finally {
        out && out.close();
    }
    print(`---downFielInfo()，获取服务器指定文件，返回给客户端，供用户下载---end`);
    // 因为是直接对response对象操作，这里不需要返回
    // return { result: true, fileByte: fileBytes };
}


export function downFileInfo(request: HttpServletRequest, response: HttpServletResponse, params: any) {
    print(`---downFileInfo()，获取服务器指定文件，返回给客户端，供用户下载---start`);
    let onclickFunction = params.onClick; // 当前点击的功能：【下载】 | 【预览】
    let filePath: string = params.filePath;
    let fileName = filePath.substring(filePath.lastIndexOf("/") + 1);
    let fileType = filePath.substring(filePath.lastIndexOf(".") + 1);
    let sendFile = new java.io.File(`/zhjg/workdir/clusters-share/internetDeal/test/${fileName}`);
    let fileBytes = Files_.readAllBytes(Paths_.get(sendFile.getAbsolutePath())); // 通过绝对路径获取文件字节流
    if(onclickFunction == "downLoad") {
        /**
        * Content-Disposition设置（下载）
        * （1）inline（浏览器打开）：如果浏览器支持该文件类型的【预览】，就会打开，而不是下载，eg：response.setHeader("Content-Disposition", "inline; filename=111.jpg");
        * （2）attachment（附件下载）：浏览器则直接进行【下载】，纵使他能够预览该类型的文件，eg：response.setHeader("Content-Disposition", "attachment; filename=111.jpg");
        */
        response.setHeader("Content-Disposition", "attachment; filename=" + fileName); // 给定【文件下载】请求头
        // response.setContentType("multipart/form-data");
        response.setContentType("application/octet-stream");
        response.setContentLength(fileBytes.length);
    } else {
        let contentType = "";
        switch(fileType) {
            case "png": contentType = "image/png";
                break;
            case "jpg": contentType = "image/png";
                break;
            case "pdf": contentType = "application/pdf";
                break;
            default:
                print(`----当前所选文件：${fileName}的类型暂时不支持，若需要支持，请联系开发人员添加----`);
        }
        // 设置请求头，实现：点击——预览
        response.setContentType(contentType);
    }
    let out: OutputStream = null;
    try {
        out = response.getOutputStream();
        out.write(fileBytes);
        out.flush();
    } catch (e) {
        print(e);
    } finally {
        out && out.close();
    }
    print(`---downFielInfo()，获取服务器指定文件，返回给客户端，供用户下载---end`);
    // 因为是直接对response对象操作，这里不需要返回
    // return { result: true, fileByte: fileBytes };
}