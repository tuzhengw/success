/**
 * 删除指定字段
 * @param request
 * @param response
 * @param params
 */
export function dropField(request: HttpServletRequest, response: HttpServletResponse, params: any) {
	let targetDataBase = params.targetDataBase;
	let targetTable = params.targetTable;
	let fieldName = params.fieldName;
	let targetDs = db.getDataSource(targetDataBase);
	let conn = targetDs.getConnection();
	try {
		let alter = db.getAlterTableProvider(conn, targetTable);
		alter.dropColumn(fieldName);
		alter.commit();
	} finally {
		conn.close();
	}
	return true;
}