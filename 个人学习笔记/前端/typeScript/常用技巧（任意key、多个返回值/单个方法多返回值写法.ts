

/**
 * 也可以自己interface封装
 */
export function checkAccessToken(access_token: string): {
	/** 授权用户的用户信息 */
	user: UserInfo;
	/**
	 * 是否 access_token 有效
	 */
	valid: boolean;
	/**
	 * access_token 无效的时候，给出的错误提示
	 */
	errorCode: string;
}