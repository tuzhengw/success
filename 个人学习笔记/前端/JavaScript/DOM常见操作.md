---
typora-root-url: images
---

# JS

# * 常见方法

## 1 scrollWidth

> 参考网址：https://developer.mozilla.org/en-US/docs/Web/API/Element/scrollWidth

​	的**`Element.scrollWidth`**只读属性是一个元素的内容的宽度，其中包括：由于溢出内容在屏幕上不可见的测量。

​	该`scrollWidth`值：等于<button>元素需要的最小宽度</button>，以便在不使用水平滚动条的情况下适应视口中的所有内容。

```js
var xScrollWidth = element.scrollWidth;
```

​	`scrollWidth`的语义就是：当一个元素**有滚动条**的时候，`scrollWidth`表示的是：元素内容区域的滚动宽度；当没有滚动条的时候，就是：元素的本身宽度。

----

## 2 getElementsByClassName()[0]

> 注意：这个方法会给我们返回一个：<button>类数组对象</button>，所有查询到的元素都会封装到对象中。

​	获取指定的对象。

```html
<div id="London" class="tabcontent">
	<span class="topright">&times;</span>
</div>
```

```js
var top=document.getElementsByClassName("topright");
var top_z=document.getElementsByClassName("topright")[0];
```

​	console

----

top：HTMLCollection(3)[span.topright, span.topright,...]

top_z：获取对象

> ​	< span class="topright">&times;</ span>

---

## 3 appendChild()

**用法：**

- 先把元素从原有父级上删除
- 再把元素添加到新父级上

```html
<div id="father">
    <div id="son">123</div>
    <div>我是对比内容</div>
</div>
<script type="text/javascript">
    var father = document.getElementById("father");
    var son = document.getElementById('son');
    father.appendChild(son);
</script>
```

效果：

```
我是对比内容
123
```

​	说明id为son的这个节点直接被拿到了：father的**最后一个子节点的后面**，而它**本身就不存在了**，**相似的操作方法还有insertBefore()。**

----

## 4 addEventListener() 方法

### 4.1 定义

addEventListener() 方法用于向指定元素：<button>添加事件句柄</button>。

提示： 使用 removeEventListener() 方法来移除 addEventListener() 方法添加的事件句柄。

### 4.2 语法

```js
element.addEventListener(event, function, useCapture)
```

### 4.3 参数值

event ：字符串，指定事件名。

> 注意: 不要使用 “on” 前缀。 例如，使用 “click”，而不是使用 “onclick”。

---

function ：指定要事件触发时执行的函数。

​	当事件对象会作为第一个参数传入函数。 事件对象的类型取决于特定的事件。

> 例如， “click” 事件属于 <button>MouseEvent(鼠标事件) 对象</button>。 
>
> click 当用户<button>点击某个对象时</button>调用的事件句柄

---

useCapture 可选。布尔值，指定事件是否在：捕获或冒泡阶段执行。

可能值: 
true - 事件句柄在捕获阶段执行 
false- false- 默认。事件句柄在冒泡阶段执行

---

### 4.4 事件冒泡或事件捕获

> 参考网址：https://developer.mozilla.org/zh-CN/docs/Learn/JavaScript/Building_blocks/Events#事件冒泡及捕获

----

> 如果<div>元素中有<p>元素，并且用户单击<p>元素，应该首先处理哪个元素的“click”事件？
>

​	在冒泡中，首先处理内部元素的事件，然后处理外部：首先处理<p>元素的click事件，然后处理<div>元素的click事件。

​	事件冒泡：当点击一个按钮后，此<button>事件会一直向上传递</button>，称：**事件冒泡**。可能会对父类活根类型事件造成影响。

​	例如：点击【按钮】弹出一个框，点击空白处，弹框消失。由于【按钮】是向上传递的，点击后会同时触发 点击空白处弹框消失事件。

----

​	在捕获最外层元素时，首先处理事件然后处理内部：首先处理<div>元素的click事件，然后处理<p>元素的click事件。

### 4.5 循环添加鼠标监听事件

> 参考网址：https://blog.csdn.net/gyueh/article/details/106039090?utm_medium=distribute.pc_relevant.none-task-blog-baidujs_title-0&spm=1001.2101.3001.4242
>
> 参考网址：https://developer.mozilla.org/zh-CN/docs/Web/API/EventTarget/addEventListener

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <div>1</div>
    <div>2</div>
    <div>3</div>
    <div>4</div>
    <div>5</div>
    <div>6</div>
    <div>7</div>
</body>
<script>
    // 获取所有的div标签，并对这些标签都添加：鼠标监听事件
    var btn = document.getElementsByTagName("div");
    console.log("开始");
    for (var i = 0; i < btn.length; i++) {
        btn[i].addEventListener('click', function () {
            console.log('点击了第' +(i+1) +  '个按钮');
        })
    }
    console.log("结束");
</script>
</html>
```

控制台输出：

```
开始
结束
```

​	这个监听事件**在构建DOM的过程中，就已经对每个`div`元素节点注册（只有点击时，才会触发回调函数）**，而不是点击某个`div`节点才创建，当点击某个`div`节点时，就直接执行对应`div`节点的回调函数。

​	当鼠标点击某个`div`时，会触发回调函数：function ()： **console.log('点击了第' +(i+1) +  '个按钮')**

----

### 4.6 终止冒泡事件

​	事件冒泡：**当点击一个按钮后，此事件会一直向上传递**，称为事件冒泡。可能会对父类活根类型事件造成影响。

​	例如：点击【按钮】弹出一个框，点击空白处，弹框消失。由于【按钮】是<button>向上传递</button>的，点击后会同时触发  点击空白处弹框消失事件。

​	js禁止事件冒泡方法：在【按钮】的触发框事件种禁止冒泡

```js
$.('btn').on('click', function(e){
    e.stopPropagation();
    //click事件
});
```

## 5 childNodes

​		childNodes 属性返回：节点的<button>子节点集合</button>。

> ​	提示：您可以使用 length 属性来确定：子节点的数量，然后您就能够遍历所有的子节点并提取您需要的信息。
>
> ​	文档里**几乎每一样东西都是一个节点**，甚至连空格和换行符都会被解释成节点。而且都包含在childNodes属性所返回的数组中。

​	在使用原生的childNodes时，包括提供的：nodeValue，firstChild()，lastChild()等等。都需要注意其他元素类型的影响，因为我们通常操作的都是元素节点。而childNodes返回的集合中包含了很多意向不到的东西，在实际使用中和容易造成错误。

---

> 在使用childNodes时，最好通过nodeType将返回集合**过滤一遍再进行使用**，能够避免很多不必要的问题。

---

##  6 replace方法

​	JavaScript中replace() 方法如果直接用str.replace("-","!") 只会替换第一个匹配的字符；而str.replace(/\-/g,"!")则可以：替<button>换掉全部匹配的字符</button>（g：为全局标志）。 

​	str.replace(/\'-' /g,"!")： '-' 是你想要被替换的字符.

```java
 后端不让有 '+'(加号)，所以要替换：'+'为'2B%' 　　　　
  // %2B是+号的意思，通过GET方式传值的时候，+号会被浏览器处理为空，所以需要转换为%2b。
 URL = URL.replace(/\+/g,'2B%');
```

## 7 e.currentTarget

```js
click(e){
	//e.target  当前点击元素
	//e.currentTarget 当前绑定事件的元素
	
	//获取当前绑定事件的元素的父元素
	e.currentTarget.parentElement
	
	//获取绑定事件元素的子节点(包括元素，文本)
	e.currentTarget.childNodes
	
	//获取绑定事件元素的子节点(只包含元素)
	e.currentTarget.children
	
	//获得点击元素的前一个元素
	e.currentTarget.previousElementSibling
	
	//获取绑定事件元素的后一个元素
	e.currentTarget.nextElementSibling
	 
    //获取绑定事件元素的第一个子元素
    e.currentTarget.firstElementChild
	
	//获取绑定事件元素的最后一个子元素
	e.currentTarget.lastElementChild
	
	//获取绑定事件元素的子元素数量
	e.currentTarget.childElementCount
	
	//获取绑定事件元素里id为“str”的的元素
    e.currentTarget.getElementById("str")
	
	//获取绑定事件元素的str属性
    e.currentTarget.getAttributeNode('str')
}
```

----

# * `var let const`

## 1 `ES5`中`var`的写法

```js
function add(num1, num2){
	var sum = num1 + num2;
	return sum;
}
var result = add(10, 20);//这是输出的是30
alert(sum);//但是这里的调用是完全不行的
```

​	这里的`sum`就会被添加到`function`的**局部环境中**，之后不可再次调用：`sum`。

----

```js
var i = 0;
for(var i = 0 ; i < 10; i++){
}
// i：10，循环结束后，还是可以调用： 
console.log(i);
```

​	令人窒息的操作。`for`循环中：`i`是一个局部变量，循环结束后，外面还是可以继续调用：`i`。

## 2 `ES6`中的`let`

​	`ES6`中新增添的`let`和`const`都是支持：<button>块级作用域</button>。

- let在全局环境中声明的话就是全局变量，在局部环境中声明就是局部变量（在{}内有效，{}外是无效的）

```js
let i = 2;
for(let i = 0; i < 10; i++){
	...
}
alert(i)//这里输出的依旧是2
```

- let只能先声明，再进行调用；var可以先调用再声明

```js
//var这样用是可以的：
name = "asu";
var name;

//但是这样使用let是不可以的，let一定需要先声明
name = "asu";
let name;//错误的
```

- 在相同的作用域中let声明的变量是不可以重置的，也就是**不可以重复的声明**；但是var是可以对之前声明的变量进行重置的

```js
//var可以重置声明的变量：
var x = 3;
var x = 2;
alert(x)/这里输出的是2

//let则是不可以重置已经声明的变量的：
let y = 2;
let y = 3;//error，这是错误的
if(true){
	let y = 5;
	alert(y)//在局部环境中声明，环境不一样，输出5
}
alert(y);//依旧输出的是2
```

## 3 `ES6`中的`const`

- let在声明的时候可以不用初始化，`const`在声明的时候**必须要进行初始化**

```js
//这样声明是正确的
var name = "LiHua";

//声明的时候没有初始化是错误的：
var city;
city = "wuhan";//error
```

---

- `const`声明的变量是不可以修改的，但是这不是绝对意义上面的不可以的修改

  注意`const`**定义的常量的值是不可以再进行赋值修改的，但是`const`定义的数组和对象是可以进行添加和修改其中的元素的**

  **最最重要的是定义的数组和对象是不可以重新赋值的**；

  let定义的变量的值是可以修改的。

```js
const names = ["Bob", "Kathy", "LiHua"];
const person = {name:"LiLei", age:20};
//添加元素和修改元素都是可以的
names[0] = "Li";
names.push("Pob")
person.age = 21;
person.gender = "female";

//但是重新给数组和对象进行赋值是错误的
names = ["asu", "LiLi"];//error
person = {name:"Bob", age:18};//error
```

相同的是：
	(1)在相同的作用域中，都是不可以进行重复声明的，不可以重置声明

​	(2)用let ，`const`声明的全局变量不归window所有

---

# * Windows对象

​	JS中最大的对象，它描述的是一个**浏览器窗口**，一般要引用他的属性和方法时，不需要用“Window.XXX”这种形式，而是直接使用“XXX”。一个框架页面也是一个窗口。

	- windows.alert()   ——> alert() 
	- onload()

----

# * DOM事件类型

|    参数     |  事件接口  |      初始化      |
| :---------: | :--------: | :--------------: |
| HTMLEvents  | HTMLEvent  |   initEvent()    |
| MouseEvents | MouseEvent | initMouseEvent() |
|  UIEvents   |  UIEvent   |  initUIEvent()   |

---

## 1 创建事件

```js
<script>
    var HTMLEvent = document.createEvent("HTMLEvents");
    var UIEvent = document.createEvent("UIEvents");
    var MouseEvent = document.createEvent("MouseEvents");
</script>
```

## 2 事件初始化

> eg：initEvent()方法

---

语法：

```js
let event;
event.initEvent(eventType, canBubble, cancelable);
```

|    参数    | 描述                                 |
| :--------: | ------------------------------------ |
| eventType  | 字符串值。事件的类型                 |
| canBubble  | 事件是否气泡                         |
| cancelable | 是否可以用：preventDefault()取消事件 |

应用：

```js
<script>
    var MouseEvent = document.createEvent("MouseEvents");
    MouseEvent.initMouseEvent("mouseover",false,true);
</script>
```

## 3 事件触发响应

定义：

```
dispatchEvent()：给节点分派一个合成事件
```

用法：

```
要触发的对象.ddispatchEvent(event);
event：必填（分派的Event对象）
```

示例：

```html
<body>
	<div onclick = "testClick()" id="divA">
	</div>
</body>
<script type="text/javascript">
	function testClick(){
		alert("click");
	}
	var clickEvent = document.createEvent("MouseEvents");
	var clickElement = document.getElementById("divA");
	clickEvent.initMouseEvent("click",false,true);
	clickElement.dispatchEvent(clickEvent);
</script>
```

# * 给`HTML`标签添加属性

## 1 setAttribute

```js
document.getElementById("save").setAttribute("disabled","1");
```

```html
<div id="save" disable="1">a</div>
```

## 2 getAttribute

```js
document.getElementById("save").getAttribute("disabled") === "0"
```

## 3 removeAttribute

```js
document.getElementById("save").removeAttribute('disabled');
```

# * `class`和`ID`查询

```html
<div class="mydiv">   
</div>
```

```js
 let mydiv = document.querySelector('.mydiv');
```

# * 箭头函数

## 1 `map`方法

​	**概念**：返回一个新数组，数组中的元素为原始数组元素调用函数处理后的值，按照原始数组元素顺序依次处理元素。

> 注意：
>
> （1）map() 不会对空数组进行检测；
>
> （2）map() 不会改变原始数组

---

## 2 获取数组内各元素长度

```ts
const materials: string[] = [
        '11', '2', '113', '4222'
    ];
```

普通用法：

```ts
let a: number[] = 
    materials.map(function (element) {
        return element.length;
    });
print(a); // [2, 1, 3, 4]
```

