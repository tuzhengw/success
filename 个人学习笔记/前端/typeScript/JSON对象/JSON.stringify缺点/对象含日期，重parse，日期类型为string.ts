/**
 * 如果obj里面有时间对象，则JSON.stringify后再JSON.parse的结果
 * 时间将只是字符串的形式，而不是对象的形式
 */
function main() {
    var test = {
        name: 'a',
        date: [
            new Date(1536627600000),
            new Date(1540047600000)
        ],
    };
    var b = JSON.parse(JSON.stringify(test));
    // object
    console.log(typeof test.date[0]);
    // string
    console.log(typeof b.date[0]);
}
