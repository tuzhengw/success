/**
 * =====================================================
 * 作者：tuzw
 * 审核人员：liuyz
 * 创建日期:2022-06-15
 * 脚本入口:CustomJs
 * 功能描述
 * 		此脚本是所有spg、表单、dash和门户定制功能入口
 * 帖子地址：
 * =====================================================
 */
import {
	ctx, SZEvent, Component, throwInfo, throwError, showConfirmDialog, showFormDialog, showErrorMessage, showMenu, isEmpty, deepAssign, uuid, message,
	quoteExpField, showDialog, showSuccessMessage, showProgressDialog, rc, downloadFile, waitRender, showWaiting, assign, IDataProvider, formatDateFriendly,
	parseDate, formatDate, tryWait, rc_get, showWarningMessage, ServiceTaskPromise, wait
} from 'sys/sys';
import { IMetaFileCustomJS, InterActionEvent } from "metadata/metadata-script-api";
import { SpgDataGovernBasic, SpgDataGovernCreateTask, SpgDataGovernTaskList } from './ruleDealModel/ruleCheck/check-task/data-govern-mgr.js';
import { Rules_CustomActions } from './ruleDealModel/ruleDealModel.js?v=2022060612';

/**
 * 直接到导出的脚本里包含各种元数据类型的注入脚本。
 * 应用在创建对应类型的viewer时会自动将对应的脚本注入。
 */
export const CustomJS: { [type: string]: IMetaFileCustomJS } = {
	"create_check_task.spg": new SpgDataGovernCreateTask(),
	"task_list.spg": new SpgDataGovernTaskList(),
	"task_view.spg": new SpgDataGovernBasic(),
	"autoCheckRules.spg": new Rules_CustomActions(),
	"checkRuleMgrPage.spg": new Rules_CustomActions(),
}