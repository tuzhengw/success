/**
 * 20211130 liuyz
 * 将产品一些内置页面嵌入到前台门户中
 */
import { MMP_Data } from "dw/dwmgr";
import { Component, getUrlParameters, UrlInfo, assign } from "sys/sys";
import { MetaModulePageArgs, MetaFilesPageArgs } from "metadata/metamgr";
import { ToolbarArgs, TabbarArgs } from "commons/basic";
import { SettingPage_securityConf } from 'metadata/syssettings/syssettings-securityConf';

enum PagesType {
	/** 数据查看页面 */
	DATA = 'data'
}

/**
 * 产品内置页面类
 */
export class BuiltInPages extends Component {
	private url: string;
	private urlParams: JSONObject;
	/** 定位的URL参数 */
	private localUrlParams: JSONObject;
	private innerPage: Component;

	protected _init_default(args) {
		super._init_default(args);
		this.url = args.url;
		this.praseSearch();
	}

	private praseSearch() {
		this.urlParams = getUrlParameters(this.url);
		this.localUrlParams = getUrlParameters("/?" + this.urlParams.urlParams);
	}

	protected _init(args) {
		let domBase = super._init(args);
		let type = <PagesType>this.urlParams.type;
		switch (type) {
			case "data":
				this.createDataPage(domBase);
				break;
			case "security":
				this.createSecurityPage(domBase);
				break;
		}
		domBase.style.height = "100%"
		return domBase;
	}

	private createDataPage(domBase: HTMLElement) {
		let innerPage = this.innerPage = new SDI_MMP_Data({
			domParent: domBase,
			name: "data",
			projectName: this.urlParams.projectName,
			executeSQL: this.urlParams.executeSQL,
			localUrlParams: this.localUrlParams
		});
	}

	private createSecurityPage(domBase: HTMLElement) {
		let innerPage = this.innerPage = new SoleSystemSecurtiyAdaptPage({
			domParent: domBase
		});
	}
}

/**
 * 数据页面
 */
class SDI_MMP_Data extends MMP_Data {
	constructor(args: MetaModulePageArgs) {
		super(args);
		this.showUrl(assign({ path: "" }, { params: args.localUrlParams })).then((self) => { // 等待渲染后，校验是否有指定SQL内容
			if (!!args.executeSQL && args.executeSQL != '') {
				let sqlModelSqlEditor = self.filesBook.filesBook.getActiveComponent().getPanels().getCurrentPanel(); // filesBook 为当前页面编辑区域，内部含有多个小的fileBook
				let sqlEditor = sqlModelSqlEditor.editor.editor;
				sqlEditor.setValue(args.executeSQL);
			}
		});
	}

	public _init_default(args: MetaFilesPageArgs) {
		super._init_default(args);
	}

	public _init(args: MetaFilesPageArgs): HTMLElement {
		let domBase = super._init(args);
		domBase.style.height = "100%"
		return domBase;
	}

	protected _getToolbarArguments(): JSONObject {
		let json = <ToolbarArgs>super._getToolbarArguments();
		json.items && (json.items = json.items.filter(item => item.id != "switchview"));
		return json;
	}

	/**
	 * 路由到到某个模块页面去
	 */
	public navigate(urlInfo: UrlInfo): Promise<any> {
		return this.showUrl(urlInfo).then(() => {
			return this;
		});
	}
}

/**
 * 系统模块——安全页面
 */
export class SoleSystemSecurtiyAdaptPage extends SettingPage_securityConf {
	protected _init(args: any): HTMLElement {
		let domBase = super._init(args);
		return domBase;
	}

	/**
	 * @override
	 * 控制页面显示的模块，要求隐藏：回归测试、安全等保、阈值设置、日志记录
	 */
	protected getTabbarArgs(): TabbarArgs {
		return {
			layoutTheme: "texttabset",
			className: "syssettings-web-tabbar",
			keyPrefix: "sys.syssettings.securityConf",
			items: [
				{ id: 'passwordSettings' },
				{ id: 'loginSettings' },
				{ id: 'sessionSettings' },
				// { id: 'securityLevelProtection' },
				// { id: 'thresholds' },
				// { id: 'regressionTesting' },
				// { id: 'log' },
				{ id: 'masking' },
				{ id: 'algorithms' },
				// { id: 'sys' }
			],
			value: 'passwordSettings', // 默认选项不可以为空
		};
	}
}