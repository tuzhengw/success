/**
 * 如果obj里有RegExp(正则表达式的缩写)、Error对象
 * 则序列化的结果将只得到：空对象
 */
function main() {
    const test = {
        name: 'a',
        date: new RegExp('\\w+'),
    };
    const copyed = JSON.parse(JSON.stringify(test));
    test.name = 'test'
    // { name: 'test', date: /\w+/ }
    console.log(test);
    // { name: 'a', date: {} }
    console.log(copyed);
}