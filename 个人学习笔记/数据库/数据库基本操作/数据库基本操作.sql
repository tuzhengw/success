﻿# 1. 查询所有学分大于3的课程：课程号和课程名
select Cno, Cname from Course where Ccredit > 3;

# 2. 查询部门是"CS"的学生：学号和学生姓名
select Sno, Sname from student where Sdept = 'CS';

# 3. 查询5号课程的平均分
select 
	AVG(S.Grade) as AVG 
from 
	SC as S, Course as C 
where 
	S.Cno = C.Cno and C.Cpno = 5;

# 4. 删除没有成绩的选课记录
delete from SC where Grade is null;

# 5. 创建所有学分等于3的课程试图：vwCourse
create view vwCourse
as
select * from Course where Ccredit = 3;
# 查视视图
-- select * from vwCourse
# 删除视图
-- drop view vwCourse;
# 更新视图
-- alter view vwCourse
-- as
-- select Cname,Cpno,Ccredit
-- from Course where Ccredit = 3;

# 6. 查询学号为1003的同学的选课成绩大于90分的课程号和课程名
select Cno, Cname from 	Course where 	Cno in(
		select Cno from SC where Sno="1003" and Grade > 90
);

# 7. 创建SC表（Sno，Cno）为主键，成绩取值在0到100之间
create table SaC(
-- 	id int auto_increment,
	Sno char(20),
	Cno char(20),
	primary key(Sno, Cno),
	foreign key(Sno) references to Student(Sno),
	foreign key(Cno) references to Course(Cno)
) engine=MyISAM default charset=utf-8;
# 修改表名
alter table oleName rename to newName;
# 修改字符集
alter table 表名 character set 字符集名 default collate 校对规则
# 修改字段名称
alter table 表名 change 旧字段名 新字段名 类型(大小）
# 修改数据类型
alter table 表名 modify 字段名 数据类型（大小）
-- alter table test modify id int(120);
# 删除字段
alter table 表名 drop 字段名


# 8. 将所有女生的选课成绩增加2分
update SC  set Grade = Grade+2 where Sno = (
	select Sno from Student where Ssex = '女'
);





