/**
 * 复制实例表数据
 */
export function copyDataToInstTable(request: HttpServletRequest, response: HttpServletResponse, params: any) {
    let srcTableId = params.src;
    let destTableIds = params.dests;
    let srcTable = meta.getJSON(srcTableId);
    destTableIds.forEach(destTableId => {
        let destTable = meta.getJSON(destTableId);
        let args: any = {};
        args.srcDataSource = srcTable.properties.datasource;
        args.destDataSource = destTable.properties.datasource;
        args.async = false;
        args.importMode = 2;
        args.createTargetTable = false;
        args.dropTargetTable = true;
        args.tables = [{
            tableName: srcTable.properties.dbTableName, targetTableName: destTable.properties.dbTableName
        }];
        etl.copyTables(args);
    })
    return { success: true }
}