export const CustomJS: { [type: string]: IMetaFileCustomJS } = {
    /**
     * 当前项目的所有spg都会进到此方法
     */
    spg: {
        CustomActions: {
            /** 
             * tuzw test
             * 测试获取页面某个过滤组件输入的参数
             * 场景：全部导出
             */
            test_getPageFilterCondition: (event: InterActionEvent) => {
                /**
                 * 获取全局字段过滤组件，id：fieldsFilter1
                 * builder为：右侧选择的属性
                 * page中包含整个SPG控件
                 */
                let fieldsFilter1: IVComponent = event.page.getComponent("fieldsFilter1");
                /** 
                 * 获取过滤组件的过滤条件
                 * 注意：查询出来为字符串，必须JSON.parse
                 * {"clauses":[{"leftExp":"XM","operator":".....
                 * 由于设计多个字段，而且是and关系，若只想过滤一个字段，其他字段则为：null，会查询不到数据，需要处理成：exp
                 */
                let allImportFilterCondition = JSON.parse(fieldsFilter1.getValue());
                /** model1.SFZJHM = '642224199610043424'  */
                let importFilterConditionExp = fieldsFilter1.values.filterExp;

                
            }
        }
    }
}
