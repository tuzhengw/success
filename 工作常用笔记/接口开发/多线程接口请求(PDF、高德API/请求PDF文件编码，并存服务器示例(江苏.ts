/**
 * =====================================================
 * 作者：tuzw
 * 审核人员：liuyz
 * 创建日期:2022-03-02
 * 脚本入口:CustomJs
 * 功能描述
 * 		此脚本是制证中心电子证照pdf数据落地应用服务器脚本
 * 帖子地址：https://jira.succez.com/browse/CSTM-17994
   项目地址：http://192.168.9.13:8037/ythpt/ZHJG/files?:open=uiIU5RjwOGEAi7KHKpgOjB
 * 
 * 20220527 tuzw
 * 需求更改：因为食品经营许可证数据的上报流程比较复杂，这样不方便管理，希望在原脚本现有的功能上，增加文件转换成功后直接上传到总局的功能，简化非结构化数据的上报流程
 * 帖子地址：https://jira.succez.com/browse/CSTM-18955?page=com.atlassian.jira.plugin.system.issuetabpanels%3Aall-tabpanel
 * =====================================================
 */
import db from "svr-api/db"; //数据库相关API
import http from "svr-api/http";
import sys from "svr-api/sys";
import fs from "svr-api/fs";
import metadata from "svr-api/metadata";
import File_ = java.io.File;
import { FtpClient } from "svr-api/types/ftp.types";
import ftp from "svr-api/ftp";

/** 一次查询的最大数量 */
const QUERY_MAX_NUMS = 500;
/** 请求地址 */
const HOST_ADDRESS = 'xxxxx';
/** 请求PDF数据地址 */
const REQUEST_PDF_DATA_URL = `${HOST_ADDRESS}/license-app/v1/license/archive`;
/** 请求TOKEN值地址 */
const REQUEST_TOKEN_URL = `${HOST_ADDRESS}/license-app/v1/security/login`;
/** 数据源名 */
const DATA_BASE_NAME = "SZ_TEST_VERTICA";
/** 共享国家食品经营表 */
const GX_GJ_SPJY_TABLE_NAME = "GX_GJ_SPJY";
/** 共享国家食品经营日志表 */
const GX_GJ_SPJY_LOG_TABLE_NAME = "GX_GJ_SPJY_LOG";
const SCHEMA = "public"
/** 临时存储目录 */
const SAVE_FILE_PATH = `/home/tomcat_4/workdir/gj_spjy_pdf/`;
/** 格式化时间戳 */
const formatStamp = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
/** 格式化年月日 */
const formatDate = new java.text.SimpleDateFormat("yyyyMMdd");
/** 手动控制程序运行文本地址 */
const Is_StopRunScriptPath = "/ZHJG/script/sendPdfFile/isStopGetPdfData.txt";

/**
 * 入口
 */
export function getPdfFileData(access_token: string): void {
    let startTime: number = new Date().getTime();
    if (!access_token) {
        print(`getPdfFileData()，获取文件信息access_token为空`);
        return;
    }
    print(`getPdfFileData()，制证中心电子证照pdf数据落地应用服务器--------start`);
    /** 处理的总数量 */
    let needDealNums: number = getNeedDealShareFoodNums();
    if (needDealNums == 0) {
        print(`需要处理的总条数为：${needDealNums}，结束执行`);
        return;
    }
    // 由于文件数量比较大，一旦启动，必须等待数据跑完才会停止，不太方便，改为：读取【指定文件】的内容，若为：true，则停止执行
    let isStop: string = metadata.getString(Is_StopRunScriptPath);

    /** 执行SQL查询的次数 */
    let needQueryDataNums: number = Math.ceil(needDealNums / QUERY_MAX_NUMS);
    /** 线程池核心执行数量 */
    const corePoolSize: number = 10;
    let threadPool = sys.getThreadPool({ // 获取一个线程池
        name: "requestPdfData", // 线程池
        corePoolSize: corePoolSize,
        maximumPoolSize: 10
    });
    let threadTask: Array<Task> = [];
    for (let queryNum = 0; queryNum < needQueryDataNums; queryNum++) {
        print(`需要处理的总条数为：${needDealNums}，每次处理条数为：${QUERY_MAX_NUMS}，共需要处理：${needQueryDataNums}次，开始分配线程：【${queryNum + 1}】`);
        isStop = metadata.getString(Is_StopRunScriptPath);
        if (isStop == "true") {
            print(`最外层循环，制证中心电子证照pdf数据落地应用服务器，被手动终止-end`);
            break;
        }
        let shareFoodInfos: GX_GJ_SPJY[] = getShareFoodInfos(queryNum * QUERY_MAX_NUMS);
        /** 正在运行的线程个数 */
        let runingThreadNums: number = 0;
        do {
            runingThreadNums = threadPool.getActiveCount();
            print(`当前线程池运行数量为：${runingThreadNums}，线程池可执行最大数量为：${corePoolSize}`);
            if (runingThreadNums >= corePoolSize) {
                print(`当前线程池运行数量已经大于【${corePoolSize}】，需要等待线程池释放`);
                sys.sleep(100000);
            }
            isStop = metadata.getString(Is_StopRunScriptPath);
            if (isStop == "true") {
                let endTime: number = Date.now();
                print(`do-while 制证中心电子证照pdf数据落地应用服务器，被手动终止，花费时间：${(endTime - startTime) / 1000}秒-end`);
                return;
            }
            if (runingThreadNums < corePoolSize) { // 若线程池数量 还有空闲，则不循环等待
                break;
            }
        } while (runingThreadNums >= corePoolSize); // 若当前没有空余的线程，则等待
        threadPool.submit({ // 线程池中执行，批处理脚本不会打印日志，得在【系统日志】查看
            method: "sendRequestGetPdf", // 指定路径的脚本方法（export）
            params: [shareFoodInfos, access_token, queryNum % corePoolSize],
            path: "/ZHJG/script/sendPdfFile/sendPdfFile.action"
        });
        print(`分配线程：【${queryNum + 1}】 成功`);
    }
    print(`考虑到此脚本与其他脚本依赖，避免脚本分配线程后就结束执行，这里等待所有线程执行完后，结束此脚本，当前线程池运行数量为：${threadPool.getActiveCount()}`);
    /**
     * 20220823 tuzw
     * 考虑到此脚本与其他脚本依赖，避免脚本分配线程后就结束执行，这里等待所有线程执行完后，结束此脚本
     */
    while (threadPool.getActiveCount() != 0) {
        isStop = metadata.getString(Is_StopRunScriptPath);
        if (isStop == "true") {
            print(`制证中心电子证照pdf数据落地应用服务器，被手动终止-end`);
            break;
        }
        print(`当前线程池运行数量为：${threadPool.getActiveCount()}，线程运行中...`);
        sys.sleep(100000);
    }
    let endTime: number = new Date().getTime();
    print(`getPdfFileData()，制证中心电子证照pdf数据落地应用服务器，花费时间：${(endTime - startTime) / 1000}秒--------end`);
}

/**
 * 20220304 tuzw
 * 问题：由于文件比较多，改为多线程处理（调用一次方法，请求500条数据）
 * 请求第三方接口，获取PDF base64编码信息，并存入到服务器中
 * @param shareFoodInfo 请求的数据
 * @param access_token token
 * @param executeId 第几次执行该方法（多线程记录）
 * @return true | false
 */
export function sendRequestGetPdf(shareFoodInfos: GX_GJ_SPJY[], access_token: string, executeId: number): boolean {
    print(`sendRequestGetPdf()，当前线程：${executeId + 1}，开始请求第三方接口，获取PDF base64编码信息，并存入到服务器中，当前处理数据条数为：${QUERY_MAX_NUMS}条数据--start`);
    let startTime: number = Date.now();
    if (!!shareFoodInfos && shareFoodInfos.length == 0) {
        print(`共享国家食品经营数据为空，结束执行`);
        return true;
    }
    /** 是否手动停止允许 */
    let isStop: string = "";
    /** 是否网络、超时等问题，导致请求失败 */
    let isRequestError: boolean = false;
    let sdfDate = new java.text.SimpleDateFormat("yyyy-MM-dd");
    let nowDate: string = sdfDate.format(new java.util.Date(Date.now()));

    for (let i = 0; i < shareFoodInfos.length && !isRequestError; i++) {
        isStop = metadata.getString(Is_StopRunScriptPath);
        if (isStop == "true") {
            print(`线程：${executeId + 1}， 制证中心电子证照pdf数据落地应用服务器，被手动终止-end`);
            break;
        }
        if (i == 0 || i % 20 == 0) {
            print(`线程：${executeId + 1}，第【${i + 1}次请求第三方接口`);
        }
        let shareFoodInfo = shareFoodInfos[i];
        let auto_code = shareFoodInfo.AUTH_CODE;
        if (!auto_code) {
            print(`线程：${executeId + 1}，电子证照查验码【${auto_code}】为空，跳过处理`);
            continue;
        }
        let dealStatusLog: GX_GJ_SPJY_LOG = {
            AUTH_CODE: auto_code,
            LIC_NO: shareFoodInfo.LIC_NO,
            ISSU_DATE: shareFoodInfo.ISSU_DATE,
            TS_TIME: formatStamp.format(new java.util.Date())
        };
        let requestUrl: string = `${REQUEST_PDF_DATA_URL}?auth_code=${auto_code}&access_token=${access_token}`;
        try {
            let httpRequestResult: ResponseResult = JSON.parse(http.get(requestUrl));
            let ack_code: string = httpRequestResult.ack_code;
            let formatIssuDate: string = formatDate.format(new java.util.Date(new Date(shareFoodInfo.ISSU_DATE).getTime()));
            let fileName: string = `${shareFoodInfo.LIC_NO}_${formatIssuDate}.pdf`;

            dealStatusLog.ACK_CODE = ack_code;
            dealStatusLog.ERRORS = typeof httpRequestResult.errors != "string" && !!httpRequestResult.errors
                ? httpRequestResult.errors.join(",")
                : httpRequestResult.errors;
            dealStatusLog.FILE_NAME = fileName;

            if (ack_code == "SUCCESS") {
                print(`电子证照查验码【${auto_code}】请求成功，开始创建PDF文件信息，文件名：${fileName}`);
                let fileBase64: string = httpRequestResult.data.file_data;

                dealStatusLog.SEND_CODE = "0";
                if (!fileBase64) {
                    dealStatusLog.ERRORS = "base 64编码为空";
                    dealStatusLog.ACK_CODE = "ERROR";
                    print(`电子证照查验码【${auto_code}】，获取的Base 64 编码为空`);
                } else {
                    /**
                     * 20220531 
                     * 问题：按照当天时间对文件进行分类，存在：若网络异常，当天数据推送失败时，再次推送必须手动执行，而且必须再当天手动，否则，需要调整代码，指定对应的日期文件夹
                     * 改进：由于父目录是固定的，可直接通过文件名定位文件位置，eg：/test_pdf/pdf1.pdf，不使用动态时间来创建文件夹存放文件
                     * 
                     * 20220701 tuzw
                     * 1）FTP推送不稳定，将文件所存路径固定，避免漏掉文件
                     * 2）经过测试，FTP推送暂不投入使用，改为将文件请求到我们服务器，由实施部同事将其推送
                     */
                    // let isCreateSuccess: boolean = fileUtils.pdfBase64ToPdf(fileBase64, `${SAVE_FILE_PATH}/${fileName}`); // 
                    let isCreateSuccess: boolean = fileUtils.pdfBase64ToPdf(fileBase64, `${SAVE_FILE_PATH}${nowDate}/${fileName}`); // todo 请求前阔以先校验文件是否存在，若存在，则不请求
                    if (isCreateSuccess) {
                        dealStatusLog.SEND_CODE = "1";
                        print(`【${fileName} 】存储路径为：${SAVE_FILE_PATH}${fileName}`);
                    } else {
                        dealStatusLog.ACK_CODE = "ERROR";
                        dealStatusLog.ERRORS = "文件创建失败";
                        print(`电子证照查验码【${auto_code}】文件创建失败`);
                    }
                }
            } else {
                print(`电子证照查验码【${auto_code}】请求失败，错误原因：`);
                print(httpRequestResult);
            }
        } catch (e) {
            dealStatusLog.ERRORS = "请求接口失败";
            isRequestError = true;
            console.error(`getPdfFileData()，请求第三方接口获取PDF base64编码请求失败，终止程序--error`);
            console.error(e);
        }
        insertShareFoodInfoLog(dealStatusLog); // 由于文件比较多，改为请求一次，插入一条数据
    }
    let endTime: number = Date.now();
    print(`sendRequestGetPdf()，当前线程：${executeId + 1}，开始请求第三方接口，获取PDF base64编码信息，并存入到服务器中，当前处理数据条数为：${QUERY_MAX_NUMS}条数据，共耗时：【${(endTime - startTime) / 1000}】s--end`);
    return isRequestError;
}

/**
 * 将成功处理的数据，插入到共享—国家—食品经营信息日志表
 * @param successData 成功处理的数据
 * @return true | false
 */
function insertShareFoodInfoLog(successData: GX_GJ_SPJY_LOG): void {
    print(`insertShareFoodInfoLog()，将成功处理的数据，插入到共享—国家—食品经营信息日志表--start`);
    if (!successData) {
        return;
    }
    let ds = db.getDataSource(DATA_BASE_NAME);
    let table = ds.openTableData(GX_GJ_SPJY_LOG_TABLE_NAME, SCHEMA);
    let successInsertNums: number = table.insert(successData);
    print(`insertShareFoodInfoLog()，一共将【${successInsertNums}】条数据成功插入到到共享—国家—食品经营信息日志表-end`);
}

/** 获取需要处理【共享—国家—食品经营信息】的总条数SQL */
const QUERY_NEED_DEAL_MAX_NUMS_SQL =
    `SELECT COUNT(*) AS MaxNums FROM public.GX_GJ_SPJY t0 
            WHERE NOT EXISTS (
                SELECT 1 FROM public.GX_GJ_SPJY_LOG t1
                    WHERE  t1.ACK_CODE ='SUCCESS' AND t0.AUTH_CODE = t1.AUTH_CODE
            )`;
/**
 * 获取需要处理【共享—国家—食品经营信息】的总条数
 */
function getNeedDealShareFoodNums(): number {
    let ds = db.getDataSource(DATA_BASE_NAME);
    let queryData = ds.executeQuery(QUERY_NEED_DEAL_MAX_NUMS_SQL)[0];
    return !!queryData ? queryData["MaxNums"] : 0;
}

/** 查询文件没有创建成功的共享—国家—食品经营信息 */
const QUERY_SHARE_FOOD_INFO_SQL =
    `SELECT * FROM public.GX_GJ_SPJY t0 
            WHERE NOT EXISTS (
                SELECT 1 FROM public.GX_GJ_SPJY_LOG t1
                    WHERE  t1.ACK_CODE ='SUCCESS' AND t0.AUTH_CODE = t1.AUTH_CODE
            )
            ORDER BY ISSU_DATE
            LIMIT ? OFFSET ?`;

/**
 * 获取共享—国家—食品经营信息
 * @param offset 查询起始页，0：第一页
 * @return []
 */
function getShareFoodInfos(offset: number): GX_GJ_SPJY[] {
    let ds = db.getDataSource(DATA_BASE_NAME);
    let queryData = ds.executeQuery(QUERY_SHARE_FOOD_INFO_SQL, [QUERY_MAX_NUMS, offset]) as GX_GJ_SPJY[];
    return queryData;
}

/**
 * 获取token值
 */
export function getToken(): ResultInfo {
    let requestResult: ResultInfo = { result: true };
    try {
        let paramJson = {
            url: REQUEST_TOKEN_URL,
            data: {
                "app_key": "kTaTjmLXeiRqyYi",
                "app_secret": "hhBZnbKwqXbogyX",
                "account": "sj_sjsc",
                "password": "AHydutcekVOwITG"
            },
            headers: {
                "Content-Type": "application/json"
            }
        };
        let httpRequestResult: HttpResponseResult = http.post(paramJson);
        if (httpRequestResult.httpCode == 200) {
            requestResult = JSON.parse(httpRequestResult.responseText);
            requestResult.result = true;
            print(`获取token成功，token：【${requestResult.access_token}】`);
        } else {
            requestResult.result = false;
            requestResult.message = `--请求错误--`;
            print(httpRequestResult);
        }
    } catch (e) {
        requestResult.result = false;
        console.error(`getToken()，获取token值失败-error`);
        console.error(e);
    }
    return requestResult;
}

/** 返回值 */
export interface ResultInfo {
    /** 返回结果 */
    result?: boolean;
    /** 错误码 */
    errorCode?: string;
    /** 消息 */
    message?: string;
    /** 错误数据 */
    errorData?: any;
    /** 过期时间，单位为秒，每次时间为10分钟 */
    expires_id?: number,
    /** token值 */
    access_token?: string;
    /** 状态码 */
    code?: number;
}

/** 共享国家食品经营  */
interface GX_GJ_SPJY {
    /** 电子证照查验码 */
    AUTH_CODE: string;
    /** 许可证编号 */
    LIC_NO: string;
    /** 发证日期（许可证上打印的发证日期） */
    ISSU_DATE: string;
    ETL_TIME?: string;
}

/** FTP参数 */
interface UploadFileInfo {
    fileId: string;
    /**目标服务器路径地址 */
    targetPath: string;
    /**文件名称 */
    fileName: string;
    /**文件内容 */
    fileContent: JdbcBlob;
}

/** 共享国家食品经营日志对象 */
interface GX_GJ_SPJY_LOG {
    /** 电子证照查验码 */
    AUTH_CODE: string;
    /** 许可证编号 */
    LIC_NO: string;
    /** 颁发日期 */
    ISSU_DATE: string | number;
    /** 处理结果码 */
    ACK_CODE?: string;
    /** 错误日志信息-只存错误信息 */
    ERRORS?: any;
    /** 存放服务器中的文件名称 */
    FILE_NAME?: string;
    /** 接口返回时间 */
    TS_TIME: string | number;
    /** 是否推送到指定服务器 */
    SEND_CODE?: string;
}

/** 返回对象 */
interface ResponseResult {
    /** 是否成功，eg：SUCCESS */
    ack_code: string;
    errors: string;
    sign: string;
    timestamp: string;
    correlation_id: string;
    response_id: string;
    data: {
        /** 文件名 */
        file_name: string;
        /** 文件Base64编码 */
        file_data: string;
    };
    [propname: string]: any;
}
/**
 * 20220209 tuzw
 * 文件工具类
 */
class FileUtils {

    /**
     * 显示指定目录下的子文件和文件夹情况
     * @param dirPath 目录路径，eg：/tempSaveZipFile/
     */
    public showFolderOrFile(dirPath: string): void {
        if (!dirPath) {
            print(`showFolderOrFile()，传入的路径不合法：【${dirPath}】`);
            return;
        }
        let dir = new File_(dirPath);
        if (!dir.isDirectory()) {
            print(`showFolderOrFile()，传入的路径不是一个目录`);
            return;
        }
        print("当前目录下的子文件和文件夹情况：");
        let childFolders: File_[] = dir.listFiles();
        if (childFolders.length > 0) {
            for (let i = 0; i < childFolders.length; i++) {
                print(childFolders[i].getAbsolutePath());
            }
        }
    }

    /**
     * 读取url文件二进制流，创建指定类型文件
     * @param fileName 绝对路径，eg：/tempSaveZipFile/t.doc
     * @return true | false
     */
    public createFile(filePath: string, fileIoByte: number[]): boolean {
        if (!filePath) {
            return false;
        }
        let isSuccess: boolean = true;
        let file = new File_(filePath);
        if (!file.exists()) {
            print(`文件：${filePath} 不存在，开始创建`);
            let parentFile = file.getParentFile();
            if (!parentFile.exists()) {
                parentFile.mkdirs();    // 若文件父目录不存在，则创建文件以及父文件目录
            }
            file.createNewFile(); // 创建真实的文件
        }
        file = fs.getFile(filePath); // 读取文件
        let outStream = file.openOutputStream();
        try {
            outStream.write(fileIoByte as number[]);
        } catch (e) {
            file.delete();
            isSuccess = false;
            print(`--写入文件${filePath}内容失败---`);
            print(e);
        } finally {
            outStream.close();
        }
        return isSuccess;
    }

    /**
     * 读取pdfBase64 文件字节，并存储到指定PDF文件中
     * 参考：http://www.javashuo.com/article/p-oayfxpeu-du.html
     * 
     * @param pdfBase64 PDF图片base64编码
     * @param filePath pdf文件绝对路径，eg：/tempSaveZipFile/t.pdf
     * 
     * 注意：IDEA报常量字符串过长，解决办法；
     *   File -> Settings -> Build,Execution,Deployment -> Compiler -> Java Compiler ，  Use Compiler, 选择Eclipse - Apply
     */
    public pdfBase64ToPdf(pdfBase64: string, filePath: string): boolean {
        if (!pdfBase64 || !filePath) {
            print(`pdfBase64ToPdf()，解析PDF base64编码参数错误`);
            return false;
        }
        pdfBase64 = pdfBase64.replace(' ', '+');
        let file = new File_(filePath);
        if (file.isDirectory()) {
            print(`pdfBase64ToPdf()，解析PDF base64编码并存入指定文件失败，${filePath}是一个目录，需要指定存入PDF文件绝对路径`);
            return false;
        }
        let fileBytes: number[] = java.util.Base64.getDecoder().decode(pdfBase64);
        let fileType: string = this.checkBase64BytesFileType(fileBytes);
        if (fileType != "pdf") {
            print(`pdfBase64ToPdf()，解析PDF  base64编码 文件类型为：${fileType}，不是PDF文件`);
            return false;
        }
        print(`pdfBase64ToPdf()，解析成功，解析的字节大小为：${fileBytes.length}`);
        return this.createFile(filePath, fileBytes);
    }

    /**
     * 根据Base64来校验文件类型
     * 思路：
     * （1）根据前两个字节（文件头）来校验文件格式
     * @param base64ImgData base64字符串编码
     * return 
     */
    public checkImageBase64FileType(base64ImgData: string): string {
        if (!base64ImgData) {
            return "";
        }
        let fileBytes: number[] = java.util.Base64.getDecoder().decode(base64ImgData);
        let fileType: string = "";
        if (0x424D == ((fileBytes[0] & 0xff) << 8 | (fileBytes[1] & 0xff))) {
            fileType = "bmp";
        }
        if (0x8950 == ((fileBytes[0] & 0xff) << 8 | (fileBytes[1] & 0xff))) {
            fileType = "png";
        }
        if (0xFFD8 == ((fileBytes[0] & 0xff) << 8 | (fileBytes[1] & 0xff))) {
            fileType = "jpg";
        }
        if (0x2550 == ((fileBytes[0] & 0xff) << 8 | (fileBytes[1] & 0xff))) {
            fileType = "pdf";
        }
        print(`checkBase64BytesFileType()，base64编码对应的文件类型为：${fileType}`);
        return fileType;
    }

    /**
     * 根据Base64来校验文件类型
     * 思路：
     * （1）根据前两个字节（文件头）来校验文件格式
     * @param fileBytes base64编码字节数组
     * return 
     */
    public checkBase64BytesFileType(fileBytes: number[]): string {
        if (!fileBytes || fileBytes.length == 0) {
            return "";
        }
        let fileType: string = "";
        if (0x424D == ((fileBytes[0] & 0xff) << 8 | (fileBytes[1] & 0xff))) {
            fileType = "bmp";
        }
        if (0x8950 == ((fileBytes[0] & 0xff) << 8 | (fileBytes[1] & 0xff))) {
            fileType = "png";
        }
        if (0xFFD8 == ((fileBytes[0] & 0xff) << 8 | (fileBytes[1] & 0xff))) {
            fileType = "jpg";
        }
        if (0x2550 == ((fileBytes[0] & 0xff) << 8 | (fileBytes[1] & 0xff))) {
            fileType = "pdf";
        }
        print(`checkBase64BytesFileType()，base64编码对应的文件类型为：${fileType}`);
        return fileType;
    }
}
const fileUtils = new FileUtils();