class Utils {

    /**
     * 校验两个对象是否相等（包含：JSON对象）
     */
    public compare(objA, objB) {
        if (!this.isObj(objA) || !this.isObj(objB)) { // 判断类型是否正确
            return false;
        }
        if (this.getLength(objA) != this.getLength(objB)) { // 判断长度是否一致
            return false;
        }
        return this.compareObj(objA, objB, true); // 默认为true
    }

    /**
     * 递归校验两个对象是否相等
     * @param objA 比较对象1
     * @param objB 比较对象2
     * @param 是否相等，初始默认true
     */
    private compareObj(objA, objB, flag) {
        for (let key in objA) {
            if (!flag) { //跳出整个循环
                break;
            }
            if (!objB.hasOwnProperty(key)) {
                flag = false; break;
            }
            if (!this.isArray(objA[key])) { // 子级不是数组时,比较属性值
                if (objB[key] != objA[key]) {
                    flag = false; break;
                }
            } else {
                if (!this.isArray(objB[key])) {
                    flag = false; break;
                }
                let oA = objA[key], oB = objB[key];
                if (oA.length != oB.length) {
                    flag = false; break;
                }
                for (let k in oA) {
                    if (!flag) // 这里跳出循环是为了不让递归继续
                        break;
                    flag = this.compareObj(oA[k], oB[k], flag);
                }
            }
        }
        return flag;
    }

    /**
     * 校验是否为对象
     */
    public isObj(object) {
        return object && typeof (object) == 'object' && Object.prototype.toString.call(object).toLowerCase() == "[object object]";
    }
    /**
     * 校验是否为数组
     */
    public isArray(object) {
        return object && typeof (object) == 'object' && object.constructor == Array;
    }

    /**
     * 获取对象长度
     */
    public getLength(object) {
        let count = 0;
        for (let i in object) count++;
        return count;
    }
}
