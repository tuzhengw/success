# java 解析 cron 表达式 cron-utils 使用 方式

# 1 cron Text表达式

```
*/45 * * * * ?
```

```
0 0/1 * * * ?   每隔1分钟
```

# 2 Java解析

```xml
<!-- 注意: 这里必须要使用 9.1.6 及以上版本, 之前的版本有严重漏洞 -->
<dependency>
    <groupId>com.cronutils</groupId>
    <artifactId>cron-utils</artifactId>
    <version>9.1.6</version>
</dependency>
```

```java
import com.cronutils.model.definition.CronDefinition;
import com.cronutils.model.definition.CronDefinitionBuilder;
import com.cronutils.model.time.ExecutionTime;
import com.cronutils.parser.CronParser;
import java.time.Duration;
import java.time.ZonedDateTime;
import java.util.Optional;

import static com.cronutils.model.CronType.QUARTZ;

public class CronDemo {
    public static void main(String[] args) {
        CronDefinition cronDefinition = CronDefinitionBuilder.instanceDefinitionFor(QUARTZ);
        CronParser parser = new CronParser(cronDefinition);
        ExecutionTime executionTime = ExecutionTime.forCron(parser.parse("*/45 * * * * ?"));
        Optional<ZonedDateTime> zonedDateTime = executionTime.nextExecution(ZonedDateTime.now());
        System.err.println("下次执行时间: " + zonedDateTime.toString());
        Optional<ZonedDateTime> zonedDateTime1 = executionTime.lastExecution(ZonedDateTime.now());
        System.err.println("最后执行时间: " + zonedDateTime1.toString());
        ZonedDateTime timeForLast = ZonedDateTime.now();
        Optional<Duration> duration = executionTime.timeFromLastExecution(timeForLast);
        System.err.println("最后一次执行时间过去了: " + duration + " 秒");
        Optional<Duration> duration1 = executionTime.timeToNextExecution(ZonedDateTime.now());
        System.err.println("距离下次执行时间: " + duration1);
    }
}
```

# 3 Ts-java版解析

```ts
import CronDefinition = com.cronutils.model.definition.CronDefinition;
import CronDefinitionBuilder = com.cronutils.model.definition.CronDefinitionBuilder;
import ExecutionTime = com.cronutils.model.time.ExecutionTime;
import CronParser = com.cronutils.parser.CronParser;
import Duration = java.time.Duration;
import ZonedDateTime = java.time.ZonedDateTime;
import Optional = java.util.Optional;
import QUARTZ = com.cronutils.model.CronType.QUARTZ;
import LocalDateTime = java.time.LocalDateTime;
import ZoneOffset = java.time.ZoneOffset;

function parseCronText(): string {
    let cronDefinition = CronDefinitionBuilder.instanceDefinitionFor(QUARTZ);
    let parser = new CronParser(cronDefinition);
    let executionTime: ExecutionTime = ExecutionTime.forCron(parser.parse("*/45 * * * * ?"));
    let zonedDateTime: Optional<ZonedDateTime> = executionTime.nextExecution(ZonedDateTime.now());
    let nextExecuteTime: LocalDateTime = zonedDateTime.get().toLocalDateTime();
    print(`下次执行时间: ${nextExecuteTime}`);
    let timeMilli: number = nextExecuteTime.toInstant(ZoneOffset.of("+8")).toEpochMilli();
    print(`下次执行时间毫秒数：${timeMilli} ms`);

    let zonedDateTime1: Optional<ZonedDateTime> = executionTime.lastExecution(ZonedDateTime.now());
    print("最后执行时间: " + zonedDateTime1.toString());
    let timeForLast: ZonedDateTime = ZonedDateTime.now();
    let duration: Optional<Duration> = executionTime.timeFromLastExecution(timeForLast);
    print("最后一次执行时间过去了: " + duration + " 秒");
    let duration1: Optional<Duration> = executionTime.timeToNextExecution(ZonedDateTime.now());
    print("距离下次执行时间: " + duration1);
    return "";
}
```

执行结果

```ts
开始执行脚本
下次执行时间: 2022-07-12T09:27
下次执行时间毫秒数：1657589220000 ms
最后执行时间: Optional[2022-07-12T09:26:45+08:00[Asia/Shanghai]]
最后一次执行时间过去了: Optional[PT12.814S] 秒
距离下次执行时间: Optional[PT2.185S]
脚本运行结束
```

