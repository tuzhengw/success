# 钻石操作符（<>）

## * 参数化类型



## `JDK 5`之前

​	在Java 1.5之前，`Collections API `仅支持：《原始类型》 - 在构造集合时，无法对参数化类型的参数进行参数化：

```java
List cars = new ArrayList();
cars.add("car"); // 转换为：Object
cars.add(new Integer(1));
// Object
System.out.println(cars.get(0).getClass().toString());
```

​	最终都会转为《Object实例》，所以《可以添加任何类型》并在运行时会导致<button>潜在的转换异常</button>

---

## `JDK 5` - 泛型

​	在Java 1.5中，引入了泛型。

​	泛型：**允许我们**在声明和构造对象时**指定类型参数**

```java
List<String> cars = new ArrayList<String>();
```

​	**在构造函数中指定参数化类型**，代码可读性会有点差：

```java
Map<String, List<Map<String, Map<String, Integer>>>> cars 
 = new HashMap<String, List<Map<String, Map<String, Integer>>>>();
```

​	**为了向后兼容性已经存在的原始类型**，因此编译器需要区分这些原始类型和泛型

```java
List<String> generics = new ArrayList<String>();
List<String> raws = new ArrayList();
```

---

## `JDK 7` - 钻石操作符

​	在Java 1.7中，钻石操作符引入了<button>类型推断</button>， 我们可以在赋值的时候，可以：《省略》指定参数化类型。

```java
List<String> cars = new ArrayList<>();
```

​	Java 1.7编译器的类型推断功能：**确定《最适合与调用匹配》的构造函数声明**

----

```java
abstract class Handler<T> {
    public T content;

    public Handler(T content){
        this.content = content;
    }

    abstract void handle();
}
```

```java
public static void main(String []args) {
        Handler<Integer> intHandler = new Handler<Integer>(1) {
            @Override
            public void handle() {
                System.out.println(content);
            }
        };
        // 1
        intHandler.handle();
        Handler<? extends Number> intHandler1 = new Handler<Number>(2) {
            @Override
            public void handle() {
                System.out.println(content);
            }
        };
    	// 2
        intHandler1.handle();
        Handler<?> handler = new Handler<Object>("test") {
            @Override
            public void handle() {
                System.out.println(content);
            }
        };
    	// test
        handler.handle();
    }
}
```

## `JDK 9` -改进钻石操作符

​	在 Java 9 中，我们可以在匿名类中<button>使用 <> 操作符</button>，如下所示：

```java
abstract class Handler<T> {
    public T content;

    public Handler(T content){
        this.content = content;
    }

    abstract void handle();
}
```

```java
public static void main(String[] args) {
    Handler<Integer> intHandler = new Handler<>(1) {
        @Override
        public void handle() {
            System.out.println(content);
        }
    };
    // 1
    intHandler.handle();
    
    Handler<? extends Number> intHandler1 = new Handler<>(2) {
        @Override
        public void handle() {
            System.out.println(content);
        }
    };
    // 2
    intHandler1.handle();
    
    Handler<?> handler = new Handler<>("test") {
        @Override
        public void handle() {
            System.out.println(content);
        }
    };
	// test
    handler.handle();
}
```



