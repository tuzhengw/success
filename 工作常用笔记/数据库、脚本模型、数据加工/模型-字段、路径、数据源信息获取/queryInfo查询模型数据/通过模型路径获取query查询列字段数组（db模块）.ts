

import dw from "svr-api/dw";
import db from 'svr-api/db';

/**
 * 通过模型路径获取query查询列字段数组
 
 * 注意：此方法，查询的模型必须包含：物理表表，视图不行
 
 * @param modelPath 模型路径
 * @return []，eg：[{ name: "id", exp: "model1.id"}]
 */
export function getTableFields(modelPath: string): QueryFieldInfo[] {
    if (!modelPath) {
        console.debug(`getTableFieldInfo()，获取模型【${modelPath}】物理表字段失败，原因：modelPath参数错误`);
        return [];
    }
    let modelProperties: DwTablePropertiesInfo = dw.getDwTable(modelPath).properties;
    let dataBase: string = modelProperties.datasource as string;
    if (dataBase == "defdw") {
        dataBase = getDefaultDbSourceName();
    }
    let tableName: string = modelProperties.dbTableName as string;
    let schema: string = modelProperties.dbSchema as string;
    let filedName: QueryFieldInfo[] = [];
    try {
        let ds: Datasource = db.getDataSource(dataBase);
        if (!schema) {
            schema = ds.getDefaultSchema();
        }
        print(`获取：${dataBase} 数据源下的Schema为：${schema}，表名为：${tableName}`);
        let dsMeta = ds.getTableMetaData(tableName, schema);
        let fields: TableFieldMetadata[] = dsMeta.getColumns();
        for (let i = 0; i < fields.length; i++) {
            filedName.push({
                name: fields[i].desc,
                exp: `model1.${fields[i].name}`
            });
        }
    } catch (e) {
        console.error(`通过模型路径【${modelPath}】获取物理表字段信息失败--error`);
        console.error(e.toString());
    }
    return filedName;
}


/**
 * 获取某模型表【查询的字段】信息
 * 注意：4.19 版本及以上不支持
 * 
 * @param modelPath 模型表路径
 * @param dbPhysicalFields 【记录】模型【物理字段名】数组，用于数组转换为JSON
 * @param dbPirmaryKey 【记录】表物理名主键
 * @param ignoredFields 忽略的【字段名】数组
 * @return 查询字段格式，eg：
 * [{ 
 *      name: "企业名称", 
 *      exp: "model1.ENTERPRISE_NAME" 
 * }]
 */
export function getTableFields(modelPath: string, dbPhysicalFields?: Array<string>, dbPirmaryKey?: Array<string>, ignoredFields?: Array<string>): QueryFieldInfo[] {
    if (!modelPath) {
        console.debug(`getTableFieldInfo()，获取模型【${modelPath}】物理表字段失败，原因：modelPath参数错误`);
        return [];
    }
    let modelProperties: DwTablePropertiesInfo = getDwTable(modelPath).properties;
    let primaryKeys = modelProperties.primaryKeys; // class java.util.ArrayList
    if (!primaryKeys) {
        print(`-- -【${modelPath}】该模型没有主键，无法推送`);
        return [];
    }
    let modelPrimary: string[] = [];
    for (let i = 0; i < primaryKeys.size(); i++) {
        modelPrimary.push(primaryKeys.get(i));
    }
    let dataBase: string = modelProperties.datasource as string;
    if (dataBase == "defdw") {
        dataBase = getDefaultDbSourceName();
    }
    let tableName: string = modelProperties.dbTableName as string;
    let schema: string = modelProperties.dbSchema as string;
    let queryFields: QueryFieldInfo[] = [];
    try {
        let ds: Datasource = db.getDataSource(dataBase);
        if (!schema) {
            schema = ds.getDefaultSchema();
        }
        print(`获取：${dataBase} 数据源下的Schema为：${schema}，表名为：${tableName}`);
        let dsMeta = ds.getTableMetaData(tableName, schema);
        let fields: TableFieldMetadata[] = dsMeta.getColumns();
        for (let i = 0; i < fields.length; i++) {
            let modelFields = fields[i];
            let dbfield: string = modelFields.name;
            if (modelFields.desc == '时间戳') {
                continue;
            }
            if (!!dbPirmaryKey && !!modelPrimary && modelPrimary.includes(modelFields.desc)) {
                dbPirmaryKey.push(dbfield);
            }
            if (!!dbPhysicalFields) {
                dbPhysicalFields.push(dbfield);
            }
            let fieldDescName: string = modelFields.desc;
            let queryField: QueryFieldInfo = {
                name: fieldDescName,
                exp: `model1.${dbfield}`
            }
            queryFields.push(queryField);
        }
    } catch (e) {
        print(`通过模型路径【${modelPath}】获取物理表字段信息失败--error`);
        print(e.toString());
    }
    print(queryFields);
    return queryFields;
}

/**
 * 获取当前项目设置的默认数据源名
 * @description defdw 为当前项目设置的默认数据源名
 */
function getDefaultDbSourceName(): string {
    print("getDefaultDbSourceName()：获取当前项目设置的默认数据源名");
    let settingPath: string = "/ZHJG/settings/settings.json";
    let projectSetting: string = metadata.getString(settingPath);
    let defalutDbName: string = JSON.parse(projectSetting).data.analysisDatasource;
    return defalutDbName;
}