
import JavaFile = java.io.File;
import Files_ = java.nio.file.Files;
import Paths_ = java.nio.file.Paths;

function main() {
    // let path = "/sysdata/app/zt.app/images/commons/logo.png";
    // let f = metadata.getFile(path).getContentAsInputStream();
    // print(f.asBytes());
    // f.close();
    // return;

    let filePath = `${sys.getClusterShareDir()}/file-storage/relativePath/`;
    let file = fs.getFile(filePath);
    print(file.listFiles())
    // print(file.delete());
    // print(fs.deleteFile(filePath))
    return;

    let shareDir: string = sys.getClusterShareDir();
    let attachParentDir: string = `${shareDir}/file-storage/relativePath`;
    let filePath: string = `${attachParentDir}/sdi_2.0_20221209180530.zip.meta`;
    let sendFile = new JavaFile(filePath);

    let absoluetePath: string = Paths_.get(sendFile.getAbsolutePath());
    let fileBytes: number[] = Files_.readAllBytes(absoluetePath); // 通过绝对路径获取文件字节流
    print(new JavaString(fileBytes));
}
