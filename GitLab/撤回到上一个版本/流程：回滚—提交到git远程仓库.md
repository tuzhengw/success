---
typora-root-url: images
---

# 回滚上一个版本

场景：需要将项目回滚到上一个提交的版本

# 1 回滚上一个提交版本

```cmd
# 查看更新日志，按“Q”退出
git log
```

《回到指定版本》

```cmd
# ---还原到上个版本，指定的commit id序号就可以
git reset --hard 版本号 
```

《本地回滚到上一个版本》在Git中,用HEAD表示当前版本

```
linux下是：git reset --hard HEAD^
windows下是：git reset --hard HEAD^
```

《案例》

​	找到需要回滚的commit，输入git reset --hard {`commitId`}，将<button>本地文件回滚</button>：

```
$ git reset --hard  d580ea7dab097d8ea6d658adbc7e9d57ef22669a
HEAD is now at d580ea7da add a constellation test case
```

​	此时本地文件已经回滚到刚刚commit `d580ea7dab097d8ea6d658adbc7e9d57ef22669a`之后的状态，但是服务器仍然没有改变，需要继续远程回滚：

​	<button>同步服务器</button>

```
$ git push -f
```

​	执行完后回滚成功

---

《注意》

```cmd
Administrator@AUTOBVT-RJ2L4SM MINGW64 /d/succezIDE/数据中台/SDI（本地：我） (master)
$ git push -f
```

会报如下错误：

```
Total 0 (delta 0), reused 0 (delta 0), pack-reused 0
remote: GitLab: You are not allowed to force push code to a protected branch on this project.
```

如果用的是`gitlab`版本库，这说明`gitlab`对仓库启用了保护，需要在仓库中设置一下：

![](../images/git保护.png)



# 2 更新远程仓库代码

## 2.1 本地和远程仓库版本不一致异常

> ​	出现这个错误的原因是git本地仓库的当前版本《低于》远程仓库的版本（在`github`上进行的修改没有同步到本地git仓库中）

```cmd
Administrator@AUTOBVT-RJ2L4SM MINGW64 /d/PHZIDE/SCDZPHZ (卫健委测试环境)
$ git push -u origin 卫健委测试环境

To gitlab.succez.com:tuzhengw_succez/SCDZPHZ.git
 ! [rejected]        卫健委测试环境 -> 卫健委测试环境 (non-fast-forward)
error: failed to push some refs to 'gitlab.succez.com:tuzhengw_succez/SCDZPHZ.git'
hint: Updates were rejected because the tip of your current branch is behind
hint: its remote counterpart. Integrate the remote changes (e.g.
hint: 'git pull ...') before pushing again.
hint: See the 'Note about fast-forwards' in 'git push --help' for details.

```

解决办法一：本地仓库<button>替换，且不保留</button>远程仓库代码

```cmd
在终端下输入：
git push -u origin [master][分支节点名] -f
```

​	执行的后果就是在远程仓库中进行的相关修改会被删除，使远程仓库回到你本地仓库未修改之前的那个版本

---

解决办法二：保留远程相关的修改

```
先在终端下输入：
git pull origin [master][分支节点名]
```

> 这句话是说将远程中进行的相关修改保存下来
>   但这时可能会报错：error: Your local changes to the following files would be overwritten by merge:
>   解决方案见另一篇博客：https://blog.csdn.net/IT_SoftEngineer/article/details/107133284
>   解决上述问题后就可保证远程仓库的更新会被同步到本地而本地仓库的修改也不会被覆盖
>   然后就是add，commit，push命令将本地的修改上传到远程仓库中

---

