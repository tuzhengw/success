import { Button, Combobox, Edit, Link, Checkbox } from "commons/basic";
import { Dialog } from "commons/dialog";
import { getCurrentUser, getMetaRepository, IMetaFileViewer, getFileImplClass, ResourceTreeDataProvider } from "metadata/metadata";
import { showResourceDialog } from "metadata/metamgr";
import { getScheduleMgr, ScheduleEditor, ScheduleLogInfo, ScheduleMgr, TaskLogInfo } from "schedule/schedulemgr";
import {
	assign, Component, ComponentArgs, ctx, ctxIf, getBaseName, IImportComponentInfo, showWaiting,
	showWarningMessage, showWaitingMessage, isEmpty, message, rc, showDialog, showErrorMessage, SZEvent, showSuccessMessage, MacScrollBar, LogItemInfo, ServiceTaskPromise, showProgressDialog
} from "sys/sys";
import { IFieldsDataProvider } from "commons/exp/expdialog";
import { IExpDataProvider, ExpVar, ExpCompiler, Token, ExpTokenIndex } from "commons/exp/expcompiler";
import { InterActionEvent, IVPage } from "metadata/metadata-script-api";
import { TableCellEditor } from "commons/table";
import { getDwTableDataManager } from "dw/dwapi";
import { ProgressLogs, ProgressPanel } from "commons/progress";


/**
 * 2022-11-18 tuzw
 * 日志工具类
 */
export class SysLogUtils {

	constructor() {
	}

	/**
	 * 展示指定计划执行日志信息
	 * @param scheduleId 计划任务ID
	 * @return 
	 */
	public showExecuteScheduleLog(scheduleId: string): Promise<any> | void {
		if (isEmpty(scheduleId)) {
			return;
		}
		let sheduleMgr: ScheduleMgr = getScheduleMgr();
		let scheduleLogs: Promise<ScheduleLogInfo[]> = sheduleMgr.queryScheduleLogs("sys", scheduleId);
		return scheduleLogs.then((logResults: ScheduleLogInfo[]) => {
			let scheduleLogInfo = this.getLastExecuteScheduleLogInfo(logResults) as ScheduleLogInfo;
			if (isEmpty(scheduleLogInfo)) {
				return;
			}
			let logs: string = scheduleLogInfo.logs;
			let logInfos = !!logs ? JSON.parse(logs).logs : "计划已经执行完成";
			return this.showLogsDialog(logInfos);
		});
	}

	/**
	 * 展示计划任务中指定任务的日志信息
	 * @param scheduleId 计划任务ID
	 * @param taskId 任务ID
	 * @return 
	 */
	public showLastTaskExecuteLogInfos(scheduleId: string, taskId: string): Promise<any> | void {
		if (isEmpty(scheduleId) || isEmpty(taskId)) {
			return;
		}
		let sheduleMgr: ScheduleMgr = getScheduleMgr();
		let scheduleLogs: Promise<ScheduleLogInfo[]> = sheduleMgr.queryScheduleLogs("sys", scheduleId);
		return scheduleLogs.then((logResults: ScheduleLogInfo[]) => {
			let scheduleLogInfo = this.getLastExecuteScheduleLogInfo(logResults) as ScheduleLogInfo;
			if (isEmpty(scheduleLogInfo)) {
				return;
			}
			let logId: string = scheduleLogInfo.scheduleLogId;
			return sheduleMgr.queryScheduleTaskLogs(scheduleId, logId).then((logs: TaskLogInfo[]) => {
				if (logs.length == 0) {
					showWarningMessage(`当前计划没有日志信息`);
					return;
				}
				let showLog: TaskLogInfo = logs[0];
				for (let i = 0; i < logs.length; i++) {
					let log: TaskLogInfo = logs[i];
					if (log.taskid == taskId) { // 根据任务ID找到需要展示的任务日志信息
						showLog = log;
						break;
					}
				}
				let state = showLog.state;
				if (state == 9) { // 若正在运行，则【轮询】获取当前最新的日志信息
					/** 任务运行的集群节点信息 */
					let lastClusterNode = showLog.lastClusterNode;
					/**	任务运行的长任务id */
					let lastRuntimeId = showLog.lastRuntimeId;
					return this.showLogsDialog([], state, lastRuntimeId, lastClusterNode);
				} else { // 若已经执行完成，则直接获取logs信息
					let logInfos = !!showLog.logs ? JSON.parse(showLog.logs).logs : "任务已经执行完成";
					return this.showLogsDialog(logInfos);
				}
			});
		});
	}

	/**
	 * 20220705 tuzw
	 * 获取最新执行的计划日志信息
	 * 
	 * 问题：由于产品对计划相关api进行了升级，queryScheduleLogs返回的顺序不再是排序的结果，此处改为自行获取最后一次执行的日志信息
	 * 帖子：https://jira.succez.com/browse/CSTM-19475
	 * 
	 * @param logResults 当前计划所有的日志信息
	 * @return 
	 */
	public getLastExecuteScheduleLogInfo(logResults: ScheduleLogInfo[]): ScheduleLogInfo | void {
		if (!logResults || logResults.length == 0) {
			return;
		}
		let maxScheduleLog: ScheduleLogInfo = logResults[0];
		for (let i = 1; i < logResults.length; i++) {
			if (maxScheduleLog.startTime < logResults[i].startTime) { // 结束时间必须得等待计划执行结束后才存在，这里使用开始时间比较
				maxScheduleLog = logResults[i];
			}
		}
		return maxScheduleLog;
	}

	/**
	 * 显示日志对话框
	 * @param logs 日志信息
	 * @param state? 任务状态
	 * @param lastRuntimeId? 任务运行的长任务id
	 * @param lastClusterNode? 任务运行的集群节点信息
	 * 
	 * @description 若展示任务日志, 支持轮询请求日志信息
	 */
	public showLogsDialog(logs: (string | LogItemInfo)[], state?: DW_RESOURCE_STATE, lastRuntimeId?: string, lastClusterNode?: string) {
		if (state === DW_RESOURCE_STATE.EXECUTING && !isEmpty(lastRuntimeId)) { // 任务正在执行，则根据长任务id向服务器请求日志
			let taskPromise = new ServiceTaskPromise({
				uuid: lastRuntimeId,
				clusterNode: lastClusterNode,
				startPoll: false // 创建时会自动启动轮询，这里不启用，而是等进度对话框显示后再轮询，否则可能导致进度对话框中的日志丢失前面一部分
			});
			return new Promise((resolve, reject) => {
				let progressDialogParams = {
					caption: message("schedulemgr.tasklog.dilaog.viewlog"),
					uuid: lastRuntimeId,
					clusterNode: lastClusterNode,
					promise: taskPromise,
					buttons: ["close"],
					onclose: (e, dialog: Dialog) => {
						taskPromise.stopPoll();
						dialog.close();
						resolve(AnaActionTriggerResultType.Canceled);
					}
				};
				/**
				 * 2022-11-18 tuzw
				 * 计划若处于执行中，查看日志信息重复展示
				 * 地址：https://jira.succez.com/browse/BI-46875
				 * 原因：showProgressDialog本身控件中会自己调用startPoll的, 不需要再次额外调用
				 */
				return showProgressDialog(progressDialogParams).then((dialog: Dialog) => {
					// taskPromise.startPoll();
				});
			});
		}
		return new Promise((resolve, reject) => {
			return showDialog({
				id: "schedulemgr-dialog-progresslogs",
				className: "schedulemgr-dialog-progresslogs",
				caption: message("schedulemgr.tasklog.dilaog.viewlog"),
				maxHeight: 810,
				maxWidth: 1440,
				height: 600,
				width: 1080,
				minWidth: 600,
				minHeight: 340,
				maxIconVisible: true,
				content: {
					implClass: ProgressLogs,
					className: "dialog-viewprogresslogs",
					autoWrap: true,
					format: true,
				},
				onclose: (e, dialog) => {
					dialog.close();
					resolve(AnaActionTriggerResultType.Canceled);
				},
				onshow: (e: SZEvent, dialog: Dialog) => {
					let content = <ProgressLogs>dialog.content;
					content.clear();
					content.addLogs(logs);
				}
			})
		});
	}
}