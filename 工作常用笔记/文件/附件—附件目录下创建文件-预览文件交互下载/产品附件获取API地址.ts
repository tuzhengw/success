//下载引入的文档
button_downloadImport: (event: InterActionEvent) => {
	let residOrPath = event.params.residOrPath;
	let attachment = event.params.attachment;
	let fileName = event.params.fileName;
	showConfirmDialog({
		caption: '下载确认',
		message: '点击"确定"按钮后将开始下载"' + fileName + '"',
		onok: () => {
			downloadFile(`/api/fapp/services/downLoadAttachment?resId=${residOrPath}&fileId=${attachment}`);
		}
	})
}