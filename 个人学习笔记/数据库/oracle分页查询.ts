SELECT SMS_ID,SMS_CONTENT,phones FROM (
    select 
        SMS_ID,SMS_CONTENT,phones,ROWNUM RN
    from 
        MESSAGES_SMS_TABLE_NEW 
    where 
        CREATE_TIME > trunc(sysdate-1) and SMS_ID not in (select SMS_ID from MESSAGES_SMS_LOG where zt='true') and SJLY='1'
    order by CREATE_TIME ASC
) mes WHERE RN > 1000 AND RN <= 2000