```
# **GET_NUMBERS_DATA_DURATION(numbers: number)**

解析某数值，返回时间段信息。

**类型必须是number，且不小于0!**

## 参数
***numbers***：需要对其转换的数值（毫秒数）。

## 示例
1. `GET_NUMBERS_DATA_DURATION(59 * 1000)` 59秒
2. `GET_NUMBERS_DATA_DURATION((44 * 60 + 2 * 60) * 1000)` 44分2秒
3. `GET_NUMBERS_DATA_DURATION(13475527)` 3时44分35秒
```

