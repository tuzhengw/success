package com.hayasi

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import kong.unirest.Unirest
import org.jsoup.Jsoup
import java.io.File
import java.nio.file.Files
import kotlin.io.path.Path


private fun main1() {
    val mapper = jacksonObjectMapper()
    var index = 5400
    while (true) { // 因为不知道要爬取多少页, 手动控制循环（debug）
        val title = writeFileByIndex(index, mapper)
        println("$index $title")
        index--;
    }
}

private fun writeFileByIndex(index: Int, mapper: ObjectMapper): String {
    val body = Unirest.get("http://baishazhou.21itcn.cn/Price/Detail/$index").asString().body
    val document = Jsoup.parse(body)
    val title = document.select("title").text()
    val table = document.select("table#market")
    val headMap = hashMapOf<Int, String>()
    for ((i, key) in table.select("thead th").withIndex()) {
        headMap[i] = key.text()
    }
    val rows = table.select("tbody tr")
    val valueMaps = arrayListOf<Map<String, String>>()
    for (row in rows) {
        val cells = row.select("td")
        val map = hashMapOf<String, String>()
        for ((i, key) in headMap) {
            val value = cells[i].text().trim()
            map[key] = value
        }
        valueMaps.add(map)
    }
    mapper.writeValue(File("D:\\images", "$title.json"), valueMaps)
    return title
}


private fun main2() {
    val files = File("D:\\images").listFiles()!!
    val mapper = jacksonObjectMapper()
    val list = ArrayList<Map<String, String>>()
    val set = HashSet<String>()
    for (file in files) {
        val name = file.name
        val type = name.substring(11, 13)
        val date = name.substring(0, 11)
        val maps = mapper.readValue<List<Map<String, String>>>(file)
        list.addAll(maps.map {
            val map = HashMap(it)
            map["date"] = date
            map["type"] = type
            set.addAll(map.keys)
            map
        })
    }
    println(mapper.writeValueAsString(set))
    mapper.writeValue(File("D:\\CLionProjects\\综合数据.json"), list)
}


fun main(){
    val mapper = jacksonObjectMapper()
    val list = mapper.readValue<List<Map<String, String>>>(File("D:\\CLionProjectsCLionProjects\\综合数据.json"))
    val typeData = list.groupBy { it["type"] }
    for ((type, data) in typeData) {
        val fields =
            data.fold(HashSet()) { acc: HashSet<String>, map: Map<String, String> -> acc.addAll(map.keys);return@fold acc }
                .toList()
        val writer = Files.newBufferedWriter(Path("$type.txt"))
        writer.use {
            it.appendLine(fields.joinToString("|"))
            for (map in data) {
                it.appendLine(fields.map { f -> map[f] }.joinToString("|"))
            }
        }
    }
}