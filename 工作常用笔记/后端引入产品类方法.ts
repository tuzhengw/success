
import bean from "svr-api/bean";

/**
 * getBean获取
 */
function methodOne() {
    let dwServiceMeta = bean.getBean("com.succez.dw.services.impl.DwServiceMeta");
}

/**
 * 直接导入
 */
import MetaFileContentImpl = com.succez.metadata.service.impl.entity.MetaFileContentImpl;
function test(): void {
    let newContent = new MetaFileContentImpl([], false);
}