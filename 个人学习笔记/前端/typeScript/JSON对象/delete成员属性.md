# * 删除成员属性--delete

> ​	注意：一定要是JSON对象，不能是JSON数组

---

```ts
data: [{
    "HZDJH": "2018",
    "HZBQ": "眼科",
    "CREATE_TIME": "2021-09-23 11:11:11"
}]
```

```TS
// 错误删除
delete data.CREATE_TIME;

// 正确删除
delete data[0].CREATE_TIME;
```

