/**
 * 字段别名过长导致标识符无效
 * 因为Oracle中表名，列名，标识列字符不能超过30个字符
 * 这里校验字段，并返回合法的标识符
 * @param colums 需要校验的列名
 * @return 大写的合法字段名
 */
public getLegalColumnDesc(colums: string): string {
	if (colums == null) {
		return "";
	}
	if (colums.length <= 30) {
		return colums.toUpperCase();
	}
	return colums.substring(0, 30).toUpperCase();
}