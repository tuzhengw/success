


import Sm2Util = com.ztf.sdk.security.crypto.Sm2Util;  // 第三方工具类
/**
 * 用于使用 dwapi 向数据表中更新或者插入数据的时候加密数据
 * 
 * 使用场景：
 * 数据加工-对某列-加密-加密方式（DES）
 * 
 * 注意：
 * 1. 脚本中可以实现多个加密算法，请通过后缀名的方式进行区分，比如
 * 1) encryptAlgorithm_DES 代表使用 DES加密算法实现
 * 2) encryptAlgorithm_3DES 代表使用 3DES加密算法实现
 *
 * @param plainText 待加密的字符串
 * @return 加密后的字符串
 */
function encryptAlgorithm_SM2(plainText: string): string {
    let publicKey: string = `MFkwEwYHKoZIzj0CAQYIKoEcz1UBgi0DQgAEww80Hcny0snHhlyK/PGu5lfESDR9fHEg4J1aXzRXXmboLhtbPRcC2t1gMMfQM71C2O2FS+k7dcFq9ve0WKlLFA==`;
    return Sm2Util.encryptBcd(plainText, publicKey); // 加密密文
}

/**
 * 用于使用dwapi查询数据的时候，解密根据{@link encryptAlgorithm_xxx(string)}函数加密数据
 * 
 * @param encryptedText 被加密的字符串
 * @return 解密后的字符串
 */
function decryptAlgorithm_SM2(encryptedText: string): string {
    let privateKey = `MIGTAgEAMBMGByqGSM49AgEGCCqBHM9VAYItBHkwdwIBAQQg1Jx
    I9SvSj+LMDjLGukZhWEUwNFSB4+lb/4X2dQpEioCgCgYIKoEcz1UBgi2hRANCAA
    TDDzQdyfLSyceGXIr88a7mV8RINH18cSDgnVpfNFdeZuguG1s9FwLa3WAwx9AzvULY7YVL6Tt1wWr297RYqUsU`;
    return Sm2Util.decryptFromBcd(encryptedText, privateKey); // 解密明文
}
