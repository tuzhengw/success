
import { ICustomJS, IMetaFileCustomJS, InterActionEvent, IVPage, IAppCustomJS, DatasetDataPackageRowInfo, IVComponent } from "metadata/metadata-script-api";

/** 
 * 记录-网络交易监管线索清单页面-监测线索信息（含店铺信息）模型 查询信息, eg:  {
        "resid": "/ZHJG/app/home.app/jgrw/WLJYJG/wljyjgxsqd.spg",
        "fields": [{
                "name": "CALCULATOR_1"
            }
        ],
        "sources": null,
        "select": true,
        "filter": [{
                "exp": "apply_filter(fieldsFilter1)",
                "source": "fieldsFilter1"
            }
        ],
        "params": [{
                "name": "param2",
                "value": "-9M_FS4S-eZ8HkfFLqFJ_g"
            }, {
                "name": "tabbar1",
                "value": "全部"
            },{
                "name": "param11",
                "value": "true"
            },{
                "name": "fieldsFilter1",
                "value": null
            }
        ],
        "sort": [],
        "options": {
            "needCodeDesc": true,
            "autoSort": true,
            "cache": true,
            "limit": 100,
            "offset": 0,
            "queryBlobFlag": true,
            "queryTotalRowCount": true,
            "needHighlightKeyword": true,
            "highlightPreTag": "<em class='sz-keyword'>",
            "highlightPostTag": "</em>"
        },
        "queryId": "model16",
        "resModifyTime": 1662364904723
    }
 */
let saveMonitorInfoModelQueryInfo: QueryInfo; // 只能前端查询, 后端无法获取SPG信息

export const CustomJS: { [type: string]: IMetaFileCustomJS } = {
	"wljyjgxsqd.spg": {
		CustomActions: {
			/**
			 * 20221027 tuzw
			 * @description 需求：网络交易检测批量导出网络交易监测违法信息登记表
			 * @description 地址：https://jira.succez.com/browse/CSTM-20971
			 */
			button_import_monitorInfoTableFiles: (event: InterActionEvent) => {
				if (isEmpty(saveMonitorInfoModelQueryInfo)) {
					showWarningMessage(`获取监测线索信息（含店铺信息）查询信息失败, 无法导出`);
					return;
				}
				/** 考虑到模型字段是dw模块查询, 根据查询字段数组, 获取线索编号字段下标位置 */
				let monitorIndex: number = -1;
				let queryFields = saveMonitorInfoModelQueryInfo.fields;
				for (let i = 0; i < queryFields.length; i++) {
					let name: string = queryFields[i].name;
					if (name == "XSBH") { // 找到[线索编号]字段下标位置
						monitorIndex = i;
						break;
					}
				}
				if (monitorIndex == -1) {
					showWarningMessage(`模型没有绑定线索编号字段, 无法生成导出文档`);
					return;
				}
				saveMonitorInfoModelQueryInfo.options.limit = null; // 获取全部数据, 去掉分页数量限制
				return getQueryManager().queryData(saveMonitorInfoModelQueryInfo, uuid()).then(result => {
					let datas: string[][] = result.data;
					/** 检测表对应的线索编号 */
					let monitorIndexIds: string[] = [];
					for (let i = 0; i < datas.length; i++) {
						let data = datas[i];
						let index: string = data[monitorIndex]; // 注意queryInfo是dw模块查询出来的, 查询字段位置固定, 线索编号查询位(0起始位): 5
						monitorIndexIds.push(index);
					}
					return;
				})
			},
			}
		},
		/**
		 * 页面所有模型查询条件都会走此处
		 */
		onQueryData: (page: IVPage, query: QueryInfo) => {
			if (query.queryId == 'model16') { // 记录页面[监测线索信息（含店铺信息）]模型 的查询queryInfo信息 
				saveMonitorInfoModelQueryInfo = query;
			}
			return;
		}
	}
}