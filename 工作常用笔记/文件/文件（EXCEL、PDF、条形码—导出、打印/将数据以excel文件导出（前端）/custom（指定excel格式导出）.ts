import { ctx, SZEvent, Component, throwInfo, throwError, showConfirmDialog, showMessage, showFormDialog, showErrorMessage, showMenu, isEmpty, deepAssign, uuid, message, quoteExpField, showDialog, showSuccessMessage, showProgressDialog, rc, downloadFile, waitRender, showWaiting, assign, IDataProvider, formatDateFriendly, parseDate, formatDate, tryWait, rc_get, showWarningMessage, timerv, showWaitingMessage, hideMessage, DataChangeEvent, EventListenerManager, GroupInfo, IFindArgs, ServiceTaskPromise, ServiceTaskInfo, ServiceTaskState, rc_task } from 'sys/sys';
import { IMetaFileCustomJS, InterActionEvent, IAppCustomJS, IDwCustomJS, DatasetDataPackageRowInfo, IDataset, IVComponent } from "metadata/metadata-script-api";
import { showDrillPage, getMetaRepository, MetaFileViewer, getCurrentUser, getSysSettings } from 'metadata/metadata';
import { getDwTableDataManager, labelData, refreshModelState, exportQuery, importData, ImportDataResult, getQueryManager, pushData, importDbTables, DwDataChangeEvent } from 'dw/dwapi';
import { DwPseudoDimName, DwTableModelFieldProvider } from 'dw/dwdps';
import { Combobox, Button, showUploadDialog, Toolbar, ToolbarItemAlign, ComboboxArgs, DataProvider, ButtonArgs, TXT_FILETYPE, IMAGE_FILETYPE, showMessageBox, MessageBoxArgs, MessageContentInfo, MessageBoxType } from 'commons/basic';
import { Tree } from 'commons/tree';
import { Menu, MenuItem } from "commons/menu";
import { SuperPageBuilder } from "app/superpage/superpagebuilder";
import { SuperPage } from 'app/superpage/superpage';
import { AnaModelBuilder } from 'ana/builder';
import { IDwTableEditorPanel, DwTableEditor } from 'dw/dwtable';
import { showLabelMgrDialog } from 'dw/dwdialog';
import { Form } from "commons/form";
import { ExDTable } from "commons/dtable";
import { Dialog } from 'commons/dialog';
import { PortalModulePageArgs, PortalHeadTitle, PortalHeadToolbar } from 'app/templatepage';
import { html2pdf, ExportReusltType } from 'commons/export';
import { modifyClass, getAppPath, showScheduleDialog } from './commons/commons.js';
import { SuperPageData } from 'app/superpage/superpagedatamgr';
import { EasyFilter } from 'commons/extra.js';
import { ExpCompiler, ExpVar, Token, ExpTokenIndex, IExpDataProvider } from 'commons/exp/expcompiler';
import { SpgEmbedSuperPage } from 'app/superpage/components/embed';
import { MetaFileRevisionView, showResourceDialog } from 'metadata/metamgr';
import { MetaFileViewer_miniapp } from 'app/miniapp';
import { ScheduleEditor, getScheduleMgr, TaskLogInfo } from "schedule/schedulemgr";
import { showDbConnectionDialog, DbConnectionPanel } from 'datasources/datasourcesmgr';
import { ProgressLogs, ProgressPanel } from 'commons/progress';
import { ExpDialog, ExpDialogValueInfo, IFieldsDataProvider, showExpDialog } from 'commons/exp/expdialog';
import { getCamelChars } from 'commons/pinyin';
import { ExpContentType } from 'commons/exp/exped';



export const CustomJS: { [type: string]: IMetaFileCustomJS } = {
	spg: {
		CustomActions: {
			/**
			 * 20211129 tuzw
			 * 数据中台的自定义规则设置界面支持导入导出
			 * 帖子地址：https://jira.succez.com/browse/CSTM-16899
			 * SPG地址：http://192.168.10.184:10480/sdi/sysdata/app/zt.app?:edit=true&:file=check_rules.spg
			 */
			button_checkRule_import: (event: InterActionEvent) => {
				let params = event.params;
				let importType: string = params.importType;
				/** 导出过滤条件，默认：导出所有数据 */
				let filter: Array<FilterInfo> = [];

				/** 勾选（单页）数据行的主键集，eg：['F_JJHK_CON_1-liuyongz-1101-3', 'EDU_F_XSSJ_1-wangyg-1125-1'] */
				let importChoseDataKeys: Array<string> = [];
				// 若导出方式为：导出当前查询的所有数据
				if (importType == 'importPageDisplayData') {
					/*
						获取页面的过滤条件
						方式1：采用拼接（前提：必须明确指定页面的过滤字段）
							pageFilterConditionExp = getImportRuleDataFilterCondition(params);
						方式2：
							获取【过滤字段】的exp值，再：exp：
					*/
					
					// 获取当前【过滤字段组件】的过滤条件
					let fieldsFilter: IVComponent = event.page.getComponent("fieldsFilter1");
					/** 页面过滤条件表达式 */
					let filterExp = fieldsFilter.values.exp;
					if (filterExp != null) {
						filter.push({
							// fieldsFilter.values.exp： "RULE_DEFINE_TYPE in ('SQL')"
							exp: `${filterExp}`
						});
					}
				} else {
					importChoseDataKeys = params.importChoseDataKeys;
					if (!!importChoseDataKeys) {
						filter.push({
							exp: `model1.RULE_ID in ('${importChoseDataKeys.join("','")}')`
						});
					}
				}
				let importModelPath: string = "/sysdata/data/tables/dg/DG_RULES.tbl";
				let fieldQueryInfo: QueryInfo = {
					fields: [
						{ name: "规则编码", exp: "model1.RULE_ID" }, { name: "规则说明", exp: "model1.RULE_NAME" }, { name: "规则定义方式", exp: "model1.RULE_DEFINE_TYPE" },
						// { name: "规则表达式", exp: "model1.RULE_EXP" }, { name: "数据加工", exp: "model1.RULE_DATA_FLOW" }, { name: "简单查询", exp: "model1.RULE_QUERY" },
						{ name: "规则表达式 数据加工 简单查询", exp: "model1.RULE_EXP + ' , ' + model1.RULE_DATA_FLOW + ' , ' + model1.RULE_QUERY" },
						{ name: "规则表达式描述", exp: "model1.RULE_EXP_DESC" }, { name: "检测资源", exp: "model1.RULES_LIB_ID" }, { name: "检测字段", exp: "model1.JCZD" },
						{ name: "错误数据显示表达式", exp: "model1.ERROR_DATA_DISPLAY_EXP" }, { name: "分值", exp: "model1.SCORE" }
					],
					sources: [{
						id: "model1",
						path: importModelPath
					}],
					filter: filter
				}

				let cellsBordersInfo: CellsBordersInfo = { // 单元格边框信息
					all: {
						color: "#000000",
						width: 1
					}
				}
				
				let exportColumnStyles: ExportColumnStyle[] = [ // 导出excel表格的列样式
					// 单位：磅，1英寸 = 72磅 = 2.54厘米
					{ width: 72 * 3 }, { width: 72 * 3 }, { width: 110 }, { width: 72 * 7 }, { width: 72 * 6 }, { width: 72 * 4 }, { width: 110 }, { width: 72 * 6 }, { width: 72 }
				];
				let exportTableStyle: ExportTableStyle = { // 设置导出Excel格式
					showGridLine: false,
					bordersInfo: cellsBordersInfo, // 填充区域边框：全部
					columns: exportColumnStyles,
					header: { // 列标题样式
						backgroundColor: "#BBFFFF", // 颜色对应表：http://www.mgzxzs.com/sytool/se.htm
						rowHeight: 45,
						font: {
							family: "宋体",
							bold: true,
							size: 18
						}
					}
				}

				return getMetaRepository().getFile(importModelPath, false, false, false).then(file => {
					let importModelResid: string = file.id;
					return import("dw/dwapi").then((m) => {
						m.exportQuery({
							resid: importModelResid, // spgID
							query: fieldQueryInfo,
							fileFormat: DataFormat.xlsx,
							fileName: "自定义规则列表",
							exportTableStyle: exportTableStyle
						} as ExportDataParams);
					});
				})
				
			}
		}
}



/**
 * 根据页面设置的选择的过滤条件，生成过滤表达式
 * @params params {
 * 	createTime?：string,
 *  createPeople?：string,
 *  errorLevel?：string,
 *  score?：string,
 *  rule_define_type?：string
 *  rule_name?: string,
 * }
 * @return 字符串形式的过滤条件表达式，eg：
 * create_time="xxx" and craetor="xx" ...
 */
function getImportRuleDataFilterCondition(params): string {
	let filterCondition: Array<string> = [];
	/** 创建时间 */
	let createTime: string = params.createTime;
	/** 创建人 */
	let createPeople: string = params.createPeople;
	/** 错误级别 */
	// let errorLevel: string = params.errorLevel;
	/** 分数 */
	let score: string = params.score;
	/** 规则定义类型 */
	let rule_define_type: Array<string> = params.rule_define_type;
	/** 规则说明 */
	let rule_name: string = params.rule_name;

	if (!!createTime) {
		filterCondition.push(`model1.CREATE_TIME='${createTime}'`);
	}
	if (!!createPeople) {
		filterCondition.push(`model1.CREATOR='${createPeople}'`);
	}
	if (!!score) {
		filterCondition.push(`model1.SCORE='${score}'`);
	}
	if (!!rule_define_type) {
		// model1.RULE_DEFINE_TYPE in ('ETL','EXP','QUERY','SQL')
		filterCondition.push(`model1.RULE_DEFINE_TYPE in ('${rule_define_type.join("','")}')`);
	}
	if (!!rule_name) {
		filterCondition.push(`model1.RULE_NAME='${rule_name}'`);
	}
	return filterCondition.join(" and ");
}