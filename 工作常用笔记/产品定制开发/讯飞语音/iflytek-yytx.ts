/**
 * ===================================================
 * 修改人：luofuw
 * 审核员：liujingj
 * 修改日期：2022-10-28
 * 脚本用途: 客户端调用科大讯飞语音听写接口
 * 帖子地址：https://jira.succez.com/browse/CSTM-20825
 * ===================================================
 */

import CryptoJS from './crypto-js.min.js';
const IP_HOST = location.host;
const PROTOCOL = location.protocol;
const transWorker = new Worker(`${PROTOCOL}//${IP_HOST}/xyfl/hbzfba/app/协同执法办案移动端NEW.app/scripts/yysb/transcode.worker.js`);
const APPID = '81c15974';
const API_SECRET = 'YjAxYWM3YjU1N2YxNDY1NjBlOWRjYjBj';
const API_KEY = '9f938eda71a462c995d82a94a2f12df4';
// 每一帧音频大小的整数倍
const FRAME_SIZE = 1280;

// 科大讯飞语音听写接口调用
export class Yytx {
    [x: string]: any;
    public CustomActions: {};

    constructor({ appId, vad, language, accent } = {}) {
        let self = this;
        this.status = 'null';
        this.language = language || 'zh_cn';
        this.accent = accent || 'mandarin';
        this.appId = appId || APPID;
        if (vad) {
            this.vad = vad < 10000 ? vad : 3000;
        } else {
            this.vad = 3000;
        }
        // 记录音频数据
        this.audioData = [];
        // 记录听写结果
        this.resultText = '';
        // wpgs下的听写结果需要中间状态辅助记录
        this.resultTextTemp = '';
        transWorker.onmessage = function (event) {
            self.audioData.push(...event.data);
        }
        this.CustomActions = {
            recorderInit: this.recorderInit.bind(this),
        }
    }

    /**
     * 修改录音听写状态
     * 
     * @params status 录音听写状态
     */
    setStatus(status) {
        this.onWillStatusChange && this.status !== status && this.onWillStatusChange(this.status, status);
        this.status = status;
    }

    /**
     * 获取当前录音听写状态
     * 
     * @params status 录音听写状态
     */
    getStatus() {
        return this.status;
    }

    /**
     * 设置识别结果内容
     * 
     * @params resultText 记录听写结果
     * @params resultTextTemp wpgs下的听写结果需要中间状态辅助记录
     */
    setResultText(resultText?, resultTextTemp?) {
        this.onTextChange && this.onTextChange(resultTextTemp || resultText || '');
        resultText !== undefined && (this.resultText = resultText);
        resultTextTemp !== undefined && (this.resultTextTemp = resultTextTemp);
    }

    /**
     * 修改听写参数
     * 
     * @params vad 静音等待时间
     * @params language 语言
     * @params accent 方言，当前仅在language为中文时，支持方言选择。
     */
    setParams(vad, language, accent) {
        vad && (this.vad = vad);
        language && (this.language = language);
        accent && (this.accent = accent);
    }

    /**
     * 对处理后的音频数据进行base64编码
     * 
     * @params buffer 数据缓冲流
     */
    toBase64(buffer) {
        let binary = '';
        let bytes = new Uint8Array(buffer);
        for (let i = 0; i < bytes.byteLength; i++) {
            binary += String.fromCharCode(bytes[i]);
        }
        return window.btoa(binary);
    }

    /**
     * 连接WebSocket
     */
    connectWebSocket() {
        return this.getWebSocketUrl().then(url => {
            let iatWS;
            if ('WebSocket' in window) {
                iatWS = new WebSocket(<any>url);
            } else if ('MozWebSocket' in window) {
                iatWS = new MozWebSocket(url);
            } else {
                alert('浏览器不支持WebSocket!');
                return false;
            }
            this.webSocket = iatWS;
            this.setStatus('init');
            iatWS.onopen = (e) => {
                this.setStatus('ing');
                // 重新开始录音
                setTimeout(() => {
                    this.webSocketSend();
                }, 500)
                console.log('打开连接');
            }
            iatWS.onmessage = (e) => {
                this.result(e.data);
            }
            iatWS.onerror = (e) => {
                this.recorderStop();
            }
            iatWS.onclose = (e) => {
                this.recorderStop();
                console.log('关闭连接');
            }
        })
    }

    /**
     * 初始化浏览器录音
     */
    recorderInit() {
        navigator.getUserMedia =
            navigator.getUserMedia ||
            navigator.webkitGetUserMedia ||
            navigator.mozGetUserMedia ||
            navigator.msGetUserMedia;

        // 创建音频环境
        try {
            this.audioContext = new (window.AudioContext || window.webkitAudioContext)();
            this.audioContext.resume();
            if (!this.audioContext) {
                alert('浏览器不支持webAudioApi相关接口');
                return false;
            }
        } catch (e) {
            if (!this.audioContext) {
                alert('浏览器不支持webAudioApi相关接口');
                return false;
            }
        }
        // 获取浏览器录音权限
        if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
            navigator.mediaDevices.getUserMedia({
                audio: true,
                video: false
            }).then(stream => {
                getMediaSuccess(stream);
            }).catch((e) => {
                getMediaFail(e);
            })
        } else if (navigator.getUserMedia) {
            navigator.getUserMedia({
                audio: true,
                video: false
            }, (stream) => {
                getMediaSuccess(stream);
            }, function (e) {
                getMediaFail(e);
            })
        } else {
            if (navigator.userAgent.toLowerCase().match(/chrome/) && location.origin.indexOf('https://') < 0) {
                console.error('获取浏览器录音功能，因安全性问题，需要在localhost 或 127.0.0.1 或 https 下才能获取权限！');
            } else {
                alert('对不起：未识别到录音设备!');
            }
            this.audioContext && this.audioContext.close();
            return false;
        }
        // 获取浏览器录音权限成功时回调
        let getMediaSuccess = (stream) => {
            // 创建一个用于通过JavaScript直接处理音频
            this.scriptProcessor = this.audioContext.createScriptProcessor(0, 1, 1);
            this.scriptProcessor.onaudioprocess = (e) => {
                if (this.status === 'ing') {
                    transWorker.postMessage(e.inputBuffer.getChannelData(0));
                }
            }
            // 创建一个新的MediaStreamAudioSourceNode 对象，使来自MediaStream的音频可以被播放和操作
            this.mediaSource = this.audioContext.createMediaStreamSource(stream);
            // 连接
            this.mediaSource.connect(this.scriptProcessor);
            this.scriptProcessor.connect(this.audioContext.destination);
            this.connectWebSocket();
        }
        // 获取浏览器录音权限失败时回调
        let getMediaFail = (e) => {
            // alert('获取MediaStream对象失败');
            alert(e);
            console.log(e);
            this.audioContext && this.audioContext.close();
            this.audioContext = undefined;
            // 关闭websocket
            if (this.webSocket && this.webSocket.readyState === 1) {
                this.webSocket.close();
            }
        }
    }

    /**
     * 启动录音
     */
    recorderStart() {
        if (!this.audioContext) {
            this.recorderInit();
        } else {
            this.audioContext.resume();
            this.connectWebSocket();
        }
    }

    /**
     * 停止录音
     */
    recorderStop() {
        if (!(/Safari/.test(navigator.userAgent) && !/Chrome/.test(navigator.userAgen))) {
            // safari下suspend后再次resume录音内容将是空白，设置safari下不做suspend
            this.audioContext && this.audioContext.suspend();
        }
        this.setStatus('end');
    }

    /**
     * 向webSocket发送数据
     */
    webSocketSend() {
        if (this.webSocket.readyState !== 1) {
            return false;
        }
        let audioData = this.audioData.splice(0, FRAME_SIZE);
        let params = {
            common: {
                app_id: this.appId,
            },
            business: {
                language: this.language, //小语种可在控制台--语音听写（流式）--方言/语种处添加试用
                domain: 'iat',
                accent: this.accent, //中文方言可在控制台--语音听写（流式）--方言/语种处添加试用
                vad_eos: this.vad, // 开始录入音频后，音频后面部分最长静音时长取值范围[0,10000]
                // dwa: 'wpgs', //为使该功能生效，需到控制台开通动态修正功能（该功能免费）
            },
            data: {
                status: 0,
                format: 'audio/L16;rate=16000',
                encoding: 'raw',
                audio: this.toBase64(audioData),
            },
        }
        this.webSocket.send(JSON.stringify(params));
        this.handlerInterval = setInterval(() => {
            // websocket未连接
            if (this.webSocket.readyState !== 1) {
                this.audioData = [];
                clearInterval(this.handlerInterval);
                return false;
            }
            if (this.status === 'end') {
                this.webSocket.send(
                    JSON.stringify({
                        data: {
                            status: 2,
                            format: 'audio/L16;rate=16000',
                            encoding: 'raw',
                            audio: '',
                        },
                    })
                );
                this.audioData = [];
                clearInterval(this.handlerInterval);
                return false;
            }
            audioData = this.audioData.splice(0, FRAME_SIZE);
            // 中间帧
            this.webSocket.send(
                JSON.stringify({
                    data: {
                        status: 1,
                        format: 'audio/L16;rate=16000',
                        encoding: 'raw',
                        audio: this.toBase64(audioData),
                    },
                })
            );
        }, 40)
    }

    /**
     * 识别结果 
     * 
     * @params resultData webSocket返回数据
     */
    result(resultData) {
        // 识别结束
        let jsonData = JSON.parse(resultData);
        if (jsonData.code !== 0) {
            this.webSocket.close();
            console.log(`${jsonData.code}:${jsonData.message}`);
        }
        if (jsonData.code === 0 && jsonData.data.status === 2) {
            this.setStatus('end');
            this.webSocket.close();
        }
        if (jsonData.data && jsonData.data.result) {
            let data = jsonData.data.result;
            let str = '';
            let ws = data.ws;
            for (let i = 0; i < ws.length; i++) {
                str = str + ws[i].cw[0].w;
            }
            // 开启wpgs会有此字段(前提：在控制台开通动态修正功能)
            // 取值为 "apd"时表示该片结果是追加到前面的最终结果；取值为"rpl" 时表示替换前面的部分结果，替换范围为rg字段
            if (data.pgs) {
                // 动态修正功能暂未实现 TODO
            } else {
                this.setResultText(this.resultText + str);
            }
        }
    }

    /**
     * 开始调用语音识别接口
     */
    start() {
        this.recorderStart();
        this.setResultText('', '');
    };

    /**
     * 停止调用语音识别接口
     */
    stop() {
        this.recorderStop();
    };

    // 获取请求websocket的url
    getWebSocketUrl() {
        return new Promise((resolve, reject) => {
            // 请求地址根据语种不同变化
            let url = 'wss://iat-api.xfyun.cn/v2/iat'
            let host = 'iat-api.xfyun.cn'
            let apiKey = API_KEY
            let apiSecret = API_SECRET
            let date = new Date().toUTCString();
            let algorithm = 'hmac-sha256'
            let headers = 'host date request-line'
            let signatureOrigin = `host: ${host}\ndate: ${date}\nGET /v2/iat HTTP/1.1`
            let signatureSha = CryptoJS.HmacSHA256(signatureOrigin, apiSecret)
            let signature = CryptoJS.enc.Base64.stringify(signatureSha)
            let authorizationOrigin = `api_key="${apiKey}", algorithm="${algorithm}", headers="${headers}", signature="${signature}"`
            let authorization = btoa(authorizationOrigin)
            url = `${url}?authorization=${authorization}&date=${date}&host=${host}`
            resolve(url)
        })
    }
};