

import db from "svr-api/db";
import dw from "svr-api/dw";
import http from "svr-api/http";
import metadata from "svr-api/metadata";
import utils from "svr-api/utils";
import sys from "svr-api/sys";

import Files_ = java.nio.file.Files;
import Paths_ = java.nio.file.Paths;
import JavaFile = java.io.File;
import ZipOutputStream = java.util.zip.ZipOutputStream;
import ZipEntry = java.util.zip.ZipEntry;
import FileOutputStream = java.io.FileOutputStream;
import FileInputStream = java.io.FileInputStream;
import IOUtils = org.apache.commons.io.IOUtils;
import JavaString = java.lang.String;
import BufferedWriter = java.io.BufferedWriter;
import FileWriter = java.io.FileWriter;


import HttpClients = org.apache.http.impl.client.HttpClients;
import EntityUtils = org.apache.http.util.EntityUtils;
import HttpGet = org.apache.http.client.methods.HttpGet;
import File_ = java.io.File; // 工具类不需要对象，直接import导入

import ZipOutputStream = java.util.zip.ZipOutputStream;
import ZipEntry = java.util.zip.ZipEntry;
import ArrayUtils = org.apache.commons.lang3.ArrayUtils

import FileOutputStream = java.io.FileOutputStream;
import FileInputStream = java.io.FileInputStream;
import IOUtils = org.apache.commons.io.IOUtils;

/**
 * 20220209 tuzw
 * 文件工具类
 */
class FileUtils {

    /**
    * 复制一个已存在的文件到指定目录
    * @param dirPath 存放复制后的目录路径，eg：/tempSaveZipFile/
    * @param copyFileOrFile 复制的文件路径 或者 文件对象，eg：/tempSaveZipFile/t.doc
    * @param newFileName 复制后的文件名.类型，eg：copy_t.doc
    * @return true | false
    */
    public copySignFileToDir(
        dirPath: string,
        copyFileOrFile: string | java.io.File,
        newFileName: string
    ): ResultInfo {
        if (!copyFileOrFile || !copyFileOrFile) {
            return { result: false, message: "复制文件操作, 参数不合法" };
        }
        if (typeof copyFileOrFile == 'string') {
            copyFileOrFile = new java.io.File(copyFileOrFile);
        }
        if (!copyFileOrFile.exists()) {
            return { result: false, message: "复制的文件不存在" };
        }
        if (!copyFileOrFile.isFile()) {
            return { result: false, message: "复制的文件不是文件" };
        }
        let saveFiledir = new java.io.File(dirPath);
        if (!saveFiledir.exists()) {
            return { result: false, message: "复制后的文件所存放目录不存在" };
        }
        if (!saveFiledir.isDirectory()) {
            return { result: false, message: `dirPath参数值不是一个目录` };
        }
        if (!newFileName) {
            newFileName = copyFileOrFile.getName(); // eg：test.txt
        }
        if (`${copyFileOrFile.getParent()}/` == dirPath) { // 若同处于一个目录下，则增加前缀：copy_
            newFileName = `copy_${newFileName}`;
        }
        let newFile = new java.io.File(`${dirPath}${newFileName}`);
        if (!newFile.exists()) {
            newFile.createNewFile();
        }
        let resultInfo: ResultInfo = { result: true };
        let fileOut: FileOutputStream = null;
        let fileIn: FileInputStream = null;
        try {
            fileOut = new FileOutputStream(newFile);
            fileIn = new FileInputStream(copyFileOrFile);
            IOUtils.copy(fileIn, fileOut);
        } catch (e) {
            resultInfo.result = false;
            resultInfo.message = "复制一个已存在的文件到指定目录失败";
            print(e);
        } finally {
            if (fileOut != null) {
                fileOut.close();
            }
            if (fileIn != null) {
                fileIn.close();
            }
        }
        return resultInfo;
    }

    /**
     * 读取url文件二进制流，创建指定类型文件
     * @param fileName 绝对路径，eg：/tempSaveZipFile/t.doc
     * @param fileType 文件类型，eg：txt、doc
     * @return true | false
     */
    public createFile(filePath: string, fileType: string, fileIoByte: number[]): boolean {
        if (!filePath || !fileType) {
            return false;
        }
        let isSuccess: boolean = true;
        let file = new java.io.File(filePath);
        if (!file.exists()) {
            let parentFile = file.getParentFile();
            if (!parentFile.exists()) {
                parentFile.mkdirs();    // 若文件父目录不存在，则创建文件以及父文件目录
            }
            file.createNewFile(); // 创建真实的文件
        }
        file = getFile(filePath); // 读取文件
        let outStream = file.openOutputStream();
        try {
            outStream.write(fileIoByte as number[]);
        } catch (e) {
            file.delete();
            isSuccess = false;
            print(`--写入文件${filePath}内容失败---`);
            print(e);
        } finally {
            outStream.close();
        }
        return isSuccess;
    }

    /**
     * 读取【下载链接】文件内容 和 文件名.类型
     * @param url 文件下载链接
     * @return eg: { fileIo: [文件二进制流数组], "test.png" }
     */
    public getFileIo(url: string): { result: boolean, fileIo?: ArrayLike<number> | undefined | string, fileName?: string } {
        print("读取【下载链接】文件内容 和 文件名.类型");

        let resultInfo = { result: false, fileIo: [], fileName: "" };
        let client = HttpClients.createDefault();
        let response;
        try {
            let httpGet = new HttpGet(url);
            response = client.execute(httpGet);
            /**
             * 20220927 tuzw
             * 获取文件名，不可使用getValuegetValue来获取，改为直接使用toString()方法将其对象转换为字符串
             * let fileName = response.getFirstHeader("Content-Disposition").getValuegetValue().split(';')[1].split('=')[1];
             * class org.apache.http.message.BufferedHeader ——> Content-Disposition: inline; filename="926/0-220925-wx-207109-1.jpg"
             */
            let contentDisposition = response.getFirstHeader("Content-Disposition").toString();
            let fileName = contentDisposition.split(';')[1].split('=')[1];

            let entity = response.getEntity();
            let contentType = response.getFirstHeader("Content-type");
            let fileType: string = "";
            if (!!contentType) { // 考虑到zip文件，不包含：Content-type，此处需要进行空校验，若不包含，则直接根据文件名来匹配文件类型
                fileType = contentType.getValue();
            } else {
                fileType = fileName; // 通过indexOf来匹配文件名所包含文件类型，故此处直接将文件名赋值给文件类型
            }
            if (fileType.indexOf("image") !== -1 || fileType.indexOf("pdf") !== -1 ||
                fileType.indexOf("jpg") !== -1 || fileType.indexOf("png") !== -1 || fileType.indexOf("mp4") !== -1
                || fileType.indexOf("jpeg") !== -1 || fileType.indexOf("zip") !== -1 || fileType.indexOf("bmp") !== -1) {
                /**
                 * 20220208 tuzw 
                 * 对方文件夹含有大量的中文名，直接获取是一串乱码，需要将其转换为utf-8
                 */
                let encodeFileName = new java.lang.String(fileName.getBytes("ISO-8859-1"), "utf-8");
                resultInfo = {
                    result: true,
                    fileIo: EntityUtils.toByteArray(entity),
                    fileName: encodeFileName.replace(/\"/g, "") // 去除字符串左右‘双引号’
                }
            } else {
                let content = EntityUtils.toString(entity);
                if (content.indexOf("认证失败") !== -1) {
                    print("认证失败");
                    print(content);
                }
            }
        }
        finally {
            response && response.close();
            client && client.close();
        }
        return resultInfo;
    }

    /**
     * 删除指定文件 或者 空文件夹
     * @description delete方法仅删除文件或者空目录
     * @description 删除文件夹以及内部所有子文件夹, fs.deleteFile("") 会将当前文件夹也一起删除
     */
    public deleteSignFileOrDir(deleteFileOrDirPath: string): ResultInfo {
        let deleteFile = new java.io.File(deleteFileOrDirPath);
        if (!deleteFile.exists()) {
            return { result: true, message: "文件不存在" };
        }
        if (deleteFile.isFile()) {
            return deleteFile.delete();
        }
        if (deleteFile.isDirectory()) {
            let listChildrenFiles = deleteFile.listFiles();
            if (listChildrenFiles.length != 0) {
                return { result: false, message: "文件夹内存在子文件, 无法删除" };
            }
            return deleteFile.delete();
        }
        return { result: true };
    }

    /**
     * 显示指定目录下的子文件和文件夹情况
     * @param dirPath 目录路径，eg：/tempSaveZipFile/
     */
    public showFolderOrFile(dirPath: string): ResultInfo {
        if (!dirPath) {
            return { result: false, message: "显示目录为空" };
        }
        let dir = new java.io.File(dirPath);
        if (!dir.isDirectory()) {
            return { result: false, message: "传入的路径不是一个目录" };
        }
        print("当前目录下的子文件和文件夹情况：");
        let childFolders: File_[] = dir.listFiles();
        if (childFolders.length > 0) {
            for (let i = 0; i < childFolders.length; i++) {
                print(childFolders[i].getAbsolutePath());
            }
        }
        return { result: true };
    }

    /**
     * 格式化时间戳
     * @parmas dates 需要格式化的时间
     * @return 格式化后的日期，2021-12-12
     */
    public formatDate(dates: Date): string {
        let Year: number = dates.getFullYear();
        let Months = (dates.getMonth() + 1) < 10
            ? '0' + (dates.getMonth() + 1)
            : (dates.getMonth() + 1);
        let Day = dates.getDate() < 10
            ? '0' + dates.getDate()
            : dates.getDate();
        return Year + '-' + Months + '-' + Day;
    }
}
const fileUtils = new FileUtils();
