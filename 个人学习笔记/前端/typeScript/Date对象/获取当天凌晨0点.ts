import LocalDateTime = java.time.LocalDateTime;
import LocalTime = java.time.LocalTime
import ZoneOffset = java.time.ZoneOffset;

/**
 * 启动入口
 */
function main() {
   let now = LocalDateTime.now();
    let currentDay = LocalDateTime.of(now.toLocalDate(), LocalTime.MIN);
    print(currentDay);
	
    let times = currentDay.toInstant(ZoneOffset.of("+8")).toEpochMilli(); // 获得时间毫秒数，以北京时间为准
	
	
	let currentDay = new Date(new Date().toLocaleDateString()); // 存在误差，早8小时
	
	print(`-----${tableName}已经初始过，开始增量请求【前天凌晨0点------当天凌晨0点】新增的数据，当前执行时间为： ${formatDate(new Date(times))}`);
}


/**
 * 格式化时间戳
 * @parmas dates 需要格式化的时间
 * @return 格式化后的日期，2021-12-12 00:00:00 
 */
function formatDate(dates: Date): string {
    let Year: number = dates.getFullYear();
    let Months = (dates.getMonth() + 1) < 10
        ? '0' + (dates.getMonth() + 1)
        : (dates.getMonth() + 1);
    let Day = dates.getDate() < 10
        ? '0' + dates.getDate()
        : dates.getDate();
    let Hours = dates.getHours() < 10
        ? '0' + dates.getHours()
        : dates.getHours();
    let Minutes = dates.getMinutes() < 10
        ? '0' + dates.getMinutes()
        : dates.getMinutes();
    let Seconds = dates.getSeconds() < 10
        ? '0' + dates.getSeconds()
        : dates.getSeconds();
    return Year + '-' + Months + '-' + Day + ' ' + Hours + ':' + Minutes + ':' + Seconds;
}