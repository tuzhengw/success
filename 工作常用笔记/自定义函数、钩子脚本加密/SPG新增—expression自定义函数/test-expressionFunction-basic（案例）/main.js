define(["require", "exports", "metadata/metadata"], function (require, exports, metadata_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.TEST_USER_EXTPROPERTY_ASYNC = exports.TEST_USER_EXTPROPERTY_SYNC = void 0;
    function TEST_USER_EXTPROPERTY_SYNC(context, userIdOrProperty, property) {
        if (!property) {
            property = userIdOrProperty;
            userIdOrProperty = metadata_1.getCurrentUser().userId;
        }
        return userIdOrProperty + ' ' + property + '属性值';
    }
    exports.TEST_USER_EXTPROPERTY_SYNC = TEST_USER_EXTPROPERTY_SYNC;
    function TEST_USER_EXTPROPERTY_ASYNC(context, userIdOrProperty, property) {
        if (!property) {
            property = userIdOrProperty;
            userIdOrProperty = metadata_1.getCurrentUser().userId;
        }
        return Promise.resolve(userIdOrProperty + ' ' + property + '属性值');
    }
    exports.TEST_USER_EXTPROPERTY_ASYNC = TEST_USER_EXTPROPERTY_ASYNC;
});
//# sourceMappingURL=main.js.map