# 慎用forEach

# 1 误用在查找

```ts
/**
 * 目的：找到元素5，结束方法
 */
function test() {
    var arr = [4, 5, 6]; // 各元素值不同
    arr.forEach((item) => {
        if (item == 5) {
            print(item);
            return; // return 没有用，循环还是会继续执行
        }
        print("继续查找元素：5");
    });
}
```

```ts
test();
继续查找元素：5
5
继续查找元素：5
```

​	**结论：**`forEach`内部的：return 无效

----

# 2 误用在删除元素

```ts
/**
 *  删除大于4的元素
 */
function test() {
    var arr = [4, 5, 6];
    arr.forEach((item, index) => {
        if (item > 4) {
            arr.splice(index, 1);
        }
    });
    print(arr);
}
```

```ts
test();
// arr数组为：
[4, 6]
// 原因：删除 5 后，6向前移动一位，导致循环结束
```

​	结论：结果只循环了 2 次，6 根本就没有进入循环，原来这个删除了之后，数组后面的前移了，但是指针还在后移，就漏掉了一个元素。