
import { IDwTableEditorPanel, DwTableEditor } from 'dw/dwtable';
import { LineageAnalysisView, DwLineageAnalysisGraph } from 'dw/dwview';
import { HierarchyGraph, HierarchyGraphNode } from 'commons/graph/graph';

/**
 * 数据模型扩展点
 */
export class TblManage {
	
	/**
	 * 对子面板进行操作导致界面刷新后前进行操作
	 * @param panel 
	 */
	public onDidRefreshPanel(panel: IDwTableEditorPanel): void | Promise<void> {
		this.showModelAnalysePageAllNode(panel);
	}
	
	
	/**
	 * 若数据模型展示为血统分析页面，则默认展开所有节点
	 * @param panel 数据模型对象
	 * 
	 * 20220902 tuzw
	 * 地址：https://jira.succez.com/browse/CSTM-20310
	 * 
	 * 20220906 tuzw
	 * 获取的跟节点存在（来源目标节点和来源节点），若两者都不存在，则不执行展开
	 */
	private showModelAnalysePageAllNode(panel: IDwTableEditorPanel): void {
		let lineageView: LineageAnalysisView.DwLineageView = panel?.panels?.modelLineageAnalysis?.lineageView;
		if (isEmpty(lineageView)) {
			return;
		}
		let dwLineageAnalysisGraph: DwLineageAnalysisGraph = lineageView?.getGraph();
		let dataInitPromise = dwLineageAnalysisGraph.waitReady();
		showWaiting(dataInitPromise).then(() => { // 等待数据加载完毕
			let hierarchyGraph: HierarchyGraph = dwLineageAnalysisGraph.hierarchyGraph;
			let nodeInfo = hierarchyGraph.getRoot();
			let rootNode: HierarchyGraphNode = nodeInfo.rootTarget;
			if (isEmpty(rootNode)) {
				rootNode = nodeInfo.rootSource;
			}
			if (isEmpty(rootNode)) {
				return;
			}
			dwLineageAnalysisGraph.expand(rootNode, true); // 初次打开自动展开全部节点
		});
	}
}