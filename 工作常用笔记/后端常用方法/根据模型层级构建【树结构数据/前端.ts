import { DwDataHierarchy, getDwTableDataManager } from "dw/dwapi";
import { DwDataHierarchyProviderArgs } from "dw/dwdps";
import { ready } from "sys/sys";

export function getData(){
    return ready().then(()=>{ // 先进行国际化，否则，直接请求会国际化失败
        let path = "/HLHT/data/tables/xml/SDI_HLHT_WORD_STRUCTURE.tbl";
        let hierarchyParmas: DwDataHierarchyProviderArgs = {
            dwTablePath: path, 
            hierarchy: "SZ_LEVEL", 
            sorts: [{ 
                exp: "CREATE_TIME", type: "asc" 
            }] 
        }
        let promise = getDwTableDataManager().getDwDataHierarchy(hierarchyParmas);
        return promise.then((hierarchy: DwDataHierarchy) => {
            let treeItemMap =hierarchy.itemMap;
			// ...获取数据后需要处理
            return ""; // 仅返回字符串
        })
    })
}
