

import db from "svr-api/db";
import dw from "svr-api/dw";
import http from "svr-api/http";
import metadata from "svr-api/metadata";
import utils from "svr-api/utils";
import sys from "svr-api/sys";

import Files_ = java.nio.file.Files;
import Paths_ = java.nio.file.Paths;
import JavaFile = java.io.File;
import ZipOutputStream = java.util.zip.ZipOutputStream;
import ZipEntry = java.util.zip.ZipEntry;
import FileOutputStream = java.io.FileOutputStream;
import FileInputStream = java.io.FileInputStream;
import IOUtils = org.apache.commons.io.IOUtils;
import JavaString = java.lang.String;
import BufferedWriter = java.io.BufferedWriter;
import FileWriter = java.io.FileWriter;


import HttpClients = org.apache.http.impl.client.HttpClients;
import EntityUtils = org.apache.http.util.EntityUtils;
import HttpGet = org.apache.http.client.methods.HttpGet;
import File_ = java.io.File; // 工具类不需要对象，直接import导入

import ZipOutputStream = java.util.zip.ZipOutputStream;
import ZipEntry = java.util.zip.ZipEntry;
import ArrayUtils = org.apache.commons.lang3.ArrayUtils

import FileOutputStream = java.io.FileOutputStream;
import FileInputStream = java.io.FileInputStream;
import IOUtils = org.apache.commons.io.IOUtils;

/** 
 * 20220209 tuzw
 * 文件压缩工具类
 * 解压和压缩 可参考中台备份和恢复功能
 * 地址：https://dev.succez.com/sdi2/sysdata/app/zt.app?:edit=true&:file=systemMgr.action.ts
 */
class ZipUtils {

    /**
     * 创建指定文件类型的临时文件
     * @param fileName 文件名, eg: text
     * @param fileType 文件类型, eg: txt
     * @param fileContent 文件内容
     */
    public createSignFile(fileName: string, fileType: string, fileContent: string): JavaFile {
        let tempFile = JavaFile.createTempFile(fileName, `.${fileType}`);
		tempFile.deleteOnExit(); // 在程序退出时删除临时文件
		
        console.info(`临时文件存放目录: ${tempFile.getAbsolutePath()}`);
        let bufferedWrite: BufferedWriter = null;
        try {
            bufferedWrite = new BufferedWriter(new FileWriter(tempFile));
            bufferedWrite.write(fileContent);
        } catch (e) {
            console.error(`创建文件[${fileName}.${fileType}] 失败----error`);
            console.error(e);
        } finally {
            bufferedWrite && bufferedWrite.close();
        }
        return tempFile;
    }

    /**
     * 将文件对象写入到压缩包对应的条目中
     * @param zipOutputStream 导出压缩文件对象
     * @param zipEntryPath 文件在压缩包位置, eg: zt.app/data-access/test.ts
     * @param file 文件对象
     * 
     * <p>
     *  1 文件写入产品使用的编码为: utf-8, 使用方式将文件内容转换为JAVA字符串.getBytes(MetaConst.UTF8)
     * </p>
     */
    public writeFileToEntry(zipOutputStream: ZipOutputStream, zipEntryPath: string, file: JavaFile): ResultInfo {
        let resultInfo: ResultInfo = { result: true };
        let fileName: string = file.getName();
        /**
         * 将创建好的条目加入到压缩文件中后就可开始往当前创建文件（条目）内写入内容（压缩包往最后创建的条目中写入）。
         * 若文件已存在, 则覆盖已有的内容。
         */
        let ztAppEntry = new ZipEntry(zipEntryPath);
        zipOutputStream.putNextEntry(ztAppEntry);

        if (file.getContentAsString() == null) { // 判断文件内容是否为空
            return { result: true };
        }
        let input: FileInputStream = null;
        try {
            input = new FileInputStream(file);
            IOUtils.copy(input, zipOutputStream);
        } catch (e) {
            resultInfo.result = false;
            resultInfo.message = `文件信息: [ ${fileName}] 写入到压缩包对应的条目中失败`;
            console.info(`writeFileToEntry执行失败, 当前处理的文件为: ${fileName}`);
            console.info(e);
        } finally {
            input && input.close();
        }
        zipOutputStream.closeEntry();
        return resultInfo;
    }

    /**
     * 创建压缩zip文件
     * @param filePath 文件路径, eg: .../file-storage/relativePath/noteFileZip.zip
     * @return 
     */
    public createZipFile(filePath: string): JavaFile {
        let zipFile = new JavaFile(filePath);
        let parentFile: JavaFile = zipFile.getParentFile();
        if (!parentFile.exists()) {
            parentFile.mkdirs(); // 递归创建所有不存在的父级目录
        }
        if (!zipFile.exists()) {
            zipFile.createNewFile();
        }
        return zipFile;
    }
	
	/**
     * 功能：压缩多个文件（可是ZIP文件）成一个zip文件
     * @param files       压缩的文件绝对路径集，eg：["/zhjg/workdir/wljy_fjcc/", "/zhjg/workdir/wljy_fjcc/t.txt"]
     * @param zipFilePath 压缩后的文件路径（绝对路径），eg：.../tempSaveZipFile/0-220119-wx-80908-1.zip
     * eg：
     *  let files: string[] = ["/zhjg/workdir/0-220119-wx-80908-1/1.png", "/zhjg/workdir/0-220119-wx-80908-1/2.pdf"];
        let zipFilePath: string = `${workDir}/2022-01-04/tempSaveZipFile/0-220119-wx-80908-1.zip`;
        zipFiles(files, zipFilePath);
     */
    public zipFiles(files: string[], zipFilePath: string): void {
        print("zipFiles()，开始压缩导出文件");
        if (files == null || files.length == 0 || !zipFilePath) {
            return;
        }
        let zipFile = new java.io.File(zipFilePath);
        if (!zipFile.exists()) {
            print("压缩文件不存在，已自动创建");
            zipFile.createNewFile();
        }
        let out: ZipOutputStream = null;
        try {
            out = new ZipOutputStream(new FileOutputStream(zipFile));
            for (let i = 0; i < files.length; i++) { // 
                let sendFile = new File_(files[i]);
                if (!sendFile.exists()) {
                    print(`文件【${files[i]}】不存在服务器上`);
                    continue;
                }
                let fileBytes = Files_.readAllBytes(Paths_.get(sendFile.getAbsolutePath())); // 通过绝对路径获取文件字节流
                out.putNextEntry(new ZipEntry(sendFile.getName()));
                out.write(fileBytes);
            }
        } catch (e) {
            print(`zipFiles()，开始压缩导出文件--error`);
            print(e);
        } finally {
            if (out != null) {
                out.close();
            }
        }
    }

    /**
     * 压缩多个文件夹 或 文件
     * @param dirs           要压缩的文件夹 或 文件，eg：[]
     * @param zipFile        压缩后的文件路径（绝对路径），eg：.../tempSaveZipFile/0-220119-wx-80908-1.zip
     * @param includeBaseDir 是否将内部文件夹一起压缩
     */
    public zipDirs(dirs: string[], zipFilePath: string, includeBaseDir: boolean): void {
        print("zipDirs()，压缩多个文件夹开始");
        if (dirs == null || dirs.length == 0 || zipFilePath == null || zipFilePath == "") {
            return;
        }
        let zipOut: ZipOutputStream = null;
        let zipFile = new java.io.File(zipFilePath);
        if (!zipFile.exists()) {
            let parentFile = zipFile.getParentFile();
            if (!parentFile.exists()) {
                parentFile.mkdirs();    // 若文件父目录不存在，则创建文件以及父文件目录
            }
            zipFile.createNewFile(); // 创建真实的文件
        }
        try {
            /**
             * 导出多个文件夹
             * 思路：压缩后直接将其文件流写入：response
             * zipOut = new ZipOutputStream(new FileOutputStream(response.getOutputStream()));
             * 
             * 参考网址：https://juejin.cn/post/6994374601329344548#heading-5
             */
            zipOut = new ZipOutputStream(new FileOutputStream(zipFile));
            for (let i = 0; i < dirs.length; i++) {
                print(`当前压缩的文件为：${dirs[i]}`);
                let fileDir = new File_(dirs[i]);
                let baseDir: string = ""; // 最外层目录
                if (includeBaseDir) {
                    baseDir = fileDir.getName();
                }
                this.compress(zipOut, fileDir, baseDir);
            }
        } catch (e) {
            print("压缩多个文件夹错误")
            print(e);
        } finally {
            if (zipOut != null) {
                zipOut.close();
            }
        }
        print("zipDirs()，压缩多个文件夹结束");
    }

    /**
     * 采用递归的方式，将文件夹下的文件放入到指定的压缩包中
     * @param zipOut    压缩对象
     * @param folder    文件夹 或 文件对象
     * @param baseDir   文件夹 或 文件名
     */
    private compress(zipOut: ZipOutputStream, folderOrFile: File_, baseDir: string): void {
        if (!folderOrFile.exists()) {
            print(`当前压缩的目录不存在，${folderOrFile.getAbsolutePath()}`);
            return;
        }
        if (folderOrFile.isDirectory()) {
            baseDir = baseDir.length == 0
                ? ""
                : baseDir + File_.separator;
            let childFiles: File_ = folderOrFile.listFiles();
            if (ArrayUtils.isEmpty(childFiles)) {
                return;
            }
            for (let i = 0; i < childFiles.length; i++) {
                this.compress(zipOut, childFiles[i], baseDir + childFiles[i].getName());
            }
        } else {
            zipOut.putNextEntry(new ZipEntry(baseDir));
            let fileIn: FileInputStream = null;
            try {
                fileIn = new FileInputStream(folderOrFile);
                IOUtils.copy(fileIn, zipOut);
            } catch (e) {
                print("文件夹下的文件放入到指定的压缩包中失败");
                print(e);
            } finally {
                if (fileIn != null) {
                    fileIn.close();
                }
            }
        }
    }
}
let zipUtils = new ZipUtils();