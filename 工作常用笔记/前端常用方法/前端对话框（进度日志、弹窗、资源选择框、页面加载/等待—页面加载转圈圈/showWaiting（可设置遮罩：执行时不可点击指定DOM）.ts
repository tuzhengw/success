/**
 * 通过前端配置的参数，请求后端处理相关检测表信息
 * @param event
 * @param checkType  checkType 检测的类型
 * （1）temp 临时计划，随机生成
 * （2）format 正式执行，指定执行任务ID
 * （3）handExecute 手动执行，指定执行的任务id，修改增量方式、最后成功检测时间字段，执行完后，【还原】原有指定计划的设置
 * @param executeRuleNumberType  检测规则数量大小类型
 * （1）apart 部分，执行勾选的数据
 * （2）all 执行全部（默认）
 *
 * @param checkRules 页面勾选所执行的规则信息，可为：[]
 * @param checkTableSettings 检测表相关配置信息
 *
 * @param showExecuteDetailPage? 是否展示检测详情页面
 * @param ruleFilterConditon?  规则过滤条件EXP（检测规则数量：all时指定，可不传），rf：规则库引入信息表 、r：规则库表
			eg：rf.MAIN_TABLE_ID= 'QGkIxQOqy3MSgfXUectmZC' AND (r.RULE_SET LIKE '%2%' OR r.RULE_SET LIKE '%7%')
 * @param executeExistCheckTaskId? 检测任务ID（已存在的）
 * @param runingBanComponentId? 运行时指定控件设置为灰色状态
 */
public requestDealRuleInfo(
	event: InterActionEvent,
	checkType: string,
	executeRuleNumberType: string,
	checkRules: CheckRule[],
	checkTableSettings: CHECK_TABLE_SETTING[],
	showExecuteDetailPage: boolean,
	ruleFilterConditon?: string,
	executeExistCheckTaskId?: string,
	runingBanComponentId?: string
): Promise<any> {
	let page = event.page;
	return rc({
		url: `${CURRENT_PROJECT_ADRESS}ruleCheck/ruleCheck.action?method=dealRuleTable`,
		method: "POST",
		data: {
			choseRuleData: checkRules,
			checkType: checkType,
			executeRuleNumberType: executeRuleNumberType,
			ruleFilterConditon: ruleFilterConditon,
			executeExistCheckTaskId: executeExistCheckTaskId,
			checkTableSettings: checkTableSettings
		}
	}).then((result: ResultInfo) => {
		if (!result.result) {
			showWarningMessage(`---【${result.message}】---`);
			return;
		}
		let checkTaskId: string = result.data.checkTaskId;
		if (!checkTaskId) {
			showWarningMessage(`---检测任务ID为：【${result.message}】---`);
			return;
		}
		let promise: ServiceTaskPromise<any> = runCheckTask(checkTaskId, uuid());
		if (showExecuteDetailPage) {
			showProgressDialog({
				caption: "任务执行日志",
				uuid: checkTaskId,
				promise: promise,
				buttons: [{
					id: "cancel",
					caption: "关闭"
				}],
				logsVisible: true,
				width: 700,
				height: 500
			});
		}
		// let noShowDom = page.getComponent("panel18").component.domParent; 
		/**
		 * showWaiting() 可设置任务未结束前，指定某个DOM被遮罩（将指定dom设置为灰色状态（由于showWaiting指定dom遮罩层不生效，改为遮罩整个body））
		 *
		 * 20220419 tuzw
		 * 更改当前任务执行进度的等待状态，默认：转圈圈
		 * 指定class样式：layoutTheme: "waiting-ring-active"
		 */
		return showWaiting(promise, document.body, true, { layoutTheme: "waiting-ring-active" }).then(() => {
			throwInfo("检测完毕");
			page.setParameter("checkTaskId", checkTaskId); // 将checkTaskId设置为全局变量
			getDwTableDataManager().updateCache([{ // 考虑脚本存在【异步】，检测任务ID更新后，需要更新【检测结果表】数据，刷新使用脚本执行
				path: "/sysdata/data/tables/dg/confs/FORMAT_CHECK_RESULT.tbl",
				type: DataChangeType.refreshall
			}, {
				path: "/sysdata/data/tables/dg/confs/IMITATE_CHECK_RESULT.tbl",
				type: DataChangeType.refreshall
			}]);
		}).catch((e) => {
			showWarningMessage(`检测失败`);
		});
	});
	}