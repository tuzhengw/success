# HttpClient的使用(GET和POST请求、Json参数)

```
httpClient版本为4.5.3

建议通过HttpGet获取信息，HttpPost提交信息

而HttpGet获取信息时需要提交的参数一般会在url中体现，或者以？ &传参，所以就没写HttpGet带参数的。
```

## 1 添加依赖

```xml
<!--http-->
<dependency>
    <groupId>org.apache.httpcomponents</groupId>
    <artifactId>httpclient-cache</artifactId>
    <version>4.5.3</version>
</dependency>

<dependency>
    <groupId>org.apache.httpcomponents</groupId>
    <artifactId>httpmime</artifactId>
    <version>4.5.3</version>
</dependency>

<!-- fastjson -->
<dependency>
    <groupId>com.alibaba</groupId>
    <artifactId>fastjson</artifactId>
    <version>1.2.47</version>
</dependency>
```

## 2 代码实现

```java
import com.alibaba.fastjson.JSON;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
/**
 * 1） GET请求  参数携带在url中
 * 2） POST请求 带参数
 * 3） POST请求 不带参数
 * 4） POST请求 携带Json格式的参数
 * 5） POST请求 BASE64加密参数
 */
public class HttpClientUtils {
    
    private static final String CONTENT_TYPE_TEXT_JSON = "text/json";
    
    private static final CloseableHttpClient httpClient = HttpClients.createDefault();
    
    private static final RequestConfig requestConfig = RequestConfig.custom() 
                                       .setConnectTimeout(2000)
                                       .setSocketTimeout(10000).build();
```

### 1）GET请求-参数携带URL中

```java

/**
 * GET请求  参数携带在url中
 *
 * @param url
 * @return
 */
public static String executeGet(String url) {
    HttpGet httpGet = new HttpGet(url);
    httpGet.setConfig(requestConfig);
    CloseableHttpResponse response = null;
    String result = null;

    try {
        response = httpClient.execute(httpGet);

        HttpEntity entity = response.getEntity();
        if (entity != null) {
            result = EntityUtils.toString(entity, "UTF-8");
        }
    } catch (IOException e) {
        e.printStackTrace();
    } finally {
        try {
            response.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    return result;
}
```

### 2）POST请求-带参数

```java
/**
 * POST请求 带参数
 *
 * @param url 请求地址
 * @param map 参数
 * @return
 */
public static String executePost(String url, Map<String, String> map) {
    List<NameValuePair> params = new ArrayList<>();
    for (Map.Entry<String, String> entry : map.entrySet()) {
        params.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
    }
    UrlEncodedFormEntity entity = null;
    String result = null;
    try {
        entity = new UrlEncodedFormEntity(params, "UTF-8");
        HttpPost httpPost = new HttpPost(url);
        httpPost.setConfig(requestConfig);
        httpPost.setEntity(entity);
        CloseableHttpResponse response = httpClient.execute(httpPost);

        HttpEntity httpEntity = response.getEntity();
        result = EntityUtils.toString(httpEntity, "UTF-8");
    } catch (IOException e) {
        e.printStackTrace();
    }
    return result;
}
```

- ts版本的携带参数

```ts
/**
 * 给指定手机号码发送短信，由于运营商限制，每天只能向同一个手机号发送5条短信
 * 参考地址：https://gsbi.succez.com/HBZHSCJG/sysdata/files?:open=fV4khoVu9bMxQjhJyOqDBE
 */
function sendSmsByInterface(mobile, content) {
	var DefaultHttpClient = Java.type('org.apache.http.impl.client.DefaultHttpClient');
	var HttpPost = Java.type('org.apache.http.client.methods.HttpPost');
	var EntityUtils = Java.type('org.apache.http.util.EntityUtils');
	var UrlEncodedFormEntity = Java.type('org.apache.http.client.entity.UrlEncodedFormEntity');
	var BasicNameValuePair = Java.type('org.apache.http.message.BasicNameValuePair');

	// 短信服务的用户名和密码,服务地址，目前只提供该接口服务，通用处理
	var CORP_ID = 'WHJS002157';
	var PWD = 'zm0513@';
	var SERVICE_URL = "http://sdk2.028lk.com:9880/sdk2/BatchSend2.aspx";
	var httpclient = new DefaultHttpClient();
	var SendTime = null;
	var Cell = null;
	try {
		var httppost = new HttpPost(SERVICE_URL);
		var nvps = [];
		nvps.push(new BasicNameValuePair("CorpID", CORP_ID));
		nvps.push(new BasicNameValuePair("Pwd", PWD));
		nvps.push(new BasicNameValuePair("Mobile", mobile));
		nvps.push(new BasicNameValuePair("Content", content));
		httppost.setEntity(new UrlEncodedFormEntity(nvps, 'GBK'));
		var response = httpclient.execute(httppost);
		var statusCode = response.getStatusLine().getStatusCode();
		var entity = response.getEntity();
		var result = "";
		if (statusCode == 200) {
			result = EntityUtils.toString(entity, 'utf-8'); // 取出应答字符串
		}
		return result;
	} catch (e) {
		return {
			statusCode: '500',
			respStr: e.message
		};
	}
}
```

### 3）POST请求-不带参数

```java
/**
 * POST请求   不带参数
 *
 * @param url
 * @return
 */
public static String executePost(String url) {
    HttpPost httpPost = new HttpPost(url);
    httpPost.setConfig(requestConfig);
    CloseableHttpResponse response = null;
    String result = null;
    try {
        response = httpClient.execute(httpPost);

        HttpEntity entity = response.getEntity();
        result = EntityUtils.toString(entity, "UTF-8");
    } catch (Exception e) {
        e.printStackTrace();
    }
    return result;
}
```

### 4） POST请求-携带Json格式的参数

```java
/**
 * POST请求  携带Json格式的参数
 *
 * @param url 请求地址
 * @param param JSON请求参数
 * @return
 * @throws IOException
 */
public static String postJson(String url, Object param) {
    HttpPost httpPost = new HttpPost(url);
    httpPost.setHeader("Content-Type", "application/json;charset=UTF-8");

    httpPost.setConfig(requestConfig);
    String parameter = JSON.toJSONString(param);
    StringEntity se = null;
    try {
        se = new StringEntity(parameter);
    } catch (UnsupportedEncodingException e) {
        e.printStackTrace();
    }
    se.setContentType(CONTENT_TYPE_TEXT_JSON);
    httpPost.setEntity(se);

    CloseableHttpResponse response = null;
    String result = null;
    try {
        response = httpClient.execute(httpPost);

        HttpEntity httpEntity = response.getEntity();
        result = EntityUtils.toString(httpEntity, "UTF-8");
    } catch (IOException e) {
        e.printStackTrace();
    }
    return result;
}
```

### 5）POST请求-BASE64加密参数

注意：

① 请求参数BASE64加密字符串包含：参数key和value

② ts写法

③ 帖子：https://jira.succez.com/browse/CSTM-18458

```ts
import util from "svr-api/utils";
import DefaultHttpClient = org.apache.http.impl.client.DefaultHttpClient;
import HttpPost = org.apache.http.client.methods.HttpPost;
import EntityUtils = org.apache.http.util.EntityUtils;
import UrlEncodedFormEntity = org.apache.http.client.entity.UrlEncodedFormEntity;
import BasicNameValuePair = org.apache.http.message.BasicNameValuePair;
import StringEntity = org.apache.http.entity.StringEntity;

/**
 * 发送短信，由于运营商限制，每天只能向同一个手机号发送5条短信
 * @param phones 手机号码集
 * @param sendContent 发送内容
 * 
 * @description 调用第三方短信接口, 发送短信
 */
function sendShortMessages(mobiles: string, sendContent: string): string | ResultInfo {
    if (!mobiles) {
        return { statusCode: ReturnStatusCode.NoSignPhone, respStr: "未获取到需要发送的手机号码" };
    }
    let resultInfo: ResultInfo = { statusCode: ReturnStatusCode.SuccessCode };
    let ecName: string = "南通市市场监督管理局";
    let apId: string = "admin";
    let secretKey: string = "spaqNT2021a!";
    let sign: string = "bM16CfN2B";
    let addSerial: string = "";
    let mac: string = util.md5([ecName, apId, secretKey, mobiles, sendContent, sign, addSerial].join(","));
    let sendParams: ShortMessageParam = {
        ecName: ecName,
        apId: apId,
        secretKey: secretKey,
        mobiles: mobiles,
        content: sendContent,
        sign: sign,
        addSerial: "",
        mac: mac
    };
    console.debug(sendParams);
    let base64Params: string = util.encodeBase64(JSON.stringify(sendParams));
    let shortMessageSendURL: string = `http://112.35.1.155:1992/sms/norsubmit`;
    let httpclient = new DefaultHttpClient();
    try {
        let httpost = new HttpPost(shortMessageSendURL);
        httpost.setHeader("Content-Type", "application/json;charset=UTF-8");
        let se: StringEntity = new StringEntity(base64Params); // 携带Json格式的参数(base64加密后的值)
        se.setContentType("text/json");
        httpost.setEntity(se);

        let response = httpclient.execute(httpost);
        let statusCode = response.getStatusLine().getStatusCode();
        let entity = response.getEntity();

        if (statusCode == 200) {
            let requestResult: InterfaceReturnMessage = EntityUtils.toString(entity, 'utf-8');
            console.debug(requestResult);

            let sendStatus: boolean = requestResult.success;
            let rspcod: string = requestResult.rspcod;
            let msgGroup: string = requestResult.msgGroup;

            if (sendStatus) {
                console.debug(`发送成功`);
            } else {
                resultInfo.statusCode = ReturnStatusCode.SendError;
                resultInfo.respStr = `短信发送失败: ${rspcod}`;
            }
        } else {
            console.debug(`请求发送接口失败`);
            console.debug(response);
            resultInfo.statusCode = statusCode;
            resultInfo.respStr = "请求短信接口失败";
        }
    } catch (e) {
        resultInfo.respStr = "发送服务器异常";
        resultInfo.statusCode = ReturnStatusCode.ServerError;
        console.error(`给手机号: [${mobiles}] 发送短信失败----error`);
        console.error(e);
    }
    return resultInfo;
}
```

### 6 ) XML请求参数

示例:  https://blog.csdn.net/weixin_44917045/article/details/125489163

```xml
<dependency>
	<groupId>org.apache.httpcomponents</groupId>
	<artifactId>httpclient</artifactId>
	<version>4.5.13</version>
</dependency>
```

```java
public static String postXmlRequest(String url, String xml) throws Exception {
	HttpPost post = new HttpPost(url);
	post.setHeader("Content-type", "text/xml");
	post.setEntity(new StringEntity(xml));

	CloseableHttpClient client = HttpClients.createDefault();
	CloseableHttpResponse response = client.execute(post);

	return response.getStatusLine().getStatusCode() == 200 ? EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8) : null;
}
```