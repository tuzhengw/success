import dw from "svr-api/dw";
import { getJSON, getFile } from 'svr-api/metadata';
import { uuid } from 'svr-api/utils'
import bean from "svr-api/bean";
import { sdiModelUtils } from "/sysdata/public/hooks/customUtils/modelUtils.action";
const Java_Integer = Java.type('java.lang.Integer');

/**
 * 数据交换提交数据通用脚本方法，校验成功则提交，失败全部打回并返回详细校验错误
 * 1. 暂仅支持insert
 * 2. 支持通用校验：非空项、数据长度、数据格式、维项是否存在等，其他业务校验暂不支持
 * @param path 模型路径
 * @param data 提交的数据，一维json数组形式
 * @param operation 对于数据库的操作类型，目前支持传入"insert"和"updata"和"merge"和"delete"
 * @param [checkRules] 校验规则，不传则不校验，格式详见{@link CheckRule}，示例如下：
 * ```json
 * {
 *  "field1": [{
 *      "ruleId": "SZ001",
 *      "desc": "[field1]不允许为空",
 *      "type": "allowNull",
 *      "allowNull": false
 *  }, {
 *      "ruleId": "SZ002",
 *      "desc": "[field1]长度要小于50",
 *      "type": "dataLength",
 *      "dataLength": 50
 *  }],
 *  "field2": ···
 * }
 * ```
 * @param limit 行数限制，不传默认为100
 * @return DXResourceScriptAPIResult
 */

/** 记录默认主键名 */
const keyNames = {};

/** 产品校验类实例对象 */
const dataChecker = bean.getBean("com.succez.dg.services.impl.DwUtilDataRulesInMemoryCheckerImpl");
/** 模型字段描述和物理字段名映射关系 */
let modelFieldRelation = {};

/**
 * 若数据全部合法，则提交
 * <p>
 *  20230606 tuzw 
 *  若接口属于高并发时, 调用此脚本, 唯一性校验可能会出现主键唯一性异常。
 *  原因: 接口高并发情况下, 在同一时间内, 推送的数据存在重复的情况下, 重复数据都未入库, 导致无法对其进行唯一性校验，
 * 被代码识别为成功, 等校验结束后, 两者同一时间写入库, 某条数据会被数据库识别为违法唯一性。
 * </p>
 * <p>
 *  20230613 tuzw
 *  代码内部已支持产品校验逻辑, 可读取模型的规则, eg: 自定义规则表达式等 对数据进行校验。
 *  注意: 
 *   1) 代码包含 自定义规则和模型本身的规则;
 *   2) 考虑到不是所有模型都会在模式本身上增加校验规则, 模型主键唯一性规则改成：自定义规则, 模型上不设置主键唯一性规则, 否则会二次校验主键唯一性, 导致数据与自己本身冲突;
 *   3) 除去主键唯一性规则, 其他字段唯一性规则可模型添加, 脚本内仅处理主键唯一性规则。
 * </p>
 * @params path 模型表路径
 * @params data 传入的数据, eg：[{ "id": "123", "age": 12 }]
 * @params operation 操作, eg：insert
 * @parmas checkRules 校验的规则, 可调用：ruleGenerator(模型表路径)自动生成
 * @params fieldNames 校验的数据库字段, eg： ["OganizationId", "OganizationName",...
 * @params limit 限制数量, 默认：100
 * @params partSubmit 是否部分合法提交, 默认：false
 * @return 
 * 成功：{
 *      "rowCount": 1,
        "data": {
            "result": true,
            "successIndexs": ["0", "1"] // 成功插入的数据下标
        }
 * }
 * 失败：{
    *   "error": "校验错误",
        "rowCount": 0,
        "data": {
            "result": false,
            "errorCode": "2003",
            "errorMessage": "校验错误",
            "checkResult": [{
                "rowNumber": 0,
                "errorResult": [{
                    "message": "[TestTime]格式必须为yyyy-MM-dd hh:mm:ss",
                    "field": "TestTime",
                    "data": "2020-05-06T00:00"
                }]
            }, {
                "rowNumber": [1],
                "errorResult": [{
                    "message": "OganizationId字段与表内主键重复",
                    "field": "OganizationId",
                    "data": "df4c027f44e9503cc901d1ceac2b2911111"
                }]
            }]
        }
 * }
 */
export function submitData(
    path: string,
    data: Record<string, FieldValue>[],
    operation: string,
    checkRules: { [fieldName: string]: CheckRule[] },
    fieldNames: string[],
    limit: number = 100,
    partSubmit?: boolean
) {
    console.debug(`开始执行[submitData], 提交[${path}]模型数据`);
    let rowCount = 0;
    let result: CommonResult = { result: true };
    let dataCheckResult = checkBatchRowsData(path, data, operation, checkRules, fieldNames, limit);
    if (dataCheckResult.result) {
        let rows = dataCheckResult.rows;
        let tempResult = sumbit(path, data, operation, fieldNames, rows);
        if (!tempResult.result) {
            result.result = false;
            result.errorMessage = tempResult.errorMessage;
        }
        /** submit传入的data是校验后合法的数据，插入数据长度为：rows.length */
        rowCount = rows.length;
        result.successIndexs = Object.keys(rows);
    } else {
        result = dataCheckResult; // 校验会记录合法和不合法校验结果, 部分合法提交仅做提交操作, 返回结果还是之前的校验结果
    }
    /**
     * 20220208 tuzw
     * 新增一个参数【partSubmit】来控制，部分数据合法是否进行提交
     * 注意：【数据存在错误】和需要进行部分提交时，才会走这部分逻辑
     */
    if (!dataCheckResult.result && !!partSubmit) {
        let partLegalResult = getPartLegalData(dataCheckResult, data, fieldNames);
        let rows: FieldValue[][] = partLegalResult.submitData;
        if (rows.length > 0) {
            let successIndexs: string[] = partLegalResult.successIndexs;
            rowCount = rows.length;
            let partDataSumbitResult: CommonResult = sumbit(path, data, operation, fieldNames, rows);
            if (partDataSumbitResult.result) {
                result.result = true;
                result.successIndexs = successIndexs;
                result.errorMessage = `数据存在部分合法, 并成功插入[${successIndexs.length}]条数据`;
            } else {
                result.result = false;
                result.errorMessage = partDataSumbitResult.errorMessage;
            }
        }
    }
    console.debug(result);
    console.debug(`执行结束[submitData], 提交[${path}]模型数据`);
    return { result: result.result, rowCount: rowCount, data: result };
}

/**
 * 检查错误数据，获取部分合法的数据信息
 * @param checkResult 校验的结果对象
 * @param datas 插入的数据
 * @param fields 对应请求表字段名集
 * @return {
 *    successIndexs: [], 
 *    submitData: [], 
 *    partLegalDatas: [], 
 *    partErrorDatas: []
 * }
 */
function getPartLegalData(checkResult: CommonResult, datas: Record<string, unknown>[], fields: string[]) {
    console.debug(`开始执行[getPartLegalData], 检查错误数据, 获取部分合法的数据信息`);
    if (datas == null) {
        return { successIndexs: [], submitData: [], partLegalDatas: [], partErrorDatas: [] };
    }
    /** 记录错误数据下标 */
    let errorDataIndexs: Array<number> = [];
    let errorRows = checkResult["checkResult"];
    if (!errorRows || errorRows.length == 0) {
        return { successIndexs: [], submitData: [], partLegalDatas: [], partErrorDatas: [] };
    }
    for (let i = 0; i < errorRows.length; i++) {
        let errorRow = errorRows[i];
        let rowNumber = errorRow["rowNumber"];
        /**
         * 主键重复会被划分为一组，其他则会单独分开
         * "rowNumber": [0.0, 1.0]
         * "rowNumber": 1.0
         */
        if (typeof rowNumber == "object") {
            let primaryKeys = rowNumber;
            for (let pk = 0; pk < primaryKeys.length; pk++) {
                let errorIndex = rowNumber[pk];
                if (errorDataIndexs.indexOf(errorIndex) == -1) { // 避免同一下标重复添加
                    errorDataIndexs.push(errorIndex);
                }
            }
        } else {
            let errorIndex = rowNumber;
            if (errorDataIndexs.indexOf(errorIndex) == -1) {
                errorDataIndexs.push(errorIndex);
            }
        }
    }
    let successIndexs: string[] = [];
    /** 不合法数据(用于日志记录) */
    let partErrorDatas: { [fieldName: string]: unknown }[] = [];
    let partLegalDatas: { [fieldName: string]: unknown }[] = [];
    let submitData: string[][] = [];
    if (errorDataIndexs.length != datas.length) { // 若存在合法数据, 则筛选合法数据
        for (let i = 0; i < datas.length; i++) {
            if (errorDataIndexs.indexOf(i) == -1) {
                partLegalDatas.push(datas[i]);
                successIndexs.push(i.toString());
            } else {
                partErrorDatas.push(datas[i]);
            }
        }
        submitData = changeSubmitDataForm(partLegalDatas, fields);
    }
    console.debug(`执行结束[getPartLegalData], 检查错误数据, 获取部分合法的数据信息`);
    return {
        successIndexs: successIndexs, // 若长度为0, 则没有合法数据
        submitData: submitData,
        partLegalDatas: partLegalDatas,
        partErrorDatas: partErrorDatas
    };
}

/**
 * 调整提交的数据（JSON--普通数组）
 * @params legalData：已经校验合法的数据(JSON)
 * @params fieldNames：插入的字段名数组
 *
 * 提交的数据格式不是JSON，而是一个二维基本类型数组，eg：
 * "rows": [{
 *          "id": "1001",
 *          "name": "张三",
 *          "sex": "男"
 *   }]
    转换后：
 *  "rows": [
        ["1001", "张三", "男"]
    ]
    该数组每行数据值跟传入的：fieldNames[]字段数组对应
    @return 基本类型的二维数组
 */
function changeSubmitDataForm(legalData, fieldNames: Array<string>): string[][] {
    if (legalData == null) {
        return [];
    }
    let submitDatas: string[][] = [];
    legalData.forEach(item => {
        let temp: string[] = [];
        fieldNames.forEach(column => {
            temp.push(item[`${column}`]);
        });
        submitDatas.push(temp);
    });
    return submitDatas;
}

/**
 * 检查插入的一批数据，如果校验通过那么返回待插入的值，如果不通过那么返回校验失败信息
 * @parmas path：模型表路径
 * @params data：检查的数据集
 * @params operation：操作（eg：insert）
 * @params checkRules：校验的规则
 * @params fieldNames：校验的字段集合
 * @params limit：限制的条数
 * @return {
*      	"result": false,
        "errorCode": "2003",
        "errorMessage": "校验错误",
        "checkResult": [{
            "rowNumber": 0, // 除主键重复，其他错误的rowNumber值为：number
            "errorResult": [{
                "message": "[TestTime]格式必须为yyyy-MM-dd hh:mm:ss",
                "field": "TestTime",
                "data": "2020-05-06T00:00"
            }]
        }, {
            "rowNumber": [1],  主键重复返回的rowNumber值为：Array<number>
            "errorResult": [{
                "message": "OganizationId字段与表内主键重复",
                "field": "OganizationId",
                "data": "df4c027f44e9503cc901d1ceac2b2911111"
            }]
        }]
 * }
 */
export function checkBatchRowsData(path: string, data: Record<string, FieldValue>[], operation: string, checkRules: { [fieldName: string]: CheckRule[] }, fieldNames: string[], limit: number = 100) {
    let dwTable: DwTableInfo = dw.getDwTable(path);
    modelFieldRelation = sdiModelUtils.getModelFieldNameAndDesc({
        modelPath: path
    }).modelFieldInfo;
    // @ts-ignore
    dataChecker.setDwTable(dwTable.getInnerWrappedObject());
    dataChecker.setFieldNames(fieldNames);

    /**
     * 由于有些表可能没有规则也没有主键，会导致传入{}空对象也能插入一条空数据，所有需要对数组内的对应先进行空对象检查
     */
    if (!data || data.length === 0) {
        return { result: false, errorCode: "2001", errorMessage: "参数缺失" };
    }
    if (data.length > limit) {
        return { result: false, errorCode: "2002", errorMessage: "数据行超过限制" };
    }
    let rows: FieldValue[][] = [];
    let checkResult: CheckDataResult[] = [];
    /** 唯一性校验字段值记录数据 */
    let uploadUniqueValues: UniqueFieldDataType = {};
    console.debug("用于校验同一批次上传数据唯一性");
    console.debug(data);

    /**
     * 20211025
     * postman调用时，foreach(ele, index){...} 无法获取index 下标值，这里改为for循环
     */
    for (let i = 0; i < data.length; i++) {
        const { row, checkDataResult } = checkRowData(i, data[i], checkRules, uploadUniqueValues, fieldNames);
        if (row && row.length > 0) {
            rows.push(row);
        }
        if (checkDataResult && checkDataResult.errorResult.length > 0) {
            checkResult.push(checkDataResult);
        }
    }
    console.debug("验证表内重复性");
    console.debug(uploadUniqueValues);

    for (let rowUniqueField in uploadUniqueValues) {  // todo 当前脚本暂时不支持多主键, 若需要支持可参考数据中台-数据交换
        let rowUniqueInfo = uploadUniqueValues[rowUniqueField];
        let dataRowIndexs: number[] = rowUniqueInfo.dataRowIndex;
        let uniqueValues: string[] = rowUniqueInfo.uniqueValues;
        let filterValues: string = uniqueValues.join(',');
        let queryInfo: QueryInfo = {
            sources: [{
                id: "model1",
                path: path
            }],
            /**
             * 若filterValues为逗号隔开的字符串, 生成的SQL: select t0.ID as "unique" from HBZHJG_ODS.ODS_SPJG_SPKJ_SAK t0 where (t0.ID IN ('12124','mac@-1691330856$31'))
             * 注意: 若值为空格, 内部会自动忽略。
             */
            filter: [{
                exp: `${rowUniqueField} = '${filterValues}'`
            }],
            fields: [{
                name: 'unique',
                exp: rowUniqueField.replace(new RegExp("\\+", "gm"), '+","+')
            }]
        };
        let uniqueQueryResult = dw.queryData(queryInfo);
        /** 唯一性字段值, eg:  ["mac@-1691330856$21", "mac@-1691330856$31"] */
        let queryDatas = uniqueQueryResult.data;
        switch (operation) {
            case "insert":
                let existUniqueDatas: string[] = [];
                /** 记录与表数据冲突下标 */
                let tableConflictIndex: number[] = [];
                for (let i = 0; i < queryDatas.length; i++) { // 循环对比唯一性值
                    for (let j = 0; j < uniqueValues.length; j++) {
                        if (queryDatas[i][0].toString().replace(new RegExp(',', "gm"), '') === uniqueValues[j]) {
                            tableConflictIndex.push(new Java_Integer(dataRowIndexs[j]));
                            existUniqueDatas.push(uniqueValues[j]);
                            break;
                        }
                    }
                }
                if (tableConflictIndex.length > 0) {
                    checkResult.push({
                        rowNumber: tableConflictIndex,
                        errorResult: [{
                            message: `${rowUniqueField.replace(new RegExp("model1.", "gm"), '').replace(new RegExp("\\+", "gm"), ',')}字段与表内主键重复`,
                            field: rowUniqueField.replace(new RegExp("model1.", "gm"), '').replace(new RegExp("\\+", "gm"), ','),
                            data: existUniqueDatas.join(",")
                        }]
                    });
                }
                break;
            case "updata":
                /** 记录未找到的行数，用于提示更新数据失败 */
                let notFoundRow: number[] = [];
                let notFoundDatas: string[] = [];
                if (queryDatas.length > 0) {
                    for (let i = 0; i < uniqueValues.length; i++) { // 修改
                        let j: number = 0;
                        for (j = 0; j < queryDatas.length; j++) {
                            if (queryDatas[j][0].toString().replace(new RegExp(',', "gm"), '') === uniqueValues[i]) {
                                break;
                            }
                        }
                        if (j === queryDatas.length) { // 若遍历整个查询结果都没有匹配值则视为不存在
                            notFoundRow.push(new Java_Integer(dataRowIndexs[i]));
                            notFoundDatas.push(uniqueValues[i]);
                        }
                    }
                } else {
                    notFoundRow = dataRowIndexs; // 若当前更新数据都不存在表中, 则都视为无法更新
                    notFoundDatas = [filterValues];
                }
                if (notFoundRow.length === 0) { // 若当前行不在表中, 视为无法更新
                    checkResult.push({
                        rowNumber: notFoundRow,
                        errorResult: [{
                            message: `${rowUniqueField.replace(new RegExp("model1.", "gm"), '').replace(new RegExp("\\+", "gm"), ',')}表内没有该行数据, 无法更新`,
                            field: rowUniqueField.replace(new RegExp("model1.", "gm"), '').replace(new RegExp("\\+", "gm"), ','),
                            data: notFoundDatas.join(",")
                        }]
                    });
                }
            default:
        }
    }
    console.debug("重复性严重完成");
    if (checkResult.length > 0) {
        return { result: false, errorCode: "2003", errorMessage: "校验错误", checkResult: checkResult };
    }
    return { result: true, rows };
}

/**
 * 检查单行数据，成功则返回json转换后的一维数组，失败则返回校验结果信息
 * <p>
 *  20230519 tuzw 
 *  地址: https://jira.succez.com/browse/CSTM-22947
 *  针对模型设置的规则, 脚本没有使用产品本身的校验逻辑, 都是读取规则配置自己去处理。
 *  优化: 将模型设置的规则从脚本中去除, 改为产品校验。
 * </p>
 * <p>
 *  20230531 tuzw
 *  地址: https://jira.succez.com/browse/BI-48317
 *  产品数据规则校验类优化空校验规则, 能够识别undefined等值
 * </p>
 * @params rowIndex 上传数据所在行下标
 * @params rowData 当前行数据
 * @params checkRules 校验规则
 * @params uploadUniqueValues 唯一性校验字段值记录数据
 */
function checkRowData(rowIndex: number, rowData: Record<string, FieldValue>, checkRules: { [fieldName: string]: CheckRule[] }, uploadUniqueValues: UniqueFieldDataType, fieldNamse: string[]) {
    // console.debug(`开始执行[checkRowData], 检查单行数据, 成功则返回json转换后的一维数组, 失败则返回校验结果信息`);
    let row: FieldValue[] = [];
    let errorResult: SingleFieldErrorInfo[] = [];
    let isEmptyObject = true;
    for (let key in rowData) {
        isEmptyObject = false;
        break;
    }
    if (!isEmptyObject) {
        for (let field of fieldNamse) {
            let fieldValue = rowData[field] as string;
            if (fieldValue == undefined) {
                fieldValue = null; // 产品的checkInert校验会忽略undefined的值
            }
            row.push(fieldValue);
            singleFieldCustomRuleLegalVerfiy(rowIndex, rowData, field, fieldValue, checkRules[field], uploadUniqueValues, errorResult);
        }
        let productCheckResult = dataChecker.checkInert(row, null);
        console.debug(`产品校验结果: ${productCheckResult}`);
        if (!productCheckResult) {
            let checkResults = dataChecker.getResults();
            for (let i = 0; i < checkResults.size(); i++) {
                let resultData = checkResults.get(i);
                let errorMessage: string = resultData.getDesc();
                if (errorMessage.indexOf("维表定义内") != -1) { // 考虑到产品数据范围校验不稳定，维表校验统一走自定义脚本校验
                    continue;
                }
                let errorFieldInfo = resultData.getErrorData();
                let keyNames: string[] = errorFieldInfo.keySet().toArray();
                let fieldName: string = keyNames[0];
                let fieldValue = errorFieldInfo.get(fieldName);
                errorResult.push({ message: errorMessage, field: modelFieldRelation[fieldName], data: fieldValue });
            }
        }
    } else {
        errorResult.push({ message: '不允许插入空对象!', field: '', data: '' });
    }
    // console.debug(errorResult);
    // console.debug(`执行结束[checkRowData], 检查单行数据, 成功则返回json转换后的一维数组, 失败则返回校验结果信息`);
    return {
        row: row,
        checkDataResult: <CheckDataResult>{
            rowNumber: new Java_Integer(rowIndex),
            errorResult: errorResult
        }
    }
}

/**
 * 根据记录的规则信息, 对单行数据进行自定义规则校验, 并记录不合法信息
 * @param rowIndex 上传数据所在行下标
 * @param rowData 校验行JSON数据
 * @param fieldName 字段名
 * @param fieldValue 字段值
 * @param fieldCheckRules 字段对应的规则
 * @param uploadUniqueValues 唯一性校验字段值记录数据
 * @param errorInfos 保存所有错误信息
 */
function singleFieldCustomRuleLegalVerfiy(
    rowIndex: number,
    rowData: { [fieldName: string]: FieldValue },
    fieldName: string,
    fieldValue: string,
    fieldCheckRules: CheckRule[],
    uploadUniqueValues: UniqueFieldDataType,
    errorInfos: SingleFieldErrorInfo[]
): void {
    if (!fieldCheckRules) {
        return;
    }
    let result = true;
    /**
     * 一个字段只需返回一条错误即可
     * desc：生成校验规则时就已经创建好的错误信息
     */
    fieldCheckRules.forEach(rule => {
        if (!result) {
            return;
        }
        if (fieldValue === undefined || fieldValue === null || fieldValue.toString().length === 0 || fieldValue.trim() == "") { // 暂时不考虑空格情况
            if (rule.type === CheckRuleType.AllowNull) {
                result = false;
            } else if (rule.type === CheckRuleType.CoustomNotNull) {
                /**
                 * 2022-09-14 luofuw
                 * 帖子地址: https://jira.succez.com/browse/CSTM-20213
                 * 场景: 一些特殊的条件不为空规则, 如: 当[aaa]字段大于2时, [bbb]不能为空，当前脚本功能不能很好的支持这个功能
                 * 功能: 增加'customNotNull'类型, 当rule中type为'customNotNull'时
                 * 执行执行rule.method中的自定义方法，完成这些特殊的条件不为空校验
                 */
                let method = rule.method;
                if (!method(fieldName, fieldValue, rowData)) {
                    result = false;
                }
            }
        } else {
            switch (rule.type) {
                case CheckRuleType.DataLength:
                    if ((fieldValue + '').length > rule.dataLength) {
                        result = false;
                    }
                    break;
                /**
                 * 2022-09-05 luofuw
                 * 帖子地址: https://jira.succez.com/browse/CSTM-20213
                 * 场景: 当前脚本没有关于小数点后位数的校验
                 * 功能: 增加'dataDecimal'类型, 当rule中type为'dataDecimal'时
                 * 将v中的小数点后位数与rule.dataDecimal中的小数位数比较，校验是否符合校验规则
                 */
                case CheckRuleType.DataDecimal:
                    if ((fieldValue + '').indexOf('.') != -1) {
                        if ((fieldValue + '').split('.')[1].length > rule.dataDecimal) {
                            result = false;
                        }
                    }
                    break;
                /**
                 * 2022-09-06 luofuw
                 * 帖子地址: https://jira.succez.com/browse/CSTM-20213
                 * 场景: 当v位数字类型且比较长时会转换成java中的Long或BigInteger类型，普通的js类型无法识别
                 * 功能: 增加对v的对象类型类型的判断，兼容java中的Long或BigInteger类型
                 */
                case CheckRuleType.DataType:
                    if (typeof fieldValue !== rule.dataType) {
                        if (toString.call(fieldValue) !== '[object java.lang.Long]' && toString.call(fieldValue) !== '[object java.math.BigInteger]') {
                            result = false;
                        }
                    }
                    break;
                case CheckRuleType.DisplayFormat:
                    try {
                        if (rule.displayFormat.length !== fieldValue.toString().length) {
                            result = false;
                            break;
                        }
                        let SimpleDateFormat = Java.type('java.text.SimpleDateFormat');
                        let dateTimeStamp;
                        if (checkDateFormat(fieldValue)) { // 日期格式为：yyyy-MM-ddTHH:mm:ss合法，这里将其统一为：yyyy-MM-dd HH:mm:ss
                            dateTimeStamp = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                        } else {
                            dateTimeStamp = new SimpleDateFormat(rule.displayFormat);
                        }
                        fieldValue = dateTimeStamp.parse(fieldValue);
                        // console.debug("格式化后的v: " + fieldValue);
                    } catch (e) { // 因为内部是java进行转换，这里若转换失败，则格式错误（保险的做法）
                        result = false;
                    }
                    break;
                case CheckRuleType.Unique:
                    let fieldString = 'model1.' + Array.prototype.join.call(rule.uniqueRefFields, "+model1.");
                    if (!uploadUniqueValues[fieldString]) {
                        uploadUniqueValues[fieldString] = {
                            dataRowIndex: [],
                            uniqueValues: []
                        };
                    }
                    let uniqueString = '';
                    rule.uniqueRefFields.forEach(field => {
                        uniqueString += rowData[field];
                    });
                    if (uploadUniqueValues[fieldString].uniqueValues.indexOf(uniqueString) === -1) { // 记录上次数据唯一性数据, 与表数据校验重复性
                        uploadUniqueValues[fieldString].dataRowIndex.push(rowIndex);
                        uploadUniqueValues[fieldString].uniqueValues.push(uniqueString);
                    } else {
                        result = false; // 上次数据中重复出现, 则视为次内数据唯一性重复
                    }
                    break;
                case CheckRuleType.Dim:
                    let dimdata = getDimData(rule.dim);
                    let vs = fieldValue.toString().split(',');
                    vs.forEach(v => {
                        let code = dimdata && dimdata[<any>v];
                        if (!code) {
                            result = false;
                        }
                    })
                    break;
                // case CheckRuleType.DimTextMatch:
                //     let dimText = rowData[rule.dimTextMatchField] ? rowData[rule.dimTextMatchField].toString() : null;
                //     let dimInfo = fieldCheckRules[0].dim;
                //     if (dimText && dimInfo && dimInfo.data[fieldValue] !== dimText) {
                //         result = false;
                //     }
                //     break;
                // case CheckRuleType.InEnum:
                //     result = false;
                //     for (let i = 0; i < rule.enum.length; i++) {
                //         if (rule.enum[i] === fieldValue) {
                //             result = true;
                //             break;
                //         }
                //     }
                //     break;
                // case CheckRuleType.Relate:
                //     result = checkRelate(rule.relate, fieldValue, fieldName);
                //     break;
                // case CheckRuleType.NotAllowNull:
                //     if (fieldValue === undefined || fieldValue === null || fieldValue.toString().length === 0
                //         && rule.notAllowNull.values.indexOf(rowData[rule.notAllowNull.field].toString()) !== -1) {
                //         result = false;
                //     }
                //     break;
                /**
                 * 2022-09-06 luofuw
                 * 帖子地址: https://jira.succez.com/browse/CSTM-20213
                 * 场景: 一些特殊的校验规则规则, 当前脚本不能很好的去实现
                 * 功能: 增加'coustom'类型, 当rule中type为'coustom'时
                 * 执行执行rule.method中的自定义方法，完成这些特殊的条件校验
                 */
                case CheckRuleType.Coustom:
                    let method = rule.method;
                    if (!method(fieldName, fieldValue, rowData)) {
                        result = false;
                    }
                    break;
                default:
                    break;
            }
        }
        if (!result) {
            errorInfos.push({ message: rule.desc, field: fieldName, data: fieldValue });
        }
    });
}

/**
 * 提交校验合法的数据
 * @params path：模型表路径
 * @params data：修改的数据（JSON、update时用到）
 * @params operation：操作（eg：insert）
 * @params fieldName：校验的字段数据
 * @params rows：insert用到，写入多行记录（要插入的数据）,是一个二维数据，
 *               eg：rows[["1001", "张三"]]，而不是：rows[{"id": "1001", "name": "张三"}]
 * 
 * @return {
 *      result: true  // 仅result，无成功插入的数量
 * }
 */
export function sumbit(path: string, data: Record<string, unknown>[], operation: string, fieldNames: string[], rows: any[]): CommonResult {
    console.debug(`开始执行[sumbit], 提交校验合法的数据`);
    if (operation === "insert") {
        let dataPackage: CommonTableSubmitDataPackage = {
            addRows: [{
                fieldNames: fieldNames,
                rows: rows
            }]
        }
        // console.debug("--提交校验合法的数据，开始修改模型表--");
        dw.updateDwTableData(path, dataPackage);
    }
    if (operation === "updata") {
        for (let i = 0; i < rows.length; i++) {
            const tempObj = new Object(null);
            tempObj["keys"] = [];
            keyNames[path].forEach(name => {
                tempObj["keys"].push(data[i][name]);
            })
            tempObj["row"] = rows[i];
            rows[i] = tempObj;
        }
        let dataPackage: CommonTableSubmitDataPackage = {
            modifyRows: [{
                keyNames: keyNames[path],
                fieldNames: fieldNames,
                rows: rows
            }]
        }
        dw.updateDwTableData(path, dataPackage);
    }
    if (operation === "merge") {
        let dataPackage: CommonTableSubmitDataPackage = {
            mergeIntoRows: [{
                fieldNames: fieldNames,
                rows: rows
            }]
        }
        dw.updateDwTableData(path, dataPackage);
    }
    if (operation === "delete") {
        let dataPackage: CommonTableSubmitDataPackage = {
            deleteRows: {
                keyNames: fieldNames,
                keyRows: rows
            }
        }
        dw.updateDwTableData(path, dataPackage);
    }
    console.debug(`执行结束[sumbit], 提交校验合法的数据`);
    return { result: true };
}

/**
 * 获取维项数据，数据缓存在内存里，仅当次调用有效复用，后续使用query的文件缓存
 * 若所有维项加起来数据量不大，可考虑使用ServletContext进行全局缓存，参考https://wiki.succez.com/pages/viewpage.action?pageId=252182590
 */
function getDimData(dim: DimInfo) {
    if (!dim) return null;
    let data = dim.data;
    if (data) {
        return data;
    }
    data = {};
    let fields: string[] = dim.fields;
    let queryFieldInfo: QueryFieldInfo[] = [];
    fields.forEach(f => {
        if (!f) {
            return;
        }
        queryFieldInfo.push({
            name: f,
            exp: 't0.' + f
        });
    })
    let queryResult = dw.queryData({
        sources: [{
            id: 't0',
            path: dim.path
        }],
        fields: queryFieldInfo
    });
    let result = queryResult.data;
    if (result && result.length > 0) {
        for (let i = 0; i < result.length; i++) {
            let r = result[i];
            let code = <string>r[0];
            let caption = r.length === 2 && <string>r[1] ? <string>r[1] : uuid();
            data[caption] = code;
            data[code] = caption;
        }
    }
    dim.data = data;
    return data;
}

/**
 * 检查是否通过数据关联限制
 */
function checkRelate(relateInfo: RelateInfo, v: string, fieldName: string) {
    const queryResult = dw.queryData({
        sources: [{
            id: 't0',
            path: relateInfo.tablePath
        }],
        fields: [{
            name: relateInfo.fieldName,
            exp: 't0.' + relateInfo.fieldName
        }],
        filter: [{
            exp: `t0.${fieldName} = '${v}'`
        }]
    });
    const result = queryResult.data;
    if (result && result.length > 0) {
        if (relateInfo.values.indexOf(result[0][0].toString()) === -1) {
            return false;
        } else {
            return true;
        }
    } else {
        return false;
    }
}

export function getLimit(apiId: string, dxcnl: DXChannelInfo) {
    const dxResourceId = dxcnl.getResource().getId();
    const queryResult = dw.queryData({
        sources: [{
            id: 't0',
            path: '/sysdata/data/tables/dx/DX_RESOURCES_APIS.tbl'
        }],
        fields: [{
            name: 'limit',
            exp: 't0.API_RESULT_LIMIT'
        }],
        filter: [{
            exp: `t0.API_ID = '${apiId}'`
        }, {
            exp: `t0.DX_RES_ID = '${dxResourceId}'`
        }]
    })
    const result = queryResult.data;
    return result[0][0];
}

/**
 * 生成默认校验规则
 * @params properties：数据库本身属性集
 * @params dimensions：数据库字段属性
 * @params rules：存储规则的数组（返回数据）
 * @params keys：主键数组（返回数据）
 * @params tablePath：表路径
 */
function defaultRuleGenerator(properties: DwTablePropertiesInfo, dimensions: DwTableFieldItemInfo[], rules: any, keys: string[], tablePath: string) {
    dimensions.forEach(dimension => {
        let dbFieldName: string = dimension["dbfield"];
        let dataType: string = dimension["dataType"];
        let fieldLen: number = dimension["length"];
        /**
         * dimension:{
         *      "name": "组织ID",
                "dataType": "C",
                "length": 200,
                "isDimension": true,
                "dbfield": "OganizationId"
            }
            console.debug(dimension.getClass()); // class java.util.LinkedHashMap
            JDK 8，fieldLen 返回的是size值
         */
        rules[dbFieldName] = [];
        /**
         * 为每个字段加入类型校验规则
         * FieldDataType.C 
         */
        if (dataType === "C") {
            const typeRule: CheckRule = {
                ruleId: uuid(),
                desc: `[${dbFieldName}]必须为字符型`,
                type: CheckRuleType.DataType,
                dataType: 'string'
            }
            const lengthRule: CheckRule = {
                ruleId: uuid(),
                desc: `[${dbFieldName}]长度要小于${dimension["length"]}`,
                type: CheckRuleType.DataLength,
                dataLength: dimension["length"]
            }
            rules[dbFieldName].push(typeRule);
            rules[dbFieldName].push(lengthRule);
        }
        if (dataType === "D") {
            const typeRule: CheckRule = {
                ruleId: uuid(),
                desc: `[${dbFieldName}]必须为字符型`,
                type: CheckRuleType.DataType,
                dataType: 'string'
            }
            const formatRule: CheckRule = {
                ruleId: uuid(),
                desc: `[${dbFieldName}]格式必须为yyyy-MM-dd`,
                type: CheckRuleType.DisplayFormat,
                displayFormat: 'yyyy-MM-dd'
            }
            rules[dbFieldName].push(typeRule);
            rules[dbFieldName].push(formatRule);
        }
        if (dataType === "P") {
            const typeRule: CheckRule = {
                ruleId: uuid(),
                desc: `[${dbFieldName}]必须为字符型`,
                type: CheckRuleType.DataType,
                dataType: 'string'
            }
            const formatRule: CheckRule = {
                ruleId: uuid(),
                desc: `[${dbFieldName}]格式必须为yyyy-MM-dd hh:mm:ss`,
                type: CheckRuleType.DisplayFormat,
                displayFormat: 'yyyy-MM-dd hh:mm:ss'
            }
            rules[dbFieldName].push(typeRule);
            rules[dbFieldName].push(formatRule);
        }
        /**
         * 2022-09-05 luofuw
         * 帖子地址: https://jira.succez.com/browse/CSTM-20213
         * 场景: 当前脚本的defaultRuleGenerator方法缺少为整型和浮点型校验规则的生成
         * 功能: 增加整型和浮点型校验规则的生成
         */
        if (dataType === "N") {
            const typeRule: CheckRule = {
                ruleId: uuid(),
                desc: `[${dbFieldName}]必须为数值型`,
                type: CheckRuleType.DataType,
                dataType: 'number'
            }
            const lengthRule: CheckRule = {
                ruleId: uuid(),
                desc: `[${dbFieldName}]长度要小于${fieldLen}`,
                type: CheckRuleType.DataLength,
                dataLength: fieldLen
            }
            const decimalRule: CheckRule = {
                ruleId: uuid(),
                desc: `[${dbFieldName}]的小数位数不超过${dimension["decimal"]}位`,
                type: CheckRuleType.DataDecimal,
                dataDecimal: dimension["decimal"]
            }
            rules[dbFieldName].push(typeRule);
            rules[dbFieldName].push(lengthRule);
            rules[dbFieldName].push(decimalRule);
        }
        if (dataType === "I") {
            const typeRule: CheckRule = {
                ruleId: uuid(),
                desc: `[${dbFieldName}]必须为数值型`,
                type: CheckRuleType.DataType,
                dataType: 'number'
            }
            const lengthRule: CheckRule = {
                ruleId: uuid(),
                desc: `[${dbFieldName}]位数要小于${fieldLen}`,
                type: CheckRuleType.DataLength,
                dataLength: fieldLen
            }
            rules[dbFieldName].push(typeRule);
            rules[dbFieldName].push(lengthRule);
        }
        for (let field in properties.primaryKeys) { // 加入主键非空规则
            if (dimension.name === properties.primaryKeys[field]) {
                if (!keyNames[tablePath]) {
                    keyNames[tablePath] = [];
                }
                keyNames[tablePath].push(dbFieldName);
                keys.push(dbFieldName);
                const allowNullRule: CheckRule = {
                    ruleId: uuid(),
                    desc: `${dbFieldName}不允许为空`,
                    type: CheckRuleType.AllowNull,
                    allowNull: false
                }
                rules[dbFieldName].push(allowNullRule);
                break;
            }
        }
    });
}

/**
 * 处理模型设置的规则信息
 */
function specialRuleGenerator(dimensions: DwTableFieldItemInfo[], auditRules: DwTableDataAuditRuleInfo[], rules: any, tablePath: string) {
    auditRules.forEach(auditRule => {
        if (auditRule.deleted == true || auditRule.enable == false) {
            return false;
        }
        let auditRuleType = auditRule.type;
        let ruleExp: string = auditRule.exp;
        let ruleField: string = auditRule.field;
        if (auditRuleType === 'indim') {
            for (let i = 0; i < dimensions.length; i++) { // 找出该字段关联维表路径
                if (dimensions[i]["dbfield"] === ruleField && dimensions[i]["dimensionPath"]) {  // 设置维表校验规则
                    const dimPath = getTrueDimPath(tablePath, dimensions[i]["dimensionPath"]);
                    const dimInfo = getJSON(dimPath);
                    const dimDimensions = dimInfo.dimensions;
                    /**
                    * 2022-09-05 luofuw
                    * 帖子地址: https://jira.succez.com/browse/CSTM-20213
                    * 场景: 使用dimInfo.properties.primaryKeys.length可能发生dimInfo.properties.primaryKeys不存在导致.length方法无法使用
                    * 功能: 防止因dimInfo.properties.primaryKeys取不到导致.length方法无法使用最终系统报错。取维表的主键时如果维表没有设置主键，默认取维表的第一个字段
                    */
                    const dimpProperties = dimInfo.properties;
                    let keyName;
                    if (dimpProperties.primaryKeys) {
                        keyName = dimpProperties.primaryKeys.length > 0 ? dimpProperties.primaryKeys[0] : dimDimensions[0].name;
                    }
                    let keyField: string;
                    let textField: string;
                    for (let j = 0; j < dimDimensions.length; j++) {
                        if (dimDimensions[j].name.toUpperCase() === keyName) {
                            keyField = dimDimensions[j].dbfield;
                            for (let k = 0; k < dimDimensions.length; k++) {
                                if (dimDimensions[j].textField && dimDimensions[k].name.toUpperCase() === dimDimensions[j].textField.toUpperCase()) {
                                    textField = dimDimensions[k].dbfield;
                                    break;
                                }
                            }
                            break;
                        }
                    }
                    if (keyNames[tablePath]) {
                        const dimRule: CheckRule = {
                            ruleId: uuid(),
                            desc: auditRule.desc,
                            type: CheckRuleType.Dim,
                            dim: {
                                path: dimPath,
                                fields: [keyField, textField]
                            }
                        }
                        rules[ruleField].unshift(dimRule);
                        break;
                    }
                }
            }
        }
        if (auditRuleType === 'unique') {
            const uniqueRule: CheckRule = {
                ruleId: uuid(),
                desc: auditRule.desc,
                type: CheckRuleType.Unique,
                uniqueRefFields: auditRule.refFields
            }
            rules[ruleField].push(uniqueRule);
        }
        if (auditRuleType === 'inenum') {
            const enumRule: CheckRule = {
                ruleId: uuid(),
                desc: auditRule.desc,
                type: CheckRuleType.InEnum,
                enum: auditRule.allowedValues
            }
            rules[ruleField].push(enumRule);
        }
        // if (auditRuleType === 'nonempty') {
        //     const allowNullRule: CheckRule = {
        //         ruleId: uuid(),
        //         desc: auditRule.desc,
        //         type: CheckRuleType.AllowNull,
        //         allowNull: false
        //     }
        //     rules[ruleField].push(allowNullRule);
        // }
        // if (auditRuleType === 'logic' && ruleExp.indexOf("dimText") != -1) {
        //     const dimTextMatchRule: CheckRule = {
        //         ruleId: uuid(),
        //         desc: auditRule.desc,
        //         type: CheckRuleType.DimTextMatch,
        //         dimTextMatchField: ruleExp.split('.')[1].slice(0, -1)
        //     }
        //     rules[ruleField].push(dimTextMatchRule);
        // }
        // if (auditRuleType === 'logic' && ruleExp.indexOf("relate") != -1) {
        //     for (let i = 0; i < dimensions.length; i++) {
        //         if (dimensions[i]["dbfield"] === ruleField && dimensions[i]["dimensionPath"]) {
        //             const relateRule: CheckRule = {
        //                 ruleId: uuid(),
        //                 desc: auditRule.desc,
        //                 type: CheckRuleType.Relate,
        //                 relate: {
        //                     tablePath: getTrueDimPath(tablePath, dimensions[i]["dimensionPath"]),
        //                     fieldName: ruleExp.split('.')[1].split('=')[0],
        //                     values: ruleExp.split('.')[1].split('=')[1].replace('\"', '').replace("'", '').split(',')
        //                 }
        //             }
        //             rules[ruleField].push(relateRule);
        //         }
        //     }
        // }
        // if (auditRuleType === 'logic' && ruleExp.indexOf("notAllowNull") != -1) {
        //     const notAllowNullRule: CheckRule = {
        //         ruleId: uuid(),
        //         desc: auditRule.desc,
        //         type: CheckRuleType.NotAllowNull,
        //         notAllowNull: {
        //             field: ruleExp.split('.')[1].split('=')[0],
        //             values: ruleExp.split('.')[1].split('=')[1].replace('\"', '').replace("'", '').split(',')
        //         }
        //     }
        //     rules[ruleField].push(notAllowNullRule);
        // }
    });
}

/**
 * 根据目录获取真实的维表路径
 */
function getTrueDimPath(tablePath: string, dimPath: string): string {
    const projectName = getFile(tablePath).projectName;
    const parentDir = getFile(tablePath).parentDir;
    let trueDimPath = '';
    if (dimPath.startsWith('$DATA:')) {
        trueDimPath = dimPath.replace('$DATA:', `/${projectName}/data/tables`);
    } else if (dimPath.startsWith('../')) {
        const index = parentDir.lastIndexOf('/');
        trueDimPath = parentDir.substr(0, index) + dimPath.replace('..', '');
    } else {
        trueDimPath = parentDir + "/" + dimPath;
    }
    return trueDimPath;
}

/**
 * 根据数据表的路径读取表结构生成提交校验规则
 * @params tablePath：数据表路径
 */
export function ruleGenerator(tablePath: string) {
    /** 返回JSON格式的：文件信息（模型表属性、各字段属性） */
    let fileJSON = getJSON(tablePath);
    /** 
     * 文件的属性，eg： 
     * properties = {
     *      modelDataType=App, extractDataEnabled=false, ... , primaryKeys=[申请ID]
     * }
     */
    let properties = fileJSON.properties;
    /**
     * 模型表字段属性，eg：
     * dimensions=[
     *      { name=申请ID, dataType=C, length=50, isDimension=true, dbfield=SQID, isAutoInc=false}, 
     *      { name=患者姓名, dataType=C, length=50, isDimension=true, dbfield=HZXM },
     *      ...
     * ]
     */
    let dimensions = fileJSON.dimensions;
    /**
     * 审计规则（可以为null）
     */
    let auditRules = fileJSON.auditRules ? fileJSON.auditRules : [];
    let rules = new Object();
    /** 主键数组 */
    let keys = [];
    // 构建字段默认规则
    defaultRuleGenerator(properties, dimensions, rules, keys, tablePath);
    /** 
     * 主键唯一性规则添加
     * 注意: 
     *  1 模型上不设置主键唯一性校验规则, 否则主键唯一性规则会存在多个, 导致数据自己已自己本身冲突;
     *  2 此处先判断rules是否已经记录过主键唯一性, 若已记录则忽略;
     *  3 其他字段唯一性规则不会存在此问题。
     */
    if (keys.length > 0) {
        let firstKey: string = keys[0];
        let pkRules: CheckRule[] = rules[firstKey];
        if (!pkRules) {
            pkRules = [];
        }
        let isExistUniqueRule: boolean = false;
        for (let i = 0; i < pkRules.length; i++) {
            let ruleInfo = pkRules[i];
            if (ruleInfo.type == CheckRuleType.Unique) {
                isExistUniqueRule = true;
                break;
            }
        }
        if (!isExistUniqueRule) { // 规则校验表内和批次内主键重复, 可参考: ${singleFieldCustomRuleLegalVerfiy}
            rules[firstKey].push({
                ruleId: uuid(),
                desc: `${keys.toString()}本批次内数据主键重复`,
                type: CheckRuleType.Unique,
                uniqueRefFields: keys
            });
        }
    }
    specialRuleGenerator(dimensions, auditRules, rules, tablePath);
    return rules;
}

/**
 * 判断字符是否符合日期格式
 * 格式：yyyy-MM-ddTHH:mm:ss
 */
function checkDateFormat(times: string): boolean {
    if (times == null || times == "") {
        return true;
    }
    let checkPatten = new RegExp(/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}$/);
    if (checkPatten.test(times)) {
        return true;
    } else {
        return false;
    }
}

/**
 * 一条校验规则，注意allowNull、dataLength、dataType、displayFormat、dim一条只允许设置一个，对应描述和类型，有多种请设置多条
 * 一个字段多条规则均错误则返回显示第一条规则
 */
interface CheckRule {
    /** 规则id */
    ruleId: string;
    /** 规则描述，返回显示的信息 */
    desc: string;
    /** 规则类型 */
    type: CheckRuleType;
    /** 是否允许为空 */
    allowNull?: boolean;
    /** 数据长度 */
    dataLength?: number;
    /** 数据类型，这里对应的是提交数据的类型，并非真正入库类型 */
    dataType?: 'number' | 'string' | 'boolean';
    /** 显示格式，目前仅支持日期格式，如："yyyy-MM-dd" */
    displayFormat?: string;
    /** 唯一性字段组合数组 */
    uniqueRefFields?: string[];
    /** 关联维项 */
    dim?: DimInfo;
    /** 维表匹配字段 */
    dimTextMatchField?: string;
    /** 枚举值数组*/
    enum?: string[];
    /** 关联限制字段信息 */
    relate?: RelateInfo;
    /** 不为空字段关联字段信息 */
    notAllowNull?: NotAllowNullInfo;
    /** 小数位数 */
    dataDecimal?: number;
    /** 自定义检验方法 */
    method?: (fieldName: string, fieldValue: any, rowData: Record<string, any>) => boolean;
}

/** 检测规则类型 */
enum CheckRuleType {
    /** 允许为空 */
    AllowNull = "allowNull",
    /** 数据长度 */
    DataLength = "dataLength",
    /** 数据类型 */
    DataType = "dataType",
    /** 显示格式 */
    DisplayFormat = "displayFormat",
    /** 唯一性验证 */
    Unique = "unique",
    /** 关联维表 */
    Dim = "dim",
    /** 维表匹配， 如果目标字段没有配置关联维表检测泽忽略*/
    DimTextMatch = "dimTextMatch",
    /** 枚举匹配 */
    InEnum = "inEnum",
    /** 关系校验 */
    Relate = "relate",
    /** 关联不允许为空*/
    NotAllowNull = "notAllowNull",
    /** 小数位数 */
    DataDecimal = "dataDecimal",
    /** 自定义检测规则 */
    Coustom = "custom",
    /** 自定义不为空检测规则 */
    CoustomNotNull = "customNotNull"
}

/**
 * 维表信息
 */
interface DimInfo {
    /** 维表路径 */
    path: string;
    /** 维表字段，第一个为主键、第二个为文字字段 */
    fields: string[];
    /** 维表数据，主要缓存作用，定义时无须传 */
    data?: { [key: string]: number | string | boolean };
}

/**
 * 关联表信息
 */
interface RelateInfo {
    /** 关联表路径 */
    tablePath: string;
    /** 关联限制的字段 */
    fieldName: string;
    /** 限制的值 */
    values: string[];
}

interface NotAllowNullInfo {
    /** 字段名 */
    field: string;
    /** 字段值 */
    values: string[];
}


/**
 * 一行数据的校验结果
 */
interface CheckDataResult {
    /** 行号 */
    rowNumber: number | number[];
    /** 错误结果 */
    errorResult: {
        /** 错误说明 */
        message: string;
        /** 错误字段 */
        field: string;
        /** 错误数据 */
        data: number | string | boolean;
    }[];
}

interface CommonResult {
    /** 返回结果 */
    result: boolean,
    /** 错误码 */
    errorCode?: string,
    /** 错误信息 */
    errorMessage?: string,
    /** 检测错误信息 */
    checkResult?: any[],
    /** 部分错误数据 */
    partErrorData?: JSONObject[];
    /** 部分合法数据 */
    partLegalDatas?: JSONObject[];
    /** 行数据数组 */
    rows?: any[],
    /** 记录成功插入的数据下标 */
    successIndexs?: string[];
}

/** 单字段错误信息 */
interface SingleFieldErrorInfo {
    /** 字段名 */
    field: string;
    /** 错误消息 */
    message: string;
    /** 错误值 */
    data: FieldValue;
}

/** 
 * 唯一性校验字段值记录类型 
 * 示例: 
 * 1 单唯一性字段
 *   "model1.ID": {
 *      dataRowIndex: [1, 2]
 *      uniqueValues: ["4864A5FA-6741-4914-B638-10C3B0FA9047", "41864A5FA-6741-4914-B638-10C3B0FA9047"]
 *   }
 * 2 多唯一性字段
 *   "model1.XH+','+model1.XM": {
 *      dataRowIndex: [2, 4],
 *      uniqueValues: ["4\\,李四", "3\\,小红"]
 *   }
 */
interface UniqueFieldDataType {
    /** 唯一性字段, eg: model1.ID、model1.ID+','+model1.XH */
    [fieldName: string]: {
        /** 唯一性字段值在上传数据所在行数, 与${uniqueValues}值一一对应, eg: [1] */
        dataRowIndex: number[],
        /** 唯一性字段拼接值, eg: ["001"] 、["001\\,张三"] */
        uniqueValues: string[]
    }
}