# in / not in（包含）

> 网址：https://blog.csdn.net/weixin_40725027/article/details/83011268

## 1 正常情况

```mysql
select * from 表名 where id in ('1','2','3');
```

## 2 数组作为in的参数

```ts
const QUERY_SQL = `select * from 表 where id in (?)`;
let ids = ["1", "2", "3"];
let table = db.opentTableData("表名");
let queryData = 
	// 直接将其数组作为参数传入
	table.executeQuery(QUERY_SQL, ids);
```

​	数据库执行的`SQL`为：

```sql
select * from 表名 where id in ('1, 2, 3');
```

​	尝试方法：《替换》 或者 《自己拼接`SQL`》

```ts
// 将“," 替换成： " ',' "
id=id.replace(",", "','")；
```

```mysql
# 无法执行
select * from 表名 where id in ('1\','2\','3\');
```

## 3 ` FIND_IN_SET`

```mysql
# FIND_IN_SET，第二个参数为：集合
select *from temp where FIND_IN_SET（id，'1, 2, 3');
```

​	案例：使用`join`方法，将数组转换为《指定间隔符》的：字符串

```ts
const QUERY_SQL = `select * from 表 where FIND_IN_SET(id, ?)`;
let ids = ["1", "2", "3"];
let table = db.opentTableData("表名");
let queryData = 
	// 直接将其数组作为参数传入
	table.executeQuery(QUERY_SQL, [ids.join(",")]);
```

​	**注意：**前提必须是：['1, 2, 3'] ，如果传入的是字符串数组，则必须对其处理

## 4 in 可作用于多个字段

​	《不能同时与多个字段》联合使用，只能是《针对一个字段》可能出现《多种值的情况》。 要想用多个字段，可以使用：《and 》连接

```mysql
# 注意MySQL版本，部分不支持
select * from 表 where (name, sex) in (('张三', 18),('李四', 19));
```

​	<button>注意：</button>` (HZDJH, HZXM)  ` 是一个对象

```mysql
select HZXM, HZZJHM 
from fact_hzsqb 
where 
	(HZDJH, HZXM) in(("6998","dd544fGi7NkblBaUKVdjyA=="));
```

​	案例：

```ts
/**
 * 测试：利用 in  替代 多个 and
 * @params conditionArray：比较的条件数组集
 */
function getPatientRegisterIds(conditionArray: Array<Array<string>>): void {
    if (!conditionArray) {
        return;
    }
    /**
     * 处理：查询条件
     * let ids = [
            ["5219", "张三"],
            ["9376", "黄敏"]
            ...
       ];
     * 将其二维数组拼接为：(?, ?),(?, ?)...
     */
    let sqlMarkCondition = "";
    let conditionArrayLen = conditionArray.length;
    if (conditionArrayLen == 1) {
        sqlMarkCondition = `(?,?)`;
    } else {
        for (let i = 0; i < conditionArrayLen; i++) {
            if (i == conditionArrayLen - 1) {
                sqlMarkCondition = sqlMarkCondition + `(?,?)`;
                break;
            }
            sqlMarkCondition = sqlMarkCondition + `(?,?),`;
        }
    }
    /**
     * select distinct HZDJH from fact_hzsqb where (HZDJH, HZXM) IN((?,?),(?,?))
     */
    let dynamicSQL = 
        `select distinct HZDJH from fact_hzsqb where (HZDJH, HZXM) IN(${sqlMarkCondition})`;
    /**
     * ["7109", "张三", "7115", "黄敏"]
     */
    let queryCondtion = flatten(conditionArray);
    let patientTable = ds.openTableData(PATIENT_INFORMATION_TABLE);
    let patientInformationArray = 
        patientTable.executeQuery(dynamicSQL, queryCondtion);
    console.debug(patientInformationArray);
}

/**
 * 二维数组转一维数组
 * 利用: ES6  优点: 多维数组也可以
 */
function flatten(arr1) {
    return [].concat(...arr1.map(x => Array.isArray(x) ? flatten(x) : x))
}
```

## 5 《数组》作为in的参数出现的问题

```mysql
select * from 表名 where id in ('?');
```

​	上述`SQL`只有一个参数，若参数是：<button>数组</button>，则会报：参数大于1（若数组长度==1，则不会报错）

**解决办法：**

​	根据数组的元素，将其元素<button>替换为：`?,`</button>

```ts
// 将内部元素替换为：?,
const conditions = '?,'.repeat(arr.length).substring(0, arr.length * 2 - 1); 
```

​	例如：["100", "101", "102"]  ——> "?, ?, ?"

```ts
let arr = ["100", "101", "102"];
let conditions = '?,'.repeat(arr.length).substring(0, arr.length * 2 - 1); 
print(conditions); // ?,?,?
```

```ts
//  select distinct HZDJH, YYID from dim_hz where HZDJH IN(?,?,?,?)
 const conditions  = '?,'.repeat(arr).substring(0, 2 * arr.length*2 - 1)
 let sql = 
    `select distinct name from dim_hz where id IN(${conditions})`;
```

## 6 `POSTMAN`传入数据存在问题

```ts
/**
 * 通过其他方式传入的data，如果直接用于SQL，则会：
 * select distinct HZDJH from dim_hz where HZDJH IN([3622, 0187]) and YYID=?
 * 正确的SQL：
 * select distinct HZDJH from dim_hz where HZDJH IN(3622, 0187) and YYID=?
 *  这里需要转换
 */
let checkData: Array<string> = [];
data.forEach(item => {
    checkData.push(item);
});
/**
 * 筛选data数组中患者登记号已存在库中的数据
 */
let patientInformationArray = getPatientRegisterIds(checkData, jgid);
```

# exists



