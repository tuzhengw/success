# t2* 校验是否为-`JSON`对象

---

## 1 `JSON.parse(str)`

​	下述写法，<button>不能完全校验</button>是否为：`JSON`对象

```ts
function isJSON(str) {
    if (typeof str == 'string') {
        try {
            JSON.parse(str);
            return true;
        } catch(e) {
            console.log(e);
            return false;
        }
    }
    console.log('It is not a string!')    
}
```

​	只是《单纯》的用`JSON.parse(str)`不能完全检验一个字符串是`JSON`格式的字符串，例如：

```ts
JSON.parse('123'); 		// 123
JSON.parse('{}'); 		// {}
JSON.parse('true'); 	// true
JSON.parse('"foo"'); 	// "foo"
JSON.parse('[1, 5, "false"]'); // [1, 5, "false"]
JSON.parse('null'); 	// null
```

​	`JS`中的数据类型分为：字符串、数字、布尔、数组、对象、Null、Undefined

---

## 2 `写法1` - 已废弃

​	如果`JSON.parse`能够转换成功；并且字符串中《包含》 <button>{ </button>时，那么这个字符串就是`JSON`格式的字符串

```ts
// v1.0 不严谨，已废弃。
function isJSON(str): boolean {
    if (typeof str == 'string') {
        try {
            var obj=JSON.parse(str);
            // 
            if(str.indexOf('{')>-1){
                return true;
            }else{
                return false;
            }

        } catch(e) {
            console.log(e);
            return false;
        }
    }
    return false;
}
```

​	上述代码存在两个漏洞：

​		（1）`JSON`不仅为：`object`，还有可能为：`array`

​		（2）判定字符串中是否含有：“{”，不准确，例如："`te{st`"，`indexOf`也是可以匹配到，而且：`parse`是可以通过的

```ts
JSON.parse('"A{BC"'); // 转换成功，但其实不是：JSON
"A{BC"
```

```ts
isJSON('"A{BC"'): // true，正确结果应该是：false
```

## 3 `写法2` - 仅字符串

> ​	**注意：**  `JSON`数据格式，主要由《对象： { } 》 和《数组： [ ] 》组成。
>
> ​    若传入的：<button>`str`为对象</button>，则校验失败

```ts
function isJSON(str): boolean {
    if (typeof str == 'string') {
        try {
            var obj=JSON.parse(str);
            // object 且不等于 null
            if(typeof obj == 'object' && obj ){
                return true;
            }else{
                return false;
            }

        } catch(e) {
            console.log('error：'+str+'!!!'+e);
            return false;
        }
    }
    console.log('It is not a string!')
}
```

​	测试：

```ts
isJSON_test('123'); 			// 转换成功：123
isJSON_test('aaaa'); 			// 转换失败：SyntaxError
isJSON_test('"aaa"');			// 转换成功：aaa
isJSON_test('true'); 			// 转换成功：true
isJSON_test('["1","2"]'); 		// 转换成功：1,2
isJSON_test('{name:"123"}'); 	// 转换失败：SyntaxError
isJSON_test('{}'); 				// 转换成功：object
isJSON_test('null'); 			// 转换成功：null
isJSON_test('Undefined'); 		// 转换失败：SyntaxError
isJSON_test('{"name":"123"}');  // 转换成功：object
isJSON_test('{"name":"123"，}'); //  转换失败：SyntaxError
```

以上测试结果，能够<button>转换成功的有</button>：数字、字符串、布尔、数组、空对象、null、`json`

其中<button>正确的`JSON`格式</button>有：  数组、空对象、`json`

 所以得出以下结论：

 		如果`JSON.parse`能够转换成功；并且转换后的类型为《object 且不等于 null》，那么这个字符串就是`JSON`格式的字符串

---

## 4 写法3 - 仅对象

```ts
/**
 * 判断是否为json对象
 */
function isJSON(obj): boolean {
    let isjson = typeof (obj) == "object" &&
        Object.prototype.toString.call(obj).toLowerCase() == "[object object]" && 
        !obj.length;
    return isjson;
}
```

```ts
let data = [
    {
        a: 1
    },
    2
]
data.forEach(item => {
    print(isJSON(item));// true / false
})
```