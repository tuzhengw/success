import "css!./data-process.css";
import { DataFlowArgs, ScheduleMgrPage, TaskDataFlow, TaskDataFlowDataProvider, TaskDataProvider, TaskFlowNodeInfo } from "schedule/schedulemgr";
import { InterActionEvent } from "metadata/metadata-script-api";
import { MetaModulePage, MetaModulePageArgs, ParentDirDataProvider } from "metadata/metamgr";
import { Grid, GridCell, ListItem, GridRow, List } from "commons/tree";
import {
	SZEvent, showMenu, formatDateFriendly, Component, Cancelable, CommandItemInfo, throwError, isEmpty, showErrorMessage, rc, timerv, showConfirmDialog,
	uuid, showProgressDialog, showWaiting, throwInfo, showWarningMessage
} from "sys/sys";
import { getProjectName } from "metadata/metadata";
import { FlowDiagram, FlowNode, FlowNodeInfo } from "commons/flowDiagram";
import { Toolbar, ToolbarItemAlign, ToolbarItemType } from "commons/basic";
import { checkCstmRule, CheckCstmRuleArgs, runCheckTask } from "dg/dgapi";
import { getScheduleMgr, ScheduleMgr, ScheduleLogInfo, ScheduleLogsPage, TaskLogInfo } from "schedule/schedulemgr";
import { showScheduleDialog } from "../commons/commons.js";

/**
 * 20220610 tuzw
 * 计划任务前端脚本
 */
export class SDI_SchduleMgr {

	/** 计划任务所在目录ID */
	public scheduleCatId: string;

	public CustomActions: any;
	constructor() {
		this.CustomActions = {
			button_schedule_executeTask: this.button_schedule_executeTask.bind(this),
			button_schedule_stopTask: this.button_schedule_stopTask.bind(this),
			button_schedule_createScheduleTask: this.button_schedule_createScheduleTask.bind(this)
		};
	}

	/**
	 * 执行计划任务
	 * @param scheduleIds 执行的计划任务集
	 */
	public button_schedule_executeTask(event: InterActionEvent) {
		let params = event?.params;
		let scheduleIds: string[] = this.stringToArr(params.scheduleIds);
		if (scheduleIds.length == 0) {
			showWarningMessage("未获取到需要执行的计划序号");
			return;
		}
		return scheduleIds && showConfirmDialog({
			message: "确认要执行选择的计划任务吗？",
			onok: (event, dialog) => {
				let promise = getScheduleMgr().runSchedules(scheduleIds, 'sys');
				showProgressDialog({
					caption: "计划执行日志",
					uuid: uuid(),
					promise: promise,
					buttons: [{
						id: "cancel",
						caption: "关闭"
					}],
					logsVisible: true,
					width: 700,
					height: 500
				});
				return showWaiting(promise).then(() => {
					throwInfo("执行完成");
				});
			}
		});
	}

	/**
	 * 中止计划任务
	 * @param scheduleIds 执行的计划任务集
	 */
	public button_schedule_stopTask(event: InterActionEvent) {
		let params = event?.params;
		let scheduleIds: string[] = this.stringToArr(params.scheduleIds);
		if (scheduleIds.length == 0) {
			showWarningMessage("未获取到需要执行的计划序号");
			return;
		}
		return scheduleIds && showConfirmDialog({
			message: "确认要中止选择的计划任务吗？",
			onok: (event, dialog) => {
				let promise = getScheduleMgr().stopSchedules(scheduleIds, 'sys');
				showProgressDialog({
					caption: "计划中止日志",
					uuid: uuid(),
					promise: promise,
					buttons: [{
						id: "cancel",
						caption: "关闭"
					}],
					logsVisible: true,
					width: 700,
					height: 500
				});
				return showWaiting(promise).then(() => {
					throwInfo("中止完成");
				});
			}
		})
	}

	/**
	 * 新建一个检测任务类型的计划任务
	 * 由于页面需要展示计划，新增后，需要刷新展示的计划模型数据
	 * @param refreshModelPath 刷新的模型路径
	 * @param scheduleCatId 当前新增计划所属目录ID
	 */
	public button_schedule_createScheduleTask(event: InterActionEvent): void {
		let params = event?.params;
		let scheduleCatId: string = params.scheduleCatId;
		if (isEmpty(scheduleCatId)) {
			showWarningMessage("请选择新增计划所属目录");
			return;
		}
		this.scheduleCatId = scheduleCatId;
		let refreshModelPath: string = params.refreshModelPath;
		let scheduleInfo: ScheduleInfo = { enable: true, concurrency: false, notifyWhen: "never", retryEnable: true, retryMaxtimes: 1, retryInterval: 1, threadsCount: 2, type: "check" };

		showScheduleDialog(scheduleInfo, {
			refreshModelPath: refreshModelPath,
			afterClickMethod: this.updateScheduleCatId.bind(this),
			isDisplayScheduleType: true
		});
	}

	/**
	 * 将字符串转换为字符串数组 
	 * @param str 转换的字符串
	 */
	private stringToArr(str: string): string[] {
		if (isEmpty(str)) {
			return [];
		}
		if (typeof str != "string") {
			return str;
		}
		return str.split(",");
	}

	/**
	 * 新增检测计划后，将其计划添加到指定的目录下
	 * @param scheduleInfo 计划信息对象
	 */
	private updateScheduleCatId(scheduleInfo: ScheduleInfo): Promise<any> {
		if (isEmpty(scheduleInfo) || isEmpty(scheduleInfo.id)) {
			showWarningMessage("计划任务ID参数为空");
			return Promise.resolve(false);
		}
		return rc({
			url: "/zt/dataScheduleMgr/updateScheduleCat",
			method: "POST",
			data: {
				scheduleId: scheduleInfo.id,
				scheduleCatId: this.scheduleCatId
			}
		}).then((result) => {
			if (!result.result) {
				showWarningMessage("设置计划所在目录失败");
				return Promise.resolve(false);;
			}
		});
	}

}