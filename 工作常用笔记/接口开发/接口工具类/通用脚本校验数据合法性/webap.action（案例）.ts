/**
 * ============================================================
 * 作者: tuzhengw
 * 审核人员：liuyongz
 * 创建日期：2021-01-14
 * 功能描述：
 *      该脚本主要是用于南通快检系统数据API接口需求，获取部门表信息
 * ============================================================
 */
import { sumbit, submitData, ruleGenerator, getLimit } from "/sysdata/public/utils/utils.action";
import utils from "svr-api/utils";
import security from 'svr-api/security';
/** API接口 */
const Requests = [
    "insertFoodInfos"
]

/** 返回值 */
interface ResultInfo {
    /** 返回值 */
    result?: boolean;
    /** 编码 */
    code?: string;
    /** 错误编码 */
    errorCode?: string;
    /** 处理消息 */
    message?: string;
    /** 返回数据 */
    data?: any;
    /** 成功插入数据的主键集 */
    insertPks?: any;
    /** 成功插入的行数  */
    rowCount?: number;
    /** 上传数据数量 */
    dataLen?: number;
}


/**
 * 湖北智慧监管-食安康数据对接
 * 需求：数据全部合法则插入，不合法，全部打回，并附带错误详细原因
 * @param dxcnl 通道信息
 * @param apiId 一个模型可以有多个api，外界可以指定调用的api
 * @param params 外部传递的参数，由脚本和外部调用者自己约定。有几个约定的参数：
 * @param output 数据输出的流或临时文件的路径，如不传递，那么将通过函数返回值返回，具体格式有脚本实现者和调用者协商。
 * @return true
 */
function onDXService(args?: { dxcnl: DXChannelInfo, apiId: string, params: JSONObject, output?: OutputStream | string, logs?: any }): ResultInfo {
    let startTime = new Date().getTime();
    /** 参数自带的通道对象 */
    let dxObj: DXResourceScriptAPIResult = { rowCount: 0, data: [] };
    let resultInfo: ResultInfo = { result: true };
    let params = args.params;
    let logs = args.logs;
    let correspondenceObj = args.dxcnl;

    /** 业务类型 */
    let bizType: string = params.BIZ_TYPE;
    /** 请求方唯一编码 */
    let reqId: string = params.REQ_ID;
    /** 数据条数 */
    let dataCount: number = params.DATA_COUNT;
    /** JSON数组，记录插入的数据 */
    let datas: DATA_LIST[] = params && params.DATA_LIST;

    let apiId: string = args.apiId;
    console.debug(`onDXService()，湖北智慧监管-食安康数据对接，参数：【${params}】--start`);
    console.debug(`当前请求的接口名为：${apiId}`);
    if (Requests.indexOf(apiId) == -1) { // 检测接口名是否正确
        resultInfo.result = false;
        resultInfo.errorCode = 'appNotExist';
        resultInfo.message = `当前接口不存在！`;
        dxObj.data = resultInfo;
        return dxObj;
    }
    if (!datas || !datas.length) {
        resultInfo.result = false;
        resultInfo.errorCode = 'dataIsNull';
        resultInfo.message = `data不为NULL，并且必须是JSON数组`;
        dxObj.data = resultInfo;
        return dxObj;
    }
    let modelPath: string = correspondenceObj.getDwTable();
    let optionMaxLimit: number = getLimit(apiId, correspondenceObj);  // 插入行数限制，默认：100
    if (!optionMaxLimit) {
        optionMaxLimit = 100;
    }
    let fields = [];
    correspondenceObj.fields.forEach(field => { // 记录插入的字段列
        fields.push(field);
    });
    let checkRules = ruleGenerator(modelPath);
    for(let i = 0 ; i < datas.length ;i++) {
        if (!datas[i].JGBM2) { // 未给定，则默认：JGBM列值
            datas[i].JGBM2 = datas[i].JGBM;
        }
        datas[i].SOURCES = "10";
        datas[i].UpdateTime = new java.sql.Timestamp(new Date().getTime());
        datas[i].UpdatePeople = security.getCurrentUser().userInfo.userId;
    }
    print(`检测数据：\n ${datas}`);
    let dataDealResult = submitData(modelPath, datas, "insert", checkRules, fields, optionMaxLimit, false);
    print(`检测结果：\n ${JSON.stringify(dataDealResult)}`);
    if (dataDealResult['data']['result']) {
        resultInfo.data = dealCheckResult(datas, dataDealResult['data'], bizType, dataCount, reqId);
    } else {
        let errorInfo = dataDealResult['data']['checkResult'];
        resultInfo.data = dealCheckResult(datas, dataDealResult['data'], bizType, dataCount, reqId);
        console.debug(`【${modelPath}】数据全部不合法，插入失败，\n 【${JSON.stringify(errorInfo)}】--`);
    }
    dxObj.data = resultInfo;

    logs.put('result', JSON.stringify(resultInfo));
    logs.put('datasLen', datas.length); // 响应方记录实际上传数量
    
    print(`返回结果：\n ${JSON.stringify(resultInfo)}`);
    let endTime = new Date().getTime();
    console.debug(`onDXService()，湖北智慧监管-食安康数据对接，花费时间为：${(endTime - startTime) / 1000}秒--end`);
    return resultInfo;
}

/**
 * 处理检测结果，按期文档规定格式返回
 * @param datas 用户上传的数据信息
 * @param checkResult 检测的结果
 * @param bizType 请求发给定业务类型
 * @param dataCount  请求发给定数据数量
 * @param repId  请求发给定请求码
 */
function dealCheckResult(datas: DATA_LIST[], checkData, bizType: string, dataCount: number, repId: string): ReturnErrorFormat {
    let sdf = new java.text.SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    let returnData: ReturnErrorFormat = {
        BIZ_TYPE: bizType,
        RESP_TIME: sdf.format(new java.util.Date(Date.now())),
        RESP_ID: java.util.UUID.randomUUID().toString(),
        REQ_ID: repId,
        RESULT_CODE: "0000",
        RESULT_MESS: "流程执行成功",
        DATA_COUNT: dataCount
    };
    if(!datas) {
        return returnData;
    }
    /** 记录错误数据下标 */
    let errorDataIndexs: Array<number> = [];
    let errorRows = checkData["checkResult"];

    let checkDataList: DATA_LIST[] = [];
    if (errorRows != null && errorRows.length > 0) { // 处理失败的数据
        for (let i = 0; i < errorRows.length; i++) {
            /**
             * 主键重复会被划分为一组，其他则会单独分开
             * "rowNumber": [0.0, 1.0]
             * "rowNumber": 1.0
             */
            if (typeof errorRows[i]["rowNumber"] == "object") {
                print("处理主键重复问题");

                let primaryKeys = errorRows[i]["rowNumber"];
                let errorResult: ErrorResult[] = errorRows[i]["errorResult"];
                let errorCode: string = getErrorCode(errorResult[0].message);
                print(`错误信息：${errorResult[0].message} 对应的 code：${errorCode}`);

                for (let pk = 0; pk < primaryKeys.length; pk++) {
                    let errorIndex = errorRows[i]["rowNumber"][pk];
                    if (errorDataIndexs.indexOf(errorIndex) == -1) { // 避免同一下标重复添加
                        errorDataIndexs.push(errorIndex);
                        checkDataList.push({
                            ID: datas[errorIndex]["ID"],
                            RETUREN_CODE: errorCode,
                            RETUREN_MESS: DataDealType[errorCode]
                        });
                    }
                }
            } else {
                let errorIndex: number = errorRows[i]["rowNumber"];
                let errorResult: ErrorResult[] = errorRows[i]["errorResult"];
                let errorCode: string = getErrorCode(errorResult[0].message);
                print(`错误信息：${errorResult[0].message} 对应的 code：${errorCode}`);
                
                if (errorDataIndexs.indexOf(errorIndex) == -1) {
                    errorDataIndexs.push(errorIndex);
                }
                checkDataList.push({
                    ID: datas[errorIndex]["ID"],
                    RETUREN_CODE: errorCode,
                    RETUREN_MESS: DataDealType[errorCode]
                });
            }
        }
    }
    for(let i = 0; i < datas.length; i++) { // 处理成功的数据
        if(!errorDataIndexs.includes(i)) {
            checkDataList.push({
                ID: datas[i]["ID"],
                RETUREN_CODE: "0000",
                RETUREN_MESS: DataDealType["0000"]
            });
        }
    }
    returnData.DATA_LIST = checkDataList;
    console.debug(`处理后的返回结果：\n ${JSON.stringify(checkDataList)}`);
    return returnData;
}

/**
 * 根据检测的message获取对应的code
 * @param message 检测行消息，eg："OganizationId字段与表内主键重复"
 * @return 指定错误码
 */
function getErrorCode(message: string): string {
    print(`开始获取${message}对应的code`);
    if(!message) { 
        return "0105";
    }
    if (message.indexOf("主键重复") != -1) {
        return "0107";
    }
    if (message.indexOf("必须为字符型") != -1) {
        return "0103";
    }
    if (message.indexOf("长度要小于") != -1) {
        return "0104";
    }
    if (message.indexOf("格式必须为yyyy-MM-dd") != -1 || message.indexOf("格式必须为yyyy-MM-dd hh:mm:ss") != -1) {
        return "0105";
    }
    if (message.indexOf("不允许为空") != -1) {
        return "0100";
    }
    return "0108";
}

/** 数据处理结果类型 */
const DataDealType = {
    "0000": "成功",
    "0100": "非空校验错误",
    "0101": "正则表达式校验错误",
    "0102": "枚举值校验错误",
    "0103": "数据范围校验错误",
    "0104": "长度校验错误",
    "0105": "其他校验错误",
    "0106": "经验主体编码错误",
    "0107": "重复传输",
    "0108": "未知错误",
}

/** 对方接口错误返回格式 */
interface ReturnErrorFormat {
    /** 签名 */
    SIGN_VALUE?: string;
    /** 业务类型 */
    BIZ_TYPE?: string;
    /** 响应时间 */
    RESP_TIME?: string;
    /** 响应 ID */
    RESP_ID?: string;
    /** 请求 ID */
    REQ_ID?: string;
    /** 交易返回编码 */
    RESULT_CODE?: string;
    /** 交易返回消息 */
    RESULT_MESS?: string;
    /** 数据条数 */
    DATA_COUNT?: number;
    /** 响应数据参数 */
    DATA_LIST?: DATA_LIST[]
}

/** 检测数据列表 */
interface DATA_LIST {
    /** 数据主键 */
    ID ?: string;
    /** 检测中心码 */
    JGBM?: string;
    /** 快检中心第三 方平台编码 */
    JGBM2?: string;
    /** 快检中心名称 */
    JGMC?: string;
    /** 快检中心类型 */
    JGLX?: string;
    /** 被检单位 */
    BJDW?: string;
    /** 检测编号 */
    JCBH?: string;
    /** 检测时间 */
    JCSJ?: string;
    /** 样品产地 */
    YPCD?: string;
    /** 样品名称 */
    YPMC?: string;
    /** 检测项目  */
    JCXM?: string;
    /** 检测值 */
    JCZ?: string;
    /** 限值 */
    XZ?: string;
    /** 检测结果 */
    JCJG?: string;
    /** 检测人员 */
    JCRY?: string;
    /** 检测设备  */
    JCSB?: string;
    /** 检测批次 */
    JCPC?: string;
    /** 批注 */
    BZ?: string;
    /** 来源 */
    SOURCES?: string;
    /** 更新时间 */
    UpdateTime?: string;
    /** 更新人 */
    UpdatePeople?: string;
    /** 检测结果码 */
    RETUREN_CODE ?: string;
    /** 检测结果 */
    RETUREN_MESS ?: string;
    /** 自定义1  */
    ZDY1?: string;
    /** 自定义2  */
    ZDY2?: string;
    /** 自定义3  */
    ZDY3?: string;
    /** 自定义4  */
    ZDY4?: string;
}

/** 单行错误结果 */
interface ErrorResult {
    /** 错误内容 */
    message?: string;
    /** 错误字段名 */
    field?: string;
    /** 错误字段值 */
    data?: string;
}
