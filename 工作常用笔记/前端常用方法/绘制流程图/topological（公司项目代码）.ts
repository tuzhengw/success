/**
 * 食品溯源流程拓扑图生成脚本
 *
 * @createdate 20210901
 * @author kongwq
 */

import { uuid, ContainerComponent, showWarningMessage, showWaiting, isEmpty } from 'sys/sys';
import { Button } from 'commons/basic';
import { getQueryManager } from 'dw/dwapi';
import * as d3 from 'd3';
import dagreD3 from './dagre-d3.js'
import 'css!./topological.css'; // 引用css文件必须用相对路径
/**
 * 食品信息节点
 */
interface FoodInfoNode {
	/** 商品批次号 */
	sppch: string;
	/** 商品名称 */
	spmc: string;
	/** 商品条形码 */
	sptxm: string;
	/** 节点类型 */
	type: NodeType;
}

/**
 * 食品溯源节点
 */
interface FoodTraceNode {
	/** 企业名称 */
	qymc: string;
	/** 企业id */
	qyid: string;
	/** 企业社会信用代码 */
	shxydm: string;
	/** 上游企业名称 */
	gysmc: string[];
	/** 上游企业社会信用代码 */
	gystyshxydm: string[];
	/** 进货日期 */
	jhrq: string;
	/** 商品数量（来自【单个】上游企业数量） */
	sl: number;
	/** 食品标识 */
	spbs: number;
	/** 商品数量（来自【多个】上游企业汇总数量） */
	hzz: number;
	/** 节点类型 */
	type: NodeType;
}

/**
 * 节点类型
 */
enum NodeType {
	/** 根节点 */
	ROOT,
	/** 普通节点 */
	NODE
}

/**
 * 路径信息
 */
interface Edge {
	/** nodes数组中起点对象的索引 */
	source: number;
	/** nodes数组中终点对象的索引 */
	target: number;
}

/**
 * 食品拓扑模型结构
 */
interface FoodTopologicalModel {
	/** 节点数组 */
	nodes: (FoodInfoNode | FoodTraceNode)[];
	/** 路径数组 */
	edges: Edge[];
}

export class Topological extends ContainerComponent {
	/** 图像dom节点 */
	private svg: any;
	/** 鼠标拖动数据 */
	private dragData: Array<{
		/** 起始x位置 */
		mx: number,
		/** 起始y位置 */
		my: number,
		/** x方向位移 */
		dx: number,
		/** y方向位移 */
		dy: number,
		/** 首次拖动标志 */
		flag: boolean
	}>;
	/** svg初始高度 */
	private initialHeight: number;
	/** svg初始宽度 */
	private initialWidth: number;
	/** 图像缩放比例 */
	private scale: number;

	/**
	 * 拓扑图初始化方法
	 * @param args
	 * @returns
	 */
	public _init(args: any): HTMLElement {
		this.scale = 1;
		const domBase = super._init(args);
		// 构建滚动容器
		this.domScrollContainer = document.createElement('div');
		this.domScrollContainer.classList.add('scroll-container');
		this.domScrollContainer.oncontextmenu = () => {
			return false;
		}
		domBase.appendChild(this.domScrollContainer);
		// 构建拓扑图容器
		const topologicalContainer = d3.select(this.domScrollContainer.appendChild(document.createElement('div')));
		topologicalContainer.attr('class', 'topological-container');
		const domBtn = document.createElement("div");
		domBtn.classList.add("topological-btn");
		domBase.appendChild(domBtn);
		this.buildBtn(domBtn);
		this.buildTopological(topologicalContainer, args);
		return domBase;
	}

	/**
	 * 构建拓扑图
	 */
	private buildTopological(domBase: any, args: any) {
		const modelGenerater = new FoodTraceModelGenerater(args.PCID, args.QYID);
		const generateModelPromise = modelGenerater.generateModel();
		showWaiting(generateModelPromise, this.domScrollContainer);
		generateModelPromise.then((model) => {
			if (model !== null) {
				this.svg = domBase.append('svg'); // 构建输出节点
				this.svg.attr('class', 'topological');
				this.setSvgDrag(); // 设置svg可拖动
				const g = this.svg.append('g'); // 使用dagre-d3构造一个拓扑图
				g.attr('id', 'topological-out');
				const topo = new dagreD3.graphlib.Graph().setGraph({}).setDefaultEdgeLabel(function () { return {}; });
				topo.setNode(0, { labelType: "html", label: this.generateDomString(model.nodes[0], args.XS), class: 'root', rx: 3, ry: 3 }); // 设置【首节点】及边的信息
				for (let i = 1; i < model.nodes.length; i++) {
					topo.setNode(i, { labelType: "html", label: this.generateDomString(model.nodes[i], args.XS), class: 'node', rx: 3, ry: 3 }); // 设置【子节点】及边的信息
				}
				for (let i = 0; i < model.edges.length; i++) { // 构造线
					let edgesTargetId = model.edges[i].target; // 【线】箭头指向的【节点所在ID】
					if (!!model.nodes[edgesTargetId]["sl"]) { // 若线的目标对象，包含：商量数量，则在【线中央】添加：商品数量：XXXX
						if (model.nodes[edgesTargetId]["spbs"] == 1) {
							topo.setEdge(model.edges[i].source, model.edges[i].target, { label: `商品数量：${model.nodes[edgesTargetId]["sl"]}` });
						} else {
							topo.setEdge(model.edges[i].source, model.edges[i].target, { label: `商品重量：${model.nodes[edgesTargetId]["sl"]}kg` });
						}
						continue;
					}
					topo.setEdge(model.edges[i].source, model.edges[i].target);
				}
				const render = new dagreD3.render();
				render(g, topo); // 渲染节点
				// 根据图的大小设置svg宽高
				this.svg.attr('width', topo.graph().width);
				this.svg.attr('height', topo.graph().height);
				this.initialWidth = topo.graph().width;
				this.initialHeight = topo.graph().height;
			} else {
				showWarningMessage('暂无数据', 5000);
			}
		});
	}

	/**
	 * 设置svg拖动功能
	 */
	private setSvgDrag() {
		const drag = d3.drag()
			.on("drag", function dragmove(d) {
				if (d.flag) {
					d.mx = d3.event.x;
					d.my = d3.event.y;
					d.flag = false;
				}
				d.dx = d.dx + d3.event.x - d.mx;
				d.dy = d.dy + d3.event.y - d.my;
				d.mx = d3.event.x;
				d.my = d3.event.y;
				d3.select(this)
					.attr("transform", `translate(${d.dx}, ${d.dy})`);
			});
		this.dragData = [{ mx: 0, my: 0, dx: 0, dy: 0, flag: false }];
		this.svg.data(this.dragData).on('mousedown', function (d: any) {
			d.flag = true;
		}).call(drag);
	}

	/**
	 * 生成节点domHTML字符串
	 * @param 展示的某个节点
	 * @param xs 设置显示模式，为 1 时显示商品追溯页面，为 0 时显示企业追溯页面
	 */
	private generateDomString(node: FoodInfoNode | FoodTraceNode, xs: string): string {
		if (xs && xs === '1') {
			if ('sppch' in node) {
				return `<div class='node-container'><div class='data-title'>批次号：${node.sppch}</div>
					<div class='data-content1'><span style='font-weight: bold;'>商品名称：&#12288</span><span>${node.spmc}</span></div>
					<div class='data-content'><span style='font-weight: bold;'>商品条形码：</span><span>${node.sptxm}</span></div></div>`
			} else {
				let html = `<div class='node-container'><div class='data-title'>企业名称：${node.qymc}</div>
						<div class='data-content1'><span style='font-weight: bold;'>社会信用代码：</span><span>${node.shxydm}</span></div>`;
				if (node.jhrq) {
					html += `<div class='data-content'><span style='font-weight: bold;'>记录时间：&#12288&#12288</span><span>${`${node.jhrq.substr(0, 4)}年${node.jhrq.substr(4, 2)}月${node.jhrq.substr(6, 2)}日`}</span></div>`
				}
				if (node.hzz) {
					if (node.spbs == 1) { // 1：预包装，2：农产品
						html += `<div class='data-content'><span style='font-weight: bold;'>商品数量：&#12288&#12288</span><span>${node.hzz}</span></div></div>`
					} else {
						html += `<div class='data-content'><span style='font-weight: bold;'>商品重量：&#12288&#12288</span><span>${node.hzz}kg</span></div></div>`
					}
				}
				return html;
			}
		} else {
			if ('sppch' in node) {
				return `<div class='node-container'><div class='data-title'>批次号：${node.sppch}</div>
						<div class='data-content1'><span style='font-weight: bold;'>商品名称：&#12288</span><span>${node.spmc}</span></div>
						<div class='data-content'><span style='font-weight: bold;'>商品条形码：</span><span>${node.sptxm}</span></div></div>`
			} else {
				let html = `<div class='node-container'><div class='data-title'>企业名称：${node.qymc}</div>
						<div class='data-content1'><span style='font-weight: bold;'>社会信用代码：</span><span>${node.shxydm}</span></div></div>`
				if (node.jhrq) {
					html += `<div class='data-content'><span style='font-weight: bold;'>记录时间：&#12288&#12288</span><span>${`${node.jhrq.substr(0, 4)}年${node.jhrq.substr(4, 2)}月${node.jhrq.substr(6, 2)}日`}</span></div>`
				}
				if (node.sl) {
					if (node.spbs == 1) { // 1：预包装，2：农产品
						html += `<div class='data-content'><span style='font-weight: bold;'>商品数量：&#12288&#12288</span><span>${node.sl}</span></div></div>`
					} else {
						html += `<div class='data-content'><span style='font-weight: bold;'>商品重量：&#12288&#12288</span><span>${node.sl}kg</span></div></div>`
					}
				}
				return html;
			}
		}

	}

	/** 
	 * 构造页面点击事情
	 * @param domBtn 
	 */
	private buildBtn(domBtn: HTMLElement) {
		/** 还原按钮点击事件 */
		const zoomReset = new Button({
			className: 'topological-zoom',
			domParent: domBtn,
			icon: 'icon-refresh',
			onclick: this.zoomReset.bind(this)
		});
		/** 放大按钮点击事件 */
		const zoomIn = new Button({
			className: 'topological-zoom',
			domParent: domBtn,
			icon: 'icon-zoomin',
			onclick: this.zoomIn.bind(this)
		});
		/** 缩小按钮点击事件 */
		const zoomOut = new Button({
			className: 'topological-zoom',
			domParent: domBtn,
			icon: 'icon-zoomout',
			onclick: this.zoomOut.bind(this)
		});
	}

	/**
	 * 缩小按钮点击事件
	 */
	private zoomOut() {
		let scale = Math.round(this.scale * 10) / 10;
		if (scale <= 0.1) {
			showWarningMessage("已至最小，不能再缩小了!", 3000);
			return;
		}
		this.scale = scale = Math.round((scale - 0.1) * 10) / 10;
		this.svg.attr('width', (this.initialWidth * scale + 'px')).attr('height', (this.initialHeight * scale + this.initialHeight / 10) + 'px');
		this.svg.select('g').attr('transform', `scale(${scale})`);
		this.requestDoResize(true);
	}

	/**
	 * 放大按钮点击事件
	 */
	private zoomIn() {
		let scale = Math.round(this.scale * 10) / 10;
		if (scale >= 2) {
			showWarningMessage("已至最大，不能再放大了!", 3000);
			return;
		}
		this.scale = scale = Math.round((scale + 0.1) * 10) / 10;
		this.svg.attr("width", this.initialWidth * scale + this.initialWidth / 10 + 'px').attr("height", (this.initialHeight * scale + this.initialHeight / 10) + 'px');
		this.svg.select('g').attr('transform', `scale(${scale})`);
		this.requestDoResize(true);
	}

	/**
	 * 还原按钮点击事件
	 */
	private zoomReset() {
		this.svg.attr("transform", `translate(0, 0)`);
		this.svg.attr("width", this.initialWidth + "px").attr("height", this.initialHeight + "px");
		this.svg.select('g').attr('transform', `scale(1)`);
		// 重置拖动属性
		this.dragData[0].mx = 0;
		this.dragData[0].my = 0;
		this.dragData[0].dx = 0;
		this.dragData[0].dy = 0;
		this.dragData[0].flag = false;
		this.requestDoResize(true);
		// 重置缩放比例
		this.scale = 1;
	}
}

/**
 * 食品溯源模型生成器
 */
class FoodTraceModelGenerater {
	/** 商品批次id */
	private pcid: string;
	/** 企业id */
	private qyid: string;
	/** 食品拓扑模型 */
	private model: FoodTopologicalModel;

	constructor(pcid: string, qyid?: string) {
		this.pcid = pcid;
		this.qyid = qyid;
	}

	/**
	 * 生成拓扑模型
	 * @returns 获取拓扑图模型的promise
	 */
	public generateModel(): Promise<FoodTopologicalModel> {
		this.model = {
			nodes: [],
			edges: []
		}
		const promise: Promise<FoodTopologicalModel> = Promise.all([this.getRootNode(), this.getNodes()]).then((data) => {
			/** 接受生成模型所需要的数据 */
			const rootNode = data[0];
			/** 展示的所有节点集 */
			let nodes = data[1];
			// 如果有企业id参数则删除无关节点
			if (this.qyid && this.qyid.length !== 0) {
				const memo: FoodTraceNode[] = [];
				const queue: FoodTraceNode[] = [];
				for (let i = 0; i < nodes.length; i++) {
					if (this.qyid === nodes[i].qyid) {
						memo.push(nodes[i]);
						queue.push(nodes[i]);
						nodes.remove(nodes[i]); // 删除原数组中元素防止重复添加
						break;
					}
				}
				if (memo.length !== 0) {
					let gystyshxydm: string[];
					// 层序遍历上层节点存入memo数组
					while (queue.length > 0) {
						const tempNode = queue.shift();
						gystyshxydm = tempNode.gystyshxydm;
						gystyshxydm.forEach(dm => {
							for (let i = 0; i < nodes.length; i++) {
								if (dm === nodes[i].shxydm) {
									memo.push(nodes[i]);
									queue.push(nodes[i]);
									nodes.remove(nodes[i]);
								}
							}
						});
					}
					nodes = memo;
				} else {
					return null;
				}
			}
			if (rootNode && nodes) {
				/** 根节点存入模型节点数组 */
				this.model.nodes.push(rootNode);
				/** 根据节点信息构造路径信息 */
				const nodesLen = nodes.length;
				for (let i = 0; i < nodesLen; i++) {
					if (nodes[i].gystyshxydm.length === 1 && nodes[i].gystyshxydm[0] === null) {
						const edge: Edge = {
							source: 0,
							target: i,
						}
						this.model.edges.push(edge);
						continue;
					}
					/** 记录循环开始前的路径数组长度，用来判断当前节点是否有上游节点 */
					const edgesLen = this.model.edges.length;
					for (let j = 0; j < nodes.length; j++) {
						if (nodes[i].gystyshxydm.indexOf(nodes[j].shxydm) !== -1) {
							const edge: Edge = {
								source: j + 1,
								target: i + 1
							}
							this.model.edges.push(edge);
						}
					}
					if (edgesLen === this.model.edges.length) {
						// 当前节点的上游节点不在该批节点中，构造一个该批次的上游节点并且加入节点数组防止重复构造
						for (let k = 0; k < nodes[i].gystyshxydm.length; k++) {
							const node: FoodTraceNode = {
								qymc: nodes[i].gysmc[k],
								qyid: null,
								shxydm: nodes[i].gystyshxydm[k],
								gysmc: null,
								gystyshxydm: null,
								jhrq: null,
								sl: null,
								type: NodeType.NODE
							}
							nodes.push(node);
							const edge1: Edge = {
								source: 0,
								target: nodes.length
							}
							const edge2: Edge = {
								source: nodes.length,
								target: i + 1
							}
							this.model.edges.push(edge1);
							this.model.edges.push(edge2);
						}
					}
				}
				// 该批次节点全部加入模型节点数组
				nodes.forEach((node) => {
					this.model.nodes.push(node);
				})

				return this.model;
			} else {
				return null;
			}
		})
		return promise;
	}

	/**
	 * 返回起始节点
	 * @returns 获取食品信息的promise
	 */
	private getRootNode(): Promise<FoodInfoNode> {
		const filter = `model1.SPPCID='${this.pcid}'`;
		const queryInfo: QueryInfo = {
			sources: [{
				id: 'model1',
				path: '/SPZS/data/tables/SJJG/F_SPXXPCMX.tbl'
			}],
			filter: [{
				exp: filter
			}],
			fields: [{
				name: 'SPPCH',
				exp: 'model1.SPPCH'
			}, {
				name: 'SPMC',
				exp: 'model1.SPMC'
			}, {
				name: 'SPTXM',
				exp: 'model1.SPTXM'
			}]
		}
		return getQueryManager().queryData(queryInfo, uuid()).then(result => {
			if (result.data.length > 0) {
				return {
					sppch: <string>result.data[0][0],
					spmc: <string>result.data[0][1],
					sptxm: <string>result.data[0][2],
					type: NodeType.ROOT
				}
			} else {
				return null;
			}
		})
	}

	/**
	 * 返回流转节点
	 * @returns 获取食品溯源信息的promise
	 */
	private getNodes(): Promise<FoodTraceNode[]> {
		const queryInfo: QueryInfo = {
			sources: [{
				id: 'model1',
				path: '/SPZS/data/tables/SJJG/F_SPSY.tbl',
				params: [{
					name: 'PCID',
					value: this.pcid
				}]
			}],
			fields: [{
				name: 'QYMC',
				exp: 'model1.QYMC'
			}, {
				name: 'QYID',
				exp: 'model1.QYID'
			}, {
				name: 'SHXYDM',
				exp: 'model1.SHXYDM'
			}, {
				name: 'GYSMC',
				exp: 'model1.GYSMC'
			}, {
				name: 'GYSTYSHXYDM',
				exp: 'model1.GYSTYSHXYDM'
			}, {
				name: 'ZZJHRQ',
				exp: 'model1.ZZJHRQ'
			}, {
				name: 'SL',
				exp: 'model1.SL'
			}, {
				name: `SPBS`,
				exp: `model1.SPBS`
			}]
		}
		return getQueryManager().queryData(queryInfo, uuid()).then(result => {
			const nodes: FoodTraceNode[] = [];
			// 存储已经存入nodes数组的企业id，用于合并统一企业的数据
			const memo: string[] = [];
			for (let i = 0; i < result.data.length; i++) {
				const index = memo.indexOf(<string>result.data[i][1]);
				if (index === -1) {
					// 构造【流转节点】对象
					const node: FoodTraceNode = {
						qymc: <string>result.data[i][0],
						qyid: <string>result.data[i][1],
						shxydm: <string>result.data[i][2],
						gysmc: <string[]>[result.data[i][3]],
						gystyshxydm: <string[]>[result.data[i][4]],
						jhrq: <string>result.data[i][5],
						sl: <number>(result.data[i][6]),
						spbs: <number>(result.data[i][7]),
						// hzz: <number>(result.data[i][8]),
						hzz: <number>(result.data[i][6]),
						type: NodeType.NODE
					}
					nodes.push(node);
					memo.push(<string>result.data[i][1]);
				} else {
					/**
					 * 如果【当前企业已经储存过】，则根据【索引】合并上游名称、上游企业社会信用代码和采购商品数量
					 * 作用：当前节点信息 + 与之关联的上游节点信息
					 */
					nodes[index].gysmc.push(<string>result.data[i][3]);
					nodes[index].gystyshxydm.push(<string>result.data[i][4]);
					nodes[index].sl = result.data[i][6];
					nodes[index].hzz = Number(nodes[index].hzz) + Number(result.data[i][6]);  // 累加上游节点的商量【数量 | 重量】
				}
			}
			if (nodes.length > 0) {
				return nodes;
			} else {
				return null;
			}
		})
	}
}