/**
 * =====================================================
 * 作者：tuzw
 * 审核人员：liuyz
 * 创建日期:2022-06-15
 * 功能描述
 * 		此脚本是规则模块前端脚本
 * 帖子地址：
 * =====================================================
 */
import {
    ctx, SZEvent, Component, throwInfo, throwError, showConfirmDialog, showFormDialog, showErrorMessage, showMenu, isEmpty, deepAssign, uuid, message,
    quoteExpField, showDialog, showSuccessMessage, showProgressDialog, rc, downloadFile, waitRender, showWaiting, assign, IDataProvider, formatDateFriendly,
    parseDate, formatDate, tryWait, rc_get, showWarningMessage, ServiceTaskPromise, wait
} from 'sys/sys';
import { IMetaFileCustomJS, InterActionEvent } from "metadata/metadata-script-api";
import { ExpDialog, ExpDialogValueInfo, IFieldsDataProvider, showExpDialog } from 'commons/exp/expdialog';
import { ExpCompiler, ExpVar, IExpDataProvider } from 'commons/exp/expcompiler';
import { ExpContentType } from 'commons/exp/exped';
import { runCheckTask, checkCstmRule } from 'dg/dgapi';
import { getDwTableDataManager, getQueryManager, updateDwTableDatas, getDwTableChangeEvent } from 'dw/dwapi';
import { FAppCommandRendererId, FAppInterActionEvent, ICustomJS, IFApp, IFAppCustomJS, IVComponent } from 'metadata/metadata-script-api';
import { AnaNodeData } from "ana/datamgr";

/** 有前缀表名的自定义规则字段的dp */
let dgRuleFieldDp: CustomRuleFieldDp;
/**
 * 规则检测类
 */
export class Rules_CustomActions {
    public CustomActions: any;
    /** 页面提示检测失败的控件ID集 */
    public errorInputComponentIds: string[];
    /** 页面聚焦的输入框控件DOM */
    public focusInputComponentDom;

    constructor() {
        this.CustomActions = {
            button_rule_getFilterCondition: this.button_rule_getFilterCondition.bind(this),
            button_rule_executePlans: this.button_rule_executePlans.bind(this),
            button_local_error_component: this.button_local_error_component.bind(this),
            button_clearOpointInputCompentCheck: this.button_clearOpointInputCompentCheck.bind(this),
            button_rule_showErrorWain: this.button_rule_showErrorWain.bind(this),
            setCheckRuleFilterExp: this.setCheckRuleFilterExp.bind(this)
        };
        this.errorInputComponentIds = [];
        this.focusInputComponentDom = null;
    }

    /**
     * 根据前端传递的参数，构造规则信息过滤表达式（exp）
     * （1）注意：多个以中文顿号（、）隔开，三者是一一对应的关系
     * @param column 过滤字段名，eg：RULE_TYPE、RULE_ID、
     * @param operater 操作符号，eg：like 、like、=
     * @param value 值，eg：'%2%'、'%7%'、'0001'
     * @param connetionOperation 连接符号，【符号个数】：表达式个数 - 1（【单个条件】不传），
     *         eg：or、and  eg：r.RULES_LIB_ID = 'QGkIxQOqy3MSgfXUectmZC' AND (r.RULE_SET LIKE '%2%' OR r.RULE_SET LIKE '%7%')
     */
    public setCheckRuleFilterExp(event: InterActionEvent): void {
        let page = event.page;
        let params = event?.params as JSONObject;
        let filterExp: string[] = [];
        let column: string[] = !!params.column
            ? params.column
                .replaceAll("\n", "")
                .replaceAll("==", "=") // 由于数据库不支持：==，将表达式中的"=="转换为"="
                .split("、")
            : [];
        let operater: string[] = !!params.operater
            ? params.operater
                .replaceAll("\n", "")
                .replaceAll("==", "=") // 由于数据库不支持：==，将表达式中的"=="转换为"="
                .split("、")
            : [];
        let value: string[] = !!params.value
            ? params.value
                .replaceAll("\n", "")
                .replaceAll("==", "=") // 由于数据库不支持：==，将表达式中的"=="转换为"="
                .split("、")
            : [];
        let connetionOperation: string[] = !!params.connetionOperation
            ? params.connetionOperation
                .replaceAll("\n", "")
                .replaceAll("==", "=") // 由于数据库不支持：==，将表达式中的"=="转换为"="
                .split("、")
            : [];

        let expNums: number = column.length;
        if (operater.length != expNums || value.length != expNums) {
            showWarningMessage(`构造规则信息过滤表达式（exp） 参数对应关系错误，请检查传递参数值是否一一对应`);
            return;
        }
        let connOperationIndex: number = 0;
        for (let i = 0; i < expNums; i++) { // 由于后端使用的是连接SQL查询，这里必须给定：前缀
            let temp: string = `r.${column[i].trim()} ${operater[i]} '${value[i].trim()}'`; // eg：RULE_ID = '1001'
            filterExp.push(temp);
            if (!!connetionOperation[connOperationIndex]) { // 
                filterExp.push(` ${connetionOperation[connOperationIndex]} `);
                connOperationIndex++;
            }
        }
        page.setParameter("ruleFilterExp", filterExp.join(" ")); // 将结果赋值到调用SPG指定全局变量
    }

    /**
     * 20220110 tuzw
     * 帖子：4.0审核规则应用
     * 地址：https://jira.succez.com/browse/CSTM-17594
     * @param sourceAlias 操作的数据源别名，在DG_RULES_LIB表里设置
     * @param isNeedPrefix 是否需要表hao达式前缀
     * @param expType  表达式类型，默认：expression，支持：auto、expression、macro、tex
     * @param outPutComponentId 设置的表达式值输出到那个组件中，eg：multipleinput1
     */
    public button_rule_getFilterCondition(event: InterActionEvent) {
        let page = event.page;
        let params = event?.params as JSONObject;
        let sourceAlias = params.sourceAlias;
        let isNeedPrefix = params.isNeedPrefix;
        let outPutComponentId = params.outPutComponentId;
        let expPrefixIsNeed: boolean = true; // 默认：true
        if (!!isNeedPrefix && isNeedPrefix == "false") {
            expPrefixIsNeed = false;
        }
        let expType: string = params.expType;
        function showExp() {
            let filterInputComp = page.getComponent(outPutComponentId);
            showExpDialog({
                readonly: false,
                newborn: true,
                defaultTab: "fields",
                expRequired: true,
                buttons: ['ok'],
                expDataProvider: dgRuleFieldDp.getExpDp(),
                descValidateArgs: {
                    required: true,
                },
                contentType: getExpContentType(expType),
                fieldPanelImpl: {
                    depends: "dsn/datasourceeditor",
                    implClass: "DataSourceEditor",
                    sourceTheme: SourceTheme.Combobox
                },
                fieldsDataProvider: dgRuleFieldDp,
                caption: " 配置检查规则测试数据范围的过滤条件",
                value: {
                    exp: filterInputComp.getValue()
                },
                needTransformValue: true,
                onok: function (e: SZEvent, dialog: ExpDialog) {
                    filterInputComp.setValue(dialog.getValue().exp);
                }
            });
        }
        if (dgRuleFieldDp == null) {
            dgRuleFieldDp = new CustomRuleFieldDp();
        }
        if (!!sourceAlias) {
            dgRuleFieldDp.setAssginSource(sourceAlias);
        }
        if (dgRuleFieldDp.getIsNeedTableName()) {
            dgRuleFieldDp.setIsNeedTableName(expPrefixIsNeed); // 设置表达式不需要前缀表别名
            if (!!sourceAlias) {
                dgRuleFieldDp.initTableAndFieldsInfo(sourceAlias).then(() => { // 重新加载字段信息
                    showExp();
                });
            } else {
                dgRuleFieldDp.initTableAndFieldsInfo().then(() => {
                    showExp();
                });
            }
        } else {
            showExp();
        }
    }

    /**
      * 执行检测任务
      *  @param checkType 检测的类型，eg：
      * （1）temp 临时计划，随机生成
      * （2）format 正式执行，指定执行任务ID
      * （3）handExecute 手动执行，指定执行的任务id，修改增量方式、最后成功检测时间字段，执行完后，【还原】原有指定计划的设置
      *
      * @param executeRuleNumberType 检测规则数量大小类型
      * （1）apart 部分，执行勾选的数据
      * （2）all 执行全部（默认）
      *
      * @param ruleFilterConditon? 规则过滤条件EXP（检测规则数量：all时指定，可不传），rf：规则库引入信息表 、r：规则库表
                eg：rf.MAIN_TABLE_ID= 'QGkIxQOqy3MSgfXUectmZC' AND (r.RULE_SET LIKE '%2%' OR r.RULE_SET LIKE '%7%')
      *
      * @param executeExistCheckTaskId? 检测任务ID（已存在的）
      *  注意：checkTaskId，若执行脚本【下方还有其他脚本】，由于检测任务内部是【异步方法】，检测任务id在【执行前】指定（全局变量：执行前设置参数：uuid）
      * @param showExecuteDetailPage? 是否展示检测详情页面

      * 注意：检测表配置设置
        @param settingType? 检测表配置类型，eg：customType、pageSettings
        第一种（customType）：页面传入一一对应的配置参数（支持不同检测表不同配置）
      * @param checkTableResids? 检测表Resid集，eg：qbJwagaQ2RaIDPOIagml1nD, fagwOIQ2agaDPOIeLml1nD
      * @param checkTableFilterCondtion? 检测表的数据范围（二维数组，外层：'|'隔开，内层："、"（中文）隔开），eg：name='张三'、 sex='男' | FCYDATE='20211201'
      * @param checkTableCheckNums? 检测数据数量，eg："100", "0"
      * @param checkTableErrorInputPaths? 检测错误结果输出路径（不传为默认输出路径），eg：dg/error1.tbl, dg/error2.tabl

      * 第二种（pageSettings）：根据不同页面类型的质控，配置不同的配置信息（配置内容除【过滤字段名】不一致，过滤值一致）
      * @param pageCheckType 检测的页面质控类型，eg：writePatientInfoPage（病案录入）
      * @param filterDateValue? 过滤日期值，eg：'2022'
      * @param fieldsFilterExp? 多字段组件值exp值，eg："age = 12 and name = '张三'
      * @param checkTableCheckNums? 检测表检测数量，eg："10"
      * @param checkTableErrorInputPaths? 检测表输出路径集，eg：eg：dg/error2.tabl
      * @param checkOtherFieldExp? 检测配置扩展字段（字段必须存在输出模型上），eg：注意单引号
                （1）静态字符串：[{"fieldName":"DYBSLX","exp":"'YTSB'"}]
                （2）将检测表对应【字段的值】写入到扩展字段：[{"fieldName":"MAIN_DOCTER","exp":"FZZDOCT"}]
                注意：扩展字段不能包含规则表的信息（DG_RULE.TBL）
        20220419 tuzw
        帖子地址：https://jira.succez.com/browse/CSTM-18520
        @param customWaitStyle? 自定义等待样式（class名），eg：waiting-ring-active
        @param runingBanComponentId? 运行时指定控件设置为灰色状态
      */
    public button_rule_executePlans(event: InterActionEvent): Promise<any> {
        let params = event?.params as JSONObject;
        let checkType: string = params.checkType;
        if (!checkType) {
            showWarningMessage(`必给参数缺少，checkType：${checkType}`);
            return Promise.resolve(false);
        }
        let executeRuleNumberType: string = !!params.executeRuleNumberType ? params.executeRuleNumberType : "all";
        let ruleFilterConditon: string = !!params.ruleFilterConditon ? params.ruleFilterConditon : "";

        let executeExistCheckTaskId: string = !!params.executeExistCheckTaskId
            ? typeof params.executeExistCheckTaskId == "string" // 获取的是全局参数，类型：数组 | 字符串
                ? params.executeExistCheckTaskId
                : params.executeExistCheckTaskId[0]
            : "";
        /** 检测表配置信息 */
        let checkTableSettings: CHECK_TABLE_SETTING[] = [];
        let settingType: string = !!params.settingType ? params.settingType.trim() : "";
        if (!!settingType) {
            let settingStatus: boolean = true;
            switch (settingType) {
                case "customType": // 自定义配置检测表参数
                    settingStatus = this.getCustomCheckSetting(params, checkTableSettings)
                    break;
                case "pageSettings": // 根据页面配置检测表参数
                    settingStatus = this.getPageCheckTableSettings(params, checkTableSettings)
                    break;
                default:
                    settingStatus = false;
            }
            if (!settingStatus) {
                showWarningMessage(`当前配置类型为：${settingType}，配置失败，请检查传入参数或者页面配置类型值错误`);
                Promise.resolve(false);
            }
        }
        /** 执行的规则信息 */
        let checkRules: CheckRule[] = [];
        this.getChoseRules(event, executeRuleNumberType, checkRules);
        if (executeRuleNumberType == "apart" && checkRules.length == 0) {
            showWarningMessage(`没有要执行的规则信息`);
            Promise.resolve(false);
        }
        let showExecuteDetailPage: boolean = !!params.showExecuteDetailPage
            ? params.showExecuteDetailPage == "false" ? false : true
            : true;
        let runingBanComponentId: string = params.runingBanComponentId;
        let customWaitStyle: string = params.customWaitStyle
        return this.requestDealRuleInfo(
            event,
            checkType,
            executeRuleNumberType,
            checkRules,
            checkTableSettings,
            showExecuteDetailPage,
            ruleFilterConditon,
            executeExistCheckTaskId,
            customWaitStyle,
            runingBanComponentId
        );
    }

    /**
     * 通过前端配置的参数，请求后端处理相关检测表信息
     * @param event
     * @param checkType  checkType 检测的类型
     * （1）temp 临时计划，随机生成
     * （2）format 正式执行，指定执行任务ID
     * （3）handExecute 手动执行，指定执行的任务id，修改增量方式、最后成功检测时间字段，执行完后，【还原】原有指定计划的设置
     * @param executeRuleNumberType  检测规则数量大小类型
     * （1）apart 部分，执行勾选的数据
     * （2）all 执行全部（默认）
     *
     * @param checkRules 页面勾选所执行的规则信息，可为：[]
     * @param checkTableSettings 检测表相关配置信息
     *
     * @param showExecuteDetailPage? 是否展示检测详情页面
     * @param ruleFilterConditon?  规则过滤条件EXP（检测规则数量：all时指定，可不传），rf：规则库引入信息表 、r：规则库表
                eg：rf.MAIN_TABLE_ID= 'QGkIxQOqy3MSgfXUectmZC' AND (r.RULE_SET LIKE '%2%' OR r.RULE_SET LIKE '%7%')
     * @param executeExistCheckTaskId? 检测任务ID（已存在的）
     * @param customWaitStyle? 自定义等待样式（class名），eg：waiting-ring-active
     * @param runingBanComponentId? 运行时指定控件设置为灰色状态
     */
    public requestDealRuleInfo(
        event: InterActionEvent,
        checkType: string,
        executeRuleNumberType: string,
        checkRules: CheckRule[],
        checkTableSettings: CHECK_TABLE_SETTING[],
        showExecuteDetailPage: boolean,
        ruleFilterConditon?: string,
        executeExistCheckTaskId?: string,
        customWaitStyle?: string,
        runingBanComponentId?: string
    ): Promise<any> {
        let page = event.page;
        return rc({
            url: `/rule/ruleCheck/dealRuleTable`,
            method: "POST",
            data: {
                choseRuleData: checkRules,
                checkType: checkType,
                executeRuleNumberType: executeRuleNumberType,
                ruleFilterConditon: ruleFilterConditon,
                executeExistCheckTaskId: executeExistCheckTaskId,
                checkTableSettings: checkTableSettings
            }
        }).then((result: ResultInfo) => {
            if (!result.result) {
                showWarningMessage(`---【${result.message}】---`);
                return;
            }
            let checkTaskId: string = result.data.checkTaskId;
            if (!checkTaskId) {
                showWarningMessage(`---检测任务ID为：【${result.message}】---`);
                return;
            }
            let promise: ServiceTaskPromise<any> = runCheckTask(checkTaskId, uuid());
            if (showExecuteDetailPage) {
                showProgressDialog({
                    caption: "任务执行日志",
                    uuid: checkTaskId,
                    promise: promise,
                    buttons: [{
                        id: "cancel",
                        caption: "关闭"
                    }],
                    logsVisible: true,
                    width: 700,
                    height: 500
                });
            }
            let waitPromise: Promise<any>;
            // let noShowDom = page.getComponent("panel18").component.domParent; // 将指定dom设置为灰色状态（由于showWaiting指定dom遮罩层不生效，改为遮罩整个body）
            if (!!customWaitStyle) {
                waitPromise = showWaiting(promise, document.body, true, { layoutTheme: customWaitStyle });
            } else {
                waitPromise = showWaiting(promise, document.body, true);
            }
            return waitPromise.then(() => {
                throwInfo("检测完毕");
                page.setParameter("checkTaskId", checkTaskId); // 将checkTaskId设置为全局变量
                getDwTableDataManager().updateCache([{ // 考虑脚本存在【异步】，检测任务ID更新后，需要更新【检测结果表】数据，刷新使用脚本执行
                    path: "/sysdata/data/tables/dg/confs/FORMAT_CHECK_RESULT.tbl",
                    type: DataChangeType.refreshall
                }, {
                    path: "/sysdata/data/tables/dg/confs/IMITATE_CHECK_RESULT.tbl",
                    type: DataChangeType.refreshall
                }]);
            }).catch((e) => {
                showWarningMessage(`检测失败`);
            });
        });
    }

    /**
     * 根据检测规则数量类型，获取对应的规则集信息
     * （1）类型：apart，则获取页面列表（默认：list1）勾选的规则信息
     * @param event 页面对象
     * @param executeRuleNumberType 检测规则数量类型 
     * @param checkRules 筛选后的规则信息
     */
    public getChoseRules(event, executeRuleNumberType: string, checkRules: CheckRule[]): void {
        if (executeRuleNumberType == "apart") { // 若检测针对于列表所选择数据，则获取列表当前勾选的规则信息
            let tableConfDataset = event.component.getComponent("list1").getCheckedDataRows();
            if (tableConfDataset.length == 0) {
                showWarningMessage(`---当前检测类型为：勾选规则，当前未捕获到页面选择的数据---`);
                return;
            }
            for (let i = 0; i < tableConfDataset.length; i++) {
                let ruleStatus: number = tableConfDataset[i]["RULE_CHECK_STATUS"];
                if (ruleStatus == 1) { // 过滤掉已经禁用的规则信息
                    checkRules.push({
                        RULE_ID: tableConfDataset[i]["RULE_ID"],
                        RULES_LIB_ID: tableConfDataset[i]["RULES_LIB_ID"]
                    });
                }
            }
        }
    }

    /**
     * 根据前端传递的参数，构造检测表相关配置信息
     * （1）设置的过滤条件、数量、输出路径统一
     * @param pageCheckType 检测的页面质控类型，eg：writePatientInfoPage（病案录入）
     * @param filterDateValue? 过滤日期值，eg：'2022'
     * @param fieldsFilterExp? 多字段组件值exp值，eg："age = 12 and name = '张三'
     * @param checkTableCheckNums? 检测表检测数量，eg："10"
     * @param checkTableErrorInputPaths? 检测表输出路径集，eg：eg：dg/error2.tabl
     * @param checkOtherFieldExp? 检测配置扩展字段（字段必须存在输出模型上），eg：[{"fieldName":"DYBSLX","exp":"'YTSB'"}]
     * @param checkTableSettings 保存生成的检测表配置信息
     * @return true | false
     */
    public getPageCheckTableSettings(params, checkTableSettings: CHECK_TABLE_SETTING[]): boolean {
        let pageCheckType: string = !!params.pageCheckType ? params.pageCheckType : "";
        if (pageCheckType == "") {
            showWarningMessage(`检测的页面质控类型为空，请检查交互参数`);
            return false;
        }
        let checkSettings: PageSettings = this.getCheckResidAndDateField(pageCheckType);
        if (!checkSettings) { // 没有配置，则不设置配置信息
            return true;
        }
        let checkTableResids: string[] = checkSettings.resids as string[];
        let filterDateColumn: string[] = checkSettings.filterDateColumn as string[];
        /** 检测表的数据范围 */
        let checkTableFilterCondtions: string[][] = [];

        let filterDateValue: string = !!params.filterDateValue ? params.filterDateValue : "";
        let fieldsFilterExp: string = !!params.fieldsFilterExp ? params.fieldsFilterExp : "";
        for (let i = 0; i < checkTableResids.length; i++) { // 设置过滤条件
            let filter: string[] = [];
            if (!!filterDateValue && !!filterDateColumn[i]) {
                filter.push(`${filterDateColumn[i]} = '${filterDateValue}'`);
            }
            if (!!fieldsFilterExp) {
                filter.push(fieldsFilterExp);
            }
            checkTableFilterCondtions.push(filter);
        }
        let checkTableCheckNums: number = !!params.checkTableCheckNums ? Number(params.checkTableCheckNums.trim()) : 0;
        let checkTableErrorInputPaths: string = !!params.checkTableErrorInputPaths ? params.checkTableErrorInputPaths.trim() : "";
        let checkOtherFieldExp: string = !!params.checkOtherFieldExp ? params.checkOtherFieldExp : ""

        let checkTableNum = checkTableResids.length;
        if (checkTableResids.length != 0) {
            for (let i = 0; i < checkTableNum; i++) {
                checkTableSettings[i] = {
                    CHECK_MODEL_RESID: checkTableResids[i].trim(),
                    IS_CONFIG: false,
                    CHECK_FILTER_EXP: this.remoteArrNoValid(checkTableFilterCondtions[i]),
                    CHECK_RANDOM_NUMS: checkTableCheckNums, // 若为0，后端则默认为：全部符合的数据
                    CHECK_ERROR_RESULT_INPUT_PATH: checkTableErrorInputPaths,
                    FIELD_VALUES: checkOtherFieldExp
                }
            }
        }
        return true;
    }

    /**
     * 根据传入的页面检测类型，获取检测的资源和过滤时间字段
     * @param pageCheckType 页面检测类型
     */
    public getCheckResidAndDateField(pageCheckType: string): PageSettings {
        if (!pageCheckType) {
            return {};
        }
        let settings: PageSettings = {};
        switch (pageCheckType) {
            case "writePatientInfoPage": // 录入病案页面
                settings = {
                    resids: ["rUVxKZpE9EKHE3YKmRq4ZG", "TYXcZeA8UgGhF3IsaEvp", "u7zLakzVMLJkK3Lu4o1R9F", "QOOcWdL1RfmOiossmMxlD", "BPKF6drGYEGIye7nQYDNHG", "wDIexNwI9NKpMzDcORVuMC",
                        "j2RviPt7Y0CSz9HR57U5UB", "cev9E7GdL9MXfUvOPWj0G"],
                    filterDateColumn: []
                }
                break;
            case "handExecuteCheckPage":  // 手动检测页面
                settings = {
                    resids: ["rUVxKZpE9EKHE3YKmRq4ZG", "TYXcZeA8UgGhF3IsaEvp", "u7zLakzVMLJkK3Lu4o1R9F", "QOOcWdL1RfmOiossmMxlD", "BPKF6drGYEGIye7nQYDNHG", "wDIexNwI9NKpMzDcORVuMC",
                        "j2RviPt7Y0CSz9HR57U5UB", "cev9E7GdL9MXfUvOPWj0G"],
                    filterDateColumn: ["FCYDATE", "FCYDATE", "FCYDATE", "FCYDATE", "FCYDATE", "FCYDATE", "FCYDATE", "FCYDATE"]
                }
                break;
            case "reportPatientPage": // 对外上报病案审核功能页面
                settings = {
                    resids: ["QGkIxQOqy3MSgfXUectmZC", "wLOTqlx7GhDVBO3rWxOF2B", "QUZtXrjBoQFWzHWPRVUPyB"],
                    filterDateColumn: ["FCYDATE", "FCYDATE", "FCYDATE"]
                }
                break;
            case "imitateCheckPage":  // 模拟检测页面
                settings = {
                    resids: ["rUVxKZpE9EKHE3YKmRq4ZG", "TYXcZeA8UgGhF3IsaEvp", "u7zLakzVMLJkK3Lu4o1R9F", "QOOcWdL1RfmOiossmMxlD", "BPKF6drGYEGIye7nQYDNHG", "wDIexNwI9NKpMzDcORVuMC",
                        "j2RviPt7Y0CSz9HR57U5UB", "cev9E7GdL9MXfUvOPWj0G"],
                    filterDateColumn: ["FCYDATE", "FCYDATE", "FCYDATE", "FCYDATE", "FCYDATE", "FCYDATE", "FCYDATE", "FCYDATE"]
                }
                break;
        }
        return settings;
    }

    /**
     * 根据前端传递的参数，构造检测表相关配置信息
     * @param checkTableResids? 检测表Resid集，eg：qbJwagaQ2RaIDPOIagml1nD, fagwOIQ2agaDPOIeLml1nD
     * @param checkTableFilterCondtion? 检测表的数据范围（二维数组，外层：'|'隔开，内层："、"（中文）隔开），eg：name='张三'、 sex='男' | FCYDATE='20211201'
     * @param checkTableCheckNums? 检测数据数量，eg："100", "0"
     * @param checkTableErrorInputPaths? 检测错误结果输出路径（不传为默认输出路径），eg：dg/error1.tbl, dg/error2.tabl
     * @param checkTableSettings 保存生成的检测表配置信息
     * @return true | false
     */
    public getCustomCheckSetting(params, checkTableSettings: CHECK_TABLE_SETTING[]): boolean {
        /** 检测表Resid集 */
        let checkTableResids: string[] = !!params.checkTableResids
            ? params.checkTableResids
                .trim()
                .replaceAll("\n", "")
                .split(",")
            : [];
        /** 检测表的数据范围（以'|'分割的二维数组） */
        let checkTableFilterCondtions: string[] = !!params.checkTableFilterCondtions
            ? params.checkTableFilterCondtions
                .trim()
                .replaceAll("\n", "")
                .replaceAll("==", "=") // 由于数据库不支持：==，将表达式中的"=="转换为"="
                .split("|")
            : [];
        /** 检测表检测数量 */
        let checkTableCheckNums: string[] = !!params.checkTableCheckNums
            ? params.checkTableCheckNums
                .trim()
                .replaceAll("\n", "")
                .split(",")
            : [];
        /** 检测表输出路径集 */
        let checkTableErrorInputPaths: string[] = !!params.checkTableErrorInputPaths
            ? params.checkTableErrorInputPaths
                .trim()
                .replaceAll("\n", "")
                .split(",")
            : [];

        if (checkTableResids.length != 0) {
            let checkTableNum: number = checkTableResids.length;
            // 跟配置表相关属性，必须要一一对应，否则，拦截执行
            if (checkTableFilterCondtions.length != checkTableNum || (checkTableCheckNums.length != 0 && checkTableCheckNums.length != checkTableNum)) {
                showWarningMessage("检测表配置信息参数不是一一映射关系，请检测参数");
                return false;
            }
            for (let i = 0; i < checkTableNum; i++) {
                checkTableSettings[i] = {
                    CHECK_MODEL_RESID: checkTableResids[i].trim(),
                    IS_CONFIG: false,
                    CHECK_FILTER_EXP: this.remoteArrNoValid(checkTableFilterCondtions[i].trim().replace('\n', "").split("、")),
                    CHECK_RANDOM_NUMS: !!checkTableCheckNums[i] ? Number(checkTableCheckNums[i]) : 0, // 若为0，后端则默认为：全部符合的数据
                    CHECK_ERROR_RESULT_INPUT_PATH: !!checkTableErrorInputPaths[i] ? checkTableErrorInputPaths[i].trim() : ""
                }
            }
        }
        return true;
    }

    /**
     * 去除字符数组中的无效值（""，" "、 undefine）
     * @param arr 处理的字符数组
     */
    public remoteArrNoValid(arr: string[]): string[] {
        if (arr.length == 0) {
            return [];
        }
        let newArr: string[] = [];
        for (let i = 0; i < arr.length; i++) {
            if (!arr[i] || arr[i] == '\n' || arr[i] == " ") {
                continue;
            }
            newArr.push(arr[i]);
        }
        return newArr;
    }

    /**
     * 20220302 tuzw
     * 检测结束后，病案录入面板将显示错误提示（注意：脚本执行完成后，页面必须【重新刷新】对应的模型数据）
     * （1）获取检测结果信息，根据检测规则找到对应检测规则检测字段
     * （2）根据检测字段 匹配 页面与之绑定字段的控件
     * （3）对当前控件增加失败的校验提示
     * @param checkTaskId 检测任务ID
     */
    public button_rule_showErrorWain(event: InterActionEvent) {
        let page = event.page;
        let params = event?.params as JSONObject;
        let contentPanel = page.getComponent("canvas");// SPG最外层画布ID

        let checkTaskId = params.checkTaskId;
        if (!checkTaskId) {
            showWarningMessage("病案录入面板显示错误提示方法，未获取到检测任务ID");
            return;
        }
        rc({ // 根据检测任务ID，获取【检测结果表】对应规则【检测的字段】信息
            url: `/rule/ruleCheck/getRuleCheckFields`,
            method: "POST",
            data: {
                dataBase: "default",
                tableName: "IMITATE_CHECK_RESULT", // 写入模拟检测结果表中
                checkTaskId: checkTaskId, // 当前: 质控 随机生成的任务ID
                schema: "baglxt"
            }
        }).then((result: ResultInfo) => {
            if (!result.result) {
                showWarningMessage(`---【${result.message}】---`);
                return;
            }
            let queryRules: CheckErrorField[] = result.queryCheckFileds;
            if (queryRules.length == 0) {
                return;
            }
            let allChildrenComponents = contentPanel.node.builder.getComponents(); // 获得指定面板内的所有子控件
            for (let i = 0; i < queryRules.length; i++) {
                let checkFieldName: string = queryRules[i].CHECK_FIELD;
                for (let j = 0; j < allChildrenComponents.length; j++) { // 遍历所有子控件所绑定的字段，找到与错误字段一致的控件
                    let component = allChildrenComponents[j];
                    let submitFieldName: string = component.metaInfo.submitField;
                    if (!submitFieldName) {
                        continue;
                    }
                    let fieldName = submitFieldName.substring(submitFieldName.lastIndexOf(".") + 1); // 去掉前缀，获取字段名，eg：model1.XM
                    if (fieldName == checkFieldName) {
                        let wainMessage: string = queryRules[i].RULE_NAME;
                        let firstStopPlace: number = wainMessage.indexOf("。");
                        if (firstStopPlace != -1) { // 展示消息，仅展示第一个句号前的信息
                            wainMessage = wainMessage.substring(0, firstStopPlace);
                        }
                        let errorComponentId = component.id;
                        this.openInputCompentCheck(page.getComponent(errorComponentId), true, wainMessage);
                        this.errorInputComponentIds.push(errorComponentId);
                        break;
                    }
                }
            }
        });
    }

    /**
     * 20220301 tuzw
     * 点击【浮动面板】某行错误信息，定位到对应组件
     * 需求：将检测的错误信息，通过浮动面板展示到前端页面，点击错误信息，可以聚焦到对应的input组件位置（组件【绑定的字段】与检测信息【检测的字段】一致）
     *
     * 帖子地址：https://jira.succez.com/browse/CSTM-17962?focusedCommentId=194275&page=com.atlassian.jira.plugin.system.issuetabpanels%3Acomment-tabpanel
     * @param errorFieldName 当前选中的错误检测字段（检测结果表——规则ID字段——关联维表——系统规则表，SPG可直接获取到关联的表信息），eg：name
     * 
     * 20220407 tuzw
     * 由于部分错误字段存在定制的JS组件中，给部分检测表新增：联合主键，eg：000342f6064ff41bd016d0c0864e221f,1 按','转为数组，最后一位为列表第几行
     * 帖子：https://jira.succez.com/browse/CSTM-18357
     * @param errorResultPkValue? 当前错误信息主键值
     */
    public button_local_error_component(event: InterActionEvent) {
        let page = event.page;
        let params = event.params as JSONObject;
        let errorFieldName = params?.errorFieldName;
        if (!errorFieldName) {
            showWarningMessage(`未获取到检测的字段名，参数错误，请检查传递参数`);
            return;
        }
        let contentPanel = page.getComponent("canvas"); // SPG最外层画布ID

        let errorFieldParentPanelId: string = ""; // 错误字段所在父面板ID（第二个父面板）
        let errorComponentId: string = "";
        let allChildrenComponents = contentPanel.node.builder.getComponents(); // 获得指定面板内的所有子控件
        if (!allChildrenComponents || allChildrenComponents.length == 0) {
            showWarningMessage(`未获取相关输入控件`);
            return true;
        }
        for (let i = 0; i < allChildrenComponents.length; i++) { // 遍历所有子控件所绑定的字段，找到与错误字段一致的控件
            let component = allChildrenComponents[i];
            let submitFieldName: string = component.metaInfo.submitField;
            if (!submitFieldName) {
                continue;
            }
            let fieldName = submitFieldName.substring(submitFieldName.lastIndexOf(".") + 1); // 去掉前缀，获取字段名
            if (fieldName == errorFieldName) {
                errorComponentId = component.id;
                errorFieldParentPanelId = component.getParent().getParent().id; // 输入框控件所在的父控件的父控件ID
                this.errorInputComponentIds.push(errorComponentId);
                break;
            }
        }
        /**
         * 20220407 tuzw
         * 由于页面存在多个JS定制脚本，若页面控件没有找到，则在定制控件寻找
         */
        if (!errorFieldParentPanelId) {
            /** 定制组件中是否存在错误字段，默认：不存在 */
            let jsComponentIsExistField: boolean = false;
            let errorResultPkValue: string = params.errorResultPkValue;
            if (!!errorResultPkValue) {
                let errorRowMessage: string[] = errorResultPkValue.split(",");
                let errorRowIndex: number = Number(errorRowMessage[errorRowMessage.length - 1]);
                if (errorRowIndex != NaN) {
                    for (let i = 1; i <= 2; i++) {
                        let jsComponent1 = page.getComponent(`jsComponent${i}`);
                        let comp = jsComponent1.component.getJSComponent().customTable;
                        errorFieldParentPanelId = jsComponent1.component.getParentComponent().id; // 获取JS控件所在父控件
                        jsComponentIsExistField = comp.focusAssignFiledComponent(errorFieldName, errorRowIndex);
                        if (jsComponentIsExistField) { // 方法内部会聚焦和增加样式，若找到，则直接返回
                            page.setParameter("errorPanelId", errorFieldParentPanelId);
                            if (this.focusInputComponentDom != null) { // 清除外部聚焦样式
                                this.focusInputComponentDom.classList.remove("inputFocus"); // 删除上一个点击控件聚焦样式
                            }
                            return;
                        }
                    }
                }
            }
            if (!jsComponentIsExistField) {
                showWarningMessage(`未获取到错误字段所在的控件，请检测检测字段是否有跟当前页面控件绑定`);
                return false;
            }
        }

        page.setParameter("errorPanelId", errorFieldParentPanelId); // 将错误字段所在面板ID设置为全局变量
        let errorComponent = page.getComponent(errorComponentId);
        // this.openInputCompentCheck(errorComponent, true); // 是否开启控件的校验功能
        let componentDom = errorComponent.component.domBase;
        if (this.focusInputComponentDom != null) {
            this.focusInputComponentDom.classList.remove("inputFocus"); // 删除上一个点击控件聚焦样式
        }
        this.focusInputComponentDom = componentDom;
        componentDom.classList.add("inputFocus");
        componentDom.focus(); // 聚焦到此控件
        return true;
    }

    /**
     * 20220301 tuzw
     * 关闭指定的输入框控件的校验功能
     */
    public button_clearOpointInputCompentCheck(event: InterActionEvent) {
        let needCloseInputIds = this.errorInputComponentIds;
        if (needCloseInputIds.length == 0) {
            return;
        }
        let page = event.page;
        for (let i = 0; i < needCloseInputIds.length; i++) {
            let input = page.getComponent(needCloseInputIds[i]);
            this.openInputCompentCheck(input, false);
        }
        return true;
    }

    /**
     * 开启输入控件的校验
     * @param inputComponent 输入控件
     * @param isOpenCheck 是否开启校验，eg：true | false
     * @param wainMessage 检测不合法提示信息，默认：检测错误
     */
    public openInputCompentCheck(inputComponent: IVComponent, isOpenCheck: boolean, wainMessage?: string): void {
        let errorComponent = inputComponent.component;
        let componentIsOpenCheck: boolean = errorComponent.compBuilder.getProperty(PropertyNames.ValidEnabled);
        if (!componentIsOpenCheck || !isOpenCheck) {
            if (!isOpenCheck) {
                errorComponent.compBuilder.setProperty(PropertyNames.ValidEnabled, isOpenCheck);
                return;
            }
            errorComponent.compBuilder.setProperty(PropertyNames.ValidEnabled, isOpenCheck);
            let data: AnaNodeData = errorComponent.getComponentData().getNodeData();
            data.setProperty(PropertyNames.ValidEnabled, true);
            data.setProperty(PropertyNames.ValidExp, false); // 设置校验公式：false
            if (!wainMessage) {
                wainMessage = "检测错误";
            }
            data.setProperty(PropertyNames.ValidMessage, wainMessage); // 根据key对组件属性进行赋值
            data.setProperty(PropertyNames.HasValidate, true);
            errorComponent.renderValidMessage(true); // 开启渲染校验状态（开启后，则不再执行）
        }
    }
}

/**
 * 获取【表达式编辑框的编辑内容或编辑的模式】
 * @param expType 表达式类型
 * @return ExpContentType
 */
function getExpContentType(expType: string): ExpContentType {
    switch (expType) {
        case "auto": return ExpContentType.AUTO;
            break;
        case "expression": return ExpContentType.EXPRESSION; // name=="张三"
            break;
        case "macro": return ExpContentType.MACRO; // ${name=="张三"}
            break;
        case "text": return ExpContentType.TEXT;
            break;
    }
    return ExpContentType.EXPRESSION;
}

/**
 * 20210720 liuyz
 * https://jira.succez.com/browse/CSTM-15853
 * 创建字段获取的dp
 */
export class CustomFieldDp implements IFieldsDataProvider {
    private dataBase: string;
    private dbTable: string;
    private expDp: CustomExpDp;
    private fieldInfos: JSONObject;
    public constructor(args: { dataBase: string, dbTable: string }) {
        this.dataBase = args.dataBase;
        this.dbTable = args.dbTable;
        this.expDp = new CustomExpDp();
        this.fieldInfos = {};
    }
    fetchDataSources(): Promise<{ id: string; desc?: string; isCurrentDataSource?: boolean; }[]> {
        return Promise.resolve([{
            id: this.dbTable,
            isCurrentDataSource: true,
            desc: this.dbTable
        }])
    }

    fetchFields(sourceId: string, fetchArgs: { [propName: string]: any; isDimension?: boolean; }): JSONObject {
        if (fetchArgs.isDimension && !isEmpty(this.fieldInfos)) {
            return this.fieldInfos;
        }
        return fetchArgs?.isDimension && rc({
            url: `/rule/ruleCheck/getFieldInfos`,
            data: {
                dataBase: this.dataBase,
                dbTable: this.dbTable
            }
        }).then(result => {
            let data: CustomFieldItem[] = [];
            for (let i = 0; i < result.length; i++) {
                let info = result[i];
                let name = info.name.toUpperCase();
                let id = "[" + name + "]";
                let json: CustomFieldItem = {
                    id,
                    caption: name,
                    name: name,
                    pickValue: [name],
                    value: id,
                    dataType: info.dataType,
                    dbfield: name,
                    isDimension: true,
                    isField: true,
                    leaf: true,
                    icon: "icon-string-dim"
                };
                data.push(json);
                this.expDp.setField(json);
            };
            this.fieldInfos = data;
            return data;
        })
    }
    searchFields(sourceId: string, searchArgs: { [propName: string]: any; keyword?: string; isDimension?: boolean; }): Promise<JSONObject[]> {
        let key = searchArgs.keyword.toUpperCase();
        return searchArgs?.isDimension && rc({
            url: `/rule/ruleCheck/getFieldInfos`,
            data: {
                dataBase: this.dataBase,
                dbTable: this.dbTable
            }
        }).then(result => {
            let data: CustomFieldItem[] = [];
            for (let i = 0; i < result.length; i++) {
                let info = result[i];
                let name = info.name.toUpperCase();
                let id = "[" + name + "]";
                if (name.indexOf(key) != -1) {
                    let json: CustomFieldItem = {
                        id,
                        caption: name,
                        name: name,
                        pickValue: [name],
                        value: id,
                        dataType: info.dataType,
                        dbfield: name,
                        isDimension: true,
                        isField: true,
                        leaf: true,
                        icon: "icon-string-dim"
                    };
                    data.push(json);
                }
            };
            return data;
        })
    }
    public getExpDp() {
        return this.expDp;
    }

    /**
     * 初始化字段信息
     * @returns
     */
    public initFiledInfo() {
        return this.fetchFields(this.dbTable, { isDimension: true });
    }
}

/**
 * 定制的表达式dp
 */
export class CustomExpDp implements IExpDataProvider {
    private tableAndFieldsExpVar: ExpVar[];
    private fields: string[];
    private tableFieleMap: JSONObject;// 数据表和模型对应关系
    /**
     * 2022-03-17
     * 如果是关联了维表的字段，那么要能获取到对应的表达式信息
     */
    public getVarsByParent(parentVar?: IExpVar): Array<IExpVar> {
        if (parentVar != null && !isEmpty(this.tableFieleMap)) {
            let expVars = this.tableFieleMap[parentVar.getName()];
            if (expVars == null) {
                let pickValue = parentVar.getPickValue();
                let modelFields = this.tableFieleMap[pickValue[0]];
                for (let i = 0; i < modelFields.length; i++) {
                    let varExp = modelFields[i];
                    if (varExp.getName() == parentVar.getName()) {
                        return varExp.items;
                    }
                }
            }
            return expVars;
        }
        return this.tableAndFieldsExpVar;
    }

    // public getVarByParent(varName: string, parentVar?: IExpVar): IExpVar{
    //     if()
    // }

    public setTable(tableInfo) {
        let expVar = new ExpVar(tableInfo);
        this.tableAndFieldsExpVar == null && (this.tableAndFieldsExpVar = []);
        this.tableAndFieldsExpVar.push(expVar);
    }

    /**
     * 设置字段表达式
     * 字段的表达式需要记录下对应的模型id,方便后面进行获取
     * @param fieldInfo
     * @param sourceId
     */
    public setField(fieldInfo, sourceId?: string) {
        let expVar = new ExpVar(fieldInfo);
        this.tableAndFieldsExpVar == null && (this.tableAndFieldsExpVar = []);
        this.fields == null && (this.fields = []);
        this.tableAndFieldsExpVar.push(expVar);
        this.fields.push(fieldInfo.caption);
        if (!isEmpty(sourceId)) {
            isEmpty(this.tableFieleMap) && (this.tableFieleMap = {});
            isEmpty(this.tableFieleMap[sourceId]) && (this.tableFieleMap[sourceId] = []);
            this.tableFieleMap[sourceId].push(expVar);
        }
    }

    public validateFieldName(name: string) {
        if (!name) {
            return message("dataflow.fieldname.null");
        }
        if (name && name.match(/[`"\[\]]+/)) {
            return message("dataflow.fieldname.existInvalidChar");
        }
        let upperName = name.toUpperCase();
        // 可以修改名称的大小写
        if (this.fields.indexOf(upperName) != -1) {
            return message("dataflow.fieldname.repeat");
        }
        return null;
    }
}

const DIMENSION_VAR_ICON: { [key: string]: string } = {
    "C": "icon-string-dim",
    "N": "icon-float-dim",
    "I": "icon-int-dim",
    "D": "icon-date-dim",
    "T": "icon-date-dim",
    "P": "icon-date-dim",
    "M": "icon-clob-dim",
    "X": "icon-blob-dim"
};

const MEASURE_VAR_ICON: { [key: string]: string } = {
    "C": "icon-string",
    "N": "icon-float",
    "I": "icon-int",
    "D": "icon-date",
    "T": "icon-date",
    "P": "icon-date",
    "M": "icon-clob",
    "X": "icon-blob"
};

/** 规则表字段对象 */
interface CustomFieldItem {
    id: string;
    /** 字段标题 */
    caption: string;
    /** 选择值 */
    pickValue: string[];
    value: string;
    /** 字段类型 */
    dataType: string;
    /** 物理字段名称 */
    dbfield: string;
    /** 是否为维度 */
    isDimension: boolean;
    /** 是否为字段 */
    isField: boolean;
    /** 是否为叶子节点 */
    leaf: boolean;
    /** 图标 */
    icon: string;
    /** 字段中文名描述 */
    name: string;
    /** 字段表达式 */
    exp?: string;
    /** 字段描述 */
    desc?: string;
    /**关联维表字段信息 */
    items?: CustomFieldItem[];
}

/**
 * 20211015 kongwq
 * 创建自定义规则字段的dp
 */
export class CustomRuleFieldDp implements IFieldsDataProvider {
    private tableInfos: { id: string, desc?: string, isCurrentDataSource?: boolean }[];
    private ruleTableInfo: Record<string, string[]>; // 表别名和id对应对象
    private ruleTabelField: Record<string, CustomFieldItem[]>
    private expDp: CustomExpDp;
    /** 设置指定的数据源 */
    private assginSourceId: string;
    /** 当前查看的数据源 */
    private currentSourceId: string;
    /** 设置表达式是否需要携带表名 */
    private isNeedTableName: boolean;

    constructor() {
        this.tableInfos = [];
        this.ruleTableInfo = {};
        this.ruleTabelField = {};
        this.expDp = new CustomExpDp();
        this.isNeedTableName = true;
    }

    /**
     * 获取规则表id和别名
     * @returns
     */
    public fetchDataSources(): Promise<{ id: string, desc?: string, isCurrentDataSource?: boolean }[]> {
        if (!isEmpty(this.tableInfos)) {
            if (!isEmpty(this.assginSourceId)) {
                return Promise.resolve(this.tableInfos.filter(t => { return t.id == this.assginSourceId }));
            } else {
                /**
                 * 遍历整个资源记录表，根据currentSourceId（id）找到当前需要处理的资源信息，使之成为：激活状态，eg：
                 * tableInfos {
                 * 		0: {id: 'EDU_F_XSSJ_1', isCurrentDataSource: false, desc: '全国近十年学生数据'}
                        1: {id: 'F_JJHK_CON_1', isCurrentDataSource: false, desc: '经济户口表——关联模型'}
                   }
                 */
                return Promise.resolve(this.tableInfos.map(table => {
                    if (table.id == this.currentSourceId) {
                        table.isCurrentDataSource = true;
                    }
                    return table;
                }));
            }
        }
        // 若当前tableInfos为null，则自动发请求去请求规则表信息数据
        return rc({
            url: `/rule/ruleCheck/getRuleTableInfos`
        }).then(results => {
            let tableInfos = this.tableInfos = [];
            results.forEach((result: Array<string>) => {
                if (result[2] === null) {
                    result[2] = result[1];
                }
                this.ruleTableInfo[result[1]] = [];
                this.ruleTableInfo[result[1]].push(result[0]);
                this.ruleTableInfo[result[1]].push(result[2])
                tableInfos.push({
                    id: result[1],
                    isCurrentDataSource: false,
                    desc: result[2]
                })
                this.expDp.setTable({ name: result[1], caption: result[2] });
            });
            if (!isEmpty(this.assginSourceId)) {
                return tableInfos.filter(t => { return t.id == this.assginSourceId });
            } else {
                return tableInfos.map(t => {
                    if (t.id == this.currentSourceId) {
                        t.isCurrentDataSource = true;
                    }
                    return t;
                });
            }
        });
    }

    /**
     * 请求当前指定资源的字段集
     * @sourceId 资源别名
     */
    public fetchFields(sourceId: string, fetchArgs: { [propName: string]: any; isDimension?: boolean; }): JSONObject {
        if (this.ruleTabelField[sourceId]) {
            return this.ruleTabelField[sourceId].filter(f => { return f.isDimension == fetchArgs.isDimension });
        } else {
            return rc({
                url: `/rule/ruleCheck/getFieldInfos`,
                method: "POST",
                data: {
                    /** 模型表的resid */
                    tableId: this.ruleTableInfo[sourceId][0],
                    /** 是否为维度字段 */
                    isDimension: fetchArgs.isDimension
                }
            }).then(results => {
                const fieldItems: CustomFieldItem[] = [];
                if (!this.ruleTabelField[sourceId]) {
                    this.ruleTabelField[sourceId] = [];
                }
                results.forEach((result: DwTableFieldInfo) => {
                    let customFieldItem: CustomFieldItem = this.setFieldInfo(result, sourceId, fetchArgs.isDimension);
                    fieldItems.push(customFieldItem);
                });
                return fieldItems;
            })
        }
    }

    public searchFields(sourceId: string, searchArgs: { [propName: string]: any; keyword?: string; isDimension?: boolean; }): Promise<JSONObject[]> {
        const key = searchArgs.keyword.toUpperCase();
        const fieldItems: CustomFieldItem[] = [];
        this.ruleTabelField[sourceId].forEach((item: CustomFieldItem) => {
            if ((item.name.indexOf(key) != -1 || item.caption.indexOf(key) != -1) && item.isDimension == searchArgs.isDimension) {
                fieldItems.push(item);
            }
        })
        return Promise.resolve(fieldItems);
    }

    public getExpDp() {
        return this.expDp;
    }

    /**
     * 设置表达式是否需要携带表名
     * @params isNeedTableName：true 需要表前缀名，false，不需要
     */
    public setIsNeedTableName(isNeedTableName: boolean): void {
        this.tableInfos = [];
        this.ruleTableInfo = {};
        this.ruleTabelField = {};
        this.expDp = new CustomExpDp();
        this.isNeedTableName = isNeedTableName;
    }

    /**
     * 获取表达式是否需要携带表名
     * @return boolean
     */
    public getIsNeedTableName(): boolean {
        return this.isNeedTableName;
    }

    /**
     * 设置指定要查看的数据源
     * @param assginSourceId  指定数据源在数据规则表中的别名
     * @param tableInfo 指定表的resid和desc（描述
     */
    public setAssginSource(assginSourceId: string, tableInfo?: { resid: string, desc: string }) {
        this.assginSourceId = assginSourceId;
        if (!isEmpty(tableInfo) && !this.ruleTableInfo[assginSourceId]) {
            let { resid, desc } = tableInfo;
            this.tableInfos.push({ id: assginSourceId, desc: desc });
            this.ruleTableInfo[assginSourceId] = [resid, desc];
            this.expDp.setTable({ name: assginSourceId, caption: desc })
        }
    }

    /**
     * 设置当前要查看的数据源
     * @param currentSourceId
     */
    public setcurrentSource(currentSourceId: string) {
        this.currentSourceId = currentSourceId;
    }

    /**
     * 初始化字段信息
     * @param currentSourceId：指定初始化数据库【别名】
     *
     * 注意：别名可通过【/sysdata/data/tables/dg/DG_RULES_LIB.tbl】表【待治理的数据表UUID】字段值设置
     */
    public initTableAndFieldsInfo(currentSourceId?: string) {
        return this.fetchDataSources().then(sources => {
            return rc({
                url: `/rule/ruleCheck/getAllTableFieldInfos`,
                method: "POST"
            }).then(results => {
                let sources = Object.keys(results);
                if (!!currentSourceId) { // 初始化【规则库（DG_RULES_LIB.LIB）】表中某表（别名指定）字段信息（一张表）
                    if (!this.ruleTabelField[currentSourceId]) {
                        this.ruleTabelField[currentSourceId] = [];
                    }
                    let data = results[currentSourceId];
                    data.dimensions.forEach((result: DwTableFieldInfo) => {
                        this.setFieldInfo(result, currentSourceId, true)
                    });
                    data.measures.forEach((result: DwTableFieldInfo) => {
                        this.setFieldInfo(result, currentSourceId, false)
                    });
                } else {
                    for (let i = 0; i < sources.length; i++) {  // 初始化【规则库（DG_RULES_LIB.LIB）】表中所有表字段信息（多张表）
                        const sourceId = sources[i];
                        if (!this.ruleTabelField[sourceId]) {
                            this.ruleTabelField[sourceId] = [];
                        }
                        let data = results[sourceId];
                        data.dimensions.forEach((result: DwTableFieldInfo) => {
                            this.setFieldInfo(result, sourceId, true)
                        });
                        data.measures.forEach((result: DwTableFieldInfo) => {
                            this.setFieldInfo(result, sourceId, false)
                        });
                    }
                }
            })
        })
    }

    private setFieldInfo(fieldInfo: DwTableFieldInfo, sourceId: string, isDimensions: boolean): CustomFieldItem {
        const name = fieldInfo.name.toUpperCase();
        const dbFieldName = fieldInfo.dbFieldName;
        const icon = isDimensions ? DIMENSION_VAR_ICON[fieldInfo.dataType] : MEASURE_VAR_ICON[fieldInfo.dataType];
        let hasChilren = !isEmpty(fieldInfo.children);
        const customFieldItem: CustomFieldItem = {
            id: dbFieldName,
            caption: name,
            name: dbFieldName,
            desc: name,
            pickValue: this.getIsNeedTableName() ? [sourceId, dbFieldName] : [dbFieldName],
            value: name,
            dataType: fieldInfo.dataType,
            dbfield: dbFieldName,
            isDimension: isDimensions,
            isField: true,
            leaf: !hasChilren,
            icon,
            exp: this.getIsNeedTableName() ? `${sourceId}.${dbFieldName}` : `${dbFieldName}`,
            items: hasChilren ? this.parseFieldInfo(fieldInfo.children, sourceId, dbFieldName) : []
        };
        this.ruleTabelField[sourceId].push(customFieldItem);
        this.expDp.setField(customFieldItem, sourceId);

        return customFieldItem;
    }

    /**
     * 2022-03-17
     * https://jira.succez.com/browse/CSTM-18115
     * 构造关联维表的字段信息
     */
    private parseFieldInfo(fieldInfos: DwTableFieldInfo[], sourceId: string, parentField: string) {
        let items = [];
        for (let i = 0; i < fieldInfos.length; i++) {
            let fieldInfo = fieldInfos[i];
            const name = fieldInfo.name.toUpperCase();
            const dbFieldName = fieldInfo.dbfield;
            const icon = DIMENSION_VAR_ICON[fieldInfo.dataType];
            let customFieldItem: CustomFieldItem = {
                id: parentField + "." + dbFieldName,
                caption: name,
                name: dbFieldName,
                desc: name,
                pickValue: this.getIsNeedTableName() ? [sourceId, parentField, dbFieldName] : [parentField, dbFieldName],
                value: name,
                dataType: fieldInfo.dataType,
                dbfield: dbFieldName,
                isDimension: true,
                isField: true,
                leaf: true,
                icon,
                exp: this.getIsNeedTableName() ? `${sourceId}.${parentField}.${dbFieldName}` : `${parentField}.${dbFieldName}`
            }
            items.push(customFieldItem);
        }
        return items;
    }
}


/** 检测结果对象 */
interface CheckErrorField {
    /** 规则id */
    RULE_ID?: string;
    /** 检测字段 */
    CHECK_FIELD?: string;
    /** 规则名称 */
    RULE_NAME?: string;
}

/** 返回信息 */
interface ResultInfo {
    /** 返回结果 */
    result: boolean;
    /** 消息 */
    message?: string;
    /** 返回数据 */
    data?: any;
    [propname: string]: any;
}

/** 检测表相关设置 */
interface CHECK_TABLE_SETTING {
    /** 检测模型id */
    CHECK_MODEL_RESID?: string;
    /** 是否已经配置过，初始：false */
    IS_CONFIG?: boolean;
    /** 检测模型数据范围，eg：LSH='5783371' */
    CHECK_FILTER_EXP?: string[];
    /** 指定随机检测数量 */
    CHECK_RANDOM_NUMS?: number;
    /** 是否增量检测 */
    IS_INCREMENT?: number;
    /** 检测错误结果输出路径 */
    CHECK_ERROR_RESULT_INPUT_PATH?: string;
    /** 扩展字段 */
    FIELD_VALUES?: string;
}

/** 检测资源固定配置信息 */
interface PageSettings {
    /** 检测资源resids */
    resids?: string[];
    /** 过滤的时间字段名 */
    filterDateColumn?: string[];
}

/** 检测规则 */
interface CheckRule {
    /** 规则编码 */
    RULE_ID: string;
    /** 规则库ID */
    RULES_LIB_ID?: string;
    /** 待治理数据表的UUID  */
    MAIN_TABLE_ID?: string;
    /** 资源别名 */
    ALIAS?: string;
    /** 是否启用 */
    RULE_CHECK_STATUS?: string
}