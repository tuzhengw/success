/**
 * ============================================================
 * 作者: tuzhengw
 * 审核人员：liuyongz
 * 创建日期：20220524
 * 项目：无锡智慧市场监管一期
 * 功能描述：
 *      此次数据对接是需要通过接口向朗新提供保供药店物资销售数据，朗新提供的对接文档见附件
 * 帖子地址：https://jira.succez.com/browse/CSTM-18828
 * ============================================================
 */

/**
 * 测试生成token
 */
export function testGeneratorToken() {
    let appKey = new JavaString("279dcdcf-b33f-42a6-8480-36fb84bbbd97");
    let appSecret = new JavaString("72b9feb0d74511ecbffd2fb4974324fb");
    let token = new Token(appKey, appSecret, 3000);
    token.createToken();
    for (let i = 0; i < 4; i++) {
        sys.sleep(1000);
        if (token.checkTokenIsEffective()) {
            print(`token有效`);
            continue;
        }
        print(`token无效`);
    }
    return token;
}

import metadata, { getJSON, getString, modifyFile } from "svr-api/metadata";
import db from "svr-api/db";
import fs from "svr-api/fs";
import { getDwTable } from "svr-api/dw";
import { getBean } from "svr-api/bean";
import sys from "svr-api/sys";
import { uuid } from "svr-api/utils";
import utils from "svr-api/utils";
import http from "svr-api/http";
import { getDataSource } from "svr-api/db"; //数据库相关API
import { queryDwTableData, queryData, openDwTableData } from "svr-api/dw";
import { DATA_EXCHANGE_UTILS, ResultInfo } from "/sdi/script/exchangeDataUtils/exchangeDataUtils.action";


/** 考虑到SQL查询慢，避免超时，每次请求控制在100条 */
const SEND_NUMS: number = 1000;

/** 数据源名 */
const DATA_SOURCE_DATA = "gsp";
const SCHEM = "GSP";
/** 数据源 */
const dataSource = getDataSource(DATA_SOURCE_DATA);

/**
 * 单线程启动
 * （1）考虑到多线程无法记录日志，改为单线程推送
 * （2）系统设置——安全设置——阈值设置——http连接超时设置：30秒，否则，会超时结束连接
 * @param requestIndex 请求信息数组对应的下标
 */
export function sendTable(requestIndex: number): void {
    if (requestIndex >= RequestInfo.length) {
        print(`请求的下标不合法，没有相关请求信息，若是新增请求表，则更改：【/sysdata/public/市场监管对接接口辅助文件/sendScript.action.ts】的【RequestInfo】数组配置信息`);
        return;
    }
    let requestInterfaceInfo: InterfaceInfo = RequestInfo[requestIndex];
    let modelPath: string = requestInterfaceInfo.modelPath;
    /** 记录当前处理模型表的物理字段名集 */
    let dbPhysicalFields: Array<string> = [];
    let dbPirmaryKey: Array<string> = [];
    let dataExchangeUtils = new DATA_EXCHANGE_UTILS();
    let queryModelFields: QueryFieldInfo[] = dataExchangeUtils.getTableFields(modelPath, dbPhysicalFields, dbPirmaryKey);
    if (queryModelFields.length == 0) {
        print(`未获取到模型：${modelPath} 的查询字段信息，结束执行`);
        return;
    }
    print(`当前请求信息：`);
    print(requestInterfaceInfo);

    let filterTimeFiled: string = requestInterfaceInfo.timeColumName;
    let requestWay: string = requestInterfaceInfo.requestWay;
    let filterCondtion = dataExchangeUtils.getQueryFilterCondition(requestWay, filterTimeFiled, true).data;

    let videoData = new Data_Exchage(requestInterfaceInfo, dbPhysicalFields, dataExchangeUtils);
    let queryInfo: QueryInfo = {
        fields: queryModelFields,
        select: true,
        sort: [{
            exp: `model1.${requestInterfaceInfo.timeColumName}`
        }],
        sources: [{
            id: "model1",
            path: `${modelPath}`
        }],
        filter: filterCondtion,
        options: {
            limit: 10,
            offset: 0,
            queryTotalRowCount: true
        }
    }
    try {
        videoData.sendTableData(queryInfo);
    } catch (e) {
        print(`sendTableData()内部方法有误，结束执行`);
        print(e.toString());
    }
}

/** 对接类 */
export class Data_Exchage {
    /** 外部手动控制程序是否强制停止的文件 */
    public headOptionTxtPath: string = "/sdi/script/cityProtectSystemData/isStopExecute.txt";
    /** 日期格式化对象，eg：yyyy-MM-dd */
    public sdfDate = new java.text.SimpleDateFormat("yyyy-MM-dd");
    /** 日期格式化对象，eg：yyyy-MM-dd HH:mm:ss */
    public sdfTime = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    /** 数据交换工具类 */
    public dataExchangeUtils: DATA_EXCHANGE_UTILS;

    /** 接口信息对象 */
    public interfaceInfo: InterfaceInfo;
    /** 当前推送表物理字段 */
    public dbPhysicalFields: string[];
	/** 日志表字段 */
    public logTableFields: string[];

    public token: Token;

    constructor(interfaceInfo: InterfaceInfo, dbPhysicalFields: string[], dataExchangeUtils: DATA_EXCHANGE_UTILS) {
        this.interfaceInfo = interfaceInfo;
        this.dbPhysicalFields = dbPhysicalFields;
        this.token = new Token(FORMAT_ENVIRONMENT_CODE.AppKey, FORMAT_ENVIRONMENT_CODE.AppSecret, 15 * 60 * 1000);
        this.dataExchangeUtils = dataExchangeUtils;
		
		this.logTableFields = [];
        this.logTableFields.pushAll(this.dbPhysicalFields); // 考虑到不同数据库之间的差异，改为dw模块写入
        this.logTableFields.pushAll(["SENT_MESSAGE", "CREATETIME"]);
    }

    /**
     * 处理单张表数据（单线程处理）
     * 主要：多线程不能使用queryData方法，内部方法没有被设置线程共享
     * @param queryInfo 查询的条件
     */
    public sendTableData(queryInfo: QueryInfo): boolean {
        print(`----sendTableData()，模型路径：${this.interfaceInfo.modelPath}， 开始推送数据，请求地址：${this.interfaceInfo.formatRequestAddress}----start`);
        let startTime: number = Date.now();
        let totalRowCount: number = 0; // 先发一次请求，获取当前模型表【数据总量】
        try {
            totalRowCount = queryData(queryInfo).totalRowCount;
        } catch (e) {
            print(`获取当前模型表【数据总量】失败，程序结束执行`);
            print(e.toString());
            throw e;
        }
        queryInfo.options.queryTotalRowCount = false; // 后续不需要获取【数据总量】，设置为：false
        if (!totalRowCount) {
            print(`模型路径：${this.interfaceInfo.modelPath}，数据为NULL，暂未推送数据，结束执行-----end`); // 可能是模型数据未提取到物理表中
            return false;
        }
        let dealCount: number = Math.ceil(totalRowCount / SEND_NUMS);
        print(`模型路径：${this.interfaceInfo.modelPath}，本次推送共：${totalRowCount}条，总共需要查询：${dealCount} 次，每次查询条数为：${SEND_NUMS} 条`);
        if (dealCount > 0) {
            queryInfo.options.limit = SEND_NUMS; // 分页：每页的数据行数
            for (let i = 0; i < dealCount; i++) {
                print(`当前是第【${i + 1}】次请求，一次请求处理：${SEND_NUMS} 条数据，总共需要处理：${dealCount} 次--start`);
                let isStop: string = getString(this.headOptionTxtPath);
                if (isStop == "true") {
                    print(`模型路径：${this.interfaceInfo.modelPath}，手动停止推送，当前已经推送：${(i - 1) * SEND_NUMS} 条数据`);
                    break;
                }
                if (i == 0 || i % 10 == 0) {
                    print(`模型路径：${this.interfaceInfo.modelPath}，开始第【${i + 1}】次请求，总请求次数为【${dealCount}】---`);
                }
                queryInfo.options.offset = i * SEND_NUMS; // 从第几行开始
                print(`当前请求的queryInfo信息options信息：`);
                print(queryInfo.options);
                let data: (string | number | boolean)[][] = [];
                try {
                    data = queryData(queryInfo).data;
                } catch (e) {
                    print(`当前是第【${i + 1}】次请求，查询地址：${i * SEND_NUMS}，执行产品的queryData失败，结束执行`);
                    print(e.toString());
                    throw e;
                }
                let legalJsonData = this.dataExchangeUtils.arrToJsonArrs(data, this.dbPhysicalFields);
                let pushDatas = [];
                for (let i = 0; i < legalJsonData.length; i++) {
                    let pushData = this.toDataField(legalJsonData[i]);
                    pushDatas.push(pushData);
                }
                let responseResult: ResultInfo = { result: true };
                try {
                    responseResult = this.requestThirdAddress(pushDatas);
                    if (!responseResult.result) {
                        this.insertErrorSentData(legalJsonData);
                    }
                } catch (e) {
                    print(`模型路径：${this.interfaceInfo.modelPath}，请求地址：${this.interfaceInfo.formatRequestAddress}，第【${i + 1}】次数据推送失败`);
                    print(e.toString());
                }
                print(`当前是第【${i + 1}】次请求推送完成，一次请求处理：${SEND_NUMS} 条数据，总共需要处理：${dealCount} 次，共消耗：${(Date.now() - startTime) / 1000} s ---end`);
            }
        }
        print(`----sendTableData()，模型路径：${this.interfaceInfo.modelPath}，请求地址：${this.interfaceInfo.formatRequestAddress}推送数据完成，共消耗：${(Date.now() - startTime) / 1000} s ----start`);
    }

    /**
     * 记录推送失败的数据
     * @param errorData 错误推送数据，JSON对象
     * @return true | false;
     */
    public insertErrorSentData(errorData): boolean {
        if (!errorData) {
            print(`insertErrorSentData()，错误信息为空，不执行写入操作`);
            return false;
        }
        print(errorData);
        let isInsertSuccess: boolean = true;
        if (!dataSource.isTableExists(this.interfaceInfo.logTableName)) {
            print(`记录错误信息的日志表：${this.interfaceInfo.logTableName} 不存在`);
            return false;
        }
        let table = dataSource.openTableData(this.interfaceInfo.logTableName);
        try {
            let insertNums: number = table.insert(errorData);
        } catch (e) {
            isInsertSuccess = false;
            print(`错误信息写入日志表：${this.interfaceInfo.logTableName} 失败--error`);
            print(e.toString());
        }
        return isInsertSuccess;
    }

    /**
     * 请求对方接口 
     * 注意：【必填项】 不能为：空 或者 ""
     * @param pushData 推送的数据，类型：JSON
     * @return 
     */
    public requestThirdAddress(pushData): ResultInfo {
        let startTime: number = Date.now();
        let baseToken: JavaString = "";
        if (this.token.checkTokenIsEffective()) {
            baseToken = this.token.getTokenValue();
        } else {
            this.token.createToken();
            baseToken = this.token.getTokenValue();
        }
        if (!this.interfaceInfo.formatRequestAddress) {
            print(`推送地址不合法：${this.interfaceInfo.formatRequestAddress}`);
            return { result: false };
        }

        let resultInfo: ResultInfo = { result: true };
        let accessAddress: string = this.interfaceInfo.formatRequestAddress;
        let requestParams = {
            url: accessAddress,
            headers: {
                "Content-Type": "application/json",
                "DigitalHub-Token": baseToken
            },
            data: {
                traceId: uuid(),
                timestamp: Math.floor(System.currentTimeMillis() / 1000),
                businessCode: this.interfaceInfo.businessCode,
                data: pushData
            }
        };
        let postResult = http.post(requestParams);
        if (postResult.httpCode == 200) {
            let requestResult = JSON.parse(postResult.responseText);
            if (requestResult.code != 0) {
                resultInfo.result = false;
                resultInfo.message = requestResult['Message'];
            }
        } else {
            resultInfo.result = false;
            resultInfo.message = "请求错误";
        }
        return resultInfo;
    }

    /** 
     * 功能：将JSON数组的key转为给定的驼峰字段名
     * 问题：由于对方提供的字段名为驼峰格式，产品查询出来的数据字段都是大写，这里需要做映射关系
     * @param data 需要处理的JSON对象，eg：[{ "ENTERPRISENAME": "123" }]
     * @param timeFiledNames 时间字段名数组，将时间转换为指定格式字符串，eg：["reportDate"]
     * @param paramFieldNames 驼峰命名数组，eg：["enterpriseName", "enterpriseCreditCode"]
     * @return
     */
    public toDataField(data) {
        if (!data || !this.interfaceInfo.paramFieldNames || this.interfaceInfo.paramFieldNames.length == 0) {
            return data;
        }
        let newData = {};
        let timeFiledNames: string[] = ["reportDate"];
        for (let i = 0; i < this.interfaceInfo.paramFieldNames.length; i++) {
            let filedName: string = this.interfaceInfo.paramFieldNames[i];
            let temp = data[filedName.toLocaleUpperCase()]; // 由于系统查询出来的字段都是大写，这里转成大学匹配
            if (timeFiledNames.includes(filedName)) { // 转行为：yyyy-MM-dd 格式的字符串
                let times: number = new Date(temp).getTime();
                let sdfTime = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                temp = sdfTime.format(new java.util.Date(times));
            }
            if (!temp) { // 符合的字段值不存在，则默认：空值
                temp = "";
                if (filedName == "inventory") {
                    temp = 0;
                }
            }
            newData[filedName] = temp;
        }
        return newData;
    }
}

/** 请求信息 */
const RequestInfo: InterfaceInfo[] = [{
    interfaceName: "朗新保供药品统计",
    modelPath: "/sdi/data/tables/etl/数据推送/朗新/朗新保供系统/DW_LX_BG_YPTJ.tbl",
    logTableName: "DW_LX_BG_YPTJ_LOG",
	logTablePath: "/xyfl/data/tables/FACT/ODS_FACT_FX_DFSCXX_LOG.tbl",
    requestWay: "增量",
    timeColumName: "N_TIMESTAMP",
    businessCode: "WZTB001",
    testRequestAddress: "http://2.21.1.29/janus01core/data/integration/inventory/report",
    formatRequestAddress: "http://2.21.1.29/janus01core/data/integration/inventory/report",
    paramFieldNames: [
        "enterpriseName", "enterpriseCreditCode", "reportDate", "siteName", "siteStatus", "siteCreditCode", "siteCode", "goodsCategory", "inventory", "transInventory",
        "yesterdaySales", "arriveInventory", "avgPrice"
    ]
}]

/** 接口信息 */
interface InterfaceInfo {
    /** 接口描述 */
    interfaceName: string;
    /** 请求方式 */
    requestWay?: string;
    /** 模型路径 */
    modelPath?: string;
    /** 物理字段名 */
    physicalTableName?: string;
    /** 日志表 */
    logTableName?: string;
    /** 日志表模型路径 */
    logTablePath?: string;
    /** 时间字段名 */
    timeColumName?: string;
    /** 测试地址 */
    requestTestAddress?: string;
    /** 测试地址短路经 */
    requestTestAddressShortPath?: string;
    /** 第三方提供的appId序号 */
    appId?: string;
    /** 参数对应的驼峰字段名 */
    paramFieldNames?: string[],
    /** 主键集 */
    primaryKeys?: string[],
    /** 业务代码 */
    businessCode?: string;
}

/** 测试环境授权码 */
enum TEST_ENVIRONMENT_CODE {
    /** 密钥 */
    AppKey = "279dcdcf-b33f-42a6-8480-36fb84bbbd97",
    /** 密钥 */
    AppSecret = "72b9feb0d74511ecbffd2fb4974324fb"
}

/** 正式环境授权码 */
enum FORMAT_ENVIRONMENT_CODE {
    /** 密钥 */
    AppKey = "cf9637dd-21a5-4a62-9057-5ac66c540785",
    /** 密钥 */
    AppSecret = "8ca14a50de6011ecb31a6d9fdcd16fae"
}

import Base64 = org.apache.commons.codec.binary.Base64;
import SecretKeySpec = javax.crypto.spec.SecretKeySpec;
import IvParameterSpec = javax.crypto.spec.IvParameterSpec;
import JavaString = java.lang.String;
import Cipher = javax.crypto.Cipher;
import StandardCharsets = java.nio.charset.StandardCharsets;
import UUID = java.util.UUID;
import Security = java.security.Security;
import BouncyCastleProvider = org.bouncycastle.jce.provider.BouncyCastleProvider;
const System = java.lang.System;
/**
 * Token生成
 * 
 * 异常：
 * 1  Cannot find any provider supporting AES/CBC/PKCS7Padding
 * 原因：在java中用aes256进行加密，但是发现java里面不能使用PKCS7Padding，而java中自带的是PKCS5Padding填充，那解决办法是，通过BouncyCastle组件来让java里面支持PKCS7Padding填充
 * 
 */
class Token {
    /**  */
    public IV: JavaString = "c624ee80fbdf11eb";
    /**  */
    public CIPHER: JavaString = "AES/CBC/PKCS7Padding";
    /**  */
    public ALG: JavaString = "aes256";
    /** 分隔符 */
    public SEPARATOR: JavaString = ":";

    /** 关键 */
    public appKey: JavaString;
    /** 密钥 */
    public appSecret: JavaString;

    /** 生成时间 */
    public generatorTime: number;
    /** 有效时间 */
    public effectiveTime: number;
    /** 生成的token值 */
    public baseToken: JavaString;

    public constructor(appKey: JavaString, appSecret: JavaString, effectiveTime: number) {
        this.appKey = appKey;
        this.appSecret = appSecret;
        this.effectiveTime = effectiveTime;
        this.generatorTime = 0;
        this.baseToken = "";
        this.init();
    }

    public init() {
        if (Security.getProvider(BouncyCastleProvider.PROVIDER_NAME) == null) {
            Security.addProvider(new BouncyCastleProvider());
        }
    }

    public createToken(): void {
        let tokenHeader: JavaString = this.createTokenHeader();
        let tokenPayload: JavaString = this.createTokenPayLoad();
        let token: JavaString = `${tokenHeader}${this.SEPARATOR}${tokenPayload}`;
        let baseToken: JavaString = Base64.encodeBase64String(token.getBytes());
        this.generatorTime = Date.now();
        this.baseToken = baseToken;
    }

    /**
     * 获取生成的token值
     */
    public getTokenValue(): JavaString {
        if (!this.baseToken) {
            return this.createToken();
        }
        return this.baseToken;
    }

    /**
     * 校验是否token是否有效
     * @return true | false
     */
    public checkTokenIsEffective(): boolean {
        if (this.generatorTime == 0) {
            return false;
        }
        let reduceTime: number = this.effectiveTime / 3;
        if (Date.now() - this.generatorTime >= this.effectiveTime - reduceTime) { // 避免时差问题，这里缩短原三分之一时间
            print(`token已失效，重新生成`);
            return false;
        }
        return true;
    }


    /**
     * 创建token请求头
     */
    private createTokenHeader(): JavaString {
        return `${this.appKey}${this.SEPARATOR}${this.ALG}`;
    }

    /**
     * 生成token明文
     * 格式：明文格式 appKey:timestamp:nonce
     * @param appKey 
     * @param appSecret 
     * return 加密后的明文
     */
    private createTokenPayLoad(): JavaString {
        let now: number = System.currentTimeMillis();
        let uuid = UUID.randomUUID().toString();
        let payLoad: JavaString = `${this.appKey}${this.SEPARATOR}${now}${this.SEPARATOR}${uuid}`;
        print("加密前：" + payLoad)
        return this.encryptBase64(payLoad);
    }

    /**
     * token加密
     * @param content 加密内容
     * @return 
     */
    private encryptBase64(content: JavaString): JavaString {
        let base64: JavaString = "";
        try {
            let secretKeySepc: SecretKeySpec = new SecretKeySpec(this.appSecret.getBytes(), this.ALG);
            let ivParameterSpec: IvParameterSpec = new IvParameterSpec(this.IV.getBytes());
            let cipher: Cipher = Cipher.getInstance(this.CIPHER);
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySepc, ivParameterSpec);
            let entryContent: number[] = cipher.doFinal(content.getBytes(StandardCharsets.UTF_8));
            base64 = Base64.encodeBase64String(entryContent);
        } catch (e) {
            print(`encryptBase64()，token生成失败，加密失败`);
            print(e);
        }
        print(`当前加密的结果：${base64}`);
        return base64;
    }
}