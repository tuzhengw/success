

import { queryDwTablesState, queryLabelLibs, saveLabelLib, updateDwTableDatas, getDwTableDataManager } from 'dw/dwapi';


function saveLables(): void {
	let idList: string[] = [];  // 模型Resid数组
	getChoseModelId(items, idList);
	let newLib: DwDataLabelLib = { option: {} };
	newLib.code = libCode;
	newLib.option.shareToSliceTables = idList;
	saveLabelLib(newLib).then(result => {
		updateDwTableDatas({ // 更新标准定义表对应的行：共享模型UUID
			"/sysdata/data/tables/dw/DW_DATA_LABELS_LIB.tbl": {
				modifyRows: [{
					fieldNames: ['SHARED_MODEL_RESID'],
					rows: [{
						keys: [libCode],
						row: [idList.join(',')]
					}]
				}]
			}
		});
		if (idList.length >= shareLabel.length) {
			let addInstTable = [];
			let insttableMap = result.shareInstTables;
			idList.forEach(l => {
				if (shareLabel.indexOf(l) == -1) {
					addInstTable.push(insttableMap[l]);
				}
			});
			addInstTable.length > 0 && rc({ // 复制选择的实例表数据
				url: getAppPath("/API/data-label/dataLabelMgr.action?method=copyDataToInstTable"),
				data: {
					src: instTable,
					dests: addInstTable
				}
			})
		}
		let updateCachePath: DwDataChangeEvent[] = [{ // 刷新标签模型表
			path: `/sysdata/data/tables/sdi/标签库脚本模型.tbl`,
			type: DataChangeType.refreshall
		}]
		getDwTableDataManager().updateCache(updateCachePath);
	});
}


/**
 * 获取一个文件下所有模型表
 * @param items 当前勾选的目录路径数组
 * @param idList 记录当前选中的模型ID
 */
function getChoseModelId(items, idList: string[]): void {
	items.forEach(item => {
		if (item.isFolder && item?.children?.length > 0) {
			let children = item.children;
			getChoseModelId(children, idList);
		} else if (!item.isFolder) {
			let id: string = item.id;
			idList.push(id);
		}
	})
}


/**
 * 复制实例表数据（后端方法）
 */
export function copyDataToInstTable(request: HttpServletRequest, response: HttpServletResponse, params: any) {
	let srcTableId = params.src;
	let destTableIds = params.dests;
	let srcTable = meta.getJSON(srcTableId);
	destTableIds.forEach(destTableId => {
		let destTable = meta.getJSON(destTableId);
		let args: any = {};
		args.srcDataSource = srcTable.properties.datasource;
		args.destDataSource = destTable.properties.datasource;
		args.async = false;
		args.importMode = 2;
		args.createTargetTable = false;
		args.dropTargetTable = true;
		args.tables = [{
			tableName: srcTable.properties.dbTableName, targetTableName: destTable.properties.dbTableName
		}];
		etl.copyTables(args);
	})
	return { success: true }
}