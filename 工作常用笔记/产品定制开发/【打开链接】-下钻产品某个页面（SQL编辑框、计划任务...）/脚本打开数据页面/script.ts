


/**
 * 20220622 tuzw
 * 打开数据加工目录
 * 问题：考虑到项目存在历史数据，导致对应的数据没有创建数据加工目录，从而打开链接报错，改为打开数据加工目录前，先进行校验目录是否存在
 * @param dataProcessDirName 当前数据加工的目录名, eg: sjzt_test.testbi-gj_datasource.testbi_dw
 */
private button_ac_openDataProcessDir(event: InterActionEvent): void {
	let params = event?.params;
	let dataProcessDirName: string = params?.dataProcessDirName;
	if (isEmpty(dataProcessDirName)) {
		showWarningMessage("未获取当前数据源的数据加工目录名");
		return;
	}
	let dataProcessDirPath: string = `/sdi/data/tables/sys/data-access/${dataProcessDirName}`;
	rc({
		url: '/zt/dataAccess/checkDataProcessDirIsExist',
		method: "POST",
		data: {
			filePath: dataProcessDirPath
		}
	}).then((result) => {
		if (!result.result) {
			showWarningMessage(`请检查数据归集生成数据加工目录是否创建成功`);
			return;
		}
		let domFileViewer = document.getElementsByClassName('metafileviewer-base'); // 门户
		let fileViewer: MetaFileViewer = domFileViewer[1].szobject as MetaFileViewer;
		fileViewer.showDrillPage({
			url: {
				path: `/sysdata/app/zt.app/commons/biBuiltInPages.html?type=data&projectName=sdi&urlParams=:locate=${dataProcessDirPath}`
			},
			title: '数据源加工目录',
			target: ActionDisplayType.Container,
			breadcrumb: true
		});
	});
}