import { Button, Combobox, Edit, Link, Checkbox } from "commons/basic";
import { Dialog } from "commons/dialog";
import { getCurrentUser, getMetaRepository, IMetaFileViewer, getFileImplClass } from "metadata/metadata";
import { showResourceDialog } from "metadata/metamgr";
import { getScheduleMgr, ScheduleEditor } from "schedule/schedulemgr";
import { assign, Component, ComponentArgs, ctx, rc1, ctxIf, getBaseName, IImportComponentInfo, isEmpty, message, rc, showDialog, showErrorMessage, SZEvent, showSuccessMessage } from "sys/sys";
import { IFieldsDataProvider } from "commons/exp/expdialog";
import { IExpDataProvider, ExpVar } from "commons/exp/expcompiler";

/**
* 存放通用的函数、类
*
* @author xh
* @createdate 20190421
*/


//存放要修改的类，key是模块名，比如“app”；值是一个map，map的key是类的名字，值是一个类
var modifyClazzes = new Map<string, Map<string, any>>();

/**
 * 修改类，功能类似314的modifyPrototype
 * @example
 *  modify("dw/dwmgr", "LeftTreeView", LeftTreeViewExt);
 * @export
 * @param {string} moduleName 要修改的类所在的模块的名字
 * @param {string} clazzName 要修改的类的类的名称，注意是字符串
 * @param {*} extClazz 要扩展的类，extClazz的所有方法都会被拷贝到clazzName所指定的类上去。extClazz本身没有什么用处，只是为了方便管理要修改的方法，它不会被new。
 * @returns {void}
 */
export function modifyClass(moduleName: string, clazzName: string, extClazz: any): void {
    function registModify(moduleName: string, className: string, extClazz: any): void {
        var module = modifyClazzes[moduleName];
        if (!module) {
            module = modifyClazzes[moduleName] = {};
        }

        if (module[className]) {
            return;
            //throw new Error("同一个类不允许调用modifyClass修改2次。moduleName为：" + moduleName + "，className为：" + className);
        }
        module[className] = extClazz;
    }

    var defined = requirejs["s"].contexts["_"].defined;
    if (defined) {
        var originModule = defined[moduleName];
        if (originModule) { //要修改的模块已经加载过了
            // clazzName 会有打点的，类似：EditorPanelImpls.modelFields.ModelFieldsDTableDataProvider
            clazzName.split('.').forEach(d => {
                originModule = originModule[d];
            });
            var originClazz = originModule;
            if (originClazz) {
                /**
                 * 20190511 xh
                 * 	打开ZT首页时报错：在让modifyClass中的修改生效时出现错误：【metadata/metamgr】中不存在名称为【MetaFilesBook】的类！请检查类的名字是否正确。
                 *  原因：metamgr还没有加载完毕，但是defined里面已经有metadata/metamgr了
                 * 	解决办法：defined中找不到要修改的类时不抛异常
                 */
                //throw new Error("在让modifyClass中的修改生效时出现错误：【" + moduleName + "】中不存在名称为【" + clazzName + "】的类！请检查类的名字是否正确。");
                applyClassModify(originClazz, extClazz);
                return;
            }
        }
    }

    registModify(moduleName, clazzName, extClazz);
}

//存放要修改的模块，key是模块名，比如“app”；值是一个function数组
var modifyModules = new Map<string, Array<Function>>();

/**
 * 修改模块，主要是将模块中的方法改成新的实现
 * @example
 *  modifyModule("dw/dwdialog", function(dwdialog){
 *      var showDbTableImportDialog_old = dwdialog.showDbTableImportDialog; //先把老的方法保存下来
 *      dwdialog.showDbTableImportDialog = function(){ //改成新的实现
 *      }
 *  });
 *
 * @export
 * @param {string} moduleName
 * @param {Function} modifyFunc
 * @returns {void}
 */
export function modifyModule(moduleName: string, modifyFunc: Function): void {
    function registModify(moduleName: string, modifyFunc: Function): void {
        var module = modifyModules[moduleName];
        if (!module) {
            module = modifyModules[moduleName] = [];
        }

        module.push(modifyFunc);
    }

    var defined = requirejs["s"].contexts["_"].defined;
    if (defined) {
        var originModule = defined[moduleName];
        if (originModule) { //要修改的模块已经加载过了
            modifyFunc(originModule);
            return;
        }
    }

    registModify(moduleName, modifyFunc);
}

/**
 * requirejs的onResourceLoad在每个模块加载完毕之后执行。在这个事件里面把对模块的修改应用到模块上去
 */
requirejs['onResourceLoad'] = function (context: any, map: any) {
    var moduleName = map.id;
    //console.log("onResourceLoad:", moduleName);
    var module: any = context.registry[moduleName];
    if (module) {
        var exports = module.exports;

        applyModuleModify(exports, moduleName);
    }
}

/**
 * 应用模块的修改
 *
 * @param {*} originModule
 * @param {*} moduleExt
 * @param {string} moduleName
 */
function applyModuleModify(originModule: any, moduleName: string) {
    let moduleModify = modifyClazzes[moduleName];
    if (moduleModify) {
        for (let className in moduleModify) {
            var extClazz = moduleModify[className];
            var originClazz = originModule[className];
            if (!originClazz) {
                throw new Error("在让modifyClass中的修改生效时出现错误：【" + moduleName + "】中不存在名称为【" + className + "】的类！请检查类的名字是否正确。");
            }
            applyClassModify(originClazz, extClazz);
            extClazz.requireModule = originModule; //将requirejs的module记录在扩展类上，通过_module可以引用模块里面的其它export对象
        }
    }

    var moduleModify2 = modifyModules[moduleName];
    if (moduleModify2) {
        for (let i = 0, len = moduleModify2.length; i < len; i++) {
            moduleModify2[i].apply(this, [originModule]);
        }
    }
}

/**
 * 应用类的修改。将extClazz类的所有prototype方法拷贝到originClazz类上去，同名的将覆盖，不同面的将追加。
 * 说明：对于被覆盖的方法，通过方法名+"_old"后缀可以获取到它。
 *
 * @param {*} originClazz
 * @param {*} extClazz
 */
function applyClassModify(originClazz: any, extClazz: any) {
    var extPrototype = extClazz.prototype;
    var originPrototype = originClazz.prototype;
    var funcNames = Object.getOwnPropertyNames(extPrototype);
    for (let i = 0, len = funcNames.length; i < len; i++) {
        var funcName = funcNames[i];
        if (funcName === 'constructor') {
            continue;
        }

        let func = extPrototype[funcName];

        var oldFunc = originPrototype[funcName];
        if (oldFunc) {
            //被覆盖的方法也是有用的，这里将它保存起来，方法名就是原方法名+"_old"后缀
            extPrototype[funcName + "_old"] = oldFunc;
        }

        originPrototype[funcName] = func;
    }
}

//公布成window对象，方便调试
window['ztModuleModify'] = { 'module': modifyModules, 'class': modifyClazzes };
/**
 * 确保只重载一次Class
 * @param moduleName
 * @param clazzName
 * @param extClazz
 */
export function ensureModifyClass(moduleName: string, clazzName: string, extClazz: any) {
    let _module = modifyClazzes[moduleName];
    let _class = _module && _module[clazzName]
    if (!_class) {// 避免多次重载
        modifyClass(moduleName, clazzName, extClazz);
    }

}
/**
 * 获取ZT应用中js、css资源的url地址。在import的时候需要
 * 参数relativeUrl的写法：
 * 1、meta-data/meta-data-mgr.js
 *
 * 20200423 xh:这里返回的url不要加contextpath。import资源的地方会自动加contextpath。
 * @param relativeUrl
 */
export function getZTResourceUrl(relativeUrl: string): string {
    return ("/HBAGL/app/BAZK.app" + relativeUrl);
}

export function getAppPath(relativeUrl: string): string {
    return ("/HBAGL/app/BAZK.app" + relativeUrl);
}

const doc = document;

export function getEmptyHtml(text?: string): HTMLElement {
    let domBase = doc.createElement('div');
    domBase.classList.add('emptypage-base');
    let domImg = doc.createElement('div');
    domImg.classList.add('emptypage-img');
    domBase.appendChild(domImg);
    let domText = doc.createElement('span');
    domText.classList.add('emptypage-text');
    domText.textContent = text || "无数据";
    domBase.appendChild(domText);
    return domBase;
}

/**
 * 获取文件类型中文名
 * @param info
 */
export function getFileTypeName(info: MetaFileInfo): string {
    let type = info.type;
    let subFileType = info.subFileType;
    let customOption = info.customOption;
    let isTableFlow = customOption && customOption.isTableFlow;
    let name = "";
    switch (type) {
        case MetaFileType.TBL:
            if (subFileType == MetaSubFileType.DATAFLOW) {
                if (isTableFlow == true) {
                    name = "简易加工";
                } else {
                    name = "数据加工";
                }
            } else if (subFileType == MetaSubFileType.ODS) {
                name = "Ods模型";
            } else if (subFileType == MetaSubFileType.EASYQUERY) {
                name = "灵活查询";
            } else if (subFileType == MetaSubFileType.SQL) {
                name = "SQL查询";
            } else {
                name = "App模型";
            }
            break;
        case MetaFileType.DASH:
            name = "仪表板";
            break;
        case MetaFileType.RPT:
            name = "报表";
            break;
        case MetaFileType.APP:
            name = "应用";
            break;
        case MetaFileType.FAPP:
            name = "表单";
            break;
        case MetaFileType.FOLD:
            name = "文件夹";
            break;
        case MetaFileType.HTML:
            name = "HTML页面";
            break;
        case MetaFileType.JSON:
        case MetaFileType.LESS:
        case MetaFileType.SPG:
        case MetaFileType.MPG:
        case MetaFileType.TPG:
            name = `${type}文件`;
            break;
        case MetaFileType.TS:
            name = "前端脚本";
            break;
        case MetaFileType.ACTION:
        case MetaFileType.ACTION_TS:
            name = "后端脚本";
            break;
        default:
            name = "未知文件";
            break;
    }
    return name;
}

/**
 * 判断是否是系统管理员
 */
export function isSysAdmin(): Promise<boolean> {
    let currentUser = getCurrentUser();
    return Promise.resolve(currentUser.isAdmin);
}

/**
 * 返回实现类对象, 从4.0拷贝一份，避免ZT使用相对路径总是找不到
 * @private
 * @param args
 * @param args.depends 类依赖的js。
 * @param args.implClass 类对象或者类的名称。
 * @return
 */
export function getImplClass<T>(args: IImportComponentInfo<T>): Promise<Constructable<T>> {
    let depends = args.depends;
    let implClass = args.implClass;
    if (typeof implClass === 'string' && depends) {
        return import(ctxIf(depends)).then((m: JSONObject) => m[<string>implClass]);
    } else if (implClass) {
        return Promise.resolve(<Constructable<T>>implClass);
    } else {
        throw new Error("找不到实现类：" + JSON.stringify(args));
    }
}

export function checkMetaFile(args: any): Promise<any> {
    return getMetaRepository().getFile(args.file, false, false, false).then((file: any) => {
        if (file) {
            showErrorMessage(args.message, 10);
            return Promise.resolve(file);
        }
        return Promise.resolve();
    });
}

/**
 * 检查字符长度,汉字算2个
 * @param args
 * @param {string} args.value 要判断的字符串
 * @param {number} args.maxLen 最大长度
 * @param {number} args.minLen 最小长度
 */
export function checkLength(args: { value: string, maxLen: number, minLen: number }): boolean {
    let length = args.value.replace(/[^\x00-\xff]/g, "xx").length;
    if (length >= args.minLen && length <= args.maxLen) {
        return true;
    }
    return false;
}

/**
 * 获取元数据名称在列表中的HTML
 * @param info
 * @param checkBoxVisible
 */
export function getNameHtml(info: MetaFileInfo): string {
    return `<span title="${info.path}" class="item-caption dividerlarge">${getBaseName(info.name)}</span>`
}

class SDI_MetaFilesBookFileInfo {
    /**
     * 是否是表加工
     */
    isTableFlow: boolean = false;

    newFile_old: any;
    open_old: any;
    show_old: any;
    fetchViewer_old: any;

    public newFile(name: string, parentDir: string, extra?: string): Promise<IMetaFileViewer> {
        if (extra && extra === "TableFlow") {
            this.isTableFlow = true;
            //extra = "DataFlow";// 不把子类型改回DataFlow，便于Url定位
        } else {
            this.isTableFlow = false;
        }
        return SDI_MetaFilesBookFileInfo.prototype.newFile_old.apply(this, [name, parentDir, extra]);
    }

    public open(file: FileInfoOrPathOrId, params?: JSONObject): Promise<IMetaFileViewer> {
        return getMetaRepository().getFile(file).then(f => {
            this.isTableFlow = f.customOption && f.customOption.isTableFlow;
            return SDI_MetaFilesBookFileInfo.prototype.open_old.apply(this, arguments);
        });
    }

    public show(): Promise<IMetaFileViewer> {
        return SDI_MetaFilesBookFileInfo.prototype.show_old.apply(this, arguments);
    }

    private fetchViewer(): Promise<IMetaFileViewer> {
        if (this['viewer']) {
            return Promise.resolve(this['viewer']); //打开过，且查看器还在，还未被回收
        }

        /**
         * 每个可以复用的viewer至少创建2个instance，这样便于2个文件快速切换对比时更流畅些
         */
        let type = this['type'];
        let owner = this['owner'];
        let filesBook = owner.filesBook;

        let viewer: IMetaFileViewer = <IMetaFileViewer>filesBook.getComponents().find((v: IMetaFileViewer) => v.isClosed() && v.isRecyclable() && v.getViewerType() === `${(type === "tbl" && this.isTableFlow) ? "tbl@TableFlow" : type}`);
        if (viewer) {
            // 先复用之前被关闭的viewer
            viewer.onmodifiedchange = this['setModified'].bind(this);
            return Promise.resolve(this['viewer'] = viewer);
        }

        let findFirstFileInfo = () => {
            let minTime = Date.now();
            let ret = null;
            let activeViewer = filesBook.getActiveComponent()
            let filesInfo = owner.filesInfo;
            for (let id in filesInfo) {
                let info = filesInfo[id];
                // 解决复用问题，还需保证isTableFlow一致
                if (info && info.type === type && info.isTableFlow === this.isTableFlow && info.viewer && info.lastActiveTime < minTime && info.viewer.isRecyclable() && info.viewer !== activeViewer) {
                    minTime = info.lastActiveTime;
                    ret = info;
                }
            }
            return ret;
        }
        let firstFileInfo = findFirstFileInfo();
        if (firstFileInfo) {
            let viewer = firstFileInfo.releaseViewer();
            viewer.onmodifiedchange = this['setModified'].bind(this);
            return Promise.resolve(this['viewer'] = viewer);
        }

        let promise: Promise<IMetaFileViewer>;
        if (type === "tbl" && this.isTableFlow) {
            promise = getImplClass({
                depends: "./tableflow.js",
                implClass: "TableFlow_DwTableEditor"
            }).then((TableFlowViewerImplClass: Constructable<any>) => {
                let viewer = new TableFlowViewerImplClass({
                    commandRenderer: owner.commandRenderer,
                    pageContainer: owner.pageContainer,
                    onmodifiedchange: this['setModified'].bind(this)
                });
                filesBook.addComponent(viewer);
                this['viewer'] = viewer;
                return viewer;
            })
        }
        else {
            promise = getFileImplClass(type, "viewer").then((FileViewerImplClass: Constructable<IMetaFileViewer>) => {
                let viewer = new FileViewerImplClass({
                    commandRenderer: owner.commandRenderer,
                    pageContainer: owner.pageContainer,
                    onmodifiedchange: this['setModified'].bind(this)
                });
                filesBook.addComponent(viewer);
                this['viewer'] = viewer;
                return viewer;
            });
        }

        return promise;
    }
}

modifyClass("metadata/metamgr", "MetaFilesBookFileInfo", SDI_MetaFilesBookFileInfo);

export interface CustomJs_List_Head {
    /** 列id */
    id: string;
    /** 列标题名称，如果不填写默认为id */
    caption?: string;
    /** 是否隐藏列 */
    visible?: boolean;
    /** 控件类型，如果不填写则默认为输入框控件，目前支持：combobox（多选框），resselect */
    type?: string;
    /** 下拉框是否支持多选 */
    isMultipleSelect?: boolean,
    /** 下拉框选项（数组） */
    comboxArgs?: {
        value: string, caption: string
    }[],
    resArgs?: { id: string, caption?: string, rootPath?: string, returnTypes?: string[], enableEmptySelect?: boolean }
}

export interface CustomJs_ListArgs extends ComponentArgs {
    headerParams: CustomJs_List_Head[];//表头参数，最后的返回值也依赖这个值
    value?: string;
    /** 是否需要底部【新增、删除】功能 */
    isNeedAddDelFunction?: boolean;
    /** 是否需要初始一无数据行 */
    isNeedInitOneRow?: boolean;
}

/**
 * 实现一个通用的列表控件，可以编辑该列表每一行的数据，最后返回的格式为数组
 */
export class CustomJs_List extends Component {
    private headerParams: CustomJs_List_Head[];
    private value: JSONObject;
    private table: HTMLTableElement;
    private rows;
    private addRowButton: Button;
    private delRowButton: Button;
    constructor(args: CustomJs_ListArgs) {
        super(args);
    }
    protected _init(args: CustomJs_ListArgs) {
        let domBase = super._init(args);
        let table = this.table = document.createElement("table");
        /**
         * 20220214 tuzw
         * 		原写法：let isNeedInitOneRow = !!args.isNeedInitOneRow ? args.isNeedInitOneRow : true，
         * 会导致：若参数值为：false时，会将结果转换为：true，导致显示结果与预期效果相反
         */
        let isNeedInitOneRow: boolean = isEmpty(args.isNeedInitOneRow)
            ? true
            : !!args.isNeedInitOneRow;
        let isNeedAddDelFunction: boolean = isEmpty(args.isNeedAddDelFunction)
            ? true
            : !!args.isNeedAddDelFunction;

        if (args.className) {
            table.classList.add(args.className);
        } else {
            table.classList.add("customJs_list_table");
        }
        let rowValues = isEmpty(args.value) ? [] : args.value;
        if (typeof args.value == 'string') {
            rowValues = JSON.parse(args.value);
        }
        let tbody = table.createTBody();
        let headRow = tbody.insertRow();
        headRow.classList.add("customJs_list_header");
        table.contentEditable = "true";
        headRow.contentEditable = "false";
        let headerParams = this.headerParams = args.headerParams;
        for (let i = 0; i < headerParams.length; i++) {
            const element = headerParams[i];
            let cell: HTMLTableDataCellElement = headRow.insertCell();
            cell.innerText = element.caption ? element.caption : element.id;
            cell.setAttribute("id", element.id);
            if (element.visible != undefined && element.visible != null && !element.visible) {
                cell.classList.add("list_colums_noDisplay");
            }
        }
        domBase.appendChild(table);
        this.rows = [];
        if (isEmpty(rowValues) && isNeedInitOneRow) {
            this.doAddRow();
        } else {
            rowValues.forEach(v => this.doAddRow(v));
        }
        if (isNeedAddDelFunction) {
            this.addRowButton = new Button({
                domParent: domBase,
                icon: 'icon-add',
                caption: "添加行",
                layoutTheme: 'smalldefbtn',
                className: 'customJs_list_addrow_button',
                onclick: this.doAddRow.bind(this)
            })
            this.delRowButton = new Button({
                domParent: domBase,
                icon: 'icon-deletedata',
                caption: "删除行",
                layoutTheme: 'smalldefbtn',
                className: 'customJs_list_delrow_button',
                onclick: this.doDelRow.bind(this)
            });
        }
        return domBase;
    }

    /**
     * 重新渲染列表数据
     * 20220311 tuzw
     * 问题: 
     * （1） 列表初次执行init()方法会自动往界面上添加一空白行
     * （2） _reInit()方法会先删除上一次渲染的数据，然后再根据当前最新的value值进行渲染列表
     * （3） 原逻辑是根据列表录入的数据删除，但若录入的某行数据为空，则不会被记录，导致根据录入的行进行删除时，会漏掉没有录入数据的行
     * 解决办法：
     * （1） 删除逻辑改为根据列表的实际行数来进行删除，根据DOM的行数删除，而不是实际录入不为空的数据行删除
     */
    public _reInit(args: any) {
        let len = this.table.rows.length;
        for (let i = 1; i < len + 1; i++) {
            this.doDelRow();
        }
        let value: JSONObject = isEmpty(args.value) ? [] : JSON.parse(args.value);
        let isNeedInitOneRow: boolean = isEmpty(args.isNeedInitOneRow)
            ? true
            : !!args.isNeedInitOneRow;
        if (isEmpty(value) && this.rows.length == 0 && isNeedInitOneRow) { // 若列表没有行，重新打开则初始化一行
            this.doAddRow();
        } else {
            for (let i = 0; i < value.length; i++) {
                this.doAddRow(value[i]);
            }
        }
    }

    /**
     * 增加行
     * @param rowValue 当前行数据，格式：{ 列号：值 }，eg：{ "1": "1001", "2" : "张三" }
     * @param headerParams 列标题数组，可包含当前列下拉框选项
     * 注意：
     * （1）不指定headerParams，当前列若设置下拉框，则【整列下拉框】以【标题列】设置的【下拉框选项】 为主
     * （2）指定headerParams，可指定【每行】下拉框选项值
     */
    public doAddRow(rowValue?: JSONObject, headerParams?: CustomJs_List_Head[]): void {
        let row = new CustomJs_Row({ table: this.table, value: rowValue, headerParams: this.headerParams });
        this.rows.push(row);
    }

    private doDelRow(): void {
        let tbody = this.table.tBodies[0];
        let rows = this.table.rows;
        if (rows.length > 1) { // 应该获取表的行数, 而不是获取实际数据的行数(this.rows)
            tbody.deleteRow(rows.length - 1);
            this.rows.remove(this.rows.length - 1);
        }
    }

    public getValue(): JSONObject {
        let value = [];
        let len = this.rows.length;
        for (let i = 0; i < len; i++) {
            const row = this.rows[i];
            let v = row.getValue();
            if (!isEmpty(v)) {
                value.push(v);
            }
        }
        return value;
    }

    public dispose() {
        super.dispose();
    }
}

export class CustomJs_Row {
    private table: HTMLTableElement;
    private headerParams: CustomJs_List_Head[];
    private value: JSONObject;
    private row: HTMLTableRowElement;
    /** 用于获取表行数据 */
    private comps: any[];
    constructor(args) {
        this.table = args.table;
        this.value = args.value;
        this.headerParams = args.headerParams;
        this.comps = [];
        this.init();
    }

    protected init() {
        let table = this.table;
        let tbody = table.tBodies[0];
        let row = this.row = tbody.insertRow();
        let v = this.value;
        for (let i = 0; i < this.headerParams.length; i++) {
            const element = this.headerParams[i];
            let cell = row.insertCell();
            if (element.visible != undefined && element.visible != null && !element.visible) {
                cell.classList.add("list_colums_noDisplay");
            }
            let id = element.id;
            let value = v && v[id]; // v： "rowUuid": "2489025822528952852..."
            cell.setAttribute("id", element.id);
            let type = element.type;
            switch (type) {
                case "combobox": // 多选框
                    cell.contentEditable = "false";
                    let comboboxDiv = document.createElement("div");
                    comboboxDiv.classList.add("customJs_list_combox");
                    cell.appendChild(comboboxDiv);
                    let items = element.comboxArgs;
                    let combobox = new Combobox({
                        multipleSelect: !!element.isMultipleSelect ? element.isMultipleSelect : false, // 多选
                        selectAllEnabled: !!element.isMultipleSelect ? element.isMultipleSelect : false, // 是否允许全选
                        checkedCountVisible: !!element.isMultipleSelect ? element.isMultipleSelect : false, // 是否显示选择数量
                        checkboxVisible: !!element.isMultipleSelect ? element.isMultipleSelect : false, // 是否在选项前显示勾选框
                        cleanIconVisible: !!element.isMultipleSelect ? element.isMultipleSelect : false, // 是否显示清空图标
                        noOptionText: '无选项'
						id: id,
                        type: "list",
                        items: items,
                        domParent: comboboxDiv
                    });
                    this.comps.push(combobox);
                    !isEmpty(value) && combobox.setValue(value);
                    break;
                case "resSelect":
                    cell.contentEditable = "false";
                    let resSelectDiv = document.createElement("div");
                    resSelectDiv.classList.add("customJs_list_resSelect");
                    cell.appendChild(resSelectDiv);
                    let resArgs = element.resArgs;
                    let resInput = new Edit({
                        id: id,
                        value,
                        className: "customJs_list_input",
                        domParent: resSelectDiv,
                        readOnly: true
                    })
                    let link = new Link({
                        domParent: resSelectDiv,
                        className: 'customJs_list_link',
                        caption: '选择',
                        onclick: function () {
                            showResourceDialog(assign({
                                onok: function (evnet: SZEvent, compoent?: Component, items?: any) {
                                    resInput.setValue(items.length > 0 ? items[0].id : null);
                                }
                            }, resArgs))
                        }
                    });
                    this.comps.push(resInput);
                    break;
                case "checkbox": // 单项框
                    cell.contentEditable = "false";
                    let checkbox = new Checkbox({
                        domParent: cell,
                        id: id, // 注意给控件添加ID值，用于getValue
                        checkedValue: 1, // 勾选状态的值
                        uncheckedValue: 0,
                        checked: true
                    });
                    this.comps.push(checkbox);
                    !isEmpty(value) && checkbox.setValue(value);
                    break;
                default:
                    !isEmpty(value) && (cell.innerText = value);
                    this.comps.push(cell);
                    break;
            }
        }
    }

    public getValue(): JSONObject {
        let comps = this.comps;
        let value = {};
        for (let i = 0; i < comps.length; i++) {
            const element = comps[i];
            let id = element.id;
            let v;
            /**
             * 20220124 tuzw
             * 若普通组件value为""，则会走处理对象的value，导致报错
             */
            if (element.innerText || element.innerText == "") {
                v = element.innerText;
            } else {
                v = element.getValue();
            }
            !isEmpty(v) && (value[id] = v);
        }
        return value;
    }
}

/** 当前选中的计划任务名 */
let currentChoseScheduleName: string = "";
/** 记录当前计划列表--所有的计划名 */
let scheduleNames: Array<string> = [];
/**
 * 20211115 liuyz
 * https://jira.succez.com/browse/CSTM-16904
 * 计划任务的设置界面支持一个通用的方法
 * @param value 当前选择的计划对象 { id，name，createTime,... }
 * 
 * 20220214 tuzw
 * （1）计划任务存在新增/修改操作，增加isEidt来判断当前操作
 * （2）考虑到编辑可能仅编辑部分栏，增加isDisplayDetail属性，来校验是否需要隐藏详细栏
 * @param isEdit 是否编辑计划任务
 * @param isDisplayDetail 是否需要隐藏部分输入栏
 */
export function showScheduleDialog(value, isEdit?: boolean, isDisplayDetail?: boolean): Promise<any> {
    return querySchedules().then((allScheduleWorkResult: ScheduleInfo[]) => {
        allScheduleWorkResult.forEach(scheduleItem => {
            // 存储所有所选计划以外的所有计划名
            if (value.id != scheduleItem.id) {
                scheduleNames.push(scheduleItem["name"]);
            }
        });
        // 计划任务是否有效，重写 (value: string) => { }
        let valid = (schedulename: string) => {
            /** 当前编辑后的新计划任务名 */
            let currentEditSchedulename = schedulename.trim();
            if (!!isEdit) {
                if (currentEditSchedulename == currentChoseScheduleName) {
                    return true;
                }
            }
            if (scheduleNames.includes(currentEditSchedulename)) {
                return {
                    result: false,
                    errorMessage: message("schedulemgr.schedule.name.alreadyExistMsg")
                }
            };
        }
        let nameValidateArgs = {
            required: true,
            validRegex: valid
        }
        showDialog({
            id: !!isEdit
                ? "schedulemgr-dilaog-editchedule"
                : "schedulemgr-dilaog-newchedule",
            buttons: ["ok", "cancel"],
            needWaitInit: true,
            resizable: false,
            caption: !!isEdit
                ? "编辑计划"
                : "添加计划",
            content: {
                implClass: ScheduleEditor,
                nameValidateArgs: nameValidateArgs
            },
            onshow: (e: SZEvent, dialog: Dialog) => {
                let editor = <ScheduleEditor>dialog.content;
                editor.form.getFormItem("type").setVisible(false);
                if (!!isEdit && !!isDisplayDetail) {
                    /**
                     * 编辑任务时，隐藏部分属性设置
                     * 帖子：https://jira.succez.com/browse/CSTM-17083
                     */
                    editor.form.getFormItem("enable").setVisible(false);
                    editor.form.getFormItem("name").setVisible(false);
                    editor.form.getFormItem("priority").setVisible(false);
                    editor.form.getFormItem("concurrency").setVisible(false);
                    editor.form.getFormItem("threadsCount").setVisible(false);
                    editor.form.getFormItem("threadsCount").setVisible(false);
                    editor.form.getFormItem("schedule").setVisible(false);
                    editor.form.getFormItem("enableClusterNodes").setVisible(false);
                    currentChoseScheduleName = value.name;
                }
                editor.setData(value);
            },
            onok: (e: SZEvent, dialog: Dialog) => {
                let editor = <ScheduleEditor>dialog.content;
                if (!editor.validate()) {
                    return false;
                };
                if (!!isEdit) {
                    dialog.waitingClose(getScheduleMgr().modifySchedule(value.id, (<ScheduleEditor>dialog.content).getData())).then(() => {
                        showSuccessMessage("修改成功");
                    });
                } else {
                    dialog.waitingClose(getScheduleMgr().addSchedule((<ScheduleEditor>dialog.content).getData())).then(() => {
                        showSuccessMessage("添加成功");
                    });
                }
                currentChoseScheduleName = "";
            }
        });
    });
}


/**
 * 查询计划
 * @param fromTime 返回某个时间点后有状态更新的计划列表
 * @param projectName 项目名
 */
function querySchedules(fromTime?: number, projectName?: string): Promise<ScheduleInfo[]> {
    return rc1({
        url: "/api/schedule/services/queryScheduleInfos",
        data: {
            projectName,
            fromTime
        }
    }, null, `/api/schedule/services/queryScheduleInfos?fromTime=${fromTime}&projectName=${projectName}`);
}