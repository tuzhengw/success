/**
 * 记录日志
 * 
 * @Args：参数对象
 * @resultInfo：返回结果集
 * @event：事件对象
 * @result：状态，true/false
 * @errorCode：错误码
 * @message：错误消息
 * @data：记录数据
 * @detailmessage：详情信息
 * @errorData：错误数据
 * @option2：其他操作
 * 
 * <pre>
*  recordLog({
        resultInfo: resultInfo, event: event,
        result: true,
        errorCode: "sqidIsNotExist",
        message: `申请不存在，请检查SQID：【${sqid}】是否正确`,
        option2: sqid + ""
    });
 * </pre>
 * 
 * 总结：这样记录并没有减少代码实际的行数，不采用
 */
function recordLog(Args: {
    /**
     * 返回结果集
     */
    resultInfo: ResultInfo,
    /**
     * 事件对象
     */
    event: ServerEvent,
    /**
     * 状态，true/false
     */
    result: string | boolean,
    /**
     * 错误码
     */
    errorCode?: string,
    /**
     * 错误消息
     */
    message?: string,
    /**
     * 记录数据
     */
    data?: any,
    /**
     * 详情信息
     */
    detailMessage?: any,
    /**
     * 错误数据
     */
    errorData?: any,
    /**
     * 其他操作
     */
    option2?: string
}): void {
    /**
     * 可选参数值不为undefined，则记录
     */
    Args.resultInfo.result = Args.result;
    Args.resultInfo.errorCode = Args.errorCode ? Args.errorCode : null;
    Args.resultInfo.message = Args.message ? Args.message : null;
    Args.resultInfo.data = Args.data ? Args.data : null;
    Args.resultInfo.detailMessage = Args.detailMessage ? Args.detailMessage : null;
    Args.resultInfo.errorData = Args.errorData ? Args.errorData : null;
    Args.event.option2 = Args.option2 ? Args.option2 : null;
    /**
     * 若成功状态，则默认：OK，则不更新状态
     */
    Args.result ? "" : Args.event.operationState = LOGLEVEL_FAIL;
    Args.event.detailInfo.result = Args.resultInfo;
}