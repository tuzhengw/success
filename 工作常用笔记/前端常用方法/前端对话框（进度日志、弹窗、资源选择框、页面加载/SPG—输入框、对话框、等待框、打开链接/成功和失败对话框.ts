import { ctx, SZEvent, Component, throwInfo, throwError, showMessage,
showConfirmDialog, showFormDialog, showMenu, 
isEmpty, deepAssign, uuid, message, quoteExpField, showDialog, 
showSuccessMessage,showErrorMessage,
showProgressDialog, rc, downloadFile, waitRender, 
showWaiting, assign, IDataProvider, formatDateFriendly, parseDate, formatDate, tryWait, 
rc_get, showWarningMessage, timerv, showWaitingMessage, hideMessage, DataChangeEvent,
 EventListenerManager, GroupInfo, IFindArgs, ServiceTaskPromise, ServiceTaskInfo, ServiceTaskState } from 'sys/sys';


/**创建一个目标物理表 */
button_create_targetDbTable: (event: InterActionEvent) => {
	let params = event.params;
	let sourceTable = params.sourceTable;
	let targetTable = params.targetTable;
	let targetTableDesc = params.targetTableDesc;
	let sourceDataBase = params.param2;
	let targetDataBase = params.param3;
	return rc({
		url: getAppPath('/data-access/dataAccessMgr.action?method=createDbTable'),
		data: {
			sourceTable: sourceTable,
			targetTable: targetTable,
			targetTableDesc: targetTableDesc,
			sourceDataBase: sourceDataBase,
			targetDataBase: targetDataBase
		}
	}).then(result => {
		if (result.result) {
			showSuccessMessage("创建成功");
			return true;
		} else {
			showErrorMessage(result.message);
			return false;
		}
	})