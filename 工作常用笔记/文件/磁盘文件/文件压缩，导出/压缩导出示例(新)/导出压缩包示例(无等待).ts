import db from "svr-api/db";
import dw from "svr-api/dw";
import http from "svr-api/http";
import metadata from "svr-api/metadata";
import utils from "svr-api/utils";
import sys from "svr-api/sys";

import Files_ = java.nio.file.Files;
import Paths_ = java.nio.file.Paths;
import JavaFile = java.io.File;
import ZipOutputStream = java.util.zip.ZipOutputStream;
import ZipEntry = java.util.zip.ZipEntry;
import FileOutputStream = java.io.FileOutputStream;
import FileInputStream = java.io.FileInputStream;
import IOUtils = org.apache.commons.io.IOUtils;
import JavaString = java.lang.String;
import BufferedWriter = java.io.BufferedWriter;
import FileWriter = java.io.FileWriter;


/**
 * 批量导出多个笔记文件
 * @param noteIds 导出笔记ID, eg: 001,002
 */
export function batchImportNoteFile(request: HttpServletRequest, response: HttpServletResponse, params: {
    noteIds: string
}): void {
    console.info(`开始执行[batchImportNoteFile], 批量导出多个笔记文件`);
    console.info(params);

    let noteId: string = params.noteIds;
    if (!noteId) {
        return;
    }
    let noteIds: string[] = noteId.split(",");
    let outPut = response.getOutputStream();

    let shareDir: string = sys.getClusterShareDir();
    let tempZipName: string = `noteFileZip_${utils.formatDate("yyyyMMddHHmmss")}`;
    let zipFilePath: string = `${shareDir}/file-storage/relativePath/${tempZipName}.zip`;
    console.info(`当前临时记录导出文件压缩包路径: ${zipFilePath}`);
    let zipFile: JavaFile = zipUtils.createZipFile(zipFilePath);

    let fileInputStream: FileInputStream = null;
    let zipOutputStream: ZipOutputStream = null;
    let noteFiles: NoteFileInfo[] = queryNoteFileInfos(noteIds);
    try {
        zipOutputStream = new ZipOutputStream(new FileOutputStream(zipFile)); // FileOutputStream 压缩包会自己关闭
        let ztAppEntry = new ZipEntry(`noteFiles/`);
        zipOutputStream.putNextEntry(ztAppEntry); // 将创建好的条目加入到压缩文件中
        zipOutputStream.closeEntry();

        for (let i = 0; i < noteFiles.length; i++) {
            let noteFile: NoteFileInfo = noteFiles[i];
            let fileName: string = noteFile.noteFileName;
            let fileContent: string = noteFile.noteContent;
            if (!fileName || fileName.length < 3) { // 文件名长度至少大于3
                fileName = `defaultPrx_${fileName}`;
            }
            let tempFile: JavaFile = zipUtils.createSignFile(fileName, "html", fileContent);
            fileInputStream = new FileInputStream(tempFile);

            let ztNoteFileEntry = new ZipEntry(`noteFiles/${fileName}.html`);
            zipOutputStream.putNextEntry(ztNoteFileEntry);
            IOUtils.copy(fileInputStream, zipOutputStream);

            zipOutputStream.closeEntry();
            console.info(`临时文件[${fileName}.html] 删除状态: ${tempFile.delete()}`);
        }

        let fileBytes = Files_.readAllBytes(Paths_.get(zipFile.getAbsolutePath()));
        response.setContentType("application/zip");
        response.addHeader("Content-Disposition", "attachment; filename=" + java.net.URLEncoder.encode(tempZipName, "UTF-8") + ".zip");
        response.addHeader("Content-transfer-Encoding", "binary");
        outPut.write(fileBytes);
        console.info(`批量导出多个笔记文件成功`);
    } catch (e) {
        console.error(`批量导出多个笔记文件执行失败----error`);
        console.error(e);
    } finally {
        fileInputStream && fileInputStream.close();
        zipOutputStream && zipOutputStream.close();
        outPut && outPut.close();
        console.info(`临时记录的压缩包文件, 删除状态: ${zipFile.delete()}`); // 删除临时压缩包
    }
}

