
import { IMetaFileViewer, MetaFileViewerArgs } from "metadata/metadata";
import { InterActionEvent, IMetaFileCustomJS } from 'metadata/metadata-script-api';
import { BaseTemplatePage } from "app/templatepage";
import { MobileFramePageManager } from "app/mobilepage";
import { rc, showProgressDialog, uuid, rc_task, SZEvent, showConfirmDialog, wait, isEmpty, showErrorMessage, Component, showWarningMessage } from 'sys/sys';
import { Dialog } from 'commons/dialog';
import { ProgressPanel } from 'commons/progress';
import { getDwTableDataManager, exportQuery } from "file://dw/dwapi";
import { AnaDataset } from 'ana/datamgr';

/**
 * 导出excel
 * @param listId 导出列表id值
 * @param fileName 导出文件名
 * @param exportFields 导出列表绑定的表 物理字段名，eg：xm,age
 */
exportListData: (event: InterActionEvent) => {
	let params = event.params;
	let listId: string = params.listId;
	let fileName: string = params.fileName;
	let exportFields: string = params.exportFields;
	let queryFields = [];
	let metaFile = event.renderer.getBuilder().getFileInfo();
	let fieldsList = exportFields && exportFields.split(",");
	if (!fieldsList || fieldsList.length == 0) {
		showWarningMessage("请选择需要导出的列名");
		return;
	}
	if (isEmpty(listId)) {
		showErrorMessage("导出按钮配置异常，请联系管理员！");
		return;
	}
	let listComp = event.page.getComponent(listId);
	let modelId = listComp.component.getCompBuilder().getDataset().getId();
	let dataset = <AnaDataset>event.page.getDataset(modelId);
	let allFields = dataset.getAllFields();
	let excelHeadNames: string[] = [];

	for (let i = 0; i < fieldsList.length; i++) { // 为了保持导出的列 与 列表顺序一致，这里循环校验
		let filed = fieldsList[i];
		for (let j = 0; j < allFields.length; j++) {
			let f = allFields[j];
			let name: string = f.getName();
			let dbField: string = f.getDbField();
			if (dbField == filed) { // 校验当前字段是否存在指定导出字段中
				queryFields.push({ name: dbField });
				excelHeadNames.push(name);
				break;
			}
		}
	}
	let resid = dataset.builder.getResid();
	let name = metaFile.name.replace(".spg", "");
	let source = dataset.getModel().getQuerySourceInfo();
	delete source.fields;
	let queryInfo: QueryInfo = {
		resid: resid,
		queryId: modelId + "_export",
		fields: queryFields,
		select: true,
		filter: dataset.getAllDynamicFilters(),
		drillFilter: dataset.getDrillFilter(),
		params: dataset.getQueryParamArray(),
		sort: dataset.getSort(),
		options: {
			needCodeDesc: !0,
			autoSort: !0,
			cache: false,
			limit: undefined,
		}
	};
	exportQuery({
		resid: resid,
		query: queryInfo,
		headNames: excelHeadNames,
		fileFormat: DataFormat.xlsx,
		fileName: fileName != null ? fileName : name
	} as ExportDataParams);
}