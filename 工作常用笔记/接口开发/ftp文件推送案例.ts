/**
 * 作者：刘永卓
 * 创建时间：2022-05-24
 * 脚本用途：读取数据库内的blob生成文件，并且推送到远程ftp服务器上
 * 相关帖子：https://jira.succez.com/browse/CSTM-18894
 */
import db from "svr-api/db";
import fs from "svr-api/fs";
import sys from "svr-api/sys";
import ftp from "svr-api/ftp";
import { FtpClient } from "svr-api/types/ftp.types";

/**
 * 执行方法入口
 */
function main() {
    let a = new UploadFileToFtpService();
    a.execute();

    // 查看推送服务器文件信息
    // a.watchFtpFile("/gj_spjy_pdf/2022-05-30");
    // a.closeFTPClient();
}

/** FTP配置项 */
const FTP_SETTING = {
    host: "",
    port: 12,
    username: "",
    password: ""
}

/**
 * 上传文件至ftp服务器类
 */
class UploadFileToFtpService {
    /** 手动控制程序运行文本地址 */
    public isStopRunScriptPath = "/ZHJG/script/sendPdfFile/isStopGetPdfData.txt";
    private dataSourceName: string = "SZ_TEST_VERTICA";
    /** 本地存储PDF文件根目录地址 */
    private localFolderPath: string;
    private ds: Datasource;
    private ftpClient: FtpClient;

    /** 查询文件创建成功的共享—国家—食品经营PDF文件信息 */
    public QUERY_SHARE_FOOD_INFO_SQL =
        `SELECT * FROM public.GX_GJ_SPJY_LOG t0 
            WHERE EXISTS (
                SELECT 1 FROM public.GX_GJ_SPJY_LOG t1
                    WHERE  t1.ACK_CODE ='SUCCESS' 
                    AND t0.AUTH_CODE = t1.AUTH_CODE 
                    AND t1.SEND_CODE = '0'
            )
            ORDER BY ISSU_DATE DESC`;

    constructor() {
        this.ds = db.getDataSource(this.dataSourceName);
        this.localFolderPath = `${sys.getWorkDir()}/gj_spjy_pdf_new/`
        this.connectFTPClient();
    }

    /**
     * 执行入口
     */
    public execute(): void {
        let pushDatas: UploadFileInfo[] = this.queryPushData();
        if (pushDatas.length == 0) {
            print(`当前没有需要推送的数据，结束执行。`);
            this.closeFTPClient();
            return;
        }
        let startTime: number = Date.now();
        print(`本次需要推送【${pushDatas.length}】条数据`);
        for (let i = 0; i < pushDatas.length; i++) {
            let isStop: string = metadata.getString(Is_StopRunScriptPath);
            if (isStop == "true") {
                print(`手动结束推送，已推送：${i + 1} 条数据。`);
                break;
            }
            if (i % 100 == 0) {
                print(`当前推送条数：${i + 1}`);
            }
            let data = pushDatas[i];
            let isSendSuccess: boolean = this.createFileAndUpload(data);
            if (isSendSuccess) {
                this.updatelogInfo(data.fileName);
            }
        }
        this.closeFTPClient();
        print(`本次推送完成，总耗时：${(Date.now() - startTime) / 1000} s`);
    }

    /**
     * 查看工作目录下的文件信息
     * @param folder 文件夹名
     */
    public watchWorkDirFile(folder?: string): void {
        let path = this.localFolderPath;
        if (folder != null) {
            path = `${path}${folder}`;
        }
        let file = fs.getFile(path);
        print(`服务器上：${path} 下文件夹和文件：`);
        print(file.listDirs())
        print(file.listFiles());
    }

    /**
     * 查看服务器上指定目录文件
     * @param path 查看路径
     */
    public watchFtpFile(path: string): void {
        print(`ftp服务器上:${path} 目录下文件：`);
        let file = this.ftpClient.list(path);
        print(file);
    }

    /**
     * 创建文件，并且将文件上传
     * @param data 推送信息
     */
    private createFileAndUpload(data: UploadFileInfo): boolean {
        let isSendSuccess: boolean = true;
        try {
            if (this.isSend(`${data.targetPath}/${data.fileName}`)) {
                print(`文件：${data.fileName} 已推送过，忽略`);
                return isSendSuccess;
            }
            this.ftpClient.put(data.targetPath, data.filePath); // 将当前服务器的文件写入到目标服务器位置
        } catch (e) {
            isSendSuccess = false;
            print(`推送异常。`);
            print(e);
        }
        return isSendSuccess;
    }

    /**
     * 校验当前文件是否推送过
     * @param filePath 校验文件的路径，eg："/gj_spjy_pdf/test.pdf"
     * @return true | false
     */
    private isSend(filePath: string): boolean {
        let isSend: boolean = false;
        try {
            let file = this.ftpClient.get(filePath);
            if (file.exists()) {
                isSend = true;
            }
        } catch (e) { // 若文件不存在，get则会报错，根据异常来校验文件是否推送过
            isSend = false;
        }
        return isSend;
    }

    /**
     * 链接FTP客户端
     */
    private connectFTPClient(): void {
        this.ftpClient = ftp.connectFTP(FTP_SETTING);
    }

    /**
     * 关闭FTP客户端链接
     */
    public closeFTPClient(): void {
        print(`ftp链接已关闭`);
        this.ftpClient.close();
    }

    /**
     * 查询以及成功推送的数据信息 
     * 
     * todo 本地文件路径需要更改动态日期取，eg：2022-05-30
     * 
     * @return 
     */
    private queryPushData(): UploadFileInfo[] {
        let queryData = this.ds.executeQuery(this.QUERY_SHARE_FOOD_INFO_SQL) as GX_GJ_SPJY_LOG[];
        let uploadFileInfo: UploadFileInfo[] = [];

        let sdfDate = new java.text.SimpleDateFormat("yyyy-MM-dd");
        let nowDate: string = sdfDate.format(new java.util.Date(Date.now()));

        /** 对方服务器存储文件夹路径 */
        let targetFilePath: string = `/gj_spjy_pdf/${nowDate}`;
        if (!this.checkDirIsExist(nowDate)) { // 若文件夹不存在，则创建推送的目标文件夹
            this.ftpClient.mkdir(targetFilePath);
        }
        for (let i = 0; i < queryData.length; i++) {
            let info: GX_GJ_SPJY_LOG = queryData[i];
            let localFilePath: string = `${this.localFolderPath}${info.FILE_NAME}`;  // todo 后续路径都增加了时间
            let file = new java.io.File(localFilePath);
            if (file.exists()) {
                uploadFileInfo.push({
                    targetPath: targetFilePath,
                    filePath: localFilePath,
                    fileName: info.FILE_NAME as string
                });
            }
        }
        return uploadFileInfo;
    }

    /**
     * 校验当前文件夹是否存在
     * @param nowDate 日期命名的文件夹名
     * @return true | false
     */
    private checkDirIsExist(nowDate: string): boolean {
        let fileLists = this.ftpClient.list("/gj_spjy_pdf");
        for (let i = 0; i < fileLists.length; i++) {
            let file = fileLists[i];
            if (file.filename == nowDate) { // "2022-05-30" == "2022-05-30"
                return true;
            }
        }
        return false;
    }

    /**
     * 更新日志信息
     * @param fileName 文件名
     */
    private updatelogInfo(fileName: string): void {
        let updateSQL: string = `update public.GX_GJ_SPJY_LOG set SEND_CODE = ? WHERE FILE_NAME = ?`;
        try {
            this.ds.executeUpdate(updateSQL, ["1", fileName]);
        } catch (e) {
            print(`更新：${fileName} 日志信息失败`);
            print(e);
        }
    }
}


/** 共享国家食品经营日志表  */
interface GX_GJ_SPJY_LOG {
    /** 电子证照查验码 */
    AUTH_CODE: string;
    /** 许可证编号 */
    LIC_NO: string;
    /** 发证日期（许可证上打印的发证日期） */
    ISSU_DATE: string;
    ETL_TIME?: string;
    /** 文件名 */
    FILE_NAME?: string;
}

interface UploadFileInfo {
    /** 目标服务器父路径地址，eg：/gj_spjy_pdf */
    targetPath: string;
    /** 文件名称，eg：test.pdf */
    fileName: string;
    /** 文件存储路径，eg：/localPath/test.pdf */
    filePath: string;
}