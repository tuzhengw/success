/**
 * 随机生成一个18位身份证（简单版）
 */
function getId_no(): string {
    let coefficientArray = ["7", "9", "10", "5", "8", "4", "2", "1", "6", "3", "7", "9", "10", "5", "8", "4", "2"];// 加权因子
    let lastNumberArray = ["1", "0", "X", "9", "8", "7", "6", "5", "4", "3", "2"];// 校验码
    let address = "420101";
    let birthday = "19810101";
    let s = Math.floor(Math.random() * 10).toString() + Math.floor(Math.random() * 10).toString() + Math.floor(Math.random() * 10).toString();
    let array = (address + birthday + s).split("");
    let total: number = 0;
    for (var i = 0; i < array.length; i++) {
        total = total + parseInt(array[i]) * parseInt(coefficientArray[i]);
    }
    let lastNumber = lastNumberArray[total % 11];
    let id_no_String: string = address + birthday + s + lastNumber;
    /**
     * 若身份证不合法，则重新生成
     */
    if (!utils.pidCheck(id_no_String)){
        getId_no();
    }
    return id_no_String;
}