import db from "svr-api/db";
import dw from "svr-api/dw";
import excel from "svr-api/excel";
import utils from "svr-api/utils";
import security from "svr-api/security";
import fs from 'svr-api/fs';
import http from 'svr-api/http';
import sys from 'svr-api/sys';

const SimpleDateFormat = java.text.SimpleDateFormat;
const Calendar = java.util.Calendar;
const FileUtils = Java.type("org.apache.commons.io.FileUtils");
const JavaFile = java.io.File;
const JavaDate = java.util.Date;
const System = java.lang.System;
const StringUtils = Java.type('org.apache.commons.lang.StringUtils');
const Timestamp = Java.type('java.sql.Timestamp');
const BaseException = com.succez.exception.business.BaseException;
const DS = db.getDefaultDataSource();
const isDebug = false;//是否打印调试日志，默认关闭
/**
 * 失败日志等级代码
 */
const LOGLEVEL_FAIL = 'FAIL';

/**
 * 日志重要程度
 */
const LOGLEVEL_FAIL_IMPT = 'IMPT';

export function importExcel(request: HttpServletRequest, response: HttpServletResponse, params: any) {
	let path, fileId, fileObj;
	if (params && params.fileId) {
		fileId = params.fileId;
		fileObj = request.getUploadFile(fileId);
		path = fileObj.file['path'];
	} else {
		path = params.path
	}
	let result;
	/**
	 * 2021/09/23 wangcan
	 * 兼容一下传入path的情况
	 */
	if (!path) {
		return { result: false, message: '未导入文件' };
	}
	let file = fs.getFile(path);
	//限制文件大小为1M, file.length()的单位是byte
	if (file && (file.length() > 1 * 1024 * 1024)) {
		return {
			result: false,
			message: '文件大小超出限制，文件最大为1M'
		};
	}
	let operationState = 'OK';
	let exception = null;
	let datas;
	try {
		let sheetjson = excel.readExcelToJSON({ sheet: 0, excelFile: path, columnName: "firstRow" });
		if (!sheetjson || sheetjson.length == 0) {
			result = { result: false, message: '空文件' };
			operationState = 'FAIL';
		} else {
			datas = sheetjson[0].rows;
			if (!datas || datas.length < 1 || datas[0].length != 15) {
				result = { result: false, message: '您的excel行或列有误，请按照模板excel进行导入，错误原因：' + (!datas || datas.length < 1 ? '0行' : (datas[0].length + '列')) };
				operationState = 'FAIL';
			} else if (datas.length > 1000) {
				result = { result: false, message: '导入excel最大行数为1000行，请重新分批导入' };
				operationState = 'FAIL';
			} else {
				addData(datas, fileId || path);
				result = { result: true };
			}
		}
	} catch (e) {
		print(e);
		log('导入应检人员excel数据出现异常', e);
		let message = (e && e.message) || (e.getMessage && e.getMessage()) || '';
		result = { result: false, message: '网络中断或系统内部异常，请稍后重试或联系管理员处理', errorMessage: message };
		//operationState = 'FAIL';
		//exception = newR BaseException(e);
		exception = e;
		throw e;
	} finally {
		let url = 'custom.importExcel.YJRY';
		let fileLogPath = addFileLog(url, path);
		let event: any = {
			name: url,
			detailInfo: {
				result: result,
				fileLogPath: fileLogPath
			},
			operationState: <any>operationState,
			exception: exception
		}
		//Utils.checkParamsOrResultCount(datas, event, Utils.ParamsType.params) && (event.detailInfo.data = datas);
		//let fileLogPath = fileId;//其实不需要记，模型表里有个文件id字段就够了
		print(event)
		sys.fireEvent(event)
	}
	return result;
}

const FIELDS = [
	'JYZT',//检验状态
	'WYJ',//唯一键
	'XM',//姓名
	'XMM',//姓名密
	'ZJLX',//证件类型
	'SFZH',//身份证号
	'SFZHM',//身份证号密
	'DHHM',//电话号码
	'DHHMM',//电话号码密
	'XB',//性别
	'XBDM',//性别代码
	'NL',//年龄
	'MZ',//民族
	'MZDM',//民族代码
	'JDXZ',//街道乡镇
	'SQCD',//社区村队
	'JGZT',//监管状态
	'JGZTDM',//监管状态代码
	'JGZQ',//监管周期
	'JGZQDM',//监管周期代码
	'SSRY',//所属人员
	'SSRYDM',//所属人员代码
	'ZDRYLB',//重点人员类别
	'ZDRYLBDM',//重点人员类别代码
	'HJDZ',//户籍地址
	'WJID',//文件ID
	'ZDCS',//重点场所
	'CYDD',//采样地点
	'CJSJ',//创建时间
	'CJZ',//创建者
	'BZ'//备注
];

function addData(datas: any[][], fileId: string) {
	if (datas.length == 0) {
		return;
	}
	let user = security.getCurrentUser();
	let userInfo = user.userInfo;
	log("当前用户信息", userInfo, false);
	let zdcs = userInfo.orgId;
	let cydd = zdcs && userInfo._dimData_['ORG_ID'] && userInfo._dimData_['ORG_ID']['DYCYJG'];
	let cjz = userInfo.userId;
	let cjsj = new JavaDate().getTime();

	let nrows = [];
	let start = Date.now();
	for (let i = 0; i < datas.length; i++) {
		let row = datas[i];
		let xm = row[0];//姓名
		let xb = row[1];//性别
		let nl = row[2];//年龄
		let mz = row[3];//民族
		let zjlx = row[4];//证件类型
		let sfzh = row[5];//身份证号
		let dhhm = row[6];//电话号码
		let jdxz = row[7];//街道乡镇
		let sqcd = row[8];//社区村队
		let jgzt = row[9];//监管状态
		let jgzq = row[10];//监管周期
		let ssry = row[11];//所属人员
		let zdrylb = row[12];//重点人员类别
		let hjdz = row[13];//户籍地址
		let bz = row[14]//备注
		if (xm) {
			xm = trim(xm);
		}
		if (xb) {
			xb = trim(xb);
		}
		if (dhhm) {//电话号码有可能是科学计数法，这里转换下
			dhhm = trim(dhhm.toString(10));
		}
		if (nl) {//年龄有可能输入浮点或字符，这里转换下，避免长度不够
			nl = trim(parseInt(nl).toString());
		}
		if (mz) {//民族可能漏掉“族”这里兼容下，
			/**
			 * 2021/08/18 wangcan
			 * 兼容mz是数字代码的情况
			 */
			if (isNaN(mz)) {
				mz = trim(mz);
				let idx = mz.lastIndexOf('族');
				idx == -1 && (mz += '族');
			}
		}
		if (sfzh) {
			sfzh = trim(sfzh);
			sfzh = sfzh.toLocaleUpperCase();//转大写吧，兼容下小写x校验不过问题
		}
		if (jdxz) {
			jdxz = trim(jdxz);
		}
		if (sqcd) {
			sqcd = trim(sqcd);
		}
		if (jgzt) {
			jgzt = trim(jgzt);
		}
		if (jgzq) {
			jgzq = trim(jgzq);
		}
		if (ssry) {
			ssry = trim(ssry);
		}
		if (zdrylb) {
			zdrylb = trim(zdrylb);
		}
		if (hjdz) {
			hjdz = trim(hjdz);
		}
		if (bz) {
			bz = trim(bz);
		}
		if (zjlx) {
			zjlx = trim(zjlx);
		}
		let nrow = [];
		nrow[1] = null;
		nrow[2] = getFieldValue(xm);
		nrow[3] = null;
		nrow[4] = getFieldValue(zjlx);
		nrow[5] = getFieldValue(sfzh);
		nrow[6] = null;
		nrow[7] = getFieldValue(dhhm);
		nrow[8] = null;
		nrow[9] = getFieldValue(xb);
		nrow[10] = null;
		nrow[11] = getFieldValue(nl);
		nrow[12] = getFieldValue(mz);
		nrow[13] = null;
		nrow[14] = getFieldValue(jdxz);
		nrow[15] = getFieldValue(sqcd);
		nrow[16] = getFieldValue(jgzt);
		nrow[17] = null;
		nrow[18] = getFieldValue(jgzq);
		nrow[19] = null;
		nrow[20] = getFieldValue(ssry);
		nrow[21] = null;
		nrow[22] = getFieldValue(zdrylb);
		nrow[23] = null;
		nrow[24] = getFieldValue(hjdz);
		nrow[25] = fileId;
		nrow[26] = getFieldValue(zdcs);
		nrow[27] = getFieldValue(cydd);
		nrow[28] = new Timestamp(cjsj);
		nrow[29] = cjz;
		nrow[30] = getFieldValue(bz);

		let state = CHECK_STATE.NoProblem;

		/**
		 * 校验：先做简单校验，提高性能
		 * 顺序：1.年龄、2.手机号、3.身份证、4.证件类型4.性别、5.民族、6.监管状态、7.监管周期、8.所属人员、9.重点人员类别、10.主键
		 */
		if (!checkAge(nl)) {
			state = CHECK_STATE.NLInvalid;
		}
		if (!checkXM(xm)) {
			state = CHECK_STATE.XMInvalid;
		}
		if (state == CHECK_STATE.NoProblem) {//手机号
			if (!checkPhone(dhhm)) {
				state = CHECK_STATE.PhoneInvalid;
			}
		}

		if (state == CHECK_STATE.NoProblem) {//证件类型
			let code = checkDim(DIMS_PATH[6], zjlx);
			if (code != null) {
				nrow[4] = code;
			} else {
				state = CHECK_STATE.ZJLXInvalid;
			}
		}

		if (state == CHECK_STATE.NoProblem) {//身份证
			if (!checkIDCard(sfzh) && (zjlx == '身份证' || zjlx == '10')) {
				state = CHECK_STATE.IDCardInvalid;
			}
		}

		//可能code会由0，在只用code校验时，会走false分支。所以要用code!=null， 也兼容了code=undefined的情况
		/**
		 * null == undefined 值为 true,
		 * null != false, undefined != false 值为true,
		 * 0 == false 值为 true,
		 * '' == false 值为 true,
		 * 0 == '' 值为 true ,
		 * 0 == null 值为false,
		 * 0 == undefined 值为false
		 *
		 */
		if (state == CHECK_STATE.NoProblem) {//性别
			let code = checkDim(DIMS_PATH[0], xb);
			if (code != null) {
				nrow[10] = code;
			} else {
				state = CHECK_STATE.XBInvalid;
			}
		}

		if (state == CHECK_STATE.NoProblem) {//民族
			let code = checkDim(DIMS_PATH[1], mz);
			if (code != null) {
				nrow[13] = code;
			} else {
				state = CHECK_STATE.MZInvalid;
			}
		}

		if (state == CHECK_STATE.NoProblem) {//监管状态
			let code = checkDim(DIMS_PATH[2], jgzt);
			if (code != null) {
				nrow[17] = code;
			} else {
				state = CHECK_STATE.JGZTInvalid;
			}
		}

		if (state == CHECK_STATE.NoProblem) {//监管周期
			let code = checkDim(DIMS_PATH[3], jgzq);
			if (jgzq == '随到随检') {
				code = '0';
			}
			if (jgzq == '随到随检' || code != null) {
				nrow[19] = code;
			} else {
				state = CHECK_STATE.JGZQInvalid;
			}
		}

		if (state == CHECK_STATE.NoProblem) {//所属人员
			let code = checkDim(DIMS_PATH[4], ssry);
			if (code != null) {
				nrow[21] = code;
			} else {
				state = CHECK_STATE.SSRYInvalid;
			}
		}

		if (state == CHECK_STATE.NoProblem) {//重点人员类别
			let code = checkDim(DIMS_PATH[5], zdrylb);
			if (code != null) {
				nrow[23] = code;
			} else {
				state = CHECK_STATE.ZDRYLBInvalid;
			}
		}

		if (state == CHECK_STATE.NoProblem) {//主键重复
			let sfzhm = encryptSM4(sfzh);
			let xmm = encryptSM4(xm);
			if (checkPrimarykey(sfzhm, xmm, zdcs, nrow[4], cydd)) {
				state = CHECK_STATE.DuplicatePrimaryKey;
			} else {
				nrow[3] = xmm;
				nrow[6] = sfzhm;
				nrow[8] = encryptSM4(dhhm);
			}
		}

		nrow[0] = state;
		nrows.push(nrow);
	}
	let end = Date.now();
	isDebug && print('遍历校验耗时：' + (end - start) + 'ms');
	let conn = DS.getConnection();//批量insert存在性能问题，需要改为事务手动提交
	conn.setAutoCommit(false);
	try {
		DS.openTableData('tmp_yjry_import', null, conn).insert(nrows, FIELDS);
		conn.commit();
		isDebug && print('插入耗时：' + (Date.now() - end) + 'ms');
	} catch (e) {
		conn && conn.rollback();
		throw e;
	} finally {
		conn && conn.close();
	}
}

function getFieldIndex(fields: string[], index: number) {
	return fields.indexOf(FIELDS[index]);
}

/**
 * 2021/08/30 wangcan
 * 去除两端的空格,制表符,换行
 * @example
 * trim('   123     ')='123'
 * trim(' 123 ') = '123'
 * trim('\n\rabc\n\r') = 'abc'
 * trim('\tabc\t') = 'abc'
 */
function trim(v: any) {
	if (!v) {
		return null;
	}
	return v.toString().trim();
}

function getFieldValue(v) {
	if (v === undefined) {
		return null;
	}
	return v;
}



const DIMS_PATH = [
	'/SCHSJC/data/tables/dim/DIM_XB.tbl',
	'/SCHSJC/data/tables/dim/DIM_MZ.tbl',
	'/SCHSJC/data/tables/dim/DIM_JGZT.tbl',
	'/SCHSJC/data/tables/dim/DIM_JCPC.tbl',
	'/SCHSJC/data/tables/dim/DIM_SSRQ_YJJJ.tbl',
	'/SCHSJC/data/tables/dim/DIM_ZDRYLB.tbl',
	'/SCHSJC/data/tables/dim/DIM_ZJLX.tbl'
];

/**
 * 维表文本-代码映射map，格式如下：
 * {
 *   'path': {
 *      fields: [],
 *      data: {
 *          'caption': 'code'
 *      }
 *   }
 * }
 */
var dims = {
	'/SCHSJC/data/tables/dim/DIM_XB.tbl': {
		fields: ['CODE_', 'CAPTION_']
	},
	'/SCHSJC/data/tables/dim/DIM_MZ.tbl': {
		fields: ['CODE_', 'CAPTION_']
	},
	'/SCHSJC/data/tables/dim/DIM_JGZT.tbl': {
		fields: ['DM', 'BT']
	},
	'/SCHSJC/data/tables/dim/DIM_JCPC.tbl': {
		fields: ['CODE_', 'CAPTION_']
	},
	'/SCHSJC/data/tables/dim/DIM_SSRQ_YJJJ.tbl': {
		fields: ['CODE_', 'CAPTION_']
	},
	'/SCHSJC/data/tables/dim/DIM_ZDRYLB.tbl': {
		fields: ['DM', 'BT']
	},
	'/SCHSJC/data/tables/dim/DIM_ZJLX.tbl': {
		fields: ['CODE_', 'CAPTION_']
	}
};

function getDimData(path: string) {
	let dim = dims[path];
	if (!dim) return null;
	let data = dim.data;
	if (data) {
		return data;
	}
	data = {};
	let fields: string[] = dim.fields;
	let queryFieldInfo: QueryFieldInfo[] = [];
	fields.forEach(f => {
		queryFieldInfo.push({
			name: f,
			exp: 't0.' + f
		});
	})
	let queryResult = dw.queryData({
		sources: [{
			id: 't0',
			path: path
		}],
		fields: queryFieldInfo
	});
	let result = queryResult.data;
	if (result && result.length > 0) {
		for (let i = 0; i < result.length; i++) {
			let r = result[i];
			let code = r[0];
			let caption = r[1];
			if (caption && code) {
				data[caption] = code;
				data[code] = code;//兼容下直接是代码的情况
			}
		}
	}
	dims[path].data = data;
	return data;
}

/**
 * 检查年龄：数字，且在[0, 200]之间
 */
function checkAge(age: string) {
	if (!age) return false;
	let num = parseInt(age);
	if (isNaN(num) || num < 0 || num > 200) {
		return false;
	}
	return true;
}


/**
 * 检查手机号是否合法
 * 规则：1开头的11位数字
 * @params {String} phone 手机号，注：sm4加密后的
 */
function checkPhone(phone: string): boolean {
	if (!phone) return false;
	return /(^1\d{10}$)/.test(phone);
}

/**
 * 检查身份证号是否合法
 * 规则：见`CheckIdCard` 或bi4.0产品的`PidChecker`
 * @params {String} idCard 身份证号，注：sm4加密后的
 */
function checkIDCard(idCard: string): boolean {
	if (!idCard) return false;
	return utils.pidCheck(idCard);
}

/**
 * 2021/09/03 wangcan
 * 校验姓名,规则:
 * 1.纯汉字,允许有"·"号
 * 2.英文姓名,可以有大小写字母或 "."号和空格, 不能以空格,制表符,换行等任意空白符开头或结尾
 *
 * 2021/09/16 wangcan
 * 由于有的生僻字无法匹配,扩大匹配区间增加[\u3400-\u4db5]的中文
 */
function checkXM(name) {
	name = trim(name).toString();
	if (!name) return false;
	return /^(?!\s)(?!.*\s$)[a-zA-Z\.\s]{1,40}$/g.test(name) || /^(?!\s)(?!.*\s$)[\u3400-\u4db5\u4e00-\u9fa5·]{1,40}$/g.test(name);
}

/**
 * 2021/09/18 wangcan
 * 检查证件类型是否正确
 */

/**
 * 检查维表数据是否存至，成功则返回code，失败返回null
 * 2021/11/24 wangcan
 * 因为false !=null 所以不能用false,改为返回null
 */
function checkDim(path: string, caption: string): string {
	if (!caption) return null;
	let data = getDimData(path);
	let code = data && data[caption];
	if (code) {
		return code;
	}
	return null;
}

var pks = [];

/**
 * 检查主键是否重复
 * 规则：身份证号+姓名作为主键
 * @params {String} idCard 身份证号，注：sm4加密后的
 * @params {String} name 姓名，注：sm4加密后的
 * @params {String} zdcs 重点场所
 * @params {String} zjlx 证件类型
 * @params {String} cydd 采样点
 */
function checkPrimarykey(idCard: string, name: string, zdcs: string, zjlx: string, cydd: string): boolean {
	let pkStr = idCard + name + zdcs + zjlx;
	if (pks.indexOf(pkStr) > -1) return true;
	let tableData = DS.openTableData('fact_yjry');
	//let tmpTableData = DS.openTableData('tmp_yjry_import');
	let where = {};
	if (idCard) {
		where['SFZH'] = idCard;
	}
	if (name) {
		where['XM'] = name;
	}
	if (zdcs) {
		where['ZDCS'] = zdcs;
	}
	if (zjlx) {
		where['ZJLX'] = zjlx;
	}
	if (cydd) {
		where['GXJG'] = cydd;
	}
	if (where == {}) {
		return false;
	}
	let rs = tableData.select('*', where);
	// where = {};
	// if (idCard) {
	// 	where['SFZHM'] = idCard;
	// }
	// if (name) {
	// 	where['XMM'] = name;
	// }
	// if (zdcs) {
	// 	where['ZDCS'] = zdcs;
	// }
	// if (zjlx) {
	// 	where['ZJLX'] = zjlx;
	// }
	// if (cydd) {
	// 	where['CYDD'] = cydd;
	// }
	// let tmpRs = tmpTableData.select('*', where);
	if (rs && rs.length > 0) {
		return true;
	}
	pks.push(pkStr);
	return false;
}

/** sm4算法的初始化向量，这里给定一个确切的值，外部使用该算法时只需要提供秘钥即可 */
const iv = "UISwD9fW6cFh9SNS";
/** 内置算法的秘钥 */
const BUILT_IN_SECRET_KEY = "JeF8U9wHFOMfs2Y8";
/**
 * 解密bi4.0模型sm4加密的数据
 * 注：mode为CBC，hexString为false
 */
function decryptSM4(v: any): string {
	return utils.decryptSM4({
		str: v,
		iv: iv,
		key: BUILT_IN_SECRET_KEY,
		mode: utils.SM4Mode.CBC,
		hexString: false,
		encoding: "utf-8"
	});
}

/**
 * 加密数据
 * 注：mode为CBC，hexString为false
 */
function encryptSM4(v: any): string {
	return utils.encryptSM4({
		str: v,
		iv: iv,
		key: BUILT_IN_SECRET_KEY,
		mode: utils.SM4Mode.CBC,
		hexString: false,
		encoding: "utf-8"
	});
}

/**
 * 检验状态
 */
enum CHECK_STATE {
	/** 无问题 */
	NoProblem = 0,
	/** 身份证号不合法 */
	IDCardInvalid = 1,
	/** 手机号不合法 */
	PhoneInvalid = 2,
	/** 主键重复 */
	DuplicatePrimaryKey = 3,
	/** 年龄不合法 */
	NLInvalid = 4,
	/** 性别不合法 */
	XBInvalid = 5,
	/** 民族不合法 */
	MZInvalid = 6,
	/** 监管状态不合法 */
	JGZTInvalid = 7,
	/** 监管周期不合法 */
	JGZQInvalid = 8,
	/** 所属人员不合法 */
	SSRYInvalid = 9,
	/** 重点人员类别不合法 */
	ZDRYLBInvalid = 10,
	/**姓名格式不合法 */
	XMInvalid = 11,
	/**证件类型不合法 */
	ZJLXInvalid = 12
}

/** 文件日志根目录：/workdir/clusters-share/api-excel-log */
const ROOT_DIR = sys.getClusterShareDir() + '/api-excel-log';

/**
 * 记录一条文件日志，将机构数据对接的所有参数+数据都保存在文件中，便于排查问题
 * 使用场景：API接口调用、excel批量导入
 * 注：路径规则：
 *      1. excel: `/workdir/clusters-share/api-excel-log/{yyyyMMdd/excel/{url}-{userId}/{uuid}-{fileName}`
 *      2. api: `/workdir/clusters-share/api-excel-log/{yyyyMMdd/api/{url}-{appId}/{uuid}.json`
 *
 * @param {string} url 接口标识，建议和记录日志的`EVENT_NAME`保持一致，不要有/、\
 * @param {string} excelPath 导入的excel的path，与jsonParams二传一
 * @param {JSONObject} jsonParams API调用的参数，包含appId以及数据
 * @returns 返回存储文件的路径，建议将文件路径存至系统日志里，便于筛查，如果文件不存在或参数不存在或者异常则返回null，表示日志记录是吧
 */
function addFileLog(url: string, excelPath?: string, jsonParams?: JSONObject): string {
	let now = new JavaDate();
	let dateStr = formateDate(now, "yyyyMMdd");
	if (excelPath) {//excel
		try {
			let userId = security.getCurrentUser().userInfo.userId;
			let excelFile = new JavaFile(excelPath);
			if (!excelFile.exists()) return null;
			let newFileDirPath = ROOT_DIR + '/' + dateStr + '/excel/' + url + '-' + userId;
			let newFileDir = new JavaFile(newFileDirPath);
			FileUtils.forceMkdir(newFileDir);
			let newFileName = excelFile.getName();
			let newFilePath = newFileDirPath + '/' + newFileName;
			let newFile = new JavaFile(newFilePath);
			FileUtils.copyFile(excelFile, newFile);//还是用copy吧，避免影响原文件
			return newFilePath;
		} catch (e) {
			log('拷贝excel异常', e);
			return null;
		}
	}

	if (jsonParams) {//api
		try {
			let appId = jsonParams['app_id'];
			if (!appId) return null;
			let newFileDirPath = ROOT_DIR + '/' + dateStr + '/api/' + url + '-' + appId;
			let newFileDir = new JavaFile(newFileDirPath);
			FileUtils.forceMkdir(newFileDir);
			let newFileName = utils.uuid() + '.json';
			let newFilePath = newFileDirPath + '/' + newFileName;
			let fsFile = fs.getFile(newFilePath);
			fsFile.writeJson(jsonParams);
			return newFilePath;
		} catch (e) {
			log('创建api接口参数json文件异常', e);
			throw e;
			//return null;
		}
	}

	return null;
}

/**
 * 下载一个日志文件
 * @param {string} path 日志文件路径
 */
export function downloadFileLog(request: HttpServletRequest, response: HttpServletResponse, params: any) {
	let path = params && params.path;
	if (!path) return null;
	let file = new JavaFile(path);
	if (!file.exists()) return null;
	let url = (<any>fs).createDownloadURL(path);
	response.sendRedirect(url);
}

/**
 * 清理日志文件，默认清理三天前的
 */
function clearFileLog(filePath?: string, dateStr?: string, isall?: boolean) {
	if (isall) {
		filePath = ROOT_DIR;
	} else if (dateStr) {
		filePath = ROOT_DIR + '/' + dateStr;
	} else if (!filePath) {
		filePath = ROOT_DIR + '/' + formateDate(addDate(new JavaDate(), -3, Calendar.DAY_OF_MONTH), 'yyyyMMdd');
	}
	if (filePath && filePath.indexOf(ROOT_DIR) == 0) {
		try {
			//FileUtils.forceDeleteOnExit(new JavaFile(filePath));
			fs.deleteFile(filePath);
		} catch (e) {
			log("删除文件出现异常", e)
		}
	}
}

/**
 * 格式化日期
 * @param {Date} date 日期
 * @param {string} format 日期格式，默认为`yyyy-MM-dd HH:mm:ss`
 * @returns 返回日期字符串
 */
function formateDate(date: Date, format?: string): string {
	if (!date) return '';
	if (!format) format = "yyyy-MM-dd HH:mm:ss";
	return new SimpleDateFormat(format).format(date).toString();
}

/**
 * 计算日期
 * @param {Date} date 日期
 * @param {number} num 需要增加的时间，可以是负数，表示减少
 * @param {string} type 类型，Calendar类型
 * @returns 返回日期类型
 */
function addDate(date: Date, num: number, type) {
	var startDT = Calendar.getInstance();
	startDT.setTime(date);
	var endDT = Calendar.getInstance();
	endDT.set(startDT.get(Calendar.YEAR), startDT.get(Calendar.MONTH), startDT.get(Calendar.DAY_OF_MONTH), startDT.get(Calendar.HOUR_OF_DAY), startDT.get(Calendar.MINUTE), startDT.get(Calendar.SECOND));
	endDT.add(type, num);
	return endDT.getTime();
}

/**
 * 打印一行日志在控制台
 */
function log(message: string, obj: any, isImp: boolean = true) {
	if (isImp || isDebug) {
		print(`-----------${message}-start------------`);
		print(obj);
		print(`-----------${message}-end------------`);
	}
}


namespace Utils {

	const MAX_DETAIL_LOG_LEN = 50000;

	/**
	 * 当传递参数数据或返回结果数据行数超过1千行时，只记录错误的message，不记录详细的数据，避免一下子数据量很大
	 * 2021/12/14 wangcan
	 * @param datas 接口的数据
	 * @param event 日志配置对象
	 * @param paramsType 参数类型，是入参还是返回结果
	 * @return 一个boolean值如果数据不超过1000条或为空为true，否则为false
	 */
	export function checkParamsOrResultCount(datas: any, event, paramsType: ParamsType): boolean {
		let datas_str = JSON.stringify(datas) || datas;
		if (!datas_str) return true;
		event.logLevel = LOGLEVEL_FAIL_IMPT;
		event.detailInfo = event.detailInfo || {};
		if (datas_str.length > MAX_DETAIL_LOG_LEN && paramsType == ParamsType.params) {
			event.detailInfo.params = event.detailInfo.params || {};
			// logExpMsgWhileParamsExcced(datas_str, event)
			event.detailInfo.params.message = `超过最大上传参数长度${MAX_DETAIL_LOG_LEN}, 当前长度:${datas.length}`;
			return false
		} else if (datas_str.length > MAX_DETAIL_LOG_LEN && paramsType == ParamsType.result) {
			// logExpMsgWhileResultExcced(datas_str, event)
			event.detailInfo.result = event.detailInfo.result || {};
			event.detailInfo.result.message = `超过最大返回数据长度${MAX_DETAIL_LOG_LEN}, 当前长度:${datas.length}`;
			return false
		}
		return true;
	}

	export enum ParamsType {
		params = 'params',
		result = 'result'
	}
	export enum EventOperationState {
		OK = 'OK',
		EXCPT = 'EXCPT',
		FAIL = 'FAIL',
		ERROR = 'ERROR'
	}
}