package interfacetest;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Hashtable;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

/**
 * @author tuzw
 * @version 1.0
 * @date 2022/1/19 0019 13:59
 */
public class QRCodeUtils {

    private static final int BLACK = 0xFF000000;//用于设置图案的颜色

    private static final int WHITE = 0xFFFFFFFF; //用于背景色

    private static final int DEFAULT_WIDTH = 300;//二维码默认宽度

    private static final int DEFAULT_HEIGHT = 300;//二维码默认高度

    private static final String DEFAULT_TYPE = "png";//二维码默认图片格式

    /**
     * 测试方法，可忽略
     */
    public static void test() {
        String filePath = "C:/Users/Administrator/Desktop/success/test.png";
        String qPath = "https://blog.csdn.net/qq_39239117/article/details/87607584";
        try {
            generateQRCode(filePath, qPath, 400, 400, "png");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
	
	 /**
     * 生成二维码，并获取二进制流，写入文件
     * @param filePath
     * @param content
     * @param width
     * @param height
     * @param type
     * @throws Exception
     */
    public static void generateQRcode1(String filePath, String content, int width, int height, String type) throws Exception {
        BitMatrix bitMatrix = getBitMarix(content, width, height);
        BufferedImage image = toBufferedImage(bitMatrix);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ImageIO.write(image, type, byteArrayOutputStream);
        byte[] bytes = byteArrayOutputStream.toByteArray(); // 获得字节数组

        File file = new File(filePath);
        if (!file.exists()) {
            System.out.println("文件不存在");
            file.createNewFile();
        }
        FileOutputStream out = new FileOutputStream(filePath);
        out.write(bytes);
    }


    /**
     * 生成二维码
     * @param imgpath 二维码图片路径
     * @param content 二维码内容
     * @param width 图片宽度
     * @param height 图片高度
     * @param type 图片格式
     * @throws Exception
     */
    public static void generateQRCode(String imgpath, String content, int width, int height, String type)
            throws Exception {
        BitMatrix bitMatrix = getBitMarix(content, width, height);
        // 生成二维码
        File file = new File(imgpath);
        if (!file.exists()) {
            System.out.println("文件不存在");
            file.createNewFile();
        }
        String newtype = FilenameUtils.getExtension(imgpath);
        type = StringUtils.isEmpty(newtype) ? (StringUtils.isEmpty(type) ? DEFAULT_TYPE : type) : newtype;
        writeToFile(bitMatrix, type, file);
    }

    /**
     * 生成二维码
     * @param image
     * @param content
     * @param width
     * @param height
     * @param type
     * @throws Exception
     */
    public static void generateQRCode(File image, String content, int width, int height, String type) throws Exception {
        BitMatrix bitMatrix = getBitMarix(content, width, height);
        // 生成二维码
        if (!image.exists()) {
            image.createNewFile();
            return;
        }
        String newtype = FilenameUtils.getExtension(image.getName());
        type = StringUtils.isEmpty(newtype) ? (StringUtils.isEmpty(type) ? DEFAULT_TYPE : type) : newtype;
        writeToFile(bitMatrix, type, image);
    }

    /**
     * 生成二维码
     * @param os
     * @param content
     * @param width
     * @param height
     * @param type
     * @throws Exception
     */
    public static void generateQRCode(OutputStream os, String content, int width, int height, String type)
            throws Exception {
        BitMatrix bitMatrix = getBitMarix(content, width, height);
        // 生成二维码
        type = StringUtils.isEmpty(type) ? DEFAULT_TYPE : type;
        writeToStream(bitMatrix, type, os);
    }

    /**
     * 设置二维码信息
     * @param content
     * @param width
     * @param height
     * @return
     * @throws WriterException
     */
    private static BitMatrix getBitMarix(String content, int width, int height) throws WriterException {
        Hashtable<EncodeHintType, Object> hints = new Hashtable<EncodeHintType, Object>();
        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);// 指定纠错等级,纠错级别（L 7%、M 15%、QRCodeUtils 25%、H 30%）
        hints.put(EncodeHintType.CHARACTER_SET, "utf-8");// 内容所使用字符集编码
        hints.put(EncodeHintType.MARGIN, 1);//设置二维码边的空度，非负数
        width = width <= 0 ? DEFAULT_WIDTH : width;
        height = height <= 0 ? DEFAULT_HEIGHT : height;
        BitMatrix bitMatrix = new MultiFormatWriter().encode(content, BarcodeFormat.QR_CODE, width, height, hints);
        return bitMatrix;
    }

    /**
     * 将二维码信息写入文件
     * @param matrix 二维码信息
     * @param format
     * @param file
     * @throws IOException
     */
    private static void writeToFile(BitMatrix matrix, String format, File file) throws IOException {
        BufferedImage image = toBufferedImage(matrix);
        System.out.println(image);
        if (!ImageIO.write(image, format, file)) {
            System.out.println("二维码信息写入失败");
        }
    }

    /**
     * 将二维码信息写入输出流
     * @param matrix 二维码信息
     * @param format
     * @param stream
     * @throws IOException
     */
    private static void writeToStream(BitMatrix matrix, String format, OutputStream stream) throws IOException {
        BufferedImage image = toBufferedImage(matrix);
        if (!ImageIO.write(image, format, stream)) {
            throw new Error("生成二维码图片失败");
        }
    }

    private static BufferedImage toBufferedImage(BitMatrix matrix) {
        int width = matrix.getWidth();
        int height = matrix.getHeight();
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                image.setRGB(x, y, (matrix.get(x, y) ? BLACK : WHITE));
            }
        }
        return image;
    }
}
