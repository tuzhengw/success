

/**
 * 将JSON数组转换为二位数组
 * @param dataSourceStateInfos 写入的数据源状态信息数组
 * @return 
 */
function jsonDataToRows(dbSourceStateInfos: SDI_MONITOR_DATASOURCESTATE[]): string[][] {
    if (dbSourceStateInfos.length == 0) {
        return [];
    }
    let rows: string[][] = [];
    let fieldKey: string[] = Object.keys(dbSourceStateInfos[0]);
    for (let i = 0; i < dbSourceStateInfos.length; i++) {
        let row: string[] = [];
        for (let k = 0; k < fieldKey.length; k++) {
            row.push(dbSourceStateInfos[i][fieldKey[k]]);
        }
        rows.push(row);
    }
    return rows;
}


/** 数据源状态对象 */
interface SDI_MONITOR_DATASOURCESTATE {
    /** 数据源名称 */
    DATASOURCE_NAME?: string;
    /** 数据库类型 */
    DBTYPE?: string;
    /** 数据库服务IP */
    HOST?: string;
    /** 数据库URL */
    URL?: string;
    /** 数据库用户名 */
    USER?: string;
    /** 数据库用户名 */
    DB_USER?: string;
    /** 连接池最大连接数 */
    MAXPOOLSIZE?: number;
    /** 数据库版本 */
    DBVERSION?: string;
    /** Jdbc驱动版本 */
    JDBCVERSION?: string;
    /** 状态 */
    STATE?: string;
    /** 日志 */
    LOG?: string;
    /** 日志 */
    CHECK_LOG?: string;
    /** 最后修改时间 */
    LAST_UPDATE_TIME?: number | any;
    /** 创建时间 */
    CREATE_TIME?: number | any;
}

/** 数据源状态 */
enum DATA_SOURCE_STATE {
    /** 失败 */
    FALL = "FAIL",
    /** 连接中 */
    WORKING = "WORKING"
}