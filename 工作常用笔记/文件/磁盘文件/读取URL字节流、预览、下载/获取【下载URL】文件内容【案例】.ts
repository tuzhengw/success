import { get } from "svr-api/http";
import { getCachedValue, putCachedValue } from "svr-api/memcache";
import { getDataSource } from "svr-api/db";
import { getFile } from "svr-api/fs";
import { getWorkDir } from "svr-api/sys";
// 注意更改一下名字，避免与JS冲突
import File_ = java.io.File;
import HttpClients = org.apache.http.impl.client.HttpClients;
import EntityUtils = org.apache.http.util.EntityUtils;
import HttpGet = org.apache.http.client.methods.HttpGet;

const DATA_SOURCE_NAME = "Vertica";
const client_id = "c335498d82b84132ba860e1f3d2ff852";
const client_secret = "380cee08c8d34ede99859d38caa9779f";
const cacheKey = "HG_IMAGE_TOKEN_CACHE";


function main() {
    // print(getWorkDir() + "/DSJFXPT/data/tables/test.txt");

    let bytes = requestImage("https://jira.succez.com/secure/attachment/177662/177662_%E5%85%B1%E9%81%93%E7%BD%91%E5%85%B3%E5%AF%B9%E6%8E%A5.docx");
	
	
    // 内部调用的是JAVA的FILE，但是工作平台只能看到元数据，JAVA创建的File不会显示
    let file = getFile(getWorkDir() + "/sysdata/public/test.txt");
    let outStream = file.openOutputStream(); // getFile获取的文件对象，才有openOutputStream方法
    try {
        outStream.write(bytes as number[]);
    } catch (e) {
        outStream?.close();
    }
}

/**
 * 20211229 
 * 读取【下载链接】文件内容
 * @param url 文件下载链接
 * @return 文件的【二进制流】数组 【直接打印是没有值的，可以用length查看具体字节大小】
 */
function requestImage(url: string): ArrayLike<number> | undefined | string {
    const client = HttpClients.createDefault();
    try {
        const httpGet = new HttpGet(url);
        const response = client.execute(httpGet);
        print(response)
        try {
            const entity = response.getEntity();
            const contentType = response.getFirstHeader("Content-type").getValue();
            // 下载文件是image
            if (contentType.indexOf("image") === -1) {
                 print(url);
                 const content = EntityUtils.toString(entity);
                 if (content.indexOf("认证失败") !== -1) {
                     return content;
                 }
                 return undefined;
             }
            return EntityUtils.toByteArray(entity);
        } finally {
            response?.close();
        }
    } finally {
        client?.close();
    }
}


import EntityUtils = org.apache.http.util.EntityUtils;
/**
 * 读取【下载链接】文件内容 和 文件名.类型
 * @param url 文件下载链接
 * @return eg: { fileIo: [文件二进制流数组], "test.png" }
 */
function getFileIo(url: string): { result: boolean, fileIo?: ArrayLike<number> | undefined | string, fileName?: string } {
    print("读取【下载链接】文件内容 和 文件名.类型");

    let resultInfo = { result: false, fileIo: [], fileName: "" };
    let client = HttpClients.createDefault();
    let response;
    try {
        let httpGet = new HttpGet(url);
        response = client.execute(httpGet);
        /**
         * 20220927 tuzw
         * 获取文件名，不可使用getValuegetValue来获取，改为直接使用toString()方法将其对象转换为字符串
         * let fileName = response.getFirstHeader("Content-Disposition").getValuegetValue().split(';')[1].split('=')[1];
         * class org.apache.http.message.BufferedHeader ——> Content-Disposition: inline; filename="926/0-220925-wx-207109-1.jpg"
         */
        let contentDisposition = response.getFirstHeader("Content-Disposition").toString();
        let fileName = contentDisposition.split(';')[1].split('=')[1];

        let entity = response.getEntity();
        let contentType = response.getFirstHeader("Content-type");
        let fileType: string = "";
        if (!!contentType) { // 考虑到zip文件，不包含：Content-type，此处需要进行空校验，若不包含，则直接根据文件名来匹配文件类型
            fileType = contentType.getValue();
        } else {
            fileType = fileName; // 通过indexOf来匹配文件名所包含文件类型，故此处直接将文件名赋值给文件类型
        }
        if (fileType.indexOf("image") !== -1 || fileType.indexOf("pdf") !== -1 ||
            fileType.indexOf("jpg") !== -1 || fileType.indexOf("png") !== -1 || fileType.indexOf("mp4") !== -1
            || fileType.indexOf("jpeg") !== -1 || fileType.indexOf("zip") !== -1 || fileType.indexOf("bmp") !== -1) {
            /**
             * 20220208 tuzw 
             * 对方文件夹含有大量的中文名，直接获取是一串乱码，需要将其转换为utf-8
             */
            let encodeFileName = new java.lang.String(fileName.getBytes("ISO-8859-1"), "utf-8");
            resultInfo = {
                result: true,
                fileIo: EntityUtils.toByteArray(entity),
                fileName: encodeFileName.replace(/\"/g, "") // 去除字符串左右‘双引号’
            }
        } else {
            let content = EntityUtils.toString(entity);
            if (content.indexOf("认证失败") !== -1) {
                print("认证失败");
                print(content);
            }
        }
    }
    finally {
        response && response.close();
        client && client.close();
    }
    return resultInfo;
}