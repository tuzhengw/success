import etl from "svr-api/etl";
import dw from "svr-api/dw";
import JavaArray = java.util.Arrays;


/**
 * 返回脚本节点字段结构。
 * 使用场景：
 * 1. 通过脚本爬取数据到数据仓库，需要生成定义好的字段结构。
 * 2. 通过脚本解析json数据，需要预解析几行数据生成字段。
 */
function onProcessFields(context: IDataFlowScriptNodeContext): DbFieldInfo[] {
    let outFields: DbFieldInfo[] = context.getInput().getFields();
    outFields.add({ //  java.util.ArrayList
        name: "勾选内容描述",
        dbfield: "checkedDesc",
        dataType: FieldDataType.C,
        length: 200
    });
    return outFields;
}

/**
 * 参考：https://jira.succez.com/browse/CSTM-19670
 
 * 返回脚本节点的数据结构。
 * 可以直接返回DbTableInfo类型的数据，ddl存储查询的sql,table存储的是对应的物理表名。也可以返回一个二维数组。
 * 使用场景：
 * 1. 调用api处理gis数据更新，并需要将处理后的物理表输出为模型。
 * 2. 调用api查询数据，读取仪表板或报表的查询条件作为参数，并将实时查询结果返回。
 */
function onProcessData(context: IDataFlowScriptNodeContext): DbTableInfo | any[][] {
    let rows: ( string| number| boolean)[][] = [];
    let administrationInfo: JSONObject = queryAdministrationCode();
    /** 上个节点对象 */
    let inputNode:FlowScriptNodeInputData = context.getInput();
	/** 上个节点字段个数 */
	let rowFieldNums: number = inputNode.getFields().length;
    while(inputNode.hasNext()) {
        /** 上个节点行数据(从上个节点首行数据开始查询) */
        let inputRow = inputNode.nextRow();
		let row: ( string| number| boolean)[] = [];
		for(let i = 0; i < rowFieldNums; i++) { // 记录原本行数据
			row.push(inputRow[i]);
		}
        /** 行政区划代码, eg: 110000,110100 */
        let checkedCode: string = inputRow[1];
        if(!!checkedCode) {
            let checkedDesc: string[] = [];
            let checkedIds:string[] = checkedCode.split(",");
            for(let i = 0; i < checkedIds.length; i++) { // 循环依次寻找行政区划代码对应的描述
                let code: string = checkedIds[i];
                let desc: string = administrationInfo[code];
                checkedDesc.push(desc);
            }
            row.push(checkedDesc.join(",")); // 将描述值添加到新增字段值中
        }
        rows.push(row);
    }
	return rows;
}


/**
 * 查询行政区划代码和描述的映射关系
 * @return {
 *    "110000": "北京市",
 *    "110100": "北京市市辖区",
 *    ...
 * }
 */
function queryAdministrationCode() : JSONObject {
    let query: QueryInfo = {
        sources: [{
            id: "model1",
            path: "/DEMO/data/tables/通用/GEN_DIM_XZQHFZ.tbl" // 模型路径
        }],
        fields: [{ // 查询的模型字段
            name: "ADCODE", exp: "model1.ADCODE"
        }, {
            name: "NAME", exp: "model1.NAME"
        }]
    }
    let queryData = dw.queryData(query).data;
    let administrationInfo: JSONObject = {};
    for(let i = 0; i < queryData.length; i++) {
        let data = queryData[i] as string[];
        let code: string = data[0];
        let name: string = data[1];
        administrationInfo[code] = name; // 设置区划代码和描述的映射关系
    }
    return administrationInfo;
}