/**  导入的路径在产品代码的的最上方 */
import { SZEvent, Component, rc, browser, showInfoDialog, showWaiting, downloadFile, showMenu, deepEqual, showSuccessMessage, showErrorMessage, BASIC_EVENT, showConfirmDialog } from 'sys/sys';
import { ICustomJS, IMetaFileCustomJS, InterActionEvent, IVPage, IAppCustomJS, DatasetDataPackageRowInfo, IVComponent, InputDataChangeInfo, IFAppCustomJS } from "metadata/metadata-script-api";
import { TableCellEditor } from "commons/table";
import { Paginator } from "commons/basic";
import { SuperPageBuilder } from "app/superpage/superpagebuilder";
import { SpgListBuilder, SpgListColumnBuilder } from "app/superpage/components/tables";
import { BaseNodeBuilder } from "metadata/bo/builder";
import { AnaDataset } from "ana/datamgr";
import { showMetaFileDialog, showDrillPage, getCurrentUser, IMetaFileViewer, CurrentUser, MetaFileViewer } from 'metadata/metadata';
import { IExpEvalDataProvider } from "commons/exp/expeval";

/**
 * 20220208 tuzw
 * 获取列表勾选的数据
 * let choseImportData = event.component.getComponent("list1").getCheckedDataRows();
 */


/** 保留列表选择的节点值（唯一，仅保留筛选的主键字段值）*/
let save_chose_all_nodes: Array<FilterHospitalConditions> = [];
/** 保留列表选择的所有行下标值 */
let save_chose_all_indexs: Array<number> = [];

/** 
 * List列表导出配置
 */
const IMPORT_CONFING_CONTENT = {
    /** 导出的SPG地址 */
    "/rlzy/app/rlzy.app/rsxx/yy/test.spg": {
        /** 筛选数据的目标list的id值（勾选导出数据） */
        "targetListId": "list1",
        /** 导出列，参照的list的id值（导出那些列），必须跟：targetListId 引入同一个数据库源 */
        "importListId": "list2"
    },
    "/rlzy/app/rlzy.app/rsxx/mz/test.spg": {
        "targetListId": "list1",
        "importListId": "list2"
    },
    "/rlzy/app/rlzy.app/rsxx/cwss/test_copy.spg": {
        "targetListId": "list1",
        "importListId": "list2"
    },
    "/rlzy/app/rlzy.app/rsxx/zs/test_copy.spg": {
        "targetListId": "list1",
        "importListId": "list2"
    },
    "/rlzy/app/rlzy.app/rsxx/yy/ckrs.spg": {
        "targetListId": "list1",
        "importListId": "list2"
    },
    "/rlzy/app/rlzy.app/rsxx/mz/ckrs.spg": {
        "targetListId": "list1",
        "importListId": "list2"
    },
    "/rlzy/app/rlzy.app/rsxx/zs/ckrs.spg": {
        "targetListId": "list1",
        "importListId": "list2"
    },
    "/rlzy/app/rlzy.app/rsxx/cwss/ckrs.spg": {
        "targetListId": "list1",
        "importListId": "list2"
    }
}

export const CustomJS: { [type: string]: IMetaFileCustomJS } = {
    /**
     * 当前项目的所有spg都会走下方逻辑
     */
    spg: {
        /** CustomActions主要是支持：SPG内部组件的交互 */
        CustomActions: {
            /** 
             * 20211027 tuzw
             * 功能：导出选中的部分列表数据
             * （可包含全部，由于SPG列表本身有很多过滤条件，若全部导出，则走原有逻辑）
             * 问题：上海人力系统测试环境，需要支持只导出勾选后内容
             * 帖子地址：https://jira.succez.com/browse/CSTM-16790
             * 思路：使用产品的exportQuery方法进行导出
             */
            importPartListDatas: (event: InterActionEvent) => {
                /**
                 * 若是全部导出，则走原来的导出交互
                 * （SPG里面设置两个交互，上面走定制脚本，若定制脚本返回false，则走原来的导出流程）
                 * return false：全部导出
                 */
                if (save_chose_all_nodes == null || save_chose_all_nodes.length == 0) {
                    return false;
                }
				/**
				 * 两种方式获取页面的控件
				 * 1. event.page.getComponent("fieldsFilter1") : 获取页面的UI控件（当前页面输入的值、控件本身的值等）
				 * 2. event.component && event.component.node.builder.getComponent("fieldsFilter1") : 获取构建的数据控件（包含控件的属性、大小、与其他组件的关系等）
				 */
                let superPageBuilder: SuperPageBuilder = event.component && event.component.node.builder;
                /** SPG路径 */
                let spgPath = superPageBuilder.getFileInfo().path;
                /** 动态获取列表显示的列以及对应数据库字段值 */
                let fields = getDisplayColumnNames(superPageBuilder, spgPath);
                /** 导出文件名 */
                let importFileName = event.params["importFileName"]
                    ? event.params["importFileName"]
                    : superPageBuilder.getFileInfo().desc;
                let disPlayListObj: SpgListBuilder = superPageBuilder.getComponent(IMPORT_CONFING_CONTENT[spgPath]["targetListId"]) as SpgListBuilder;
                /** 获取模型表的ID */
                let modelId = disPlayListObj.getQueries()[0].getMainModel().getId();
                /** 导出的模型路径 */
                let importModelPath = disPlayListObj.getQueries()[0].getMainModel().getRefDwTablePaths().toString();
                /** SPG的resid */
                let spgResid = superPageBuilder.getResid();
                /** SPG的模型的数据对象（包含：模型表的主键），动态获取List引入模型表主键集 */
                let currentModelKeys: Array<string> = superPageBuilder.getData().getDataset(modelId).getPrimaryKeys();;
                // /** 获取过滤条件，没有勾选表示：全部导出 */
                // let filterConditions: Array<FilterInfo> = [];
                // if (save_chose_all_nodes.length == 0) {
                //     /** 获取SPG上的字段过滤组件 */
                //     let fieldsFilter1: IVComponent = event.page.getComponent("fieldsFilter1");
                //     if (fieldsFilter1 != undefined) {
                //         let filterExp: string = fieldsFilter1.values.filterExp;
                //         filterConditions.push({ exp: filterExp });;
                //     }
                // } else {
                //     filterConditions = produceFilterCondition(currentModelKeys, modelId) as Array<FilterInfo>;
                // }
                /** 
					获取过滤条件，若没有勾选数据，则走原有逻辑 
					20211201 tuzw
					改进：
					 （1）不用通过脚本获取主键，直接通过前端传递过来（列表.选中值）
				*/
                let filterConditions: Array<FilterInfo> = produceFilterCondition(currentModelKeys, modelId) as Array<FilterInfo>;
                let fieldQueryInfo: QueryInfo = {
                    /** 导出后的列表名和对应的数据库字段 */
                    fields: fields,
                    sources: [{
                        id: modelId, /** eg：模型表别名（model1），这里的别名需要跟fields中的exp表达式一致 */
                        path: importModelPath
                    }],
                    filter: filterConditions  /** 过滤条件 */
                }
                return import("dw/dwapi").then((m) => {
                    m.exportQuery({
                        /** 模型表的resid主键，在查看路径的地方查看 */
                        resid: spgResid,
                        /** 查询条件（包含：查询字段、过滤条件、查询表） */
                        query: fieldQueryInfo,
                        fileFormat: DataFormat.xlsx,
                        fileName: importFileName
                    });
                });
            },
            /**
             * 将脚本的值返回给SPG
             * 思路：
             * （1）在SPG创建一个：文本控件
             * （2）getComponent("id号")获取到：文本控件
             * （3）将脚本返回的值 赋值给 文本控件的：value
             */
            returnScriptValueToSPG: (event: InterActionEvent) =>{
                let testComponent = event.component.getComponent("text13");
                console.log(testComponent);
                testComponent.setValue("我是大帅比");
                console.log(testComponent.getValue());
            }
        },
        /**
         * onRender放置到spg内
         * 1. 这样当前项目所有的spg若需要使用都直接可以调用，不用重复复制多次代码
         * 2. onRender每次页面变动都会执行，可能会导致某个固定值随机
         * eg：某页页码，因为加载n次，可能最终获取的页面值不是最终想要的，可以在变动的位置：重新捕获
         * 
         * 20211027 tuzw
         * 问题：上海人力系统测试环境，需要支持只导出勾选后内容
         * 帖子地址：https://jira.succez.com/browse/CSTM-16790
         * 思路：
         * 1. 在onRender这个spg扩展点中实现该功能，获取到列表控件的tableEditor对象，重新实现这个对象的oninlinecompchange 方法，这个方法是会在点击了列表勾选框会触发。
         * 2. 可以获取到勾选前和勾选后的差异，从而可以获取到本次勾选了哪些列，取消勾选了哪些列
         * 3. 使用全局数据来存储所有选中的下标和行数据，根据勾选前后差异，同步更新全局数组
         * 
         * 4. 考虑到可能存在多个SPG调用此定制，而且每个SPG的list引入的模型表不同，将动态获取对应List引入的模型表主键（过滤条件）
         */
        onRender: (event: InterActionEvent): Promise<void> => {
            let superPageBuilder: SuperPageBuilder = event.component && event.component.node.builder;
            /** 当前SPG路径 */
            let spgPath = superPageBuilder.getFileInfo().path;
            /** 由于所有SPG都会调用onRender，这里校验是否为指定的SPG */
            if (IMPORT_CONFING_CONTENT[spgPath] != undefined) {
                let disPlayListObj: SpgListBuilder = superPageBuilder.getComponent(IMPORT_CONFING_CONTENT[spgPath]["targetListId"]) as SpgListBuilder;
                /** 获取模型表的ID */
                let modelId = disPlayListObj.getQueries()[0].getMainModel().getId();
                /** SPG的模型的数据对象（包含：模型表的主键），动态获取List引入模型表主键集 */
                let currentModelskeys: Array<string> = superPageBuilder.getData().getDataset(modelId).getPrimaryKeys();
                /** 
					若当前SPG存在列表组件，则获取最后一个列表（显示状态）的id值，eg：list1
				 */
                let id = event.component && event.component.getId();
				
                /** 
                 * 显示：list1，导出：list2（必须跟list1来源同一个数据源）
                 * 因为导出数据最终都是导出数据库的数据，列表的数据仅作筛选条件，不是最终导出的数据
                 * 导出以list2显示的列为主，也就是：list2，仅用于读取导出的列名集合，没有实际作用
                 * 筛选数据来源：list1，导出那些列来源：list2
                 */
				 
				 // 校验当前SPG的列表ID值 是否 跟配置文件中的targetListId值 相等
                if (id == IMPORT_CONFING_CONTENT[spgPath]["targetListId"]) {
                    console.log(`==list列表初始化onRender`);
                    let comp: IVComponent = event.component; // 由于外部校验必须是：列表控件才可以进入，这里的component就是列表的【数据对象】
                    /** 列表控件 */
                    let tableEditor = comp.component.tableEditor;
                    /** 分页控件 */
                    let paginator: Paginator = comp.component.paginator;
                    /** 页码 */
                    let pageIndex = paginator.getPageIndex();
                    /** 页面大小 */
                    let pageSize = paginator.getPageSize();

                    if (tableEditor) {
						// 保留原有列表控件的：oninlinecompchange方法
                        let oninlinecompchange_old = tableEditor.oninlinecompchange;
                        /**
                         *      页面加载时，需要校验是否有选中的节点
                         *      避免：页面加载后，选择节点，但由于页面已经加载过了，不会在走这部分逻辑，
                         * 导致页面虽然选择了，但实际没有获取到选择的行数
                         */
                        if (save_chose_all_indexs.length != 0) {
                            /**
                             * 由于每次刷新页面后，再返回时，原有的数据不会被勾选
                             * 故：手动调用comp.setCheckedDataRows()自动将相应的列进行勾选
                             * 勾选后，即可获取之前在《当前页》所选择的节点
                             */
                            let currentPageChosedNodesIndex: Array<number> = [];
                            /**
                             * 因：每页下标都是：0——99（假设每页100行），故：
                             * 某行下标值：当前页码 * 页面大小 + 当前行的下标值（0——99）
                             * 获取第几页第几行下标：校验公式：行下标值 / 页面大小 == 当前页码，若为：true，则为当前页所选择的行
                             *
                             *  eg：第二页第一个元素下标为：100+20，获取时，(100+20)%100 = 1（第1页，切下标为：20）
                             */
                            for (let i = 0; i < save_chose_all_indexs.length; i++) {
                                let currentNodePageIndex = Math.floor(save_chose_all_indexs[i] / pageSize);
                                if (currentNodePageIndex == pageIndex) { // 下标就是0-100，这里为了区分是那页的，存的是页码*页大小+下标位置，故重新渲染需要减去
                                    currentPageChosedNodesIndex.push(save_chose_all_indexs[i] - currentNodePageIndex * pageSize);
                                }
                            }
							// listComponent.component.setCheckedRows(currentListChosedNodesIndex);
                            comp.setCheckedDataRows(currentPageChosedNodesIndex);
                        }
                        /**
                         * 重写oninlinecompchange 方法（会在点击了列表勾选框会触发）
                         * 可通过：console.log 将 event、cell、checkComp三个对象打印出来，查看内部属性和方法
                         * 
                         * 首次，进入onRender后，会创建tableEditor对象，该对象不会被销毁，同时会给每一行创建oninlinecompchange方法
                         * 其次，避免每次点击行后，进入onRender都会创建oninlinecompchange方法，为了避免重复创建，这里新增了一个：hasOnRender属性（boolean）
                         */
                        if (!tableEditor.hasOnRender) {
                            tableEditor.hasOnRender = true;
                            tableEditor.oninlinecompchange = (event: SZEvent, cell: TableCellEditor, checkComp: any) => {
                                /** 避免多次onrender，导致页码存在多个值，这里重新捕获最终的页码，todo，通过get获取：pageIndex */
                                pageIndex = paginator.getPageIndex();
                                /** 获取之前在当前页所选中的节点下标 */
                                let lastCheckIndexs: Array<number> = comp.component.lastCheckedRows;
                                let lastCompDatas: JSONObject[] = comp.getCheckedDataRows();
                                /** 走原有的方法流程，获取当前页选中的最新节点 */
                                oninlinecompchange_old(event, cell, checkComp);
                                /** 获取当前页所选择的节点下标 */
                                let newCheckIndexs: Array<number> = comp.component.lastCheckedRows;
                                /** 获取当前页所选中节点 */
                                let newCompDatas: JSONObject[] = comp.getCheckedDataRows();

                                deleteAndUpdateChoseNodeIndexs(lastCheckIndexs, newCheckIndexs, pageIndex, pageSize);
                                deleteAndUpdateChoseNodesDatas(lastCompDatas, newCompDatas, currentModelskeys);
                            }
                        }
                    }
                    console.log(`==list列表初始化end`);
                }
            }
            return Promise.resolve();
        }
    }
}


/**
 * 20211027 tuzw
 * 功能：获取导出那些列
 * （与筛选的数据分离，这里仅动态获取指定list显示列）
 * 
 * 注意：
 * 1. 若每个SPG的list引入的列表都不一样，这里根据调用的SPG，动态获取指定list的列名
 * 2. 获取指定列表（对应的模型表）的《列名》和对应的《数据库字段 》
 * @params superPageBuilder：spg页面构建对象
 * @params spgPath：当前调用脚本的SPG路径
 * <pre>
 * eg： fields: [
 *      { name: "姓名", exp: "model1.XM" },
 *      { name: "身份证号码", exp: "model1.SFZJHM" },
 *      ...
 * ]
 * 字段列表，字段顺序和查询定义的查询字段列表一一对应
 * </pre>
 */
function getDisplayColumnNames(superPageBuilder: SuperPageBuilder, spgPath: string): Array<DwTableFieldCompiledInfo> {
    /**  
     * 获取当前页面所有的组件节点，找到list2组件
     * 显示：list1，导出：list2
     */
    let spgListBuilder: SpgListBuilder = superPageBuilder.getComponent(IMPORT_CONFING_CONTENT[spgPath]["importListId"]) as SpgListBuilder;
    /**
     *  改为直接使用内置方法：getComponent("组件名")
     *  let allNodes: BaseNodeBuilder[] = superPageBuilder.allNodes;
        for (let i = 0; i < allNodes.length; i++) {
            if (allNodes[i].getId() == "list2") {
                spgListBuilder = allNodes[i] as SpgListBuilder;
                break;
            }
        }
     */
    let isExistPointUserGroups = checkCurrentUserGroupAuthority();
    /** 获取当前list引入的模型id，eg：model1 */
    let modelId = spgListBuilder.getQueries()[0].getMainModel().getId();
    // let spgListColumnBuilders = spgListBuilder.subComponents.columns as Array<SpgListColumnBuilder>;
    let spgListColumnBuilders = spgListBuilder.getSubComponents() as Array<SpgListColumnBuilder>;
    /** 存储当前（id：list2）列表显示的列 */
    let display_list_columns: Array<DwTableFieldCompiledInfo> = [];
    spgListColumnBuilders.forEach(item => {
        /** 
         * 校验当前列是否为数据库的列（去掉说明列和隐藏列）
         * getOnlyField()：获取唯一引用的字段对象
         */
        if (item.getOnlyField() != undefined && (item.metaInfo.visible == undefined || item.metaInfo.visible == "true")) {
            let temp: DwTableFieldCompiledInfo = {
                /**
                 * 注意：SPG新增列，需要修改标题（若是自动生成，先修改再还原），否则读取不到caption属性
                 *      name值不能为null 或者 undefined，否则导出会报错
                 * 解决办法：
                 *      1. 从全局模型获取（superPageBuilder.getData()）
                 *          原因：若用户没有修改标题，默认从父级获取，不设置caption
                 *      2. 直接调用：getCaption()
                 */
                name: item.getCaption(),
                exp: item.getOnlyField().getId()
            }
            /** 若当前用户为卫健委账号或者管理员账号，则导出文件列包含：卫健委机构、医疗机构 */
            if (temp.exp == `${modelId}.YLJGID` || temp.exp == `${modelId}.WSJID`) {
                if (isExistPointUserGroups) {
                    display_list_columns.push(temp);
                }
            } else {
                display_list_columns.push(temp);
            }
        }
    });
    console.log("当前列表可导出的字段为");
    console.log(display_list_columns);
    return display_list_columns;
}



/**
 * 校验当前用户组是否属于：卫健委机构 或 医疗机构列
 * @return boolean
 */
function checkCurrentUserGroupAuthority(): boolean {
    /** 指定用户组ID集 */
    const adminGroupIds = ["admin", "wjwyh"];
    /** 当前用户所在的用户组ID集 */
    let currentUserGroupIds = getCurrentUserGroupIds();
    for (let i = 0; i < currentUserGroupIds.length; i++) {
        if (adminGroupIds.indexOf(currentUserGroupIds[i]) != -1) {
            return true;
        }
    }
    return false;
}

/**
 * 获取当前登录用户所在用户组的ID集
 * 
 * eg:[{
 *      enabled: true
        groupId: "wjwyh"
        groupName: "卫健委"
 *    },{
 *      enabled: true
        groupId: "admin"
        groupName: "admin"
 * }]
 * 转换为：["wjwyh", "admin"]
 */
function getCurrentUserGroupIds(): Array<string> {
    let currentUserGroups: UserGroupInfo[] = getCurrentUser().groups;
    let currentUserGroupIds: Array<string> = currentUserGroups.map(item => {
        return item.groupId;
    });
    return currentUserGroupIds;
}

/**
 * 20211028 tuzw
 * 
 * 功能：根据选择的所有行数据（联合主键），生成数据库过滤条件
 * @params currentModelskeys：模型表主键集
 * @params modelId：模型表的ID
 * 
 * 注意：必须要确保筛选的数据不为：undefined，也就是页面需要设置对应的列，否则，无法读取
 * eg：
 * [{
 *  exp: (model1.SFZJHM = '642224199610043424' and  model1.SJQQ = '20211026' and model1.SJQZ = '20211026')
 *        or
 *       (model1.SFZJHM = '642224199610043424' ...
 * }]
 */
function produceFilterCondition(currentModelskeys: Array<string>, modelId: string): Array<FilterInfo> {
    /** 若没有勾选，则表示将指定列表的所有数据导出 */
    if (save_chose_all_nodes == null || save_chose_all_nodes.length == 0) {
        return null;
    }
    let expFilterConditions: Array<FilterInfo> = [];
    let queryFilterConditions = [];
    /**
     * 20211028 因为设计到多个模型表，改为：动态处理
     * save_chose_all_nodes.forEach(item => {
            let temp = `(model1.SFZJHM = '${item.SFZJHM}' and  model1.SJQQ = '${item.SJQQ}' and model1.SJQZ = '${item.SJQZ}') `;
            queryFilterConditions.push(temp);
        });
     */
    /** 考虑到每个模型表的主键不同，这里根据主键，动态生成过滤条件 */
    save_chose_all_nodes.forEach(item => {
        let temp: Array<string> = [];
        currentModelskeys.forEach(key => {
            /**
             * eg：model1.UUID = '310000000190310105761028321'
             */
            temp.push(`${modelId}.${key} = '${item[`${key}`]}'`);
        });
        queryFilterConditions.push(temp.join(" and "));
    });
    // console.log("--动态生成的过滤条件为：--");
    // console.log(queryFilterConditions.join(" or "));
    expFilterConditions.push({ exp: queryFilterConditions.join(" or ") });
    return expFilterConditions;
}

/**
 * 20211026 tuzw
 * 将最新选中的行下标与之前选中的行下标数组对比差异
 * 删除已取消节点下标，并将最新选择的节点下标添加到全局数组（save_chose_all_indexs）
 * @params lastCheckIndexs：之前选中的节点下标集合
 * @params newCheckIndexs：当前重新选择的节点下标集合
 * @params pageIndex：当前页码
 * @params pageSize：当前页显示行数
 */
function deleteAndUpdateChoseNodeIndexs(lastCheckIndexs: Array<number>, newCheckIndexs: Array<number>, pageIndex: number, pageSize: number): void {
    if (lastCheckIndexs == null || newCheckIndexs == null) {
        return;
    }
    /**
     * 将最新选中的行下标与之前选中的行下标数组对比差异
     * （1）将取消的节点存入endChoseNodeIndexs集合中
     */
    /** 存入前后选择节点的差异节点 */
    let removeNodeIndexs: Array<number> = [];
    for (let i = 0; i < lastCheckIndexs.length; i++) {
        /**
         * 若节点已被取消，则将其放置删除节点数组
         */
        let nodeInNewCheckIndex = newCheckIndexs.indexOf(lastCheckIndexs[i]);
        if (nodeInNewCheckIndex == -1) {
            removeNodeIndexs.push(lastCheckIndexs[i] + pageIndex * pageSize);
        }
    }
    /** 将取消的节点下标从全局节点下标数组中去除 */
    for (let i = 0; i < removeNodeIndexs.length; i++) {
        let deleteNodeIndex = save_chose_all_indexs.indexOf(removeNodeIndexs[i]);
        if (Math.floor(removeNodeIndexs[i] / pageSize) == pageIndex && deleteNodeIndex != -1 && deleteNodeIndex >= 0) {
            save_chose_all_indexs.remove(deleteNodeIndex);
        }
    }
    for (let i = 0; i < newCheckIndexs.length; i++) {
        let currentPageNodeIndex = newCheckIndexs[i] + pageIndex * pageSize;
        /** JS 的number做：1/100 不是取整的意思 */
        /** Math.floor(key / pageSize) == pageIndex：onrender可能会执行多次，这里校验必须是所选择页码 */
        if (save_chose_all_indexs.indexOf(currentPageNodeIndex) == -1 && Math.floor(currentPageNodeIndex / pageSize) == pageIndex) {
            save_chose_all_indexs.push(currentPageNodeIndex);
        }
    }
}

/**
 * 列表医院人事信息数据过滤对象（主键进行过滤）
 * 
 * 由于JS获取的行对象不是一个规范值，不易比较，将其内过滤数据单独提出并封装
 */
interface FilterHospitalConditions {
    /**
     * 身份证
     */
    SFZJHM?: string;
    /**
     * 数据期起
     */
    SJQQ?: string;
    /**
     * 数据期止
     */
    SJQZ?: string;
    /**
     * UUID
     */
    UUID?: string;
    /**
     * 自定义key 任意值
     */
    [propname: string]: any,
}

/**
 * 20211026 tuzw
 * 将最新选中的行值与之前选中的行值数组对比差异
 * 删除已取消节点行值，并将最新选择的节点值添加到全局数组（save_chose_all_nodes）
 * @params lastCompDatas：之前选中的节点值集合
 * @params newCompDatas：当前重新选择的节点值集合
 * @parmas currentModelskeys：模型表主键集
 */
function deleteAndUpdateChoseNodesDatas(lastCompDatas: JSONObject[], newComDatas: JSONObject[], currentModelskeys: Array<string>): void {
    if (lastCompDatas == null || newComDatas == null) {
        return;
    }
    /**
     * 考虑到JS对象过于复杂（内部嵌套多个对象），不易比较
     * 这里需要将其转换为类似JAVA对象，仅保留指定字段值
     */
    let lastChoseNodesDatas: Array<FilterHospitalConditions> = [];
    let nowChoseNodesDatas: Array<FilterHospitalConditions> = [];
    lastCompDatas.forEach(item => {
        let temp: FilterHospitalConditions = {};
        /** 考虑到每个模型表主键不同，这里循环赋值 */
        currentModelskeys.forEach(key => {
            /**
             * temp.key：表示给item属性赋值，会覆盖前面的值，改为：temp[`${key}`]
             * eg：['SFZJHM', 'SJQQ', 'SJQZ']
             * 变成：
             *  SFZJHM: "SFZJHM"
                SJQQ: "SJQQ"
                SJQZ: "SJQZ"
             */
            temp[`${key}`] = item[`${key}`];
        });
        lastChoseNodesDatas.push(temp);
    });
    newComDatas.forEach(item => {
        let temp: FilterHospitalConditions = {};
        /** 考虑到每个模型表主键不同，这里循环赋值 */
        currentModelskeys.forEach(key => {
            /**
             * temp.key：表示给item属性赋值，会覆盖前面的值，改为：temp[`${key}`]
             * eg：['SFZJHM', 'SJQQ', 'SJQZ']
             * 变成：
             *  SFZJHM: "SFZJHM"
                SJQQ: "SJQQ"
                SJQZ: "SJQZ"
             */
            temp[`${key}`] = item[`${key}`];
        });
        nowChoseNodesDatas.push(temp);
    });
    /** 将最新选中的节点值与之前选中的节点值数组对比差异  */
    /** 存入前后选择节点的差异节点 */
    let removeNodeDatas: Array<FilterHospitalConditions> = [];
    lastChoseNodesDatas.forEach(lastItem => {
        let isRemoveNode = true;
        for (let i = 0; i < nowChoseNodesDatas.length; i++) {
            /** 对象之间比较转换为：JSON后比较字符串（若对象内嵌套对象，则不能使用：JSON.stringify） */
            if (JSON.stringify(lastItem) == JSON.stringify(nowChoseNodesDatas[i])) {
                /** 
                 * todo
                 * 若节点未被删除，从当前选择节点去除，避免重复添加
                 * nowChoseNodesDatas.remove(i);
                 * 由于onRender执行多次，导致这里执行了N次，可能一个选项可能会重复选择多次，这种方法不可取
                 */
                isRemoveNode = false;
                break;
            }
        }
        if (isRemoveNode) {
            removeNodeDatas.push(lastItem);
        }
    });
    /** 将取消的节点从全局节点值数组中去除，JS中的数组长度会自动调整 */
    removeNodeDatas.forEach(removeItem => {
        for (let i = 0; i < save_chose_all_nodes.length; i++) {
            if (JSON.stringify(save_chose_all_nodes[i]) == JSON.stringify(removeItem)) {
                save_chose_all_nodes.remove(i);
                break;
            }
        }
    });
    /** 将当前所选节点值存入全局数组：save_chose_all_nodes */
    nowChoseNodesDatas.forEach(item => {
        save_chose_all_nodes.push(item);
    });
    /**
     * 利用set去重
     * set仅针对基本数组，对象数组无法去重（JS）
     * save_chose_all_nodes = [... new Set(save_chose_all_nodes)];
     * 
     * 由于onRender执行多次，导致这里执行了N次，可能一个选项可能会重复选择多次，必须对其去重
     */
    save_chose_all_nodes = removeRepeatDatas(save_chose_all_nodes);
}


/**
 * 去除对象数组的重复值
 * 注意：必须保证对象内没有包含：对象
 * @params obj：需要处理的对象数组
 * @return uniques：去除重复值后的对象数组
 */
function removeRepeatDatas(objArray) {
    let uniques = [];
    let stringify = {};
    for (let i = 0; i < objArray.length; i++) {
        let keys = Object.keys(objArray[i]);
        keys.sort(function (a, b) {
            return (Number(a) - Number(b));
        });
        let str = '';
        for (let j = 0; j < keys.length; j++) {
            str += JSON.stringify(keys[j]);
            str += JSON.stringify(objArray[i][keys[j]]);
        }
        if (!stringify.hasOwnProperty(str)) {
            uniques.push(objArray[i]);
            stringify[str] = true;
        }
    }
    uniques = uniques;
    return uniques;
}

