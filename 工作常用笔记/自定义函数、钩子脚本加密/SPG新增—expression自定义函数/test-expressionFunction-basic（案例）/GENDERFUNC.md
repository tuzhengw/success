---
permalink: /exp/func/genderfunc
sidebarDepth: 0
---
# **GENDERFUNC([*p_id*,] property)**

根据身份证号获取性别

**获取性别**

## 参数

***p_id***：必须，居民18位身份证号。

## 示例

1. `GENDERFUNC("420100********1112")` 返回 男，表示男性
