
/**
 *  第一步：找到修改的DOM位置，查看：父DOM的class：toolbar-item 和 自己的id：saveSoftLabel
 
	注意：若父模块的class【包含多个】，eg：mobile spgpanel-base spgcomponent spgcomponent-container panel-style7 sb141b33c9bb108d85a
	   可以只取其中几个，eg：mobile spgpanel-base ，只要唯一标识就可以
 */
<div id="toolbar" class="toolbar-base dw-viewer-toolbar toolbar-leftdesc flexlayout-item" style="height: 43px; top: 0px;">
	<span class="toolbar-block">
		<div class="toolbar-item">
			<div id="fields" class="combobox-base dw-viewer-fieldscombobox toolbar-component dw-viewer-toolbar-button" tabindex="0">
				<div class="combobox-caption">全部</div><input class="combobox-input" tabindex="-1" readonly="readonly" style="cursor: pointer; display: none;">
				<span class="combobox-arrow icon-lowtrip"></span>
			</div>
			<span class="toolbar-desc">选择字段：</span>
		</div>
		
		<div class="toolbar-item">
			<span id="refresh" class="button-base defbtn toolbar-component dw-viewer-toolbar-button" tabindex="0">查询</span>
			<span class="toolbar-desc"></span>
		</div>
		<div class="toolbar-item">
			<span id="saveSoftLabel" class="button-base toolbar-component dw-viewer-toolbar-button" tabindex="0">保存条件为软标签</span>
			<span class="toolbar-desc"></span><
		/div>
	</span>
</div>


/**
  第三步：追加 【保存条件为软标签】 样式
  toolbar-item：dom的上一级父dom的class名，也可更改为：id号
  saveSoftLabel：自己的id号，也可更改为：class名
*/
.toolbar-item #saveSoftLabel {
  // !important：提升指定样式规则的应用优先权
  display: none!important;
}

/**
 * 注意：
 * 1）多个class不能直接复制，eg：class=edit-base want-resizeevent forminputwidth-full 转为： .edit-base .want-resizeevent .forminputwidth-full
 * 2）父子级之间间距大小无影响
 * 3）每个页面的DOM的ID值是唯一的，但是要注意与其他页面是否冲突，可以多增加几个id或者class 使之唯一
 * 4）命名不要包含：'.'（逗号）
 */
// 隐藏系统设置-阈值设置
#thresholds {
   display: none!important;
}

// 父模块可以取几个，eg：mobile spgpanel-base spgcomponent spgcomponent-container panel-style7 sb141b33c9bb108d85a
// 取：mobile spgpanel-base ，只要唯一标识就可以

.mobile .spgpanel-base .spgcomponent-bg { // 我的消息模块 背景透明（注意：spgcomponent-bg所有spg通用）
  background-color: transparent !important;
}



/**
 * 隐藏模型下所有文件夹，显示模型下【sys、etl】 
 * 考虑到设置隐藏后，对应文件夹只是不显示前端，仍然存在前端，【占据本身的位置：留个空白面积】，可将【显示】的文件夹【拖】到最上方
 */
#DNru3nwgs3KjNjnG3vmYIE {
    display: none!important;
}
#Ng8AQ1yA5mGoFyMZNpY4eC  {
    display: none!important;
}
#IcrVUS4yufDS4AKJcs0JAG  {
    display: none!important;
}
#LBxavY7diSDgKj6FFVt2AE {
    display: none!important;
}


/**
 * 指定某个SPG 或者 表单生效
 */
#数据标准维护 .fapp-default .fappform-base  .fappform-bg {
    background: url(images/standard-word-images/数据表管理背景.png) center center / contain no-repeat !important;
}