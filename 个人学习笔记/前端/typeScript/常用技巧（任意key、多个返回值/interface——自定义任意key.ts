interface FilterHospitalConditions {
    /**
     * 身份证
     */
    SFZJHM?: string;
    /**
     * 数据期起
     */
    SJQQ?: string;
    /**
     * 数据期止
     */
    SJQZ?: string;
    /**
     * UUID
     */
    UUID?: string;
    /**
     * 自定义key 任意值
     */
    [propname: string]: any,
}
