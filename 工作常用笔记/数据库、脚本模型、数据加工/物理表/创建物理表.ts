
import { getDataSource, getDefaultDataSource } from 'svr-api/db';

function createDbTable(): void {
    print(`-- 通过SQL创建两张【测试表】，并给字段添加描述信息`);
    let datasource = getDefaultDataSource();
    let defaultSchema = datasource.getDefaultSchema();
    let tableFields: TableFieldMetadata[] = [{
        name: "YEAR",
        desc: "日期",
        dataType: FieldDataType.C,
        length: 20
    }, {
        name: "MONTH",
        desc: "日期",
        dataType: FieldDataType.C,
        length: 20
    }, {
        name: "DAY",
        desc: "日期",
        dataType: FieldDataType.C,
        length: 20
    }];
    for (let i = 0; i < testFieldDescTables.length; i++) {
        let createDbTableName: string = testFieldDescTables[i];
        if (!datasource.isTableExists(createDbTableName)) {
            datasource.createTable({
                tableName: createDbTableName.toLocaleUpperCase(),
                schema: defaultSchema,
                desc: `数据接入数据加工测试表_${i + 1}`,
                fields: tableFields,
            });
            print(`测试物理表：${createDbTableName} 创建成功`);
        }
    }
}