import HttpClients = org.apache.http.impl.client.HttpClients;
import EntityUtils = org.apache.http.util.EntityUtils;
import HttpGet = org.apache.http.client.methods.HttpGet;
import File_ = java.io.File; // 工具类不需要对象，直接import导入

import ZipOutputStream = java.util.zip.ZipOutputStream;
import ZipEntry = java.util.zip.ZipEntry;
import ArrayUtils = org.apache.commons.lang3.ArrayUtils;

import FileOutputStream = java.io.FileOutputStream;
import FileInputStream = java.io.FileInputStream;
import Files_ = java.nio.file.Files;
import Paths_ = java.nio.file.Paths;
import IOUtils = org.apache.commons.io.IOUtils;

const GdGenericRequest = com.gongdao.yuncourt.security.model.GdGenericRequest; // 通过jar包内部源码查看
const GdApiClient = com.gongdao.yuncourt.security.GdApiClient;
const GdGenericResponse = com.gongdao.yuncourt.security.model.GdGenericResponse;
const GdApiException = com.gongdao.yuncourt.security.exception.GdApiException;
const GDClientV2Impl = com.gongdao.yuncourt.security.impl.GDClientV2Impl;
const JSONObject = com.alibaba.fastjson.JSONObject;
const JSONArray = com.alibaba.fastjson.JSONArray;
const JSON_ = com.alibaba.fastjson.JSON;  // 避免与前端JSON对象冲突

import { getJSON, getString, getDwTable, modifyFile } from 'svr-api/metadata';
import { queryDwTableData, queryData, openDwTableData } from "svr-api/dw";
import { uuid } from "svr-api/utils";
import { getDataSource } from "svr-api/db"; //数据库相关API
import { getFile, deleteFile } from "svr-api/fs";
import { getWorkDir, ServerEvent, fireEvent } from "svr-api/sys";


/**
 * 多个文件压缩测试
 */
function test_zipAllFiles(): void {
    let workDir = getWorkDir();
    let tempFileDirPath: string = `${workDir}/2022-01-04/tempSaveZipFile/`;
    let file = new java.io.File(tempFileDirPath);
    print(file.exists());
    if (!file.exists()) {
        file.mkdirs(); // 创建目录
    }
    let dir1 = new java.io.File(`${workDir}/2022-01-04/tempSaveZipFile/test1`);
    let dir2 = new java.io.File(`${workDir}/2022-01-04/tempSaveZipFile/test2`);
    if (!dir1.exists()) {
        dir1.mkdirs();
    }
    if (!dir2.exists()) {
        dir2.mkdirs();
    }

    let file1 = new java.io.File(`${workDir}/2022-01-04/tempSaveZipFile/test1/test.txt`);
    let file2 = new java.io.File(`${workDir}/2022-01-04/tempSaveZipFile/test2/test.txt`);
    if (!file1.exists()) {
        file1.createNewFile();
    }
    if (!file2.exists()) {
        file2.createNewFile();
    }

    let f = getFile(`${workDir}/2022-01-04/tempSaveZipFile/`);
    print(f.listFiles());
    print(f.listDirs());

    let f2 = getFile(`${workDir}/2022-01-04/tempSaveZipFile/test1/`);
    print(f2.listFiles());

    let f3 = getFile(`${workDir}/2022-01-04/tempSaveZipFile/test2/`);
    print(f3.listFiles());

    let zipUtils = new ZipUtils();
    let dirs: string[] = [`${workDir}/2022-01-04/tempSaveZipFile/test1/`, `${workDir}/2022-01-04/tempSaveZipFile/test2/`];
    zipUtils.zipDirs(dirs, `${workDir}/2022-01-04/tempSaveZipFile/test.zip`, true);

    let t = getFile(`${workDir}/wljy_fjcc/2022-01-04/tempSaveZipFile/`); // 查看
    print(t.listDirs());
    print(t.listFiles());
}

/**
 * 导出多个文件夹测试
 * word：${workDir}/2022-01-04/tempSaveZipFile/test1/word.doc
 */
function test_importAllZipFolder(): void {
    let dirs: string[] = [`/zhjg/workdir/wljy_fjcc/2022-01-20/saba_3d62a7b889cc4c79b9b9a3026515b809/`, `/zhjg/workdir/wljy_fjcc/2022-01-20/0-220119-wx-80908-1/`];

    let workDir = getWorkDir();
    let currentTime: string = formatDate2(new Date());

    let saveZipFolderPath: string = `${workDir}/2022-01-04/tempSaveZipFile/${currentTime}/`;
    let importZipFileName: string = `${uuid()}.zip`;

    let importZip = new java.io.File(`${saveZipFolderPath}/${importZipFileName}`);
    if (!importZip.exists()) {
        let parentFile = importZip.getParentFile();
        if (!parentFile.exists()) {
            parentFile.mkdirs();    // 若文件父目录不存在，则创建文件以及父文件目录
        }
        importZip.createNewFile(); // 创建真实的文件
    }

    for (let i = 0; i < dirs.length; i++) {
        /**
         * 20220210 tuzw
         * 给每个目录，新增一个word文档
         */
        fileUtils.dirAddNewFile(dirs[i], `${workDir}/2022-01-04/tempSaveZipFile/test1/word.doc`);
    }
    zipUtils.zipDirs(dirs, importZip, true);

    let t = getFile("/zhjg/workdir/2022-01-04/tempSaveZipFile/2022-02-10/");
    print(t.listDirs());
    print(t.listFiles());

    // deleteFile("/zhjg/workdir/2022-01-04/tempSaveZipFile/2022-02-10/");  // 删除文件夹以及子文件
    // print(t.listFiles());
}

/** 
 * 20220209 tuzw
 * 文件压缩工具类
 */
class ZipUtils {

    /**
     * 功能：压缩多个文件（可是ZIP文件）成一个zip文件
     * @param files       压缩的文件绝对路径集，eg：["/zhjg/workdir/wljy_fjcc/", "/zhjg/workdir/wljy_fjcc/t.txt"]
     * @param zipFilePath 压缩后的文件路径（绝对路径），eg：.../tempSaveZipFile/0-220119-wx-80908-1.zip
     * eg：
     *  let files: string[] = ["/zhjg/workdir/0-220119-wx-80908-1/1.png", "/zhjg/workdir/0-220119-wx-80908-1/2.pdf"];
        let zipFilePath: string = `${workDir}/2022-01-04/tempSaveZipFile/0-220119-wx-80908-1.zip`;
        zipFiles(files, zipFilePath);
     */
    public zipFiles(files: string[], zipFilePath: string): void {
        print("zipFiles()，开始压缩导出文件");
        if (files == null || files.length == 0 || !zipFilePath) {
            return;
        }
        let zipFile = new java.io.File(zipFilePath);
        if (!zipFile.exists()) {
            print("压缩文件不存在，已自动创建");
            zipFile.createNewFile();
        }
        let out: ZipOutputStream = null;
        try {
            out = new ZipOutputStream(new FileOutputStream(zipFile));
            for (let i = 0; i < files.length; i++) { // 
                let sendFile = new File_(files[i]);
                if (!sendFile.exists()) {
                    print(`文件【${files[i]}】不存在服务器上`);
                    continue;
                }
                let fileBytes = Files_.readAllBytes(Paths_.get(sendFile.getAbsolutePath())); // 通过绝对路径获取文件字节流
                out.putNextEntry(new ZipEntry(sendFile.getName()));
                out.write(fileBytes);
            }
        } catch (e) {
            print(`zipFiles()，开始压缩导出文件--error`);
            print(e);
        } finally {
            if (out != null) {
                out.close();
            }
        }
    }

    /**
     * 压缩多个文件夹 或 文件
     * @param dirs           要压缩的文件夹 或 文件，eg：["/zhjg/workdir/wljy_fjcc/", "/zhjg/workdir/wljy_fjcc/t.txt"]
     * @param zipFile        压缩后的文件路径（绝对路径），eg：.../tempSaveZipFile/0-220119-wx-80908-1.zip
     * @param includeBaseDir 是否包括最外层目录
     */
    public zipDirs(dirs: string[], zipFilePath: string, includeBaseDir: boolean): void {
        print("zipDirs()，压缩多个文件夹开始");
        if (dirs == null || dirs.length == 0 || zipFilePath == null || zipFilePath == "") {
            return;
        }
        let zipOut: ZipOutputStream = null;
        let zipFile = new java.io.File(zipFilePath);
        if (!zipFile.exists()) {
            let parentFile = zipFile.getParentFile();
            if (!parentFile.exists()) {
                parentFile.mkdirs();    // 若文件父目录不存在，则创建文件以及父文件目录
            }
            zipFile.createNewFile(); // 创建真实的文件
        }
        try {
            /**
             * 导出多个文件夹
             * 思路：压缩后直接将其文件流写入：response
             * zipOut = new ZipOutputStream(new FileOutputStream(response.getOutputStream()));
             * 参考网址：https://juejin.cn/post/6994374601329344548#heading-5
             */
            zipOut = new ZipOutputStream(new FileOutputStream(zipFilePath));
            for (let i = 0; i < dirs.length; i++) {
                print(`当前压缩的文件为：${dirs[i]}`);
                let fileDir = new File_(dirs[i]);
                let baseDir: string = ""; // 最外层目录
                if (includeBaseDir) {
                    baseDir = fileDir.getName();
                }
                this.compress(zipOut, fileDir, baseDir);
            }
        } catch (e) {
            print("压缩多个文件夹错误")
            print(e);
        } finally {
            if (zipOut != null) {
                zipOut.close();
            }
        }
        print("zipDirs()，压缩多个文件夹结束");
    }

    /**
     * 采用递归的方式，将文件夹下的文件放入到指定的压缩包中
     * @param zipOut    压缩对象
     * @param folder    文件夹 或 文件对象
     * @param baseDir   文件夹 或 文件名
     */
    private compress(zipOut: ZipOutputStream, folderOrFile: File_, baseDir: string): void {
        if (folderOrFile.isDirectory()) {
            baseDir = baseDir.length == 0
                ? ""
                : baseDir + File_.separator;
            let childFiles: File_ = folderOrFile.listFiles();
            if (ArrayUtils.isEmpty(childFiles)) {
                return;
            }
            for (let i = 0; i < childFiles.length; i++) {
                this.compress(zipOut, childFiles[i], baseDir + childFiles[i].getName());
            }
        } else {
            zipOut.putNextEntry(new ZipEntry(baseDir));
            let fileIn: FileInputStream = null;
            try {
                fileIn = new FileInputStream(folderOrFile);
                IOUtils.copy(fileIn, zipOut);
            } catch (e) {
                print("文件夹下的文件放入到指定的压缩包中失败");
                print(e);
            } finally {
                if (fileIn != null) {
                    fileIn.close();
                }
            }
        }
    }
}
const zipUtils = new ZipUtils();

/**
 * 20220209 tuzw
 * 文件工具类
 */
class FileUtils {

    /**
     * 给指定目录下，新增一个文件
     * @param dirPath 目录路径，eg：/tempSaveZipFile/
     * @param newAddFilePath 新增的文件路径，eg：/tempSaveZipFile/t.doc
     * @param copyFileName 复制后的文件名.类型，eg：copy_t.doc
     * @return true | false
     */
    public dirAddNewFile(dirPath: string, newAddFilePath: string, copyFileName?: string): boolean {
        if (dirPath == "" || newAddFilePath == "") {
            print(`dirAddNewFile()，给指定目录下，新增一个文件：传递参数不合法，dirPath：${dirPath}，newAddFilePath：${newAddFilePath}`);
            return false;
        }
        let newAddFile = new java.io.File(newAddFilePath);
        if (!newAddFile.exists()) {
            print(`新增文件不存在：${newAddFilePath}`);
            return false;
        }
        let dir = new java.io.File(dirPath);
        if (!dir.exists()) {
            print(`指定目录不存在：${dirPath}`);
            return false;
        }
        if (!copyFileName) {
            copyFileName = newAddFile.getName(); // eg：test.txt
        }
        if (`${newAddFile.getParent()}/` == dirPath) { // 若同处于一个目录下，则增加前缀：copy_
            copyFileName = `copy_${copyFileName}`;
        }
        let copyFile = new java.io.File(`${dirPath}${copyFileName}`);
        if (!copyFile.exists()) {
            copyFile.createNewFile();
        }
        let fileOut: FileOutputStream = null;
        let fileIn: FileInputStream = null;
        try {
            fileOut = new FileOutputStream(copyFile);
            fileIn = new FileInputStream(newAddFilePath);
            IOUtils.copy(fileIn, fileOut);
            print(`给指定目录：【${dirPath}】，新增一个文件：【${newAddFilePath}】-成功`);
            return true;
        } catch (e) {
            print("dirAddNewFile()，给指定目录下，新增一个文件失败");
            print(e);
        } finally {
            if (fileOut != null) {
                fileOut.close();
            }
            if (fileIn != null) {
                fileIn.close();
            }
        }
        return false;
    }

    /**
     * 显示指定目录下的子文件和文件夹情况
     * @param dirPath 目录路径，eg：/tempSaveZipFile/
     */
    public showFolderOrFile(dirPath: string): void {
        if (!dirPath) {
            print(`showFolderOrFile()，传入的路径不合法：【${dirPath}】`);
            return;
        }
        let dir = new java.io.File(dirPath);
        if (!dir.isDirectory()) {
            print(`showFolderOrFile()，传入的路径不是一个目录`);
            return;
        }
        print("当前目录下的子文件和文件夹情况：");
        let childFolders: File_[] = dir.listFiles();
        if (childFolders.length > 0) {
            for (let i = 0; i < childFolders.length; i++) {
                print(childFolders[i].getAbsolutePath());
            }
        }
    }

    /**
     * 删除指定目录下，指定类型的文件
     * @param dirPath 目录路径，eg：/tempSaveZipFile/
     * @param fileType 文件类型，eg: doc
     */
    public deleteAppointFile(dirPath: string, fileType: string): void {
        // todo
    }

}
const fileUtils = new FileUtils();
