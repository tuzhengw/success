/**
 * 作者：tuzw
 * 创建时间：2022-11-01
 * 脚本用途：XML内容格式化展示定制脚本
 */
import { MonacoEditor, MonacoEditorArgs } from "commons/monaco/monacoeditor";
import { BeautifyCode } from "commons/beautify"

/**
 * 定制JS控件-脚本编辑器
 * 地址：https://jira.succez.com/browse/CSTM-21033
 */
export class Custom_ScriptEditorComp extends MonacoEditor {

    /** 美化格式对象 */
    private beautifyCode: BeautifyCode;

    /**
     * @param language 显示文本语言, eg: XML、JSON
     * @param value 编辑器显示内容
     * @param readonly? 是否可读状态, 默认: false
     */
    constructor(args: MonacoEditorArgs) {
        super(args);
    }

    public _init_default(args: MonacoEditorArgs) {
        super._init_default(args);
    }

    public _init(args: MonacoEditorArgs): HTMLElement {
        this.beautifyCode = new BeautifyCode();
        args.value = this.customFormatContent(args.value as string);
        let domBase = super._init(args);
        this.setStyleDom(domBase);
        return domBase;
    }

    /**
     * 处理样式
     */
    private setStyleDom(domBase: HTMLElement): HTMLElement {
        domBase.style.position = 'absolute';
        domBase.style.height = '100%';
        domBase.style.width = '100%';
        return domBase;
    }

    public _reInit(args: any) {
        let content = this.customFormatContent(args.value)
        this.setValue(content);
    }

    /**
     * 格式化编辑器内容
     * @param editorContent 编辑器内容
     * @return 
     * 
     * @description MonacoEditor对象的format无法格式化, 此处借用BeautifyCode进行格式化, eg: {"name":"张三","age":12 }
     * @description 格式化后直接将内容返回, MonacoEditor对象会原样显示格式化后的字符串内容, eg: {
     *     "name": "张三",
     *     "age": 12
     * }
     */
    private customFormatContent(editorContent: string): string {
        let language: string = super.getLanguage().toUpperCase();
        switch (language) {
            case "XML":
                editorContent = this.beautifyCode.xml(editorContent);
                break;
            case "JSON":
                try {
                    let jsonObj = JSON.parse(editorContent);
                    editorContent = this.beautifyCode.json(jsonObj, {
                        jsonLength: 10, // JSON字符串超过多少长度才会执行格式化操作
                        arrayLength: 120
                    });
                } catch (e) {
                    // 若内容不是JSON，则原样显示内容
                }
                break;
        }
        return editorContent;
    }

    /**
     * 重新设置显示内容
     * @param editorContent 编辑器内容
     */
    public setValue(editorContent: string): void {
        super.setValue(editorContent);
    }
} 