import { ctx, SZEvent, Component, throwInfo, throwError, showMessage,
showConfirmDialog, showFormDialog, showMenu, 
isEmpty, deepAssign, uuid, message, quoteExpField, showDialog, 
showSuccessMessage,showErrorMessage,
showProgressDialog, rc, downloadFile, waitRender, 
showWaiting, assign, IDataProvider, formatDateFriendly, parseDate, formatDate, tryWait, 
rc_get, showWarningMessage, timerv, showWaitingMessage, hideMessage, DataChangeEvent,
 EventListenerManager, GroupInfo, IFindArgs, ServiceTaskPromise, ServiceTaskInfo, ServiceTaskState } from 'sys/sys';

import { showMessageBox, MessageBoxArgs, MessageContentInfo, MessageBoxType } from 'commons/basic';

// 警告框
showWarningMessage("")


showMessageBox({
	content: "当前无日志信息",
	type: MessageBoxType.INFO,
	top: 200
});


/**
 * 显示一个消息框，并指定消息框类型
 */
export function showMessageBox(args: MessageBoxArgs): Promise<MessageBox> {
	if (!messagebox) {
		messagebox = new MessageBox(args);
	}
	if (messagebox.hidingPromise) {
		return messagebox.hidingPromise.then(() => { return messagebox.show(args); });
	} else {
		return Promise.resolve(messagebox.show(args));
	}
}

/**
 * 显示在MessageBox内容区域的对象。
 * http://www.typescriptlang.org/docs/handbook/advanced-types.html#Type Aliases
 */
export type MessageContentInfo = string | Component | HTMLElement;

export interface MessageBoxArgs extends ComponentArgs {
	/**
	 * 要显示的内容。
	 */
	content: MessageContentInfo,
	/**
	 * messagebox类型。默认为info。
	 */
	type?: MessageBoxType,
	/**
	 * 是否显示状态图标。
	 */
	iconVisible?: boolean,
	/**
	 * 是否显示关闭图标。
	 */
	closeVisible?: boolean,
	/**
	 * 自动隐藏，值是毫秒数。如果为0或者负数，表示不自动隐藏。默认3000。
	 * 支持传入一个promise，表示promise结束后隐藏。
	 */
	hideTime?: number | Promise<any>,
	/**
	 * 显示的高度，对于表单应用中消息框高度会自己计算
	 */
	top?: number,
}

export const enum MessageBoxType {
	INFO = 'info',
	SUCCESS = 'success',
	ERROR = 'error',
	WAITING = 'waiting',
	WARNING = 'warning'
}

