/**
 * 作者：刘永卓
 * 创建日期：2022-06-09
 * 脚本用途：用于为定制计划任务界面提供后端支持
 */
import db from "svr-api/db";
import metadata from "svr-api/metadata";
interface TaskFlowNodeInfo {
    id: string;
    resid?: string;
    state?: DW_RESOURCE_STATE;
    name?: string;
    dataSource?: string;
    schema?: string;
    type?: string;
    icon?: string;
    metaInfo?: MetaFileInfo;
    path?: string;
    selected?: boolean;
    isDtable?: boolean;
    isOutputDbtable?: boolean;
    isOds?: boolean;
    isFile?: boolean;
    loopPaths?: string[][];
    [key: string]: any;
}
/**
 * 查询指定节点信息
 */
export function queryNodeInfo(request: HttpServletRequest, response: HttpServletResponse, params: { dbInfo: { dbList: string[]; dbTableNode: { datasource: string, schema: string, name: string }[] }, nodeInfo: { notReferResidList: string[], dataFlowResidList: string[] } }) {
    let { dbInfo, nodeInfo } = params;
    let { dbList, dbTableNode } = dbInfo;
    let { notReferResidList, dataFlowResidList } = nodeInfo;
    if (typeof dbList === "string") {
        dbList = dbList.split(",");
    }
    if (typeof notReferResidList === "string") {
        notReferResidList = notReferResidList.split(",");
    }
    /**
     * 2022-09-05 liuyz
     * 如果没有任何节点此处不要进行查询
     */
    if (dbList.length == 0 && notReferResidList.length == 0 && dataFlowResidList.length == 0) {
        return { outPutDBNodeList: [], systemNodeList: [] }
    }
    let service = new ScheduleNodeService({ dbList, notReferResidList, dataFlowResidList, dbTableNode });
    let outPutDBNodeList = service.queryResidOutPutDbInfo();
    let systemNodeList = service.querySystemInfoByDb();
    let dataFlowNodeList = service.queryDataFlowInfo();
    let dbTableNodeList = service.queryDbTableNodeInfo();
    return {
        outPutDBNodeList, systemNodeList, dataFlowNodeList, dbTableNodeList
    }
}

/**
 * 20220616 liuyz
 * 流向图节点服务类
 */
class ScheduleNodeService {
    /**数据源数组 */
    private dbList: string[];
    /**未被引用加工id数组 */
    private notReferResidList: string[];
    /**数据加工模型id数组 */
    private dataFlowResidList: string[];

    private dbTableNodeList: { datasource: string, schema: string, name: string }[];

    private ds: Datasource;

    private dbStateInfos: {
        [fieldName: string]: any;
    }[]

    constructor(args: { dbList: string[], notReferResidList: string[], dataFlowResidList: string[], dbTableNode: { datasource: string, schema: string, name: string }[] }) {
        this.dbList = args.dbList;
        this.notReferResidList = args.notReferResidList;
        this.dataFlowResidList = args.dataFlowResidList;
        let ds = this.ds = db.getDefaultDataSource();
        let dbStateTable = ds.openTableData('SDI_MONITOR_DATASOURCESTATE');
        let dbStateInfos = this.dbStateInfos = dbStateTable.select(["DATASOURCE_NAME", "URL", "DB_USER", "STATE"]);
        this.dbTableNodeList = args.dbTableNode;
    }

    /**
     * 查询数据加工输出节点的数据源信息
     */
    public queryResidOutPutDbInfo(): TaskFlowNodeInfo[] {
        let residList = this.notReferResidList;
        let metaTable = this.ds.openTableData("szsys_4_meta_tables");
        let dbNodeList: TaskFlowNodeInfo[] = [];
        let dbNameList: string[] = [];
        for (let i = 0; i < residList.length; i++) {
            let resid = residList[i];
            let dataSource = <string><unknown>metaTable.selectFirst("DATASOURCE", { "MODEL_RESID": resid });
            let dbFile = metadata.getFile("/sysdata/data/sources/" + dataSource + ".jdbc");
            if (dbFile == null) {
                continue;
            }
            if (dbNameList.includes(dataSource)) {
                dbNodeList.forEach(node => {
                    if (node.id == dataSource) {
                        node.originResid.push(resid)
                    }
                });
                continue;
            }
            let dbStateInfo = this.getDbInfo(dataSource);
            let desc = dbFile.desc || dataSource;
            let node: TaskFlowNodeInfo = {
                id: dataSource,
                name: desc,
                type: "dataBase",
                isDataBase: true,
                isDtbale: false,
                isOds: false,
                isFile: false,
                originResid: [resid],
                inputLinks: [],
                icon: "icon-datasource",
                resInfo: {
                    name: desc,
                    desc: dataSource,
                    file: false,
                    ods: false,
                    dbTable: false,
                    uid: dataSource,
                    dataBaseInfo: dbStateInfo.URL,
                    dataBaseState: dbStateInfo.STATE == "WORKING" ? "连接" : "断开",
                    dataBaseUser: dbStateInfo.DB_USER
                },
                prevNodes: []
            }
            dbNameList.push(dataSource);
            dbNodeList.push(node);
        }
        return dbNodeList;
    }

    /**
     * 根据数据源信息查询物理表
     */
    public querySystemInfoByDb(): TaskFlowNodeInfo[] {
        let dbList = this.dbList;
        let systemNodeList: TaskFlowNodeInfo[] = [];
        let defaultDb = db.getDefaultDataSource();
        let querySQL: string = `
				SELECT t2.ID,
				t2.NAME
			FROM SDI_2_DATABASE t1
			LEFT JOIN SDI_2_MIS_SYSTEM t2
				ON t1.MIS_SYSTEM_ID = t2.ID
			WHERE t1.name = ?
		`;
        for (let i = 0; i < dbList.length; i++) {
            let dbName: string = dbList[i];
            let systemInfos = defaultDb.executeQuery(querySQL, [dbName]);
            if (systemInfos.length == 0) {
                print(`数据源：${dbName} 不存在对应的系统信息`);
                continue;
            }
            let systemInfo = systemInfos[0];
            if (systemInfo == null) {
                continue;
            }
            let dbStateInfo = this.getDbInfo(dbName);
            let node: TaskFlowNodeInfo = {
                id: systemInfo.ID,
                name: systemInfo.NAME,
                type: 'system',
                dataSource: dbName,
                icon: "icon-sysinfo",
                isDtbale: false,
                isOds: false,
                isFile: false,
                isSystem: true,
                inputLinks: [],
                resInfo: {
                    name: systemInfo.NAME,
                    desc: dbName,
                    file: false,
                    ods: false,
                    dbTable: false,
                    uid: systemInfo.ID,
                    dataBaseInfo: dbStateInfo.URL,
                    dataBaseState: dbStateInfo.STATE == "WORKING" ? "连接" : "断开",
                    dataBaseUser: dbStateInfo.DB_USER
                },
                prevNodes: []
            }
            systemNodeList.push(node);
        }
        return systemNodeList
    }

    /**
     * 获取数据加工节点信息，例如加工上次执行结果，以及当前表数量
     *
     * 20220729 tuzw
     * 问题：考虑到计划任务包含的加工过多，可能存在上千个，列表最大的表达式数量为1000，这里需要循环查询
     * 帖子：https://jira.succez.com/browse/CSTM-19741
     */
    public queryDataFlowInfo() {
        let ds = this.ds;
        let table = ds.openTableData("SZSYS_4_META_TABLES")
        let filters: string[] = [];
        let params: string[] = [];
        let dataFlowResidList = this.dataFlowResidList;
        print(`queryDataFlowInfo（），计划任务加工个数为：${dataFlowResidList.length} 个`);
        let map: JSONObject = {};
        dataFlowResidList.forEach(d => {
            filters.push("?");
            params.push(d);
            if (filters.length / 1000 == 1) { // 若当前个数为1000，则执行查询
                this.queryScheduleDataFlowRunResult(table, map, filters, params);
                filters = [];
                params = [];
            }
        });
        if (filters.length != 0 && params.length != 0) {
            this.queryScheduleDataFlowRunResult(table, map, filters, params);
        }
        return map;
    }

    /**
     * 查询计划任务加工执行结果
     *
     * 2022-09-05 liuyz
     * 支持查询所有模型节点的描述
     *
     * @param table 当前查询表对象
     * @param map 保存查询的加工执行的结果
     * @param fliters 查询的参数个数，eg："?,?,?"
     * @param filterValues 查询的参数对应的值，eg："001,002,003"
     */
    private queryScheduleDataFlowRunResult(table: TableData, map: JSONObject, fliters: string[], filterValues: string[]): void {
        try {
            let sql = `select t0.MODEL_RESID,t0.ROW_COUNT,t1.LAST_STATE from SZSYS_4_META_TABLES t0 LEFT JOIN SZSYS_4_SCHEDULETASK t1 ON (t0.MODEL_RESID=t1.RESID) WHERE MODEL_RESID in (${fliters.join(",")})`;
            let rows = table.executeQuery(sql, filterValues);
            for (let i = 0; i < rows.length; i++) {
                let row = rows[i];
                let state = row["LAST_STATE"];
                let lastState = "没有执行过";
                switch (state) {
                    case 1: lastState = "执行成功"; break;
                    case 2: lastState = "执行失败"; break;
                    case 3: lastState = "执行中止"; break;
                    case 9: lastState = "正在执行"; break;
                }
                let file = metadata.getFile(row["MODEL_RESID"]);
                let desc = file && file.desc;
                map[row["MODEL_RESID"]] = {
                    lastState, rowCount: row["ROW_COUNT"], desc
                }
            }
        } catch (e) {
            print(`queryScheduleDataFlowRunResult()，查询计划任务加工执行结果 --- error`);
            print(e);
        }
    }

    private getDbInfo(dbName: string) {
        let dbStateInfos = this.dbStateInfos;
        for (let i = 0; dbStateInfos && i < dbStateInfos.length; i++) {
            let info = dbStateInfos[i];
            if (info.DATASOURCE_NAME == dbName) {
                return info;
            }
        }
        return {};
    }

    /**
     * 查询物理表对应的描述信息
     */
    public queryDbTableNodeInfo() {
        let dbTableNodeList = this.dbTableNodeList;
        let list = [];
        for (let i = 0; i < dbTableNodeList.length; i++) {
            let info = dbTableNodeList[i];
            let ds = db.getDataSource(info.datasource);
            let tableMeta = ds.getTableMetaData(info.name, info.schema);
            list.push({ id: info.datasource + info.schema + info.name, desc: tableMeta.getComment() });
        }
        return list;
    }

}