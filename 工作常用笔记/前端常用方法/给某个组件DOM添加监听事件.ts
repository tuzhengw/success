

/**
 * 给某个组件DOM添加监听事件
 */
export class SpgNoteLabelClass {

    /** 页面对象 */
    private superPage: IVPage;
    /** 交互管理器 */
    private actionMgr: ActionManager;
    /** 富文本对象 */
    private tinyMCEText: TinyMCEText;

    public CustomActions: any;

    constructor() {
        this.CustomActions = {};
    }

    public onRender(event: InterActionEvent): void | Promise<void> {
        let id = event.component && event.component.getId();
        this.actionMgr = event?.renderer.getActionManager();

        if (id == "richtextinput1") {
            this.addRichtextinputOnkeydown(event, id);
        }
    }

    /**
     * 给富文本组件输入添加键盘监听事件
     * <p>
     *  1 异步执行结束后, then内部要使用外部的变量，内部才会加载
     * </p>
     * @param 
     * @param richtextinputId 富文本ID
     */
    private addRichtextinputOnkeydown(event: InterActionEvent, richtextinputId: string) {
        let page: IVPage = event.page;
        this.superPage = page;

        let richtextInput = page.getComponent(richtextinputId);
        if (isEmpty(richtextInput)) {
            return;
        }
        let tinyMCEText: TinyMCEText = richtextInput.component.getInnerComoponent();
        this.tinyMCEText = tinyMCEText;

        let tinyMCETextOldDoInit = tinyMCEText.doInit.bind(tinyMCEText);
        tinyMCEText.doInit = () => { // 重写富文本初始渲染方法
            tinyMCETextOldDoInit().then(() => { // 等待富文本加载完成后增加键盘监听事件
                let editor = tinyMCEText.editor;
                editor.on("keyup", this.tinyMCETextKeyup.bind(this, {
                    actionCompId: "button9",
                    saveInputValueGlobalParamName: "choseNoteCotent"
                }));
            });
        }
    }

    /**
     * 富文本输入对象增加键盘监听事件——自动保存输入内容
     * 
     * @param actionCompId 交互执行的组件ID (页面可用一个隐藏按钮, 用于自动保存提交)
     * @param saveInputValueGlobalParamName? 保存富文本组件值的全局参数名
     * 
     * <p>
     *  1 连续输入内容时, 当前秒为5的倍数, 则自动保存内容
     * </p>
     */
    private tinyMCETextKeyup(args: {
        actionCompId: string,
        saveInputValueGlobalParamName?: string
    }): void {
        let nowTime: number = Date.now();
        let second: number = Math.ceil(nowTime / 1000);

        if (second % 5 == 0) {
            let tinyMCEText = this.tinyMCEText
            let editor = tinyMCEText.editor;
            let value = editor.getContent();
            if(isEmpty(value)) {
                return;
            }
            if (tinyMCEText.disposed != true && tinyMCEText.value != value) {
                tinyMCEText.value = value;
                tinyMCEText.onchange && tinyMCEText.onchange({ component: tinyMCEText }, this.tinyMCEText);
            }

            let actionCompId = args.actionCompId;
            let saveInputValueGlobalParamName = args.saveInputValueGlobalParamName;
            if (!isEmpty(saveInputValueGlobalParamName)) {
                this.superPage.setParameter("choseNoteCotent", value); // 设置提交内容到全局参数值
            }
            if (!isEmpty(actionCompId)) {
                this.actionMgr.triggerActions({
                    compId: actionCompId,
                    triggerType: ActionTriggerType.Click
                });
            }
        }
    }
}