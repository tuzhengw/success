---
typora-root-url: images
---

# import其他模块

> 注意：最终引入的是：<button>JS文件</button>

![](../images/导入其他模块文件.png)

---

APP.TS

```ts
function main() {
}
export class Apple{
    Apple(){
    }
    init(): number{
        return 1;
    }
}
```

APP.jS

```js
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function main() {
}
class Apple {
    Apple() {
    }
    init() {
        return 1;
    }
}
exports.Apple = Apple;

```

Test.TS

```TS
import { Apple } from '../js/app.js'

function main(){
    var app = new Apple();
    print(app.init());
    print("The %s jumped over %d tall buildings", 1, 2);
}

function print(...args) {
    console.log(...args);
}

main();
```

