
import { showConfirmDialog } from "commons/dialog";
import "css!./data-govern-mgr.css";
import { DatasetDataPackageRowInfo, IDataset, InterActionEvent, IVComponent } from "metadata/metadata-script-api";
import { ExpDialog, IFieldsDataProvider, showExpDialog } from 'commons/exp/expdialog';
import { Component, isEmpty, SZEvent, throwInfo, rc, message, uuid, showProgressDialog, showWaiting, rc_task, assign, showWarningMessage, LogItemInfo, ServiceTaskPromise, showDialog, showError, showErrorMessage } from "sys/sys";
import { CustomJs_List, CustomJs_List_Head, getAppPath, showScheduleDialog } from "../commons/commons.js";
import { ExpVar, IExpDataProvider } from "commons/exp/expcompiler";
import { checkCstmRule, CheckCstmRuleArgs, runCheckTask } from "dg/dgapi";
import { exportQuery, getDwTableDataManager, getQueryManager } from "dw/dwapi";
import { showDrillPage, getMetaRepository } from 'metadata/metadata';
import { ExpContentType } from 'commons/exp/exped';
import { getScheduleMgr, ScheduleMgr, ScheduleLogInfo, TaskLogInfo } from "schedule/schedulemgr";
import { ProgressLogs } from "commons/progress"
import { Dialog } from "commons/dialog";
import { SuperPageBuilder } from "app/superpage/superpagebuilder";
import { SuperPage } from 'app/superpage/superpage';

/** 有前缀表名的自定义规则字段的dp */
let dgRuleFieldDp: CustomRuleFieldDp;

const DIMENSION_VAR_ICON: { [key: string]: string } = {
	"C": "icon-string-dim",
	"N": "icon-float-dim",
	"I": "icon-int-dim",
	"D": "icon-date-dim",
	"T": "icon-date-dim",
	"P": "icon-date-dim",
	"M": "icon-clob-dim",
	"X": "icon-blob-dim"
};

const MEASURE_VAR_ICON: { [key: string]: string } = {
	"C": "icon-string",
	"N": "icon-float",
	"I": "icon-int",
	"D": "icon-date",
	"T": "icon-date",
	"P": "icon-date",
	"M": "icon-clob",
	"X": "icon-blob"
};

interface CustomFieldItem {
	id: string;
	caption: string;//字段标题
	pickValue: string[];//选择值
	value: string;
	dataType: string;//字段类型
	dbfield: string;//物理字段名称
	isDimension: boolean;//是否为维度
	isField: boolean;//是否为字段
	leaf: boolean;//是否为叶子节点
	icon: string;//图标
	name: string;
	exp?: string;
	desc?: string;
}

/**
 * 定制的表达式dp
 * liuyz 20210721
 */
export class CustomExpDp implements IExpDataProvider {
	/** 别名和中文描述之间的映射关系 */
	private tableAndFieldsExpVar: ExpVar[];
	private fields: string[];
	/** 数据表和模型对应关系 */
	private tableFieleMap: JSONObject;

	public getVarsByParent(parentVar?: IExpVar): Array<IExpVar> {
		if (parentVar != null && !isEmpty(this.tableFieleMap)) {
			return this.tableFieleMap[parentVar.getName()];
		}
		return this.tableAndFieldsExpVar;
	}

	/**
	 * 清空成员属性
	 */
	public clear(): void {
		this.tableAndFieldsExpVar = [];
		this.fields = [];
		this.tableFieleMap = {};
	}

	public setTable(tableInfo) {
		let expVar = new ExpVar(tableInfo);
		this.tableAndFieldsExpVar == null && (this.tableAndFieldsExpVar = []);
		this.tableAndFieldsExpVar.push(expVar);
	}

	/**
	 * 设置字段表达式
	 * 字段的表达式需要记录下对应的模型id,方便后面进行获取
	 * @param fieldInfo
	 * @param sourceId
	 */
	public setField(fieldInfo, sourceId?: string) {
		let expVar = new ExpVar(fieldInfo);
		this.tableAndFieldsExpVar == null && (this.tableAndFieldsExpVar = []);
		this.fields == null && (this.fields = []);
		this.tableAndFieldsExpVar.push(expVar);
		this.fields.push(fieldInfo.caption);
		if (!isEmpty(sourceId)) {
			isEmpty(this.tableFieleMap) && (this.tableFieleMap = {});
			isEmpty(this.tableFieleMap[sourceId]) && (this.tableFieleMap[sourceId] = []);
			this.tableFieleMap[sourceId].push(expVar);
		}
	}

	public validateFieldName(name: string) {
		if (!name) {
			return message("dataflow.fieldname.null");
		}
		if (name && name.match(/[`"\[\]]+/)) {
			return message("dataflow.fieldname.existInvalidChar");
		}
		let upperName = name.toUpperCase();
		// 可以修改名称的大小写
		if (this.fields.indexOf(upperName) != -1) {
			return message("dataflow.fieldname.repeat");
		}
		return null;
	}
}

/**
 * 20211015 kongwq
 * 创建自定义规则字段的dp
 */
export class CustomRuleFieldDp implements IFieldsDataProvider {
	private tableInfos: { id: string, desc?: string, isCurrentDataSource?: boolean }[];
	private ruleTableInfo: Record<string, string[]>; // 表别名和id对应对象
	private ruleTabelField: Record<string, CustomFieldItem[]>
	private expDp: CustomExpDp;
	/** 设置指定的数据源 */
	private assginSourceId: string;
	/** 当前查看的数据源 */
	private currentSourceId: string;
	/** 设置表达式是否需要携带表名 */
	private isNeedTableName: boolean;

	constructor() {
		this.tableInfos = [];
		this.ruleTableInfo = {};
		this.ruleTabelField = {};
		this.expDp = new CustomExpDp();
		this.isNeedTableName = true;
	}

	/**
	 * 获取规则表ID和别名，用于规则框选择资源
	 * @param args.signTableAlias 获取指定表的别名的表中文描述
	 * @param args.checkTableType 获取检测表类型，默认获取检测规则表字段信息
	 * @param args.standardLibId 标准库ID
	 * 
	 * 20220708 tuzw
	 * 考虑到检测表名称存在相同时，字段引入会被混淆，无法准确定位字段属于那张表的
	 * 解决办法：指定需要获取的别名信息
	 * 帖子地址：https://jira.succez.com/browse/CSTM-19484
	 * 
	 * @returns
	 */
	public fetchDataSources(args?: {
		signTableAlias?: string,
		checkTableType?: CheckTableType,
		standardLibId?: string
	}): Promise<{ id: string, desc?: string, isCurrentDataSource?: boolean }[]> {
		let signTableAlias: string = args?.signTableAlias as string;
		let checkTableType: CheckTableType = args?.checkTableType as CheckTableType;
		let standardLibId: string = args?.standardLibId as string;
		if (!isEmpty(this.tableInfos)) {
			if (!isEmpty(this.assginSourceId)) {
				return Promise.resolve(this.tableInfos.filter(t => { return t.id == this.assginSourceId }));
			} else {
				/**
				 * 遍历整个资源记录表，根据currentSourceId（id）找到当前需要处理的资源信息，使之成为：激活状态，eg：
				 * tableInfos {
				 * 		0: {id: 'EDU_F_XSSJ_1', isCurrentDataSource: false, desc: '全国近十年学生数据'}
						1: {id: 'F_JJHK_CON_1', isCurrentDataSource: false, desc: '经济户口表——关联模型'}
				   }
				 */
				return Promise.resolve(this.tableInfos.map(table => {
					if (table.id == this.currentSourceId) {
						table.isCurrentDataSource = true;
					}
					return table;
				}));
			}
		}
		return rc({ // 若当前tableInfos为null，则自动发请求去请求规则表信息数据
			url: "/zt/dataGovern/getRuleTableInfos",
			data: {
				checkTableType: checkTableType,
				standardLibId: standardLibId
			}
		}).then(results => {
			let tableInfos = this.tableInfos = [];
			results.forEach((result: Array<string>) => {
				if (result[2] === null) { // 若中文描述为空，则描述默认别名
					result[2] = result[1];
				}
				this.ruleTableInfo[result[1]] = [];
				this.ruleTableInfo[result[1]].push(result[0]);
				this.ruleTableInfo[result[1]].push(result[2])
				tableInfos.push({
					id: result[1],
					isCurrentDataSource: false,
					desc: result[2]
				});
				if (isEmpty(signTableAlias) || (!isEmpty(signTableAlias) && result[1] == signTableAlias)) {
					this.expDp.setTable({ name: result[1], caption: result[2] });
				}
			});
			if (!isEmpty(this.assginSourceId)) {
				return tableInfos.filter(t => { return t.id == this.assginSourceId });
			} else {
				return tableInfos.map(t => {
					if (t.id == this.currentSourceId) {
						t.isCurrentDataSource = true;
					}
					return t;
				});
			}
		});
	}

	/**
	 * 请求当前指定资源的字段集
	 * @sourceId 资源别名
	 */
	public fetchFields(sourceId: string, fetchArgs: { [propName: string]: any; isDimension?: boolean; }): JSONObject {
		if (this.ruleTabelField[sourceId]) {
			return this.ruleTabelField[sourceId].filter(f => { return f.isDimension == fetchArgs.isDimension });
		} else {
			return rc({
				url: "/zt/dataGovern/getFieldInfos",
				data: {
					/** 模型表的resid */
					tableId: this.ruleTableInfo[sourceId][0],
					/** 是否为维度字段 */
					isDimension: fetchArgs.isDimension
				}
			}).then(results => {
				const fieldItems: CustomFieldItem[] = [];
				if (!this.ruleTabelField[sourceId]) {
					this.ruleTabelField[sourceId] = [];
				}
				results.forEach((result: DwTableFieldInfo) => {
					let customFieldItem: CustomFieldItem = this.setFieldInfo(result, sourceId, fetchArgs.isDimension);
					fieldItems.push(customFieldItem);
				});
				return fieldItems;
			})
		}
	}

	public searchFields(sourceId: string, searchArgs: { [propName: string]: any; keyword?: string; isDimension?: boolean; }): Promise<JSONObject[]> {
		const key = searchArgs.keyword.toUpperCase();
		const fieldItems: CustomFieldItem[] = [];
		this.ruleTabelField[sourceId].forEach((item: CustomFieldItem) => {
			if ((item.name.indexOf(key) != -1 || item.caption.indexOf(key) != -1) && item.isDimension == searchArgs.isDimension) {
				fieldItems.push(item);
			}
		})
		return Promise.resolve(fieldItems);
	}

	public getExpDp() {
		return this.expDp;
	}

	/**
	 * 设置表达式是否需要携带表名
	 * @params isNeedTableName：true 需要表前缀名，false，不需要
	 */
	public setIsNeedTableName(isNeedTableName: boolean): void {
		this.tableInfos = [];
		this.ruleTableInfo = {};
		this.ruleTabelField = {};
		this.expDp = new CustomExpDp();
		this.isNeedTableName = isNeedTableName;
	}

	/**
	 * 获取表达式是否需要携带表名
	 * @return boolean
	 */
	public getIsNeedTableName(): boolean {
		return this.isNeedTableName;
	}

	/**
	 * 设置指定要查看的数据源
	 * @param assginSourceId  指定数据源在数据规则表中的别名
	 * @param tableInfo 指定表的resid和desc（描述
	 */
	public setAssginSource(assginSourceId: string, tableInfo?: { resid: string, desc: string }) {
		this.assginSourceId = assginSourceId;
		if (!isEmpty(tableInfo) && !this.ruleTableInfo[assginSourceId]) {
			let { resid, desc } = tableInfo;
			this.tableInfos.push({ id: assginSourceId, desc: desc });
			this.ruleTableInfo[assginSourceId] = [resid, desc];
			this.expDp.setTable({ name: assginSourceId, caption: desc })
		}
	}

	/**
	 * 设置当前要查看的数据源
	 * @param currentSourceId
	 */
	public setcurrentSource(currentSourceId: string) {
		this.currentSourceId = currentSourceId;
	}

	/**
	 * 初始化字段信息和
	 * @param args.initTableAlias 指定初始化数据表别名
	 * @param args.initTableDesc 指定初始化数据表中文描述
	 * @param args.checkTableResid 检测表的resid（用于新引入检测资源时给定）
	 * @param args.checkTableType 获取检测表类型，默认获取检测规则表字段信息
	 * @param args.standardLibId 标准库ID
	 * 
	 * 20220706 tuzw
	 * 由于新增的检测资源还未写入规则库中，无法获取到新增检测资源表的字段信息，增加通过指定表ID获取
	 * 地址：https://jira.succez.com/browse/CSTM-19484
	 * 
	 * 20220817 tuzw
	 * 问题：在引入资源表提交之前设置表达式，由于尚未提交，数据表别名没有入库，无法通过别名获取对应的表中文描述
	 * 地址：https://jira.succez.com/browse/CSTM-20068
	 * 解决办法：新增资源表设置表达式，将其资源表的中文描述传入
	 * 
	 * 20220823 tuzw
	 * 改进：后端获取检测表所有字段信息，原仅获取数据检测的所有检测表字段信息，现在需要根据前端传入的checkTableType来获取对应的检测表字段信息
	 * 地址：https://jira.succez.com/browse/CSTM-20164
	 */
	public initTableAndFieldsInfo(args?: {
		initTableAlias?: string,
		initTableDesc?: string,
		checkTableResid?: string,
		checkTableType?: CheckTableType,
		standardLibId?: string
	}): Promise<void> {
		let initTableAlias: string = args?.initTableAlias as string;
		let checkTableResid: string = args?.checkTableResid as string;
		let checkTableType: CheckTableType = args?.checkTableType as CheckTableType;
		let standardLibId: string = args?.standardLibId as string;
		if (checkTableType == CheckTableType.StandardCheck && isEmpty(standardLibId)) {
			showWarningMessage(`初始数据标准检测表字段，未传递标准库ID`);
			return Promise.resolve();
		}
		return this.fetchDataSources({
			signTableAlias: initTableAlias,
			checkTableType: checkTableType,
			standardLibId: standardLibId
		}).then(sources => {
			if (isEmpty(checkTableResid) && isEmpty(initTableAlias) && !isEmpty(this.ruleTabelField)) { // 若未指定获取某表的字段信息和已经获取过，则不重新获取
				return Promise.resolve();
			}
			if (!isEmpty(checkTableResid) && !isEmpty(initTableAlias) && Object.keys(this.ruleTabelField).length == 1 && !isEmpty(this.ruleTabelField[initTableAlias])) { // 校验当前检测表的字段信息是否已经获取（必须保证整个字段信息对象只有一个）
				return Promise.resolve();
			}
			/**
			 * 若指定获取的检测表字段信息，则清空历史获取的字段信息
			 * 注意：expDp存储别名和中文描述之间的映射关系，清空后，需要还原别名和中文描述映射关系
			 */
			if (!isEmpty(checkTableResid)) {
				this.ruleTabelField = {};
				this.expDp.clear();
				if (!!this.ruleTableInfo[initTableAlias]) {
					this.expDp.setTable({ name: initTableAlias, caption: this.ruleTableInfo[initTableAlias][1] });
				} else {
					let initTableDesc: string = args?.initTableDesc as string;
					this.expDp.setTable({ name: initTableAlias, caption: initTableDesc });
				}
			}
			return this.getCheckTableFieldInfos(checkTableResid, initTableAlias, checkTableType);
		});
	}

	/**
	 * 获取检测表字段信息
	 * @param checkTableResid 检测表的resid（用于新引入检测资源时给定）
	 * @param initTableAlias 指获取检测表字段-表别名
	 * @param checkTableType 获取检测表类型，默认获取检测规则表字段信息
	 * @return 
	 */
	public getCheckTableFieldInfos(checkTableResid: string, initTableAlias: string, checkTableType: CheckTableType): Promise<any> {
		return rc({
			url: "/zt/dataGovern/getCheckTableFieldInfos",
			data: {
				signTableResid: checkTableResid,
				tableAlias: initTableAlias,
				checkTableType: checkTableType
			}
		}).then(results => {
			let sources = Object.keys(results);
			if (!!initTableAlias) {// 初始化指定的数据库别名
				if (!this.ruleTabelField[initTableAlias]) {
					this.ruleTabelField[initTableAlias] = [];
				}
				let data = results[initTableAlias];
				data?.dimensions?.forEach((result: DwTableFieldInfo) => {
					this.setFieldInfo(result, initTableAlias, true)
				});
				data?.measures?.forEach((result: DwTableFieldInfo) => {
					this.setFieldInfo(result, initTableAlias, false)
				});
			} else {
				for (let i = 0; i < sources.length; i++) { // 将其规则库中的所有数据库信息都初始化
					const sourceId = sources[i];
					if (!this.ruleTabelField[sourceId]) {
						this.ruleTabelField[sourceId] = [];
					}
					let data = results[sourceId];
					data?.dimensions?.forEach((result: DwTableFieldInfo) => {
						this.setFieldInfo(result, sourceId, true)
					});
					data?.measures?.forEach((result: DwTableFieldInfo) => {
						this.setFieldInfo(result, sourceId, false)
					});
				}
			}
		})
	}

	private setFieldInfo(fieldInfo: DwTableFieldInfo, sourceId: string, isDimensions: boolean): CustomFieldItem {
		let name = fieldInfo.name.toUpperCase();
		let dbFieldName = fieldInfo.dbFieldName;
		let dataType: FieldDataType = fieldInfo.dataType as FieldDataType;
		let icon = isDimensions ? DIMENSION_VAR_ICON[dataType] : MEASURE_VAR_ICON[dataType];
		let customFieldItem: CustomFieldItem = {
			id: dbFieldName,
			caption: name,
			name: dbFieldName,
			desc: name,
			pickValue: this.getIsNeedTableName() ? [sourceId, dbFieldName] : [dbFieldName],
			value: name,
			dataType: dataType,
			dbfield: dbFieldName,
			isDimension: fieldInfo.innerWrappedObject.dimension,
			isField: true,
			leaf: true,
			icon,
			exp: this.getIsNeedTableName() ? `${sourceId}.${dbFieldName}` : `${dbFieldName}`
		};
		this.ruleTabelField[sourceId].push(customFieldItem);
		this.expDp.setField(customFieldItem, sourceId);

		return customFieldItem;
	}
}

/** 获取检测表类型 */
enum CheckTableType {
	/** 数据检测 */
	DataGovernCheck = 'governCheck',
	/** 数据标准 */
	StandardCheck = 'standardCheck'
}