import dw from 'svr-api/dw';
import db from 'svr-api/db';
/**
 * 获取物理表主键
 * @param dataBase 数据源
 * @param tableName  数据源下的表
 */
function getTablePrimaryKeys(dataBase: string, tableName: string): string[] {
    if(dataBase == null || tableName == null){
        return [];
    }
	let ds = db.getDataSource(dataBase);
	let dsMeta = ds.getTableMetaData(tableName);
	let keys = dsMeta.getPrimaryKey();
	let result = [];
	for (let i = 0; keys && i < keys.length; i++) {
		result.push(keys[i]);
	}
	return result.length == 0 ? null : result;
}