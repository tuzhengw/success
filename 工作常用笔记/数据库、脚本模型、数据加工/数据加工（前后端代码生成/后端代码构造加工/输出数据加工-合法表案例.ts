
import dw from "svr-api/dw";
import db from "svr-api/db";
import metadata from "svr-api/metadata";
import utils from 'svr-api/utils';
import security from 'svr-api/security';


/**
 * 检测任务信息结构
 */
interface CheckTaskInfo {
	/** 任务编码 */
	taskId: string;
	/** 任务名称 */
	taskName: string;
	/** 计划任务 */
	schedule?: string;
	/** 输出结果 */
	isOutPut?: string;
	dataSource?: string;
	outputDirPath?: string;
	/** 拦截规则等级字段 */
	interceptRuleLevel?: number;
}

/**
 * 被检测资源信息
 */
interface CheckTableInfo {
	resid: string;//被检测模型信息
	dataFilter?: string;
	isIncrement?: string;
	lastCheckTime?: number;
	lastCheckTimeExp?: string;
	errDetailTablePath: string;
	outputSuccessTable?: string;
	outputInterceptTable?: string;
	isOutPut?: string;
	dataSource?: string;//被检测模型所在数据源
}


/** 错误明细表字段 */
const DG_ERR_DETAIL_FIELDS: DwTableFieldItemInfo[] = [{
	"name": "检测任务编码",
	"dataType": FieldDataType.C,
	"length": 64,
	"isDimension": true,
	"dbfield": "CHECK_TASK_ID",
	"inputField": "CHECK_TASK_ID",
	"hidden": true,
	"modifiedInfo": {
		"hidden": true,
		"name": "检测任务编码"
	}
}, {
	"name": "规则代码",
	"dataType": FieldDataType.C,
	"length": 32,
	"isDimension": true,
	"dbfield": "RULE_ID",
	"inputField": "RULE_ID",
	"modifiedInfo": {
		"hidden": false,
		"name": "规则代码"
	}
}, {
	"name": "问题表ID",
	"dataType": FieldDataType.C,
	"length": 32,
	"isDimension": true,
	"dbfield": "DW_TABLE_ID",
	"inputField": "DW_TABLE_ID",
	"hidden": true,
	"modifiedInfo": {
		"hidden": true,
		"name": "问题表ID"
	}
}, {
	"name": "数据标识",
	"dataType": FieldDataType.C,
	"length": 256,
	"isDimension": true,
	"dbfield": "DATA_ID",
	"inputField": "DATA_ID",
	"modifiedInfo": {
		"hidden": false,
		"name": "数据标识"
	}
}, {
	"name": "最后检测时间",
	"dbfield": "LAST_CHECK_TIME",
	"dataType": FieldDataType.P,
	"isDimension": true,
	"periodType": "timestampdate",
	"dateKeyValueFormat": "yyyyMMdd",
	"inputField": "LAST_CHECK_TIME",
	"hidden": true,
	"modifiedInfo": {
		"hidden": true,
		"name": "最后检测时间"
	}
}, {
	"name": "是否修正",
	"dbfield": "DATA_STATE",
	"dataType": FieldDataType.I,
	"length": 19,
	"isDimension": true,
	"inputField": "DATA_STATE",
	"hidden": true,
	"modifiedInfo": {
		"hidden": true,
		"name": "是否修正"
	}
}]

/** 规则表数据加工显示字段 */
const DG_RUEL_FIELDS: DwTableFieldItemInfo[] = [{
	"name": "规则代码",
	"dataType": FieldDataType.C,
	"length": 64,
	"isDimension": true,
	"dbfield": "RULE_ID",
	"inputField": "RULE_ID",
	"modifiedInfo": {
		"hidden": false,
		"name": "规则代码"
	}
}, {
	"name": "错误级别",
	"dataType": FieldDataType.C,
	"length": 50,
	"isDimension": true,
	"dbfield": "ERROR_LEVEL",
	"inputField": "ERROR_LEVEL",
	"dimensionPath": "/sysdata/data/tables/sdi/data-govern/dims/DIM_ERROR_LEVEL.tbl",
	"modifiedInfo": {
		"hidden": false,
		"name": "错误级别"
	}
}]


/**
 * 将检测任务的检测资源自动生成加工，生成合法表
 
   调用案例：
	import dg from "/sysdata/app/zt.app/data-govern/dataGovern.action";
	import etl from "svr-api/etl";
	function main() {
		let taskId ="2f6dc44663014a5e996c9a74bb8ed877";
		let checkTask = new dg.checkTaskToDataFlow({ taskId: "2f6dc44663014a5e996c9a74bb8ed877" });
		
		checkTask.createDataFlow(); // 创建数据加工，生成合法表
		
		let resid = checkTask.getOutPutModelId();
		
		etl.runETL(resid); // 提取模型表数据
	}

 */
export class checkTaskToDataFlow {
	private taskId: string;//检测任务id
	private taskInfo: CheckTaskInfo;
	private outPutModelId: string[];
	constructor(args: { taskId: string }) {
		this.taskId = args.taskId;
		this.taskInfo = this.buildTaskInfo();
		this.outPutModelId = [];
	}

	private buildTaskInfo() {
		let taskId = this.taskId;
		let ds = db.getDefaultDataSource();
		let checkTaskTable = ds.openTableData(CHECK_TASK_TABLE);
		print(taskId);
		let checkTaskInfo = checkTaskTable.selectFirst("*", { "CHECK_TASK_ID": taskId });
		print(checkTaskInfo);
		let taskName = checkTaskInfo["CHECK_TASK_NAME"];
		let outputDirPath = this.createOutputDir(taskId, taskName);
		let taskInfo: CheckTaskInfo = {
			taskId,
			taskName: taskName,
			schedule: checkTaskInfo["SCHEDULE_CODE"],
			isOutPut: checkTaskInfo["IS_OUTPUT_RESULT"],
			dataSource: checkTaskInfo["OUTPUT_DATASOURCE"],
			interceptRuleLevel: checkTaskInfo["INTERCEPT_RULE_LEVEL"],
			outputDirPath
		}
		return taskInfo;
	}
	
	
	/**
	 * 创建数据加工，生成合法表
	 */
	public createDataFlow() {
		let taskInfo = this.taskInfo;
		let ds = db.getDefaultDataSource();
		let checkResTable = ds.openTableData(CHECK_TASK_RES_TABLE);
		let checkResInfo = checkResTable.select("*", { "CHECK_TASK_ID": this.taskId });
		for (let i = 0; i < checkResInfo.length; i++) {
			print("开始创建第" + i + "个数据加工")
			let row = checkResInfo[i];
			let tableInfo = this.buildTableInfo(row);
			print("生成数据加工初始化结构" + JSON.stringify(tableInfo))
			let createCheckTable = new CheckTableToDataFlow({ tableInfo, taskInfo });
			createCheckTable.createOutPutDataFlow();
			this.outPutModelId.push(createCheckTable.getOutPutModelId());
		}
	}

	public getOutPutModelId() {
		return this.outPutModelId;
	}

	/**
	 * 构建检测资源生成加工所需的数据结构
	 */
	private buildTableInfo(row: any) {
		let errDetailTable = row["ERR_DATAIL_TABLE_PATH"];
		let resid = row["DW_TABLE_ID"];
		let ds = db.getDefaultDataSource();
		let checkResTable = ds.openTableData("SZSYS_4_META_TABLES");
		let dataSource = checkResTable.selectFirst("DATASOURCE", { "MODEL_RESID": resid });
		if (errDetailTable == null) {//如果没有指定错误明细表那么获取系统指定的明细表
			errDetailTable = "/sysdata/data/tables/dg/confs/" + dataSource + ".tbl";
		}
		let tableInfo: CheckTableInfo = {
			resid: row["DW_TABLE_ID"],
			dataFilter: row["DATA_FILTER"],
			isIncrement: row["IS_INCREMENT"],
			lastCheckTime: row["LAST_CHECK_TIME"],
			lastCheckTimeExp: row["LAST_CHECK_TIME_EXP"],
			errDetailTablePath: errDetailTable,
			isOutPut: row["IS_OUTPUT_RESULT"],
			dataSource
		};
		return tableInfo;
	}

	/**
	 * 生成检测任务的目录
	 */
	private createOutputDir(taskId, taskName) {
		let path = "/sdi/data/tables/sys/data-govern/" + taskId;
		metadata.mkdirs({
			name: taskId,
			desc: taskName,
			path
		});
		return path;
	}
}


/**
 * 将检测资源自动输出成加工
 * 生成的加工要考虑是否为增量检测
 * 如果为增量检测，那么生成加工时关联的有过滤条件，条件为最后检测时间等于DG_CHECK_TASK_TABLES中的最后成功检测时间
 * 判断当前检测资源是否指定了错误明细表路径，如果指定了要关联指定的错误明细表
 * //TODO 考虑数据期的影响
 * 如果是合法表，那么将被检测模型和检测结果表关联，取DATA_ID为空的数据，即为合法数据
 * //TODO 拦截表如何生成，多条规则同时拦截该怎么显示？
 */
class CheckTableToDataFlow {
	private tableInfo: CheckTableInfo;// 检测资源表信息
	private taskInfo: CheckTaskInfo;
	private tableDirPath: string;
	private tableType: string;//TODO 输出表类型，取值为legal intercept
	private checkTableInfo: MetaFileInfo;
	private dataFlowContent: JSONObject | boolean;
	private outPutModelId: string;
	constructor(args: { tableInfo: CheckTableInfo, taskInfo: CheckTaskInfo }) {
		this.tableInfo = args.tableInfo;
		this.taskInfo = args.taskInfo;
		this.createOutPutTableDir();
		this.checkTableInfo = this.getCheckTableMetaInfo();
	}

	/**
	 * 生成数据加工，并输出
	 */
	public createOutPutDataFlow() {
		let tableInfo = this.tableInfo;
		let resid = tableInfo.resid;
		if (tableInfo.isOutPut != '1') {
			console.debug("当前加工不用同步，删除掉加工的计划任务");
			resid != null && this.deleteTableToSchedule();
			return;
		}
		console.debug("开始构建加工");
		let content = this.dataFlowContent = this.constitueDataFlowContent();
		if (!content) {//如果构建出现异常了那么忽略该表继续创建后续加工
			return;
		}
		this.createDataFlowMetaFile();
	}

	/**
	 * 创建一个数据加工元数据文件
	 */
	private createDataFlowMetaFile() {
		let tableDirPath = this.tableDirPath;
		let metaTableInfo = this.checkTableInfo;
		let name = metaTableInfo.name;
		let tableName = name.replace(".tbl", "") + "_LEAGAL_TABLE"
		let path = tableDirPath + "/" + tableName + ".tbl";
		let fileExist = metadata.getFile(tableDirPath + "/" + tableName + ".tbl");
		console.debug(`当前数据加工：${tableName}，是否存在：${fileExist}`);
		if (fileExist == null) {
			try {
				console.debug(`当前创建的数据加工路径为：${path}`);
				let file = metadata.createFile({
					name: tableName,
					path: path,
					type: "tbl",
					content: JSON.stringify(this.dataFlowContent)
				});
				let id = this.outPutModelId = file.id;
				dw.refreshState(id);
			} catch (e) {
				console.error("创建数据加工异常");
				console.error(e);
			}
		} else {
			let id = this.outPutModelId = fileExist.id;
			metadata.modifyFile(id, { content: JSON.stringify(this.dataFlowContent) });
		}
		//this.insertTableToSchedule();
	}

	/**
	 * 生成合法表
	 * 
	 * 20220507 tuzw
	 * 问题：输出合法表支持根据问题严重等级——错误 OR 预警进行数据拦截
	 * 地址：https://jira.succez.com/browse/CSTM-18682
	 */
	public constitueDataFlowContent() {
		let fields = this.getCheckTableFields();
		let checkTableNode: DataFlowNodeInfo = this.constituteCheckTableNode(fields);

		let checkErrorTableNode: DataFlowNodeInfo = this.constituteCheckErrorNode();
		let ruleTableNode: DataFlowNodeInfo = this.constitueRuleNode();
		let errorNodeJoinRuleNode: DataFlowNodeInfo = this.constitueErrorJoinRuleNode(checkErrorTableNode, ruleTableNode);

		let joinNode = this.constitueJoinNode(checkTableNode, errorNodeJoinRuleNode);
		let outputNode = this.constitueOutPutNode(joinNode);
		let prop = this.constitueProp();
		let nodes: JSONObject = {};

		nodes[checkErrorTableNode.id] = checkErrorTableNode;
		nodes[ruleTableNode.id] = ruleTableNode;
		nodes[errorNodeJoinRuleNode.id] = errorNodeJoinRuleNode;

		nodes[checkTableNode.id] = checkTableNode;
		nodes[joinNode.id] = joinNode;
		nodes[outputNode.id] = outputNode;
		return {
			version: "4.0.0",
			properties: prop,
			dimensions: outputNode.dimensions,
			measures: outputNode.measures,
			dataFlow: { nodes }
		}
	}

	/**
	 * 构建数据加工-检测表节点
	 */
	private constituteCheckTableNode(fieldsInfo: { dimensions: Array<DwTableFieldItemInfo>, measures: Array<DwTableFieldInfo | DwTableFieldFolderInfo> }) {
		let tableInfo = this.tableInfo;
		let isIncrement = tableInfo.isIncrement;
		let lastCheckTimeExp = tableInfo.lastCheckTimeExp;
		let metaTableInfo = this.checkTableInfo;
		let tablePath = metaTableInfo.path;
		let name = metaTableInfo.name;
		let fields = [];
		let dimensions = fieldsInfo.dimensions;
		let measures = fieldsInfo.measures;
		fields.pushAll(dimensions);
		fields.pushAll(measures);
		let mainNode: DataFlowNodeInfo = {
			id: utils.uuid(),
			alias: name.replace(".tbl", ""),
			type: "ModelTable",
			moduleTablePath: tablePath,
			checkFields: true,
			checkFreshData: false,
			checkRows: false,
			fields,
			dimensions,
			measures,
			lastModifyTime: new Date().getTime()
		}
		if (isIncrement == '1') {
			let content = JSON.parse(metaTableInfo.getContentAsString());
			let prop = content.properties;
			let lastUpdateTime = prop.lastUpdateTime;
			mainNode.filter = {
				"matchAll": true,
				"enabled": true,
				"clauses": [
					{
						"exp": `[${lastUpdateTime}]>=${lastCheckTimeExp}`
					}
				]
			}
		}
		return mainNode;
	}

	/**
	 * 构建数据加工-检测结果错误表节点
	 */
	private constituteCheckErrorNode() {
		let tableInfo = this.tableInfo;
		let taskInfo = this.taskInfo;
		let taskId = taskInfo.taskId;
		let lastCheckTime = tableInfo.lastCheckTime;
		let errDetailTablePath = tableInfo.errDetailTablePath;
		let name;
		let tableName = "SZSYS_4_DG_ERR_DETAIL";
		if (errDetailTablePath == null) {
			let fileInfo = this.checkTableInfo;
			let content = JSON.parse(fileInfo.getContentAsString());
			let prop = content.properties;
			name = prop.datasource == "defdw" ? "default" : prop.datasource;
		} else {
			let fileInfo = metadata.getFile(errDetailTablePath);
			let content = JSON.parse(fileInfo.getContentAsString());
			let prop = content.properties;
			name = prop.datasource == "defdw" ? "default" : prop.datasource;
			tableName = prop.dbTableName;
		}
		let dimensions = DG_ERR_DETAIL_FIELDS;
		let clauses: FilterClauseInfo[] = [{
			"leftExp": "[问题表ID]",
			"operator": ClauseOperatorType.DoubleEqual,
			"rightValue": tableInfo.resid
		}, {
			"exp": "[是否修正]=0 or [是否修正] is null"
		}, {
			"leftExp": "[检测任务编码]",
			"operator": ClauseOperatorType.DoubleEqual,
			"rightValue": taskId
		}, {
			"exp": `[最后检测时间]='${utils.formatDate('yyyy-MM-dd HH:mm:ss', lastCheckTime)}'`
		}]
		let subNode: DataFlowNodeInfo = {
			id: utils.uuid(),
			alias: "SZSYS_4_DG_ERR_DETAIL",
			type: "DbTable",
			datasource: name,
			dbTableName: tableName,
			checkFields: true,
			checkFreshData: false,
			checkRows: false,
			fields: dimensions,
			dimensions,
			measures: [],
			lastModifyTime: new Date().getTime(),
			filter: {
				clauses,
				"matchAll": true,
				"enabled": true
			}
		};
		return subNode;
	}

	/**
	 * 数据加工-（检测结果错误表和规则表内连接结果）与 检测表节点 左连接
	 * @param mainNode 检测表节点
	 * @param subNode 检测结果错误表和规则表内连接结果
	 * @return 
	 */
	private constitueJoinNode(mainNode: DataFlowNodeInfo, subNode: DataFlowNodeInfo): DataFlowNodeInfo {
		let mainNodeId = mainNode.id;
		let mainNodeName = mainNode.alias;
		let subNodeId = subNode.id;
		let subNodeName = subNode.alias;
		let fields = [];
		/** 控制当前节点显示的字段信息 */
		let dimensions = [];
		let mainNodeFields = this.cloneFieldInfo(mainNode.fields, mainNodeName);
		let subNodeFieldss = this.cloneFieldInfo(subNode.fields, subNodeName, true);
		let mainNodeDimFields = this.cloneFieldInfo(mainNode.dimensions, mainNodeName);
		let mainNodeMeasureFields = this.cloneFieldInfo(mainNode.measures, mainNodeName);
		// let subNodeDimFields = this.cloneFieldInfo(subNode.dimensions, subNodeName, true);
		fields.pushAll(mainNodeFields);
		fields.pushAll(subNodeFieldss);
		dimensions.pushAll(mainNodeDimFields);
		// dimensions.pushAll(subNodeDimFields);
		let sysTimeField = {
			"name": "SYS_LEGAL_CREATE_TIME",
			"dbfield": "SYS_LEGAL_CREATE_TIME",
			"dataType": "P",
			"exp": "NOW()",
			"isDimension": true,
			"extractable": true
		};
		fields.push(sysTimeField);
		dimensions.push(sysTimeField);
		let content = JSON.parse(this.checkTableInfo.getContentAsString());
		let properties = content.properties;
		let primaryKeys = properties.primaryKeys;
		let joinNode: DataFlowNodeInfo = {
			type: "Join",
			inputNodes: [mainNodeId, subNodeId],
			alias: "关联",
			id: utils.uuid(),
			joinConditions: [{
				joinType: SQLJoinType.LeftJoin,
				leftTable: mainNodeName,
				rightTable: subNodeName,
				clauses: [{
					"leftExp": `[${mainNodeName.toUpperCase()}].[${primaryKeys[0]}]`,
					"operator": ClauseOperatorType.Equal,
					"rightExp": `[${subNodeName.toUpperCase()}].[数据标识]`,
					"clauseType": 0
				}]
			}],
			filter: {
				"matchAll": true,
				"enabled": true,
				"clauses": [
					{
						"leftExp": "[数据标识]",
						"operator": ClauseOperatorType.Empty,
						"rightValue": ""
					}
				]
			},
			lastModifyTime: new Date().getTime(),
			fields,
			dimensions,
			measures: mainNodeMeasureFields
		};
		return joinNode
	}

	/**
	 * 数据加工- 检测结果错误表与规则表 内连接
	 * @param checkResultErrorNode 检测规则错误表节点
	 * @param ruleNode 系统规则表节点
	 * @return 
	 */
	private constitueErrorJoinRuleNode(checkResultErrorNode: DataFlowNodeInfo, ruleNode: DataFlowNodeInfo): DataFlowNodeInfo {
		console.debug(`constitueErrorJoinRuleNode()，数据加工-使检测错误表节点与规则表节点关联`);
		let errorNodeId = checkResultErrorNode.id as string;
		let errorNodeName = checkResultErrorNode.alias as string;
		let ruleNodeId = ruleNode.id as string;
		let ruleNodeName = ruleNode.alias as string;
		let fields = [];
		/** 当前节点输出字段 */
		let dimensions = [];
		let errorNodeFields = this.cloneFieldInfo(checkResultErrorNode.fields, errorNodeName);
		let ruleNodeFieldss = this.cloneFieldInfo(ruleNode.fields, ruleNodeName, true);
		let errorNodeDimFields = this.cloneFieldInfo(checkResultErrorNode.dimensions, errorNodeName);
		let ruleNodeDimFields = this.cloneFieldInfo(ruleNode.dimensions, ruleNodeName, true);
		let mainNodeMeasureFields = this.cloneFieldInfo(checkResultErrorNode.measures, errorNodeName);

		fields.pushAll(errorNodeFields);
		fields.pushAll(ruleNodeFieldss);
		dimensions.pushAll(errorNodeDimFields);
		dimensions.pushAll(ruleNodeDimFields);

		let errorNodeJoinRuleNode: DataFlowNodeInfo = {
			type: "Join",
			inputNodes: [errorNodeId, ruleNodeId],
			alias: "关联",
			id: utils.uuid(),
			joinConditions: [{
				joinType: SQLJoinType.InnerJoin,
				leftTable: errorNodeName,
				rightTable: ruleNodeName,
				clauses: [{
					"leftExp": `[${errorNodeName.toUpperCase()}].[规则代码]`,
					"operator": ClauseOperatorType.Equal,
					"rightExp": `[${ruleNodeName.toUpperCase()}].[规则代码]`,
					"clauseType": 0
				}]
			}],
			lastModifyTime: new Date().getTime(),
			fields,
			dimensions,
			measures: mainNodeMeasureFields
		};
		return errorNodeJoinRuleNode;
	}

	/**
	 * 构建数据加工-规则表节点
	 * @return
	 */
	private constitueRuleNode() {
		let taskInfo = this.taskInfo;

		let ruleTableName: string = "SZSYS_4_DG_RULES";
		let ruleTablePath: string = "/sysdata/data/tables/dg/DG_RULES.tbl";

		let fileInfo = metadata.getFile(ruleTablePath);
		let content = JSON.parse(fileInfo.getContentAsString());
		let prop = content.properties;
		let datasource: string = prop.datasource == "defdw" ? "default" : prop.datasource;

		let dimensions: DwTableFieldItemInfo[] = DG_RUEL_FIELDS;
		let clauses: FilterClauseInfo[] = [{
			"leftExp": "[错误级别].[代码]",
			"operator": ClauseOperatorType.GreaterEqual,
			"rightValue": !!taskInfo.interceptRuleLevel ? taskInfo.interceptRuleLevel : 1
		}]

		let ruleTableNode: DataFlowNodeInfo = {
			id: utils.uuid(),
			alias: ruleTableName,
			type: "DbTable",
			datasource: datasource,
			dbTableName: ruleTableName,
			checkFields: true,
			checkFreshData: false,
			checkRows: false,
			fields: dimensions,
			dimensions,
			measures: [],
			lastModifyTime: new Date().getTime(),
			filter: {
				clauses,
				"matchAll": true,
				"enabled": true
			}
		};
		return ruleTableNode;
	}

	/**
	 * 构建数据加工-模型输出节点
	 * @param joinNode （检测结果错误表和规则表内连接结果）与 检测表节点 左连接
	 */
	private constitueOutPutNode(joinNode: DataFlowNodeInfo) {
		let fields = this.cloneFieldInfo(joinNode.fields);
		let dimensions = this.cloneFieldInfo(joinNode.dimensions);
		let measures = this.cloneFieldInfo(joinNode.measures);
		let outPutNode: DataFlowNodeInfo = {
			id: "default",
			type: "Output",
			alias: "模型输出",
			inputNodes: [joinNode.id],
			lastModifyTime: new Date().getTime(),
			fields,
			dimensions,
			measures
		};
		return outPutNode;
	}

	private constitueProp() {
		let taskInfo = this.taskInfo;
		let tableInfo = this.tableInfo;
		let dataSource = taskInfo.dataSource;
		if (dataSource == null) {
			dataSource = tableInfo.dataSource;
		}
		let metaTableInfo = this.checkTableInfo;
		let name = metaTableInfo.name;
		let content = JSON.parse(metaTableInfo.getContentAsString());
		let properties = content.properties;
		let primaryKeys = properties.primaryKeys;
		let dbTableName = name.replace(".tbl", "") + "_LEAGAL_TABLE"
		let prop: DwTablePropertiesInfo = {
			modelDataType: ModelDataType.DataFlow,
			extractDataEnabled: true,
			rollbackEnabled: false,
			modifyDbTableEnabled: [
				"field"
			],
			rebuildIndexAndKey: false,
			datasource: dataSource,
			dbTableName,
			dbSchema: "",
			primaryKeys,
			extractMethod: "OverwriteAll"
		};
		if (tableInfo.isIncrement) {
			prop.extractMethod = "Merge";
			prop.mergeData = true;
		}
		return prop;
	}

	/**
	 * 复制字段信息用于构成输出节点信息，不要复制exp元素
	 * @param old_field
	 * @returns
	 */
	private cloneFieldInfo(old_field, originalNode?, isHidden?: boolean) {
		let fieldList = [];
		for (let j = 0; old_field && j < old_field.length; j++) {
			let keys = Object.keys(old_field[j]);
			let field: JSONObject = {};
			for (let i = 0; i < keys.length; i++) {
				let element = keys[i];
				field[element] = old_field[j][element];
			}
			if (originalNode != null) {
				field.originalNode = originalNode;
				field.originalField = field.name;
				field.inputNode = originalNode;
			}
			if (field.name == "数据标识") {
				field.inputField = field.name;
			}
			if (isHidden) {
				field.modifiedInfo = {
					"hidden": true
				}
			}
			if (field.exp != null) {
				field.originalNode = "关联";
				field.inputField = field.name;
				field.originalField = field.name;
				field.exp = null;
			}
			fieldList.push(field);
		}
		return fieldList;
	}

	/**
	 * 获取被检测模型维表和度量字段
	 * @returns
	 */
	private getCheckTableFields(): null | { dimensions: Array<DwTableFieldItemInfo>, measures: Array<DwTableFieldInfo | DwTableFieldFolderInfo> } {
		let checkTableInfo = this.checkTableInfo;
		let content = JSON.parse(checkTableInfo.getContentAsString());
		let dimensions = content.dimensions;
		for (let i = 0; i < dimensions.length; i++) {
			let dim = dimensions[i];
			dim.inputField = dim.name;
		}
		let measures = content.measures;
		for (let i = 0; measures && i < measures.length; i++) {
			let measure = measures[i];
			measure.inputField = measure.name;
		}
		return { dimensions, measures };
	}

	/**
	 * 生成检测任务输出表的目录
	 */
	public createOutPutTableDir() {
		let tableInfo = this.tableInfo;
		let taskInfo = this.taskInfo;
		let resid = tableInfo.resid;
		let outputDirPath = taskInfo.outputDirPath;
		let outputTableDirPath = this.tableDirPath = outputDirPath + "/" + resid;
		let metaFile = metadata.getFile(resid);
		let desc = metaFile.desc || metaFile.name;
		metadata.mkdirs({
			name: resid,
			desc,
			path: outputTableDirPath
		});
	}

	/**
	 * 获取被检测模型元数据信息
	 * @returns
	 */
	private getCheckTableMetaInfo(): MetaFileInfo {
		let tableInfo = this.tableInfo;
		let resid = tableInfo.resid;
		let file = metadata.getFile(resid);
		return file;
	}

	/**
	 * 终止当前模型的所有计划任务
	 * @param resid 模型id
	*/
	private deleteTableToSchedule() {
		let resid = this.tableInfo.resid;
		let ds = db.getDefaultDataSource();
		let table = ds.openTableData(SCHEDULETASK);
		table.del({ "RESID": resid });
	}

	/**
	 * 将资源添加到任务表中，如果存在一条数据，计划id和资源id一模一样，那么就不做任何修改
	 * 如果不一样则去修改计划id
	 * 如果不存在则新增数据
	 * @param resid
	 * @param schedule
	*/
	public insertTableToSchedule() {
		console.debug(`insertTableToSchedule，将资源添加到任务表中`);
		let resid = this.outPutModelId;
		let taskInfo = this.taskInfo;
		let taskId = taskInfo.taskId;
		let ds = db.getDefaultDataSource();
		let table = ds.openTableData(SCHEDULETASK);
		let info = table.selectRows(["ID", "SCHEDULE_ID"], { "RESID": resid });
		let userId = security.getCurrentUser().userInfo?.userId;
		let now = new Timestamp(new Date().getTime());
		console.debug(`insertTableToSchedule，将资源添加到任务表中，当前模型id为${resid},当前查询出已存在的计划任务条数为${info.length}`);
		if (info.length == 0) {
			table.insert({ ID: utils.uuid(), SCHEDULE_ID: taskId, RESID: resid, CREATOR: userId, CREATE_TIME: now, TYPE: 'etl', PRIORITY: 50, ENABLE: 1 });
		} else {
			table.update({ SCHEDULE_ID: taskId }, { RESID: resid });
		}
	}

	public getOutPutModelId() {
		return this.outPutModelId;
	}
}