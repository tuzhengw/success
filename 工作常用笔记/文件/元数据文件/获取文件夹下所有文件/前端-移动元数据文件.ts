/**
 * 作者：liuyz
 * 创建日期：2022-04-24
 * 脚本用途：用于初始化数据中台环境，初始化维表数据，初始化扩展控件
 */
import { showConfirmDialog } from "commons/dialog";
import { getMetaRepository, InterActionEvent } from "metadata/metadata";
import { rc, showSuccessMessage, showWaiting, showWaitingMessage, showWarningMessage, SZEvent, uuid, showProgressDialog } from "sys/sys";

/**
 * 初始化服务
 */
export class InitializeService {
	private extensionsFold: FileInfoOrPathOrId[] = ["/succ-dataVisualization-customselectpanel", "/succ-dataVisualization-dataLabelDisplay", "/succ-dataVisualization-modelDataRange", "/succ-dataVisualization-searchbox", "/succ-fappComponent-customResourceSelector", "/succ-sdlExtensionsExpression"];
	public CustomActions: any;
	constructor() {
		this.CustomActions = {
		}
	}


	/**
	 * 复制扩展控件到app中
	 */
	public copyExtensionsToApp(event: InterActionEvent) {
		let files: FileInfoOrPathOrId[] = [];
		this.extensionsFold.forEach(f => {
			files.push('/sysdata/extensions' + f);
		});
		showConfirmDialog({
			id: "move.extensions",
			caption: "复制扩展到项目",
			message: `确认要将${this.extensionsFold.join(",")}这些扩展复制到zt.app项目内吗？`,
			onok: () => {
				this.moveToFold(files, "/sysdata/app/zt.app/initialize/extensions");
			}
		})
	}

	/**
	 * 移动文件到指定目录
	 * @param filesInfoOrIdOrPath
	 * @param targetDir
	 * @returns
	 */
	private moveToFold(filesInfoOrIdOrPath: FileInfoOrPathOrId[], targetDir: string) {
		let promise = getMetaRepository().copyTo({
			filesInfoOrIdOrPath: filesInfoOrIdOrPath,
			parentDir: targetDir,
			overwrite: true
		});
		showWarningMessage("正在复制中，请稍后");
		return showWaiting(promise).then(() => {
			showSuccessMessage("复制成功！");
		});
	}
}