
import { InterActionEvent } from "metadata/metadata-script-api";
import { ExpCompiler, ExpTokenIndex, Token } from "commons/exp/expcompiler";
import { CustomRuleFieldDp } from '../data-govern/data-govern-mgr.js';
import { showWarningMessage } from "sys/sys";

/**
 * 自定义规则表达式处理类
 */
export class CustomExpCompiler extends ExpCompiler {

    constructor(args?) {
        super(args);
    }

    public getParentVar(prevToken: Token, compiler: ExpCompiler, varnames?: Array<string>): string | boolean {
        if (!varnames)
            varnames = [];
        while (prevToken) {
            if (prevToken.getIndex() === ExpTokenIndex.DOT) {
                prevToken = prevToken.getPrevToken();
                if (prevToken) {
                    if (prevToken.getIndex() === ExpTokenIndex.IDENTIFIER) {
                        varnames.unshift(prevToken.orginToken);
                    } else {
                        return false; // 点前面不是标识符
                    }
                    prevToken = prevToken.getPrevToken();
                    continue;
                } else { // 点前面没有token，
                    return false;
                }
            }
            break;
        }
        return varnames[0];
    }

    public getAllParentVar(exp: string): string[] {
        let expression = this.compile(exp);
        let tokens = expression.getAllTokens();
        let parentVars: string[] = [];
        for (let i = 0; i < tokens.length; i++) {
            let token = tokens[i];
            let parentVar: string = this.getParentVar(token.getPrevToken(), this) as string;
            if (parentVar && parentVars.indexOf(parentVar) == -1) {
                parentVars.push(parentVar);
            }
        }
        return parentVars;
    }
}

/**
 * 自定义脚本类
 */
export class CustomExpCompilerJs {

    /** 自定义规则DP对象  */
    public dgRuleFieldDp: CustomRuleFieldDp;
    /** 自定义规则表达式对象 */
    public customExpCompiler: CustomExpCompiler;

    public CustomActions: any;
    constructor() {
        this.CustomActions = {
            button_Js_paraseRuleExp: this.button_Js_paraseRuleExp.bind(this)
        }
        this.dgRuleFieldDp = new CustomRuleFieldDp();
        this.customExpCompiler = new CustomExpCompiler();
    }

    /**
     * 解析规则表达式中存在的别名
     * 注意：
     * 1）解析的别名必须先通过dp对象查询出来，否则，无法识别表达式的变量
     * 2）我们自己定制的DP对象，外层包装，先通过外层dp将数据查询出来，再将数据写入到表达式dp对象中
     * 3）这里dp查询的是数据标准表的别名（英文表名）
     */
    public button_Js_paraseRuleExp(event: InterActionEvent): Promise<void> {
        return this.dgRuleFieldDp.initTableAndFieldsInfo({
            checkTableType: CheckTableType.StandardCheck,
            standardLibId: "5a4500c9b69942e29cb3f0e53b579c9a"
        }).then(() => { // 重新加载字段信息
            this.customExpCompiler.setDataProvider(this.dgRuleFieldDp.expDp);
            let ruleExp: string = '[TEST_STUDENT].[FIELD1] is not null';
            let ruleAlias: string[] = [];
            try {
                ruleAlias = this.customExpCompiler.getAllParentVar(ruleExp)
            } catch (e) { // eg：变量“TEST_STUDENT”不存在属性ID。
                showWarningMessage(`${e.toString()}，获取别名失败`);
            }
            console.log(ruleAlias); // ["[TEST_STUDENT]"]
        });
    }
}

/** 获取检测表类型 */
enum CheckTableType {
    /** 数据检测 */
    DataGovernCheck = 'governCheck',
    /** 数据标准 */
    StandardCheck = 'standardCheck'
}