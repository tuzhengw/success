
import HttpClients = org.apache.http.impl.client.HttpClients;
import EntityUtils = org.apache.http.util.EntityUtils;
import HttpGet = org.apache.http.client.methods.HttpGet;
import File_ = java.io.File; // 避免与前端File冲突

import { getWorkDir } from "svr-api/sys";
import { uuid } from "svr-api/utils";
import { getDataSource } from "svr-api/db"; //数据库相关API
import { getFile } from "svr-api/fs";
/**
 * 批处理脚本模版
 * 创建的文件：在本地环境的workDir目录下查看
 */
function main() {
	let pdfTest = "https://wx.ctrl.jianguancloud.com/api/sabaFile/fileOperation/external/download?fileIdStr=w9VMrTS0KKDZ6SSp5J_RTQ";
    let pngTest = "https://wx.ctrl.jianguancloud.com/api/sabaFile/fileOperation/external/download?fileIdStr=w9VMrTS0KKDZ6SSp5J_RTQ";
    let { result, fileIo, fileName } = getFileIo(pdfTest);

    // if (result) {
    //     let fileParent = "test";
    //     createFile(fileParent, "pdfFile1", fileType, fileIo as ArrayLike<number>);
    // }
}

/**
 * 根据文件二进制流在对应服务器下创建文件
 * eg：SuccBI-trial-4.16.0\SuccBI-standalone-4.16.0\workdir\internetDeal\test\imageFile.pdf
 * 扩展知识：
 * （1）File类时对【文件系统】的映射，并不是硬盘上真实的文件
 * （2）new File("xxx") 只是在内存中创建File文件映射对象，并不会在硬盘创建文件 
 * 参考：https://blog.csdn.net/weixin_33994429/article/details/94455128
 * @param parentFileName 父文件夹名
 * @param fileName 文件名.文件类型
 * @param fileIoByte 写入给定文件的文件二进制流bao
 * @return { result: true }
 */
function createFile(parentFileName: string, fileName: string, fileIoByte: ArrayLike<number>): ResultInfo {
    let resultInfo: ResultInfo = { result: true };
    let workDir = getWorkDir();
    let file = new java.io.File(`${workDir}/clusters-share/internetDeal/${parentFileName}/${fileName}`);
    if (!file.exists()) {
        print(`------文件${fileName}不存在硬盘中，已创建文件`);
        let parentFile = file.getParentFile();
        if (!parentFile.exists()) {
            parentFile.mkdirs();    // 若文件父目录不存在，则创建文件以及父文件目录
        }
        file.createNewFile(); // 创建真实的文件
    }
    file = getFile(`${workDir}/clusters-share/internetDeal/${parentFileName}/${fileName}`); // 读取文件
    let outStream = file.openOutputStream();
    try {
        outStream.write(fileIoByte as number[]);
    } catch (e) {
        resultInfo.result = false;
        print(`------写入文件${fileName}内容失败---`);
        print(e);
    } finally {
        outStream.close();
    }
    return { result: true };
}



import EntityUtils = org.apache.http.util.EntityUtils;
/**
 * 读取【下载链接】文件内容 和 文件名.类型
 * @param url 文件下载链接
 * @return eg: { fileIo: [文件二进制流数组], "test.png" }
 */
function getFileIo(url: string): { result: boolean, fileIo?: ArrayLike<number> | undefined | string, fileName?: string } {
    const client = HttpClients.createDefault();
    try {
        const httpGet = new HttpGet(url);
        const response = client.execute(httpGet);
        try {
            const entity = response.getEntity();
            const contentType = response.getFirstHeader("Content-type").getValue();
            if (contentType.indexOf("image") !== -1 || contentType.indexOf("pdf") !== -1) {
                let fileName = response.getFirstHeader("Content-Disposition").getValue().split(';')[1].split('=')[1];
                /**
                 * 20220208 tuzw 
                 * 对方文件夹含有大量的中文名，直接获取是一串乱码，需要将其转换为utf-8
                 */
                let encodeFileName = new java.lang.String(fileName.getBytes("ISO-8859-1"), "utf-8");
                return { result: true, fileIo: EntityUtils.toByteArray(entity), fileName: encodeFileName.replace(/\"/g, "") }; // 去除字符串左右‘双引号’
            }
            print("-----文件类型错误-----");
            const content = EntityUtils.toString(entity);
            if (content.indexOf("认证失败") !== -1) {
                print(content);
                return { result: false };
            }
            return { result: false };
        } finally {
            response.close();
        }
    } finally {
        client.close();
    }
}