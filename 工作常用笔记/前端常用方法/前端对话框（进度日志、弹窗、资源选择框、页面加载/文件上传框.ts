/**
 * 互联互通-注册页面
 */
export class HTLD_RegisterWord {

	public CustomActions: any;
	constructor() {
		this.CustomActions = {
			button_uploadXmlFile: this.button_uploadXmlFile.bind(this)
		}
	}


	/**
	 * 2022-10-17 丁煜
	 * https://jira.succez.com/browse/CSTM-20823
	 * 在spg中实现xml上传功能，并且在后端脚本中获取到上传的文件
	 */
	public button_uploadXmlFile(event: InterActionEvent): Promise<any> {
		let page = event.page;
		return new Promise((resolve, reject) => {
			return showUploadDialog({
				fileTypes: ["xml"],
				multiple: false,
				maxSize: 1024 * 1024,
				onchange: (sez: SZUploadEvent) => {
					resolve(this.getUploadFile(page, sez));
				},
				oncancel: (e: SZUploadEvent) => { // 选择文件时点了取消按钮，不应该执行导入数据节点
					page.setParameter("taskId", "");
					resolve(AnaActionTriggerResultType.Canceled);
				}
			});
		});
	}

	/**
	 * 请求获取上传文件
	 * @param page
	 * @param sez 
	 */
	private getUploadFile(page: IVPage, sez: SZUploadEvent): Promise<boolean> {
		return rc<{ result: boolean, message: string, wordId?: string, taskId?: string }>({
			url: "/HLHT/app/shareWord.app/API/xmlPaser.action?method=getUploadFile",
			data: {
				fileId: sez.value![0].id
			}
		}).then(data => {
			let caption = '导入文件校验';
			if (!data.result) {
				caption = '导入文件校验出错';
			}
			if (data.wordId && data.taskId) {//TODO 添加按钮实现点击跳转到错误属性信息查看界面
				page.setParameter("taskId", data.taskId);
				return true;
			} else {
				showConfirmDialog({
					caption: caption,
					message: data.message
				});
				return false;
			}
		});
	}
}
