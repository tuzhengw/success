
/**
 * =====================================================
 * 作者：tuzw
 * 审核人员：liuyz
 * 创建日期: 2022-08-16
 * 功能描述：系统管理脚本
 * =====================================================
 */
import { isEmpty, showWarningMessage, showSuccessMessage, showDialog, rc } from "sys/sys";
import { InterActionEvent, IVPage } from "metadata/metadata-script-api";
import { Dialog, ShowDialogArgs } from "commons/dialog";
import { ProgressLogs } from "commons/progress";
import { showUploadDialog } from "commons/basic";
import { SZUploadEvent } from "commons/upload";
import { AnaActionTriggerResultType } from "ana/anabrowser";


/**
 * 系统恢复SPG脚本
 */
class SpgSysRestoreMgr {
	public CustomActions: any;
	constructor() {
		this.CustomActions = {
			sys_UploadRestorePackage: this.sysUploadRestorePackage.bind(this)
		}
	}

	/**
	 * 上传备份的文件, 并将文件ID和文件名设置到SPG页面
	 * @param fileIdParamName 记录上传文件ID的SPG参数名
	 * @param fileNameParamName 记录上传文件名的SPG参数名
	 */
	public sysUploadRestorePackage(event: InterActionEvent) {
		let page = event.page;
		let params = event.params;
		let fileIdParamName: string = params?.fileIdParamName;
		let fileNameParamName: string = params?.fileNameParamName;
		if (isEmpty(fileIdParamName) || isEmpty(fileNameParamName)) {
			showWarningMessage(`未指定值所存SPG全局参数名称`);
			return;
		}
		return new Promise((resolve, reject) => {
			return showUploadDialog({
				fileTypes: ["zip"],
				multiple: false,
				maxSize: 1024 * 1024 * 100, // 上传文件最大100M
				onchange: (sez: SZUploadEvent) => {
					let fileInfos = sez.value as FileInfo[];
					if (isEmpty(fileInfos) || fileInfos.length == 0) {
						showWarningMessage(`未获取到上传的文件信息`);
					} else {
						let fileInfo = fileInfos[0];
						page.setParameter(fileIdParamName, fileInfo.id);
						page.setParameter(fileNameParamName, fileInfo.name);
						showSuccessMessage("上传成功");
					}
					resolve(AnaActionTriggerResultType.Success);
				},
				oncancel: (e: SZUploadEvent) => {
					resolve(AnaActionTriggerResultType.Canceled);
				}
			});
		});
	}
}