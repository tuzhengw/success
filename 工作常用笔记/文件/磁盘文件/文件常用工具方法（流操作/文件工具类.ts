import HttpClients = org.apache.http.impl.client.HttpClients;
import EntityUtils = org.apache.http.util.EntityUtils;
import HttpGet = org.apache.http.client.methods.HttpGet;
import File_ = java.io.File; // 工具类不需要对象，直接import导入

import ZipOutputStream = java.util.zip.ZipOutputStream;
import ZipEntry = java.util.zip.ZipEntry;
import ArrayUtils = org.apache.commons.lang3.ArrayUtils;

import FileOutputStream = java.io.FileOutputStream;
import FileInputStream = java.io.FileInputStream;
import Files_ = java.nio.file.Files;
import Paths_ = java.nio.file.Paths;
import IOUtils = org.apache.commons.io.IOUtils;
import { getFile, deleteFile } from "svr-api/fs";

const formatDate = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); // formatDate.format(new java.util.Date())

/**
 * 获取字符串的字节数组
 */
function getStrBytes(): number[] {
	let xmlJavaStr = new java.lang.String("galjglajgagwga");
    return xmlJavaStr.getBytes();
}


File类常用方法
地址：https://blog.csdn.net/qq_53679247/article/details/127005811

public String getName()：返回File对象锁表示的文件名或者目录名（若为目录，返回的是最后一级子目录）。
public String getParent()：返回此File对象所对应的路径名，返回String类型。
public File getParentFile()：返回此File对象的父目录，返回File类型。
public String getPath()：返回此File对象所对应的路径名，返回String类型。
public boolean isAbsolute()：判断File对象所对应的文件或者目录是否是绝对路径。
public String getAbsolutePath()：返回此File对象所对应的绝对路径，返回String类型。
public String getCanonicalPath() throws IOException：
public File getCanonicalFile() throws IOException：
public File getAbsoluteFile()：返回此File对象所对应的绝对路径，返回File类型。
public boolean canRead()：判断此File对象所对应的文件或目录是否可读。
public boolean canWrite()：判断此File对象所对应的文件或目录是否可写。
public boolean canExecute()：判断此File对象所对应的文件或目录是否可执行。
public boolean exists()：判断此File对象所对应的文件或目录是否存在。
public boolean isDirectory()：判断此File对象是否为目录。
public boolean isFile()：判断此File对象是否为文件。
public boolean isHidden()：判断此File对象是否为隐藏。
public long lastModified()：返回该File对象最后修改的时间戳，我们可以通过SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");进行格式化为时间日期展示。
public boolean setLastModified(long time)：设置该File对象最后修改的时间戳。
public long length()：返回该File对象的文件内容长度。
public boolean createNewFile() throws IOException：当此File对象所对应的文件不存在时，该方法会新建一个该File对象所指定的新文件，如果创建成功，返回true；否则，返回false。
public boolean delete()：删除File对象所对应的文件或目录，删除成功，返回true；否则，返回false。
public void deleteOnExit()：Requests that the file or directory denoted by this abstract pathname be deleted when the virtual machine terminates.意思就是在VM关闭的时候，删除该文件或者目录，不像delete()方法一调用就删除。一般用于临时文件比较合适。
public String[] list()：列出File对象的所有子文件名和路径名，返回的是String数组。
public File[] listFiles()：列出File对象的所有子文件吗和路径名，返回的是File数组。
public boolean mkdir()：创建目录，并且只能在已有的父类下面创建子类，如果父类没有，那么就无法创建子类。
public boolean mkdirs()：也是创建目录，而且可以在父文件夹不存在的情况下，创建子文件夹，顺便将父文件夹也创建了，递归创建。
public boolean renameTo(File dest)：重命名此File对象所对应的文件或目录，如果重命名成功，则返回true；否则，返回false。
public boolean setReadOnly()：设置此File对象为只读权限。
public boolean setWritable(boolean writable, boolean ownerOnly)：写权限设置，writable如果为true，允许写访问权限；如果为false，写访问权限是不允许的。ownerOnly如果为true，则写访问权限仅适用于所有者；否则它适用于所有人。
public boolean setWritable(boolean writable)：

/**
 * 20220209 tuzw
 * 文件工具类
 */
class FileUtils {
	
	/**
	 * 获取文件字节流
	 * @param filePath 文件绝对路径
	 */
	public getFileBytes(filePath: string): number[] {
		let sendFile = new java.io.File(filePath);
		let fileBytes: number[] = Files_.readAllBytes(Paths_.get(sendFile.getAbsolutePath())); // 通过绝对路径获取文件字节流
		return fileBytes;
	}

    /**
     * 给指定目录下，新增一个文件
     * @param dirPath 目录路径，eg：/tempSaveZipFile/
     * @param newAddFilePath 新增的文件路径，eg：/tempSaveZipFile/t.doc
     * @param copyFileName 复制后的文件名.类型，eg：copy_t.doc
     * @return true | false
     */
    public dirAddNewFile(dirPath: string, newAddFilePath: string, copyFileName?: string): boolean {
        if (dirPath == "" || newAddFilePath == "") {
            print(`dirAddNewFile()，给指定目录下，新增一个文件：传递参数不合法，dirPath：${dirPath}，newAddFilePath：${newAddFilePath}`);
            return false;
        }
        let newAddFile = new java.io.File(newAddFilePath);
        if (!newAddFile.exists()) {
            print(`新增文件不存在：${newAddFilePath}`);
            return false;
        }
        let dir = new java.io.File(dirPath);
        if (!dir.exists()) {
            print(`指定目录不存在：${dirPath}`);
			let isCreate: boolean = file.mkdir();
            return false;
        }
        if (!copyFileName) {
            copyFileName = newAddFile.getName(); // eg：test.txt
        }
        if (`${newAddFile.getParent()}/` == dirPath) { // 若同处于一个目录下，则增加前缀：copy_
            copyFileName = `copy_${copyFileName}`;
        }
        let copyFile = new java.io.File(`${dirPath}${copyFileName}`);
        if (!copyFile.exists()) {
            copyFile.createNewFile();
        }
        let fileOut: FileOutputStream = null;
        let fileIn: FileInputStream = null;
        try {
            fileOut = new FileOutputStream(copyFile);
            fileIn = new FileInputStream(newAddFilePath);
            IOUtils.copy(fileIn, fileOut);
            print(`给指定目录：【${dirPath}】，新增一个文件：【${newAddFilePath}】-成功`);
            return true;
        } catch (e) {
            print("dirAddNewFile()，给指定目录下，新增一个文件失败");
            print(e);
        } finally {
            if (fileOut != null) {
                fileOut.close();
            }
            if (fileIn != null) {
                fileIn.close();
            }
        }
        return false;
    }

    /**
     * 给指定目录下，新增一个文件
     * @param dirPath 目录路径，eg：/tempSaveZipFile/
     * @param newAddFilePath 新增的文件路径，eg：/tempSaveZipFile/t.doc
     * @param copyFileName 复制后的文件名.类型，eg：copy_t.doc
     * @return true | false
     */
    public dirAddNewFile2(dirPath: string, newAddFile: java.io.File, copyFileName?: string): boolean {
        if (dirPath == "") {
            print(`dirAddNewFile2()，给指定目录下，新增一个文件：传递参数不合法，dirPath：${dirPath}`);
            return false;
        }
        if (!newAddFile.exists()) {
            print(`新增文件不存在`);
            return false;
        }
        let dir = new java.io.File(dirPath);
        if (!dir.exists()) {
            print(`指定目录不存在：${dirPath}`);
            return false;
        }
        if (!copyFileName) {
            copyFileName = newAddFile.getName(); // eg：test.txt
        }
        if (`${newAddFile.getParent()}/` == dirPath) { // 若同处于一个目录下，则增加前缀：copy_
            copyFileName = `copy_${copyFileName}`;
        }
        let isAddSuccess: boolean = true;
        let copyFile = new java.io.File(`${dirPath}${copyFileName}`);
        if (!copyFile.exists()) {
            copyFile.createNewFile();
        }
        let fileOut: FileOutputStream = null;
        let fileIn: FileInputStream = null;
        try {
            fileOut = new FileOutputStream(copyFile);
            fileIn = new FileInputStream(newAddFile);
            IOUtils.copy(fileIn, fileOut);
            print(`给指定目录：【${dirPath}】，新增一个文件成功`);
        } catch (e) {
            isAddSuccess = false;
            print("dirAddNewFile2()，给指定目录下，新增一个文件失败");
            print(e);
        } finally {
            fileOut && fileOut.close();
            fileIn && fileIn.close();
        }
        return isAddSuccess;
    }

    /**
     * 显示指定目录下的子文件和文件夹情况
     * @param dirPath 目录路径，eg：/tempSaveZipFile/
     */
    public showFolderOrFile(dirPath: string): void {
        if (!dirPath) {
            print(`showFolderOrFile()，传入的路径不合法：【${dirPath}】`);
            return;
        }
        let dir = new java.io.File(dirPath);
        if (!dir.isDirectory()) {
            print(`showFolderOrFile()，传入的路径不是一个目录`);
            return;
        }
        print("当前目录下的子文件和文件夹情况：");
        let childFolders: File_[] = dir.listFiles();
        if (childFolders.length > 0) {
            for (let i = 0; i < childFolders.length; i++) {
                print(childFolders[i].getAbsolutePath());
            }
        }
    }

    /**
     * 删除指定目录下，指定类型的文件
     * @param dirPath 目录路径，eg：/tempSaveZipFile/
     * @param fileType 文件类型，eg: doc
     */
    public deleteAppointFile(dirPath: string, fileType: string): void {
        // todo
    }

    /**
     * 读取url文件二进制流，创建指定类型文件
     * @param fileName 创建文件的绝对路径，eg：/tempSaveZipFile/t.doc
	 * @param fileIoByte 创建文件的内容流
     * @return true | false
     */
    public createFile(filePath: string, fileIoByte: number[]): boolean {
        if (!filePath) {
            return false;
        }
        let isSuccess: boolean = true;
        let file = new java.io.File(filePath);
        if (!file.exists()) {
            let parentFile = file.getParentFile();
            if (!parentFile.exists()) {
                parentFile.mkdirs();    // 若文件父目录不存在，则创建文件以及父文件目录
            }
            file.createNewFile(); // 创建真实的文件
        }
        file = getFile(filePath); // 读取文件
        let outStream = file.openOutputStream();
        try {
            outStream.write(fileIoByte as number[]);
        } catch (e) {
            file.delete();
            isSuccess = false;
            print(`--写入文件${filePath}内容失败---`);
            print(e);
        } finally {
            outStream.close();
        }
        return isSuccess;
    }

    /**
     * 读取pdfBase64 文件字节，并存储到指定PDF文件中
     * 参考：http://www.javashuo.com/article/p-oayfxpeu-du.html
     * 
     * @param pdfBase64 PDF图片base64编码
     * @param filePath pdf文件绝对路径，eg：/tempSaveZipFile/t.pdf
     * 
     * 注意：IDEA报常量字符串过长，解决办法；
     *   File -> Settings -> Build,Execution,Deployment -> Compiler -> Java Compiler ，  Use Compiler, 选择Eclipse - Apply
     */
    public pdfBase64ToPdf(pdfBase64: string, filePath: string): boolean {
        if (!pdfBase64 || !filePath) {
            console.debug(`pdfBase64ToPdf()，解析PDF base64编码参数错误`);
            return false;
        }
        pdfBase64 = pdfBase64.replace(' ', '+');
        let file = new File_(filePath);
        if (file.isDirectory()) {
            console.debug(`pdfBase64ToPdf()，解析PDF base64编码并存入指定文件失败，${filePath}是一个目录，需要指定存入PDF文件绝对路径`);
            return false;
        }
        let fileBytes: number[] = java.util.Base64.getDecoder().decode(pdfBase64);
        let fileType: string = this.checkBase64BytesFileType(fileBytes);
        if (fileType != "pdf") {
            console.debug(`pdfBase64ToPdf()，解析PDF  base64编码 文件类型为：${fileType}，不是PDF文件`);
            return false;
        }
        console.debug(`pdfBase64ToPdf()，解析成功，解析的字节大小为：${fileBytes.length}`);
        return this.createFile(filePath, fileBytes);
    }

    /**
     * 根据Base64来校验文件类型
     * 思路：
     * （1）根据前两个字节（文件头）来校验文件格式
     * @param base64ImgData base64字符串编码
     * return 
     */
    public checkImageBase64FileType(base64ImgData: string): string {
        if (!base64ImgData) {
            return "";
        }
        let fileBytes: number[] = java.util.Base64.getDecoder().decode(base64ImgData);
        let fileType: string = "";
		// ((fileBytes[0] & 0xff) << 8 | (fileBytes[1] & 0xff))结果是：十进制，转换为：十六进制
        if (0x424D == ((fileBytes[0] & 0xff) << 8 | (fileBytes[1] & 0xff))) {
            fileType = "bmp";
        }
        if (0x8950 == ((fileBytes[0] & 0xff) << 8 | (fileBytes[1] & 0xff))) {
            fileType = "png";
        }
        if (0xFFD8 == ((fileBytes[0] & 0xff) << 8 | (fileBytes[1] & 0xff))) {
            fileType = "jpg";
        }
        if (0x2550 == ((fileBytes[0] & 0xff) << 8 | (fileBytes[1] & 0xff))) {
            fileType = "pdf";
        }
        console.debug(`checkBase64BytesFileType()，base64编码对应的文件类型为：${fileType}`);
        return fileType;
    }

    /**
     * 根据Base64来校验文件类型
     * 思路：
     * （1）根据前两个字节（文件头）来校验文件格式
     * @param fileBytes base64编码字节数组
     * return 
     */
    public checkBase64BytesFileType(fileBytes: number[]): string {
        if (!fileBytes || fileBytes.length == 0) {
            return "";
        }
        let fileType: string = "";
        if (0x424D == ((fileBytes[0] & 0xff) << 8 | (fileBytes[1] & 0xff))) {
            fileType = "bmp";
        }
        if (0x8950 == ((fileBytes[0] & 0xff) << 8 | (fileBytes[1] & 0xff))) {
            fileType = "png";
        }
        if (0xFFD8 == ((fileBytes[0] & 0xff) << 8 | (fileBytes[1] & 0xff))) {
            fileType = "jpg";
        }
        if (0x2550 == ((fileBytes[0] & 0xff) << 8 | (fileBytes[1] & 0xff))) {
            fileType = "pdf";
        }
        console.debug(`checkBase64BytesFileType()，base64编码对应的文件类型为：${fileType}`);
        return fileType;
    }
	
    /**
     * 获取url中指定类型的文件流
     * @param url 超链接
     * @param fileType 文件类型，eg：image、pdf
     * @return 二进制流
     */
    public getFileIo(url: string, fileType: string): ArrayLike<number> {
        const client = HttpClients.createDefault();
        try {
            const httpGet = new HttpGet(url);
            const response = client.execute(httpGet);
            try {
                const entity = response.getEntity();
                const contentType = response.getFirstHeader("Content-type").getValue();
                print(contentType);
                if (contentType.indexOf(`${fileType}`) !== -1) {
                    // 避免文件夹含有大量的中文名，直接获取是一串乱码，需要将其转换为utf-8
                    // let fileName = response.getFirstHeader("Content-Disposition").getValue().split(';')[1].split('=')[1];
                    // let encodeFileName = new java.lang.String(fileName.getBytes("ISO-8859-1"), "utf-8");
                    return EntityUtils.toByteArray(entity); // 去除字符串左右‘双引号’
                }
                print("-----文件类型错误-----");
                const content = EntityUtils.toString(entity);
                if (content.indexOf("认证失败") !== -1) {
                    print(content);
                    return [];
                }
                return [];
            } finally {
                response.close();
            }
        } finally {
            client.close();
        }
    }
}
const fileUtils = new FileUtils();