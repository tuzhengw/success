
import db from "svr-api/db";

/**
 * 20220411 tuzw
 * 功能：获取vertica下表的字段描述
 * 问题：若是vertica下的表，无法通过getColumns()方法获取到表字段描述
 * 帖子：https://jira.succez.com/browse/CSTM-17942
 * 表字段描述解决办法：直接SQL查询对应表的字段描述信息
 * 帖子：https://jira.succez.com/browse/BI-31641
 * @param dataBase 数据库名
 * @param tableName 表名
 * @param schema 
 * @return []，eg： {
 * 		"NAME": "姓名",
		"AGE": "年龄",
 * }
 */
function getVerticaTableFiledDesc(dataBase: string, tableName: string, schema?: string) {
	console.debug(`getVerticaTableFiledDesc()，获取vertica【${dataBase}】表【${tableName}】的字段描述`);
	if (!dataBase || !tableName) {
		return {};
	}
	let ds: Datasource = db.getDataSource(dataBase);
	if (!schema) {
		schema = ds.getDefaultSchema();
	}
	let dbJdbc: DbConnectionInfo = ds.getJdbcConf();
	let dbType: string = dbJdbc.dbType as string;
	if (!dbType || dbType != 'com.vertica.jdbc.Driver') { // 校验当前数据库是否为vertica
		console.debug(`当前数据库【${dataBase}】不是vertica类型，结束执行`);
		return {};
	}
	let querySQL: string = `
		select
			cols.table_column_name as COLUMN_NAME,
			coms.comment as COMMENTS
		from
			v_catalog.comments coms
		inner join 
			v_catalog.projection_columns cols 
		on
			coms.object_id = cols.column_id
			and coms.object_type = 'COLUMN'
			and cols.table_schema = ?
			and cols.table_name = ?
	`;
	let queryDescResult: VERTICA_FILED_DESC[] = ds.executeQuery(querySQL, schema, tableName) as VERTICA_FILED_DESC[];
	if (!queryDescResult || queryDescResult.length == 0) {
		return {};
	}
	let vtcTableFieldDesc = {};
	for (let i = 0; i < queryDescResult.length; i++) {
		let filedName: string = queryDescResult[i].COLUMN_NAME;
		let filedDesc: string = queryDescResult[i].COMMENTS;
		vtcTableFieldDesc[filedName] = filedDesc;
	}
	console.debug(`vertica下【${dataBase}】中表【${tableName}】的字段描述：\n ${JSON.stringify(vtcTableFieldDesc)}`);
	return vtcTableFieldDesc;
}