import { Button, Combobox, Edit, Link, Checkbox } from "commons/basic";
import { Dialog } from "commons/dialog";
import { getCurrentUser, getMetaRepository, IMetaFileViewer, getFileImplClass } from "metadata/metadata";
import { showResourceDialog } from "metadata/metamgr";
import { getScheduleMgr, ScheduleEditor } from "schedule/schedulemgr";
import {
	assign, Component, ComponentArgs, ctx, ctxIf, getBaseName, IImportComponentInfo, showWaiting, rc1,
	showWarningMessage, showWaitingMessage, isEmpty, message, rc, showDialog, showErrorMessage, SZEvent, showSuccessMessage
} from "sys/sys";
import { IFieldsDataProvider } from "commons/exp/expdialog";
import { IExpDataProvider, ExpVar } from "commons/exp/expcompiler";
import { InterActionEvent } from "metadata/metadata-script-api";
import { TableCellEditor } from "commons/table";
import { getDwTableDataManager } from "dw/dwapi";

/** 当前选中的计划任务名 */
let currentChoseScheduleName: string = "";
/** 记录当前计划列表--所有的计划名 */
let scheduleNames: Array<string> = [];
/**
 * 20211115 liuyz
 * https://jira.succez.com/browse/CSTM-16904
 * 计划任务的设置界面支持一个通用的方法
 * @param scheduleContent 当前选择的计划对象
 *
 * 20220214 tuzw
 * （1）计划任务存在新增/修改操作，增加isEidt来判断当前操作
 * （2）考虑到编辑可能仅编辑部分栏，增加isDisplayDetail属性，来校验是否需要隐藏详细栏
 * @param shceduleOptionParam 计划任务页面操作参数
 */
export function showScheduleDialog(scheduleContent: ScheduleInfo, shceduleOptionParam?: ScheduleOptionParam): Promise<any> {
	if (!shceduleOptionParam) { // 若未指定页面操作参数，则设置默认值
		shceduleOptionParam = {
			isEdit: false
		};
	}
	return querySchedules().then((allScheduleWorkResult: ScheduleInfo[]) => {
		allScheduleWorkResult.forEach(scheduleItem => {
			if (scheduleContent.id != scheduleItem.id) { 	// 存储所有所选计划以外的所有计划名
				scheduleNames.push(scheduleItem["name"]);
			}
		});
		let valid = (schedulename: string) => { 	// 计划任务是否有效，重写 (value: string) => { }
			/** 当前编辑后的新计划任务名 */
			let currentEditSchedulename = schedulename.trim();
			if (!!shceduleOptionParam.isEdit) {
				if (currentEditSchedulename == currentChoseScheduleName) {
					return true;
				}
			}
			if (scheduleNames.includes(currentEditSchedulename)) {
				return {
					result: false,
					errorMessage: message("schedulemgr.schedule.name.alreadyExistMsg")
				}
			};
		}
		let nameValidateArgs = {
			required: true,
			validRegex: valid
		}
		showDialog({
			id: !!shceduleOptionParam.isEdit
				? "schedulemgr-dilaog-editchedule"
				: "schedulemgr-dilaog-newchedule",
			buttons: ["ok", "cancel"],
			needWaitInit: true,
			resizable: false,
			caption: !!shceduleOptionParam.isEdit
				? "编辑计划"
				: "添加计划",
			content: {
				implClass: ScheduleEditor,
				nameValidateArgs: nameValidateArgs
			},
			onshow: (e: SZEvent, dialog: Dialog) => {
				scheduleDialogOnShow(scheduleContent, dialog, shceduleOptionParam);
			},
			onok: (e: SZEvent, dialog: Dialog) => {
				scheduleDialogOnOk(scheduleContent, dialog, shceduleOptionParam);
			}
		});
	});
}

/**
 * 20220429 tuzw
 * 功能：查询计划信息
 * 问题：由于产品升级到4.20，原本metadata模块中的querySchedules方法没有被export了，故将其单独抽出来
 * @param fromTime 返回某个时间点后有状态更新的计划列表
 * @param projectName 项目名
 */
function querySchedules(fromTime?: number, projectName?: string): Promise<ScheduleInfo[]> {
	return rc1({
		url: "/api/schedule/services/queryScheduleInfos",
		data: {
			projectName,
			fromTime
		}
	}, null, `/api/schedule/services/queryScheduleInfos?fromTime=${fromTime}&projectName=${projectName}`);
}

/**
 * 计划任务对话框onok方法
 * @param scheduleContent 计划的详细信息
 * @param dialog 计划任务对话框对象
 * @param shceduleOptionParam 计划任务页面操作参数
 * @return true | false
 */
function scheduleDialogOnOk(scheduleContent: ScheduleInfo, dialog: Dialog, shceduleOptionParam: ScheduleOptionParam): boolean {
	let editor = <ScheduleEditor>dialog.content;
	if (!editor.validate()) {
		return false;
	};
	if (!!shceduleOptionParam.isEdit) {
		dialog.waitingClose(getScheduleMgr().modifySchedule(scheduleContent.id, (<ScheduleEditor>dialog.content).getData())).then(() => {
			showSuccessMessage("修改成功");
		});
	} else {
		dialog.waitingClose(getScheduleMgr().addSchedule((<ScheduleEditor>dialog.content).getData())).then((scheduleInfo: ScheduleInfo) => {
			showSuccessMessage("添加成功");
			if (!!shceduleOptionParam.refreshModelPath) {
				let updateCachePath: DwDataChangeEvent[] = [{
					path: shceduleOptionParam.refreshModelPath,
					type: DataChangeType.refreshall
				}]
				getDwTableDataManager().updateCache(updateCachePath);
			}
			if (!!shceduleOptionParam.afterClickMethod) {
				shceduleOptionParam.afterClickMethod(scheduleInfo);
			}
		});
	}
	currentChoseScheduleName = "";
	return true;
}

/** 
 * 计划任务对话框onshow方法
 * @param scheduleContent 计划的详细信息
 * @param dialog 计划任务对话框对象
 * @param shceduleOptionParam 计划任务页面操作参数
 */
function scheduleDialogOnShow(scheduleContent: ScheduleInfo, dialog: Dialog, shceduleOptionParam: ScheduleOptionParam): void {
	let editor = <ScheduleEditor>dialog.content;
	if (!shceduleOptionParam.isDisplayScheduleType) {
		editor.form.getFormItem("type").setVisible(false);
	}
	if (!!shceduleOptionParam.isEdit && !!shceduleOptionParam.isDisplayDetail) {
		/**
		 * 编辑任务时，隐藏部分属性设置
		 * 帖子：https://jira.succez.com/browse/CSTM-17083
		 */
		editor.form.getFormItem("enable").setVisible(false);
		editor.form.getFormItem("name").setVisible(false);
		editor.form.getFormItem("priority").setVisible(false);
		editor.form.getFormItem("concurrency").setVisible(false);
		editor.form.getFormItem("threadsCount").setVisible(false);
		editor.form.getFormItem("threadsCount").setVisible(false);
		editor.form.getFormItem("schedule").setVisible(false);
		editor.form.getFormItem("enableClusterNodes").setVisible(false);
		currentChoseScheduleName = scheduleContent.name as string;
	}
	editor.setData(scheduleContent);
}

/** 计划任务页面操作参数 */
interface ScheduleOptionParam {
	/** 是否编辑计划任务 */
	isEdit?: boolean;
	/** 编辑时，是否需要显示详细栏 */
	isDisplayDetail?: boolean;
	/** 页面是否显示选择计划类型，默认：不显示 */
	isDisplayScheduleType?: boolean;
	/** 刷新计划任务表模型路径 */
	refreshModelPath?: string;
	/** 点击事件后执行方法 */
	afterClickMethod?: (scheduleInfo: ScheduleInfo) => void;
}

/**
 * 20211203 tuzw
 * 获取当前计划列表——所有计划任务名
 * @return 所有计划名数组
 */
function getAllScheduleWorkName(): Promise<string[]> {
	// 注意返回的是：异步
	return querySchedules().then((allScheduleWorkResult: ScheduleInfo[]) => {
		let scheduleNames: Array<string> = [];
		allScheduleWorkResult.forEach(scheduleItem => {
			// 存储所有已存在计划名记录
			scheduleNames.push(scheduleItem["name"]);
		});
		return scheduleNames;
	});
}