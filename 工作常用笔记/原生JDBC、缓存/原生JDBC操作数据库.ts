import bean from "svr-api/bean";
import metadata from "svr-api/metadata";
import db from "svr-api/db";
import fs from "svr-api/fs";
import sys from "svr-api/sys";
import dw from "svr-api/dw";

import StringUtils = org.apache.commons.lang3.StringUtils;
import Class = java.lang.Class;
import Statement = java.sql.Statement;
import ResultSet = java.sql.ResultSet;
import { LogType, PackageLog, ResultInfo, getPackageSetInfo } from "/sysdata/app/packageApp.app/commons/packageCom.action";

/**
 * 批处理脚本模版
 */
function main() {
    let greenPackageNoDeFaultDir = sys.getWorkDir().replace("workdir", "server/resources");
    let dbMgr = new RestoreNoDefaultTableDatas({
        noDefaultDbDir: greenPackageNoDeFaultDir,
        noDefaultDbInfo: getPackageSetInfo().noDefaultDbInfos,
        logger: new PackageLog({ name: "sys.package" })
    });
    print(dbMgr.executeNoDefaultDbDatas());
}

/**
 * 恢复非默认数据库的表数据
 * <p>
 *  20230506 tuzw
 *  产品内部初始化仅处理默认数据库, 非默认数据库没有处理, 只能自己原生写JDBC来创建数据库, 
 * 但每个数据库连接和SQL用法都不一致, 暂且仅支持MySQL数据库。
 *  注意:
 *   1 非默认数据库表数据都存在磁盘指定的resources目录下, 路径格式: ../resources/${数据源连接名}/${数据库名(schema名)}/${表名}.csv。
 *   2 暂时仅处理绿色包非默认数据库数据, 绿色包非默认数据源数据存在在非系统元数据项目目录(resources)下。
 * </p>
 * <p>
 *  示例: 
 *  let greenPackageNoDeFaultDir = sys.getWorkDir().replace("workdir", "server/resources");
    let dbMgr = new RestoreNoDefaultTableDatas({
        noDefaultDbDir: greenPackageNoDeFaultDir,
        noDefaultDbInfo: {
            "bds3": ["bds3"]
        },
        logger: new PackageLog({ name: "sys.package" })
    });
    print(dbMgr.executeNoDefaultDbDatas());
 * </p>
 */
export class RestoreNoDefaultTableDatas {

    /** 非系统数据源存储父目录, eg: .../resources */
    private noDefaultDbDir: string;
    /** 
     * 处理的非默认数据源, eg: {
     *  "数据源连接名": ["数据库名1(schema名)", "数据库名2(schema名)"]
     * }
     */
    private noDefaultDbInfo: { [dbConnName: string]: string[] };
    /** 日志对象 */
    private logger: PackageLog;

    /** 自定义数据源管理实例 */
    private customDataSourceMgr: CustomDataSourceMgr;
    /** 元数据仓库对象 */
    private metaServiceRepository;
    /** 模型修复服务实例(用于修复模型文件) */
    private modelSaver;

    constructor(args: {
        noDefaultDbDir: string,
        noDefaultDbInfo: { [dbConnName: string]: string[] },
        logger: PackageLog
    }) {
        this.noDefaultDbDir = args.noDefaultDbDir;
        this.noDefaultDbInfo = args.noDefaultDbInfo ? args.noDefaultDbInfo : {};
        this.logger = args.logger;
        this.logger.syncEnvLogLevel();
        this.customDataSourceMgr = new CustomDataSourceMgr({
            logger: this.logger
        });
        this.metaServiceRepository = bean.getBean("com.succez.metadata.service.MetaServiceRepository");
        let dwServiceMeta = bean.getBean("com.succez.dw.services.impl.DwServiceMeta");
        this.modelSaver = dwServiceMeta.getDwServiceModelSaver();
    }

    /**
     * 处理非默认数据源物理表数据
     */
    public executeNoDefaultDbDatas(): ResultInfo {
        this.logger.addLog(`开始执行[executeNoDefaultDbDatas], 处理非默认数据源`);
        let noDefaultDbDir = this.noDefaultDbDir;
        let dbDirFile = fs.getFile(noDefaultDbDir);
        if (!dbDirFile.exists()) {
            return { result: false, message: `目录[${noDefaultDbDir}]不存在` };
        }
        let noDefaultDbInfo = this.noDefaultDbInfo;
        let dbConnNames: string[] = Object.keys(noDefaultDbInfo);
        if (dbConnNames.length == 0) {
            return { result: false, message: `没有要处理的非默认数据源` };
        }
        let resultInfo: ResultInfo = { result: true };
        for (let dbIndex = 0; dbIndex < dbConnNames.length && resultInfo.result; dbIndex++) {
            let dbConnName: string = dbConnNames[dbIndex]; // eg: bds3
            this.logger.addLog(`开始处理非默认数据源: ${dbConnName}`);
            let schemaNames: string[] = noDefaultDbInfo[dbConnName];
            if (schemaNames.length == 0) {
                this.logger.addLog(`数据源[${dbConnName}]没有要处理的数据库`);
                continue;
            }
            let dbSource = db.getDataSource(dbConnName);
            if (dbSource == null) {
                resultInfo = this.customDataSourceMgr.connectDataBase({
                    dbName: schemaNames[0], // 若数据源不存在, 则增加数据源, 并将首个数据库作为连接数据库名
                    connName: dbConnName
                });
                if (!resultInfo.result) {
                    break;
                }
                dbSource = db.getDataSource(dbConnName);
            }
            this.repairEnvModelTable(dbConnName);
            resultInfo = this.csvFielToTableDatas(dbSource, dbConnName, schemaNames);
        }
        this.logger.addLog(resultInfo);
        this.logger.addLog(`执行结束[executeNoDefaultDbDatas], 处理非默认数据源`);
        return resultInfo;
    }

    /**
     * 修复非默认数据源模型对应的物理表
     * @param dbConnName 数据源连接名
     * @return 
     */
    private repairEnvModelTable(dbConnName: string): void {
        this.logger.addLog(`开始修复数据源[${dbConnName}]所涉及的模型对应的物理表`);
        let envAllProjectInfos = this.metaServiceRepository.getAllProjects();
        for (let projectIndex = 0; projectIndex < envAllProjectInfos.length; projectIndex++) {
            let projectName: string = envAllProjectInfos.get(projectIndex).name;

            /** 项目默认数据源名 */
            let projectDefaultDbName: string = JSON.parse(metadata.getString("/core/settings/settings.json"))["data"]["analysisDatasource"];
            let dataTablePaths = `/${projectName}/data/tables`;
            let modelFiles = metadata.searchFiles({
                parentDir: dataTablePaths,
                type: "tbl"
            });
            for (let fileIndex = 0; fileIndex < modelFiles.length; fileIndex++) {
                let modelFile = modelFiles[fileIndex];
                let modelPath: string = modelFile.path;
                let modelInfo: DwTableInfo = JSON.parse(metadata.getString(modelPath));
                let datasource = modelInfo.properties.datasource;
                if (!datasource) {
                    continue;
                }
                if (datasource == dbConnName || ((datasource == "default" || datasource == "defdw") && dbConnName == projectDefaultDbName)) {
                    try {
                        this.modelSaver.checkAndRepair(modelPath, {}, null);
                        this.logger.addLog(`模型[${modelPath}]修复成功`);
                    } catch (e) {
                        this.logger.addLog(`修改模型[${modelPath}]失败[${e.toString()}]`);
                    }
                }
            }
        }
    }

    /**
     * 将数据源目录下所有数据库对应的csv文件转换为对应的物理表数据
     * @param dbSource 数据源实例
     * @param dbConnName 数据源连接名
     * @param schemaNames 处理的数据库名
     * @return 
     */
    private csvFielToTableDatas(dbSource: Datasource, dbConnName: string, schemaNames: string[]): ResultInfo {
        let resultInfo: ResultInfo = { result: true };
        /** 已存在的数据库名 */
        let existedSchemaNames: string[] = this.customDataSourceMgr.getExistedSchemaNames(dbSource);
        for (let schemaIndex = 0; schemaIndex < schemaNames.length && resultInfo.result; schemaIndex++) {
            let schemaName: string = schemaNames[schemaIndex];
            this.logger.addLog(`开始处理数据库: ${schemaName}`);
            try {
                // @ts-ignore
                if (!existedSchemaNames.includes(schemaName)) {
                    resultInfo = this.customDataSourceMgr.createDataBase(schemaName);
                    existedSchemaNames.push(schemaName);
                }
                if (!resultInfo.result) {
                    break;
                }
                let schemaCsvDir: string = `${this.noDefaultDbDir}/${dbConnName}/${schemaName}`;
                let csvDirFile = fs.getFile(schemaCsvDir);
                if (!csvDirFile.exists()) {
                    this.logger.addLog(`存储csv目录[${schemaCsvDir}]不存在, 越过`);
                    continue;
                }
                let listFiles = csvDirFile.listFiles();
                this.logger.addLog(`数据库[${schemaName}]需要恢复[${listFiles.length}]张表数据`);
                for (let fileIndex = 0; fileIndex < listFiles.length; fileIndex++) {
                    let file = listFiles[fileIndex];
                    let filePath: string = file.getPath();
                    if (!StringUtils.endsWith(filePath, ".csv")) {
                        continue;
                    }
                    let dataImportMode: DataImportMode = DataImportMode.NEWTABLE;
                    let fileName: string = file.getName().replace(".csv", "");
                    let importTableName: string = `${schemaName}.${fileName}`;
                    if (dbSource.isTableExists(importTableName)) {
                        dataImportMode = DataImportMode.MERGE;
                    }
                    let csvFile = new java.io.File(filePath);
                    try {
                        dbSource.importData({
                            tableName: importTableName, // eg: ${schema}.${tableName}
                            importMode: dataImportMode as DataImportMode,
                            file: csvFile,
                            fileType: FileType.CSV
                        });
                        this.logger.addLog(`物理表[${importTableName}]数据导入成功`);
                    } catch (e) {
                        this.logger.addLog(`物理表[${importTableName}]数据导入失败[${e.toString()}]`);
                    }
                }
            } catch (e) {
                resultInfo.result = false;
                resultInfo.message = `恢复非默认数据源下数据库[${schemaName}]失败[${e.toString()}]`;
                this.logger.addLog(`执行[dealSingleDbSource]恢复非默认数据源下数据库失败`);
                this.logger.addLog(e);
            }
        }
        return resultInfo;
    }
}

/**
 * 自定义数据库管理
 * <p>
 *  功能: 在指定数据源下创建模式(数据库)、获取数据库连接。
 *  注意: 
 *   1 MySQL可通过URL来指定使用数据库(schema)。
 *   2 Oracle通过ALTER SESSION SET CURRENT_SCHEMA = schema_name 来指定。
 *   3 可通过SQL删除和展示数据库: drop database bds3; show databases;
 * </p>
 */
export class CustomDataSourceMgr {

    /** 数据源类型 */
    private dbType: DbType;
    /** 数据库连接IP, eg: 127.0.0.1 */
    private host: string;
    /** 数据库端口号, eg: 3310 */
    private port: number;
    /** 用户名, eg: root */
    private userName: string;
    /** 用户密码, eg: 123456 */
    private password: string;

    /** 判断数据库是否存在SQL */
    private dbSourceIsExistSQL = "SELECT COUNT(*) FROM information_schema.schemata  WHERE schema_name = ?";
    /** 日志对象 */
    private logger: PackageLog;

    constructor(args: {
        logger: PackageLog,
        dbType?: DbType,
        host?: string,
        port?: number,
        userName?: string,
        password?: string
    }) {
        this.logger = args.logger;
        this.dbType = args.dbType != undefined ? args.dbType : DbType.MySQL;
        this.host = args.host != undefined ? args.host : "127.0.0.1";
        this.port = args.port != undefined ? args.port : 3310;
        this.userName = args.userName != undefined ? args.userName : "root";
        this.password = args.password != undefined ? args.password : "rootisadmin666";
    }

    /**
     * 环境连接数据库连接下的某个数据库, 若数据库不存在则会先创建
     * @param dbName 数据库名
     * @param connName 连接名称
     * @return {
     *   result: true
     * }
     */
    public connectDataBase(args: {
        dbName: string,
        connName?: string
    }): ResultInfo {
        let dbName: string = args.dbName;
        let resultInfo: ResultInfo = this.createDataBase(dbName);
        if (!resultInfo.result) {
            return resultInfo;
        }
        let dbPath = `/sysdata/data/sources/${dbName}.jdbc`;
        let dbFile = metadata.getFile(dbPath);
        if (dbFile != null) {
            this.logger.addLog(`数据库[${dbName}]已连接过, 不需要再次连接`);
            return { result: true };
        }
        let connName = args.connName;
        if (connName == undefined) {
            connName = dbName;
        }
        try {
            let dataSourceMgr = bean.getBean("com.succez.dw.services.impl.DwServiceDataSourcesServiceImpl");
            let status: string = dataSourceMgr.addConnection(this.getDbConnInfo(connName, dbName), "sys");
            this.logger.addLog(`数据库[${status}]连接成功`);
        } catch (e) {
            resultInfo.result = false;
            resultInfo.message = `连接数据库[${dbName}]失败[${e.toString()}]`;
            console.error(`执行[connectDataBase], 连接数据库[${dbName}]失败[${e.toString()}]`);
            console.error(e);
        }
        return resultInfo;
    }

    /**
     * 获取数据库连接信息
     * @param dbName 数据库名
     * @param connName 连接名称
     * @return 
     */
    private getDbConnInfo(connName: string, dbName: string): DbConnectionInfo {
        let dbInfo: DbConnectionInfo = {
            "name": connName,
            "dbType": this.dbType,
            "host": this.host,
            "port": this.port,
            "databaseName": dbName,
            "user": this.userName,
            "password": this.password,
            "driver": this.getDriverName(),
            "minPoolSize": 1,
            "purpose": "writable",
            "desc": "",
            "testConnectionOnCheckout": false
        };
        let dbUrl: string = this.getDbUrl();
        switch (this.dbType) {
            case DbType.MySQL:
                dbInfo.url = `${dbUrl}${dbName}?useUnicode=true&characterEncoding=utf8&allowLoadLocalInfile=true&zeroDateTimeBehavior=convertToNull&useSSL=false`;
                break;
        }
        this.logger.addLog(`数据库连接信息: `);
        this.logger.addLog(dbInfo);
        return dbInfo;
    }

    /**
     * 指定数据库连接下创建一个数据库
     * <p>
     *  注意: 
     *   1 数据库连接仅一个, 内部有多个数据库, 也称为模式(schema); 
     *   2 产品数据源页面连接的是数据库连接下的某个数据库(schema表示连接所使用的数据库名);
     *   3 产品打开的数据源必须是数据库下存在的schema。
     *   示例: 
     *   1 本地数据库连接, 数据库(schema)含: succbi、succbidev、succbiyw 等;
     * </p>
     * @param dbName 数据源名
     */
    public createDataBase(dbName: string): ResultInfo {
        let driverName: string = this.getDriverName();
        if (driverName == "") {
            return { result: false, message: `未获取到数据库驱动名` };
        }
        let resultInfo: ResultInfo = { result: true };
        let createDbSQL: string = this.getCreateDbSQL(dbName);
        // @ts-ignore
        let mysqlConn: Connection = null;
        let statement: Statement = null;
        let queryResult: ResultSet = null;
        try {
            mysqlConn = this.getDbConnection() as Connection;
            if (mysqlConn == null) {
                resultInfo = { result: false, message: `数据库连接失败` };
            }
            if (resultInfo.result) {
                this.logger.addLog(`[${this.dbType}]数据库连接成功`);
                statement = mysqlConn.prepareStatement(this.dbSourceIsExistSQL);
                statement.setString(1, dbName);
                queryResult = statement.executeQuery();
                if (queryResult.next()) {
                    if (queryResult.getInt(1) == 0) { // 判断数据库是否创建
                        // @ts-ignore
                        statement = mysqlConn.createStatement();
                        statement.executeUpdate(createDbSQL);
                        this.logger.addLog(`数据库[${dbName}]创建成功`);
                    } else {
                        this.logger.addLog(`数据库[${dbName}]已存在不需要额外创建`);
                    }
                }
            }
        } catch (e) {
            resultInfo.result = false;
            resultInfo.message = `创建数据库失败[${e.toString()}]`;
            console.error(`执行[createDataBase], 创建一个数库失败[${e.toString()}]`);
            console.error(e);
        } finally {
            mysqlConn && mysqlConn.close();
            statement && statement.close();
            queryResult && queryResult.close();
        }
        return resultInfo;
    }

    /**
     * 获取数据源下已存在的数据库名
     * @param dbSource 数据源实例对象
     * @return eg: ["mysql", "information_schema", "performance_schema"]
     */
    public getExistedSchemaNames(dbSource: Datasource): string[] {
        if (dbSource == null) {
            return [];
        }
        let existedDbNames: string[] = [];
        let querySQL: string = "SELECT SCHEMA_NAME FROM information_schema.schemata";
        try {
            let queryDatas = dbSource.executeQuery(querySQL);
            for (let i = 0; i < queryDatas.length; i++) {
                existedDbNames.push(queryDatas[i]["SCHEMA_NAME"]);
            }
        } catch (e) {
            console.error(`执行[getExistedSchemaNames], 获取数据源下已存在的数据库名失败[${e.toString()}]`);
            console.error(e);
        }
        return existedDbNames;
    }

    /**
     * 连接数据库
     * <p>
     *  注意: 
     *   1 Class.forName加载驱动是按当前环境所配置驱动顺序加载的, 若环境存在多个数据库驱动, 加载的驱动可能不是指定的。
     *   2 示例: 本地环境会优先加载com.oceanbase.jdbc.Driver。
     *     解决办法: 
     *     1) 去除驱动: DriverManager.deregisterDriver(new com.oceanbase.jdbc.Driver());
     *     2) 经过测试, 去除驱动不生效, 没有将驱动删除成功, 改为反射加载驱动。
     *   3 获取的conn连接对象需要关闭。
     * </p>
     * @return 数据库连接对象
     */
    private getDbConnection(): Connection | null {
        let dbUrl = this.getDbUrl();
        if (!dbUrl) {
            return null;
        }
        let driverName: string = this.getDriverName();
        let driverClass = Class.forName(driverName);
        this.logger.addLog(`数据库驱动[${driverName}]加载成功`);

        let constru = driverClass.getConstructor();
        let driver = constru.newInstance();
        let method = driverClass.getMethod("connect", java.lang.String.class, java.util.Properties.class);
        let property = new java.util.Properties();
        property.put("user", this.userName);
        property.put("password", this.password);
        let conn: Connection = method.invoke(driver, dbUrl, property);
        return conn;
    }

    /**
     * 获取创建数据库SQL
     * @param dbName 数据源名
     * @return 
     */
    private getCreateDbSQL(dbName: string): string {
        let createDbSQL: string = "";
        switch (this.dbType) {
            case DbType.MySQL:
                createDbSQL = `create database ${dbName}`;
                break;
        }
        this.logger.addLog(`创建数据库[${dbName}]SQL: ${createDbSQL}`, LogType.DEBUGGER);
        return createDbSQL;
    }

    /**
     * 获取数据库驱动名
     */
    private getDriverName(): string {
        let driver: string = "";
        switch (this.dbType) {
            case DbType.MySQL:
                driver = "com.mysql.cj.jdbc.Driver"; // MySQL仅支持8.0以上
                break;
        }
        this.logger.addLog(`数据库驱动名: ${driver}`, LogType.DEBUGGER);
        return driver;
    }

    /**
     * 获取数据库连接地址
     * @return eg: jdbc:mysql://127.0.0.1:3310/
     */
    private getDbUrl(): string {
        let url: string = "";
        let host = this.host;
        let port = this.port;
        switch (this.dbType) {
            case DbType.MySQL:
                url = `jdbc:mysql://${host}:${port}/`;
                break;
        }
        this.logger.addLog(`当前数据库连接地址: ${url}`, LogType.DEBUGGER);
        return url;
    }
}

/** 数据库类型 */
export enum DbType {
    MySQL = "MySQL"
}