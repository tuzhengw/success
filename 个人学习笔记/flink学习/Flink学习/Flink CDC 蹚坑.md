---
typora-root-url: images
---

# flink cdc 集成蹚坑合集

<!--修改后需要重启数据库、重新连接数据库-->

## 1 数据包太大

​		**异常：Packet for query is too large (4,739,923 > 65,535). You can change this value on the server by setting the 'max_allowed_packet' variable.**

​	注解: 用于查询的数据包太大（4739923>65535）。您可以通过设置“max_allowed_packet”变量在服务器上更改此值。

```mysql
# 地址：https://blog.csdn.net/wxyf2018/article/details/106115678
show VARIABLES like '%max_allowed_packet%';
```

## 2 MySQL版本太低

​		**异常： The server time zone value 'PDT' is unrecognized or represents more than one time zone. You must configure either the server or JDBC driver (via the 'serverTimezone' configuration property) to use a more specifc time zone value if you want to utilize time zone support**

参考: https://blog.csdn.net/dreamboy_w/article/details/96505068

使用了Mysql最新版驱动所以报错

新版驱动名字为 driverClass=“com.mysql.cj.jdbc.Driver”

**解决方案：**

方案1、在项目代码-数据库连接URL后，加上 ?serverTimezone=UTC（注意大小写必须一致）

```java
DebeziumSourceFunction<String> soureFunction = MySqlSource.<String>builder()
                .hostname("192.168.144.129")
                .port(3306)
                .serverTimeZone("UTC")
                .username("root")
                .password("123456")
                .databaseList("test")
                .tableList("student1")
                .deserializer(new StringDebeziumDeserializationSchema())
                .startupOptions(StartupOptions.initial())
                .build();
```

方案2、在mysql中设置时区，默认为SYSTEM（推荐）

```mysql
mysql>set global time_zone='+8:00';
```

## 3 binlog日志未开启

​		**异常： Cannot read the binlog filename and position via 'SHOW MASTER STATUS'. Make sure your server is correctly configured**

​	注解：无法通过“SHOW MASTER STATUS”读取binlog文件名和位置。确保服务器配置正确。

​	参考：https://blog.csdn.net/qq_42657496/article/details/122393821

​	注意：需要将配置写到[mysqld]下，否则不生效

```mysql
-- SHOW MASTER STATUS
show variables like '%log_bin%';
```

## 4 类加载机制顺序冲突

​	地址：https://www.cnblogs.com/quchunhui/p/12506037.html

```java
Caused by: java.lang.ClassCastException: cannot assign instance of java.util.ArrayList to field org.apache.flink.runtime.jobgraph.JobVertex.results of type java.util.Map in instance of org.apache.flink.runtime.jobgraph.JobVertex
	at java.base/java.io.ObjectStreamClass$FieldReflector.setObjFieldValues(ObjectStreamClass.java:2190)
	at java.base/java.io.ObjectStreamClass$FieldReflector.checkObjectFieldValueTypes(ObjectStreamClass.java:2153)
	at java.base/java.io.ObjectStreamClass.checkObjFieldValueTypes(ObjectStreamClass.java:1407)
	at java.base/java.io.ObjectInputStream.defaultCheckFieldValues(ObjectInputStream.java:23
```

​	原因（可能是启动的版本不对）：

```
LinkedMap class is being loaded from two different packages, and those are being assigned to each other.
LinkedMap类从两个不同的包中加载，这些包被相互分配
```

## 5 无法通过“显示主控状态”读取binlog文件名和位置 

```java
Cannot read the binlog filename and position via 'SHOW MASTER STATUS'. Make sure your server is correctly configured
```

![](/无法读取binlog日志名称和位置.png)

**问题**：没有获取binlog文件名和起始位置权限

**参考**：[MySQL Binlog 权限](https://www.cnblogs.com/chenzechao/p/15839542.html)

- MySQL Binlog权限需要三个权限 SELECT, REPLICATION SLAVE, REPLICATION CLIENT，同时增加超级复制客户端权限

```mysql
SHOW MASTER STATUS;  # 可查看binlog文件名和起始位置。

# 执行异常
# Access denied; you need (at least one of) the SUPER, REPLICATION CLIENT privilege(s) for this operation（访问；此操作需要（至少一个）超级复制客户端权限）
```

## 6 不能识别LONGTEXT(Clob)

```
java.lang.UnsupportedOperationException: class org.apache.calcite.sql.SqlIdentifier: LONGTEXT
```

在 Apache Flink 的 SQL 中，默认情况下是不支持 `LONGTEXT` 数据类型的。Flink SQL 内置的数据类型集合有限，只包含一组常用的数据类型，如 INTEGER、VARCHAR、BOOLEAN 等。

如果你想在 Flink SQL 中使用类似 `LONGTEXT` 的数据类型，可以考虑通过以下方式来处理：

1. 使用 VARCHAR 类型：将 `LONGTEXT` 字段映射为 Flink SQL 中的 VARCHAR 类型。VARCHAR 类型可以容纳较长的文本值，具体允许的最大长度取决于 Flink 版本和配置。
2. 自定义表函数或表源：如果你需要更复杂的文本处理，例如超过 VARCHAR 类型允许的长度限制，你可以**自定义 Flink 的表函数或表源来处理 `LONGTEXT` 类型**。这样可以根据实际需求进行灵活的数据处理。

请注意，在选择数据类型时应该综合考虑数据大小、性能和内存消耗等因素。确保所选的数据类型适用于你的数据集，并且能够满足计算和处理的要求。

另外，需要注意的是，Flink 的版本更新可能会引入新的数据类型或功能。因此，确保你使用的 Flink 版本与你的需求相匹配，并参考相关的官方文档以获取最新信息和支持的数据类型列表。

----

- 解决办法1：**自定义 Flink 的表函数或表源来处理 `LONGTEXT` 类型示例**

要在 Java 中自定义 Flink 的表函数或表源来处理 `LONGTEXT` 类型，你可以使用 Flink 提供的接口和类来实现自定义功能。下面是一个简单的示例，展示了如何自定义一个表函数来处理 `LONGTEXT` 类型的数据：

```java
import org.apache.flink.table.functions.ScalarFunction;

public class LongTextProcessor extends ScalarFunction {
    public String processLongText(String longText) {
        // 进行对长文本数据的处理逻辑
        // 在这里可以实现你需要的文本处理操作
        // 返回处理后的结果
        return "Processed: " + longText;
    }
}
```

在上述示例中，我们创建了一个名为 `LongTextProcessor` 的表函数，并重写了 `processLongText` 方法以进行自定义的文本处理逻辑。你可以根据实际需求在该方法中编写你自己的文本处理代码。

接下来，你可以将此表函数应用于 Flink 表中的 `LONGTEXT` 类型列。以下是一个示例演示如何在 Flink SQL 中使用自定义的表函数：

```java
import org.apache.flink.table.api.EnvironmentSettings;
import org.apache.flink.table.api.TableEnvironment;

public class CustomFunctionExample {
    public static void main(String[] args) {
        EnvironmentSettings settings = EnvironmentSettings.newInstance().inStreamingMode().build();
        TableEnvironment tableEnv = TableEnvironment.create(settings);

        // 注册自定义表函数
        tableEnv.registerFunction("longTextProcessor", new LongTextProcessor());

        // 执行 Flink SQL 查询，并应用自定义表函数
        tableEnv.executeSql("CREATE TABLE myTable (id INT, longText LONGTEXT) WITH (...)");

        String query = "SELECT id, longTextProcessor(longText) AS processedText FROM myTable";
        tableEnv.executeSql(query).print();
    }
}
```

在上述示例中，我们首先注册了自定义的表函数 `longTextProcessor`。然后，在 Flink SQL 查询中使用该函数来处理 `myTable` 表中的 `LONGTEXT` 类型列。

请注意，在实际应用中，你需要根据具体的场景和需求，将自定义表函数集成到 Flink 的执行环境中，并进行适当的配置和调优。

类似地，你也可以自定义 Flink 的表源（TableSource）来处理 `LONGTEXT` 类型的数据。通过实现相应的接口和方法，你可以构建自己的表源来读取、解析和处理 `LONGTEXT` 类型的数据。

- 解决办法2：**针对特殊字段类型，做个字段映射转换**

MySQL字段映射转换：https://ververica.github.io/flink-cdc-connectors/master/content/connectors/mysql-cdc%28ZH%29.html#id17

## 7 com.fasterxml.jackson

​		序列化失败，原因是：提交环境和提交的flink平台环境的jar包版本不一致导致的。

​		解决办法，将本地环境和flink环境的jackson jar包版本改成一致即可。

![](/jackjson序列号jar冲突问题.png)

```java
Caused by: org.apache.flink.runtime.JobException: Cannot instantiate the coordinator for operator Source: TableSourceScan(table=[[default_catalog, default_database, MyTable1]], fields=[id, account, password, name]) -> DropUpdateBefore -> NotNullEnforcer(fields=[id]) -> Sink: Sink(table=[default_catalog.default_database.MyTable2], fields=[id, account, password, name])
	at org.apache.flink.runtime.executiongraph.ExecutionJobVertex.<init>(ExecutionJobVertex.java:217)
	at org.apache.flink.runtime.executiongraph.DefaultExecutionGraph.attachJobGraph(DefaultExecutionGraph.java:792)
	at org.apache.flink.runtime.executiongraph.DefaultExecutionGraphBuilder.buildGraph(DefaultExecutionGraphBuilder.java:196)
	at org.apache.flink.runtime.scheduler.DefaultExecutionGraphFactory.createAndRestoreExecutionGraph(DefaultExecutionGraphFactory.java:107)
	at org.apache.flink.runtime.scheduler.SchedulerBase.createAndRestoreExecutionGraph(SchedulerBase.java:342)
	at org.apache.flink.runtime.scheduler.SchedulerBase.<init>(SchedulerBase.java:190)
	at org.apache.flink.runtime.scheduler.DefaultScheduler.<init>(DefaultScheduler.java:122)
	at org.apache.flink.runtime.scheduler.DefaultSchedulerFactory.createInstance(DefaultSchedulerFactory.java:132)
	at org.apache.flink.runtime.jobmaster.DefaultSlotPoolServiceSchedulerFactory.createScheduler(DefaultSlotPoolServiceSchedulerFactory.java:110)
	at org.apache.flink.runtime.jobmaster.JobMaster.createScheduler(JobMaster.java:340)
	at org.apache.flink.runtime.jobmaster.JobMaster.<init>(JobMaster.java:317)
	at org.apache.flink.runtime.jobmaster.factories.DefaultJobMasterServiceFactory.internalCreateJobMasterService(DefaultJobMasterServiceFactory.java:107)
	at org.apache.flink.runtime.jobmaster.factories.DefaultJobMasterServiceFactory.lambda$createJobMasterService$0(DefaultJobMasterServiceFactory.java:95)
	at org.apache.flink.util.function.FunctionUtils.lambda$uncheckedSupplier$4(FunctionUtils.java:112)
	... 7 more
Caused by: java.io.InvalidClassException: com.fasterxml.jackson.databind.cfg.MapperConfigBase; local class incompatible: stream classdesc serialVersionUID = 968812702237387205, local class serialVersionUID = 2342207288015455914
	at java.base/java.io.ObjectStreamClass.initNonProxy(ObjectStreamClass.java:689)
	at java.base/java.io.ObjectInputStream.readNonProxyDesc(ObjectInputStream.java:1903)
	at java.base/java.io.ObjectInputStream.readClassDesc(ObjectInputStream.java:1772)
	at java.base/java.io.ObjectInputStream.readNonProxyDesc(ObjectInputStream.java:1903)
	at java.base/java.io.ObjectInputStream.readClassDesc(ObjectInputStream.java:1772)
	at java.base/java.io.ObjectInputStream.readOrdinaryObject(ObjectInputStream.java:2060
```



# end 

