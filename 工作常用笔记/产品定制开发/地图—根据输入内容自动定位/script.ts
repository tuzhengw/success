import { assign, SZEvent, Component, showWarningMessage, rc, wait, isEmpty, getCurrentMobileApp } from 'sys/sys';
import { InterActionEvent } from "metadata/metadata-script-api";
import { Dialog } from "commons/dialog";
import { getLocation, LngLat, GisMapType, Address, getAddress } from "metadata/gisapi";
import { GisMap_Amap, GisMapPoints_Amap, GisMapPointsArgs, GisMapLayer, GisMapArgs, GisMapLayerArgs, SearchBoxControl, GisMap } from "gismap/gismap.js";
import { Form } from "commons/form";
import { IMobileFrame } from "app/mobilepage";

/**
 * gis地图定位
 * 
 * 问题：选择地址—取消后, 交互没有关闭，导致选择地址无法再次触发（未解决）
 * 地址：https://jira.succez.com/browse/BI-44720
 * 
 * 20220815 tuzw
 * 问题：地图纠错中【选择位置】定位100米内功能定制
 * 地址：https://jira.succez.com/browse/CSTM-20019
 * 
 * 问题：gis地图定位后，地图背景是白色
 * 地址：https://jira.succez.com/browse/BI-45841
 */
export class ScanCodeSupervise {
    public CustomExpFunctions: any;
    public CustomActions: any;
    /** 警告提示消息 */
    public wainMessage: string;
    /** 是否绘制地图鹰眼，默认：关闭 */
    public isNeedDrawEagleEye: boolean;
    /** 地图鹰眼对象 */
    public gisMapDrawEagleEye: GisMapDrawEagleEye;
    private renderer: any;

    constructor() {
        this.CustomActions = {
            button_setSelectAddressCotent: this.button_setSelectAddressCotent.bind(this),
        };
        this.gisMapDrawEagleEye = null;
        this.isNeedDrawEagleEye = false;
        this.wainMessage = "该地址与您当前位置相距太远，请选择蓝色区域范围内地址";
    }

    /**
     * 自动定位到当前位置
     * 
     * @description 脚本交互和打开gis地图交互同时打开——立即执行
     * 
     * @param localAddress 定位的详细地址，eg："武汉东方赛思有限责任公司"，注意：SPG可直接：当前位置.详细地址
     * @param limitLocatRange? 限制地图所选地址范围，eg: "300"
     * @parma isNeedDrawEagleEye? 是否需要根据限制范围绘制地图鹰眼，eg：'false'
     * @param wainMessage? 超过所选范围的地址提示信息
     */
    private button_setSelectAddressCotent(event: InterActionEvent) {
        this.renderer = event.renderer;
        let page = event.page;
        let params = event?.params;
        let localAddress: string = !!params.localAddress ? params.localAddress.trim() : "";
        let limitLocatRange: number | string = params.limitLocatRange;
        if (!isEmpty(limitLocatRange) && typeof limitLocatRange == 'string') {
            limitLocatRange = Number(limitLocatRange);
        }
        this.init(params);
        if (!localAddress) {
            showWarningMessage("未捕获到定位的地址信息");
            return;
        }
        return getLocation(localAddress).then((addressLngLat: LngLat) => {
            if (!addressLngLat) {
                showWarningMessage(`未获取到${localAddress}的经纬度信息`);
                return;
            }
            /** 经度 */
            let lng: number = addressLngLat.lng;
            /** 纬度 */
            let lat: number = addressLngLat.lat;
            page.setParameter("lng", lng);
            page.setParameter("lat", lat);
            /**
             * 由于选择地址交互必须等待确认后，才会执行后续的交互，故：
             * 		将选择地址和后续交互都改成：立即执行，这里需要等待：选择地址地图加载完成
             */
            let autoLocal = () => {
                return wait(10).then(() => {
                    console.log('check chooselocationpanel');
                    let dom = document.querySelector(".chooselocationpanel-base");
                    let obj = dom && dom.szobject;
                    if (!obj) return autoLocal();
                    console.log('waitRender chooselocationpanel');
                    return obj.waitRender().then(() => {
                        let mobileApp: IMobileFrame = getCurrentMobileApp();
                        if (mobileApp) { // 校验当前是否移动端打开，注意：移动端不会打开对话框
                            this.mobileAutoLocalCurrentSite(lng, lat, limitLocatRange as number);
                        } else {
                            this.pcAutoLocalCurrentSite(lng, lat, limitLocatRange as number);
                        }
                        return true;
                    });
                });
            };

            return autoLocal();
        });
    }

    /**
     * 处理外部传入的参数
     */
    private init(params: {
        isNeedDrawEagleEye?: string,
        wainMessage?: string
    }): void {
        let isNeedDrawEagleEye: string = params.isNeedDrawEagleEye;
        if (!isEmpty(isNeedDrawEagleEye) && isNeedDrawEagleEye == 'true') {
            this.isNeedDrawEagleEye = true;
        }
        let wainMessage: string = params.wainMessage;
        if (!isEmpty(wainMessage)) {
            this.wainMessage = wainMessage;
        }
    }

    /**
     * 功能：【PC端】自动定位到当前位置
     * @param lng 经度
     * @param lat 纬度
     * @param limitLocatRange 限制用户在地图所选范围
     */
    private pcAutoLocalCurrentSite(lng: number, lat: number, limitLocatRange?: number): void {
        /** 地图所在面板对象 */
        let markPanel = document.querySelector(".chooselocationpanel-base").szobject;
        getAddress({ lng: lng, lat: lat }).then((address: Address) => {
            markPanel.value = address; // 将地址对象赋值给选择位置信息的面板, 不然没办法将自动定位的地址返回页面。

            /** 地图所在的对话框对象 */
            let gisDiaLog = markPanel.domBase.closest(".dialog-base").szobject;
            let searchComponent: SearchBoxControl = markPanel.search;
            searchComponent.setValue(address.formattedAddress); // 将当前地址写入页面搜索栏

            /** 高德地图对象 */
            let gisMap: GisMap_Amap = markPanel.map;
            markPanel.setMarker({ lng: lng, lat: lat }); // 标记当前位置
            gisMap.setZoomAndCenter(16, { lng: lng, lat: lat }); // 设置缩放级别 和 定位经纬度
            // @see https://jira.succez.com/browse/BI-47569 隐藏自动定位时显示的等待
            gisMap.waitRender().then(() => this.renderer && this.renderer.hideWaiting());

            if (!!limitLocatRange) {
                if (this.isNeedDrawEagleEye) {
                    if (this.gisMapDrawEagleEye == null) {
                        this.gisMapDrawEagleEye = new GisMapDrawEagleEye({
                            amap: gisMap.amap,
                            domBase: gisMap.domBase
                        });
                    } else {
                        this.gisMapDrawEagleEye.amap = gisMap.amap;
                    }
                }
                this.dialogCheckCurrentAddressDistanceIsLegal(lng, lat, limitLocatRange, gisDiaLog);
                this.mapCheckCurrentAddressDistanceIsLegal(lng, lat, limitLocatRange, gisMap);
                this.searchCheckCurrentAddressDistanceIsLegal(lng, lat, limitLocatRange, searchComponent);

                if (this.isNeedDrawEagleEye) {
                    this.gisMapDrawEagleEye.clearDrawGeometry(true);
                    this.gisMapDrawEagleEye.loadDrawGeometry(`around(${lng}, ${lat}, ${limitLocatRange})`);
                }
            }
        });
    }

    /**
     * 功能：【移动端】自动定位到当前位置
     * @param lng 经度
     * @param lat 纬度
     * @param limitLocatRange 限制用户在地图所选范围
     */
    private mobileAutoLocalCurrentSite(lng: number, lat: number, limitLocatRange?: number): void {
        /** 地图所在面板对象 */
        let markPanel = document.querySelector(".chooselocationpanel-base.mobile").szobject;
        getAddress({ lng: lng, lat: lat }).then((address: Address) => { // 根据经纬度获取具体的地址信息
            markPanel.value = address; // 将地址对象赋值给选择位置信息的面板, 不然没办法将自动定位的地址返回页面。
            let searchComponent: SearchBoxControl = markPanel.search;
            searchComponent.setValue(address.formattedAddress); // 将当前地址写入页面搜索栏

            /** 高德地图对象 */
            let gisMap: GisMap_Amap = markPanel.map;
            markPanel.setMarker({ lng: lng, lat: lat }); // 标记当前位置
            gisMap.setZoomAndCenter(16, { lng: lng, lat: lat }); // 设置缩放级别 和 定位经纬度
            // @see https://jira.succez.com/browse/BI-47569 隐藏自动定位时显示的等待
            gisMap.waitRender().then(() => this.renderer && this.renderer.hideWaiting());

            if (this.isNeedDrawEagleEye) {
                if (this.gisMapDrawEagleEye == null) {
                    this.gisMapDrawEagleEye = new GisMapDrawEagleEye({
                        amap: gisMap.amap,
                        domBase: gisMap.domBase
                    });
                } else {
                    this.gisMapDrawEagleEye.amap = gisMap.amap;
                }
            }
            this.panelCheckCurrentAddressDistanceIsLegal(lng, lat, limitLocatRange, markPanel);
            this.mapCheckCurrentAddressDistanceIsLegal(lng, lat, limitLocatRange, gisMap);
            this.searchCheckCurrentAddressDistanceIsLegal(lng, lat, limitLocatRange, searchComponent);

            if (this.isNeedDrawEagleEye) {
                this.gisMapDrawEagleEye.clearDrawGeometry(true);
                this.gisMapDrawEagleEye.loadDrawGeometry(`around(${lng}, ${lat}, ${limitLocatRange})`);
            }
        });
    }

    /**
     * 功能：移动端-【地图所在面板框——确认】校验当前选择地址的距离（与当前用户所在位置）是否合法
     * 注意：选择地址控件——不要设置【连续输入】
     * @param startLng 用户所在地址经度
     * @param startLat 用户所在地址纬度
     * @param limitLocatRange? 限制地图所选地址范围
     * @param panel 地图所在面板对象
     */
    private panelCheckCurrentAddressDistanceIsLegal(startLng: number, startLat: number, limitLocatRange: number, panel): void {
        if (!!limitLocatRange || limitLocatRange == 0) { // 是否需要限制选择范围
            panel.startLng = startLng;
            panel.startLat = startLat;
            panel.limitLocatRange = limitLocatRange
            panel.oldOnok = panel.onok; // 保留原有onok方法

            panel.onok = (e: { value: Address } & SZEvent, component?: Component) => {
                let startLng: number = panel.startLng;
                let startLat: number = panel.startLat;

                let currentLocalAddress = panel.getValue();
                if (!!currentLocalAddress) { // 校验当前是否又再页面选择新地址，若地址未发生变化，则页面提示：校验失败
                    let endLng: number = currentLocalAddress.location.lng;
                    let endLat: number = currentLocalAddress.location.lat;
                    let distance: number = this.getTwoAddressDistance(startLng, startLat, endLng, endLat);
                    if (distance > Number(limitLocatRange)) {
                        showWarningMessage(this.wainMessage);
                        return;
                    } else {
                        panel.oldOnok(e);
                    }
                } else {
                    panel.oldOnok(e);
                }
            }
        }
    }

    /**
     * 功能：【对话框-确认】校验当前选择地址的距离（与当前用户所在位置）是否合法
     * 注意：选择地址控件——不要设置【连续输入
     * @param startLng 用户所在地址经度
     * @param startLat 用户所在地址纬度
     * @param limitLocatRange? 限制地图所选地址范围
     * @param dialog 地图所在对话框对象
     */
    private dialogCheckCurrentAddressDistanceIsLegal(startLng: number, startLat: number, limitLocatRange: number, dialog: Dialog): void {
        if (!!limitLocatRange || limitLocatRange == 0) { // 是否需要限制选择范围
            dialog.startLng = startLng; // 避免上下文丢失，这里将参数赋值到对话框对象上
            dialog.startLat = startLat;
            dialog.limitLocatRange = limitLocatRange

            dialog.onok = (sze: SZEvent, dialog: Dialog, btn: Button, form?: Form) => { // 重写已有的对话框确认事件，校验当前用户位置与页面所选位置之间的距离
                let startLng: number = dialog.startLng;
                let startLat: number = dialog.startLat;

                let chooseLocationPanel = dialog.getInnerComponent();
                let currentLocalAddress = chooseLocationPanel.getValue();
                if (!!currentLocalAddress) { // 校验当前是否又再页面选择新地址，若地址未发生变化，则页面提示：校验失败
                    let endLng: number = currentLocalAddress.location.lng;
                    let endLat: number = currentLocalAddress.location.lat;
                    let distance: number = this.getTwoAddressDistance(startLng, startLat, endLng, endLat);
                    if (distance > Number(limitLocatRange)) {
                        showWarningMessage(this.wainMessage);
                        return false;
                    } else {
                        return dialog.doClick(sze); // 走原有关闭逻辑，将其页面选择的地址信息返回到页面
                    }
                } else {
                    return dialog.doClick(sze);
                }
            }
        }
    }

    /**
     * 功能：【地图对象】校验当前选择地址的距离（与当前用户所在位置）是否合法
     * @param startLng 用户所在地址经度
     * @param startLat 用户所在地址纬度
     * @param limitLocatRange? 限制地图所选地址范围
     * @param map 地图对象
     */
    private mapCheckCurrentAddressDistanceIsLegal(startLng: number, startLat: number, limitLocatRange: number, map: GisMap_Amap): void {
        if (!!limitLocatRange || limitLocatRange == 0) { // 是否需要限制选择范围
            map.startLng = startLng; // 避免上下文丢失，这里将参数赋值到对话框对象上
            map.startLat = startLat;
            map.limitLocatRange = limitLocatRange;
            map.oldOnClick = map.onclick; // 保留原有onclick方法

            map.onclick = (sze: SZEvent, map: GisMap, position: LngLat) => {
                let startLng: number = map.startLng;
                let startLat: number = map.startLat;
                let distance: number = this.getTwoAddressDistance(startLng, startLat, position.lng, position.lat);
                if (distance > limitLocatRange) {
                    showWarningMessage(this.wainMessage);
                    return; // 直接返回，输入框不会记录当前点击的地址信息
                } else {
                    map.oldOnClick(sze, map, position);
                }
            }
        }
    }

    /**
     * 功能：【地图搜索框】校验当前选择地址的距离（与当前用户所在位置）是否合法
     * @param startLng 用户所在地址经度
     * @param startLat 用户所在地址纬度
     * @param limitLocatRange? 限制地图所选地址范围
     * @param search 搜索框对象
     */
    private searchCheckCurrentAddressDistanceIsLegal(startLng: number, startLat: number, limitLocatRange: number, search: SearchBoxControl): void {
        if (!!limitLocatRange || limitLocatRange == 0) { // 是否需要限制选择范围
            search.startLng = startLng; // 避免上下文丢失，这里将参数赋值到对话框对象上
            search.startLat = startLat;
            search.limitLocatRange = limitLocatRange
            search.onok = search.onok; // 保留原有onok方法

            search.onok = (address: Address) => {
                let startLng: number = search.startLng;
                let startLat: number = search.startLat;
                let endLng: number = address.location.lng;
                let endlat: number = address.location.lat;
                let distance: number = this.getTwoAddressDistance(startLng, startLat, endLng, endlat);
                if (distance > limitLocatRange) {
                    showWarningMessage(this.wainMessage);
                    return; // 直接返回，输入框不会记录当前点击的地址信息
                } else {
                    search.onok(address);
                }
            }
        }
    }

    /**
     * 功能：通过经纬度计算两个地址距离
     * @param startLng 起始纬度
     * @param startLat 起始经度
     * @param endLng 结束纬度
     * @param endLat 结束经度
     * @return 两者之间的距离，单位米
     */
    public getTwoAddressDistance(startLng: number, startLat: number, endLng: number, endLat: number): number {
        if ((!startLng && !startLat) || (!endLng && !endLat)) {
            return 0;
        }
        let pi: number = Math.PI / 180.0;
        let latDistance: number = startLat * pi - endLat * pi;
        let lngDistance: number = startLng * pi - endLng * pi;
        let distance: number = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(latDistance / 2), 2) + Math.cos(startLat * pi) * Math.cos(endLat * pi) * Math.pow(Math.sin(lngDistance / 2), 2)));
        distance = distance * 6378.137;
        distance = Math.round(distance * 10000) / 10;
        return distance;
    }
}

/**
 * 20220523 tuzw
 * 地图——鹰眼绘制
 */
export class GisMapDrawEagleEye {
    private ondiddraw: (event: SZEvent, component: GisMapDrawEagleEye) => string;
    /** 地图实例 */
    public amap: AMap.Map;
    /** 地图所在DOM */
    public domBase;
    /** 地图上的所有图层 */
    private layers: GisMapLayer[];
    /** 地图上的所有图层的数据类 */
    private layerMap: Map<string, GisMapLayer>;
    /** 鼠标绘图工具 */
    private mouseTool: any;
    /** 地图上各个形状的编辑器 */
    private geometryEditors: any[];
    /** 绘图信息 */
    private drawGeometry: string;
    /** 绘制的覆盖物对象 */
    private drawOverlays: AMap.Overlay[];
    /** 加载绘图控件的promise */
    private loadMouseToolPromise: Promise<void>;
    /** 绘制形状的样式(背景、边框) */
    private geometryOptions: JSONObject;
    /** 记录地图状态，如是否正在绘制形状、编辑形状等，统一管理方便控制鼠标事件是否应该生效 */
    private mapStatus: Set<string>;
    /** 鼠标相关事件是否应该触发，每次更新mapStatus时会重新计算 */
    private mouseEventEnable: boolean;

    constructor(args: GisMapDrawEagleEyeArgs) {
        this.layers = [];
        this.drawOverlays = [];
        this.geometryEditors = [];
        this.amap = args.amap;
        this.domBase = args.domBase;
        this.layerMap = new Map<string, GisMapLayer>();
        this.drawGeometry = args.drawGeometry || null;
        this.ondiddraw = args.ondiddraw || null;
        this.mapStatus = new Set<string>();
        this.geometryOptions = assign({
            noSelect: true,
            fillColor: "#1791fc",
            fillOpacity: 0.35,
            strokeColor: "#1791fc",
            strokeOpacity: 0.8,
            strokeStyle: "solid",
            strokeWeight: 2,
            zIndex: 1000
        }, args.geometryOptions);
    }

    /**
     * 开始绘制形状。如果当前正处于绘制状态，则终止并删除正在绘制的形状，并开始新的绘制。
     * @param type around|rectangle|polygon
     */
    public startDrawGeometry(type: string): Promise<void> {
        let mouseTool = this.mouseTool;
        let promise: Promise<void>;
        if (!mouseTool) {
            this.loadMouseToolPromise = promise = new Promise((resolve) => {
                this.amap.plugin(["AMap.MouseTool"], () => {
                    let mouseTool = this.mouseTool = new (<any>AMap).MouseTool(this.amap);//在地图中添加MouseTool插件
                    this.loadMouseToolPromise = null;
                    (<any>AMap).Event.addListener(mouseTool, 'draw', (e: any) => { // 添加事件
                        this.updateMapStatus('drawGeometry', false);
                        if (!e) {
                            return;
                        }
                        let obj = e.obj;
                        this.createGeometryEditor(obj).then(editor => {
                            this.domBase.style.cursor = '';
                            mouseTool.close();
                            this.drawOverlays.push(obj);
                            if (false) { // 处于编辑状态则刚绘制的形状也进入编辑状态，不更新属性this.isEitingGeometry()
                                editor.open();
                            } else { // 不处于编辑状态则更新属性、触发事件
                                let geometry = this.getGeometryString(obj);
                                if (geometry === '') {
                                    return;
                                }
                                let drawGeometry = this.drawGeometry;
                                this.drawGeometry = drawGeometry ? drawGeometry + ';' + geometry : geometry;
                                this.ondiddraw && this.ondiddraw({
                                    component: this.domBase,
                                    value: geometry
                                }, this);
                            }
                        });
                    });
                });
            });
        } else {
            promise = Promise.resolve();
        }
        this.domBase.style.cursor = 'Crosshair';
        return promise.then(() => {
            if (!type || type === 'around') {
                this.mouseTool.circle(this.geometryOptions);
            } else if (type === 'rectangle') {
                this.mouseTool.rectangle(this.geometryOptions);
            } else if (type === 'polygon') {
                this.mouseTool.polygon(this.geometryOptions);
            }
        });
    }

    /**
     * 绘制图形，如果当前有形状，则覆盖
     * @param geometry 绘制图像的参数，形状(经度，纬度，半径/米)
     * 1. 方圆500米`around(117.195907, 39.118327, 500)`
     * 2. 矩形`rectangle(117.195907, 39.118327, 116.925304, 38.935671)`
     * 3. 或多边形`polygon(117.195907, 39.118327, 116.925304, 38.935671, 117.654173, 39.032846)`
     */
    public loadDrawGeometry(geometry: string): void {
        this.clearDrawGeometry(false); // 清空历史图形
        this.drawGeometry = geometry;
        if (!geometry) {
            return;
        }
        geometry = geometry.toLowerCase();
        let geometries = geometry.split(';');
        let drawOverlays = this.drawOverlays;
        geometries.forEach(gm => {
            let overlay: AMap.Overlay;
            if (gm.indexOf('around') === 0) {
                let array = gm.substr(7, gm.length - 8).split(',').map(item => parseFloat(item.trim()));
                overlay = new AMap.Circle(assign({ center: [array[0], array[1]], radius: array[2] }, this.geometryOptions) as any);
            } else if (gm.indexOf('rectangle') === 0) {
                let array = gm.substr(10, gm.length - 11).split(',').map(item => parseFloat(item.trim()));
                overlay = new AMap.Rectangle(assign({ bounds: new AMap.Bounds([array[0], array[1], array[2], array[3]]) }, this.geometryOptions) as any);
            } else if (gm.indexOf('polygon') === 0) {
                let array = gm.substr(8, gm.length - 9).split(',').map(item => parseFloat(item.trim()));
                let path: AMap.LocationValue[] = [];
                for (let i = 0; i < array.length; i = i + 2) {
                    const lng = array[i];
                    const lat = array[i + 1];
                    path.push([lng, lat]);
                }
                overlay = new AMap.Polygon(assign({ path: path }, this.geometryOptions) as any);
            }
            if (overlay) {
                drawOverlays.push(overlay);
                this.amap.add(overlay);
            }
        });
        this.ondiddraw && this.ondiddraw({
            value: this.getDrawGeometry(),
            component: this.domBase
        }, this);
    }

    /**
     * 开始编辑形状
     * @param obj 
     */
    private createGeometryEditor(obj: any): Promise<any> {
        let amap = this.amap;
        switch (obj.className) {
            case 'Overlay.Circle': {
                return new Promise<void>(resolve => {
                    (AMap as any).CircleEditor ? resolve() : amap.plugin(['AMap.CircleEditor'], resolve);
                }).then(() => {
                    let editor = new (AMap as any).CircleEditor(amap, obj);
                    this.geometryEditors.push(editor);
                    return editor;
                });
            }
            case 'Overlay.Rectangle': {
                return new Promise<void>(resolve => {
                    (AMap as any).RectangleEditor ? resolve() : amap.plugin(['AMap.RectangleEditor'], resolve);
                }).then(() => {
                    let editor = new (AMap as any).RectangleEditor(amap, obj);
                    this.geometryEditors.push(editor);
                    return editor;
                });
            }
            case 'Overlay.Polygon': {
                return new Promise<void>(resolve => {
                    (AMap as any).PolyEditor ? resolve() : amap.plugin(['AMap.PolyEditor'], resolve);
                }).then(() => {
                    let editor = new (AMap as any).PolyEditor(amap, obj);
                    this.geometryEditors.push(editor);
                    return editor;
                });
            }
            default: {
                return null;
            }
        }
    }

    /**
     * 获取一个形状的字符串形式
     * @param obj 
     */
    public getGeometryString(obj: any): string {
        switch (obj.className) {
            case 'Overlay.Circle': {
                let center = <AMap.LngLat>obj.getCenter();
                let radius = obj.getRadius();
                return `around(${center.getLng()}, ${center.getLat()}, ${radius})`;
            }
            case 'Overlay.Rectangle': {
                let bounds = <AMap.Bounds>obj.getBounds();
                let nw = bounds.getNorthWest();
                let se = bounds.getSouthEast();
                return `rectangle(${nw.getLng()},${nw.getLat()},${se.getLng()},${se.getLat()})`;
            }
            case 'Overlay.Polygon': {
                let path = <any>obj.getPath();
                let length = path.length;
                let geometry = '';
                if (length > 0) {
                    geometry = 'polygon(';
                    path.forEach((p: AMap.LngLat, index: number) => {
                        geometry += `${p.getLng()},${p.getLat()}`;
                        if (index < length - 1) {
                            geometry += ',';
                        }
                    });
                    geometry += ')';
                }
                return geometry;
            }
        }
        return '';
    }

    /**
     * 等待鼠标绘图工具加载完
     */
    public waitMouseToolLoad(): Promise<void> {
        return this.loadMouseToolPromise || Promise.resolve();
    }

    /**
     * 清除正在绘制和已经绘制得形状。
     * @param fire 是否触发形状变更事件，装载形状的清空操作不应触发事件，装载完成后再触发
     */
    public clearDrawGeometry(fire = true): void {
        this.endEditAllGeometry(false);
        this.drawGeometry = null;
        this.mouseTool && this.mouseTool.close();
        this.amap.remove(this.drawOverlays);
        this.drawOverlays = [];
        this.geometryEditors = [];
        fire && this.ondiddraw && this.ondiddraw({
            value: this.getDrawGeometry(),
            component: this.domBase
        }, this);
    }

    /**
     * 结束编辑形状
     * @param fire 是否触发形状变更事件
     */
    public endEditAllGeometry(fire = true): void {
        this.geometryEditors.forEach(e => e.close());
        this.drawGeometry = this.drawOverlays.map(obj => this.getGeometryString(obj)).join(';'); // 结束编辑重新计算当前形状
        fire && this.ondiddraw && this.ondiddraw({
            value: this.getDrawGeometry(),
            component: this.domBase
        }, this);
    }

    /**
     * 返回当前已经绘制完成的形状。
     */
    public getDrawGeometry(): string {
        return this.drawGeometry;
    }

    /**
     * 改变地图状态，并刷新鼠标事件是否应该触发
     * 如：
     * updateMapStatus('drawGeometry', true) 表示进入绘制形状的状态，这种状态下鼠标事件都不应该触发，鼠标移入散点图层的样式也不应该改变
     * updateMapStatus('drawGeometry', false) 表示退出绘制形状的状态
     */
    private updateMapStatus(name: string, value: boolean): void {
        let set = this.mapStatus;
        value ? set.add(name) : set.delete(name);
        this.setMouseEventEnable(set.size === 0);
    }

    /**
     * 设置鼠标事件是否触发
     */
    private setMouseEventEnable(enable: boolean): void {
        let e = this.mouseEventEnable;
        if (e === enable) {
            return;
        }
        e = this.mouseEventEnable = enable;
        this.amap.emit('mouseeventenablechange', {
            enable: e
        });
    }

    /**
     * 是否正在编辑形状
     */
    public isEitingGeometry(): boolean {
        return this.mapStatus.has('editGeometry');
    }
}

/** {@link GisMapDrawEagleEye} 的构造参数 */
export interface GisMapDrawEagleEyeArgs {
    /** 地图实例 */
    amap: AMap.Map;
    /** dom */
    domBase,
    /** 图层 */
    layers?: GisMapLayerArgs[];
    /** 地图初始中心 */
    center?: LngLat;
    /** 默认的框选范围 */
    drawGeometry?: string;
    /** 绘制矢量图形的样式设置 */
    geometryOptions?: JSONObject;
    /** 地图状态，是否可缩放、可拖拽等 */
    status?: JSONObject;
    /** 画完矢量图形后调用的方法，返回覆盖物类型 */
    ondiddraw?: (event: SZEvent, component: GisMapPoints_Amap) => string;
}