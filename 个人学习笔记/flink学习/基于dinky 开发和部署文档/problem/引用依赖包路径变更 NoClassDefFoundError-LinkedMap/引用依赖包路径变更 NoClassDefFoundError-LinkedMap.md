---
typora-root-url: images
---

# `NoClassDefFoundError`

# 案例: `Oracle`依赖包路径被`shade`化

## 1 异常堆栈

​	`Oracle`同步到其他数据库提示：识别不到此路径，shading（阴影化）处理，导致类的**实际路径发生了变化**. 由于类路径发生了变更, 引用的路径是不存在类的,  从而导致: `java.lang.NoClassDefFoundError`.

```java
java.lang.NoClassDefFoundError: com/ververica/cdc/connectors/shaded/org/apache/commons/collections/map/LinkedMap
    
	at com.ververica.cdc.debezium.DebeziumSourceFunction.<init>(DebeziumSourceFunction.java:146)
	at com.ververica.cdc.connectors.oracle.OracleSource$Builder.build(OracleSource.java:195)
	at com.dlink.cdc.oracle.OracleCDCBuilder.build(OracleCDCBuilder.java:115)
	at com.dlink.trans.ddl.CreateCDCSourceOperation.build(CreateCDCSourceOperation.java:200)
	at com.dlink.interceptor.FlinkInterceptor.build(FlinkInterceptor.java:54)
	at com.dlink.executor.Executor.pretreatExecute(Executor.java:230)
	at com.dlink.executor.Executor.executeSql(Executor.java:243)
	at com.flinkbi.FlinkBiSync.flinkJobSubmit(FlinkBiSync.java:153)
```

## 2 前后生成对比

​	2024年10月23日重新对整个本地的`Flink cdc `执行`mvn clean install -Dmaven.test.skip=true`命令生成相关执行`jar`包，产生了一些和之间**编译生成的包有着不一样**的地方，例如：

```
org.apache.commons.collections.map

com.ververica.cdc.connectors.shaded.org.apache.commons.collections.map.LinkedMap
```

​	`org.apache.commons.collections.map`依赖包，编译后引用包路径是原本依赖包本身的路径，但最新编译后的包，其引用依赖则增加了`shade`前缀路径来放置重名包路径类名冲突问题。

​	==前后并没有更改==生成`pom.xml`文件的打包配置，但确出现了上面的差异，截图：

![](/linkedMap正确引用路径.png)

​	上述截图效果是之前编译生成的类路径，最新生成后的包变更后的引用路径如下：

![](/linkedMap变更引用路径.png)

## 3 分析引入路径变更原因

​	整个`Flink connector cdc `内的子项目中`pom.xml`都是配置的打包插件：`Maven Shade Plugin`，该插件：用于将项目及其所有依赖项打包成一个可执行的胖 JAR（fat JAR）。这个插件不仅可以合并多个依赖，还支持过滤、排除、修改 MANIFEST 文件等高级功能。

> `Maven Shade Plugin`插件详细地址：
>
> https://blog.csdn.net/Li_WenZhang/article/details/142874764?sharetype=blog&shareId=142874764&sharerefer=APP&sharesource=2301_79143796&sharefrom=qq

​	使用 `relocations` 可以将某些包路径重命名，以防止不同依赖中的类冲突：

```xml
<relocation>
    <!-- 原包名 -->
    <pattern>org.apache.commons.collections</pattern>
    <!-- 变更后的自定义包名 -->
    <shadedPattern>
        com.ververica.cdc.connectors.shaded.org.apache.commons.collections
    </shadedPattern>
</relocation>
```

​	检查了整个仓库中的`pom.xml`配置，==并没有针对`org.apache.commons.collections`引用路径的变更处理==，但最终生成出来的`fat jar`包中相关引用地方的引用路径都发生了变更。

​	根据最终生成的代码，==引用路径变更了，肯定是有地方配置影响了，而没有将代码放置，排查是因为==：所有相关子项目并没有将`org.apache.commons.collections.jar`打入包中，该包在项目引用都是来自：`flink-core.jar`中的子依赖，且设置都是：`provded`，所以：**仅变更了引用路径，而没有没有放置相关代码到指定的存放包路径**（例如存储在`fat jar`的父路径：`com.ververica.cdc.connectors.shaded`）。

​	由于没有找到相关的影响配置，就尝试给`org.apache.commons.collections`配置`relocation`，让变更后的存储目录下，能够存储`org.apache.commons.collections`相关代码，避免异常：

```
java.lang.NoClassDefFoundError: com/ververica/cdc/connectors/shaded/org/apache/commons/collections/map/LinkedMap
```



## 4 `sql-Oracle`配`linkedmap`变更

### 4.1 参考

​	参考：https://github.com/apache/flink-cdc/pull/2501，详细我已回复到此帖子中。

![](/解决NoClassDefFoundError依赖包.png)

### 4.2 详细`pom.xml`配置

> ​	直接配置 relocations, 由于此`commons-collections jar`来自`flink-core`, 且是provided, 导致打包被忽略了==, 为了生效将其独立引用==.
> ​	参考: https://github.com/apache/flink-cdc/pull/2501/files.

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!--
Copyright 2023 Ververica Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
  http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
-->
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <parent>
        <artifactId>flink-cdc-connectors</artifactId>
        <groupId>com.ververica</groupId>
        <version>2.4-SNAPSHOT</version>
    </parent>
    <modelVersion>4.0.0</modelVersion>

    <artifactId>flink-sql-connector-oracle-cdc</artifactId>

    <dependencies>
        <dependency>
            <groupId>com.ververica</groupId>
            <artifactId>flink-connector-oracle-cdc</artifactId>
            <version>${project.version}</version>
        </dependency>
 
<!--        <dependency>-->
<!--            <groupId>commons-collections</groupId>-->
<!--            <artifactId>commons-collections</artifactId>-->
<!--            <version>3.2.2</version>-->
<!--        </dependency>-->
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-shade-plugin</artifactId>
                <version>3.2.4</version>
                <executions>
                    <execution>
                        <id>shade-flink</id>
                        <phase>package</phase>
                        <goals>
                            <goal>shade</goal>
                        </goals>
                        <configuration>
                            <shadeTestJar>false</shadeTestJar>
                            <artifactSet>
                                <includes>
                                    <include>io.debezium:debezium-api</include>
                                    <include>io.debezium:debezium-embedded</include>
                                    <include>io.debezium:debezium-core</include>
                                    <incldue>io.debezium:debezium-ddl-parser</incldue>
                                    <include>io.debezium:debezium-connector-oracle</include>
                                    <include>com.ververica:flink-connector-debezium</include>
                                    <include>com.ververica:flink-cdc-base</include>
                                    <include>com.ververica:flink-connector-oracle-cdc</include>
                                    <include>org.antlr:antlr4-runtime</include>
                                    <include>com.github.jsqlparser:jsqlparser</include>
                                    <include>com.oracle.ojdbc:*</include>
                                    <!--
                                        2023-09-03 tuzw
                                        在启动应用时，JXDocumentBuilderFactory类与JDK 11的XML解析类冲突。
                                        作用：com.oracle.database.xml 是 Oracle 数据库的一个特定依赖项，用于支持 XML 数据处理功能，
                                     项目中使用 Oracle CDC，并且不需要处理 XML 数据，排除 com.oracle.database.xml 依赖项，以避免不必要的依赖。
                                        注意：若需要此类，可通过指定启动JVM参数来指定XML解析来解决解析类冲突的问题。
                                        建议：应用环境使用排除XML包后的cdc jar，而flink环境使用未排除的cdc jar包（数据同步最终由Flink环境负责）。
                                     -->
                                    <!-- <include>com.oracle.database.xml:*</include>-->
                                    <include>org.apache.kafka:*</include>
                                    <include>com.fasterxml.*:*</include>
                                    <include>com.google.guava:*</include>
                                    <!--  Include fixed version 30.1.1-jre-14.0 of flink shaded guava  -->
                                    <include>org.apache.flink:flink-shaded-guava</include>
<!--                                    <include>commons-collections:commons-collections</include>-->
                                </includes>
                            </artifactSet>
                            <filters>
                                <filter>
                                    <artifact>org.apache.kafka:*</artifact>
                                    <excludes>
                                        <exclude>kafka/kafka-version.properties</exclude>
                                        <exclude>LICENSE</exclude>
                                        <!-- Does not contain anything relevant.
                                            Cites a binary dependency on jersey, but this is neither reflected in the
                                            dependency graph, nor are any jersey files bundled. -->
                                        <exclude>NOTICE</exclude>
                                        <exclude>common/**</exclude>
                                    </excludes>
                                </filter>
                            </filters>
                            <relocations>

<!--                            <relocation>-->
<!--                                &lt;!&ndash; 原包名 &ndash;&gt;-->
<!--                                <pattern>org.apache.commons.collections</pattern>-->
<!--                                &lt;!&ndash; 变更后的自定义包名 &ndash;&gt;-->
<!--                                <shadedPattern>-->
<!--                                    com.ververica.cdc.connectors.shaded.org.apache.commons.collections-->
<!--                                </shadedPattern>-->
<!--                            </relocation>-->

                                <relocation>
                                    <pattern>org.apache.kafka</pattern>
                                    <shadedPattern>
                                        com.ververica.cdc.connectors.shaded.org.apache.kafka
                                    </shadedPattern>
                                </relocation>
                                <relocation>
                                    <pattern>org.antlr</pattern>
                                    <shadedPattern>
                                        com.ververica.cdc.connectors.shaded.org.antlr
                                    </shadedPattern>
                                </relocation>
                                <relocation>
                                    <pattern>com.google</pattern>
                                    <shadedPattern>
                                        com.ververica.cdc.connectors.shaded.com.google
                                    </shadedPattern>
                                </relocation>
                                <relocation>
                                    <pattern>com.fasterxml</pattern>
                                    <shadedPattern>
                                        com.ververica.cdc.connectors.shaded.com.fasterxml
                                    </shadedPattern>
                                </relocation>
                                <relocation>
                                    <pattern>net.sf.jsqlparser</pattern>
                                    <shadedPattern>
                                        com.ververica.cdc.connectors.shaded.net.sf.jsqlparser
                                    </shadedPattern>
                                </relocation>
                            </relocations>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>
</project>
```

## 5 `shade`的`relocations`注意点

​	内部引用的包，==必须被使用，其包的范围不能==是：`test`、`provided`，否则无法正确将对应的依赖包放入生成的后的包内。

## 6 重新生成包

![](/重新生成的flink-sql-oracle内的coolections存放位置.png)

## 7 解释为什么本地执行无误

​	`cdc`生成的fat 包==基础包类路径和类名是一致==的。

​	例如执行`Oracle -> GaussDb`，

```java
/**
 * 获取Oracle同步到Gauss SQL
 */
public static String getOracleToGaussSQL() {
    String cdcExecuteSql = "EXECUTE CDCSOURCE oracle-gauss WITH ("
            + "'connector' = 'oracle-cdc',"
            + "'hostname' = '192.168.10.205',"
            + "'port' = '1521',"
            + "'database-name' = 'orcl',"
            + "'username' = 'succez_test',"
            + "'password' = 'succez3.14',"
            + "'parallelism' = '1',"
            + "'checkpoint' = '30000',"
//                + "'scan.startup.mode' = 'latest-offset',"
            + "'scan.startup.mode' = 'initial',"
            + "'debezium.log.mining.sleep.time.default.ms' = '1000',"
            + "'debezium.log.mining.sleep.time.increment.ms' = '1000',"
            + "'debezium.log.mining.sleep.time.min.ms' = '1000',"
//                + "'debezium.log.mining.sleep.time.max.ms' = '30000',"
            + "'debezium.database.history.store.only.captured.tables.ddl' = 'true',"
            + "'debezium.log.mining.strategy' = 'online_catalog',"
            + "'debezium.decimal.handling.mode' = 'string',"
            + "'table-name' = 'SUCCEZ_TEST\\.TEST_STUDENT',"
            + "'sink.table.prefix' = 'ODS_',"
            + "'sink.connector' = 'jdbc',"
            + "'sink.url' = 'jdbc:opengauss://192.168.3.174:5432/test1',"
            + "'sink.table-name' ='flink_test.${tableName}',"
            + "'sink.username' = 'test1',"
            + "'sink.password' = 'Succez_2024',"

            + "'sink.sink.buffer-flush.interval' = '2s',"
            + "'sink.sink.buffer-flush.max-rows' = '100',"
            + "'sink.sink.max-retries' = '5'"
            + ")";
    return cdcExecuteSql;
}
```

​	`com.ververica.cdc.debezium.DebeziumSourceFunction`类内容是一致的，本地调式走的是`flink-sql-connector-mysql-cdc-2.4-SNAPSHOT.jar`内的`com.ververica.cdc.debezium.DebeziumSourceFunction`类。

![](/mysql和oracle的fat包对比.png)

​	`Debug`:

![](/Debug走到了mysql的jar.png)



## 8 更新`Flink`服务器`CDC fat`包

​	修改后，也需要将修改的包更新到`Flink`服务器上，否则：

```java
Caused by: org.apache.flink.streaming.runtime.tasks.StreamTaskException: Cannot load user class: com.ververica.cdc.connectors.shaded.org.apache.commons.collections.map.LinkedMap
ClassLoader info: URL ClassLoader:
Class not resolvable through given classloader.
	at org.apache.flink.streaming.api.graph.StreamConfig.getStreamOperatorFactory(StreamConfig.java:397)
	at org.apache.flink.streaming.runtime.tasks.OperatorChain.<init>(OperatorChain.java:162)
	at org.apache.flink.streaming.runtime.tasks.RegularOperatorChain.<init>(RegularOperatorChain.java:60)
	at org.apache.flink.streaming.runtime.tasks.StreamTask.restoreInternal(StreamTask.java:685)
	at org.apache.flink.streaming.runtime.tasks.StreamTask.restore(StreamTask.java:672)
	at org.apache.flink.runtime.taskmanager.Task.runWithSystemExitMonitoring(Task.java:935)
	at org.apache.flink.runtime.taskmanager.Task.restoreAndInvoke(Task.java:904)
	at org.apache.flink.runtime.taskmanager.Task.doRun(Task.java:728)
	at org.apache.flink.runtime.taskmanager.Task.run(Task.java:550)
	at java.base/java.lang.Thread.run(Thread.java:834)
Caused by: java.lang.ClassNotFoundException: com.ververica.cdc.connectors.shaded.org.apache.commons.collections.map.LinkedMap
	at java.base/java.net.URLClassLoader.findClass(URLClassLoader.java:471)
	at java.base/java.lang.ClassLoader.loadClass(ClassLoader.java:588)
	at org.apache.flink.util.FlinkUserCodeClassLoader.loadClassWithoutExceptionHandling(FlinkUserCodeClassLoader.java:67)
	at org.apache.flink.util.ChildFirstClassLoader.loadClassWithoutExceptionHandling(ChildFirstClassLoader.java:74)
	at org.apache.flink.util.FlinkUserCodeClassLoader.loadClass(FlinkUserCodeClassLoader.java:51)
	at java.base/java.lang.ClassLoader.loadClass(ClassLoader.java:521)
	at org.apache.flink.util.FlinkUserCodeClassLoaders$SafetyNetWrapperClassLoader.loadClass(FlinkUserCodeClassLoaders.java:192)
	at java.base/java.lang.Class.forName0(Native Method)
	at java.base/java.lang.Class.forName(Class.java:398)
	at org.apache.flink.util.InstantiationUtil$ClassLoaderObjectInputStream.resolveClass(InstantiationUtil.java:78)
	at java.base/java.io.ObjectInputStream.readNonProxyDesc(ObjectInputStream.java:1965)
	at java.base/java.io.ObjectInputStream.readClassDesc(ObjectInputStream.java:1851)
	at java.base/java.io.ObjectInputStream.readOrdinaryObject(ObjectInputStream.java:2139)
	at java.base/java.io.ObjectInputStream.readObject0(ObjectInputStream.java:1668)
	at java.base/java.io.ObjectInputStream.defaultReadFields(ObjectInputStream.java:2434)
	at java.base/java.io.ObjectInputStream.readSerialData(ObjectInputStream.java:2328)
	at java.base/java.io.ObjectInputStream.readOrdinaryObject(ObjectInputStream.java:2166)
	at java.base/java.io.ObjectInputStream.readObject0(ObjectInputStream.java:1668)
	at java.base/java.io.ObjectInputStream.defaultReadFields(ObjectInputStream.java:2434)
	at java.base/java.io.ObjectInputStream.readSerialData(ObjectInputStream.java:2328)
	at java.base/java.io.ObjectInputStream.readOrdinaryObject(ObjectInputStream.java:2166)
	at java.base/java.io.ObjectInputStream.readObject0(ObjectInputStream.java:1668)
	at java.base/java.io.ObjectInputStream.defaultReadFields(ObjectInputStream.java:2434)
	at java.base/java.io.ObjectInputStream.readSerialData(ObjectInputStream.java:2328)
	at java.base/java.io.ObjectInputStream.readOrdinaryObject(ObjectInputStream.java:2166)
	at java.base/java.io.ObjectInputStream.readObject0(ObjectInputStream.java:1668)
	at java.base/java.io.ObjectInputStream.readObject(ObjectInputStream.java:482)
	at java.base/java.io.ObjectInputStream.readObject(ObjectInputStream.java:440)
	at org.apache.flink.util.InstantiationUtil.deserializeObject(InstantiationUtil.java:617)
	at org.apache.flink.util.InstantiationUtil.deserializeObject(InstantiationUtil.java:602)
	at org.apache.flink.util.InstantiationUtil.deserializeObject(InstantiationUtil.java:589)
	at org.apache.flink.util.InstantiationUtil.readObjectFromConfig(InstantiationUtil.java:543)
	at org.apache.flink.streaming.api.graph.StreamConfig.getStreamOperatorFactory(StreamConfig.java:383)
```

![](/未更新修改的jar，平台直接异常.png)

## 9 提交到平台后报错

​	更新平台相关包后，提交的任务依然报错，异常堆栈如下：

```java
Caused by: java.lang.ClassCastException: cannot assign instance of com.ververica.cdc.connectors.shaded.org.apache.commons.collections.map.LinkedMap to field com.ververica.cdc.debezium.DebeziumSourceFunction.pendingOffsetsToCommit of type org.apache.commons.collections.map.LinkedMap in instance of com.ververica.cdc.debezium.DebeziumSourceFunction
    
	at java.base/java.io.ObjectStreamClass$FieldReflector.setObjFieldValues(ObjectStreamClass.java:2205)
	at java.base/java.io.ObjectStreamClass$FieldReflector.checkObjectFieldValueTypes(ObjectStreamClass.java:2168)
	at java.base/java.io.ObjectStreamClass.checkObjFieldValueTypes(ObjectStreamClass.java:1422)
	at java.base/java.io.ObjectInputStream.defaultCheckFieldValues(ObjectInputStream.java:2450)
	at java.base/java.io.ObjectInputStream.readSerialData(ObjectInputStream.java:2357)
	at java.base/java.io.ObjectInputStream.readOrdinaryObject(ObjectInputStream.java:2166)
	at java.base/java.io.ObjectInputStream.readObject0(ObjectInputStream.java:1668)
	at java.base/java.io.ObjectInputStream.defaultReadFields(ObjectInputStream.java:2434)
	at java.base/java.io.ObjectInputStream.readSerialData(ObjectInputStream.java:2328)
	at java.base/java.io.ObjectInputStream.readOrdinaryObject(ObjectInputStream.java:2166)
	at java.base/java.io.ObjectInputStream.readObject0(ObjectInputStream.java:1668)
	at java.base/java.io.ObjectInputStream.defaultReadFields(ObjectInputStream.java:2434)
	at java.base/java.io.ObjectInputStream.readSerialData(ObjectInputStream.java:2328)
	at java.base/java.io.ObjectInputStream.readOrdinaryObject(ObjectInputStream.java:2166)
	at java.base/java.io.ObjectInputStream.readObject0(ObjectInputStream.java:1668)
	at java.base/java.io.ObjectInputStream.readObject(ObjectInputStream.java:482)
	at java.base/java.io.ObjectInputStream.readObject(ObjectInputStream.java:440)
	at org.apache.flink.util.InstantiationUtil.deserializeObject(InstantiationUtil.java:617)
	at org.apache.flink.util.InstantiationUtil.deserializeObject(InstantiationUtil.java:602)
	at org.apache.flink.util.InstantiationUtil.deserializeObject(InstantiationUtil.java:589)
	at org.apache.flink.util.InstantiationUtil.readObjectFromConfig(InstantiationUtil.java:543)
	at org.apache.flink.streaming.api.graph.StreamConfig.getStreamOperatorFactory(StreamConfig.java:383)
	... 9 more
```

​		通过上述异常，得到不仅仅是在`flink-connector-cdc`中配置关于`org.apache.commons.collections`的路径变更处理，而且还要再`Flink`仓库配置，==也就是生成的`cdc fat jar`需要将`Flink`相关引用包也打进去==，让`Flink`中关于`org.apache.commons.collections.map.LinkedMap`的引用路径也变更为：`com/ververica/cdc/connectors/shaded/org/apache/commons/collections/map/LinkedMap`，但这样生成的`fat jar`就很庞大，**很明显，这种方式不可取**。

​	分析了下`flink-connector-cdc`==最新版本`3.2`也没有关于`org.apache.commons.collections`的路径变更处理==，猜测是**不需要这部分的处理**的，最终回到最开始的猜想：**一定是项目内某个子项目中的`pom.xml内的Maven Shade Plugin `插件配置影响到了**。

## 10 `oceanbase relocations`配置有误

​	经过排查，最终定位是`flink-sql-connector-oceanbase-cdc`项目内的`pom.xml`有误，详细可参考：https://github.com/apache/flink-cdc/issues/3663。

> ​	这里也就解释了：为什么之前生成的`flink-sql-connector-oracle-cdc fat jar`引用路径正确，但现在又不正确了， 经过多次测试生成的`flink-sql-connector-oracle-cdc fat jar`**是否正确是偶发**的，时而正确时而错误。
>
> ​	原因就是`oceanbase`的配置影响到了全局`Maven Shade Plugin`插件的打包构建。

​	`flink-sql-connector-oceanbase-cdc`其原配置：

```xml
<relocations>
    <!--
        这个配置将匹配所有以 org.apache.commons 开头的包, 并将其重定位到com.ververica.cdc.connectors.shaded.org.apache.commons.
       因此，这个配置会将所有以 org.apache.commons 开头的包都进行重定位，包括 org.apache.commons.collections。
    -->
    <!-- <relocation>-->
    <!--    原包名 -->
    <!--   <pattern>org.apache.commons</pattern>-->
    <!--      变更后的自定义包名 -->
    <!--      <shadedPattern>-->
    <!--         com.ververica.cdc.connectors.shaded.org.apache.commons-->
    <!--      </shadedPattern>-->
    <!--  </relocation>-->
    
    <relocation>
        <pattern>org.apache.commons.lang3</pattern>
        <shadedPattern>
            com.ververica.cdc.connectors.shaded.org.apache.commons.lang3
        </shadedPattern>
    </relocation>
    <relocation>
        <pattern>org.apache.commons.codec</pattern>
        <shadedPattern>
            com.ververica.cdc.connectors.shaded.org.apache.commons.codec
        </shadedPattern>
    </relocation>
```

​	原` org.apache.commons`这个配置将匹配所有以` org.apache.commons` 开头的包, 并将其重定位到`com.ververica.cdc.connectors.shaded.org.apache.commons.`。

​	虽然==项目意图==是处理` com.ververica.cdc.connectors.shaded.org.apache.commons.lang3`和`com.ververica.cdc.connectors.shaded.org.apache.commons.codec`，但着==配置间接==也影响到了其他以` org.apache.commons` 开头的包，例如：`org.apache.commons.collections`。

> 去掉`flink-sql-connector-oracle-cdc`中增加关于`org.apache.commons.collections`引用路径变更配置

​	按上述配置，仅更改`flink-sql-connector-oceanbase-cdc`项目内的`pom.xml`配置即可，更改后重新打包，其关于`org.apache.commons.collections`的引用路径正确，例如：

![](/oceanbase关于linkedmap引用路径.png)



# end 

