/**
 * ================================================
 * 作者：liuyz
 * 审核员：
 * 创建日期：2022-01-04
 * 脚本用途：
 *      数据治理前端脚本，数据治理相关方法都存在这个文件中
 * ================================================
 */
import { showConfirmDialog } from "commons/dialog";
import "css!./data-govern-mgr.css";
import { DatasetDataPackageRowInfo, IDataset, InterActionEvent, IVComponent } from "metadata/metadata-script-api";
import { ExpDialog, ExpDialogValueInfo, IFieldsDataProvider, showExpDialog } from 'commons/exp/expdialog';
import { Component, isEmpty, SZEvent, throwInfo, rc, message, uuid, showProgressDialog, showWaiting, rc_task, assign } from "sys/sys";
import { CustomJs_List, CustomJs_List_Head, getAppPath, showScheduleDialog } from "../../../common/commons.js";
import { ExpVar, IExpDataProvider } from "commons/exp/expcompiler";
import { checkCstmRule, CheckCstmRuleArgs, runCheckTask } from "dg/dgapi";
import { getDwTableDataManager, getQueryManager } from "dw/dwapi";
import { getMetaRepository } from "metadata/metadata";
import { ExpContentType } from 'commons/exp/exped';

/** 有前缀表名的自定义规则字段的dp */
let dgRuleFieldDp: CustomRuleFieldDp;

/**
 * 数据检测基本类，处理执行检测任务
 */
export class SpgDataGovernBasic {
    public CustomActions: any;
    constructor() {
        this.CustomActions = {
            executeCheckTask: this.executeCheckTask.bind(this)
        }
    }

    /**
     * 执行计划任务检测
     * @param event
     * @returns
     */
    public executeCheckTask(event) {
        let params = event.params;
        let taskId = params?.taskId;
        return taskId && showConfirmDialog({
            message: "确认要执行选择的检测任务吗？",
            onok: (event, dialog) => {
                let promise = runCheckTask(taskId, uuid());
                showProgressDialog({
                    caption: "任务执行日志",
                    uuid: taskId,
                    promise: promise,
                    buttons: [{
                        id: "cancel",
                        caption: "关闭"
                    }],
                    logsVisible: true,
                    width: 700,
                    height: 500
                });
                return showWaiting(promise).then(() => {
                    throwInfo("检测完毕");
                    getDwTableDataManager().updateCache([{ // 计划执行完成后，刷新【数据检测任务列表】
                        path: "/sysdata/data/tables/sdi/data-check/SDI_CHECK_TASK_LIST.tbl",
                        type: DataChangeType.refreshall
                    }]);
                })
            }
        })
    }
}

/**
 * 创建检测任务页面
 */
export class SpgDataGovernCreateTask {
    public CustomActions: any;
    private lastSelectedRow: number;
    constructor() {
        this.lastSelectedRow = -1;
        this.CustomActions = {
            button_dg_batchImportStan_new: this.button_dg_batchImportStan_new.bind(this),
            button_dg_createStructureRule: this.button_dg_createStructureRule.bind(this),
            button_dg_check_select_row: this.button_dg_check_select_row.bind(this),
            button_dg_create_check_schedule: this.button_dg_create_check_schedule.bind(this),
            button_dg_fetchExistData: this.button_dg_fetchExistData.bind(this),
            button_dg_createErrTable: this.button_dg_createErrTable.bind(this),
            button_setExpandField: this.button_setExpandField.bind(this)
        }
    }

    /**
     * 给自定义规则列表和内置规则列表界面设置默认勾选项
     * @param event
     * @returns
     */
    public onRender(event: InterActionEvent) {
        let isModify = event.page.getUrlParams().isModify;
        let task_id = event.page.getUrlParams().task_id;
        let isFirstRender;
        event.page.getParameterNames().forEach(param => {
            if (param.name === 'isFirstRender') {
                isFirstRender = param.value;
                return;
            }
        });
        if (isModify == 'true') {
            let comp = event.component;
            if (comp && comp.getId() == 'list2') {//对于内置规则列表控件
                let storeDataset = event.page.getDataset('model3');
                return this.loadExistDataToList(storeDataset, comp, task_id, 'nzgz');
            }
            if (comp && comp.getId() == 'list5') {//自定义规则列表控件
                let storeDataset = event.page.getDataset('model12');
                return this.loadExistDataToList(storeDataset, comp, task_id, 'cstm');
            }
        }
    }

    /**
     * 列表刷新时自动更新勾选值
     * @param event
     */
    public onDidRefresh(event: InterActionEvent) {
        let component = event.component;
        let lastSelectedRow = this.lastSelectedRow;
        let id = component && component.getId();
        if (id === 'list3') {
            let comp = event.page.getComponent('list3').component;
            let len = event.page.getComponent('list3').component.lastCheckedRows.length;
            let new_index;
            if (lastSelectedRow == -1 && len > 0) {
                lastSelectedRow = comp.lastCheckedRows[0];
            }
            let newChecked = [];
            if (len > 1) {
                let index = comp.lastCheckedRows.indexOf(lastSelectedRow);
                new_index = 1 - index;
                newChecked.push(comp.lastCheckedRows[new_index]);
                lastSelectedRow = comp.lastCheckedRows[new_index];
            }
            len > 1 && component.setCheckedDataRows(newChecked);
        }
    }

    public onDidLoadFile(viewer, file) {
        return rc({
            url: `/data/dataGovern/updateBuiltInRule`
        }).then(() => {
            getDwTableDataManager().updateCache([{ path: "/sysdata/data/tables/dg/DG_RULES.tbl", type: "refreshall" }], true);
        })
    }

    /**
     * 引入一批数据标准作为检测资源
     * @param event
     * @returns
     */
    public button_dg_batchImportStan_new(event: InterActionEvent) {
        let data = [];
        let ids = event.params.param7;
        let names = event.params.param1;
        if (ids.length > 1) {
            showConfirmDialog({
                caption: '提示',
                message: `只能勾选一条资源`,
                buttons: ["ok"]
            })
            return false;
        }
        if (ids.length <= 0) {
            showConfirmDialog({
                caption: '提示',
                message: `您还未选择资源`,
                buttons: ["ok"]
            })
            return false;
        }
        for (let i = 0; i < ids.length; i++) {
            data.push({
                id: ids[i],
                caption: names[i]
            })
        }
        return rc({
            url: `/data/dataGovern/batchImportStandard_new`,
            method: 'POST',
            data: {
                datas: JSON.stringify(data)
            }
        }).then((result: any) => {
            let state = result.state;
            let ignoreResInfo = result.ignoreResInfo;
            let addResInfo = result.addResInfo;
            if (ignoreResInfo && ignoreResInfo.length > 0) {
                let info_ignore_arr = [];
                for (let i = 0; i < ignoreResInfo.length; i++) {
                    let info = ignoreResInfo[i];
                    info_ignore_arr.push(`<li>${i + 1}:模型名称：${info.name}，地址:${info.path}</li>`)
                }
            }
            else if (addResInfo && addResInfo.length > 0) {
                //TODO 导入成功提示
            } else {
                showConfirmDialog({
                    caption: '提示',
                    message: `勾选的标准没有关联实例表，无须导入`,
                    buttons: ["ok"]
                })
                return false;
            }
        })
    }

    /**
     * 新建一个检测结构任务，往检测任务规则表中添加值
     * @param event
     * @returns
     */
    public button_dg_createStructureRule(event: InterActionEvent) {
        let data = event.dataRow;
        let task_id = data.input1.value;
        let floatpanel = event.page.getComponent("floatpanel1");
        let floatDetailInfo = floatpanel.getFloatInstances();
        let insertRow = [];
        let bmList = [];
        floatDetailInfo.forEach(info => {
            let dataViewRow = info.getDataViewRow();
            let rowData = dataViewRow.getData();
            let checkElem = rowData.checkbox1.getValue();
            let checkSTD = rowData.checkbox2.getValue();
            let resid = rowData.text23.getValue();
            let bm = rowData.text24.getValue();
            bmList.push(bm);
            if (checkElem == '1') {
                let row = [];
                row.push(task_id);
                row.push(resid);
                row.push('ELEM');
                row.push('STRUCT');
                insertRow.push(row);
            }
            if (checkSTD == '1') {
                let row = [];
                row.push(task_id);
                row.push(resid);
                row.push('STD');
                row.push('STRUCT');
                insertRow.push(row);
            }
        });
        rc({
            url: `/data/dataGovern/updateRuleRef`,
            method: 'POST',
            data: {
                names: bmList.join(",")
            }
        })
        return rc({
            url: getAppPath("/commons/commons.action?method=updateDwTableData"),
            method: "POST",
            data: {
                tablePath: "/sysdata/data/tables/dg/DG_CHECK_TASK_RULES.tbl",
                datapackage: {
                    addRows: [{
                        fieldNames: ['CHECK_TASK_ID', 'DW_TABLE_ID', 'RULE_FROM', 'RULE_ID'],
                        rows: insertRow
                    }]
                }
            }
        }).then(() => {
            return true;
        })
    }

    /**
     * 检查是否勾选了数据
     * @param event
     */
    public button_dg_check_select_row(event: InterActionEvent) {
        let floatpanel = event.page.getComponent("floatpanel1");
        let floatDetailInfo = floatpanel.getFloatInstances();
        let result = false;
        floatDetailInfo.forEach(info => {
            let dataViewRow = info.getDataViewRow();
            let rowData = dataViewRow.getData();
            let checkElem = rowData.checkbox1.getValue();
            let checkSTD = rowData.checkbox2.getValue();
            if (checkElem == '1') {
                result = true;
            }
            if (checkSTD == '1') {
                result = true;
            }
        });
        if (result == false) {
            showConfirmDialog({
                caption: "未勾选数据",
                message: "暂未勾选数据，请求数据后再提交。",
                buttons: ["cancel"]
            })
        }
        return result;
    }

    /**
     * 新建一个检测任务类型的计划任务
     * @param event
     */
    public button_dg_create_check_schedule(event: InterActionEvent) {
        let value = { enable: true, concurrency: false, notifyWhen: "never", retryEnable: true, retryMaxtimes: 1, retryInterval: 1, threadsCount: 2, type: "check" };
        showScheduleDialog(value);
    }

    /**
     * liuyz 20211018
     * 获取当前数据源的最新数据
    */
    public button_dg_fetchExistData(event: InterActionEvent) {
        let params = event.params;
        let taskId = params.taskId;
        let modelId = params.modelId;
        let type = params.type;
        let storeDataset = event.page.getDataset(modelId);
        let comp = event.page.getComponent(type == "nzgz" ? "list2" : "list5");
        let compData = comp.getDataRows();
        let queryInfo: QueryInfo = {
            sources: [{
                id: "model1",
                path: "/sysdata/data/tables/dg/DG_CHECK_TASK_RULES.tbl",
                filter: `model1.CHECK_TASK_ID ='${taskId}' and model1.RULE_FROM ='CSTM'`
            }],
            fields: [{ name: "RULE_ID", exp: "model1.RULE_ID" }, { name: "RULE_FROM", exp: "model1.RULE_FROM" }, { name: "CHECK_TASK_ID", exp: "model1.CHECK_TASK_ID" }, { name: "DW_TABLE_ID", exp: "model1.DW_TABLE_ID" }]
        };
        return getQueryManager().queryData(queryInfo, uuid()).then(result => {
            let datas = result.data;
            if (isEmpty(compData)) {//列表本身没有数据，表示没有任何规则可以选择，此时不进行任何操作
                return;
            }
            let arrInsert: DatasetDataPackageRowInfo[] = [];
            datas.forEach(data => {
                let row: DatasetDataPackageRowInfo = {
                    data: { "RULE_ID": data[0], "RULE_FROM": data[1], "CHECK_TASK_ID": data[2], "DW_TABLE_ID": data[3] }
                }
                arrInsert.push(row);
            });
            storeDataset.saveDraft(arrInsert);
        })
    }

    /**
     * 20211109 liuyz
     * https://jira.succez.com/browse/CSTM-16920 支持创建错误明细表
     */
    public button_dg_createErrTable(event: InterActionEvent) {
        let { dataSource, tableName } = event.params;
        if (!tableName.endsWith(".tbl")) {
            tableName = tableName + ".tbl";
        }
        let meta = {
            name: tableName, ppath: "/sysdata/data/tables/dg/confs", "newborn": true,
            "props": {
                "syncDbTable": true,
                "dwSyncInfos": {},
                "renameFields": {}
            }, "content": {
                "version": "4.0.0",
                "properties": {
                    "modelDataType": "App",
                    "extractDataEnabled": false,
                    "modifyDbTableEnabled": true,
                    "datasource": dataSource,
                    "dbTableName": "",
                    "primaryKeys": ["检测任务编码", "问题表ID", "所属单位", "数据期", "数据标识", "规则代码"]
                },
                "dimensions": [{ "name": "检测任务编码", "dataType": "C", "length": 64, "isDimension": true, "dbfield": "CHECK_TASK_ID", "inputField": "CHECK_TASK_ID" }, { "name": "问题表ID", "dataType": "C", "length": 32, "isDimension": true, "dbfield": "DW_TABLE_ID", "inputField": "DW_TABLE_ID" }, { "name": "所属单位", "dataType": "C", "length": 256, "isDimension": true, "dbfield": "ORG_ID", "inputField": "ORG_ID" }, { "name": "数据期", "dataType": "C", "length": 128, "isDimension": true, "dbfield": "DATA_PERIOD", "inputField": "DATA_PERIOD" }, { "name": "规则代码", "dataType": "C", "length": 200, "isDimension": true, "dbfield": "RULE_ID", "inputField": "RULE_ID" }, { "name": "管辖单位", "dataType": "C", "length": 64, "isDimension": true, "dbfield": "GOV_ID", "inputField": "GOV_ID" }, { "name": "数据标识", "dataType": "C", "length": 256, "isDimension": true, "dbfield": "DATA_ID", "inputField": "DATA_ID" }, { "name": "数据描述", "dataType": "C", "length": 1024, "isDimension": true, "dbfield": "DATA_DESC", "inputField": "DATA_DESC" }, { "name": "错误字段描述", "dataType": "C", "length": 1024, "isDimension": true, "dbfield": "ERR_DESC", "inputField": "ERR_DESC" }, { "name": "规则描述", "dataType": "C", "length": 1024, "isDimension": true, "dbfield": "RULE_DESC", "inputField": "RULE_DESC" }, { "name": "问题修改时间", "dataType": "P", "isDimension": true, "dbfield": "DATA_MODIFY_TIME", "inputField": "DATA_MODIFY_TIME" }, { "name": "最后检测时间", "dataType": "P", "isDimension": true, "dbfield": "LAST_CHECK_TIME", "inputField": "LAST_CHECK_TIME" }, { "name": "是否修正", "dataType": "I", "length": 19, "isDimension": true, "dbfield": "DATA_STATE", "inputField": "DATA_STATE" }, { "name": "问题出现时间", "dataType": "P", "isDimension": true, "dbfield": "DATA_CREATE_TIME", "inputField": "DATA_CREATE_TIME" }, { "name": "首次检测时间", "dataType": "P", "isDimension": true, "dbfield": "FIRST_CHECK_TIME", "inputField": "FIRST_CHECK_TIME" }],
                "measures": []
            }
        };
        return rc_task({
            url: "/api/dw/model/inputData",
            data: {
                meta: meta
            },
            uuid: uuid()
        }).then(() => {
            getMetaRepository().refresh('sysdata', 'data');
        });
    }

    /**
     * 设置扩展字段到控件中
     */
    public button_setExpandField(event: InterActionEvent) {
        let page = event.page;
        let input = page.getComponent("input10");
        let jsComponent = page.getComponent("jsComponent1");
        let value = jsComponent.component.innerComponent.getValue();
        input.setValue(JSON.stringify(value));
    }

    /**
     * 加载数据库已存在的数据
     * 20211214 liuyz
     * 由于内置规则也存储到DG_RULES 实际上也当作自定义规则处理了
     */
    private loadExistDataToList(storeDataset: IDataset, comp: IVComponent, task_id: string, type: string) {
        let tableEditor = comp.component.tableEditor;
        let compData = comp.getDataRows();
        if (!tableEditor.setNewFunction) {//列表初次加载时下载数据库表数据到暂存表中
            let oninlinecompchange_old = tableEditor.oninlinecompchange;
            tableEditor.oninlinecompchange = oninlinecompchange_new.bind(tableEditor);
            tableEditor.setNewFunction = true;
            function oninlinecompchange_new(event, cell, checkComp) {
                let addRows = [];
                let delRows = [];
                let lastCheckIndex = comp.component.lastCheckedRows;
                let lastCompData = comp.getCheckedDataRows();
                oninlinecompchange_old(event, cell, checkComp);
                let newCheckIndex = comp.component.lastCheckedRows;
                let newCompData = comp.getCheckedDataRows();
                if (lastCompData.length == null) {
                    addRows.pushAll(newCompData);
                } else if (newCompData.length == null) {
                    delRows.pushAll(lastCompData);
                } else {
                    let difElement = lastCheckIndex.concat(newCheckIndex).map(function (v, i, arr) {
                        if (arr.indexOf(v) === arr.lastIndexOf(v)) {
                            i < lastCheckIndex.length ? delRows.push(lastCompData[i]) : addRows.push(newCompData[i - lastCheckIndex.length]);
                            return v;
                        }
                    })
                }
                let deleteRows = delRows.map(function (v) {
                    let keys = { "CHECK_TASK_ID": task_id, "RULE_ID": v["RULE_ID"], "DW_TABLE_ID": v["MAIN_TABLE_ID"], "RULE_FROM": 'CSTM' };
                    return keys;
                })
                let newAddRows = addRows.map(function (v) {
                    let keys = { "CHECK_TASK_ID": task_id, "RULE_ID": v["RULE_ID"], "DW_TABLE_ID": v["MAIN_TABLE_ID"], "RULE_FROM": 'CSTM' };
                    return keys;
                })
                storeDataset.insert(newAddRows, true);
                storeDataset.delete(deleteRows, true);
            }
            let queryInfo: QueryInfo = {
                sources: [{
                    id: "model1",
                    path: "/sysdata/data/tables/dg/DG_CHECK_TASK_RULES.tbl",
                    filter: `model1.CHECK_TASK_ID ='${task_id}' and model1.RULE_FROM ='CSTM'`
                }],
                fields: [{ name: "RULE_ID", exp: "model1.RULE_ID" }, { name: "RULE_FROM", exp: "model1.RULE_FROM" }, { name: "CHECK_TASK_ID", exp: "model1.CHECK_TASK_ID" }, { name: "DW_TABLE_ID", exp: "model1.DW_TABLE_ID" }],
            }
            return getQueryManager().queryData(queryInfo, uuid()).then(result => {
                let datas = result.data;
                if (isEmpty(compData)) {//列表本身没有数据，表示没有任何规则可以选择，此时不进行任何操作
                    return;
                }
                let arrInsert: DatasetDataPackageRowInfo[] = [];
                let checkList = [];
                datas.forEach(data => {
                    let row: DatasetDataPackageRowInfo = {
                        data: { "RULE_ID": data[0], "RULE_FROM": data[1], "CHECK_TASK_ID": data[2], "DW_TABLE_ID": data[3] }
                    }
                    arrInsert.push(row);
                    for (let i = 0; i < compData.length; i++) {
                        if (data[0] == compData[i]['RULE_ID']) {
                            checkList.push(i);
                        }
                    }
                });
                storeDataset.saveDraft(arrInsert);
                comp.setCheckedDataRows(checkList);
            })
        } else {
            if (comp.getCheckedDataRows().length > 0) {
                return;
            }
            let storeRows = storeDataset.getRows();
            let checkList = [];
            storeRows.forEach(row => {
                let data = row.getData();
                for (let i = 0; i < compData.length; i++) {
                    if (data['RULE_ID'] == compData[i]['RULE_ID']) {
                        checkList.push(i);
                    }
                }
            });
            comp.setCheckedDataRows(checkList);
            return;
        }
    }
}

/**
 * 检测规则设置页面
 */
export class SpgDataGovernRules {
    public CustomActions: any;
    constructor() {
        this.CustomActions = {
            button_dg_getFilterCondition: this.button_dg_getFilterCondition.bind(this),
            button_dg_customRule: this.button_dg_customRule.bind(this),
            deleteCSTMRule: this.deleteCSTMRule.bind(this),
            button_dg_ruleExp_showExpDialog: this.button_dg_ruleExp_showExpDialog.bind(this),
            button_dg_errExp_showExpDialog: this.button_dg_errExp_showExpDialog.bind(this),
            button_setRuleDesc: this.button_setRuleDesc.bind(this),
            button_dg_setExp: this.button_dg_setExp.bind(this)
        }
    }

    /**
     * 修改左侧树资源节点样式
     * @param event
     */
    public onRender(event: InterActionEvent) {
        let data = event.data;
        let treeData = data && data.dataset;
        if (treeData && Array.isArray(treeData) && treeData.length) {
            treeData.forEach((d) => {
                let fields = d.fields;
                let index = fields && fields.findIndex(f => f === '资源id');
                if (index !== undefined && index !== -1) {
                    let isTable = d.data[index];
                    if (isTable != null) {
                        d.html = `<span class = "custom_check_task_istable"></span>${d.caption}`;
                    } else {
                        d.html = `${d.caption}`;
                    }
                }
            });
        }
    }

    public onDidLoadFile(viewer, file) {
        return rc({
            url: `/data/dataGovern/updateBuiltInRule`
        }).then(() => {
            getDwTableDataManager().updateCache([{ path: "/sysdata/data/tables/dg/DG_RULES.tbl", type: "refreshall" }], true);
        })
    }
    /**
     * 20211118 tuzw
     * 配置检查规则测试数据范围的过滤条件
     * 帖子：https://jira.succez.com/browse/BI-41856
     */
    public button_dg_getFilterCondition(event: InterActionEvent) {
        let page = event.page;
        let params = event.params;
        /** 操作的数据源别名，eg：sourceId：'EDU_F_XSSJ_1' ———— resid：'D8OxNhGgaeCvTtUMICaMoE' */
        let sourceName = params.sourceName;
        function showExp() {
            let filterInputComp = page.getComponent("multipleinput4");
            showExpDialog({
                readonly: false,
                newborn: true,
                defaultTab: "fields",
                expRequired: true,
                buttons: ['ok'],
                expDataProvider: dgRuleFieldDp.getExpDp(),
                fieldPanelImpl: {
                    depends: "dsn/datasourceeditor",
                    implClass: "DataSourceEditor",
                    sourceTheme: SourceTheme.Combobox
                },
                fieldsDataProvider: dgRuleFieldDp,
                caption: " 配置检查规则测试数据范围的过滤条件",
                value: {
                    exp: filterInputComp.getValue()
                },
                needTransformValue: true,
                onok: function (e: SZEvent, dialog: ExpDialog) {
                    filterInputComp.setValue(dialog.getValue().exp);
                }
            });
        }
        if (dgRuleFieldDp == null) {
            dgRuleFieldDp = new CustomRuleFieldDp();
        }
        dgRuleFieldDp.setAssginSource(sourceName);

        if (dgRuleFieldDp.getIsNeedTableName()) {
            dgRuleFieldDp.setIsNeedTableName(false); // 设置表达式不需要前缀表别名
            dgRuleFieldDp.initTableAndFieldsInfo(sourceName).then(() => { // 重新加载字段信息
                showExp();
            });
        } else {
            showExp();
        }
    }

    /**
     * 根据自定义规则进行检测
     * 20210702 liuyz
     * 刷新交互会有问题现在脚本中触发模型刷新
    */
    public button_dg_customRule(event: InterActionEvent) {
        let builder = (<any>event.page).getBuilder();
        let params = event.params;
        let filter = params.filter;
        let models = builder.getModels();
        let needRefreshPath: DwDataChangeEvent[] = [];
        models.forEach(model => {
            let path = model.path;
            let json: DwDataChangeEvent = {
                path,
                type: DataChangeType.refreshall
            };
            needRefreshPath.push(json);
        });
        let rule_id = params["rule_id"];
        let checkCstmRuleArgs: CheckCstmRuleArgs = {
            ruleId: rule_id,
            filter: !!filter ? filter : ""
        }
        let promise = checkCstmRule(checkCstmRuleArgs, uuid());
        showProgressDialog({
            caption: "自定义检测执行日志",
            uuid: rule_id,
            promise: promise,
            buttons: [{
                id: "cancel",
                caption: "关闭"
            }],
            logsVisible: true,
            width: 700,
            height: 500
        });
        return promise.then(() => {
            throwInfo("检测完毕");
            getDwTableDataManager().updateCache(needRefreshPath, true);
        })
    }

    /**
     * 删除自定义规则
     * @param event
     */
    public deleteCSTMRule(event: InterActionEvent) {
        let params = event.params;
        let ruleId = params?.ruleId;
        let ruleName = params?.ruleName;
        let dataset = event.page.getDataset('model3');
        let queryInfo: QueryInfo = {
            sources: [{
                id: "model1",
                path: "/sysdata/data/tables/dg/DG_CHECK_TASK_RULES.tbl"
            }],
            fields: [{
                name: "CHECK_TASK_ID",
                exp: "model1.CHECK_TASK_ID"
            }],
            filter: [{
                exp: `model1.RULE_ID='${ruleId}'`
            }],

        }
        getQueryManager().queryData(queryInfo, uuid()).then(result => {
            let data = result.data;
            if (!isEmpty(data)) {
                showConfirmDialog({
                    caption: '提示',
                    message: `该检测自定义规则有检测任务，不允许删除，请先删除检测任务再删除自定义规则`,
                    buttons: ['ok']
                });
            } else {
                showConfirmDialog({
                    caption: '确认删除',
                    message: `确认要删除${ruleName}这个自定义规则吗？`,
                    onok: () => {
                        rc({
                            url: `/data/dataGovern/deleteCstmRule`,
                            method: "POST",
                            data: {
                                ruleId
                            }
                        }).then((result) => {
                            dataset.refresh({ notifyChange: true, force: true });
                            throwInfo(result.message);
                        });
                    }
                });
            }
        })
    }

    /**
     * 20211101 liuyz
     * 展示自定义规则表达式对话框
     */
    public button_dg_ruleExp_showExpDialog(event: InterActionEvent) {
        let page = event.page;
        let params = event.params;
        /** 操作的数据源别名，eg：sourceId：'EDU_F_XSSJ_1' ———— resid：'D8OxNhGgaeCvTtUMICaMoE' */
        let currentSource = params.currentSource;
        let ruleExp = params.rule_exp;
        function showExp() {
            const displayInput = page.getComponent('multipleinput1');
            const trueExpInput = page.getComponent('input12');

            showExpDialog({
                readonly: false,
                newborn: true,
                defaultTab: "fields",
                expRequired: true,
                buttons: ['ok'],
                expDataProvider: dgRuleFieldDp.getExpDp(),
                fieldPanelImpl: {
                    depends: "dsn/datasourceeditor",
                    implClass: "DataSourceEditor",
                    sourceTheme: SourceTheme.Combobox
                },
                fieldsDataProvider: dgRuleFieldDp,
                caption: "设置自定义规则表达式",
                value: {
                    exp: ruleExp
                },
                needTransformValue: true,
                onok: function (e: SZEvent, dialog: ExpDialog) {
                    displayInput.setValue(dialog.exped.originValue);
                    page.setParameter("rule_exp", dialog.getValue().exp);
                }
            });
        }
        if (dgRuleFieldDp == null) {
            dgRuleFieldDp = new CustomRuleFieldDp();
        }
        dgRuleFieldDp.setcurrentSource(currentSource);
        dgRuleFieldDp.setAssginSource(null);

        if (!dgRuleFieldDp.getIsNeedTableName()) {
            dgRuleFieldDp.setIsNeedTableName(true);  // 设置表达式需要前缀表别名
            dgRuleFieldDp.initTableAndFieldsInfo().then(() => { // 重新加载字段信息
                showExp();
            });
        } else {
            showExp();
        }
    }

    /**
     * 20211101 liuyz
     * 设置错误显示表达式对话框
     */
    public button_dg_errExp_showExpDialog(event: InterActionEvent) {
        let page = event.page;
        let params = event.params;
        let sourceName = params.sourceId;
        let errInputCompId = params.errInputCompId;
        function showExp() {
            let errInputComp = page.getComponent(errInputCompId);
            showExpDialog({
                readonly: false,
                newborn: true,
                defaultTab: "fields",
                expRequired: true,
                buttons: ['ok'],
                expDataProvider: dgRuleFieldDp.getExpDp(),
                contentType: ExpContentType.MACRO,
                fieldPanelImpl: {
                    depends: "dsn/datasourceeditor",
                    implClass: "DataSourceEditor",
                    sourceTheme: SourceTheme.Combobox
                },
                fieldsDataProvider: dgRuleFieldDp,
                caption: "设置错误显示表达式",
                value: {
                    exp: errInputComp.getValue()
                },
                needTransformValue: true,
                onok: function (e: SZEvent, dialog: ExpDialog) {
                    errInputComp.setValue(dialog.getValue().exp);
                }
            });
        }
        if (dgRuleFieldDp == null) {
            dgRuleFieldDp = new CustomRuleFieldDp();
        }
        dgRuleFieldDp.setAssginSource(sourceName);

        if (!dgRuleFieldDp.getIsNeedTableName()) {
            dgRuleFieldDp.setIsNeedTableName(true);  // 设置表达式需要前缀表别名
            dgRuleFieldDp.initTableAndFieldsInfo().then(() => { // 重新加载字段信息
                showExp();
            });
        } else {
            showExp();
        }
    }

    /**
     * 设置规则详情描述到控件中
     */
    public button_setRuleDesc(event: InterActionEvent) {
        let page = event.page;
        let input = page.getComponent("input13");
        let jsComponent = page.getComponent("jsComponent1");
        let value = jsComponent.component.innerComponent.getValue();
        input.setValue(JSON.stringify(value));
    }

    /**
     * 20220123 tuzw
     * 检测资源管理-资源属性-新增自定义数据摘要表达式
     * 帖子地址：https://jira.succez.com/browse/CSTM-17728?page=com.atlassian.jira.plugin.system.issuetabpanels%3Acomment-tabpanel&focusedCommentId=193382#comment-193382
     * @param sourceName 操作的数据源别名，eg：sourceId：'EDU_F_XSSJ_1' —— resid：'D8OxNhGgaeCvTtUMICaMoE
     * @param isNeedPrefix 是否需要表达式前缀
     * @param expType 表达式类型，默认：expression，支持：auto、expression、macro、tex
     * @param outPutComponentId 设置的表达式值输出到那个组件中，eg：multipleinput1
     */
    public button_dg_setExp(event: InterActionEvent) {
        let page = event.page;
        let params = event.params;
        let sourceName = params.sourceName;
        let isNeedPrefix = params.isNeedPrefix;
        let outPutComponentId = params.outPutComponentId;
        let expPrefixIsNeed: boolean = true; // 默认：true
        if (!!isNeedPrefix && isNeedPrefix == "false") {
            expPrefixIsNeed = false;
        }
        let expType: string = params.expType;
        function showExp() {
            let filterInputComp = page.getComponent(outPutComponentId);
            showExpDialog({
                readonly: false,
                newborn: true,
                defaultTab: "fields",
                expRequired: true,
                buttons: ['ok'],
                expDataProvider: dgRuleFieldDp.getExpDp(),
                descValidateArgs: {
                    required: true,
                },
                contentType: getExpContentType(expType),
                fieldPanelImpl: {
                    depends: "dsn/datasourceeditor",
                    implClass: "DataSourceEditor",
                    sourceTheme: SourceTheme.Combobox
                },
                fieldsDataProvider: dgRuleFieldDp,
                caption: " 配置检查规则测试数据范围的过滤条件",
                value: {
                    exp: filterInputComp.getValue()
                },
                needTransformValue: true,
                onok: function (e: SZEvent, dialog: ExpDialog) {
                    filterInputComp.setValue(dialog.getValue().exp);
                }
            });
        }
        if (dgRuleFieldDp == null) {
            dgRuleFieldDp = new CustomRuleFieldDp();
        }
        dgRuleFieldDp.setAssginSource(sourceName);

        if (dgRuleFieldDp.getIsNeedTableName()) {
            dgRuleFieldDp.setIsNeedTableName(expPrefixIsNeed); // 设置表达式不需要前缀表别名
            dgRuleFieldDp.initTableAndFieldsInfo(sourceName).then(() => { // 重新加载字段信息
                showExp();
            });
        } else {
            showExp();
        }
    }
}

/**
 * 获取【表达式编辑框的编辑内容或编辑的模式】
 * @param expType 表达式类型
 * @return ExpContentType
 */
function getExpContentType(expType: string): ExpContentType {
    switch (expType) {
        case "auto": return ExpContentType.AUTO;
            break;
        case "expression": return ExpContentType.EXPRESSION; // name=="张三"
            break;
        case "macro": return ExpContentType.MACRO; // ${name=="张三"}
            break;
        case "text": return ExpContentType.TEXT;
            break;
    }
    return ExpContentType.EXPRESSION;
}

/**
 * 检测任务列表界面
 */
export class SpgDataGovernTaskList extends SpgDataGovernBasic {
    public CustomActions: any;
    constructor() {
        super();
        this.CustomActions = assign(this.CustomActions, {
            button_dg_deleteCheckTask: this.button_dg_deleteCheckTask.bind(this)
        })
    }
    /**
     * 删除某个检测任务
     */
    public button_dg_deleteCheckTask(event: InterActionEvent) {
        let params = event.params;
        let task_id = params?.taskId;
        let task_name = params?.taskName;
        let dataset = event.page.getDatasets()[0];
        showConfirmDialog({
            caption: '确认删除',
            message: `确认要删除${task_name}这个检测任务吗？`,
            onok: () => {
                rc({
                    url: `/data/dataGovern/deleteTask`,
                    method: 'POST',
                    data: {
                        task_id: task_id
                    }
                }).then(() => {
                    dataset.refresh({ notifyChange: true, force: true });
                    throwInfo(`${task_name}该检测任务已经删除`);
                })
            }
        })
    }
}

const HEADER_PARAMS: CustomJs_List_Head[] = [{
    id: "name",
    caption: "标题"
}, {
    id: "desc",
    caption: "规则详情描述"
}, {
    id: "dimPath",
    caption: "关联维表",
    type: "resSelect",
    resArgs: {
        id: "dg_ruleDesc",
        caption: "选择关联的维表",
        rootPath: "/sdi/data/tables",
        returnTypes: ["tbl"],
        enableEmptySelect: true,
    }
}];
/**
 * 用于定义自定义规则的详情描述
 */
export class CustomJs_RuleDesc extends Component {
    private customJsList: CustomJs_List;
    protected _init(args: any): HTMLElement {
        let domBase = super._init(args);
        let customJsList = this.customJsList = new CustomJs_List({
            headerParams: HEADER_PARAMS,
            domBase: domBase,
            value: args.value
        })
        return domBase;
    }

    public _reInit(args: any) {
        this.customJsList._reInit(args);
    }

    public getValue(): JSONObject {
        return this.customJsList.getValue();
    }

    public dispose() {
        super.dispose();
    }

}

/**
 * 扩展字段定义页面
 */
export class CustomJs_ExpandField extends Component {
    private customJsList: CustomJs_List;
    protected _init(args: any): HTMLElement {
        let domBase = super._init(args);
        let value = args.value;
        let headers: CustomJs_List_Head[] = [{
            id: "fieldName",
            caption: "物理字段名"
        }, {
            id: "name",
            caption: "模型字段名称"
        }, {
            id: "exp",
            caption: "表达式"
        }, {
            id: "dataType",
            caption: "字段类型",
            type: "combobox",
            comboxArgs: [{
                value: "C", caption: "字符型"
            }, {
                value: "N", caption: "浮点型"
            }, {
                value: "I", caption: "整型"
            }, {
                value: "D", caption: "日期型"
            }, {
                value: "T", caption: "时间型"
            }, {
                value: "P", caption: "时间戳类型"
            }, {
                value: "M", caption: "Clob类型"
            }, {
                value: "X", caption: "Blob类型"
            }]
        }, {
            id: "length",
            caption: "字段长度"
        }];
        let customJsList = this.customJsList = new CustomJs_List({
            domBase: domBase,
            value: value,
            headerParams: headers
        })
        return domBase;
    }
    public _reInit(args: any) {
        this.customJsList._reInit(args);
    }

    public getValue(): JSONObject {
        return this.customJsList.getValue();
    }

    public dispose() {
        super.dispose();
    }
}

const DIMENSION_VAR_ICON: { [key: string]: string } = {
    "C": "icon-string-dim",
    "N": "icon-float-dim",
    "I": "icon-int-dim",
    "D": "icon-date-dim",
    "T": "icon-date-dim",
    "P": "icon-date-dim",
    "M": "icon-clob-dim",
    "X": "icon-blob-dim"
};

const MEASURE_VAR_ICON: { [key: string]: string } = {
    "C": "icon-string",
    "N": "icon-float",
    "I": "icon-int",
    "D": "icon-date",
    "T": "icon-date",
    "P": "icon-date",
    "M": "icon-clob",
    "X": "icon-blob"
};

interface CustomFieldItem {
    id: string;
    caption: string;//字段标题
    pickValue: string[];//选择值
    value: string;
    dataType: string;//字段类型
    dbfield: string;//物理字段名称
    isDimension: boolean;//是否为维度
    isField: boolean;//是否为字段
    leaf: boolean;//是否为叶子节点
    icon: string;//图标
    name: string;
    exp?: string;
    desc?: string;
}

/**
 * 定制的表达式dp
 * liuyz 20210721
 */
export class CustomExpDp implements IExpDataProvider {
    private tableAndFieldsExpVar: ExpVar[];
    private fields: string[];
    private tableFieleMap: JSONObject;//数据表和模型对应关系
    public getVarsByParent(parentVar?: IExpVar): Array<IExpVar> {
        if (parentVar != null && !isEmpty(this.tableFieleMap)) {
            return this.tableFieleMap[parentVar.getName()];
        }
        return this.tableAndFieldsExpVar;
    }

    public setTable(tableInfo) {
        let expVar = new ExpVar(tableInfo);
        this.tableAndFieldsExpVar == null && (this.tableAndFieldsExpVar = []);
        this.tableAndFieldsExpVar.push(expVar);
    }

    /**
     * 设置字段表达式
     * 字段的表达式需要记录下对应的模型id,方便后面进行获取
     * @param fieldInfo
     * @param sourceId
     */
    public setField(fieldInfo, sourceId?: string) {
        let expVar = new ExpVar(fieldInfo);
        this.tableAndFieldsExpVar == null && (this.tableAndFieldsExpVar = []);
        this.fields == null && (this.fields = []);
        this.tableAndFieldsExpVar.push(expVar);
        this.fields.push(fieldInfo.caption);
        if (!isEmpty(sourceId)) {
            isEmpty(this.tableFieleMap) && (this.tableFieleMap = {});
            isEmpty(this.tableFieleMap[sourceId]) && (this.tableFieleMap[sourceId] = []);
            this.tableFieleMap[sourceId].push(expVar);
        }
    }

    public validateFieldName(name: string) {
        if (!name) {
            return message("dataflow.fieldname.null");
        }
        if (name && name.match(/[`"\[\]]+/)) {
            return message("dataflow.fieldname.existInvalidChar");
        }
        let upperName = name.toUpperCase();
        // 可以修改名称的大小写
        if (this.fields.indexOf(upperName) != -1) {
            return message("dataflow.fieldname.repeat");
        }
        return null;
    }
}

/**
 * 20211015 kongwq
 * 创建自定义规则字段的dp
 */
export class CustomRuleFieldDp implements IFieldsDataProvider {
    private tableInfos: { id: string, desc?: string, isCurrentDataSource?: boolean }[];
    private ruleTableInfo: Record<string, string[]>; // 表别名和id对应对象
    private ruleTabelField: Record<string, CustomFieldItem[]>
    private expDp: CustomExpDp;
    /** 设置指定的数据源 */
    private assginSourceId: string;
    /** 当前查看的数据源 */
    private currentSourceId: string;
    /** 设置表达式是否需要携带表名 */
    private isNeedTableName: boolean;

    constructor() {
        this.tableInfos = [];
        this.ruleTableInfo = {};
        this.ruleTabelField = {};
        this.expDp = new CustomExpDp();
        this.isNeedTableName = true;
    }

    /**
     * 获取规则表id和别名
     * @returns
     */
    public fetchDataSources(): Promise<{ id: string, desc?: string, isCurrentDataSource?: boolean }[]> {
        if (!isEmpty(this.tableInfos)) {
            if (!isEmpty(this.assginSourceId)) {
                return Promise.resolve(this.tableInfos.filter(t => { return t.id == this.assginSourceId }));
            } else {
                /**
                 * 遍历整个资源记录表，根据currentSourceId（id）找到当前需要处理的资源信息，使之成为：激活状态，eg：
                 * tableInfos {
                 * 		0: {id: 'EDU_F_XSSJ_1', isCurrentDataSource: false, desc: '全国近十年学生数据'}
                        1: {id: 'F_JJHK_CON_1', isCurrentDataSource: false, desc: '经济户口表——关联模型'}
                   }
                 */
                return Promise.resolve(this.tableInfos.map(table => {
                    if (table.id == this.currentSourceId) {
                        table.isCurrentDataSource = true;
                    }
                    return table;
                }));
            }
        }
        // 若当前tableInfos为null，则自动发请求去请求规则表信息数据
        return rc({
            url: `/data/dataGovern/getRuleTableInfos`,
        }).then(results => {
            let tableInfos = this.tableInfos = [];
            results.forEach((result: Array<string>) => {
                if (result[2] === null) {
                    result[2] = result[1];
                }
                this.ruleTableInfo[result[1]] = [];
                this.ruleTableInfo[result[1]].push(result[0]);
                this.ruleTableInfo[result[1]].push(result[2])
                tableInfos.push({
                    id: result[1],
                    isCurrentDataSource: false,
                    desc: result[2]
                })
                this.expDp.setTable({ name: result[1], caption: result[2] });
            });
            if (!isEmpty(this.assginSourceId)) {
                return tableInfos.filter(t => { return t.id == this.assginSourceId });
            } else {
                return tableInfos.map(t => {
                    if (t.id == this.currentSourceId) {
                        t.isCurrentDataSource = true;
                    }
                    return t;
                });
            }
        });
    }

    /**
     * 请求当前指定资源的字段集
     * @sourceId 资源别名
     */
    public fetchFields(sourceId: string, fetchArgs: { [propName: string]: any; isDimension?: boolean; }): JSONObject {
        if (this.ruleTabelField[sourceId]) {
            return this.ruleTabelField[sourceId].filter(f => { return f.isDimension == fetchArgs.isDimension });
        } else {
            return rc({
                url: `/data/dataGovern/getFieldInfos`,
                data: {
                    /** 模型表的resid */
                    tableId: this.ruleTableInfo[sourceId][0],
                    /** 是否为维度字段 */
                    isDimension: fetchArgs.isDimension
                }
            }).then(results => {
                const fieldItems: CustomFieldItem[] = [];
                if (!this.ruleTabelField[sourceId]) {
                    this.ruleTabelField[sourceId] = [];
                }
                results.forEach((result: DwTableFieldInfo) => {
                    let customFieldItem: CustomFieldItem = this.setFieldInfo(result, sourceId, fetchArgs.isDimension);
                    fieldItems.push(customFieldItem);
                });
                return fieldItems;
            })
        }
    }

    public searchFields(sourceId: string, searchArgs: { [propName: string]: any; keyword?: string; isDimension?: boolean; }): Promise<JSONObject[]> {
        const key = searchArgs.keyword.toUpperCase();
        const fieldItems: CustomFieldItem[] = [];
        this.ruleTabelField[sourceId].forEach((item: CustomFieldItem) => {
            if ((item.name.indexOf(key) != -1 || item.caption.indexOf(key) != -1) && item.isDimension == searchArgs.isDimension) {
                fieldItems.push(item);
            }
        })
        return Promise.resolve(fieldItems);
    }

    public getExpDp() {
        return this.expDp;
    }

    /**
     * 设置表达式是否需要携带表名
     * @params isNeedTableName：true 需要表前缀名，false，不需要
     */
    public setIsNeedTableName(isNeedTableName: boolean): void {
        this.tableInfos = [];
        this.ruleTableInfo = {};
        this.ruleTabelField = {};
        this.expDp = new CustomExpDp();
        this.isNeedTableName = isNeedTableName;
    }

    /**
     * 获取表达式是否需要携带表名
     * @return boolean
     */
    public getIsNeedTableName(): boolean {
        return this.isNeedTableName;
    }

    /**
     * 设置指定要查看的数据源
     * @param assginSourceId  指定数据源在数据规则表中的别名
     * @param tableInfo 指定表的resid和desc（描述
     */
    public setAssginSource(assginSourceId: string, tableInfo?: { resid: string, desc: string }) {
        this.assginSourceId = assginSourceId;
        if (!isEmpty(tableInfo) && !this.ruleTableInfo[assginSourceId]) {
            let { resid, desc } = tableInfo;
            this.tableInfos.push({ id: assginSourceId, desc: desc });
            this.ruleTableInfo[assginSourceId] = [resid, desc];
            this.expDp.setTable({ name: assginSourceId, caption: desc })
        }
    }

    /**
     * 设置当前要查看的数据源
     * @param currentSourceId
     */
    public setcurrentSource(currentSourceId: string) {
        this.currentSourceId = currentSourceId;
    }

    /**
     * 初始化字段信息和
     * @param currentSourceId：指定初始化数据库别名
     */
    public initTableAndFieldsInfo(currentSourceId?: string) {
        return this.fetchDataSources().then(sources => {
            return rc({
                url: `/data/dataGovern/getAllTableFieldInfos`
            }).then(results => {
                let sources = Object.keys(results);
                // 初始化指定的数据库别名
                if (!!currentSourceId) {
                    if (!this.ruleTabelField[currentSourceId]) {
                        this.ruleTabelField[currentSourceId] = [];
                    }
                    let data = results[currentSourceId];
                    data.dimensions.forEach((result: DwTableFieldInfo) => {
                        this.setFieldInfo(result, currentSourceId, true)
                    });
                    data.measures.forEach((result: DwTableFieldInfo) => {
                        this.setFieldInfo(result, currentSourceId, false)
                    });
                } else {
                    // 将其规则库中的所有数据库信息都初始化
                    for (let i = 0; i < sources.length; i++) {
                        const sourceId = sources[i];
                        if (!this.ruleTabelField[sourceId]) {
                            this.ruleTabelField[sourceId] = [];
                        }
                        let data = results[sourceId];
                        data.dimensions.forEach((result: DwTableFieldInfo) => {
                            this.setFieldInfo(result, sourceId, true)
                        });
                        data.measures.forEach((result: DwTableFieldInfo) => {
                            this.setFieldInfo(result, sourceId, false)
                        });
                    }
                }
            })
        })
    }

    private setFieldInfo(fieldInfo: DwTableFieldInfo, sourceId: string, isDimensions: boolean): CustomFieldItem {
        const name = fieldInfo.name.toUpperCase();
        const dbFieldName = fieldInfo.dbFieldName;
        const icon = isDimensions ? DIMENSION_VAR_ICON[fieldInfo.dataType] : MEASURE_VAR_ICON[fieldInfo.dataType];
        const customFieldItem: CustomFieldItem = {
            id: dbFieldName,
            caption: name,
            name: dbFieldName,
            desc: name,
            pickValue: this.getIsNeedTableName() ? [sourceId, dbFieldName] : [dbFieldName],
            value: name,
            dataType: fieldInfo.dataType,
            dbfield: dbFieldName,
            isDimension: fieldInfo.innerWrappedObject.dimension,
            isField: true,
            leaf: true,
            icon,
            exp: this.getIsNeedTableName() ? `${sourceId}.${dbFieldName}` : `${dbFieldName}`
        };
        this.ruleTabelField[sourceId].push(customFieldItem);
        this.expDp.setField(customFieldItem, sourceId);

        return customFieldItem;
    }
}