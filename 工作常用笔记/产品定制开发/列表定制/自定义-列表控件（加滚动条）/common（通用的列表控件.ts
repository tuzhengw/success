import { Button, Combobox, Edit, Link, Checkbox } from "commons/basic";
import { Dialog } from "commons/dialog";
import { getCurrentUser, getMetaRepository, IMetaFileViewer, FileInfoOrPathOrId, getFileImplClass, querySchedules } from "metadata/metadata";
import { showResourceDialog } from "metadata/metamgr";
import { getScheduleMgr, ScheduleEditor } from "schedule/schedulemgr";
import { assign, Component, ComponentArgs, ctx, ctxIf, getBaseName, IImportComponentInfo, isEmpty, message, rc, showDialog, showErrorMessage, SZEvent } from "sys/sys";

export interface CustomJs_List_Head {
	/** 列id */
	id: string;
	/** 列标题名称，如果不填写默认为id */
	caption?: string;
	/** 是否隐藏列 */
	visible?: boolean;
	/** 控件类型，如果不填写则默认为输入框控件，目前支持：combobox（多选框），resselect */
	type?: string;
	/** 下拉框是否支持多选 */
	isMultipleSelect?: boolean,
	/** 下拉框选项（数组） */
	comboxArgs?: {
		value: string, caption: string
	}[],
	resArgs?: { id: string, caption?: string, rootPath?: string, returnTypes?: string[], enableEmptySelect?: boolean }
}

/**
 * 定制列表初始化参数配置
 */
export interface CustomJs_ListArgs extends ComponentArgs {
	headerParams: CustomJs_List_Head[];//表头参数，最后的返回值也依赖这个值
	value?: string;
	/** 是否需要底部【新增、删除】功能 */
	isNeedAddDelFunction?: boolean;
	/** 是否需要初始一无数据行 */
	isNeedInitOneRow?: boolean;
	/** 外部JS列表所在面板的滚动条对象 */
	outsideListPanelScrollBar?: MacScrollBar;
	/** 触发新增/删除行后的点击事件 */
	onClickAdjustRowEvent?: () => void;
}

/**
 * 实现一个通用的列表控件，可以编辑该列表每一行的数据，最后返回的格式为数组
 */
export class CustomJs_List extends Component {
	private headerParams: CustomJs_List_Head[];
	private value: JSONObject;
	private table: HTMLTableElement;
	private rows;
	private addRowButton: Button;
	private delRowButton: Button;

	/** 外部JS列表所在面板的滚动条对象 */
	public outsideListPanelScrollBar?: MacScrollBar;
	/** 触发新增/删除行后的点击事件 */
	public onClickAdjustRowEvent: () => void;

	constructor(args: CustomJs_ListArgs) {
		super(args);
		this.parseParamSetting(args);
	}

	/**
	 * 解析外部配置的参数
	 * @param args 外部配置的列表参数
	 */
	private parseParamSetting(args: CustomJs_ListArgs): void {
		if (!isEmpty(args.onClickAdjustRowEvent)) {
			this.onClickAdjustRowEvent = args.onClickAdjustRowEvent;
		}
		if (!isEmpty(args.outsideListPanelScrollBar)) {
			this.outsideListPanelScrollBar = args.outsideListPanelScrollBar as MacScrollBar;
		}
	}

	protected _init(args: CustomJs_ListArgs): HTMLElement {
		let domBase = super._init(args);
		this.init_ListDom(domBase, args);
		return domBase;
	}

	/**
	 * 初始化列表DOM
	 * @param domBase 当前JS控件DOM
	 * @param args 外部配置的列表参数
	 */
	private init_ListDom(domBase: HTMLElement, args: CustomJs_ListArgs): void {
		let table = this.table = document.createElement("table");
		/**
		 * 20220214 tuzw
		 * 		原写法：let isNeedInitOneRow = !!args.isNeedInitOneRow ? args.isNeedInitOneRow : true，
		 * 会导致：若参数值为：false时，会将结果转换为：true，导致显示结果与预期效果相反
		 */
		let isNeedInitOneRow: boolean = isEmpty(args.isNeedInitOneRow) ? true : !!args.isNeedInitOneRow;
		let isNeedAddDelFunction: boolean = isEmpty(args.isNeedAddDelFunction) ? true : !!args.isNeedAddDelFunction;

		table.classList.add(isEmpty(args.className) ? "customJs_list_table" : args.className as string);
		let rowValues = isEmpty(args.value) ? [] : args.value;
		if (typeof args.value == 'string') {
			rowValues = JSON.parse(args.value);
		}

		let tbody = table.createTBody();
		let headRow = tbody.insertRow();
		headRow.classList.add("customJs_list_header");
		table.contentEditable = "true";
		headRow.contentEditable = "false";
		let headerParams = this.headerParams = args.headerParams;
		for (let i = 0; i < headerParams.length; i++) {
			const element = headerParams[i];
			let cell: HTMLTableDataCellElement = headRow.insertCell();
			cell.innerText = element.caption ? element.caption : element.id;
			cell.setAttribute("id", element.id);
			if (element.visible != undefined && element.visible != null && !element.visible) {
				cell.classList.add("list_colums_noDisplay");
			}
		}
		domBase.appendChild(table);
		this.rows = [];
		if (isEmpty(rowValues) && isNeedInitOneRow) {
			this.doAddRow();
		} else {
			rowValues.forEach(v => this.doAddRow(v));
		}
		if (isNeedAddDelFunction) {
			this.addListOptionFunction(domBase);
		}
	}

	/**
	 * 给列表增加：添加行和删除行功能
	 * @param 当前JS控件DOM
	 */
	private addListOptionFunction(domBase: HTMLElement): void {
		this.addRowButton = new Button({
			domParent: domBase,
			icon: 'icon-add',
			caption: "添加行",
			layoutTheme: 'smalldefbtn',
			className: 'customJs_list_addrow_button',
			onclick: this.doAddRow.bind(this)
		})
		this.delRowButton = new Button({
			domParent: domBase,
			icon: 'icon-deletedata',
			caption: "删除行",
			layoutTheme: 'smalldefbtn',
			className: 'customJs_list_delrow_button',
			onclick: this.doDelRow.bind(this)
		});
	}

	/**
	 * 重新渲染列表数据
	 * 20220311 tuzw
	 * 问题:
	 * （1） 列表初次执行init()方法会自动往界面上添加一空白行
	 * （2） _reInit()方法会先删除上一次渲染的数据，然后再根据当前最新的value值进行渲染列表
	 * （3） 原逻辑是根据列表录入的数据删除，但若录入的某行数据为空，则不会被记录，导致根据录入的行进行删除时，会漏掉没有录入数据的行
	 * 解决办法：
	 * （1） 删除逻辑改为根据列表的实际行数来进行删除，根据DOM的行数删除，而不是实际录入不为空的数据行删除
	 */
	public _reInit(args: any) {
		let len = this.table.rows.length;
		for (let i = 1; i < len + 1; i++) {
			this.doDelRow();
		}
		let value: JSONObject = isEmpty(args.value) ? [] : JSON.parse(args.value);
		let isNeedInitOneRow: boolean = isEmpty(args.isNeedInitOneRow)
			? true
			: !!args.isNeedInitOneRow;
		if (isEmpty(value) && this.rows.length == 0 && isNeedInitOneRow) { // 若列表没有行，重新打开则初始化一行
			this.doAddRow();
		} else {
			for (let i = 0; i < value.length; i++) {
				this.doAddRow(value[i]);
			}
		}
	}

	/**
	 * 增加行
	 * @param rowValue 当前行数据，格式：{ 列号：值 }，eg：{ "1": "1001", "2" : "张三" }
	 * @param headerParams 列标题数组，可包含当前列下拉框选项
	 * 注意：
	 * （1）不指定headerParams，当前列若设置下拉框，则【整列下拉框】以【标题列】设置的【下拉框选项】 为主
	 * （2）指定headerParams，可指定【每行】下拉框选项值
	 */
	public doAddRow(rowValue?: JSONObject, headerParams?: CustomJs_List_Head[]): void {
		let row = new CustomJs_Row({ table: this.table, value: rowValue, headerParams: this.headerParams });
		this.rows.push(row);
		this.afterRowOptionEvents();
	}

	private doDelRow(): void {
		let tbody = this.table.tBodies[0];
		let rows = this.table.rows;
		if (rows.length > 1) { // 根据表的行数来校验, 而不是获取实际数据的行数(this.rows)
			tbody.deleteRow(rows.length - 1);
			this.rows.remove(this.rows.length - 1);
		}
		this.afterRowOptionEvents();
	}

	/**
	 * 列表新增、删除行后的操作
	 * 
	 * 20220624 tuzw
	   * 由于外部列表新增删除后，所在面板的滚动条无法重新计算当前面板的高度，通用列表对象获取外部列表所在面板的滚动条，然后去触发滚动条计算方法
	 */
	public afterRowOptionEvents(): void {
		if (!isEmpty(this.onClickAdjustRowEvent)) {
			this.onClickAdjustRowEvent();
		}
		if (!isEmpty(this.outsideListPanelScrollBar)) {
			(this.outsideListPanelScrollBar as MacScrollBar).updateScrollBar();
		}
	}

	public getValue(): JSONObject {
		let value = [];
		let len = this.rows.length;
		for (let i = 0; i < len; i++) {
			const row = this.rows[i];
			let v = row.getValue();
			if (!isEmpty(v)) {
				value.push(v);
			}
		}
		return value;
	}

	public dispose() {
		super.dispose();
	}
}

export class CustomJs_Row {
	private table: HTMLTableElement;
	private headerParams: CustomJs_List_Head[];
	private value: JSONObject;
	private row: HTMLTableRowElement;
	/** 用于获取表行数据 */
	private comps: any[];
	constructor(args) {
		this.table = args.table;
		this.value = args.value;
		this.headerParams = args.headerParams;
		this.comps = [];
		this.init();
	}

	protected init() {
		let table = this.table;
		let tbody = table.tBodies[0];
		let row = this.row = tbody.insertRow();
		let v = this.value;
		for (let i = 0; i < this.headerParams.length; i++) {
			const element = this.headerParams[i];
			let cell = row.insertCell();
			if (element.visible != undefined && element.visible != null && !element.visible) {
				cell.classList.add("list_colums_noDisplay");
			}
			let id = element.id;
			let value = v && v[id]; // v： "rowUuid": "2489025822528952852..."
			cell.setAttribute("id", element.id);
			let type = element.type;
			switch (type) {
				case "combobox": // 多选框
					cell.contentEditable = "false";
					let comboboxDiv = document.createElement("div");
					comboboxDiv.classList.add("customJs_list_combox");
					cell.appendChild(comboboxDiv);
					let items = element.comboxArgs;
					let combobox = new Combobox({
						multipleSelect: !!element.isMultipleSelect ? element.isMultipleSelect : false, // 多选
						selectAllEnabled: !!element.isMultipleSelect ? element.isMultipleSelect : false, // 是否允许全选
						checkedCountVisible: !!element.isMultipleSelect ? element.isMultipleSelect : false, // 是否显示选择数量
						checkboxVisible: !!element.isMultipleSelect ? element.isMultipleSelect : false, // 是否在选项前显示勾选框
						cleanIconVisible: !!element.isMultipleSelect ? element.isMultipleSelect : false, // 是否显示清空图标
						noOptionText: '无选项',
						id: id,
						type: "list",
						items: items,
						domParent: comboboxDiv
					});
					this.comps.push(combobox);
					!isEmpty(value) && combobox.setValue(value);
					break;
				case "resSelect":
					cell.contentEditable = "false";
					let resSelectDiv = document.createElement("div");
					resSelectDiv.classList.add("customJs_list_resSelect");
					cell.appendChild(resSelectDiv);
					let resArgs = element.resArgs;
					let resInput = new Edit({
						id: id,
						value,
						className: "customJs_list_input",
						domParent: resSelectDiv,
						readOnly: true
					})
					let link = new Link({
						domParent: resSelectDiv,
						className: 'customJs_list_link',
						caption: '选择',
						onclick: function () {
							showResourceDialog(assign({
								onok: function (evnet: SZEvent, compoent?: Component, items?: any) {
									resInput.setValue(items.length > 0 ? items[0].id : null);
								}
							}, resArgs))
						}
					});
					this.comps.push(resInput);
					break;
				case "checkbox": // 单项框
					cell.contentEditable = "false";
					let checkbox = new Checkbox({
						domParent: cell,
						id: id, // 注意给控件添加ID值，用于getValue
						checkedValue: 1, // 勾选状态的值
						uncheckedValue: 0,
						checked: true
					});
					this.comps.push(checkbox);
					!isEmpty(value) && checkbox.setValue(value);
					break;
				default:
					!isEmpty(value) && (cell.innerText = value);
					this.comps.push(cell);
					break;
			}
		}
	}

	public getValue(): JSONObject {
		let comps = this.comps;
		let value = {};
		for (let i = 0; i < comps.length; i++) {
			const element = comps[i];
			let id = element.id;
			let v;
			/**
			 * 20220124 tuzw
			 * 若普通组件value为""，则会走处理对象的value，导致报错
			 */
			if (element.innerText || element.innerText == "") {
				v = element.innerText;
			} else {
				v = element.getValue();
			}
			!isEmpty(v) && (value[id] = v);
		}
		return value;
	}
}