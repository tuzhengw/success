/**
 * =====================================================
 * 作者：tuzw
 * 创建日期:2022-06-13
 * 功能描述：常见工具类方法
 * =====================================================
 */
import { isEmpty, showWarningMessage } from "sys/sys";
/**
 * 扩展表达式工具方法
 */
export class ExtensionExpressionsUtils {

    /**
     * 将某毫秒数转换为时间段信息
     * @param numbers 处理的数值（毫秒）
     * @return eg: 13475527 ———— 4小时44分
     */
    public getFriendlyDuration(numbers: number): string {
        if (typeof numbers != "number" || numbers < 0) {
            return "typeError";
        }
        // 若1秒都没有，则保留2位小数
        if (numbers < 1000) {
            return `${(numbers / 1000.0).toFixed(2)}秒`;
        }
        numbers = Math.floor(numbers / 1000); /** 转换为秒 */
        let hour = Math.floor(numbers / 3600);
        let minute = Math.floor(numbers % 3600 / 60); /** numbers % 3600 去除"时"，获取剩下的"分" */
        let second = numbers % 60;
        if (numbers < 60) {
            return `${second}秒`;
        }
        if (numbers < 60 * 60) {
            return `${minute}分${second}秒`;
        }
        return `${hour}时${minute}分${second}秒`;
    }

    /**
     * 校验字符串是否为数字
     */
    public isNumber(val) {
        let regPos = /^\d+(\.\d+)?$/; //非负浮点数
        let regNeg = /^(-(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*)))$/; //负浮点数
        if (regPos.test(val) || regNeg.test(val)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 根据选择【前后缀】信息，修改表的前后缀
     * @param tablePrefix 前后缀和操作信息，eg："delPre.123:addPre.ODS_"
     *      源表前后缀是否删除 + "." + 源表前后缀名 + ":" + 目标表前后缀是否新增 + "." + 目标表前后缀名
     * @param tableName 处理的表名
     * @param ignoreCase 比较是否忽略大小写，默认不忽略
     * <pre>
     *      parseTablePrefix("delPre.ODS_:addPre.NEW_", "ODS_TABLE_NAME") ——> NEW_TABLE_NAME
     * </pre>
     */
    public parseTablePrefix(tablePrefix: string, tableName: string, ignoreCase?: boolean): string {
        if (isEmpty(tablePrefix)) {
            return tableName;
        }
        if (isEmpty(tableName)) {
            return "";
        }
        let newTableName: string = tableName.trim();
        let dealStr: string[] = tablePrefix.split(":");
        let sourceTableStr: string[] = dealStr[0].split(".");

        let sourceTableOption: string = sourceTableStr[0];
        let sourceTableAdjustStr: string = sourceTableStr[1];
        if (!isEmpty(sourceTableOption) && !isEmpty(sourceTableAdjustStr)) { // 处理源表信息
            switch (sourceTableOption) {
                case "delPre": newTableName = this.removeStart(newTableName, sourceTableAdjustStr, ignoreCase);
                    break;
                case "delSuf": newTableName = this.removeEnd(newTableName, sourceTableAdjustStr, ignoreCase);
                    break;
            }
        }
        let targetTableStr = dealStr[1].split(".");
        let targetTableOption: string = targetTableStr[0];
        let targetTableAdjustStr: string = targetTableStr[1];
        if (!isEmpty(targetTableOption) && !isEmpty(targetTableAdjustStr)) { // 处理目标表信息
            switch (targetTableOption) {
                case "addPre": newTableName = this.prependCase(newTableName, targetTableAdjustStr, ignoreCase);
                    break;
                case "addSuf": newTableName = this.appendCase(newTableName, targetTableAdjustStr, ignoreCase);
                    break;
            }
        }
        return newTableName;
    }

    /**
     * 校验str是否以字符串prefix开头，若是，则删除头部prefix
     * @param str 用于验证的字符串
     * @param prefix 匹配开头的字符串
     * @param ignoreCase 比较是否忽略大小写，默认不忽略
     * @return
     */
    public removeStart(str: string, prefix: string, ignoreCase?: boolean): string {
        if (isEmpty(prefix)) {
            return str;
        }
        if (isEmpty(str)) {
            return str;
        }
        if (str.length < prefix.length) {
            return str;
        }
        if (!isEmpty(ignoreCase)) {
            str = str.toUpperCase();
            prefix = prefix.toUpperCase();
        }
        /** 是否指定字母开头 */
        let isOppontLetterStart: boolean = true;
        for (let i = 0; i < prefix.length && isOppontLetterStart; i++) {
            if (str[i] == prefix[i]) {
                continue;
            } else {
                isOppontLetterStart = false;
            }
        }
        if (isOppontLetterStart) {
            str = str.substring(prefix.length);
        }
        return str;
    }

    /**
     * 校验str是否以字符串prefix结尾，若是，则删除尾部prefix
     * @param str 用于验证的字符串
     * @param prefix 匹配结尾的字符串
     * @param ignoreCase 比较是否忽略大小写，默认不忽略
     * @return 
     */
    public removeEnd(str: string, prefix: string, ignoreCase?: boolean): string {
        if (isEmpty(prefix)) {
            return str;
        }
        if (isEmpty(str)) {
            return str;
        }
        if (str.length < prefix.length) {
            return str;
        }
        if (!isEmpty(ignoreCase)) {
            str = str.toUpperCase();
            prefix = prefix.toUpperCase();
        }
        /** 是否指定字母结尾 */
        let isOppontLetterEnd: boolean = true;
        let strIndex: number = str.length - 1;
        for (let i = prefix.length - 1; i >= 0 && isOppontLetterEnd; i--) {
            if (str[strIndex] == prefix[i]) {
                strIndex--;
                continue;
            } else {
                isOppontLetterEnd = false;
            }
        }
        if (isOppontLetterEnd) {
            str = str.substring(0, strIndex + 1);
        }
        return str;
    }

    /**
     * 确保字符串str以字符串prefix开头，返回处理之后的字符串
     * @param str 用于验证的字符串
     * @param prefix 匹配结尾的字符串
     * @param ignoreCase 比较是否忽略大小写，默认不忽略
     * return 
     */
    public prependCase(str: string, prefix: string, ignoreCase?: boolean): string {
        if (isEmpty(prefix) && isEmpty(str)) {
            return str;
        }
        if (isEmpty(str) && !isEmpty(prefix)) {
            return prefix;
        }
        if (!isEmpty(str) && isEmpty(prefix)) {
            return str;
        }
        if (!isEmpty(ignoreCase)) {
            str = str.toUpperCase();
            prefix = prefix.toUpperCase();
        }
        /** 是否指定字母开头 */
        let isOppontLetterStart: boolean = true;
        for (let i = 0; i < prefix.length && isOppontLetterStart; i++) {
            if (str[i] == prefix[i]) {
                continue;
            } else {
                isOppontLetterStart = false;
            }
        }
        if (!isOppontLetterStart) {
            str = `${prefix}${str}`;
        }
        return str;
    }

    /**
     * 确保字符串str以字符串suffix结尾或者以params中任意一个字符串结尾，返回处理之后的字符串
     * @param str 用于验证的字符串
     * @param prefix 匹配结尾的字符串
     * @param ignoreCase 比较是否忽略大小写，默认不忽略
     * return 
     */
    public appendCase(str: string, prefix: string, ignoreCase?: boolean): string {
        if (isEmpty(prefix) && isEmpty(str)) {
            return str;
        }
        if (isEmpty(str) && !isEmpty(prefix)) {
            return prefix;
        }
        if (!isEmpty(str) && isEmpty(prefix)) {
            return str;
        }
        if (!isEmpty(ignoreCase)) {
            str = str.toUpperCase();
            prefix = prefix.toUpperCase();
        }
        /** 是否指定字母结尾 */
        let isOppontLetterEnd: boolean = true;
        let strIndex: number = str.length - 1;
        for (let i = prefix.length - 1; i >= 0 && isOppontLetterEnd; i--) {
            if (str[strIndex] == prefix[i]) {
                strIndex--;
                continue;
            } else {
                isOppontLetterEnd = false;
            }
        }
        if (!isOppontLetterEnd) {
            str = `${str}${prefix}`;
        }
        return str;
    }

    /**
     * 根据字段名数组，构建query查询字段
     * @param fieldNames 查询的字段数组，eg：["name","age","sex"]
     * @return 
     */
    public generatorQueryField(fieldNames: string[]): QueryFieldInfo[] {
        if (fieldNames.length == 0) {
            return [];
        }
        let queryFields: QueryFieldInfo[] = [];
        for (let i = 0; i < fieldNames.length; i++) {
            queryFields.push({
                name: fieldNames[i],
                exp: `model1.${fieldNames[i]}`
            });
        }
        return queryFields;
    }

    /**
     * 根据字段和值构建query查询exp表达式
     * @param fieldNames 过滤的字段，eg：["name","age","sex"]
     * @param filterValue 过滤的值，与fieldNames一一对应
     * @param connectionOpation 多字段关系，eg：or、and
     * @return 
     */
    public generatorQueryFilterExp(fieldNames: string[], filterValue: string[], connectionOpation: string): string {
        if (fieldNames.length == 0) {
            return "";
        }
        let filterExp: string = `model1.${fieldNames[0]}=${filterValue[0]}`;
        for (let i = 1; i < fieldNames.length; i++) {
            filterExp = `${filterExp} ${connectionOpation} model1.${fieldNames[i]}=${filterValue[i]}`;
        }
        return filterExp;
    }

}