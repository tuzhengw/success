
import { getDataSource } from "svr-api/db";

import BufferedImage = java.awt.image.BufferedImage;
import File_ = java.nio.file.File;
import ImageIO = javax.imageio.ImageIO;
import BarcodeFormat = com.google.zxing.BarcodeFormat;
import EncodeHintType = com.google.zxing.EncodeHintType;
import MultiFormatWriter = com.google.zxing.MultiFormatWriter;
import WriterException = com.google.zxing.WriterException;
import BitMatrix = com.google.zxing.common.BitMatrix;
import ErrorCorrectionLevel = com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import FileUtils = com.succez.commons.util.io.FileUtils;
import ByteArrayInputStream = java.io.ByteArrayInputStream;
import ByteArrayOutputStream = java.io.ByteArrayOutputStream;
import FilenameUtils = org.apache.commons.io.FilenameUtils;
import StringUtils = org.apache.commons.lang.StringUtils;
import ResultSet = java.sql.ResultSet;
import M_Blob_ = java.sql.Blob;
import LocalDateTime = java.time.LocalDateTime;
import SimpleDateFormat = java.text.SimpleDateFormat;
import DateFormat_ = java.text.DateFormat;
import O_BLOB_ = oracle.sql.BLOB;
import IOUtils = org.apache.commons.io.IOUtils;

/** 数据源名 */
const DATA_BASE_NAME = "hbscjg";
const DW_JGC_WAREHOUSINGOUT = "dw_jgc_warehousingout_copy";
const DW_JGC_WAREHOSEINGOUT_SCHEM = "epidemic_prevention";
const alDs = getDataSource("JZJGC");
/**
 * 获取Blob字段中存储的二进制流，写入response对象返回
 * @param 
 */
export function dealBlobFields(request: HttpServletRequest, response: HttpServletResponse, params: { certificateNo: string }): void {
    let certificateNo: string = params.certificateNo;
    console.debug(`传入参数：${params}`);
    console.debug(certificateNo);
    if (!certificateNo) {
        console.debug("传入参数不合法");
        return;
    }
    let conn = alDs.getConnection();

    response.setHeader("Content-Disposition", "attachment; filename=123.png"); // 给定【文件下载】请求头
    response.setContentType("application/png"); // 已二进制流的形式返回给前端
    response.addHeader("Content-transfer-Encoding", "binary");

    let preparestatement = null;
    let resultSet = null;
    let inputStream = null;
    let outputStream = null;
    let blob = null;
    try {
        let querySQL: string = `select QRCODEPATH from epidemic_prevention.dw_jgc_warehousingout_copy where CERTIFICATENO=?`; // 锁行
        print(querySQL);
        preparestatement = conn.prepareStatement(querySQL);
        preparestatement.setString(1, certificateNo);

        resultSet = preparestatement.executeQuery();
        if (resultSet.next()) {
            blob = resultSet.getBlob(1);
        }
        if (blob == null) {
            console.debug(`当前数据二维码不存在`);
            return;
        }
        inputStream = blob.getBinaryStream();
        response.setContentLength(inputStream.available());
        outputStream = response.getOutputStream();
        IOUtils.copy(inputStream, outputStream);
    } catch (e) {
        console.error(`获取出仓管理-国家出仓报告备案号：error`);
        console.error(e);
    } finally {
        if (preparestatement != null) {
            preparestatement.close();
        }
        if (resultSet != null) {
            resultSet.close();
        }
        if (inputStream != null) {
            inputStream.close();
        }
        if (outputStream != null) {
            outputStream.close();
        }
        conn.close();
    }
}
