/**
 * 名数组
 */
const hindName: Array<string> = ['李', '孔', '丁', '吴', '余', '王', '颜', '屈', '羽', '黄', '明'];
/**
 * 名数组
 */
const headName: Array<string> = ['行', '天', '开', '名', '敏', '楷', '我', '李', '思', '英', '奥', '改', '命', '由', '不', '才', '子'];
/**
 * 随机生成一个中文名
 */
function randomName(): string{
    return hindName[random(1, 11)] + headName[random(1, 17)];
}
/**
 * 随机生成min-max的随机数
 */
function random(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}