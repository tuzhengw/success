---
typora-root-url: images
---

# Idea如何自动编译TypeScript文件

## **步骤1：新建文件夹**

> （1）在main文件下，创建一个：webapp文件夹
>
> （2）**在webapp文件夹新建js和ts文件夹**

![](../images/1.png)

---

## **步骤2：右击TS文件，选择<button>终端打开</button>（也可win+R）**

![](../images/2.png)

---

## **步骤3：在TS目录下生成tsconfig.json文件**

```cmd
D:\MyIDEA\quintTest\src\main\webapp\ts>tsc --init
```

![](../images/3.png)

---

## **步骤4：修改配置文件**

![](../images/4.png)

---

## **步骤5：监视tsconfig文件**

```cmd
D:\MyIDEA\quintTest\src\main\webapp\ts>tsc -p tsconfig.json --watch
```