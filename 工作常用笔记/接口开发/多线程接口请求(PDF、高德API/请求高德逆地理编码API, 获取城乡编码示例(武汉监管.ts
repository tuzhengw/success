/**
 * 作者: tuzw
 * 时间: 2022-11-29 
 * 需求: 逆地理编码-将经纬度数据转换为行政区划代码
 * 地址: https://jira.succez.com/browse/CSTM-21405
 */
import db from "svr-api/db";
import dw from "svr-api/dw";
import http from "svr-api/http";
import metadata from "svr-api/metadata";
import sys from "svr-api/sys";

const Timestamp = Java.type('java.sql.Timestamp');

/**
 * 入口
 */
function main() {
    threadConvertGisInfos();
}

/** 高德账户密钥 */
const MapKeys = [
    "xxx", "xxxx", 
];
/** 当前使用的高德账户密钥下标, 初始：0(线程中递增不会影响此值) */
let currentUseMapKeyIndex: number = 0;
/** 高德-逆地理编码API每天调用上线次数, 一个账户免费每天调用5000次 */
const MapRegeoNums = 5000;

/** 外部控制程序执行 */
const ControlIsStopExecutePath = "/sysdata/script/controlProgramStop.txt";
/**
 * 多线程处理—逆地理编码-将经纬度数据转换为行政区划代码
 * 
 * @description
 * 1) 每次处理的数据为: 【未获取】城市编码和乡镇街道编码信息的网格点数据
 */
function threadConvertGisInfos(): void {
    console.info(`开始执行[threadConvertGisInfos], 线程处理逆地理编码-将经纬度数据转换为行政区划代码`);
    let startTime: number = Date.now();
    let singalDealNums: number = 1000;
    let modelQueryUtils = new ModelQueryUtils({
        modelPath: "/sdi/data/tables/测试/sunying/F_WGXXB_CL.tbl",
        queryFields: [{
            name: "ID", exp: "model1.ID"
        }, {
            name: "LATITUDE", exp: "model1.LATITUDE"
        }, {
            name: "LONGITUDE", exp: "model1.LONGITUDE"
        }],
        filterConditions: [{
            exp: `model1.ADCODE IS NULL AND model1.TOWNCODE IS NULL`
        }],
        sortField: "CREATE_TIME",
        queryMaxNum: singalDealNums
    });
    let dealDataNums: number = modelQueryUtils.getQuerySumNums();
    if (!dealDataNums) {
        console.info(`未获取到需要处理的数量`);
        return;
    }

    /** 线程池核心执行数量, 最好分配: 3 */
    let corePoolSize: number = 3;
    let threadPool = sys.getThreadPool({
        name: "requestCountryCode", // 线程池中唯一的标识, 若name已经存在，则会获取已存在的线程池
        corePoolSize: corePoolSize,
        maximumPoolSize: 5
    });

    let isStop: string = "false";

    let dealCounts: number = Math.ceil(dealDataNums / singalDealNums);
    console.info(`模型需要处理总数据量: ${dealDataNums} 条, 每次处理: ${singalDealNums} 条, 共需要处理: ${dealCounts} 次`);

    for (let dealIndex = 0; dealIndex < dealCounts; dealIndex++) {
        let noDealNums: number = dealCounts - dealIndex - 1;
        console.info(`开始第 ${dealIndex} 次处理, 还剩下: ${noDealNums} 次`);
        if (currentUseMapKeyIndex == MapKeys.length) {
            console.info(`所有高德用户web服务key都已使用完(免费调用API额度今日已用完), 结束执行, 还剩下: ${noDealNums} 次`);
            break;
        }
        /**
         * 注意：
         * 1) 构造的queryInfo包含过滤条件: ADCODE和TOWNCODE值为空
         * 2) 每次请求成功后都会将其ADCODE和TOWNCODE设置值, 导致查询数据量逐渐递减
         * 3) 分页查询会根据已有的queryInfo重新查询，携带过滤条件
         * 4) 由于处理过的数据已经不符合, 导致满足条件的数据会前移, 设置查询起始页会导致忽略部分数据
         * 5) 此处查询不能直接分页查询，而是每次都从第0行查询尚未处理过的数据
         */
        let queryResult = modelQueryUtils.queryModelDatas(0);
        if (!queryResult.result) {
            console.info(`第[${dealIndex}] 次处理失败, 停止处理`);
            break;
        }
        let datas: F_WGDXX_CSHJ[] = queryResult.data;

        /** 正在运行的线程个数 */
        let runingThreadNums: number = 0;
        do {
            runingThreadNums = threadPool.getActiveCount();
            console.info(`当前线程池运行数量为: ${runingThreadNums}, 线程池可执行最大数量为: ${corePoolSize}`);
            if (runingThreadNums >= corePoolSize) {
                console.info(`当前线程池运行数量已经大于【${corePoolSize}】, 需要等待线程池释放`);
                sys.sleep(10000);
            }
            isStop = metadata.getString(ControlIsStopExecutePath).trim();
            if (isStop == "true") {
                break;
            }
            if (runingThreadNums < corePoolSize) { // 若线程池数量 还有空闲，则不循环等待
                break;
            }
        } while (runingThreadNums >= corePoolSize); // 若当前没有空余的线程，则等待

        isStop = metadata.getString(ControlIsStopExecutePath).trim();
        if (isStop == 'true') {
            console.info(`第[${dealIndex}] 次处理被手动终止, 停止处理`);
            break;
        }
        let currentThreadId: number = dealIndex % corePoolSize;
        console.info(`执行次数: [${dealIndex + 1}], 开始分配线程: ${currentThreadId}`);
        let threadTask = threadPool.submit({ // 线程池中执行，批处理脚本不会打印日志，得在【系统日志】查看
            method: "getCountryAreaIds",
            params: [datas, currentThreadId],
            path: "/sysdata/data/batch/tuzw-逆地理编码-将经纬度数据转换为行政区划代码.action"
        });
        let executeResult = threadTask.get();
        console.info(executeResult);
    }
    while (threadPool.getActiveCount() != 0) {
        isStop = metadata.getString(ControlIsStopExecutePath);
        if (isStop == "true") {
            console.info(`逆地理编码-将经纬度数据转换为行政区划代码, 被手动终止-end`);
            break;
        }
        console.info(`当前线程池运行数量为: ${threadPool.getActiveCount()}, 线程运行中...`);
        sys.sleep(10000);
    }
    console.info(`执行结束[threadConvertGisInfos], 线程处理逆地理编码-将经纬度数据转换为行政区划代码, 共耗时: ${(Date.now() - startTime) / 1000} s`);
}

/** 数据源名 */
const DbSourceName = "default";
/** 网格点信息物理表名 */
const WebInfoPhysicalName = "F_WGXXB";
/**
 * 处理给定的网格点信息数据, 根据经纬度信息获取所在区县的行政区划
 * @param datas 网格点数据
 * @param threadId 线程序号
 */
export function getCountryAreaIds(datas: F_WGDXX_CSHJ[], threadId: string): string {
    let dealDataLen: number = datas.length;
    console.info(`线程: [${threadId}] 开始处理给定网格点信息数据, 共[${dealDataLen}] 条数据, 根据经纬度信息获取所在区县的行政区划`);
    let startTime: number = Date.now();
    let dbSouce = db.getDataSource(DbSourceName);
    let defaultSchema: string = dbSouce.getDefaultSchema();
    let table = dbSouce.openTableData(WebInfoPhysicalName, defaultSchema);

    let errorLogs: F_WGDXX_CSHJ[] = [];
    for (let i = 0; i < dealDataLen; i++) {
        if (currentUseMapKeyIndex == MapKeys.length) {
            console.info(`线程: [${threadId}] 停止执行, 所有高德用户web服务key都已使用完(免费调用API额度今日已用完), 结束执行`);
            break;
        }
        let isStop: string = metadata.getString(ControlIsStopExecutePath).trim();
        if (isStop == 'true') {
            console.info(`线程[${threadId}] 被手动中止, 当前已处理: ${i + 1} 条数据, 还剩: ${dealDataLen - i + 1} 条数据未处理`);
            break;
        }
        let data: F_WGDXX_CSHJ = datas[i];
        let id: string = data.ID;
        let longitude: string = data.LONGITUDE;
        let latitude: string = data.LATITUDE;

        let requestResult = requestRestApiMapInfo(longitude, latitude);
        let updateResult: ResultInfo = { result: true };

        if (requestResult.result) {
            let responseData = requestResult.data;
            let adCode: string = responseData['adcode'];
            let towncode: string = responseData['towncode'];
            updateResult = updateCountryDistrictInfo(table, id, adCode, towncode);
        }
        if (!requestResult.result || !updateResult.result) {
            errorLogs.push({
                ID: id,
                LATITUDE: latitude,
                LONGITUDE: longitude,
                STATUS: "FAIL",
                ERROR_MESSAGE: !requestResult.result ? requestResult.message : updateResult.message,
                CREATE_TIME: new Timestamp(Date.now())
            });
        }
    }
    insertRequestCodeErrorLog(dbSouce, errorLogs);
    let resMessage: string = `线程: [${threadId}] 执行结束, [${dealDataLen}] 条数据有: [${errorLogs.length}] 条数据请求失败, 共耗时: ${(Date.now() - startTime) / 1000} s`;
    console.info(`线程: [${threadId}] 执行结束, [${dealDataLen}] 条数据有: [${errorLogs.length}] 条数据请求失败, 共耗时: ${(Date.now() - startTime) / 1000} s`);
    return resMessage;
}

/**
 * 请求高德提供的对外接口——逆地理编码接口
 * @param longitude 经度
 * @param latitude 纬度
 * @return 
 * 
 * @description 
 * 1) 高德接口地址：https://lbs.amap.com/api/webservice/guide/api/georegeo#regeo
 * 2) 参数注意: 经度在前，纬度在后, 经纬度小数点后不要超过 6 位
 * 3) 请求接口示例：
 *    地址：https://restapi.amap.com/v3/geocode/regeo?key=e7280b8b6d1b98dcc2e34dd49ac90f4b&location=114.59825,30.62997
 *    返回成功结果：{
        "status": "1",
        "regeocode": {
            "addressComponent": {
                "city": "武汉市",
                "province": "湖北省",
                "adcode": "420117",
                "district": "新洲区",
                "towncode": "420117009000",
                "streetNumber": {
                    "number": "13号",
                    "location": "114.600590,30.627647",
                    "direction": "东南",
                    "distance": "341.855",
                    "street": "阳枫公路"
                },
                "country": "中国",
                "township": "双柳街道",
                "businessAreas": [
                    []
                ],
                "building": {
                    "name": [],
                    "type": []
                },
                "neighborhood": {
                    "name": [],
                    "type": []
                },
                "citycode": "027"
            },
            "formatted_address": "湖北省武汉市新洲区双柳街道邱湖村世华小学"
        },
        "info": "OK",
        "infocode": "10000"
    }
    返回失败结果, eg: {
        "status": "0",
        "info": "INSUFFICIENT_ABROAD_PRIVILEGES",
        "infocode": "20011"
    }
 */
function requestRestApiMapInfo(longitude: string, latitude: string): ResultInfo {
    let resultInfo: ResultInfo = { result: true };
    longitude = checkFloatStrLen(longitude, 6);
    latitude = checkFloatStrLen(latitude, 6);

    /**
     * 考虑到一个账户调用接口每天限制5000，改为多个账户循环调用（全局参数记录已使用过的账户）
     * 1 多线程执行会导致每次执行都是独立的，每次分配线程后执行程序，全局参数都会重新初始化为默认值
     * 2 每个线程初次执行, currentUseMapKeyIndex 都从0开始递增, 直到找到可使用的高德web服务key 或者 所有key都使用完
     */
    while (currentUseMapKeyIndex < MapKeys.length) {
        let isStop: string = metadata.getString(ControlIsStopExecutePath).trim();
        if (isStop == 'true') {
            break;
        }
        let mapKey: string = MapKeys[currentUseMapKeyIndex];
        let urlAddress: string = `https://restapi.amap.com/v3/geocode/regeo?key=${mapKey}&location=${longitude},${latitude}`;
        let requestResult = http.request({
            url: urlAddress
        });
        let httpCode: number = requestResult.httpCode;
        if (requestResult.httpCode != 200) {
            resultInfo.result = false;
            resultInfo.message = `请求接口失败, code: ${httpCode}`;
            break;
        }
        let districtInfo = JSON.parse(requestResult.responseText);
        let status: string = districtInfo['status'];
        if (status == "1") {
            let addressComponent = districtInfo['regeocode']['addressComponent'];
            let adcode: string = addressComponent['adcode'];
            let towncode: string = addressComponent['towncode'];
            resultInfo.data = {
                adcode: checkStrIsArray(adcode), // 返回值可能为数组
                towncode: checkStrIsArray(towncode)
            }
            break; // 若当前key可以使用 并且请求成功, 则返回
        }
        let resErrorCode: string = districtInfo['info'];
        if (resErrorCode == "USER_DAILY_QUERY_OVER_LIMIT") { // 若返回结果为接口当然调用次数上限, 则使用下一个key
            console.info(`web服务key当日调用次数上限: ${MapKeys[currentUseMapKeyIndex]}`);
            currentUseMapKeyIndex++;
            console.info(`使用下一个高德web服务key: ${MapKeys[currentUseMapKeyIndex]} 继续请求`);
            continue;
        }
        resultInfo.result = false;
        resultInfo.message = resErrorCode;
        break; // 此处循环主要是请求次数上限，循环更换key，其他错误则停止循环
    }
    return resultInfo;
}

/**
 * 检查参数是否为数组, 并将其转换为字符串返回
 * @param param 校验的参数
 * @return 
 */
function checkStrIsArray(param: string): string | void {
    if (!param) {
        return;
    }
    if (Array.isArray(param)) { // 返回值可能为数组
        if (param.length == 0) {
            return;
        }
        param = param.join(",");
    }
    return param;
}

/**
 * 检查浮点型字符串小数位是否超过指定长度, 若超过则截取超过的部分
 * @param floatStr 浮点型字符串
 * @param smallLen 允许的小数长度
 * @return 
 */
function checkFloatStrLen(floatStr: string, smallLen: number): string {
    if (!floatStr) {
        return floatStr;
    }
    let smallIndex: number = floatStr.indexOf(".");
    if (smallIndex == -1) {
        return floatStr;
    }
    let smallActurlLen: number = floatStr.substring(smallIndex + 1).length;
    if (smallActurlLen <= smallLen) {
        return floatStr;
    }
    return floatStr.substring(0, smallIndex + smallLen + 1);
}

/**
 * 更新县行政区划
 * @param table 网格点信息表对象
 * @param id 更新行主键ID
 * @param adcode 城市编码
 * @param townCode 乡镇街道编码
 * @return 
 */
function updateCountryDistrictInfo(table: TableData, id: string, adcode: string, townCode: string): ResultInfo {
    let updateResult: ResultInfo = { result: true };
    try {
        let updateNums: number = table.update({ "ADCODE": adcode, "TOWNCODE": townCode }, { "ID": id });
        if (updateNums == 0) {
            updateResult.result = false;
            updateResult.message = "SQL更新失败";
        }
    } catch (e) {
        updateResult.result = false;
        updateResult.message = `SQL更新失败, ${e.toString()}`;
        console.info(`更新网格主键: ${id} 的城市编码和乡镇街道编码失败----error`);
        console.info(e);
    }
    return updateResult;
}

/**
 * 记录获取乡镇街道编码失败的网格点信息
 * @param dbSouce 数据源对象
 * @param errorInfo 转换失败的信息
 */
function insertRequestCodeErrorLog(dbSouce: Datasource, errorInfo: F_WGDXX_CSHJ[]): void {
    if (errorInfo.length == 0) {
        return;
    }
    let defaultSchema: string = dbSouce.getDefaultSchema();
    let logTable = dbSouce.openTableData("F_WGDXX_CSHJ_LOG", defaultSchema);
    try {
        logTable.insert(errorInfo);
    } catch (e) {
        console.error(`记录获取乡镇街道编码失败的网格点信息失败----error`);
        console.error(e);
    }
}

/** 网格点信息 */
interface F_WGDXX_CSHJ {
    /** 网格点信息主键 */
    ID: string;
    /** 纬度 */
    LATITUDE: string;
    /** 经度 */
    LONGITUDE: string;
    /** 失败原因 */
    ERROR_MESSAGE?: string;
    /** 状态 */
    STATUS?: string;
    /** 创建时间 */
    CREATE_TIME: Date;
}

/**
 * 模型分页查询工具类
 * @description
 * 1) 内置dw模块查询, 不支持多线程
 */
class ModelQueryUtils {

    /** 查询模型路径 */
    private modelPath: string;
    /** 每次查询数据量(limit), 默认： 10 */
    private queryMaxNum: number;

    /** 排序字段 */
    private sortField: string;
    /** 排序方式, 默认: asc、desc */
    private sortMethod: string;

    /** 查询方式, 默认：全量 */
    private queryMethod: ModelQueryMethod;
    /** 增量字段 */
    private incrementField: string;

    /** 
     * 查询字段信息, eg: [{
     *   name: "TASK_ID", exp: "model1.TASK_ID.SCHEDULE"
     * }] 
     */
    private queryFields: QueryFieldInfo[];
    /** 
     * 模型过滤条件, eg: [{
     *   exp: `model1.ADCODE IS NULL AND model1.TOWNCODE IS NULL`
     * }]
     */
    private filterConditions: FilterInfo[];

    /** 满足条件的数据总数量 */
    private querySumNums: number;
    /** 查询的queryInfo参数 */
    private queryInfo: QueryInfo;

    constructor(args: {
        modelPath: string,
        queryFields: QueryFieldInfo[],
        sortField: string,
        sortMethod?: string,
        queryMethod?: ModelQueryMethod,
        incrementField?: string,
        filterConditions?: FilterInfo[],
        queryMaxNum?: number
    }) {
        this.modelPath = args.modelPath;
        this.queryFields = args.queryFields;
        this.sortField = args.sortField;

        this.sortMethod = !!args.sortMethod ? args.sortMethod : "asc";
        this.queryMethod = !!args.queryMethod ? args.queryMethod : ModelQueryMethod.FullQuery;
        this.incrementField = args.incrementField as string;
        this.queryMaxNum = !!args.queryMaxNum ? args.queryMaxNum : 100;
        this.filterConditions = !!args.filterConditions ? args.filterConditions : [];
        this.initParams();
    }

    /**
     * 初始内部参数
     */
    private initParams(): void {
        this.builderQueryInfo();
        this.queryFulfilConditionNums();
    }

    /**
     * 分页查询模型数据
     * @param queryStartIndex 从第几行开始, 0: 是第一行,不传递此参数表示从第一行开始, 默认值为0
     * @return {
     *   result: true,
     *   data: [{
     *     JSON数据
     *   }]
     * }
     */
    public queryModelDatas(queryStartIndex: number): ResultInfo {
        if (!queryStartIndex) {
            queryStartIndex = 0;
        }
        let querySumNums: number = this.querySumNums;
        if (queryStartIndex > querySumNums) {
            return { result: false, message: `当前查询行数超过查询的总行数, 总行数: ${querySumNums}` };
        }
        let preCheckResult: ResultInfo = this.preCheck();
        if (!preCheckResult.result) {
            return preCheckResult;
        }
        let queryInfo: QueryInfo = this.queryInfo;
        queryInfo.options = {
            limit: this.queryMaxNum,
            offset: queryStartIndex,
            queryTotalRowCount: false
        }
        let datas: JSONObject[] = [];
        let queryDatas = dw.queryData(queryInfo).data;
        for (let i = 0; i < queryDatas.length; i++) {
            let queryData = queryDatas[i] as string[];
            let data: JSONObject = {};
            for (let fieldIndex = 0; fieldIndex < this.queryFields.length; fieldIndex++) { // 查询的结果根查询字段顺序一致, 反向将数组转换为JSON数据
                let fieldName: string = this.queryFields[fieldIndex].name;
                data[fieldName] = queryData[fieldIndex];
            }
            datas.push(data);
        }
        return { result: true, data: datas };
    }

    /**
     * 获取查询数量的总数
     */
    public getQuerySumNums(): number {
        return this.querySumNums;
    }

    /**
     * 预检查
     */
    private preCheck(): ResultInfo {
        if (!this.modelPath) {
            return { result: false, message: "模型地址为空" };
        }
        if (!this.queryFields || this.queryFields.length == 0) {
            return { result: false, message: "查询字段未指定" };
        }
        if (this.queryMethod == ModelQueryMethod.Increment && !this.incrementField) {
            return { result: false, message: `增量查询, 未指定增量字段` };
        }
        if (!this.sortField) {
            return { result: false, message: "未指定排序字段" };
        }
        return { result: true };
    }

    /**
     * 构造queryInfo信息
     */
    private builderQueryInfo(): void {
        let queryFields: QueryFieldInfo[] = this.queryFields;
        if (!this.modelPath || !queryFields || queryFields.length == 0 || !this.sortField) {
            return;
        }
        this.getQueryFilterCondition();
        this.queryInfo = {
            fields: queryFields,
            select: true,
            sources: [{
                id: "model1",
                path: this.modelPath
            }],
            filter: this.filterConditions,
            sort: [{
                exp: `model1.${this.sortField}`,
                type: this.sortMethod
            }],
            options: {
                limit: 1,
                offset: 0,
                queryTotalRowCount: true
            }
        };
        console.info(`查询的queryInfo信息为: `);
        console.info(this.queryInfo);
    }

    /**
     * 查询满足条件的数据总数
     */
    private queryFulfilConditionNums(): void {
        if (!this.queryInfo) {
            return;
        }
        let queryData = dw.queryData(this.queryInfo);
        if (queryData != null) {
            this.querySumNums = queryData.totalRowCount as number;
        }
        console.info(`共需要查询数量为: ${this.querySumNums} 条`);
    }

    /** 
     * 根据查询方式构造查询条件
     * @description
     * 1) 增量默认为执行时间的前一天0点到当天0点, 数据范围为：24小时
     */
    private getQueryFilterCondition(): void {
        let sdfTime = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        if (this.queryMethod == ModelQueryMethod.Increment) {
            let filterField: string = this.incrementField;
            if (!filterField) {
                return;
            }
            let now: number = new Date().setHours(0, 0, 0);
            let startTime: string = sdfTime.format(now);
            let filterNums: number = now - 24 * 60 * 60 * 1000 - 1 * 60 * 1000;
            let lastSendTime: string = sdfTime.format(new java.util.Date(filterNums));
            let filterExp: string = `model1.${filterField} >= '${lastSendTime}' and model1.${filterField} < ${startTime}`;
            console.info(`当前请求方式是: 增量请求, 过滤表达式: [${filterExp}]`);
            this.filterConditions.push({
                exp: filterExp
            });
        }
    }
}

/** 返回值 */
interface ResultInfo {
    /** 返回值 */
    result?: boolean;
    /** 处理消息 */
    message?: string;
    /** 返回数据 */
    data?: any;
}

/** 模型查询方式 */
enum ModelQueryMethod {
    /** 全量 */
    FullQuery = "fullQuery",
    /** 增量 */
    Increment = "increment"
}