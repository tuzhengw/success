/**
 * es6的class
 */
class Animal{
    /**
     * 定义属性:这是es6的新写法，之前es6只能在constructor中通过this.name来定义
     */
    name;
    constructor(name){
        this.name = name;
    }
    onLoad(){
        alert('000');
    }
    showName(){
        console.log(this.name);
    }
}
let animal = new Animal('狗');
animal.showName();
let tmpStr = 'string';
/**
 * 在ts中当开始赋值为string类型后，后面便不可再赋值另外一种类型；
 * 然而在es6中这种语法是允许的
 */
// tmpStr = 1;//

/**
 * 类的继承:使用extends实现继承，通过super()或super.XX调用父类的构造方法或属性和普通方法
 * 若继承了父类，则构造方法中必须要调用父类的构造方法super(...)
 */
class Dog extends Animal{//
    constructor(name){
        super(name);
    }
    /**
     * 覆盖父类showName()方法
     */
    showName(){
        super.showName();
    }
}
let dog = new Dog('汤姆狗');
dog.showName();


/**
 * 存取属性setter,getter:会拦截住name属性的取值和存值
 */
class Animal2{
    private  name: string;
    /**
     * 注：如果写了存取值器，则不能再这里进行定义了，但是可以在构造器中this.name = name;
     * 原因暂时不清楚
     * @param name
     */
    constructor(name){
        this.name = name;
    }

    /**
     * 这个方法只是会在name属性被设置值的时候出发，不是setName()，因此不能在这里this.name = name;
     * 估计这样做可能会不停调用到这个监听函数，导致栈溢出
     * @param name
     */
    // set name(name) {
        //this.name = name;//error:VM514:6 Uncaught RangeError: Maximum call stack size exceeded
    //     console.log('setter: ' + name);
    // }
    setName(name: string){
        this.name = name;
    }
    /**
     * 暂时没想明白这个有什么作用，这样都拿不到name属性的当前值了
     * @returns {string}
     */
    get name(){
        console.log('getter...');
        return 'jack';
    }
}

/**
 * 熊二，构造方法也会出发set name()函数
 * @type {Animal2}
 */
let animal2 = new Animal2('熊二');
animal2.name = '光头强';//光头强
console.log(animal2.name);//getter... jack


/**
 * 静态方法:static
 */
class Animal3{
    // constructor(name){
    //     static this.name = name;//不能这么定义静态变量
    // }
    static isAnimal(animal){
        console.log(animal instanceof Animal3);
    }
}
let animal3 = new Animal3();
Animal3.isAnimal(animal3);//直接使用类名调用

//静态属性:之前es6不支持在constructor外定义属性的，如上：开始不支持定义静态属性
// class Animal4{//浏览器控制台并不报错
//     static name;
// }


//TypeScript 中类的用法
//public(属性和方法的默认修饰符) private 和 protected
//private
class Animal5 {
    private name;
    public constructor(name) {
        this.name = name;
    }
}

let animal5 = new Animal5('Jack');
// console.log(animal5.name); // error:私有的不能访问
// animal5.name = 'Tom'; //error:不能访问

//private，子类也不能访问
class Animal6 {
    private name;
    public constructor(name) {
        this.name = name;
    }
}

class Cat extends Animal6 {
    constructor(name) {
        super(name);
        // console.log(this.name);//不能访问父类的private属性
    }
}

//protected 修饰，则允许在子类中访问


///抽象类///
abstract class Animal7{
    abstract name;//抽象属性，好像很少这么用
    constructor(name){//抽象类不能new对象
        //this.name = name;//
    }
    public abstract showName();//抽象方法
}
class Fish implements Animal7{
    name;//实现抽象属性
    constructor(name){
        this.name = name;
    }
    public showName() {//实现showName()方法，且修饰符要是>=父类方法的修饰符
        console.log(this.name);
    }
}
new Fish('清江鱼').showName();


//类的类型
class Student0{
    public sno:string;
    public name:string;
    constructor(sno,name){
        this.sno = sno;
        this.name = name;
    }
    public show():string{
        return `sno:${this.sno};name:${this.name}`;
    }
}
let stud:Student0 = new Student0('007','林俊杰');
stud.show();