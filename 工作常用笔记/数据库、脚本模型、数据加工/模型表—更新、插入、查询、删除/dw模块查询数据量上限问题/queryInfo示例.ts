
import db from 'svr-api/db';
import { queryData, updateDwTableData } from "svr-api/dw"


function testQuery(): void {
    let info: QueryInfo = {
        "fields": [{
            "name": "企业(机构)名称",
            "exp": "model1.ENTNAME"
        }],
        "select": true,
        "sort": [{
            "exp": "model1.EXPDATE"
        }],
        "sources": [{
            "id": "model1",
            "path": "/xyfl/data/tables/FACT/ZJSJ/SC/FACT_FX_DFSCXX.tbl"
        }],
        "filter": [],
        "options": {
            "limit": 11,
            "offset": 500000,
            "queryTotalRowCount": true
        }
    };
    let queryResult = queryData(info);
    console.info("查询结果: ");
    console.info(queryResult.data);

    console.info("超过50w数据（可系统设置-安全阈值设置——最大查询数量）, dw模块无法查询, 但db模块执行其生成的SQL可以查询出来数据");
    let sql = queryResult.sql[0];
    let dbSouce = db.getDefaultDataSource();
    console.info(dbSouce.executeQuery(sql));
}
