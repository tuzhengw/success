import "css!./data-process.css";
import { ScheduleMgrPage, TaskDataProvider } from "schedule/schedulemgr";
import { MetaModulePage, MetaModulePageArgs, ParentDirDataProvider } from "metadata/metamgr";
import { Grid, GridCell, ListItem, GridRow, List } from "commons/tree";
import { SZEvent, showMenu, formatDateFriendly } from "sys/sys";
import { getProjectName } from "metadata/metadata";
import { MMP_Me, PersonalCenterPage_settings } from 'me/personalcenter';

/**
 * 单独显示“个人中心” 但是左侧没有点击事件
 * export default：门户显示
 * export ：JS组件显示
 */
export class ZTSchedulePage extends PersonalCenterPage_settings {
    protected _init(args: any): HTMLElement {
        let domBase = super._init(args);
        return domBase;
    }
}

 /**
     * 注意：通过js组件去内嵌一个 系统设置页面后，系统设置页面只占有了页面的一部分，不会铺满
     * 解决办法：https://jira.succez.com/browse/BI-41364
     * （
     *  （1）通过脚本去掉最外层面板的spgcomponent-body的display属性样式
     *  （2）JS组件设置交互————触发脚本
     * ）
     * args：JS组件（DOM）
     */
	 

// export default class ZTSchedulePage extends PersonalCenterPage_settings {
//     protected _init(args: any): HTMLElement {
//         let domBase = super._init(args);
//         return domBase;
//     }
// }


import { Component, ComponentArgs, ctx, message, SZEvent } from 'sys/sys';
import 'css!./customComponent.css';
import { MMySMessagePage } from "me/smessage";
import { ModifyUserName, ModifyUserPhone, ModifyUserPassword } from 'me/personalcenter';
import { SettingsList, SettingsListItemType } from "commons/mobile/settingslist";
import { DataProvider } from "commons/basic";
import { getCurrentUser } from "metadata/metadata";

/**
 * 移动端个人中心
 */
export class MPersonalCenter extends Component {
    private dataProvider: DataProvider<JSONObject>;
    public settings: SettingsList;

    protected _init(args: ComponentArgs): HTMLElement {
        let domBase = super._init(args);
        getCurrentUser().getUserInfo().then((userInfo: UserInfo) => {
            if (this.isDisposed()) return;
            let userId = userInfo.userId;
            let userName = userInfo.userName;
            let email = userInfo.email;
            let phone = userInfo.phone;
            let avatarImage = ctx(`/api/co/avatar/${userId}`);

            this.dataProvider = new DataProvider({
                data: [
                    {
                        id: 'user',
                        caption: userName,
                        desc: userId,
                        icon: { image: avatarImage },
                        className: 'mpersonalcenter-user',
                        type: SettingsListItemType.Directory,
                        items: [
                            {
                                id: 'user-avatar',
                                caption: message('me.mpersonalcenter.user.avatar'),
                                icon: { image: avatarImage },
                                className: 'mpersonalcenter-avatar',
                                arrowVisible: true,
                                type: SettingsListItemType.Cell
                            },
                            {
                                id: 'user-name',
                                caption: message('me.mpersonalcenter.user.name'),
                                value: userName,
                                arrowVisible: true,
                                type: SettingsListItemType.Directory,
                                items: [
                                    {
                                        id: 'usernameform',
                                        type: SettingsListItemType.Custom,
                                        implClass: ModifyUserName,
                                        onsuccess: (e: SZEvent, component: ModifyUserName, settings: any) => {
                                            let user = e.data;
                                            let userName = user && user.userName;
                                            let bl = settings && settings.booklayout;
                                            if (bl) {
                                                let userItem = bl.getComponent('root').getItems().filter((d: any) => d.id == 'user');
                                                let _userItem = userItem && userItem.length && userItem[0];
                                                _userItem && _userItem.reInit(Object.assign({}, _userItem.data, { caption: userName }));

                                                let userNameItem = bl.getComponent('user').getItems().filter((d: any) => d.id == 'user-name');
                                                let _userNameItem = userNameItem && userNameItem.length && userNameItem[0];
                                                _userNameItem && _userNameItem.reInit(Object.assign({}, _userNameItem.data, { value: userName }));
                                            }
                                        }
                                    }
                                ]
                            },
                            {
                                id: 'user-email',
                                caption: message('me.mpersonalcenter.user.email'),
                                value: email,
                                arrowVisible: true,
                                type: SettingsListItemType.Cell
                            },
                            {
                                id: 'user-phone',
                                caption: message('me.mpersonalcenter.user.phone'),
                                value: phone,
                                type: SettingsListItemType.Directory,
                                items: [
                                    {
                                        id: 'userphoneform',
                                        type: SettingsListItemType.Custom,
                                        implClass: ModifyUserPhone,
                                        onsuccess: (e: SZEvent, component: ModifyUserPhone, settings: any) => {
                                            let user = e.data;
                                            let phone = user && user.userInfo && user.userInfo.phone;
                                            let bl = settings && settings.booklayout;
                                            if (bl) {
                                                let item = bl.getComponent('user').getItems().filter((d: any) => d.id == 'user-phone');
                                                let _item = item && item.length && item[0];
                                                _item && _item.reInit(Object.assign({}, _item.data, { value: phone }));
                                            }
                                        }
                                    }
                                ]
                            },
                            {
                                id: 'user-pwd',
                                caption: message('me.mpersonalcenter.user.password'),
                                type: SettingsListItemType.Directory,
                                items: [
                                    {
                                        id: 'userpwdform',
                                        type: SettingsListItemType.Custom,
                                        implClass: ModifyUserPassword,
                                        onsuccess: (e: SZEvent, component: ModifyUserPassword, settings: any) => {
                                        }
                                    }
                                ]
                            },
                        ]
                    },
                    {
                        id: 'split',
                        type: SettingsListItemType.Split
                    },
                    {
                        id: 'message',
                        caption: message('me.mpersonalcenter.message.caption'),
                        icon: 'icon-nbmessage',
                        type: SettingsListItemType.Directory,
                        items: [
                            {
                                id: 'msmessage',
                                type: SettingsListItemType.Custom,
                                implClass: MMySMessagePage
                            }
                        ]
                    },
                    {
                        id: 'subscribe',
                        caption: message('me.mpersonalcenter.subscribe.caption'),
                        icon: 'icon-subscribe',
                        arrowVisible: true,
                        type: SettingsListItemType.Cell
                    },
                    {
                        id: 'earlywarning',
                        caption: message('me.mpersonalcenter.earlywarning.caption'),
                        icon: 'icon-earlywarning',
                        arrowVisible: true,
                        type: SettingsListItemType.Cell
                    },
                    {
                        id: 'share',
                        caption: message('me.mpersonalcenter.share.caption'),
                        icon: 'icon-share',
                        arrowVisible: true,
                        type: SettingsListItemType.Cell
                    },
                    {
                        id: 'split',
                        type: SettingsListItemType.Split
                    },
                    {
                        id: 'settings',
                        caption: message('me.mpersonalcenter.settings.caption'),
                        icon: 'icon-settings',
                        arrowVisible: true,
                        type: SettingsListItemType.Cell
                    },
                ]
            });

            this.settings = new SettingsList({
                domParent: domBase,
                dataProvider: this.dataProvider
            });
        });
        return domBase;
    }

    public dispose(): void {
        if (this.domBase) {
            this.settings.dispose();
            this.domBase = null;
        }
        super.dispose();
    }

    public back(): Promise<void> {
        return this.settings.back();
    }

}