/**
 * ============================================================
 * 作者:tuzhengw
 * 审核人员：liuyongz
 * 创建日期：2022-01-17
 * 功能描述：
 *      该脚本功能：生成二维码并存储为图片写入数据库表字段
 * 提示：
 *    参考产品代码：QRCodeUtils.java
 * ============================================================
 */

function main() {
    let qRCodeUtils = new QRCodeUtils();
    print(qRCodeUtils.generateQRCode("https://www.cnblogs.com/liangjiejava/p/9850067.html", 200, 200, "png"));
}

import BufferedImage = java.awt.image.BufferedImage;
import File_ = java.nio.file.File;
import ImageIO = javax.imageio.ImageIO;
import BarcodeFormat = com.google.zxing.BarcodeFormat;
import EncodeHintType = com.google.zxing.EncodeHintType;
import MultiFormatWriter = com.google.zxing.MultiFormatWriter;
import WriterException = com.google.zxing.WriterException;
import BitMatrix = com.google.zxing.common.BitMatrix;
import ErrorCorrectionLevel = com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import FileUtils = com.succez.commons.util.io.FileUtils;
import ByteArrayInputStream = java.io.ByteArrayInputStream;
import ByteArrayOutputStream = java.io.ByteArrayOutputStream;
// import BASE64Decoder = sun.misc.BASE64Decoder;
// import BASE64Encoder = sun.misc.BASE64Encoder;
import FilenameUtils = org.apache.commons.io.FilenameUtils;
import StringUtils = org.apache.commons.lang.StringUtils;
import ResultSet = java.sql.ResultSet;
import M_Blob_ = java.sql.Blob;

import LocalDateTime = java.time.LocalDateTime;
import SimpleDateFormat = java.text.SimpleDateFormat;
import DateFormat_ = java.text.DateFormat;
import O_BLOB_ = oracle.sql.BLOB;

import { queryDwTableData, queryData, openDwTableData } from "svr-api/dw";
import { uuid } from "svr-api/utils";
import { getDataSource } from "svr-api/db";

/**
 * 二维码工具类
 */
class QRCodeUtils {
    /** 图案的颜色，默认：0xFF000000   */
    private BLACK: string = "0xFF000000";
    /** 背景色，默认：0xFFFFFFFF */
    private WHITE: string = "0xFFFFFFFF";
    /** 二维码宽度，默认：30 */
    private DEFAULT_WIDTH: number = 300;
    /** 二维码高度，默认：300 */
    private DEFAULT_HEIGHT: number = 300;
    /** 二维码图片格式，默认：png */
    private DEFAULT_TYPE: string = "png";

    /**
     * 生成二维码，并返回对应的二进制流
     * @param codePath 二维码路径
     * @param width 二维码宽
     * @param height 二维码高
     * @param type 二维码图片类型，eg："jpg"
     * @return 二进制流数组（无Base64加密）
     */
    public generateQRCode(codePath: string, width: number, height: number, codeType: string): number[] {
        if (!codePath) {
            console.debug(`generateQRCode()，生成二维码，并返回对应的二进制流：生成二维码的链接不能为NULL`);
            return [];
        }
        if (StringUtils.isEmpty(codeType)) {
            codeType = this.DEFAULT_TYPE;
        }
        try {
            let bitMatrix: BitMatrix = this.getBitMarix(codePath, width, height); // 生成二维码 
            let image: BufferedImage = this.toBufferedImage(bitMatrix);
            let out = new ByteArrayOutputStream();
            ImageIO.write(image, codeType, out);
            let bytes: number[] = out.toByteArray();
            return bytes;
        } catch (e) {
            console.error(`generateQRCode()，生成二维码，并返回对应的二进制流：error`);
            console.error(e);
        }
    }
    /**
     * 将文件二进制流转换为图片
     * @param bytes 文件二进制流
     * @param fileSaveAbsolutePath 文件的绝对路径（包含：文件类型）
     * @param imageType 图片类型，eg：jpg、png、gif
     * 参考：https://www.cnblogs.com/oumyye/p/4306219.html
     */
    public fileBufferToImage(bytes: number[], fileSaveAbsolutePath: string, imageType: string): void {
        if (!bytes || !fileSaveAbsolutePath || !imageType) {
            console.debug(`fileBufferToImage()，将文件二进制流转换为图片：参数存在不合法`);
            return;
        }
        try {
            let bais = new ByteArrayInputStream(bytes);
            let image: BufferedImage = ImageIO.read(bais);
            let file = new File_(fileSaveAbsolutePath);
            ImageIO.write(image, imageType, file);
        } catch (e) {
            console.error(`fileBufferToImage()，将文件二进制流转换为图片：error`);
            console.error(e);
        }
    }

    /**
     * 生成二维码，并转换为图片
     * @param os 输出对象
     * @param codePath 二维码内容链接
     * @param width 二维码高
     * @param height 二维码宽
     * @param codeType 文件类型
     */
    public generateQRCode1(os: OutputStream, codePath: string, width: number, height: number, codeType: string): void {
        let bitMatrix: BitMatrix = this.getBitMarix(codePath, width, height); // 生成二维码 
        if (StringUtils.isEmpty(codeType)) {
            codeType = this.DEFAULT_TYPE;
        }
        this.writeToStream(bitMatrix, codeType, os);
    }

    /**
     * 生成二维码，并转换为图片
     * @param imgpath 二维码图片路径
	 * @param codePath 二维码内容链接
	 * @param width 图片宽度
	 * @param height 图片高度
	 * @param codeType 图片格式
     */
    public generateQRCode2(imgPath: string, codePath: string, width?: number, height?: number, codeType?: string): void {
        let bitMatrix: BitMatrix = this.getBitMarix(codePath, width, height);
        let file = new File_(imgPath); // 初始二维码图片文件
        if (!file.exists()) {
            FileUtils.forceCreateFile(file);
        }
        codeType = StringUtils.isEmpty(codeType) ? this.DEFAULT_TYPE : codeType;
        let actualType = FilenameUtils.getExtension(imgPath); // 通过文件路径，获取文件的扩展名
        if (!StringUtils.isEmpty(actualType)) {
            codeType = actualType;
        }
        this.writeToFile(bitMatrix, codeType, file);
    }

    /**
     * 生成二维码，并转换为图片
     * @param imageFile 文件对象
	 * @param codePath 二维码内容链接
	 * @param width 图片宽度
	 * @param height 图片高度
	 * @param type 图片格式
     */
    public generateQRCode3(imageFile: File_, codePath: string, width?: number, height?: number, codeType?: string): void {
        if (imageFile == null) {
            console.debug(`generateQRCode3()，生成二维码，并转换为图片，文件对象为NULL`);
            return;
        }
        if (!imageFile.exists()) {
            FileUtils.forceCreateFile(imageFile);
        }
        let bitMatrix: BitMatrix = this.getBitMarix(codePath, width, height);
        codeType = StringUtils.isEmpty(codeType) ? this.DEFAULT_TYPE : codeType;
        let actualType = FilenameUtils.getExtension(imageFile); // 通过文件对象，获取文件的扩展名
        if (!StringUtils.isEmpty(actualType)) {
            codeType = actualType;
        }
        this.writeToFile(bitMatrix, codeType, imageFile);
    }

    /**
     * 设置二维码信息
     * @param content 二维码内容链接
	 * @param width 图片宽度
	 * @param height 图片高度
     * @return BitMatrix对象
     */
    private getBitMarix(codePath: string, width: number, height: number): BitMatrix {
        let hints = new java.util.Hashtable();
        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);// 指定纠错等级，纠错级别（L 7%、M 15%、Q 25%、H 30%）  
        hints.put(EncodeHintType.CHARACTER_SET, "utf-8");// 内容所使用字符集编码  
        hints.put(EncodeHintType.MARGIN, 1);//  设置二维码边的空度，非负数  
        width = width <= 0 ? this.DEFAULT_WIDTH : width;
        height = height <= 0 ? this.DEFAULT_HEIGHT : height;
        let bitMatrix: BitMatrix = new MultiFormatWriter().encode(codePath, BarcodeFormat.QR_CODE, width, height, hints);
        return bitMatrix;
    }

    /**
     * 将二维码信息写入文件
	 * @param matrix 二维码信息
	 * @param format 文件格式
	 * @param file 文件对象
     * @return 图片流
     */
    private writeToFile(bitMatrix: BitMatrix, format: string, file: File_): void {
        let image: BufferedImage = this.toBufferedImage(bitMatrix);
        if (!ImageIO.write(image, format, file)) {
            console.debug(`生成二维码图片失败`);
            throw new Error("生成二维码图片失败");
            return;
        }
        console.debug(`生成二维码图片成功`);
    }

    /**
     * 将二维码对象信息生成图片
     * @param bitMatrix 二维码信息对象
     * @return 二维码转换后的图片流
     */
    private toBufferedImage(bitMatrix: BitMatrix): BufferedImage {
        let width: number = bitMatrix.getWidth();
        let height: number = bitMatrix.getHeight();
        let image: BufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        for (let x = 0; x < width; x++) {
            for (let y = 0; y < height; y++) {
                image.setRGB(x, y, (bitMatrix.get(x, y) ? this.BLACK : this.WHITE));
            }
        }
        return image;
    }

    /**
     * 将二维码信息写入输出流
     * @param matrix 二维码信息
	 * @param format 图片格式，eg：png
	 * @param stream 输出流
     */
    private writeToStream(matrix: BitMatrix, format: string, stream: OutputStream): void {
        let image: BufferedImage = this.toBufferedImage(matrix);
        if (!ImageIO.write(image, format, stream)) {
            console.debug(`生成二维码图片失败`);
            throw new Error("生成二维码图片失败");
            return;
        }
        console.debug(`生成二维码图片成功`);
    }
}

