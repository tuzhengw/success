import { FAppInterActionEvent, InterActionEvent, IVPage } from "metadata/metadata-script-api";
import { Combobox } from "commons/basic";
/**
 * 给指定下拉框指定勾选选项
 * @param page 当前SPG页面对象
 * @param setCombobox 需要设置勾选的下拉框信息
 * @param 
 */
private setComboboxValues(page: IVPage, setCombobox: ComboboxSetting[]): void {
	if (setCombobox.length == 0) {
		return;
	}
	for (let i = 0; i < setCombobox.length; i++) {
		let comboboxSet = setCombobox[i];
		let combobox: Combobox = page.getComponent(comboboxSet.comboboxId).component.getInnerComoponent();
		/**
		 * 下拉框设置维项过滤，同时刷新当前选项值
		 * 注意：若下拉框设置了维项过滤，那么set的值会在刷新的过程中丢失，建议：直接set到页面某个全局参数，组件使用：计算
		 */
		combobox.setValue(comboboxSet.comboboxCheckedValues, true);
	}
}


/** 下拉框配置 */
interface ComboboxSetting {
	/** 下拉框组件ID值 */
	comboboxId: string;
	/** 下拉框需要勾选的值 */
	comboboxCheckedValues: string[];
}