
import {
	SZEvent, Component, rc, browser, showInfoDialog, showWaiting, downloadFile, showMenu, deepEqual, showSuccessMessage, showErrorMessage, showWarningMessage,
	BASIC_EVENT, showConfirmDialog
} from 'sys/sys';
import { ICustomJS, IMetaFileCustomJS, InterActionEvent } from "metadata/metadata-script-api";
import { TableCellEditor } from "commons/table";

/**
 * 注意：
 * （1）列表勾选值除非刷新，否则，不会自动删除，需要手动调用脚本清理
 * （2）勾选完后，点击确定——置空：listComponent.historyCheckedRows = []; // 操作完后，将历史选项置空 下次打开时，列表会重新加载
 * （3）删除列表记录的勾选值，需要注意：删除后，下标的改变，解决办法：逆向循环删除，eg：
		for (let i = historyCheckedPks.length - 1; i >= 0; i--) { // 避免删除后，下标错位，用逆向循环删除
			let sourceTable = historyCheckedPks[i]['SOURCE_TBALE'];
			if (deleteSourceTables.includes(sourceTable)) {
				listComponent.historyCheckedPks.splice(i, 1);  // 去除当前从配置表删除的来源表信息
			}
		}
		
 * 列表多页勾选
 */
export class List_Multipage_CheckedRows {
	public CustomActions: any;
	/** 
	 * 根据历史勾选值重新渲染页面勾选行数据
	 * 注意：列表绑定的模型必须包含主键
	 * 思路：可以将勾选的值记录到当前指定的列表控件上，新增属性
	 * （1）historyCheckedPks ：勾选的主键值（用于渲染历史勾选值）
	 * （2）historyCheckedRows ：勾选的行列值（包含主键）
	 * 1. 若当前没有这个属性，则表示暂时没有勾选数据，并给列表增加这个属性
	 * 2. 若存在此属性，读取属性的值，加载页面之前已经勾选的数据
	 * @param listId 处理的列表id值
	 * @return 当前勾选的所有数据集，值可能为：null undefined
	 */
	public getListCheckedRows(event: InterActionEvent): void {
		let page = event.page;
		let params = event?.params as JSONObject;
		let listId: string = params.listId;
		if (!listId) {
			showWarningMessage(`未指定要处理的列表id值`);
			return;
		}
		let listComponent = page.getComponent(listId);
		let modelId: string = listComponent.node.metaInfo && listComponent.node.metaInfo.dataSet;
		if (!modelId) {
			showWarningMessage(`当前列表未绑定模型`);
			return;
		}
		/** 列表绑定模型主键字段 */
		let currentModelskeys: string[] = page.getDataset(modelId).getPrimaryKeys();
		if (!currentModelskeys || currentModelskeys.length == 0) {
			showWarningMessage(`列表绑定的模型没有设置主键，不支持多页勾选功能`);
			return;
		}
		/** 列表显示的列字段名集 */
		let listDisplayFileds: string[] = this.getListDisplayFields(page.getDataset(modelId).getAllFields());
		if (listDisplayFileds.length == 0) {
			showWarningMessage(`没有获取到列表绑定的模型的物理表字段名`);
			return;
		}
		/** 列表控件 */
		let tableEditor = listComponent.component.tableEditor;
		if (tableEditor) {
			let oninlinecompchange_old = tableEditor.oninlinecompchange; // 保留原有列表控件的：oninlinecompchange方法
			this.loadHistoryCheckedRows(page, listComponent, currentModelskeys);
			if (!tableEditor.hasOnRender) {
				tableEditor.hasOnRender = true;
				tableEditor.oninlinecompchange = (event: SZEvent, cell: TableCellEditor, checkComp: any) => {
					let lastCompDatas: JSONObject[] = listComponent.getCheckedDataRows();
					oninlinecompchange_old(event, cell, checkComp); //  走原有的方法流程，获取当前页选中的最新节点
					let newCompDatas: JSONObject[] = listComponent.getCheckedDataRows();
					this.deleteAndUpdateCheckedRows(listComponent, lastCompDatas, newCompDatas, currentModelskeys, listDisplayFileds);
				}
			}
		}
	}

	/**
	 * 重新设置勾选（仅处理当前页历史勾选数据）
	 * @param page 当前页对象
	 * @param listComponent 列表控件对象
	 * @param currentModelskeys 当前列表绑定模型的主键数组
	 */
	private loadHistoryCheckedRows(page, listComponent, currentModelskeys: Array<string>): void {
		let modelId: string = listComponent.node.metaInfo.dataSet;
		let modelData = page.getDataset(modelId);
		/** 当前页面显示的行数据 */
		let currentListPageDatas = this.formatJsObj(modelData.getRows(), currentModelskeys);
		if (currentListPageDatas.length == 0) {
			return;
		}
		/** 历史勾选的数据（新增的属性） */
		let historyCheckedPks = listComponent.historyCheckedPks;
		if (!historyCheckedPks || historyCheckedPks.length == 0) {
			return;
		}
		/** 记录当前页历史勾选数据下标 */
		let currentListCheckedRowsIndex: number[] = [];
		for (let i = 0; i < currentListPageDatas.length; i++) {
			for (let j = 0; j < historyCheckedPks.length; j++) { // 校验当前数据是否之前勾选过
				if (objectUtils.compare(currentListPageDatas[i], historyCheckedPks[j])) {
					currentListCheckedRowsIndex.push(i);
					break;
				}
			}
		}
		if (currentListCheckedRowsIndex.length == 0) {
			return;
		}
		let listRefreshPromise = listComponent.component.refreshPromise;
		if (!!listRefreshPromise) { // 初次加载时，reFreshPromise对象为空，showWaiting()方法返回的不是一个promise对象
			listComponent.component.lastCheckedRows = []; // 在set前，将上一次勾选的值数组置空，避免页面过滤后，setCheckedRows内部取消上次勾选值失败
			/**
			 * 问题：当过滤列表数据时，列表对象的重新渲染方法和setCheckedRows()方法内的重新渲染方法冲突，导致列表无法显示过滤后的数据信息
			 * 解决办法：等待列表重新渲染方法结束后，再执行数据驱动渲染表格的勾选行方法：setCheckedRows
			 */
			showWaiting(listRefreshPromise).then(() => {
				listComponent.component.setCheckedRows(currentListCheckedRowsIndex);
			});
		} else {
			listComponent.component.setCheckedRows(currentListCheckedRowsIndex);
		}
	}

	/**
	 * 将前端数据对象转换为指定属性的JSON数组
	 * （1）由于前端js对象包含的属性很多，这里根据【主键数组】保留相关成员属性值
	 * @param currentListPageDatas 处理的前端js对象数组
	 * @param currentModelskeys 当前列表绑定模型的主键数组
	 */
	public formatJsObj(currentListPageDatas, currentModelskeys: string[]) {
		if (!currentListPageDatas || currentListPageDatas.length == 0) {
			return [];
		}
		let dealedData = []; // 记录转换后的数据集
		let currenttListPageSize = currentListPageDatas.length;
		for (let i = 0; i < currenttListPageSize; i++) {
			let jsonItems = currentListPageDatas[i].getJSON();
			let temp = {};
			currentModelskeys.forEach(key => {
				temp[`${key}`] = jsonItems[`${key}`];
			});
			dealedData.push(temp);
		}
		return dealedData;
	}

	/**
	 * 将最新选中的行值与之前选中的行值数组对比差异
	 * @parma listComponent 列表控件对象
	 * @param lastCompDatas 获取当前页上次选中节点
	 * @param newCompDatas 获取当前页所选中节点
	 * @param currentModelskeys 当前列表绑定模型的主键数组
	 * @param listDisplayFileds 列表显示的列字段名数组
	 */
	public deleteAndUpdateCheckedRows(listComponent, lastCompDatas, newCompDatas, currentModelskeys: string[], listDisplayFileds: string[]): void {
		if (lastCompDatas == null || newCompDatas == null) {
			return;
		}
		let lastCheckedRowsPks = [];
		let nowCheckedRowsPks = [];
		let lastCheckedRowsDatas = [];
		let nowCheckedRowsDatas = [];

		this.classifyArrDatas(lastCompDatas, currentModelskeys, lastCheckedRowsPks, lastCheckedRowsDatas, listDisplayFileds);
		this.classifyArrDatas(newCompDatas, currentModelskeys, nowCheckedRowsPks, nowCheckedRowsDatas, listDisplayFileds)

		let historyCheckedPks = listComponent.historyCheckedPks;
		let historyCheckedRows = listComponent.historyCheckedRows;
		// 若初次记录，则直接记录当前页勾选的值（注意：主键集和选择行集数量是一致的，且一一对应）
		if (lastCheckedRowsPks.length == 0 && (nowCheckedRowsPks.length > 0 && (!historyCheckedPks || historyCheckedPks.length == 0))) {
			listComponent.historyCheckedPks = nowCheckedRowsPks;
			listComponent.historyCheckedRows = nowCheckedRowsDatas;
			return;
		}
		lastCheckedRowsPks.forEach(removeItem => { // 去除当前页上一次勾选的值
			for (let i = 0; i < historyCheckedPks.length; i++) {
				// 由于内部循环仅校验值是否删除，删除后，则结束循环，下标的改变不影响
				if (objectUtils.compare(historyCheckedPks[i], removeItem)) {
					historyCheckedPks.remove(i);
					historyCheckedRows.remove(i);
					break;
				}
			}
		});
		nowCheckedRowsPks.forEach(item => { // 将当前页最新勾选节点值存入数组
			historyCheckedPks.push(item);
		});
		nowCheckedRowsDatas.forEach(item => { // 将当前页最新勾选节点值存入数组
			historyCheckedRows.push(item);
		});
		listComponent.historyCheckedPks = this.removeRepeatDatas(historyCheckedPks);
		listComponent.historyCheckedRows = this.removeRepeatDatas(historyCheckedRows);
	}

	/**
	 * 分类传入的数组数据（主键值和行数据值）
	 * @param data 当前列表所选行数组
	 * @param currentModelskeys 列表绑定模型的主键字段数组
	 * @param pks 记录每行的主键值数组
	 * @param rows 记录每行的行值数组
	 * @param listDisplayFileds 列表显示的列字段名数组
	 */
	private classifyArrDatas(data, currentModelskeys: string[], pks, rows, listDisplayFileds: string[]): void {
		data.forEach(item => {
			let tempPk = {};
			let tempRow = {};
			for (let i = 0; i < listDisplayFileds.length; i++) {
				let filed = listDisplayFileds[i];
				if (filed.indexOf("$") != -1) { // 去掉对象非key的属性，eg：$XM
					continue;
				}
				if (currentModelskeys.includes(filed)) { // 校验当前字段是否为主键字段
					tempPk[`${filed}`] = item[`${filed}`];
				}
				tempRow[`${filed}`] = item[`${filed}`];
			}
			pks.push(tempPk);
			rows.push(tempRow);
		});
	}

	/**
	 * 获取列表显示的物理字段列名
	 * @param 当前列表绑定模型列对象
	 * @return 物理表字段名数组
	 */
	private getListDisplayFields(modelAllFields): string[] {
		if (!modelAllFields) {
			return [];
		}
		let listDisplayDbFileds: string[] = [];
		for (let i = 0; i < modelAllFields.length; i++) {
			let dbFiled: string = modelAllFields[i].getDbField();
			listDisplayDbFileds.push(dbFiled);
		}
		return listDisplayDbFileds;
	}

	/**
	 * 去除对象数组的重复值
	 * 注意：必须保证对象内【没有包含】对象
	 * @params obj：需要处理的对象数组
	 * @return uniques：去除重复值后的对象数组
	 */
	public removeRepeatDatas(objArray) {
		if (!objArray) {
			return [];
		}
		let uniques = [];
		let stringify = {};
		for (let i = 0; i < objArray.length; i++) {
			let keys = Object.keys(objArray[i]);
			keys.sort(function (a, b) {
				return (Number(a) - Number(b));
			});
			let str = '';
			for (let j = 0; j < keys.length; j++) {
				str += JSON.stringify(keys[j]);
				str += JSON.stringify(objArray[i][keys[j]]);
			}
			if (!stringify.hasOwnProperty(str)) {
				uniques.push(objArray[i]);
				stringify[str] = true;
			}
		}
		uniques = uniques;
		return uniques;
	}
}

/**
 * 对象比较工具类
 */
class ObjectUtils {
	/**
	 * 校验两个对象是否相等（包含：JSON对象）
	 */
	public compare(objA, objB) {
		if (!this.isObj(objA) || !this.isObj(objB)) { // 判断类型是否正确
			return false;
		}
		if (this.getLength(objA) != this.getLength(objB)) { // 判断长度是否一致
			return false;
		}
		return this.compareObj(objA, objB, true); // 默认为true
	}

	/**
	 * 递归校验两个对象是否相等
	 * @param objA 比较对象1
	 * @param objB 比较对象2
	 * @param 是否相等，初始默认true
	 */
	private compareObj(objA, objB, flag) {
		for (let key in objA) {
			if (!flag) { //跳出整个循环
				break;
			}
			if (!objB.hasOwnProperty(key)) {
				flag = false; break;
			}
			if (!this.isArray(objA[key])) { // 子级不是数组时,比较属性值
				if (objB[key] != objA[key]) {
					flag = false; break;
				}
			} else {
				if (!this.isArray(objB[key])) {
					flag = false; break;
				}
				let oA = objA[key], oB = objB[key];
				if (oA.length != oB.length) {
					flag = false; break;
				}
				for (let k in oA) {
					if (!flag) // 这里跳出循环是为了不让递归继续
						break;
					flag = this.compareObj(oA[k], oB[k], flag);
				}
			}
		}
		return flag;
	}

	/**
	 * 校验是否为对象
	 */
	public isObj(object) {
		return object && typeof (object) == 'object' && Object.prototype.toString.call(object).toLowerCase() == "[object object]";
	}
	/**
	 * 校验是否为数组
	 */
	public isArray(object) {
		return object && typeof (object) == 'object' && object.constructor == Array;
	}

	/**
	 * 获取对象长度
	 */
	public getLength(object) {
		let count = 0;
		for (let i in object) count++;
		return count;
	}
}
let objectUtils = new ObjectUtils();


export const CustomJS: { [file_OR_type_OR_resid: string]: IMetaFileCustomJS } = {
	/**
	 * 用文件名设置脚本作用到哪个具体的仪表板、报表或spg等，文件名包含文件扩展名
	 */
	"list_test.spg": {
		onRender: (event: InterActionEvent): void | Promise<void> => {
			let id = event.component && event.component.getId();
			if (id == "list1") { // 仅列表控件调用
				event.params = {
					listId: "list1"
				}
				let choseDatas = listMultipageChose.getListCheckedRows(event);
				console.log(choseDatas);
			}
		}
		
		/**
		 * 注意：功能执行完后，要清空列表勾选的值 和 记录的主键值
		 * 
		 * 将勾选的同步表信息暂存写入到
		 * @param listId 列表控件id
		 * @param dataBaseId 数据库ID
		 * @param tablePrefix 物理表前缀
		 */
		 button_ac_tempInsertChoseSyncTables(event: InterActionEvent): Promise<boolean> {
			let page = event.page;
			let params = event?.params;
			let listId: string = params.listId;
			if (!listId) {
				showWarningMessage('暂存同步表，参数未传递，未给定列表ID值');
				return Promise.resolve(false);
			}
			let dataBaseId: string = params.dataBaseId;

			let userId = getCurrentUser().userId;
			let listComponent = page.getComponent(listId);
			/** 列表所勾选的行数据 */
			let historyChoseDatas = listComponent.historyCheckedRows;
			if (!historyChoseDatas || historyChoseDatas.length == 0) {
				showWarningMessage('请选择至少一张同步表');
				return Promise.resolve(false);
			}
			/** 数据接入配置表数据对象 */
			let tableConfDataset: IDataset = page.getDataset('model4');
			/** 保存已经暂存过的来源表名 */
			let saveDealSourceNames: string[] = [];

			/** 获得已添加到接入表中的来源表信息 */
			let addedTables = tableConfDataset.getRows();
			addedTables.forEach(item => {
				let data = item.getJSON();
				saveDealSourceNames.push(data['SOURCE_TABLE']);
			});
			let insertRows: SDI_DATA_ACCESS_TABLES_CONF[] = [];
			for (let i = 0; i < historyChoseDatas.length; i++) {
				let rowData = historyChoseDatas[i];
				let sourceTableName: string = rowData['SOURCE_TBALE'];
				// 由于同步表中存在来源表UUID字段，这个字段值是动态生成的，每次勾选都会重新生成一个uuid值，无法去重，这里需要根据【来源表名】校验是否已经配置
				if (saveDealSourceNames.includes(sourceTableName)) {
					continue;
				}
				saveDealSourceNames.push(sourceTableName);
				insertRows.push({
					'UUID': uuid(),
					'DATABASE_ID': dataBaseId,
					'SOURCE_TABLE': sourceTableName,
					'SCHEDULE_CODE': 'default',
					'POLICY': 'TOTAL',
					'CREATE_TIME': new Date().getTime(),
					'CREATOR': userId
				});
			}
			if (insertRows.length == 0) {
				showWarningMessage(`请选择没有导入的同步表信息`);
				return Promise.resolve(false);
			}
			tableConfDataset.insert(insertRows); // 写入暂存中
			listComponent.historyCheckedRows = []; // 操作完后，将历史选项置空
			// listComponent.historyCheckedPks = [];
			return Promise.resolve(true);
		}
	}
}

