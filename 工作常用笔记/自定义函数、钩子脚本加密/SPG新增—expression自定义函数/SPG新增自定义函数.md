---
typora-root-url: images
---

# 一`SPG`新增自定义函数

> 网址：https://docs.succbi.com/dev/extension-points/expression/#manurl-md

## 1 效果

​		自定义一个`GET_NUMBERS_DATA_DURATION(numbers)`函数，解析某数值，返回时间段信息。

![](../images/效果.png)

## 2 案例demo

​		下载公司产品代码，打开<button>工作区</button>

![](../images/案例.png)



## 3 文件结构介绍

1. [package.json](https://docs.succbi.com/dev/extension-points/expression/#package-json) 定义扩展的配置信息
2. [main.ts](https://docs.succbi.com/dev/extension-points/expression/#main-ts) 定义和实现前端扩展函数
3. [main.action.ts](https://docs.succbi.com/dev/extension-points/expression/#main-action-ts) 定义和实现后端扩展函数
4. [manURL.md](https://docs.succbi.com/dev/extension-points/expression/#manurl-md) 定义帮助文档

推荐同时实现前端和后端函数，但<button>也可只实现一种</button>，同时实现时，需要确保前后端<button>的函数名相同</button>

> 详情：https://docs.succbi.com/dev/extension-points/expression/#manurl-md

---

# 二 实现自定义扩展函数

# `GET_NUMBERS_DATA_DURATION`

## 1 编写位置

​		在"系统资源"的"extensions"目录下创建一个文件夹

![](../images/位置.png)

## 2 编写扩展配置信息**`package.json`**

```json
{
    "name": "succ-repressionFunction-basic",
    "displayName": "解析某数值，返回时间段信息",
    "description": "一个空白的表达式扩展模板",
    "version": "1.0.0",
    "author": {
        "name": "tuzw"
    },
    "categories": [
        "other"
    ],
    "main": "main",
    "contributes": {
        "expressionFunction": [
            {
                // 这里的name与main.ts中的方法名一一对应，否则无法识别
                "name": "GET_NUMBERS_DATA_DURATION",
                "description": "解析某数值，返回时间段信息",
                "manURL": "GET_NUMBERS_DATA_DURATION.md",
                "group": "others",
                "returnType": "String",
                "async": true,
                "params": [
                    {
                        "name": "numbers",
                        "returnType": "number"
                    }
                ]
            }
        ]
    }
}
```

## 3 编写前端配置文件**`main.ts`**

```ts
import { IExpEvalDataProvider } from "commons/exp/expeval";

/**
 * 解析某数值，返回时间段信息
 * @param context：SPG对象
 * @param numbers 处理的数值（必须为number，且不小于0）
 * @return eg: 13475527 ———— 4小时44分35秒
 */
export function GET_NUMBERS_DATA_DURATION(context: IExpEvalDataProvider, numbers: string): string {
    if (!isNumber(numbers)) {
        return "参数错误";
    }
    return getFriendlyDuration(parseFloat(numbers));
}

/**
 * 将某毫秒数转换为时间段信息
 * @param numbers 处理的数值（毫秒）
 * @return eg: 13475527 ———— 4小时44分
 */
export function getFriendlyDuration(numbers: number): string {
    if (typeof numbers != "number" || numbers < 0) {
        return "typeError";
    }
    // 若1秒都没有，则保留2位小数
    if (numbers < 1000) {
        return `${(numbers / 1000.0).toFixed(2)}秒`;
    }
    numbers = Math.floor(numbers / 1000); /** 转换为秒 */
    let hour = Math.floor(numbers / 3600);
    let minute = Math.floor(numbers % 3600 / 60); /** numbers % 3600 去除"时"，获取剩下的"分" */
    let second = numbers % 60;
    if (numbers < 60) {
        return `${second}秒`;
    }
    if (numbers < 60 * 60) {
        return `${minute}分${second}秒`;
    }
    return `${hour}时${minute}分${second}秒`;
}


/**
 * 校验字符串是否为数字
 */
function isNumber(val) {
    var regPos = /^\d+(\.\d+)?$/; //非负浮点数
    var regNeg = /^(-(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*)))$/; //负浮点数
    if (regPos.test(val) || regNeg.test(val)) {
        return true;
    } else {
        return false;
    }
}

```

## 4 编写函数定义**`GET_NUMBERS_DATA_DURATION.md`**

```md
---
permalink: /exp/func/get_numbers_data_duration
sidebarDepth: 0
---
# **GET_NUMBERS_DATA_DURATION(numbers)**

解析某数值，返回时间段信息。

**类型必须是number，且不小于0!**

## 参数

***numbers***：需要对其转换的数值（毫秒数）。

## 示例

1. `GET_NUMBERS_DATA_DURATION(59 * 1000)` 59秒
2. `GET_NUMBERS_DATA_DURATION((44 * 60 + 2 * 60) * 1000)` 44分2秒
3. `GET_NUMBERS_DATA_DURATION(13475527)` 3时44分35秒
```

## 5 测试

​		处理完上述步骤，`SPG`会<button>自动扫描将其函数添加</button>到指定<button>函数类型组</button>中

```
${GET_NUMBERS_DATA_DURATION(13475527)}
```

![](../images/效果.png)