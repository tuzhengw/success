/**  导入的路径在产品代码的的最上方 */
import { SZEvent, Component, rc, browser, showInfoDialog, showWaiting, downloadFile, showMenu, deepEqual, showSuccessMessage, showErrorMessage, BASIC_EVENT } from 'sys/sys';
import { ICustomJS, IMetaFileCustomJS, InterActionEvent, IVPage, IAppCustomJS, DatasetDataPackageRowInfo, IVComponent, InputDataChangeInfo } from "metadata/metadata-script-api";
import { TableCellEditor } from "commons/table";
import { Checkbox, Paginator } from "commons/basic";

export const CustomJS: { [type: string]: IMetaFileCustomJS } = {
    spg: {
        CustomActions: {
            /**
             * 导出选中的列表数据（实际为指定数据库的数据）
             */
            importListDatas: (event: InterActionEvent) => {
				/** 
					其他参数见产品代码：DataFlowNodeInfo类（dw-meta—types.d.ts）
					- 点击参数的类型，可以查看某参数的属性
				*/
                let fieldQueryInfo: QueryInfo = {
                    /**
                     * 这里查询字段（column）和表达式（exp）对应
                     * name：导出的文件列名
                     * exp：主要是计算当前字段显示的值（可以是聚合函数）
                     */
                    // fields: [{ name: "身份证号码", exp: "model1.SFZJHM" }, { name: "数据期起", exp: "model1.SJQZ" }],
					fields: [{ name: "QYMC" }, { name: "TYSHXYDM" }], // 产品导出列格式改变了
					/** 
						根据SPG显示列，动态生成fields条件
						let superPageBuilder: SuperPageBuilder = event.component && event.component.node.builder;
						fields: getDisplayColumnNames(superPageBuilder);
					*/
                    sources: [{
                        /** 模型表别名（model1） */
                        id: "model1",
                        /** 模型表（可为物理表）路径 */
                        path: "/rlzy/data/tables/fact/FACT_RLZY_YYRSXX.tbl"
                    }],
                    /**
                     * 过滤条件：FilterClauseInfo类
                     * leftExp：模型表的字段表达式
                     * operator：计算操作（eg：==）
                     * rightValue：比较值（单个值 `"2019/05/17"`、`"420000"`、多选值 `["420000","430000"]`、区间   `"[10~100]"`）
					 * 《rightExp : rightValue 一个即可》
                     * eg：leftExp == rightValue
                     */
                    filter: 
						/** clasues：一条一条添加关联条件，这些条件之间是AND关系 */
						/** 若需要使用or，则使用：exp */
                        [
                            {
                                "clauses": [{
                                    "leftExp": "model1.SFZJHM",
                                    "operator": ClauseOperatorType.DoubleEqual,
                                    "rightValue": "642221199802112624"
                                }, {
                                    "leftExp": "model1.SJQQ",
                                    "operator": ClauseOperatorType.DoubleEqual,
                                    "rightValue": "20211026"
                                }, {
                                    "leftExp": "model1.SJQZ",
                                    "operator": ClauseOperatorType.DoubleEqual,
                                    "rightValue": "22991231"
                                }],
                                /** 是否and连接，默认：true */
                                "matchAll": false
                            }
                            , {
                                "clauses": [{
                                    "leftExp": "model1.SFZJHM",
                                    "operator": ClauseOperatorType.DoubleEqual,
                                    "rightValue": "622825199309032713"
                                }, {
                                    "leftExp": "model1.SJQQ",
                                    "operator": ClauseOperatorType.DoubleEqual,
                                    "rightValue": "20211026"
                                }, {
                                    "leftExp": "model1.SJQZ",
                                    "operator": ClauseOperatorType.DoubleEqual,
                                    "rightValue": "22991231"
                                }],
                                /** 是否and连接，默认：true */
                                "matchAll": false
                            }
                        ]
					/*
					 * filter格式可以不一样，可以传递SQL条件
						filter: [{ 
							exp: `model1.EX_DEPT='${ex_dept}' and model1.EXCHANGE_MODE='${ex_type}'` 
						}]
					*/
					/*
						let filter = isSupply == '1' 
							? `model1.BCSQRWID= '` + supply_id + `' and model1.RESID=model2.DX_RES_ID` 
							: `model1.EX_TASK_ID= '` + task_id + `' and model1.RESID=model2.DX_RES_ID`;
					*/
					/*
						filter: [{
							不能作用于多个字段，单个字段 in 可以
							in 有数量限制，model1.SJQQ+model1.SJQZ+model1.SFZJHM in (xxxx)
							
							// exp: `(model1.SJQQ, model1.SJQZ) in (('20211026','22991231'),('20211026','22991231'))`
							exp: `model1.DX_CHANNEL_ID in  ('` + filterList.join("','") + `')` 
						}]
						
					*/
					/*
					options: {
						limit: 10,
						offset: 0,
						queryTotalRowCount: true  // 设置获取【数据总数】
					}
					*/
                }
                return import("dw/dwapi").then((m) => {
                    m.exportQuery({
                        /** 模型表的resid主键，在查看路径的地方查看 */
                        resid: "yV6XMKpt92GdttZnYswrkF",
                        /** 查询条件（包含：查询字段、过滤条件、查询表） */
                        query: fieldQueryInfo,
                        fileFormat: DataFormat.xlsx,
						/** 默认为：模型表名字  */
                        fileName: "自定义文件名"
                    });
                });
            }
        }
    },
    "/rlzy/app/rlzy.app/rsxx/yy/test.spg": {
        /**
         * onRender每次页面变动都会执行，可能会导致某个固定值随机
         * eg：某页页码，因为加载n次，可能最终获取的页面值不是最终想要的，可以在变动的位置：重新捕获
         */
        onRender: (event: InterActionEvent): Promise<void> => {
           
            return Promise.resolve();
        }
    }
}


/**
 * 20211027 tuzw
 * 页面初始时，获取指定列表（对应的模型表）的列名和对应的数据库字段 
 * @params superPageBuilder：spg页面构建对象
 * <pre>
 * eg： fields: [
 *      { name: "姓名", exp: "model1.XM" },
 *      { name: "身份证号码", exp: "model1.SFZJHM" },
 *      ...
 * ]
 * 字段列表，字段顺序和查询定义的查询字段列表一一对应
 * </pre>
 */
function getDisplayColumnNames(superPageBuilder: SuperPageBuilder): Array<DwTableFieldCompiledInfo> {
    /** 获取当前页面所有的组件节点，找到list组件 */
    let allNodes: BaseNodeBuilder[] = superPageBuilder.allNodes;
    let spgListBuilder: SpgListBuilder = null;
    for (let i = 0; i < allNodes.length; i++) {
        if (allNodes[i].getId() == "list1") {
            spgListBuilder = allNodes[i] as SpgListBuilder;
            break;
        }
    }
    let spgListColumnBuilders = spgListBuilder.subComponents.columns as Array<SpgListColumnBuilder>;
    /** 存储当前（id：list1）列表显示的列 */
    let display_list_columns: Array<DwTableFieldCompiledInfo> = [];
    spgListColumnBuilders.forEach(item => {
        /** 校验当前列是否为数据库的列（去掉说明列和隐藏列） */
        if (item.metaInfo.field != undefined && (item.metaInfo.visible == undefined || item.metaInfo.visible == "true")) {
            let temp: DwTableFieldCompiledInfo = {
                /** 
                 * todo
                 * 注意：SPG新增列，需要修改标题（若是自动生成，先修改再还原），否则读取不到caption属性
                 *      name值不能为null 或者 undefined，否则导出会报错
                 */
                name: item.metaInfo.caption ? item.metaInfo.caption : item.metaInfo.field,
                exp: item.metaInfo.field
            }
            display_list_columns.push(temp);
        }
    });
    console.log("当前列表可导出的字段为");
    console.log(display_list_columns);
    return display_list_columns;
}
