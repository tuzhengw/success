

/**
 * 自动创建和修复SDI所有模型对应的物理表
 * @param restoreModelPaths 恢复的模型再压缩包路径集, eg: ['/sysdata/data/tables/co/COMMENTS.tbl']
 * <p>
 *  1 检查并修复模型, 将数据表调整为元数据定义的一致
 *  2 视图处理, checkAndRepair方法内部会自己忽略(修复仅针对于：设置提交物理表的模型)
 * </p>
 */
function repairSdiPhysicalTables(restoreModelPaths: string[]): void {
	this.logger.addLog(`开始执行[repairSdiPhysicalTables], 自动创建和修复SDI所有模型对应的物理表`);
	let dwServiceMeta = bean.getBean("com.succez.dw.services.impl.DwServiceMeta");
	let modelSaver = dwServiceMeta.getDwServiceModelSaver();
	for (let i = 0; i < restoreModelPaths.length; i++) {
		let zipModelPath: string = restoreModelPaths[i];
		let modelProjectPath: string = this.getRestoreActualPath(zipModelPath);
		let modelInfo: MetaFileInfo = metadata.getFile(modelProjectPath);
		if (modelInfo == null || modelInfo.type != 'tbl') {
			continue;
		}
		modelSaver.checkAndRepair(modelProjectPath, {}, this.progress);
	}
	this.logger.addLog(`执行结束[repairSdiPhysicalTables], 自动创建和修复SDI所有模型对应的物理表`);
}