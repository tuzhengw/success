/**
 * ============================================================
 * 作者:tuzhengw
 * 审核人员：liuyongz
 * 创建日期：2021-12-27
 * 功能描述：该脚本主要是用于【网络交易监管数据对接接口开发】---获取数据
 * 地址：https://jira.succez.com/browse/CSTM-18197
 *
 * IDEA反编译jar包：https://blog.csdn.net/weixin_30247781/article/details/101931298
 * Maven jar包下载：https://mvnrepository.com/artifact/com.alibaba/fastjson/1.2.59
 * 
 * 执行集群节点：http://2.20.103.167:8080/zhjg/syssettings/performance/jvmthreads
 * 
 * <p>
 *  20230602  tuzw
 *  共道网络交易主体、商品等打标信息接口开发
 *  地址: https://jira.succez.com/browse/CSTM-22778
 * </p>
 * ============================================================
 */
import HttpClients = org.apache.http.impl.client.HttpClients;
import EntityUtils = org.apache.http.util.EntityUtils;
import HttpGet = org.apache.http.client.methods.HttpGet;
import JavaRequestConfig = org.apache.http.client.config.RequestConfig;
const PoolingHttpClientConnectionManager = org.apache.http.impl.conn.PoolingHttpClientConnectionManager;

const GdGenericRequest = com.gongdao.yuncourt.security.model.GdGenericRequest; // 通过jar包内部源码查看
const GdApiClient = com.gongdao.yuncourt.security.GdApiClient;
const GdGenericResponse = com.gongdao.yuncourt.security.model.GdGenericResponse;
// const GdApiException = com.gongdao.yuncourt.security.exception.GdApiException;
// const GDClientV2Impl = com.gongdao.yuncourt.security.impl.GDClientV2Impl;
// const JSONObject = com.alibaba.fastjson.JSONObject;
// const JSONArray = com.alibaba.fastjson.JSONArray;
// const JSON_ = com.alibaba.fastjson.JSON;
import LocalDateTime = java.time.LocalDateTime;
import LocalTime = java.time.LocalTime
import ZoneOffset = java.time.ZoneOffset;

import { getString } from 'svr-api/metadata';
import { uuid } from "svr-api/utils";
import { getDataSource } from "svr-api/db";
import fs from "svr-api/fs";
import { getWorkDir } from "svr-api/sys";

/** 请求环境类型, eg: format 、 test */
let REQUEST_TYPE = 'format';

/** 应用秘钥 */
let APP_KEY: string = REQUEST_TYPE == "format" ? "saba" : "savo";
/** 标记秘钥 */
let SIGN_KEY: string = REQUEST_TYPE == "format" ? "xxx" : "xxx";
/** 数据秘钥 */
let DATA_KEY: string = REQUEST_TYPE == "format" ? "xxxx" : "xxxx";
/** 租户秘钥 */
let SECURITY_TENANT_ID: string = REQUEST_TYPE == "format" ? "xxxx" : "xxx";

/** 请求HOST地址 */
let REQUEST_HOST = REQUEST_TYPE == "format" ? GdApiClient.ONLINE_HOST_URL : GdApiClient.SANDBOX_HOST_URL;

/** 外部控制程序停止路径 */
const StopExecuteTxtPath: string = "/sdi/script/isStopExecute.txt";

/** 一次请求数量大小，对方请求最大：500（注意：首次全量请求，【线索表】每次请求不能超过200，内存不够） */
const PAGE_NUMS: number = 500;
/** 请求数据表的数据源 */
const DATA_BASE = REQUEST_TYPE == "format" ? "sjzt_ods" : "zhjg_ods";;

/**
 * 执行入口
 * @param requestPath 请求地址
 * @param tableName 请求数据存储的表名
 * @param outputFields 模型输出的字段属性（返回的字段顺序与当前平台属性不一致）
 * @return { result: true, rows: [][] }
 */
export function dealRequest(requestPath: string, tableName: string, outputFields: DbFieldInfo[]): ResultInfo {
    // return { result: false };
    print(`开始执行[dealRequest], 根据请求配置对象，请求网络交易监管数据对接接口，当前处理的请求路径为：${requestPath}，处理的表名为：${tableName}`);
    let start_time = new Date();
    let resultInfo: ResultInfo = { result: true };
    let rows: Array<Array<string>> = [];

    if (!requestPath || !tableName || !outputFields) {
        print(`---参数不对，requestPath：${requestPath}，tableName：${tableName}，outputFields：${outputFields}`);
        return { result: false };
    }
    let request: Request = getRequest(tableName);
    let responseResult: ResultInfo = requestInterfaceGetData(requestPath, request);  // 先发一次请求，获取当前数据的总数
    if (!responseResult.result) {
        print(`-----请求结束，${JSON.stringify(responseResult)}`);
        return { result: false };
    }
    request.page.length = PAGE_NUMS;
    print(request);
    let dataNums: number = responseResult.data['count'];
    let requestCounts: number = Math.ceil(dataNums / PAGE_NUMS);
    print(`---满足条件的数据总数为：【${!!dataNums ? dataNums : 0}】，每次请求数据量为：【${PAGE_NUMS}】，共需要请求次数为：【${requestCounts}】`);
    /**
     * 20220112 tuzw
     * 经过测试，对方数据库中的数据【没有排序】，新增数据可能在首页，故每次必须从第0页请求
     * 注意：
     * （1）数据模型采用：主键合并并追加，不用担心主键重复异常
     * （2）若需要重新全量，已加上若文件存在，则不继续创建文件
     */
    for (let count = 0; count < requestCounts; count++) {
        let isStop: string = getString(StopExecuteTxtPath);
        if (isStop == "true") {
            print(`---手动停止获取，第【${count + 1}】请求【${requestPath}】接口，共需要请求次数为：【${requestCounts}】`);
            break;
        }
        if (count == 0 || count % 10 == 0) {
            print(`---第【${count + 1}】请求【${requestPath}】接口，起始页为第${count * PAGE_NUMS + 1}行，共需要请求次数为：【${requestCounts}】`);
        }
        if (count != 0) {
            request.page.begin = count * PAGE_NUMS; // 更改查询起始行号
        }
        responseResult = requestInterfaceGetData(requestPath, request);

        if (!responseResult.result || responseResult.data.length == 0) {
            resultInfo.result = false;
            print(`---请求${requestPath}，第${count}次请求【失败】或者【没有数据】，详情：${responseResult}`);
            break;
        }
        let responseData = responseResult.data["data"];
        /**
         * 若处理的表为【线索信息】，则需要单独处理【截图文件、存证文件】URL，读取文件流，存储到服务器
         * 
         * 20220120 tuzw
         * 若数据需要全量，需要注意：
         * （1）由于文件是按时间作为路径存储的，若重新【全量】，之前获取的数据也会重新创建，为了避免重复创建，可以选择性的注释部分代码：!dealImageAndRecord(responseData).result)
         * （2）模型设置：按主键追加并更新，若直接注释此入口，会将【存储文件路径的字段】值赋值为：null，解决办法：在模型设置：更新历史数据--忽略存储【文件路径】的字段
         */
        if (tableName === "ODS_WLJYJG_SXXX" && !dealImageAndRecord(responseData).result) {
            break;
        }
        let adjustResult = adjustDataSpecialValue(responseData);
        if (!adjustResult.result) {  // 数据格式调整放到最后一步
            resultInfo.result = false;
            print(`---数据格式调整失败，${adjustResult.message}---`);
            break;
        }
        for (let dataIndex = 0; dataIndex < responseData.length; dataIndex++) {
            let isStop: string = getString(StopExecuteTxtPath);
            if (isStop == "true") {
                print(`---手动停止获取，第【${count + 1}】请求【${requestPath}】接口，共需要请求次数为：【${requestCounts}】`);
                break;
            }
            let data = responseData[dataIndex];
            let row: Array<string> = [];
            for (let fieldIndex = 0; fieldIndex < outputFields.length; fieldIndex++) {  // 返回的二维数组，必须跟给定的输出字段顺序一致
                let fieldName = outputFields[fieldIndex].dbfield;
                let temp = !!data[fieldName] ? data[fieldName] : null;
                row.push(temp);
            }
            rows.push(row);
        }
    }
    resultInfo.rows = rows;
    let end_time = new Date();
    let spendTime = end_time.getTime() - start_time.getTime();
    print(`执行结束[dealRequest], 根据请求配置对象，请求网络交易监管数据对接接口，花费时间：${spendTime / 1000}秒`);
    return resultInfo;
}

/**
 * 处理【请求数据】的请求参数
 * @param tableName 请求的表名
 * @param {
 *   "securityTenantId": "SECURITY_TENANT_ID",
      "page": {
            "begin": 0,
            "length": 10
       }
 * }
 * 
 * 20211231
 * 增量请求，每次请求【前天凌晨0点后新增的数据】所有新增数据
 * 注意：最终按【主键合并追加】，不需要考虑【主键重复问题】问题，需要模型勾选：更新数据
 * 
 * 20220122 tuzw
 * let currentDay = new Date(new Date().toLocaleDateString());
 * 上述写法存在地区时差，无法获取正确的零点时间
 * 
 * 20220310 tuzw
 * 由于对方审核时间存在两次审核，可能时间存在差异，这里改成每次获取：推迟24个小时（重复数据更新）
 */
function getRequest(tableName: string): Request {
    let request: Request = {
        "securityTenantId": SECURITY_TENANT_ID,
        "page": {
            "begin": 0,
            "length": 1
        }
    }
    if (tableName == "ODS_WLJYJG_ZDXX") { // 若请求的为【数字字典】则添加type
        request["type"] = "subject_app_type";
    }
    if (tableName == "ODS_WLJYJG_STRUCTURE") { // 结构化经营类目，增加类型: business
        request["type"] = "businessScope";
    }
    /**
     * 数据最早为2022-01-01 00:00:00
     */
    if (!checkTableIsContainsData(DATA_BASE, tableName)) {
        let now = LocalDateTime.now();
        let currentDay = LocalDateTime.of(now.toLocalDate(), LocalTime.MIN);
        let times = currentDay.toInstant(ZoneOffset.of("+8")).toEpochMilli();
        print(`-----${tableName}已经初始过，开始增量请求【前天凌晨0点后新增的数据】新增的数据，当前执行时间为： ${formatDate(new Date(times))}`);
        request["gmtModifiedGe"] = times - 24 * 60 * 60 * 1000 * 3;

        // request["gmtModifiedGe"] = new Date("2023-03-20 00:00:00").getTime(); // 寻找漏掉线索编号, 需要开启方法-dealImageAndRecord()内部的开关。
        // request["gmtModifiedLe"] = new Date("2022-02-14 11:19:00").getTime();  // 小于等于, 可和gmtModifiedGe一起使用, 请求指定时间范围
    }
    return request;
}

/**
 * 请求第三方接口，并获取数据
 * @param requestPath  请求地址
 * @parma page 分页对象
 * @return { result: true, data: { "count":100, "data":[] } }
 */
function requestInterfaceGetData(requestPath: string, request: Request): ResultInfo {
    let gdGenericRequest = new GdGenericRequest(requestPath, request);
    gdGenericRequest.setAppMessageId(uuid());

    let gdApiClient = new GdApiClient(APP_KEY, SIGN_KEY, DATA_KEY);
    gdApiClient.setHost(REQUEST_HOST);

    gdApiClient.setEncryptRequest(true);
    gdApiClient.setDecryptResponse(true);

    let gdGenericResponse = new GdGenericResponse();
    gdGenericResponse = gdApiClient.execute(gdGenericRequest);
    let responseResult = JSON.parse(gdGenericResponse.getResult());
    if (responseResult == null || responseResult.data == null) {
        return { result: true, message: "请求成功", data: [] };
    }
    if (!!responseResult.code) {
        return { result: false, message: "请求失败", errorData: JSON.stringify(responseResult) };
    }
    return { result: true, message: "请求成功", data: responseResult };
}

/** 
 * 将【处理好的数据】插入到【指定数据源】下的【表】中
 * @param dataBase 数据源名
 * @param tableName 物理表名
 * @param legalInsertData 合法的插入数据
 * @return { result: true }
 */
function saveDataToTable(dataBase: string, tableName: string, legalInsertData, schem?: string): ResultInfo {
    let resultInfo: ResultInfo = { result: true };
    let dataSource = getDataSource(dataBase);
    let conn = dataSource.getConnection();
    try {
        conn.setAutoCommit(false);
        let table: TableData;
        if (!!schem) {
            table = dataSource.openTableData(tableName, schem, conn);
        } else {
            table = dataSource.openTableData(tableName, "", conn);
        }
        table.insert(legalInsertData);
        conn.commit();
    } catch (e) {
        resultInfo.result = false;
        print(`--将请求数据插入到【${tableName}】表失败，error--`);
        print(e);
    } finally {
        conn.close();
    }
    return resultInfo;
}

/**
 * 调整请求返回数据的【部分特殊值】
 * @param responseData 请求的数据集【直接对其数组操作】
 * @return { result: true }
 * <p>
 *  2023-02-01 tuzw
 *  1 提取的模型[创建时间]字段值最开始确定后, 更新数据时, 不再更改[创建时间]字段值。
 *  2 模型更新时间为: gmtModified 字段。
 *  3 提取加工——输出节点——设置按主键合并追加，忽略更新字段：创建时间
 * </p>
 */
function adjustDataSpecialValue(responseData: { [fieldName: string]: string }[]): ResultInfo {
    if (!responseData) {
        return { result: false, message: "没有需要调整的数据" };
    }
    let createTime = new java.sql.Timestamp(new Date());
    let noNeedKeys: string[] = ["class", "securityTenantId"];
    let dateFields: string[] = ["gmtModified", "monitorTaskEndTime", "gmtCreate", "monitorTaskStartTime"];
    let arrFields: string[] = ["tags", "illegalPointUrls", "businessScope", "realBusinessScope", "panTags", "objectPanTags"];
    for (let i = 0; i < responseData.length; i++) {
        responseData[i]["createTime"] = createTime;
        let allKey = Object.keys(responseData[i]);
        for (let j = 0; j < allKey.length; j++) {
            let key = allKey[j];
            // @ts-ignore
            if (noNeedKeys.includes(key)) {
                delete responseData[i][key]; // 删除请求数据中的多余的key
            }
            // @ts-ignore
            if (dateFields.includes(key) && responseData[i][key] != null) {
                responseData[i][key] = new java.sql.Timestamp(new Date(responseData[i][key])); // 将给定的值转换为数据库支持的：【java.sql.Timestamp】
            }
            // @ts-ignore
            if (arrFields.includes(key) && responseData[i][key] != null) {
                // @ts-ignore
                responseData[i][key] = responseData[i][key].join(",");  // 将数组转换为字符串
            }
        }
    }
    return { result: true };
}

/**
 * 处理特殊字段【图片】和【凭证】字段，读取其字节流，保存到服务器中
 * @param responseData 请求的数据集
 */
export function dealImageAndRecord(responseDatas: { [fieldName: string]: string }[]): ResultInfo {
    print(`---处理特殊字段【图片】、【凭证】、【违法点截图列表】字段，读取其字节流，保存到服务器中，一共要处理：【${responseDatas.length}】条数据--start`);
    if (!responseDatas) {
        print(`---数据为：NULL，不做处理---end`);
        return { result: true };
    }
    let resultInfo: ResultInfo = { result: true };
    let connectionManager = new PoolingHttpClientConnectionManager();
    let httpclient;
    try {
        let requestConfig = JavaRequestConfig.custom()   // 创建RequestConfig对象，并设置连接超时和读取超时
            .setConnectTimeout(5000)
            .setSocketTimeout(10000)
            .build();
        httpclient = HttpClients.custom()
            .setConnectionManager(connectionManager)
            .setDefaultRequestConfig(requestConfig)
            .build();
        resultInfo = convertImageAndRecordFile(httpclient, responseDatas);
    } catch (e) {
        resultInfo.result = false;
        print(e);
    } finally {
        httpclient && httpclient.close();
        connectionManager && connectionManager.close();
    }
    print(`---处理特殊字段【图片】、【凭证】、【违法点截图列表】字段，读取其字节流，保存到服务器中，一共要处理：【${responseDatas.length}】条数据--end`);
    return resultInfo;
}

/** 
 * 处理特殊字段【图片】和【凭证】字段，读取其字节流，保存到服务器中
 * <p>
 *  2023-02-03 tuzw
 *  1 不能在此方法对已存在的线索信息拦截, 因为内部对附件字段进行赋值， 若拦截后, 会导致附件字段值为空, 从而将库中的附件信息清空
 *  2 若需要拦截，则需要在模型设置：更新忽略字段，将附件字段都忽略掉，主要适用：重新请求漏掉数据。
 *  3 若有漏掉数据，可根据漏掉数据的审核时间，进行请求, gmtModifiedGe: 大于等于, gmtModifiedLe: 小于等于, 合起来就是等于
 * </p>
 * @param httpclient 请求客户端对象
 * @param responseDatas 处理的数据
 * @return 
 */
function convertImageAndRecordFile(httpclient, responseDatas: { [fieldName: string]: string }[]): ResultInfo {
    let resultInfo: ResultInfo = { result: true };
    /** 单条url数据字段名称 */
    let fileUrlName: Array<string> = ["grabRecordEvidenceUrl", "grabRecordCertUrl"];
    /** 保存对应url文件路径字段名称 */
    let saveFilePathFields: Array<string> = ["grabRecordEvidenceID", "grabRecordCertID"];
    for (let i = 0; i < responseDatas.length; i++) {
        let responseData = responseDatas[i];
        let isStop: string = getString(StopExecuteTxtPath);
        if (isStop == "true") {
            print(`处理特殊字段【图片】和【凭证】字段，读取其字节流，保存到服务器中，手动停止`);
            break;
        }
        /**
         * 20220221 tuzw
         * 考虑到根据【线索编号】作为文件父文件夹名不太实用，改为字段【线索可读编号】作为每个线索信息文件的父名称
         */
        let fileParent = responseData['sabaCode'];
        let pkValue: string = responseData.pk;
        if (!fileParent) { // 若【线索可读编号】为NULL，则以【线索编号】
            fileParent = pkValue;
        }
        print(`---当前处理第[${i + 1}]条数据，其父路径为：${fileParent}`);

        let illegalPointUrls: string[] = []; // 处理存储违法点截图【多条url】数组字段
        let illegalUrl = responseData["illegalPointUrls"];
        if (typeof illegalUrl == "string") {
            illegalPointUrls = illegalUrl.split(",");
        } else {
            illegalPointUrls = illegalUrl;
        }

        if (illegalPointUrls != null && illegalPointUrls.length > 0) { // 处理【违法点截图Url列表】
            let saveIllegalPointFilePaths: string[] = [];
            for (let illegalIndex = 0; illegalIndex < illegalPointUrls.length; illegalIndex++) {
                let url: string = illegalPointUrls[illegalIndex];
                if (!url) {
                    continue;
                }
                let { result, fileIo, fileName } = getFileIo(httpclient, url);
                if (result) {
                    let { result, filePath } = createFile(fileParent, fileName, fileIo as ArrayLike<number>);
                    if (result) {
                        saveIllegalPointFilePaths.push(filePath);
                    }
                }
            }
            responseData["illegalPointFilePath"] = saveIllegalPointFilePaths.join(",");
        }
        for (let urlIndex = 0; urlIndex < fileUrlName.length; urlIndex++) { // 处理存储【图片】和【凭证】数据字段
            let url: string = responseData[fileUrlName[urlIndex]];
            if (!url) {
                continue;
            }
            let { result, fileIo, fileName } = getFileIo(httpclient, url);
            if (result) {
                let { result, filePath } = createFile(fileParent, fileName, fileIo as ArrayLike<number>);
                if (result) {
                    responseData[saveFilePathFields[urlIndex]] = filePath; // 记录附件路径
                }
            }
        }
    }
    return resultInfo;
}

/**
 * 读取【下载链接】文件内容 和 文件名.类型
 * @param url 文件下载链接
 * @return eg: { fileIo: [文件二进制流数组], "test.png" }
 */
function getFileIo(httpclient, url: string): { result: boolean, fileIo?: number[] | undefined | string, fileName?: string } {
    let resultInfo = { result: false, fileIo: [], fileName: "" };
    let response;
    try {
        let httpGet = new HttpGet(url);
        response = httpclient.execute(httpGet);
        /**
         * 20220927 tuzw
         * 获取文件名，不可使用getValuegetValue来获取，改为直接使用toString()方法将其对象转换为字符串
         * let fileName = response.getFirstHeader("Content-Disposition").getValuegetValue().split(';')[1].split('=')[1];
         * class org.apache.http.message.BufferedHeader ——> Content-Disposition: inline; filename="926/0-220925-wx-207109-1.jpg"
         */
        let contentDisposition = response.getFirstHeader("Content-Disposition").toString();
        let fileName = contentDisposition.split(';')[1].split('=')[1];

        let entity = response.getEntity();
        let contentType = response.getFirstHeader("Content-type");
        let fileType: string = "";
        if (!!contentType) { // 考虑到zip文件，不包含：Content-type，此处需要进行空校验，若不包含，则直接根据文件名来匹配文件类型
            fileType = contentType.getValue();
        } else {
            fileType = fileName; // 通过indexOf来匹配文件名所包含文件类型，故此处直接将文件名赋值给文件类型
        }
        if (fileType.indexOf("image") !== -1 || fileType.indexOf("pdf") !== -1 ||
            fileType.indexOf("jpg") !== -1 || fileType.indexOf("png") !== -1 || fileType.indexOf("mp4") !== -1
            || fileType.indexOf("jpeg") !== -1 || fileType.indexOf("zip") !== -1 || fileType.indexOf("bmp") !== -1) {
            /**
             * 20220208 tuzw 
             * 对方文件夹含有大量的中文名，直接获取是一串乱码，需要将其转换为utf-8
             */
            let encodeFileName = new java.lang.String(fileName.getBytes("ISO-8859-1"), "utf-8");
            resultInfo = {
                result: true,
                fileIo: EntityUtils.toByteArray(entity),
                fileName: encodeFileName.replace(/\"/g, "") // 去除字符串左右‘双引号’
            }
        } else {
            let content = EntityUtils.toString(entity);
            if (content.indexOf("认证失败") !== -1) {
                print("认证失败");
                print(content);
            }
        }
    } finally {
        response && response.close();
    }
    return resultInfo;
}

/**
 * 根据文件二进制流在对应服务器下创建文件
 * @param parentFileName 父文件夹名
 * @param fileName 文件名.文件类型
 * @param fileIoByte 写入给定文件的文件二进制流bao
 * @return { result: true }
 */
function createFile(parentFileName: string, fileName: string, fileIoByte: ArrayLike<number>): { result: boolean, filePath: string } {
    let workDir = getWorkDir();
    let currentTime: string = formatDate2(new Date());
    let result: boolean = true;
    /**
     * 文件存储位置（166 、 167 节点）
     * 路径：/data/workdir/wljy_fjcc/2022-01-14/0-220112-wx-74783-1/certPath_1164935678589424608D3EF9B7E37A296164AF3DDAA994D3B.pdf
     * 节点设置：计划任务-设置-指定节点
     * 
     * 注意：重新获取，记得注意路径是按【当天】日期作为路径 
     * 
     * 20220122 tuzw
     * 由于需求还在变动（违法点URL还未上线），暂时不采取【动态】获取日期作为父目录
     * let filePath: string = `${getWorkDir()}/wljy_fjcc/2022-01-20/${parentFileName}/${fileName}`;
     * 
     * 20220221 tuzw
     *      由于需要更改单个线索文件的父文件夹名改为字段【线索可读编号】作为每个线索信息文件的父名称，
     * 这里对2022-01-01及以后的数据重新获取，并存到2022-02-22文件下
     * 
     * 20230329 tuzw
     * 问题: 计划任务执行必须保证167节点INF设置共享, 否则调用exists方法, 内部会阻塞, 异常: java.base@11.0.5/java.io.UnixFileSystem.getBooleanAttributes0(Native Method)
     * 原因: 文件位于已关闭的 NFS 共享上
     */
    let filePath: string = `${getWorkDir()}/wljy_fjcc/2022-02-22/${parentFileName}/${fileName}`;
    let javaFile = new java.io.File(filePath);
    if (!javaFile.exists()) {
        // print(`--文件${fileName}不存在硬盘中，开始创建文件，路径为：${filePath}`);
        let parentFile = javaFile.getParentFile();
        if (!parentFile.exists()) {
            parentFile.mkdirs();    // 若文件父目录不存在，则创建文件以及父文件目录
        }
        javaFile.createNewFile(); // 创建真实的文件
    } else {
        return { result: result, filePath: filePath };
    }

    let file = fs.getFile(filePath); // 读取文件
    let outStream = file.openOutputStream();
    try {
        outStream.write(fileIoByte as number[]);
    } catch (e) {
        result = false;
        print(`--写入文件${fileName}内容失败---`);
        print(e);
    } finally {
        outStream && outStream.close();
    }
    return { result: result, filePath: filePath };
}

/**
 * 格式化时间戳
 * @parmas dates 需要格式化的时间
 * @return 格式化后的日期，2021-12-12
 */
function formatDate2(dates: Date): string {
    let Year: number = dates.getFullYear();
    let Months = (dates.getMonth() + 1) < 10
        ? '0' + (dates.getMonth() + 1)
        : (dates.getMonth() + 1);
    let Day = dates.getDate() < 10
        ? '0' + dates.getDate()
        : dates.getDate();
    return Year + '-' + Months + '-' + Day;
}

/**
 * 格式化时间戳
 * @parmas dates 需要格式化的时间
 * @return 格式化后的日期，2021-12-12 00:00:00 
 */
function formatDate(dates: Date): string {
    let Year: number = dates.getFullYear();
    let Months = (dates.getMonth() + 1) < 10
        ? '0' + (dates.getMonth() + 1)
        : (dates.getMonth() + 1);
    let Day = dates.getDate() < 10
        ? '0' + dates.getDate()
        : dates.getDate();
    let Hours = dates.getHours() < 10
        ? '0' + dates.getHours()
        : dates.getHours();
    let Minutes = dates.getMinutes() < 10
        ? '0' + dates.getMinutes()
        : dates.getMinutes();
    let Seconds = dates.getSeconds() < 10
        ? '0' + dates.getSeconds()
        : dates.getSeconds();
    return Year + '-' + Months + '-' + Day + ' ' + Hours + ':' + Minutes + ':' + Seconds;
}

/** 
 * 校验某数据源下的数据库【某表】是否为：空表
 * @param dataBase 数据源名
 * @param tableName 表名
 * @parma schem
 * @return true | false
 */
export function checkTableIsContainsData(dataBase: string, tableName: string, schem?: string): boolean {
    let dataSource = getDataSource(dataBase);
    let conn = dataSource.getConnection();
    let tableIsNull: boolean = false;
    try {
        let table: TableData;
        table = dataSource.openTableData(tableName, schem, conn);
        let checkContentIsNullSql: string = `select count(1) as COUNT from ${tableName}`;
        let queryData = table.executeQuery(checkContentIsNullSql);
        print(queryData);
        if (Number(queryData[0]["COUNT"]) == 0) {
            tableIsNull = true;
        }
    } catch (e) {
        tableIsNull = undefined;
        print(`--校验数据库【${tableName}】是否为：空表--error--`);
        print(e);
    } finally {
        conn.close();
    }
    return tableIsNull;
}

/**
 * 获取某表指定【时间字段】最大的时间
 * @param dataBase 数据源名
 * @param tableName 表名
 * @param timeColumName 时间字段名
 * @parma schem
 * @return eg："2021-12-26 06:43:49"
 */
function getMaxTime(dataBase: string, tableName: string, timeColumName: string, schem?: string): string {
    let dataSource = getDataSource(dataBase);
    let conn = dataSource.getConnection();
    let maxTimes: string = "";
    try {
        let table: TableData;
        if (!!schem) {
            table = dataSource.openTableData(tableName, schem, conn);
        } else {
            table = dataSource.openTableData(tableName, "", conn);
        }
        let selectMaxDateSQL: string = `select MAX(${timeColumName}) as MAXDATE from ${tableName}`;
        let maxDate = table.executeQuery(selectMaxDateSQL);
        if (maxDate != null) {
            let queryDateType: string = maxDate[0]["MAXDATE"].getClass();   // 注意：版本不同，查出来的结果可能是：字符串 或 时间类型 class java.sql.Timestamp
            if (queryDateType == "class java.lang.String") {
                maxTimes = formatDate(new Date(maxDate[0]["MAXDATE"]));
            }
            if (queryDateType == "class java.sql.Timestamp") {
                maxTimes = formatDate(new Date(maxDate[0]["MAXDATE"]));
            }
            if (queryDateType == "class java.sql.Date") {
                maxTimes = formatDate(maxDate[0]["MAXDATE"]);
            }
            print(`-----当前【物理表】中数据最大时间字段${timeColumName}值为：${maxTimes}`);
        }
    } catch (e) {
        print(`--获取【${tableName}】指定【时间字段】最大的时间，error--`);
        print(e);
    } finally {
        conn.close();
    }
    return maxTimes;
}

/** 返回格式 */
interface ResultInfo {
    /** 执行结果 */
    result: boolean;
    /** 返回消息 */
    message?: string;
    /** 数据 */
    data?: any;
    /** 错误数据 */
    errorData?: any;
    [propname: string]: any;
}

/** 请求参数 */
interface Request {
    /** 租户秘钥 */
    securityTenantId: string;
    /** 分页对象 */
    page?: Page;
    /** 新增或者更新时间毫秒数（大于等于） */
    gmtModifiedGe?: number;
    /** 新增或者更新时间毫秒数（小于等于） */
    gmtModifiedLe?: number;
    /** 推送数据（JSON数组字符串） */
    data?: any;
    [propname: string]: any;
}

/** 分页对象 */
interface Page {
    /** 页码 */
    begin: number;
    /** 页大小 */
    length: number;
    [propname: string]: any;
}

/** 请求配置对象 */
interface RequestConfig {
    /** 表中文描述 */
    desc: string;
    /** 请求数据落地的表名 */
    tableName: string;
    /** 请求第三方地址 */
    requestPath: string;
    [propname: string]: any;
}