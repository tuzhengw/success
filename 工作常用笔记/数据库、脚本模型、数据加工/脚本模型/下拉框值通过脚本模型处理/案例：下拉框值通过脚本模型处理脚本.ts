
import db from 'svr-api/db';
/**
 * 20211110 tuzw
 * 获取多个指定数据源、物理表相同的字段
 * 触发条件：使用SPG绑定当前数据源某字段组件时触发
 */
function onQueryData(queryInfo: QueryInfo): DbTableInfo | any[][] | QueryDataResultInfo {
	print("获取多个指定数据源、物理表相同的时间字段--start");
	/**
	 * queryInfo:
	 * sources=[{
	 * 		path=/sysdata/data/tables/sdi/data-access/script/SDI_DATA_ACCESS_LIKE_FIELDS.tbl,
	 * 		id=model28,
	 * 		fields=null,
	 * 		refm=[model28]
	 * }],
	 * params=[{
	 * 		name: "param3", value: "sjzt_sjjh"
	 * 	   }, {
	 * 		name: "param15", value: ["cra_db", "csjb"]
	 * }],
	 * fields=[{
	 * 		name=LIKE_FIELDS,
	 * 		exp=model28.LIKE_FIELDS
	 * }],
	 * options={
	 * 		needCodeDesc=true,
	 * 		limit=10000,
	 * 		cacheMode=auto,
	 * 		cacheTime=3,
	 * 		cacheExpireTime=null
	 * },
	 * filter=[{}],
	 * 		distinct=true,
	 * 		select=true,
	 * 		sort=[{
	 * 		exp=model28.LIKE_FIELDS
	 * }],
	 * queryId=model28.LIKE_FIELDS_distinct,
	 * resid=/sysdata/app/zt.app/data-access/data-access-create_test.spg,
	 * resModifyTime=1636523846867,
	 * fieldFilters=[]
	 */
	print(queryInfo);
	/** 过滤参数集 */
	let params = queryInfo.params;
	/** 数据源 */
	let dataBase = getParams(params, 'param3') as string;
	/** 所选物理表名集合，去空值 */
	let physicalTableNames = getParams(params, 'param15') as Array<string>;
	physicalTableNames = filterNoUseValue(physicalTableNames);
	/** 
	 * 组件绑定物理字段名
	 * fields=[{
	 * 		name=LIKE_FIELDS,
	 * 		exp=model28.LIKE_FIELDS
	 * }]
	 */
	if (dataBase == null || physicalTableNames == null || physicalTableNames.length < 2){
		print("数据源或者物理表为NULL 或 目标表仅一个");
		return null;
	}else {
		let ds: Datasource = db.getDataSource(dataBase);
		if(ds != null){
			/** 存储各物理表时间字段 */
			let chosePhysicalTableFields: Array<Array<string>> = [];
			for (let i = 0; i < physicalTableNames.length; i++) {
				let isTableExist: boolean = ds.isTableExists(physicalTableNames[i]);
				if (isTableExist) {
					/** 存储单个表所有物理时间字段集 */
					let aloneTableTimesFields: Array<string> = [];
					let tableMeta: TableMetaData = ds.getTableMetaData(physicalTableNames[i]);
					let modelColumns: TableFieldMetadata[] = tableMeta.getColumns();
					modelColumns.forEach(item =>{
						if (!!item.name && [FieldDataType.D, FieldDataType.P, FieldDataType.T].indexOf(item.dataType) != -1){
							aloneTableTimesFields.push(item.name);	
						}
					});
					chosePhysicalTableFields.push(aloneTableTimesFields);
				} else {
					print(`表${physicalTableNames[i]}不存在与数据源${dataBase}下`);
				}
			}
			/** 存储各物理表共同时间字段 */
			let commonTimeFields = getCommonElement(chosePhysicalTableFields);
			if(commonTimeFields.length == 0){
				print("存储各物理表共同时间字段为NULL");
				return null;
			}
			return changeArrFrom(commonTimeFields);
		}
	}
	print("获取多个指定数据源、物理表相同的时间字段--end");
	return null;
}

/**
 * 去重数组（一维）无效值
 */
function filterNoUseValue(arr: string[]): string[] {
	let effectiveArr: Array<string> = [];
	for(let i = 0; i < arr.length; i++){
		if(!!arr[i]){
			effectiveArr.push(arr[i]);
		}
	}
	return effectiveArr;
}


/** 
 * 将其一维数组处理为二维数组
 */
function changeArrFrom(arr: string[]): string[][] {
	if(arr == null){
		return null;
	}
	let resultArr: Array<Array<string>> = [];
	for(let i = 0; i < arr.length; i++){
		resultArr.push(
			[arr[i]]
		)
	}
	return resultArr;
}


/**
 * 求二维数组各行的交集（去重）
 * 思路：每次通过上一次比较后的交集，再与下一行比较求交集
 * eg：
 * [
		["START_TIME", "END_TIME", "LAST_UPDATE_TIME", "LAST_LOGIN_TIME"],
		["START_TIME", "END_TIME", "LAST_UPDATE_TIME", "LAST_LOGIN_TIME"]
   ]
 */
function getCommonElement(arr: string[][]): Array<string> {
	let res = arr.reduce((arr1, arr2) =>{
		let temp = arr1.filter(item => arr2.indexOf(item) != -1);
		/** 每次求得两数组交集后去重 */
		temp = temp.reduce((cur, next) =>{
			cur.indexOf(next) == -1 && cur.push(next);
			return cur;
		}, [])
		return temp;
	});
	return res;
}

/**
 * 解析Params，获取对应name的value
 * @params paramsName：参数name值
 * params=[
 * 	{name=param5, value=ADDRESS}, 
 * 	{name=param15, value=[ACTOR, ACTOR_1]}
 * ]
   @return name对应的value值
 */
function getParams(params: QueryParamInfo[], type): unknown {
	for (let i = 0; params && i < params.length; i++) {
		let p = params[i];
		if (p.name == type) {
			print(p.value);
			return p.value;
		}
	}
}