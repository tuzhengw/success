# `SQL`增删改（字段不确定的情况）

​	产品搜索：`ScriptObject_TableData.java`

---

```java
private Connection getConnection() throws SQLException {
	if (conn == null) {
		conn = ds.getConnection();
		myconn = true;
		conn.setAutoCommit(autoCommit);
	}
	return conn;
}
```



----

# * `schema`

​	`schema`就是数据库对象的集合，这个集合包含了各种对象如：表、视图、存储过程、索引等。

> ​	如果把database看作是一个仓库，仓库很多房间（schema），一个schema代表一个房间，table可以看作是每个房间中的储物柜，user是每个schema的主人，有操作数据库中每个房间的权利，就是说每个数据库映射的user有每个schema（房间）的钥匙。

----

​	`MySQL`，下面两种写法均可以创建：数据库

```sql
create schema
create database
```

​	<button>作用</button>：作为数据库名

````mysql
# 查询两个数据库中的表，实现：不同数据库之间的操作
select * from 数据库名1.表1 as A left join 数据库名2.表2 as B on A.id=B.id;
````

# * 更新操作

## 1 更新列和条件确定

```java
String sql = `update 表名 set name=? where id=?`;
数据对象.executeUpdate(sql,"张三",1001);
```

## 2 更新列和条件不确定

## （update(Map,String)）

​	更新列：Map集合（外部调用：`JSON`或者二维数组）

​	更新条件：更新列：Map集合（外部调用：条件字符串）

```java
/**
 * 更新数据
 * @param columnValues 要更新的字段及值，如`{"f1":"1","f2":"substr(FIELD3,0,3)"}`
 * @param where 匹配条件，可以是一个字符串，如"f1=123 AND f2='abc'"
 * @return 返回删除的行数
 * @throws SQLException 
 * @throws IOException
 */
public int update(Map<String, Object> columnValues, String where) 
    throws SQLException, IOException {
    // args：存储更新的值，eg：张三, 10001
    List<Object> args = new ArrayList();
    String sql = this.getUpdateSql(columnValues, (String)where, args);
    /**
     * sql：update 表名 set name = ? where id = ?
     * args.toArray()：张三,1001
     * 执行SQL：update 表名 set name = 张三 where id = 1001
     */
    return this.executeUpdate(sql, args.toArray());
}
```

​	拼接《更新列》

```java
/**
 * cond：更新列，[{name : 张三_修改, sex : 男_修改, ...}]
 * where：更新条件，name = ?
 * args：存储更新列对应的值（按顺序存储）
 */
private String getUpdateSql(Map<String, Object> cond,
                            String where, List<Object> args) {
    StringBuilder sb = new StringBuilder();
    
    sb.append("update ");
    if (StringUtils.isNotBlank(this.schema)) {
        sb.append(this.schema).append(".");
    }

    sb.append(this.tablename).append(" set ");
    // 第一个条件前是没有：","号的，第二个到最后一个
    boolean firstCondition = true;
    // update 表明 set a=?,b=?,... where c=?....
    Iterator var7 = cond.keySet().iterator();
    while(var7.hasNext()) {
        String key = (String)var7.next();
        if (firstCondition) {
            firstCondition = false;
        } else {
            sb.append(", ");
        }

        sb.append(key).append("=?");
        // 存储key对应的value值
        args.add(cond.get(key));
    }
    sb.append(" where ").append(where);
    return sb.toString();
}
```

​	返回《结果》：

```sql
update 表名 set 列1=列值,列2=列值,...  where 条件1=值;
```



## 3 更新列和条件不确定

## （update(Map,Map)）

​	更新列：Map集合（外部调用：`JSON`或者二维数组）

​	更新条件：更新列：Map集合（外部调用：`JSON`或者二维数组）

```java
/**
 * @param columnValues 要更新的字段及值，如`{"f1":"1","f2":"substr(FIELD3,0,3)"}`
 * @param where 匹配条件，也可以是一个json，如`{"f1":123, "f2":"abc"}`
 * @return 返回删除的行数
 * @throws SQLException 
 * @throws IOException
 */
public int update(Map<String, Object> columnValues, Map<String, Object> where) 
    throws SQLException, IOException {
    // 记录更新列的值（value）
    List<Object> args = new ArrayList();
    String sql = this.getUpdateSql(columnValues, (Map)where, args);
    /**
     * sql：update 表名 set name = ? where id = ?
     * args.toArray()：张三, 1001
     * 执行SQL：update 表名 set name = 张三 where id = 1001
     */
    return this.executeUpdate(sql, args.toArray());
}
```

​	拼接《更新列》：

```java
/**
 * 作用：根据传入的 更新列和更新条件 拼接SQL
 * params columnValues：更新列和值，[{name : 张三_修改, sex : 男_修改, ...}]
 * params where：更新条件，[{id : 1, name : 张三}]
 * params args：存储更新列对应的值（按顺序存储）
 */
private String getUpdateSql(Map<String, Object> cond,
                            Map<String, Object> where, List<Object> args) {
        StringBuilder sb = new StringBuilder();
    
        sb.append("update ");
        if (StringUtils.isNotBlank(this.schema)) {
            sb.append(this.schema).append(".");
        }

        sb.append(this.tablename).append(" set ");
    	// 第一个条件 前 是没有：","号的，第二个到最后一个
        boolean firstCondition = true;
        Iterator var7 = cond.keySet().iterator();
        while(var7.hasNext()) {
            String key = (String)var7.next();
            if (firstCondition) {
                firstCondition = false;
            } else {
                sb.append(", ");
            }

            sb.append(key).append("=?");
            args.add(cond.get(key));
        }

        this.getWhereCondition(sb, where, args);
        return sb.toString();
    }
```

​	拼接《条件》，where：

```java
/**
 * 作用：根据传入的 更新列和更新条件 拼接SQL
 * params sb：update 表名 set 列1=列值,列2=列值,... 
 * params where：更新条件，[{id : 1, name : 张三}]
 * params args：存储更新列对应的值（按顺序存储）
 */
private String getWhereCondition(StringBuilder sb, 
                                 Map<String, Object> wheres, List<Object> args) {
    sb.append(" where ");
    /**
    	 * 第一条件前不用加：逗号
    	 */
    boolean firstCondition = true;
    Iterator var6 = wheres.keySet().iterator();

    while(var6.hasNext()) {
        String key = (String)var6.next();
        if (firstCondition) {
            firstCondition = false;
        } else {
            sb.append(" and ");
        }

        sb.append(key).append("=?");
        /**
         * 针对复杂的value需要特殊处理，例如：value是一个功能
         * 这里仅考虑：value为：普通字符串值
         */
        // args.add(JSScriptUtils.getJavaObject(wheres.get(key)));
        args.add(key);
    }
    return sb.toString();
}
```

​	最终《返回结果》：

```mysql
update 表名 set 列1=列值,列2=列值,...  where 条件1=值,条件2=值;
```

```java
import jdk.nashorn.api.scripting.ScriptObjectMirror;
```

```java
/**
 * 将js返回的对象“彻底“的转化为java对象。
 * 所谓彻底的是指会将对象中的map、数组、列表等等全部都转化成对应的java对象。
 * @param obj
 * @return
 */
public static Object getJavaObject(Object obj) {
    if (obj == null || isUndefined(obj)) {
        return null;
    }
    if (obj instanceof ScriptObjectMirror) {
        ScriptObjectMirror objm = (ScriptObjectMirror) obj;
        
        if (objm.isArray()) {
            /**
             * 数组都转化成List吧，方便外界遍历时统一处理，否则还要判断List和[]的情况。 
             */
            Object len = objm.get("length");
            List<Object> listResult = null;
            if (len != null) {
                Integer length = NumberUtils.toInt(len);
                listResult = new ArrayList<Object>(length);
                for (int i = 0; i < length; i++) {
                    Object v = objm.getSlot(i);
                    listResult.add(getJavaObject(v));
                }
            }
            return listResult;
        }

        if ("Date".equals(objm.getClassName())) {
            // Number time = (Number) objm.callMember("getTime");
            return new Date(objm.to(Long.class));
        }

        if (objm.isFunction()) {
            return objm.toString();
        }
        
        Map<String, Object> mapResult = new LinkedHashMap<String, Object>();
        for (Map.Entry<String, Object> entry : objm.entrySet()) {
            Object v = entry.getValue();
            mapResult.put(entry.getKey(), getJavaObject(v));
        }
        return mapResult;
    }
    
    if (obj instanceof JSONListAdapter) {
        JSONListAdapter arr = (JSONListAdapter) obj;
        int length = arr.size();
        List<Object> listResult = new ArrayList<Object>(length);
        for (int i = 0; i < length; i++) {
            Object v = arr.get(i);
            listResult.add(getJavaObject(v));
        }
        return listResult;
    }
    /**
     * 1. 后端的脚本api实现经常要包装原始的java对象，比如File、DWTable等
     * 2. 在脚本中可能会把包装器传递给某些API，比如：
     file（在脚本中是ScriptObject_File对象），传递给某些api时需要能方便的获得原始的File对象
     * 3. 利用此接口可以统一的进行获取。
     */
    if (obj instanceof ScriptObjectWrapper) {
        obj = ((ScriptObjectWrapper) obj).getInnerWrappedObject();
    }
    return obj;
}
```

# * 插入操作

## 1 插入一条数据

## insert（Map）

```java
/**
 * 插入一行数据
 * @param kv：更新的字段和值，例如：{name : 张三, sex : 男, ...}
 */
public int insert(Map<String, Object> kv) throws SQLException {
    /**
     * 这里仅将数据放置在：List集合中，最终还是调用的：插入多行数据的insert()方法
     */
    List<Map<String, Object>> kvs = new ArrayList<Map<String, Object>>(32);
    kvs.add(kv);
    /**
     * 调用插入多行的insert()方法
     */
    return insert(kvs);
}
```

## 2 插入多条数据

## insert(List<Map<String, Object>>)

```mysql
INSERT INTO department
		(dno, dname, loc)
	VALUES
		(40, 'Sales', 'NJ'),(50,  'Marketting',  'MO'),(60,  'Testing',  'MN');
```

```java
/**
 * 插入多行数据
 * @param kvs ：List集合，元素为：Map
 * @throws SQLException 
 */
public int insert(List<Map<String, Object>> kvs) throws SQLException {
    if (kvs == null || kvs.size() == 0) {
        return 0;
    }
    int total = 0;
	// 记录所有的key值
    String[] fields = new String[kvs.get(0).size()];
    /**
     * kvs.get(0) ： 由于kvs集合中的所有元素 列 都是一致的，传递一个元素即可（默认第一个），
     			并将其对应的key获取，并拼接SQL，例如：
     				insert into 表名 (id, name, sex)
     * fields：存储key的数组
     */
    String sql = getInsertSql(kvs.get(0), fields);
	
    // 创建一个临时二维数据，记录：每行的value值
    Object[][] args = new Object[kvs.size()][];
    
    int index = 0;
    for (Map<String, Object> kv : kvs) {
        Object[] arg = getInsertArgs(kv, fields);
        args[index++] = arg;
    }
    // 批量插入数据
    int[] result = executeBatchUpdate(sql, args);
    // 累加成功的行数
    for (int i = 0; i < result.length; i++) {
        total += result[i];
    }
    return total;
}
```

​	拼接插入的`SQL`

```java
/**
 * params cond：
 * params fields：每行插入数据对应的列
 */
private String getInsertSql(Map<String, Object> cond, String[] fields) {
    StringBuilder ddl = new StringBuilder();
    StringBuilder vsb = new StringBuilder();
    ddl.append("insert into ");
    if (StringUtils.isNotBlank(schema)) {
        ddl.append(schema).append(".");
    }
    ddl.append(tablename).append(" (");
    int i = 0;
    /**
     * 第一个条件前不需要：逗号
     */
    boolean firstCondition = true;
    for (String key : cond.keySet()) {
        if (firstCondition) {
            firstCondition = false;
        }
        else {
            ddl.append(",");
            vsb.append(",");
        }
        ddl.append(key);
        fields[i++] = key;
        vsb.append("?");
    }

    ddl.append(") values (");
    ddl.append(vsb);
    ddl.append(")");
    return ddl.toString();
}
```

​	批量插入数据：

```java
private int[] executeBatchUpdate(String sql, Object[][] args) 
						throws SQLException {
   Connection conn = getConnection();
   try {
      return executeBatch(conn, sql, args);
   }
   finally {
      this.closeIfNeed(conn);
   }
}
```

​	`MySQL`内置方法

```java
import com.succez.commons.jdbc.Dialect;
import com.succez.commons.jdbc.ResultSetExtractor;
import com.succez.commons.jdbc.impl.JdbcUtils;
```

```java
private int[] executeBatch(Connection connection, String sql, Object[][] args) 
	throws SQLException {
		return JdbcUtils.batchUpdate(conn, sql, args);
	}
```

# * 删除操作

## 1 `del(String where)`

```java
/**
 * 删除符合条件的数据行
 * @param where	是一个表示条件的字符串，如"FIELD1=123 AND FIELD2='abc'"
 * @return
 * @throws SQLException
 * @throws IOException
 */
public int del(String where) throws SQLException, IOException {
    String delete = "delete from " + 
        (StringUtils.isNotBlank(schema) ? "" : (schema + ".")) 
        + tablename + 
        " where " + where;
    return executeUpdate(delete, (Object[]) null);
}
```

## 2 ` del(Map<String, Object>)`

```java
/**
 * 删除符合条件的数据行
 * @param where	是一个表示条件的map，key为字段名称，value为字段的值 
 				如{"FIELD1":123, "FIELD2":"abc"}
 * @return 成功影响的行数
 * @throws SQLException
 * @throws IOException
 */
public int del(Map<String, Object> where) throws SQLException, IOException {
    List<Object> args = new ArrayList<Object>();
    String delete = getDeleteSql(where, args);
    return executeUpdate(delete, args.toArray());
}
```

​	拼接删除的`SQL`：

```java
private String getDeleteSql(Map<String, Object> where, List<Object> args) {
    StringBuilder sb = new StringBuilder();
    sb.append("delete from ");
    if (StringUtils.isNotBlank(schema)) {
        sb.append(schema).append(".");
    }
    sb.append(tablename);
    getWhereCondition(sb, where, args);
    return sb.toString();
}
```

​	拼接`where条件`:

```java
private String getWhereCondition(StringBuilder sb, 
			Map<String, Object> wheres, List<Object> args) {
	sb.append(" where ");
	boolean first = true;
	for (String key : wheres.keySet()) {
		if (first) {
			first = false;
		}
        else {
        	sb.append(" and ");
        }
        sb.append(key).append("=?");
        args.add(JSScriptUtils.getJavaObject(wheres.get(key)));
    }
	return sb.toString();
}
```

## 3 `deleteAll()`

```java
/**
 * 删除所有的数据行
 * @throws SQLException
 * @throws IOException
 */
public void deleteAll() throws SQLException, IOException {
    String delete = "delete from " + 
        (StringUtils.isNotBlank(schema) ? "" : (schema + ".")) 
        + tablename;
    executeUpdate(delete, (Object[]) null);
}
```

