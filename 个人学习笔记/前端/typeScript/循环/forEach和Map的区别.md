# `forEach`和`Map`

# 1 相同点

​	都是循环遍历数组中的每一项

​	`forEach`和`map`方法里每次执行匿名函数《都支持3个参数》，参数分别是：

​			item（当前每一项）、index（索引值）、arr（原数组）

​	匿名函数中的this都是指向：window

​	《只能》遍历数组

---

# 2 `forEach`

​	`forEach() `方法用于调用数组的每个元素，并将元素传递给回调函数。

​	注意：` forEach()` 对于《空数组》是不会执行回调函数的。

​	`forEach`《没有返回值》，所以不能链式调用

```ts
array.forEach(function(currentValue, index, arr), thisValue)
currentValue // 代表当前元素
index // 代表当前元素的索引
arr // 当前元素所属的数组对象
```

​	案例：

```ts
let arr = [1, 2, 3];
arr.forEach(item =>{
	print(item);
});
```

​	



# 3 `Map`

​	 map() 方法返回：<button>一个新数组</button>，数组中的元素为原始数组元素调用函数处理后的值

 	map() 方法按照原始数组元素顺序依次处理元素

​	 注意： map() 不会对空数组进行检测

​	 注意： map() 不会改变原始数组

```ts
/**
 * ...result：将类数组转换为普通数组
 *  将
 * 	0: {MCLB: "纱布、药棉", CPID: "38738"}
 	1: {MCLB: "灭菌纱布、胶布", CPID: "38739"}
	转换为：
 * 	0: "38738=纱布、药棉"
 	1: "38739=灭菌纱布、胶布"
 */
result = [... result].map(item=>{
    let id: string = item['CPID'];
    let title: string = item['MCLB'];
    return `${id}=${title}`;
});
```

