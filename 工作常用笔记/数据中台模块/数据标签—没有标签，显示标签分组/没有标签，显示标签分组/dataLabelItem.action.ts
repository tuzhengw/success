/**
 * @date 20211012
 * @author liuyz
 * @description 获取标签库每个标签分组信息
 */
import { getDefaultDataSource } from 'svr-api/db';
import { getFile } from 'svr-api/metadata';
import { getCurrentUser } from "svr-api/security";
import { updateDwTableData } from "svr-api/dw";
let Font = Java.type("java.awt.Font");
let FontMetrics = java.awt.FontMetrics;
let BufferedImage = java.awt.image.BufferedImage;
let FontDesignMetrics = Java.type("sun.font.FontDesignMetrics");
const dwDataLabelTable = "/sysdata/data/tables/dw/DW_DATA_LABELS.tbl";
const DW_DATA_LABELS = 'SZSYS_4_DW_DATA_LABELS';

const SQL = `SELECT 
		t0.LABEL_LIB_CODE,t0.LABEL_CODE, t0.LABEL_NAME,t0.LABEL_DESC,t0.LABEL_CATEGORY,t0.FONT, t0.IMAGE_PATH,t0.IMAGE,t0.SVG,  t0.LABEL_TYPE, t0.CREATOR,
		t0.MANAGMENT_DEPT
	FROM (
		SELECT 
			t1.LABEL_LIB_CODE, t1.LABEL_CODE, t1.LABEL_NAME, t1.LABEL_CATEGORY, t1.FONT, t1.IMAGE_PATH, t1.IMAGE, t1.SVG, t1.LABEL_TYPE, t1.CREATOR,t1.LABEL_DESC,t1.MANAGMENT_DEPT 
		FROM 
			SZSYS_4_DW_DATA_LABELS t1) t0 
		INNER JOIN (
			SELECT * FROM SZSYS_4_DW_DATA_LABELS_LIB 
		WHERE 
			(MAIN_TABLE_ID = ?)	OR SHARED_MODEL_RESID LIKE ?) t1  ON(t0.LABEL_LIB_CODE = t1.LABEL_LIB_CODE) LEFT JOIN (SELECT t0.USER_ID AS 用户ID,	t0.DEPT_ID AS DEPT_ID	FROM  SZSYS_4_USERS t0 ) t2	ON (t0.CREATOR=t2.用户ID) LEFT JOIN( SELECT t0.SZ_PID1 AS SZ_PID1,	t0.DEPT_ID AS 部门ID,t0.DEPT_NAME	FROM  SZSYS_4_DEPTS t0) t3	ON (t2.DEPT_ID=t3.部门ID)`;

let ds = getDefaultDataSource();
let labelsTable = ds.openTableData(DW_DATA_LABELS);
export function getDataLabelItem(request: HttpServletRequest, response: HttpServletResponse, params: { labelCode?: string, modelIdOrPath?: string, forceUpdate?: boolean }) {
	let groups = [];
	let { labelCode, modelIdOrPath, forceUpdate } = params;
	let user = getCurrentUser();
	let isAdmin = user.isAdmin;
	let deptId = user.userInfo.deptId;
	if (labelCode) {//传递了标签id，则获取指定标签信息
		let labelInfo = labelsTable.selectFirst("*", { "LABEL_CODE": labelCode });
		if (labelInfo != null) {
			let item = getLabelItem(labelInfo, forceUpdate, request);
			item != null && groups.push(item);
		}
	} else if (modelIdOrPath) {//获取一个模型所有的标签信息
		let metaFile = getFile(modelIdOrPath, false);
		if (metaFile != null) {
			let labelInfos = ds.executeQuery(SQL, metaFile.id, metaFile.id);
			for (let index = 0; index < labelInfos.length; index++) {
				const labelInfo = labelInfos[index];
				let createDeptId = labelInfo["MANAGMENT_DEPT"];
				if (!isAdmin && createDeptId != deptId) {//非管理员，只能查看和维护自己部门的标签
					continue;
				}
				let item = getLabelItem(labelInfo, forceUpdate, request);
				item != null && groups.push(item);
			}
		}
	}
	return groups;
}

function getLabelItem(labelInfo: { [fieldName: string]: any }, forceUpdate: boolean, req: HttpServletRequest) {
	let id = labelInfo["LABEL_CODE"];
	let caption = labelInfo["LABEL_NAME"];
	let desc = labelInfo["LABEL_DESC"];
	let fontInfo = JSON.parse(labelInfo["FONT"]);
	let image = labelInfo["IMAGE"];
	let imagePath = labelInfo["IMAGE_PATH"];
	let isImage = !!(image || imagePath);
	let svg = labelInfo["SVG"];
	let category = labelInfo["LABEL_CATEGORY"];
	let labelType = labelInfo["LABEL_TYPE"];
	let creator = labelInfo['CREATOR'];
	let allow_edit = true;
	let svgHeigth = 20;
	let svgWidth = 20;
	let fontPadding = 5;
	let fontHeight = 10;
	let fontWidth = 14;
	let color = "#3D4B61";
	let fontSize = 12;
	let className = "label-svg-img";
	if (!isImage && caption) {
		//2. 获取字体宽高
		let fontName = fontInfo && fontInfo.font || "微软雅黑";
		let fontStyle = Font.PLAIN;
		fontSize = fontInfo && fontInfo.size || 20;
		color = fontInfo && fontInfo.color || "#3D4B61";
		let font = new Font(fontName, fontStyle, fontSize);
		let graphics = new BufferedImage(1, 1, 1).getGraphics();
		let fm = graphics.getFontMetrics(font);
		//fontHeight = fm.getHeight();
		fontWidth = fm.stringWidth(caption);
		//fontWidth = 14 * (caption.length);
		// svgHeigth = 2 * fontHeight;
		svgWidth = fontWidth + svgHeigth + 2 * fontPadding;
		fontSize = 14;
		className = "label-svg-word";
	} else {
		svgHeigth = 24;
		svgWidth = 24;
	}
	if (svg && forceUpdate != true) {//svg已经生成过了，则直接使用即可
		//return { "id": id, "category": category, "className": className, "caption": caption, "height": svgHeigth, "width": svgWidth, "svg": svg };
	}


	if (isImage) {// 生成引用图片标签的svg, 带圆形边框
		let href = '';
		if (imagePath) {//图片为元数据路径
			href = getBaseUrl(req) + imagePath;
		} else if (image) {//直接为base64图片数据
			href = 'data:image/png+jpg+svg;base64,' + image;
		}
		//开头
		svg = '<svg width="24" height="24" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid meet">';
		//圆形边框
		//svg += '<circle cx="12"  cy="12" r="11" fill="none" stroke="' + color + '" stroke-width=".8" stroke-opacity="1" />';
		//嵌入图片
		svg += '<image xlink:href="' + href + '" x="0" y="0" width="24" height="24"/>';
		//结束
		svg += '</svg>';
	} else if (caption) {// 生成字体标签的svg, 箭头边框
		//开头
		svg = '<svg width="' + svgWidth + '" height="' + svgHeigth + '" version="1.1" xmlns="http://www.w3.org/2000/svg">';
		//文字
		svg += '<text x="' + parseInt(fontHeight + fontPadding) + '" y="' + parseInt(fontHeight + fontPadding) + '" font-size="' + fontSize + '" fill="' + color + '">' + caption + '</text>';
		//上边框
		svg += '<line x1="0" y1="0" x2="' + parseInt(svgWidth - fontHeight) + '" y2="0" stroke="' + color + '" stroke-width="1.2"></line>';
		//下边框
		svg += '<line x1="0" y1="' + svgHeigth + '" x2="' + parseInt(svgWidth - fontHeight) + '" y2="' + svgHeigth + '" stroke="' + color + '" stroke-width=".8"></line>';
		//左上边框
		svg += '<line x1="0" y1="0" x2="' + fontHeight + '" y2="' + fontHeight + '" stroke="' + color + '" stroke-width=".6"></line>';
		//左下边框
		svg += '<line x1="0" y1="' + svgHeigth + '" x2="' + fontHeight + '" y2="' + fontHeight + '" stroke="' + color + '" stroke-width=".6"></line>';
		//右上边框
		svg += '<line x1="' + parseInt(svgWidth - fontHeight) + '" y1="0" x2="' + svgWidth + '" y2="' + fontHeight + '" stroke="' + color + '" stroke-width=".6"></line>';
		//右下边框
		svg += '<line x1="' + parseInt(svgWidth - fontHeight) + '" y1="' + svgHeigth + '" x2="' + svgWidth + '" y2="' + fontHeight + '" stroke="' + color + '" stroke-width=".6"></line>';
		//结束
		svg += '</svg>';
	}

	if (svg) {
		//3. 将svg图片入库方便下次使用
		//table.update({ "SVG": svg }, { "LABEL_CODE": id });
		return { "id": id, "category": category, "className": className, "caption": caption, "height": svgHeigth, "width": svgWidth, "svg": svg, "allow_edit": allow_edit, "label_type": labelType, "desc": desc };//带上宽高一起返回，方便设置大小
	}
	return null;
}

/**
 * 返回基本的url，是用户浏览器发送请求时的url（考虑了反向代理），包括协议、地址、端口和上下文路径，不以`/`结尾。
 *
 * 如：
 * 1. http://localhost:6080/exam/rptdsn?resid=46497529884 contextpath为/exam 返回 http://localhost:6080/exam。
 * 2. http://localhost:6080/abc/def contextpath为"" 返回 http://localhost:6080
 * 3. http://localhost:6080 返回 http://localhost:6080
 *
 */
function getBaseUrl(req: HttpServletRequest) {
	let httpServletRequest = req.getInnerHttpServletRequest();
	let url = httpServletRequest.getRequestURL().toString();
	let proto = httpServletRequest.getHeader("X-Forwarded-Proto"); // 请求头X-Forwarded-Proto说明： https://developer.mozilla.org/zh-CN/docs/Web/HTTP/Headers/X-Forwarded-Proto
	if ("https" == proto && url.startsWith("http://")) {
		url = url.replaceFirst("http:", "https:");
	}
	let index = url.indexOf("/", url.indexOf("//") + 2);
	if (index < 0) {
		return url.toString();
	}

	let context = httpServletRequest.getContextPath();
	if (context != null && context.length() != 0) {
		if (context.charAt(0) != '/') {
			context = '/' + context;
		}
		if (context.charAt(context.length() - 1) == '/') {
			context = context.substring(0, context.length() - 1);
		}
	} else {
		context = "";
	}

	return url.substring(0, index + context.length());
}

export function updateDwLabelTable(request: HttpServletRequest, response: HttpServletResponse, params: any) {
	let data = params.data;
	data && updateDwTableData(dwDataLabelTable, data);
}

/** 
 * 根据标签编码查询对应的分组标题信息
 * @param libCode 标签库编码
 */
export function queryGroupNodeInfos(request: HttpServletRequest, response: HttpServletResponse, params: { libCode: string }): DataLabelTileGroupInfo[] {
	let libCode = params.libCode;
	let queryData: DataLabelTileGroupInfo[] = [];
	let querySQL: string = `select CODE, CAPTION from SSJ_BQGL.DIM_LABEL_CATEGORY WHERE PCODE=?`;
	queryData = ds.executeQuery(querySQL, libCode);
	console.debug(`queryGroupNodeInfos()，根据标签编码查询对应的分组标题信息：\n ${queryData}`);
	return queryData;
}

/**
 * 标签选择控件data数据格式
 */
interface DataLabelTileGroupInfo extends JSONObject {
	/** 标签编码 */
	CODE?: string;
	/** 标签题目 */
	CAPTION?: string;
	/** 是否分组节点 */
	isGroupNode?: boolean;
}