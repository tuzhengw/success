/**
 * ================================================
 * 作者：tuzw
 * 创建日期：2022-09-21
 * 脚本用途：DIP脚本入口
 * ================================================
 */
import { exportQuery } from "dw/dwapi";
import { showSuccessMessage, isEmpty } from "sys/sys";
import { IMetaFileCustomJS, InterActionEvent } from "metadata/metadata";

/**
 * 目标制定脚本
 */
export class Custom_TargetMakeJs {
	public CustomActions: any;
	constructor() {
		this.CustomActions = {
			button_export_patientEntityDatas: this.button_export_patientEntityDatas.bind(this),
			button_export_TechnicalOfficeDatas: this.button_export_TechnicalOfficeDatas.bind(this)
		}
	}

	/**
	 * 定制导出病种数据
	 * @param filterDateValue 导出数据加工模型过滤日期参数值
	 * @param importFileName 导出文件名
	 */
	public button_export_patientEntityDatas(event: InterActionEvent): Promise<void> {
		let parmas = event.params;
		let filterDateValue: string = parmas?.filterDateValue;
		let importFileName = parmas?.importFileName;
		if (isEmpty(importFileName)) {
			importFileName = `导出文件`;
		}
		let exportTableStyle: ExportTableStyle = this.getExportExcelStyle();
		exportTableStyle.columns = [
			{ format: "@txt" }, { format: "@txt" }, { format: "@" }, { format: "@txt" }, { format: "0.00" },
			{ format: "0.00%" }, { format: "0.00" }, { format: "0.00" }, { format: "0.00%" }
		];
		let queryInfo: ExportDataParams = {
			resid: "aeEZ1nIV83LlQERtYg410",
			query: {
				sources: null,
				resid: "aeEZ1nIV83LlQERtYg410",
				fields: [
					{ name: "YEARMONTH" }, { name: "WDID" }, { name: "WD" }, { name: "DIPMC" }, { name: "YBTJJE" },
					{ name: "YBDFL" }, { name: "DIPFZ" }, { name: "PJZYR" }, { name: "YZB" }
				],
				filter: [],
				options: {
					limit: 0,
					offset: 0
				},
				params: [{
					name: "param2",
					value: filterDateValue
				}],
				queryId: "model5_export",
				select: true,
				sort: [],
			},
			fileFormat: DataFormat.xlsx,
			headNames: ["数据期", "维度ID", "维度", "病种名称", "医保统筹基金额（万元）", "医保兑付率（%）", "DIP总分值", "平均住院日", "药占比（%）"],
			encoding: EncodingType.GBK,
			compressed: false,
			exportTableStyle: exportTableStyle,
			fileName: importFileName
		}
		return exportQuery(queryInfo).then(() => {
			showSuccessMessage(`病种数据导出成功`);
		});
	}

	/**
	 * 获取导出列头样式
	 */
	private getExportExcelStyle(): ExportTableStyle {
		let exportTableStyle: ExportTableStyle = { // 设置导出Excel格式
			header: {
				backgroundColor: null,
				rowHeight: 27,
				font: {
					family: "宋体",
					bold: true,
					color: "#FF5C5C",
					size: 18
				}
			},
			bordersInfo: {
				bottom: {
					color: "#ECECEC",
					type: "solid",
					width: 1
				},
				middle: {
					color: "#ECECEC",
					type: "solid",
					width: 1
				}
			},
			body: {
				backgroundColor: null,
				rowHeights: [36],
				font: {
					bold: false,
					color: "#2E2E2E",
					family: "微软雅黑, MicroSoft YaHei",
					italic: false,
					lineThrough: false,
					size: 14,
					underline: false
				}
			}
		}
		return exportTableStyle
	}
}