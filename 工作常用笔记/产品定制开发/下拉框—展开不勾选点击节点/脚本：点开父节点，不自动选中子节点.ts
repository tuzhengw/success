import { encodeBase64, rc_get, waitAnimationFrame, ctx, uuid, clone, SZEvent, Component, showWarningMessage, showConfirmDialog, rc, browser, showInfoDialog, showWaiting, downloadFile, showMenu, deepEqual, showSuccessMessage, showErrorMessage, BASIC_EVENT, isEmpty, showManual, message, showError } from 'sys/sys';
import { ICustomJS, IMetaFileCustomJS, InterActionEvent, IVPage, IAppCustomJS, DatasetDataPackageRowInfo, IVComponent, InputDataChangeInfo } from "metadata/metadata-script-api";
import { ITemplatePagePartRenderer, TemplatePagePartArgs, ResourcesTree } from 'app/templatepage'
import { showMetaFileDialog, showDrillPage, getCurrentUser, IMetaFileViewer, CurrentUser, MetaFileViewer } from 'metadata/metadata';
import { exportPDFByPaths, AnaActionEvent, AnaObjectRenderer, TriggerActionsArgs } from 'ana/anabrowser';
import { RuntimePropertyInfo } from 'ana/compiler';
import { CrossQuery, FloatQuery } from 'ana/builder';
import { getDwTableDataManager, DwDataChangeEvent, exportQuery } from "dw/dwapi";
import { SuperPage, SpgComponent, showBackConfirmPanel } from 'app/superpage/superpage';
import { SuperPageBuilder, SpgComponentBuilder } from "app/superpage/superpagebuilder";
import { AnaDataset, AnaNodeData, AnaDatasetRow, AnaObjectData } from 'ana/datamgr';
import { EditMode, importTableInlineComponent, isImportedTableInlineComponent } from 'commons/table';
import { GroupTableCellHeader, TableBase } from 'ana/dashboard/components/tables';
import { DashboardComponentBuilder, DashboardDataDefinitionBuilder } from 'ana/dashboard/dashboardbuilder';
import { DataProvider, showUploadDialog, Button, ComboboxFloatPanel, Combobox } from "commons/basic";
import { UploadArgs, SZUploadEvent, ThumbUpload } from "commons/upload";
import { genAttachmentUrl, refreshModelState } from 'dw/dwapi';
import { modifyClass } from './public/commons.js';

import { SpgTreeSelector } from "app/superpage/components/inputs";
import { Dialog, DialogEventCallback, IDialogButtonArgs } from "commons/dialog";
import { SpgDialog } from 'app/superpage/components/embed';
import { MessageChangeType } from 'me/smessage';

import { Tree, List, TreeItem } from "commons/tree";
import { IExpEvalDataProvider } from "commons/exp/expeval";

/**
 *  自动触发脚本，不用指定
	
	//  随着加载SPG，自动加载脚本
	"/ZHJG/app/home.app/qyd/wjbsgl/wjbsgl.spg":
	
	spg:{
		CustomActions:{
			// 点击时触发脚本
			方法1...
 
 */
 export const CustomJS: { [type: string]: IMetaFileCustomJS } = {
	/**
	 * 20210913 tuzhengw
	 * 
	 * 解决：限制下拉框可以勾选市级、区级市场监管局的节点
	 * 思路：重写onwillcheck方法，在点击的时候，校验当前点击节点是否为特定节点，若是，则不全选
	 */
	"/ZHJG/app/home.app/qyd/wjbsgl/wjbsgl.spg": {
		/*
			准备阶段执行
		 */
		onRender: (event: InterActionEvent): Promise<void> => {
			/*
			   注意：在控制台查看需要选中：message 而不是其他，例如：error
			*/
			console.log(event);
			let id = event.component && event.component.getId();
			/*
				页面加载时会获取多个event对象，这里需要校验是否是操作的event对象
			*/
			if (id == "combobox6") {
				console.log(`==onRender:`);
				let combobox = event.component.component && event.component.component.innerComponent;
				let tree: Tree = combobox.panel && combobox.panel.component;

				let comboboxFloatPanel: ComboboxFloatPanel = combobox.panel;
				let owner: Combobox = comboboxFloatPanel.owner;
				let multipleSelect: boolean = comboboxFloatPanel.multipleSelect;
				if (tree) {
					/**
					 * 注意：this（上下文关联），在前端找到对应的this对象
					 *
					 * 保留原来tree的onwillcheck方法
					 * let onwillcheck_old = tree.onwillcheck;
					 * 不能勾选父节点时，换为勾选所有叶子节点
					 */
					tree.onwillcheck = (sze: SZEvent, list: List, item: TreeItem) => {
						let result: boolean;
						if (owner.isTree && owner.selectLeafOnly && owner.checkboxVisible && multipleSelect && !item.isLeaf()) {
							result = false;
							let noChooseItemsArray: Array<string> = ["320200000000", "320205000000", "320206000000", "320211000000", "320213000000", "320214000000",
								"320281000000", "320282000000", "320292000000"];
							let currentItemId: string = item.id;
							if (noChooseItemsArray.indexOf(currentItemId) != -1) {
								/**
								 * 将当前节点勾选状态保持不变（无勾选状态）
								 */
								item.setChecked(false);
								/**
								 * comboboxFloatPanel.doCheckItem(); 等同于：this.doCheckItem()
								 * doCheckItem：统一处理勾选选项的事件，包括全选，allCheckedClicked? ：是否点击了全选勾选框
								 */
								comboboxFloatPanel.doCheckItem();
							} else {
								if (item.getChecked() === false) {
									/**
									 * 将其自己 以及 子节点 都勾选
									 */
									item.loadChildren(true).then(childrens => {
										childrens.filter(e => e.isLeaf() && e.setChecked(true));
										comboboxFloatPanel.doCheckItem();
									});
								} else {
									item.setChecked(false);
									comboboxFloatPanel.doCheckItem();
								}
							}
						}
						return result;
					}
				}
			}
			return Promise.resolve();
		},
		/**
			刷新阶段执行
		 */
		onRefresh: (event: InterActionEvent): Promise<void> => {
			let id = event.component && event.component.getId();
			if (id == "combobox6") {
				console.log(`==onRefresh:`);
			}
			return Promise.resolve();
		},
		onDidRefresh: (event: InterActionEvent): Promise<void> => {
			let id = event.component && event.component.getId();
			if (id == "combobox6") {
				console.log(`==onDidRefresh:`);
			}
			return Promise.resolve();
		}
	}

 }