

import utils from "svr-api/utils";
import bean from "svr-api/bean";

function main() {
	let checkTask;
    try {
		checkTask = bean.getBean("com.succez.dg.services.impl.DGServiceCheckTaskExecutor");
		checkTask.setOptions({ "uuid": utils.uuid(), "checkTaskId": checkTaskId });
		checkTask.run(null);
	} catch (e) {
		resultInfo = { result: false, msg: "质控规则存在问题, 无法进行质控" };
		console.error(checkTask.getProgress().getLogs()); // 将质控错误日志打印出来
		console.error(`质控规则存在问题, 无法进行质控`);
		console.error(e);
	}
}