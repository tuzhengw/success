---
typora-root-url: images
---

文档仅展示部分堆栈，详细可见本目录堆栈文件夹

> `Flink UI  ` 界面展示的日志若不是首次出现，展示的日志不一定是详情日志，需上下翻动或者服务器查看日志文件

# 任务配置

![](/任务配置.png)

# 1 同包同类覆盖

> [<font color='red'> `Caused by: java.lang.NoSuchMethodError: io.debezium.connector.mysql.MySqlConnection$MySqlConnectionConfiguration.<init>(Lio/debezium/config/Configuration;Ljava/util/Properties;)V`</font>]　

​	**版本**：`MySQL ` `CDC 2.4`

​	**参考**：https://github.com/ververica/flink-cdc-connectors/issues/2340

​	**原因**：内部jar包-> CDC 2.4和对应的`debezium-connector-mysql-1.9.7.Final.jar` 都存在`MySqlConnectionConfiguration`类，其构造参数不一致，导致无法正确初始化示例对象，其错误来源内部jar包，需要更改源码。

​	根本原因是[https://github.com/ververica/flink-cdc-connectors/pull/1217]提交修改后的类名已有类名冲突，可根据[https://github.com/ververica/flink-cdc-connectors/commit/5415bcf0f43db627cd3604697245cc07a630f808]提交差异修改源码还原类名，然后重新打包，已对此问题进行提问：https://github.com/ververica/flink-cdc-connectors/issues/2423

```
Caused by: java.lang.NoSuchMethodError: io.debezium.connector.mysql.MySqlConnection$MySqlConnectionConfiguration.<init>(Lio/debezium/config/Configuration;Ljava/util/Properties;)V
	at com.ververica.cdc.connectors.mysql.debezium.DebeziumUtils.createMySqlConnection(DebeziumUtils.java:85)
	at com.ververica.cdc.connectors.mysql.debezium.DebeziumUtils.createMySqlConnection(DebeziumUtils.java:78)
	at com.ververica.cdc.connectors.mysql.source.reader.MySqlSplitReader.getSnapshotSplitReader(MySqlSplitReader.java:229)
	at com.ververica.cdc.connectors.mysql.source.reader.MySqlSplitReader.pollSplitRecords(MySqlSplitReader.java:113)
	at com.ververica.cdc.connectors.mysql.source.reader.MySqlSplitReader.fetch(MySqlSplitReader.java:80)
	at org.apache.flink.connector.base.source.reader.fetcher.FetchTask.run(FetchTask.java:58)
	at org.apache.flink.connector.base.source.reader.fetcher.SplitFetcher.runOnce(SplitFetcher.java:142)
	at org.apache.flink.connector.base.source.reader.fetcher.SplitFetcher.run(SplitFetcher.java:105)
	at java.base/java.util.concurrent.Executors$RunnableAdapter.call(Executors.java:515)
	at java.base/java.util.concurrent.FutureTask.run(FutureTask.java:264)
	at java.base/java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1128)
	at java.base/java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:628)
	... 1 more
```

​	**解决办法**：下载对应版本 `cdc` 分支代码，自行打包，此时打包是一个fat jar 包，其包含依赖jar包。

​	仓库地址：https://github.com/ververica/flink-cdc-connectors

​	参考：https://zhuanlan.zhihu.com/p/43238220

​	在 jar 包里嵌套其他 jar，这个方法可以彻底避免解压同名覆盖的问题，但是这个方法不被 `JVM` 原生支持，因为` JDK `提供的 `ClassLoader `仅支持装载嵌套 jar 包的 class 文件。所以这种方法需要自定义 `ClassLoader `以支持嵌套 jar。

​	将`cdc`以及依赖jar放置同一个jar包，这样 引用环境不需要使用maven加载对应`cdc`相关jar包，应用环境和`flink` 环境平台仅放置对应的` flink-sql-connector-mysql-cdc-${版本号}` 的jar包即可，仅限`cdc`。

​	**详细日志**：

![](/展示直接异常日志.png)

​	实际的日志，搜索：`Could not initialize class io.debezium.embedded.EmbeddedEngine$EmbeddedConfig`

![](/connect-api版本冲突.png)







# 2 取消任务180秒未成功 shut down task

` Task did not exit gracefully within 180 + seconds.`

测试素材：2张千万级别的表，同步模型：initial，并行度：1

```JAVA
2024-03-20 14:39:55,210 ERROR org.apache.flink.runtime.taskexecutor.TaskExecutor           [] - Task did not exit gracefully within 180 + seconds.
org.apache.flink.util.FlinkRuntimeException: Task did not exit gracefully within 180 + seconds.
	at org.apache.flink.runtime.taskmanager.Task$TaskCancelerWatchDog.run(Task.java:1778) [flink-dist-1.16.2.jar:1.16.2]
	at java.lang.Thread.run(Thread.java:834) [?:?]
2024-03-20 14:39:55,210 ERROR org.apache.flink.runtime.taskexecutor.TaskManagerRunner      [] - Fatal error occurred while executing the TaskManager. Shutting it down...
org.apache.flink.util.FlinkRuntimeException: Task did not exit gracefully within 180 + seconds.
	at org.apache.flink.runtime.taskmanager.Task$TaskCancelerWatchDog.run(Task.java:1778) [flink-dist-1.16.2.jar:1.16.2]

```



# 3 检查点在完成之前已过期

测试素材：2张千万级别的表，同步模型：initial，并行度：2，检查点超时时间：30分钟，检查点间隔时间：10分钟

```java
org.apache.flink.runtime.checkpoint.CheckpointException: Checkpoint expired before completing.
	at org.apache.flink.runtime.checkpoint.CheckpointCoordinator$CheckpointCanceller.run(CheckpointCoordinator.java:2172) [flink-dist-1.16.2.jar:1.16.2]
	at java.util.concurrent.Executors$RunnableAdapter.call(Executors.java:515) [?:?]
	at java.util.concurrent.FutureTask.run(FutureTask.java:264) [?:?]
	at java.util.concurrent.ScheduledThreadPoolExecutor$ScheduledFutureTask.run(ScheduledThreadPoolExecutor.java:304) [?:?]
	at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1128) [?:?]
	at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:628) [?:?]
	at java.lang.Thread.run(Thread.java:834) [?:?]
```

**原因**：检查点超过指定时间完成视为失败，可以调整检查点超时时间解决，或者直接设置：保存点失败不shut down任务

```java
env.getCheckpointConfig().setCheckpointTimeout(900000);
```

默认情况下，如果Checkpoint过程失败，会导致整个应用重启，我们可以关闭这个功能，这样Checkpoint失败不影响作业的运行

```
env.getCheckpointConfig.setFailOnCheckpointingErrors(false).
```



# 4 `Dlink`无法使用保存点恢复

​	提交的任务和指定保存点记录的序列化状态不一致

​	issue地址：https://github.com/DataLinkDC/dinky/issues/3318

```
根据源码分析，dlink内部在sink表时，是先获取指定schema下所有表，然后内部match同步的表，这里不用同步的表去匹配同步的表，是因为要支持正则匹配同步的表，导致每次生成的算子图顺序不一致
我看了下 ExecutorSetting对象可以支持savepointPath入参，但指定了路径，而不指定execution.savepoint.path，提交后保存点没有生效，我尝试入参和恢复保存点路径都指定，提交任务后保存点成功恢复且每次提交生成的算子图一致，例如：

HashMap<String, String> tableConf = new HashMap<>();
tableConf.put("execution.savepoint.path", "file:///tmp/flink/savepoints/savepoint-d031e7-a02abd60af81");
String savepointPath = "file:///tmp/flink/savepoints/savepoint-d031e7-a02abd60af81";
ExecutorSetting executorSetting = new ExecutorSetting(3000, 1, false, false, false, savepointPath, testJobName, tableConf);
Executor executor = new RemoteStreamExecutor(eEnvironmentSetting, executorSetting);

本地测试这块代码通过，具体问题还需要进一步研究下ExecutorSetting对象入参保存点内部到底做了什么操作以及为什么指定后算子图每次顺序一致
```



```java
Caused by: org.apache.flink.util.StateMigrationException: For heap backends, the new state serializer (org.apache.flink.api.common.typeutils.base.ListSerializer@706bfa31) must not be incompatible with the old state serializer (org.apache.flink.api.common.typeutils.base.ListSerializer@50845561).
	at org.apache.flink.runtime.state.heap.HeapKeyedStateBackend.tryRegisterStateTable(HeapKeyedStateBackend.java:247) ~[flink-dist-1.16.2.jar:1.16.2]
	at org.apache.flink.runtime.state.heap.HeapKeyedStateBackend.createOrUpdateInternalState(HeapKeyedStateBackend.java:326) ~[flink-dist-1.16.2.jar:1.16.2]
	at org.apache.flink.runtime.state.heap.HeapKeyedStateBackend.createOrUpdateInternalState(HeapKeyedStateBackend.java:313) ~[flink-dist-1.16.2.jar:1.16.2]
	at org.apache.flink.runtime.state.KeyedStateFactory.createOrUpdateInternalState(KeyedStateFactory.java:47) ~[flink-dist-1.16.2.jar:1.16.2]
	...
```

## 4.1 分析`Flink REST API`设置保存点

​	排查步骤1：根据异常信息得到是后端状态数据序列化不一致导致的，怀疑生成的保存点不正确，缺失后端状态文件**（保存点没有正常完成或者不仅仅只有一个`metadata`文件，还有对应的`datastate`文件）**

----

​	`Flink`的`/jobs/:jobid/savepoints API` 创建保存点时，`Flink`会生成一个包含应用程序的拓扑结构、配置信息和与状态相关的元信息的`metadata`文件。

​	这个`metadata`文件实际上是一个描述性文件，用于指导`Flink`在恢复时重新构建作业图和状态。

​	而实际的数据状态是存储在所配置的状态后端中的。`Flink`支持多种状态后端，例如`RocksDB`、`MemoryStateBackend`等。

​	当使用保存点来恢复应用程序时，`Flink`会根据`metadata`文件**中**的元信息和所配置的状态后端，从相应的状态后端中加载实际的数据状态。

> 经过反复验证，得到结论：
>
> 记录的保存点文件目录仅：`metadata `文件（含些元数据，像：`rocksdb.column.family.name`以及 state data的全目录）

![](/保存点文件示例.png)



## 4.2 分析`rocksdb`后端状态文件是否记录

​	目前任务后端状态由：hash（存储在`JVM`内存中）改成`rocksdb`存储到本地服务器上，经过测试：<font color='red'>正常取消会删除当前任务所有`rocksdb`存储的后端状态</font>（非正常取消则不会）.

​	源码分析`rocksdb`在任务取消后，内部执行了dispose()删除了保存的数据状态文件，且<font color='red'>外部无参数</font>可以控制是否保留这部分文件.

![](/rocksdb取消任务删除记录的数据状态文件源码.png)

​	尝试取消任务前，将记录的后端状态文件迁移到另外一个文件下，取消任务后，按记录的保存点恢复前，将备份的后端状态文件重新迁移到：`rocksdb` 存储后端状态数据目录（`state.backend.rocksdb.log.dir`）下，测试结果：<font color='red'>失败</font>.

## 4.3 `CDC SOURCE`生成算子图分析

​	整个流程走的是`dlink`整库同步流程，目前除去`CDC 3.0`外，官方还未支持整库同步，每个任务仅单表同步，只要提交的信息未发生变化，那每次提交的算子图是一致的，故通过保存点恢复单个任务同步是无问题.

​	`dlink`整库同步是一源多用的情况，例如：

![](/整库同步生成算子图.png)

​	经过测试，多表情况下（表数量越多），其每次生成的算子图顺序不一致的概率越高，导致按保存点文件恢复时，若提交的任务生成的算子图和记录保存点时任务的<font color='red'>算子图一致</font>.，则按：保存点文件提交任务：成功，否则：<font color='red'>失败</font>.

## 4.4 `dlink`生成算子图顺序源码分析

​	若`FlinkSQL`同步部分表, 例如: `"test\.student,test\.score"`，则会走下方逻辑，根据源表schema进行分类，依次获取单个schema所有表，然后判断那些表符合同步的表：

![](/dlink源码匹配同步表位置.png)

​	`dlink`是先根据数据库信息，获取对应schema下所有的表，然后循环使用正则匹配同步的表，这也就导致<font color='red'>无法保证</font>每次提交任务时，查询数据库下所有表返回的顺序每次都是一致的.

​	这里主要是考虑到了<font color='red'>正则匹配</font>同步表方式，故没用同步表去匹配数据库已存在的表，而且正则匹配：不能保证每次返回匹配结果表顺序一致.

---

​	算子图生成源码分析：

![](/flink算子图生成顺序逻辑.png)

​	生成算子图是根据`tagMap`存储的表顺序进行构造的，源码本身`tagMap`是`HashMap`无序存储，上方截图是我修改源码为有序Map存储，由于内部是无序存储，也就导致外部无法通过配置要求内部根据同步表的顺序生成算子图.

## 4.5 修改生成算子图源码

​	查看内部构造逻辑，处理算子图生成顺序方式有几种，如下：

### 4.5.1 在源头对数据源下所有表排序

​	通过源码可以得到，不管是整库还是部分表都是直接先根据schema获取其下所有已存在的表，然后在根据指定的匹配内容进行正则匹配，将满足条件的表添加到：`schemaTable`集合中，虽然记录的集合是List有序集合，但算子图生成那边又重新匹配并用`HashMap`无序存储，如果要改源码，那么需要改：<font color='red'>`CreateCDCSourceOpeartion.java`</font>和<font color='red'>`SQLSinkBuilder.java`</font>两个类实现.

​	对`SchemaList`（源表schema不唯一）以及其下的`dirver.listtables`的结果进行排序，同时构建算子图的hash改成有序`LinkHash`存储.

> 注意：对整个获取的结果集排序，只要成功匹配的结果表名和数量不发生变化，即使数据库下表名或者表数量发现变化，不影响成功匹配返回的表的相对位置，例如：
>
> `jira`下有4张表：`jira.student,jira.arr1,jira.arr2,jira.teacher`
> 同步表：`jira.arr* -> jira.arr1,jira.arr2`
> 即使删除`jira.student`表，成功匹配返回的表依然是：` jira.arr1,jira.arr2`，任务顺序未发生变化.

​	整库和部分表获取和匹配源码如下：

![](/整库同步和部分表匹配源码位置.png)

---

### 4.5.2 对处理的结果集进行排序

​	按照`FlinkSQL`同步表的顺序，添加到构建算子图集合中,  按照同步的表的顺序，对单个匹配表结果进行排序，若同步表中有个是正则，则对当前正则匹配的结果集排序，例如：

> 同步表内容：`jira.student,jira.arr*,jira.teacher`
>
> 先匹配：`jira.student`
>
> 再匹配：`jira.arr1,jira.arr2 `(对当前批次内的结果排序，若匹配成功的表被删除，下次将无法匹配)
>
> 最后匹配：`jira.teacher`
>
> 最终匹配的顺序：`jira.student,jira.arr1,jira.arr2,jira.teacher`

​	**源码修改**：

![](/根据flinkSQL同步表顺序添加算子图集合.png)

​	下方源码仅单个schema和无正则的情况，若有正则匹配内容或者多个源schema，则对两者进行排序即可，由于不是最佳方案.

​	最佳方案：考虑到存在多源schema的情况下，先对schema进行升序排序，再依次对单个schema内同步表进行升序排序，源码修改：

![](/对最终同步schema和表升序排序.png)



