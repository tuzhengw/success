import { MetaFileViewer_miniapp } from "app/miniapp.js";
import { SpgEmbedSuperPage } from "app/superpage/components/embed";
import { SuperPage } from "app/superpage/superpage.js";
import { SuperPageData } from "app/superpage/superpagedatamgr";
import { Dialog } from "commons/dialog";
import { ExportReusltType, html2pdf } from "commons/export";
import { ExTableCellBuilder } from "commons/extable";
import { ProgressPanel } from "commons/progress";
import { getQueryManager, pushData, getDwTableDataManager } from "dw/dwapi";
import { FAppFloatAreaData, FAppFloatAreaDataRow } from "fapp/form/fappdatamgr";
import { getCurrentUser } from "metadata/metadata";
import { FAppInterActionEvent, InterActionEvent, IVPage } from "metadata/metadata-script-api";
import { showResourceDialog } from "metadata/metamgr";
import {
	Component, formatDate, isEmpty, parseDate, rc, showConfirmDialog, showErrorMessage, 
	showProgressDialog, showSuccessMessage, showWaiting, SZEvent, uuid, showWarningMessage, assign 
} from "sys/sys";

/**
 * 数据交换列表界面类
 */
export class Spg_Data_Exchange {
	public CustomActions: any;
	constructor() {
		this.CustomActions = {
			button_ex_openResourceDialog: this.button_ex_openResourceDialog.bind(this)
		}
	}

	/**
	 * 20220621 tuzw
	 * 问题：数据开发中心-数据交换登记-新增交换详情界面-资源选择支持多选
	 * 地址：https://jira.succez.com/browse/CSTM-19281
	 * @param rootPath 选择资源的根目录，eg：/TestCase
	 * @param inputCompantId 选择内容写入到那个控件中
	 * @param moduleComboboxId 业务板块下拉框ID
	 * @param topicComboboxId 业务主题下拉框ID
	 */
	private button_ex_openResourceDialog(event: InterActionEvent): void {
		let page = event.page;
		let param = event?.params as JSONObject;
		let rootPath: string = param.rootPath;
		let inputCompantId: string = param.inputCompantId;
		let moduleComboboxId: string = param.moduleComboboxId;
		let topicComboboxId: string = param.topicComboboxId;

		if (isEmpty(rootPath) || isEmpty(inputCompantId) || isEmpty(moduleComboboxId) || isEmpty(topicComboboxId)) {
			showWarningMessage(`参数为空，打开选择资源对话框失败`);
			return;
		}
		let inputComp = page.getComponent(inputCompantId);
		let choseValue: string | string[] = inputComp.getValue();
		if (!isEmpty(choseValue)) {  // 获得之前选中的值，用于重新打开时，进行勾选渲染
			choseValue = choseValue.split(",");
		}
		let self = this;
		showResourceDialog({
			id: "metaResourceDialog-exchangeResource",
			caption: "选择交换资源",
			returnTypes: ["tbl"],
			checkBoxVisible: true,
			multiple: true,
			value: choseValue,
			rootPath: rootPath.trim(),
			multipleCheckable: true,
			enableEmptySelect: true,
			onok: function (event1: SZEvent, component?: Component, items?: any) {
				let choseResourceId: string[] = [];
				self.getChoseModelId(items, choseResourceId);
				let inputComp = page.getComponent(inputCompantId);
				inputComp.setValue(choseResourceId.join(",")); // 将选择的资源ID写入到指定的控件中
				if (choseResourceId.length != 0) {
					self.getBusinessModuleAndTopic(choseResourceId).then((result) => {
						page.setParameter("businessModuleValues", result.moduleIds); // 将下拉框勾选值设置到全局参数
						page.setParameter("businessTopicValues", result.topicIds);
					});
					showSuccessMessage("资源设置成功");
				} else {
					showSuccessMessage("未选择资源");
				}
			}
		})
	}

	/**
	 * 根据资源ID获取对应-业务板块和业务主题数据，并将其内容设置到指定的下拉框控件中
	 * @param sourceIds 资源ID数组
	 * @return 
	 */
	private getBusinessModuleAndTopic(sourceIds: string[]): Promise<any> {
		if (sourceIds.length == 0) {
			return Promise.resolve({ moduleIds: [], topicIds: [] });;
		}
		let queryInfo: QueryInfo = {
			fields: [{
				name: "BUSINESS_MODULE", exp: `CASE WHEN ([是否为文件夹] = 0 and STARTSWITH([父路径], '/sdi/data/tables/domains')) THEN REGEXP_EXTRACT([父路径], '[^/]+', 6) WHEN ([是否为文件夹] = 0 and STARTSWITH([父路径], '/sdi/data/tables/main-data')) then REGEXP_EXTRACT([父路径], '[^/]+', 5)  else '' END`
			}, {
				name: "BUSINESS_TOPIC", exp: `CASE WHEN ([是否为文件夹] = 0 and STARTSWITH([父路径], '/sdi/data/tables/domains')) THEN REGEXP_EXTRACT([父路径], '[^/]+', 7) WHEN ([是否为文件夹] = 0 and STARTSWITH([父路径], '/sdi/data/tables/main-data')) THEN REGEXP_EXTRACT([父路径], '[^/]+', 6) ELSE '' END`
			}],
			filter: [{
				exp: `model1.ID in '${sourceIds.join(",")}'`
			}],
			sources: [{
				id: "model1",
				path: "/sysdata/data/tables/meta/META_FILES.tbl"
			}]
		};
		return getQueryManager().queryData(queryInfo, uuid()).then((result: QueryDataResultInfo) => {
			let datas = result.data;
			if (datas.length == 0) {
				showWarningMessage("未从文件表中获取到对应的资源信息");
				return;
			}
			let moduleIds: string[] = [];
			let topicIds: string[] = [];
			for (let i = 0; i < datas.length; i++) {
				let moduleId: string = datas[i][0] as string;
				if (!isEmpty(moduleId)) {
					moduleIds.push(moduleId.toLocaleLowerCase());
				}
				let topicId: string = datas[i][1] as string;
				if (!isEmpty(topicId)) {
					topicIds.push(topicId.toLocaleLowerCase());
				}
			}
			return { moduleIds: moduleIds, topicIds: topicIds };
		});
	}

	/**
	 * 获取选中的文件夹下的所有子文件和选择的文件
	 * @param dirPathOrFile 当前勾选的目录路径或者文件
	 * @param choseResourceId 选择的资源ID（模型、文件、样式等）
	 */
	public getChoseModelId(dirPathOrFiles, choseResourceId: string[]): void {
		dirPathOrFiles.forEach(item => {
			if (item.isFolder && item?.children?.length > 0) {
				let children = item.children;
				this.getChoseModelId(children, choseResourceId);
			} else if (!item.isFolder) {
				let id: string = item.id;
				choseResourceId.push(id);
			}
		})
	}
}