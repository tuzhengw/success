import { showDrillPage, getMetaRepository, MetaFileViewer, getCurrentUser, getSysSettings } from 'metadata/metadata';

showDrillPage({
	url: getAppPath('/elem-viewer/elem-viewer.spg?sjydm=' + data),
	title: '数据元信息',
	target: ActionDisplayType.Dialog,
	breadcrumb: true, // 面包屑
	dialogArgs: {
		buttons: [{
			id: 'close',
			caption: '取消'
		}]
	},
})