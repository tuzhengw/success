import { showMessage } from 'sys/sys';
import { showMessageBox, MessageBoxArgs, MessageContentInfo, MessageBoxType } from 'commons/basic';

/**
 * 显示提示类型MessageBox。
 * @param content
 */
export function showMessage(content: MessageContentInfo): Promise<MessageBox | void> {
	return showMessageBox({
		content: content
	})
}