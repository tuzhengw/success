/**
 * 如果obj里有函数
 * 则序列化的结果会把函数或 undefined丢失
 */
function main() {
    const test = {
        name: 'a',
        date: function hehe() {
            console.log('fff')
        },
    };
	// parse：重新序列化
    const copyed = JSON.parse(JSON.stringify(test));
    // { name: 'a', date: [Function: hehe] }
    console.log(test);
    // { name: 'a' }
    console.log(copyed);
    // console.error('ddd', test, copyed);
}