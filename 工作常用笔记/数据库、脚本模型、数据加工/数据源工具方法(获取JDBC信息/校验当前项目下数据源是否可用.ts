
import bean from "svr-api/bean";
import db from "svr-api/db";
/**
 * 校验指定数据源在当前项目模块下是否可用
 
 * 注意：连接成功 和 可用是两种状态
 
 * @description 20220808 tuzw
 * 帖子地址：https://jira.succez.com/browse/CSTM-19956
 * 
 * @param projectName 当前项目名，eg：sys、sdi
 * @param dbSourceName 检测的数据源名
 * @return 
 */
export function isCheckDbSourceUse(request: HttpServletRequest, response: HttpServletResponse, params: {
    projectName: string,
    dbSourceName: string
}): { result: boolean, isValid?: boolean, message?: string } {
    let dbSourceName: string = params.dbSourceName;
    let projectName: string = params.projectName;
    print(`开始校验当前数据源：【${dbSourceName}】状态`);
    if (!dbSourceName || !projectName) {
        print(`校验当前数据源状态是否有效，参数不合法，dbSourceName：${dbSourceName}、projectName：${projectName}`);
        return { result: false, message: "校验当前数据源状态是否有效，参数不合法" };
    }
    let dsService = bean.getBean("com.succez.dw.services.impl.DwServiceDataSourcesServiceImpl");
    let isValid: boolean = dsService.checkDataSourceValid("sdi", dbSourceName);
    return { result: true, isValid: isValid };
}