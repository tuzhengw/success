

```ts
/**
 * 查询附件信息
 * @param queryAttachmentInfos 查询指定主键的附件信息
 * @return 
 */
function queryXMLAttachmentInfo(attachPkInfos: XMLAttachmentInfo[]): ResultInfo {
    let resultInfo: ResultInfo = { result: true };
    let optionDb = db.getDataSource("HLHT");
    let dbConn = optionDb.getConnection();
    // @ts-ignore
    let stmt: JdbcPreparedStatement = null;
    // @ts-ignore
    let resultSet: JdbcResultSet = null;
    let queryAttachInfos: XMLAttachmentInfo[] = [];
    try {
        let querySQL: string = `
            SELECT WORD_ID, EMPIID, XML_FILE_NAME, FILE_BYTE_CONTENT FROM SDI_HLHT_HISTORY_XML_ATTACH WHERE WORD_ID = ? AND EMPIID = ?`;
        stmt = dbConn.prepareStatement(querySQL);
        for (let i = 0; i < attachPkInfos.length; i++) {
            let xmlAttach = attachPkInfos[i];
            stmt.setString(1, xmlAttach.WORD_ID as string);
            stmt.setString(2, xmlAttach.EMPIID as string);
            resultSet = stmt.executeQuery();
            while (resultSet.next()) {
                let wordId: string = resultSet.getString(1);
                let xmlPkValue: string = resultSet.getString(2);
                let xmlFileName: string = resultSet.getString(3);
                let xmlFileContent: number[] = resultSet.getBytes(4);
                queryAttachInfos.push({
                    WORD_ID: wordId,
                    EMPIID: xmlPkValue,
                    XML_FILE_NAME: xmlFileName,
                    FILE_BYTE_CONTENT: xmlFileContent
                });
            }
        }
    } catch (e) {
        resultInfo.result = false;
        resultInfo.message = "查询XML附件信息失败";
        console.info(`执行[queryXMLAttachmentInfo], 查询XML附件信息失败----error`);
        console.info(e);
    } finally {
        resultSet && resultSet.close();
        stmt && stmt.close();
        dbConn && dbConn.close();
    }
    return resultInfo;
}
```

