# mysql：insert ignore、insert和replace区别

# 创建测试表
# 表要求：有PrimaryKey，或者unique索引
# 结果：表id都会自增
CREATE TABLE names(
    id INT(10) PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(255) UNIQUE,
    age INT(10)
)

# 插入测试数据
insert into names(name,age) values("小明", 23),("大红", 24),("大壮", 45);

# insert
# 插入已存在，但是插入不成功，会报错《id会自增》
insert into names(name, age) values("小明", 24);
# Duplicate entry '小明' for key 'name'

# replace
# 替换已存在的，删除原来的记录《id会自增》
# 不存在替换，添加新的记录
replace into names(name, age) values("小明", 23);


# insert ignore
# 当前插入数据已存在，忽略新插入的记录，id会自增，不会报错《id会自增》 
# 插入不存在，添加新的记录
insert ignore into names(name, age) values("大壮", 25);












