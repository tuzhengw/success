/**
 * 作者：tuzw
 * 创建时间：2022-11-01 
 * 脚本用途：XML后端脚本
 */
import fs from "svr-api/fs";
import sys from "svr-api/sys";

/**
 * 2022-11-01 tuzw
 * 获取服务器上XML文件内容
 * 地址： https://jira.succez.com/browse/CSTM-21033
 * 
 * @param fileSaveAddress xml服务器存储地址, eg: workdir:2022102716513035301_1.个人基本健康信息登记.xml
 * @return 
 */
export function getServiceXmlFileContent(request: HttpServletRequest, response: HttpServletResponse, params: {
    fileSaveAddress: string
}): string {
    console.info(`开始执行[xml.getServiceXmlFileContent], 获取服务器上XML文件内容`);
    console.info(params);

    let fileSaveAddress: string = params.fileSaveAddress;
    if (!fileSaveAddress) {
        return "未获取当前XML文件存储地址";
    }
    let xmlFileName: string = fileSaveAddress.split(":")[1]; // 2022103117591597301_1.个人基本健康信息登记.xml
    if (xmlFileName.lastIndexOf(".xml") == -1) {
        return `${xmlFileName} 不是xml文件`;
    }
    let shareDir: string = sys.getClusterShareDir();
    let attachParentDir: string = `${shareDir}/file-storage/relativePath`;
    let xmlFilePath: string = `${attachParentDir}/${xmlFileName}`;
    let xmlFile = fs.getFile(xmlFilePath);
    console.info(`开始获取服务器[${xmlFilePath}] 文件内容, 文件大小: ${xmlFile.length()}`);
    if (!xmlFile.exists()) {
        return `${xmlFileName} 不存在`;
    }
    let xmlContent: string = xmlFile.readString();
    console.info(`执行结束[xml.getServiceXmlFileContent], 获取服务器上XML文件内容`);
    return xmlContent;
}