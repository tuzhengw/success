
import { IMetaFileCustomJS, InterActionEvent, IAppCustomJS, IDwCustomJS, FAppInterActionEvent, IFApp, IDataset, IFAppForm, IVComponent } from "metadata/metadata-script-api";
import { showDrillPage, getMetaRepository, MetaFileViewer, getCurrentUser, getSysSettings, IMetaFileViewer } from 'metadata/metadata';
import { IVPage } from 'metadata/metadata-script-api';
import { url2pdf, ExportReusltType, ExportSizeOptionsInfo, createExportPDFIFrame, PdfBuilder, ExportTextInfo, ExportTextCompInfo, ExportCompType } from "commons/export";
import { ctx, showWaitingIcon, waitIframeRender } from "sys/sys";

/** 身份证号 */
let cardValues: string = "";
/** 页面日期过滤值，默认时间：当天 */
let filterDateValue: string = formatDate();


/**
 * 数据服务
 */
const DATA_SERVICE = {
    /**
     * 20211207 tuzw
     * spg中列表需要可以pdf格式导出，且带上背景
     * 帖子地址：https://jira.succez.com/browse/CSTM-17253
	 * 脚本地址：http://192.168.9.199:8080/SJDWFW/app/%E6%95%B0%E6%8D%AE%E6%9C%8D%E5%8A%A1.app?:edit=true&:file=custom.ts
     */
    importPDF: (event: InterActionEvent) => {
        let page = event.page;
        let fileInfo: MetaFileInfo = page.getFileInfo();
        let params = event.params;
        let pageUniqueCode: string = params.pageUniqueCode;
        let isDisplayTopDom: boolean = params.isDisplayTopDom;
        let importPdfName: string = fileInfo.name;
        /** 是否导出PDF方法: url2pdf()打开的页面 */
        let isImportOpenPage: boolean = params.isImportOpenPage;
        /*
			导出PDF大小信息
			let exportSizeOptionsInfo: ExportSizeOptionsInfo = {
				height: 5000,
				width: 5500
			}
	    */
        let exportPdf = (): Promise<void> => {
            let width = 793;
            let height = 1122;
            let frame = createExportPDFIFrame(width, height);
            frame.src = ctx(`${fileInfo.path}?pageUniqueCode=${pageUniqueCode}&isDisplayTopDom=${isDisplayTopDom}&isImportOpenPage=${isImportOpenPage}`);
            return waitIframeRender(frame, 100000).then(() => {
                let list1 = frame.contentDocument.getElementById('list1').szobject;
                return list1.getExportTableBuilder().then((tableBuilder) => {
                    let pdfBuilder = new PdfBuilder();
                    pdfBuilder.setName(importPdfName);
                    pdfBuilder.addTableBuilder(tableBuilder, { titleRows: '1' });//第一行为标题行
                    pdfBuilder.setAlign(TextAlign.Center);
                    //纸张大小，A1横向
                    let paperHeight = 2245;
                    let paperWidth = 3178;
                    pdfBuilder.setPageSize({ width: paperWidth, height: paperHeight});
                    pdfBuilder.setMargin({
                        left: 10,
                        right: 10,
                        top: 10,
                        bottom: 10
                    });// 页边距
                    pdfBuilder.setZoomScale(null);
                    pdfBuilder.setFitToWidth(1);//缩放到一页宽
                    //计算水印信息
                    let watermark = event.params.watermark;
                    let watermarkHeight = 300;
                    let watermarkWidth = 300;
                    let watermarkTextInfo: ExportTextInfo = {
                        text: watermark,
                        fontInfo: {
                            family: '微软雅黑, MicroSoft YaHei',
                            color: '#DFE0E4',
                            size: 20
                        },
                        align: TextAlign.Center
                    };
                    let watermarkInfos: ExportTextCompInfo[]=[];
                    for (let i = 0, rl = Math.ceil(paperHeight / watermarkHeight); i < rl; i++) {
                        for (let j = 0, cl = Math.ceil(paperWidth / watermarkWidth); j < cl; j++) {
                            let textInfo: ExportTextCompInfo = {
                                type: ExportCompType.text,
                                top: i * watermarkHeight,
                                left: j * watermarkWidth,
                                width: watermarkWidth,
                                height: watermarkHeight,
                                data: [watermarkTextInfo],
                                vAlign: VerticalAlign.Middle,
                            };
                            watermarkInfos.push(textInfo);
                        }
                    }
                    return pdfBuilder.getPdfInfo().then((pdfInfo) => {
                        if (!pdfInfo.fonts['微软雅黑-normal']) {
                            pdfInfo.fonts['微软雅黑-normal'] = {
                                allNames: ["微软雅黑", "MicroSoft YaHei"],
                                fontStyle: "normal",
                                name: "微软雅黑"
                            };
                        }
                        pdfInfo.pages.forEach(page=>{
                            page.components.splice(0, 0, ...watermarkInfos);
                        });
                        return import("commons/jspdf/pdfgen").then((m) => {
                            return m.saveAsPdf(fileInfo.id, pdfInfo);
                        });
                    });
                });
            });
        };
        /**
         * url2pdf：将url的内容转换成PDF
         * （1）会重新打开一个SPG页面作为辅助页面下载，下载完后关闭
         * （2）可通过全局参数来 控制 新打开的页面应显示那些DOM
         * （3）数据模型最大分页显示数量，暂时不支持表达式动态修改，采用：onQueryData()更改数据元数据
         *
         * ctx：获取系统的上下文路径
         *
         * 注意点：
         * （1）异常：'没有字体：XXX'，通过DOM找到对应对象，改为：宋体 或 微软雅黑（ttf）
         * （2）当前版本导出的PDF仅导出当前【看的见】的数据（eg：当前页100条，当前显示：10条，导出：10条）
         *      解决帖子：https://jira.succez.com/browse/CSTM-17293
         */
        showWaitingIcon({
            // target: event.renderer.domBase,
            target: document.body,
            promise: exportPdf(),
            // promise: url2pdf(
            //     fileInfo.id,
            //     ctx(`${fileInfo.path}?pageUniqueCode=${pageUniqueCode}&isDisplayTopDom=${isDisplayTopDom}&isImportOpenPage=${isImportOpenPage}`),
            //     `${importPdfName}`,
            //     ExportReusltType.DOWNLOAD,
            //     exportSizeOptionsInfo
            // ),
            delay: 200,
            layoutTheme: 'waiting-icon-default',
            throwOnError: true
        });
    }
}

export const CustomJS: { [type: string]: IMetaFileCustomJS } = {
    spg: {
		 onQueryData: (page: IVPage, query: QueryInfo): Promise<QueryDataResultInfo> => {
            let urlParams = page.getUrlParams();
            if (urlParams && urlParams.isImportOpenPage == 'true') {
				/**
				 * 设置【导出的模型】当前页显示的数量
				 */
                page.getDataset("model3").setPageSize(10000);
            }
            return;
        }
	}
}
