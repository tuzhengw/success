/**
 * ============================================================
 * 作者:tuzhengw
 * 审核人员：liuyz
 * 创建日期：2021-12-21
 * 功能描述：
 *      该脚本主要是用于【请求第三方接口，落地数据到库】
 * ============================================================
 */
import { getDataSource } from "svr-api/db"; //数据库相关API
import { EventOperationState, ServerEvent, fireEvent } from "svr-api/sys";
import { post, request } from "svr-api/http";
import { requestData, getToken, getMaxTime, checkTableIsContainsData } from "/sdi/script/requestExternalInterface.action";

/**
* 脚本返回类型，必须定义，后台会根据返回结果做不同处理
* 1. Table，在物理表基础上再做加工
* 2. Sql，在sql基础上再做加工
* 3. Stream，流式加工数据
* 4. None，无结果
*/
function getReturnType(): ScriptNodeDataType {
    return ScriptNodeDataType.Stream;
}

/** 输出字段【ODS_ZSCQ_ZLSQ：【专利授权】】 */
const outputFields = [
    { name: "专利权人名称", dbfield: "ZLQRMC", dataType: FieldDataType.C, length: 4000 },
    { name: "专利权人名称（新）", dbfield: "ZLQRMC_NEW", dataType: FieldDataType.C, length: 500 },
    { name: "专利权人名称（英文）", dbfield: "ZLQRMC_YW", dataType: FieldDataType.C, length: 500 },
    { name: "专利权人地址", dbfield: "ZLRQDZ", dataType: FieldDataType.C, length: 4000 },
    { name: "专利权人类型", dbfield: "ZLQRLX", dataType: FieldDataType.C, length: 50 },
    { name: "专利权人邮编", dbfield: "ZLQRYB", dataType: FieldDataType.C, length: 50 },
    { name: "专利类型", dbfield: "ZLLX", dataType: FieldDataType.C, length: 50 },
    { name: "主分类号", dbfield: "ZFLH", dataType: FieldDataType.C, length: 4000 },
    { name: "主键", dbfield: "ID", dataType: FieldDataType.C, length: 50 },
    { name: "代理机构代码", dbfield: "AGENCYCODE", dataType: FieldDataType.C, length: 500 },
    { name: "代理机构代码(旧)", dbfield: "DLJGDM", dataType: FieldDataType.C, length: 50 },
    { name: "代理机构名称", dbfield: "DLJGMC", dataType: FieldDataType.C, length: 3000 },
    { name: "企业类型", dbfield: "QYLX", dataType: FieldDataType.C, length: 100 },
    { name: "企业规模", dbfield: "QYGM", dataType: FieldDataType.C, length: 10 },
    { name: "内部序号", dbfield: "NBXH", dataType: FieldDataType.C, length: 100 },
    { name: "发明名称", dbfield: "FMMC", dataType: FieldDataType.C, length: 3000 },
    { name: "城市名称", dbfield: "CSMC", dataType: FieldDataType.C, length: 50 },
    { name: "导入月份", dbfield: "INPUTCODE", dataType: FieldDataType.C, length: 100 },
    { name: "所属区县（旧）", dbfield: "BELONGAREA", dataType: FieldDataType.C, length: 100 },
    { name: "所属市（旧）", dbfield: "BELONGCITY", dataType: FieldDataType.C, length: 50 },
    { name: "所属省（旧）", dbfield: "BELONGPROVINCE", dataType: FieldDataType.C, length: 50 },
    { name: "所属镇", dbfield: "BELONGTOWN", dataType: FieldDataType.C, length: 50 },
    { name: "授权入库日", dbfield: "SQRKR_CHAR", dataType: FieldDataType.C, length: 50 },
    { name: "授权入库日日期型", dbfield: "SQRKR", dataType: FieldDataType.P },
    { name: "是否战略企业", dbfield: "SFZLQY", dataType: FieldDataType.C, length: 10 },
    { name: "是否规上企业", dbfield: "SFGSQY", dataType: FieldDataType.C, length: 30 },
    { name: "是否高新企业", dbfield: "SFGXQY", dataType: FieldDataType.C, length: 30 },
    { name: "是否龙头企业", dbfield: "SFLTQY", dataType: FieldDataType.C, length: 20 },
    { name: "申请号", dbfield: "SQH", dataType: FieldDataType.C, length: 50 },
    { name: "申请日", dbfield: "SQR_CHAR", dataType: FieldDataType.C, length: 50 },
    { name: "申请日日期型", dbfield: "SQR", dataType: FieldDataType.P },
    { name: "省份名称", dbfield: "SFMC", dataType: FieldDataType.C, length: 50 },
    { name: "系统划分区县", dbfield: "XIAN", dataType: FieldDataType.C, length: 100 },
    { name: "系统划分城市", dbfield: "SHI", dataType: FieldDataType.C, length: 100 },
    { name: "系统划分省份", dbfield: "SHENG", dataType: FieldDataType.C, length: 100 },
    { name: "经济户口类别", dbfield: "JJHKLB", dataType: FieldDataType.C, length: 10 },
    { name: "经营状态", dbfield: "JYZT", dataType: FieldDataType.C, length: 50 },
    { name: "行政区划", dbfield: "SS_XZQH", dataType: FieldDataType.C, length: 25 }
]

/**
 * 返回脚本节点字段结构
 */
function onProcessFields(context: IDataFlowScriptNodeContext): DbFieldInfo[] {
    return outputFields;
}

/** 表类型 */
const requestTyle = "专利授权";
/** 数据源名 */
const dataBase: string = "sjzt_ods";
/** 脚本提取的目标表 */
const targetTableName: string = "ODS_ZSCQ_ZLSQ";
/**
 * 返回脚本节点的数据结构
 * @return 返回二维数组
 * 参考文档：https://docs.succbi.com/data-process/script/
 */
function onProcessData(context: IDataFlowScriptNodeContext): DbTableInfo | any[][] {
    let rows: Array<Array<string>> = [];
	
	let isPreview = context.isPreview; // 是否为预览数据
    if (isPreview) {
        print(`--【专利授权】脚本，当前状态【预览】，不执行脚本`);
        return rows;
    }
    let token: string = "";
    let requestTokenResult = getToken();
    if (!requestTokenResult.success) {
        console.debug(`--【ODS_ZSCQ_ZLSQ】 获取凭证token失败--`);
        throw Error(`--【ODS_ZSCQ_ZLSQ】 获取凭证token失败--`);
        return rows;
    }
    token = requestTokenResult.access_token;

    let isContainData: boolean = checkTableIsContainsData(dataBase, targetTableName);
    if (isContainData) { // true：没有数据，全量加载
        console.debug(`--【${targetTableName}】表没有数据，首次加载，请求所有【南通数据中台】的数据--start`);
        let filter = [{
            "SS_XZQH": { "val": "3206", "oper": "startsWith" }
        }];
        rows = requestData(requestTyle, targetTableName, token, filter);
        console.debug(`--【${targetTableName}】初始化【南通数据中台】的数据完成--end`);
    } else {
        let lastRequestMaxTime: string = getMaxTime(dataBase, targetTableName, "INPUTCODE");
        console.debug(`--【${targetTableName}】表增量请求，过滤条件：根据上次请求的【导入月份】最大值【${lastRequestMaxTime}】-至今，来过滤数据--start`);
        let filter = [{
            "INPUTCODE": { "val": `${lastRequestMaxTime}`, "oper": "greaterThan" }
        }, {
            "SS_XZQH": { "val": "3206", "oper": "startsWith" }
        }];
        rows = requestData(requestTyle, targetTableName, token, filter);
        console.debug(`--【${targetTableName}】表增量请求--end`);
    }
    return rows;
}