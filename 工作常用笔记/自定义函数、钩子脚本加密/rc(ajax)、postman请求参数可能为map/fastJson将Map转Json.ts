/**
 * 请求参数通过java处理后，params类型为：class java.util.LinkedHashMap
 * 引入java的：fastJson，将其map转换为：jsonString
 */
const FastJson = Java.type("com.alibaba.fastjson.JSONObject");
let jsonString = new FastJson(params);
let jsonObject = JSON.prase(jsonString);


/**
 * 获取请求中的参数
 *
 * 注意：解析参数时需要注意解析的对象类型（除wabapi.action.ts），后端脚本处理外部参数需注意：
 * 外层类型为：class java.util.LinkedHashMap，可：get获取
 * 内层若存在数组：class java.util.List，可通过：[... Array] 转换为前端可识别的数组
 * 引入fastJson将其Map转换为：JSONObject也可
 * @param request
 */
function getParams(request: HttpServletRequest): unknown {
    /** 
     * 外部传递参数类型：class java.util.LinkedHashMap
     * 获取参数方式：params.get("key");
     * webapi内部做了调整，可通过：params["key"]方式获取value
     */
    let params: unknown = request.getRequestBody();
    console.debug(`--获取请求参数：${params}`);
    if (!params || params == "" || params == null) {
        return null;
    } else {
        if (typeof params === "string") {
            console.debug(`--是string：`);
            return JSON.parse(params);
        } else {
            return params;
        }
    }
}