# Excel常见函数

1 ISERROR

```
ISERROR(公式)  如果是#VALUE，返回true
```

2 获取首次空格前后内容（注意更改单元格）

```
excel 替换 空格字符后面的所有字符

=TRIM(REPLACE(A1,FIND(" ",A1),999,))

=TRIM(REPLACE(A1,1,FIND(" ",A1),))  // 获取第一个空格后的所有内容

=TRIM(MID(A1,FIND(" ",A1),99))

=MID(A1,FIND("*",SUBSTITUTE(A1," ","*")),99) // 获取第一个空格前的所有内容
```