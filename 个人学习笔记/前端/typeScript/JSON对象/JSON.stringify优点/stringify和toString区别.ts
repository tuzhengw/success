/**
 *  JSON.stringify()与toString()这两者虽然都可以将目标值转为字符串，但本质上还是有区别的
 */
function main() {
    let arr = [1,2,3];
    //'[1,2,3]'
    console.log(JSON.stringify(arr));
    //1,2,3
    console.log(arr.toString());
}