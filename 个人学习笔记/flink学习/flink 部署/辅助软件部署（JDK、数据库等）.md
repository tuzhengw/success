---
typora-root-url: images
---

# Flink部署笔记

# 一 安装linux操作系统

## 1 安装

​	使用vmWare软件安装虚拟机，镜像采用: CentOS-7-x86_64-DVD-2009.iso，CentOS7端口号: 22。

## 2 编辑文件

```cmd
通过 vim 命令进行编辑(vim后面有个空格)
[admin@localhost ~]$ vim /etc/sysconfig/network-scripts/ifcfg-ens33 

按: i 进入编辑

按: esc 退出编辑

退出vim命名
1) 输入:
2) q(退出)、wq(保存退出)、q!(不保存退出)

$ 定位最后一行
gg 文件首行
```

## 3 关闭虚拟机

​	不要直接点击上方的关闭按钮，应直接关闭虚拟机。

![](/关闭虚拟机.png)

## 4 Linux常见问题

### 1) sudo 命令

Linux sudo命令以系统管理者的身份执行指令，也就是说，经由 sudo 所执行的指令就好像是 root 亲自执行。

使用权限：在 /etc/sudoers 中有出现的使用者。

**注意**:  可不使用sudo命令

```cmd
$ sudo ls
[sudo] password for hnlinux: 
hnlinux is not in the sudoers file. This incident will be reported.
```

### 2) 用户无操作权限-进入超级用户模式

​	admin is not in the sudoers file.  This incident will be reported .

 	参考地址：https://www.cnblogs.com/dingjiaoyang/p/7268739.html

```cmd
[root@node2 opt]# rpm -e mariadb-libs-5.5mariadb-libs-5.5.68-1.el7.x86_64 // 例如删除指定文件, 无权限
error: package mariadb-libs-5.5.68-1.el7.x86_6 is not installed
admin is not in the sudoers file.  This incident will be reported 

[admin@localhost ~]$ su  // 切换到root用户下(一般只需要输入此命令即可)
Password:   // 输入超级用户密码, 输入密码后就进入了超级用户模式


[root@localhost admin]# rpm -qa|grep mariadb
mariadb-libs-5.5.68-1.el7.x86_64
[root@localhost admin]# rpm -e --nodeps mariadb-libs-5.5.68-1.el7.x86_64
[root@localhost admin]# rpm -qa|grep mariadb
[root@localhost admin]# 
```





## 5 yum命令常见问题

### 1)  fastestmirror问题

​	 无权限问题,添加su root权限,即可

```cmd
[lss@localhost ~]$ yum install -y libtool
Loaded plugins: fastestmirror, langpacks
You need to be root to perform this command.
```

解决办法: 

```cmd
[lss@localhost ~]$ su root
Password: 
[root@localhost lss]# 
```

### 2)  yum在锁定状态

```cmd
Another app is currently holding the yum lock; waiting for it to exit…（ 另一个应用程序目前持有yum锁;等待它退出…）
The other application is: yum
Memory : 21 M RSS (313 MB VSZ)
Started: Sun May 19 15:10:43 2019 - 07:14 ago
State : Traced/Stopped, pid: 1229
```

**原因**：上一次执行yu命令被强制中断。

**解决办法**：

使用kill命令杀掉

```cmd
[admin@localhost ~]$ ps aux | grep yum  # 查询有关yum的进程，找到那个更新进程
root       2609  2.2 13.2 716452 132260 ?       SN   22:35   0:06 /usr/bin/python /usr/share/PackageKit/helpers/yum/yumBackend.py get-updates none
root       2900  0.0  0.0 112808   960 pts/0    R+   22:40   0:00 grep --color=auto yum

[admin@localhost ~]$ kill -s 2609 pid # 上一步找到pid后直接杀掉
```

强制关掉yum的进程

```cmd
[admin@localhost ~]$ rm -f /var/run/yum.pid
```

## 6 防火墙

​	参考地址: https://blog.csdn.net/istrangeboy/article/details/112747366

### 1) centos7之前版本

​	centos7**之前的版本**可以通过iptables相关命令实现防火墙的打开和关闭

```cmd
[root@localhost /]# sudo yum install iptables services  # 安装

Loaded plugins: fastestmirror, langpacks
Loading mirror speeds from cached hostfile
 * base: mirrors.ustc.edu.cn
 * extras: mirrors.ustc.edu.cn
 * updates: mirrors.ustc.edu.cn
base                                                                      | 3.6 kB  00:00:00     
extras                                                                    | 2.9 kB  00:00:00     
updates                                                                   | 2.9 kB  00:00:00     
Package iptables-1.4.21-35.el7.x86_64 already installed and latest version
No package services available.
Nothing to do
```

### 2 centos7以及之后版本

​	centos7使用firewall命令来开启和关闭防火墙

​	systemctl命令

（1）systemctl  status firewalld.service 查看防火墙的状态；

（2）systemctl  start firewalld.service 启动防火墙；

（3）systemctl  stop firewalld.service 关闭防火墙；

（4）systemctl  restart firewalld.service 重启防火墙；

（5）systemctl  enable firewalld.service 开机启动防火墙；

（6）systemctl  disable firewalld.service 开机禁用防火墙；

（7）systemctl  is-enabled firewalld.service 查看防火墙是否开机启动

​	示例:  

```cmd
[root@localhost bin]# systemctl status firewalld.service  

// active (running) 激活状态

● firewalld.service - firewalld - dynamic firewall daemon
   Loaded: loaded (/usr/lib/systemd/system/firewalld.service; enabled; vendor preset: enabled)
   Active: active (running) since Sun 2023-04-09 22:33:41 PDT; 6h ago
     Docs: man:firewalld(1)
 Main PID: 670 (firewalld)
    Tasks: 2
   Memory: 1.5M
   CGroup: /system.slice/firewalld.service
           └─670 /usr/bin/python2 -Es /usr/sbin/firewalld --nofork --nopid
           .....
```

## 7 常见命令

### 1 删除文件

```cmd
[root@localhost /]$ rm -rf /usr/home/zipFile  # 删除文件夹以及内部所有子文件
```

### 2 查看进程

```cmd
[root@localhost /]$ ps -ef | grep mysql
```

### 3 查看端口

```cmd
[admin@localhost ~]$ netstat -anptl | grep LISTEN

tcp        0      0 0.0.0.0:3306            0.0.0.0:*               LISTEN      -                   
tcp        0      0 0.0.0.0:111             0.0.0.0:*               LISTEN      -                   
tcp        0      0 192.168.122.1:53        0.0.0.0:*               LISTEN      -                   
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      -                   
tcp        0      0 127.0.0.1:631           0.0.0.0:*               LISTEN      -                   
tcp6       0      0 :::111                  :::*                    LISTEN      -                   
tcp6       0      0 :::22                   :::*                    LISTEN      -                   
tcp6       0      0 ::1:631                 :::*                    LISTEN      -    
```

## 8 查看日志

```cmd
[root@localhost /]$ tail -100 /var/log/mysqld3307.log  // 查询最后100行日志信息
```

## 9 查看磁盘存储情况

```cmd
[admin@localhost ~]$  df -h
Filesystem      Size  Used Avail Use% Mounted on
devtmpfs        895M     0  895M   0% /dev
tmpfs           910M     0  910M   0% /dev/shm
tmpfs           910M   11M  900M   2% /run
tmpfs           910M     0  910M   0% /sys/fs/cgroup
/dev/sda3        18G   13G  5.3G  71% /
/dev/sda1       297M  163M  134M  55% /boot
tmpfs           182M   12K  182M   1% /run/user/42
tmpfs           182M     0  182M   0% /run/user/1000
tmpfs           182M     0  182M   0% /run/user/0
```

 	–max-depth=<目录层数> 超过指定层数的目录后，予以忽略。

```cmd
[root@localhost /]# du -h --max-depth=1 # 查看当前目录下个文件占用大小
0       ./proc
11M     ./run
0       ./sys
43M     ./etc
80K     ./root
5.4G    ./var
8.0K    ./tmp
13G     ./usr
4.3M    ./home
0       ./media
0       ./mnt
0       ./opt
0       ./srv
432M    ./docker
```

```cmd
[root@localhost install]# du -h --max-depth=1
289M    ./jdk-11
5.4G    ./mysql
374M    ./flink-1.13.6
6.1G    .
```



# 二 electerm连接虚拟机

参考网址：https://blog.csdn.net/weixin_44480609/article/details/125261348

## 1 确保虚拟机拥有ssh服务

```cmd
[admin@localhost ~]$ which ssh
/usr/bin/ssh  # 返回路径则表示虚拟机存在ssh服务
```

安装ssh

```cmd
[admin@localhost ~]$ yum install -y openssh-server
```

## 2 开启网关

​	1 若网关未开启,  无法查询到当前电脑的IP地址。

​	参考地址：https://blog.csdn.net/qq_42455262/article/details/125117414

## 3 查看IP地址

​	centos6 命令: ifconfig

​	centos7 命令: ip addr 

​	**注意:** 此说法不一定正确，版本7使用ifconfig命令也可以查看, 虚拟机IP地址对外连接地址不是127.0.0.1, 上述两个命令都输入看看。

![](/查看虚拟机IP地址.png)

## 4 electerm软件下载

​	公司wiki: https://wiki.succez.com/pages/viewpage.action?pageId=260769453

​	可根据自己的需求下载对应的工具

​	electerm，仿xshell，跨平台（macos，win，linux），下载地址：https://electerm.github.io/electerm/

​	filezilla，下载地址：https://filezilla-project.org/download.php

![](/ssh和文件传输工具.png)

## 5 设置连接信息

​		electerm软件下载后，可连接指定虚拟机主机，输入连接信息后，点击下方的测试连接，若连接成功可保存此连接。

​		注意:  

​			先配置网卡配置IP才行，不配置就只能通过VMware控制台连接。

​			若不再使用electerm, 直接关闭虚拟机就行。

![](/连接虚拟机测试.png)

# 三 安装docker

参考地址：https://cloud.tencent.com/developer/article/2061665

手动安装Docker分三步：卸载、设置仓库、安装。

## 1 卸载Docker（可选）

**第一步，卸载历史版本**。这一步是可选的，如果之前安装过旧版本的Docker，可以使用如下命令进行卸载：

```javascript
yum remove docker \
                  docker-client \
                  docker-client-latest \
                  docker-common \
                  docker-latest \
                  docker-latest-logrotate \
                  docker-logrotate \
                  docker-selinux \
                  docker-engine-selinux \
                  docker-engine \
                  docker-ce
```

## 2 设置源仓库

​		新主机上首次安装Docker Engine-Community之前，需要设置Docker仓库。此后可从仓库安装和更新Docker。

​		在设置仓库之前，需先按照所需的软件包。yum-utils提供了yum-config-manager，并且device mapper存储驱动程序需要device-mapper-persistent-data和lvm2。

```cmd
[root@localhost admin]# yum install -y yum-utils   device-mapper-persistent-data   lvm2
```

​		执行上述命令，安装完毕即可进行仓库的设置。使用官方源地址设置命令如下：

```cmd
[root@localhost admin]$ yum-config-manager     --add-repo     https://download.docker.com/linux/centos/docker-ce.repo

Loaded plugins: fastestmirror, langpacks
adding repo from: https://mirrors.tuna.tsinghua.edu.cn/docker-ce/linux/centos/docker-ce.repo
grabbing file https://mirrors.tuna.tsinghua.edu.cn/docker-ce/linux/centos/docker-ce.repo to /etc/yum.repos.d/docker-ce.repo
repo saved to /etc/yum.repos.d/docker-ce.repo
```

通常，官方的源地址比较慢，可将上述的源地址替换为国内比较快的地址：

- 清华大学源：https:**//**mirrors.tuna.tsinghua.edu.cn**/**docker-ce**/**linux**/**centos**/**docker-ce.repo

仓库设置完毕，即可进行Docker的安装。

## 3 Docker安装

执行一下命令，安装最新版本的 Docker Engine-Community 和 containerd。

```cmd
[root@localhost admin]$ yum install -y docker-ce docker-ce-cli containerd.io

Loaded plugins: fastestmirror, langpacks
Loading mirror speeds from cached hostfile
 * base: mirrors.ustc.edu.cn
 * extras: mirrors.ustc.edu.cn
 * updates: mirrors.ustc.edu.cn
docker-ce-stable                                                          | 3.5 kB  00:00:00     
......
Dependency Installed:
  container-selinux.noarch 2:2.119.2-1.911c772.el7_8 docker-buildx-plugin.x86_64 0:0.10.4-1.el7 
  docker-ce-rootless-extras.x86_64 0:23.0.3-1.el7    docker-compose-plugin.x86_64 0:2.17.2-1.el7
  fuse-overlayfs.x86_64 0:0.7.2-6.el7_8              fuse3-libs.x86_64 0:3.6.1-4.el7            
  slirp4netns.x86_64 0:0.4.3-4.el7_8                

Complete!
```

​		docker-ce为社区免费版本。稍等片刻，docker即可安装成功。但安装完成之后的默认是：未启动的，需要进行启动操作。

​		如果不需要docker-ce-cli或containerd.io可直接执行如下命令：

```cmd
[root@localhost admin]$ yum install -y docker-ce
```

至此，完成Docker安装。

## 4 Docker启动

**启动Docker的命令**：

```cmd
[root@localhost admin]$ sudo systemctl start docker
```

通过运行hello-world镜像来验证是否正确安装了Docker Engine-Community。

```cmd
[root@localhost admin]$ sudo docker pull hello-world # 拉取镜像

Using default tag: latest
...
```

```cmd
[root@localhost admin]$ sudo docker run hello-world # 执行hello-world

Hello from Docker!
This message shows that your installation appears to be working correctly.
...
```

如果执行之后，控制台显示如下信息，则说明Docker安装和启动成功：

```cmd
[root@localhost admin]$ docker run hello-world

Hello from Docker!
This message shows that your installation appears to be working correctly.
……
```

除了启动Docker，一些其他启动相关的命令：

- **守护进程重启**

```cmd
[root@localhost admin]$ sudo systemctl daemon-reload
```

- **重启Docker服务**

```cmd
[root@localhost admin]$ sudo systemctl restart docker 
或者
[root@localhost admin]$ sudo service docker restart
```



## 5 关闭Docker服务

```cmd
[root@localhost admin]$ sudo docker service docker stop
或者
[root@localhost admin]$ sudo docker systemctl stop docker

Warning: Stopping docker.service, but it can still be activated by:
  docker.socket
```

**警告原因**:  

​		除了[docker](https://so.csdn.net/so/search?q=docker&spm=1001.2101.3001.7020).service单元文件，还有一个docker.socket单元文件…docker.socket这是用于套接字激活。
​		该警告意味着：如果你试图连接到docker [socket](https://so.csdn.net/so/search?q=socket&spm=1001.2101.3001.7020)，而docker服务没有运行，系统将自动启动docker。

​		解决方案一
​			你可以删除 /lib/systemd/system/docker.socket
​			从docker中 docker.service 文件 删除 fd://，即remove -H fd://

​		解决方案二
​			如果不想被访问时自动启动服务
​			输入命令：sudo systemctl stop docker.socket

**注意：可忽视警告, 不做干预**



## 6 删除Docker

删除安装包：

```cmd
yum remove docker-ce
```

删除镜像、[容器](https://cloud.tencent.com/product/tke?from=20065&from_column=20065)、配置文件等内容：

```cmd
rm -rf /var/lib/docker
```

## 7 常见命令

安装完成Docker之后，这里汇总列一下常见的Docker操作命令：

- 搜索仓库镜像：docker search 镜像名
- 拉取镜像：docker pull 镜像名
- 查看正在运行的容器：docker ps
- 查看所有容器：docker ps -a
- 删除容器：docker rm container_id
- 查看镜像：docker images
- 删除镜像：docker rmi image_id
- 启动（停止的）容器：docker start 容器ID
- 停止容器：docker stop 容器ID
- 重启容器：docker restart 容器ID
- 启动（新）容器：docker run -it ubuntu /bin/bash
- 进入容器：`docker attach 容器ID`或`docker exec -it 容器ID /bin/bash`，推荐使用后者。

更多的命令可以通过`docker help`命令来查看。

https://blog.csdn.net/weixin_45965089/article/details/122104677

## 8 docker部署MySQL8.0.20

​	参考：https://juejin.cn/post/7031188920511660063

### 8.1 拉取MySQLB

```cmd
[root@localhost /]# sudo docker pull mysql:8.0.23
```

### 8.2 检查MySQL镜像

```cmd
[root@localhost /]# sudo docker images
```

![](/查看镜像文件.png)

### 8.3 启动运行MySQL镜像

docker run的车参数中 -v是挂载
-v 服务器本机路径:容器内部路径



​	需要指定MySQL时区, 否则默认美国时区。

<!-- docker run 用于启动一个容器 -->

```cmd
[root@localhost /]# sudo docker run -p 3306:3306 --name mysql \
-v /usr/local/docker/mysql/mysql-files:/var/lib/mysql-files \
-v /usr/local/docker/mysql/conf:/etc/mysql \
-v /usr/local/docker/mysql/logs:/var/log/mysql \
-v /usr/local/docker/mysql/data:/var/lib/mysql \
-e TZ=Asia/Shanghai  \
-e MYSQL_ROOT_PASSWORD=root  \
-d mysql:8.0.20 \
--default-time_zone='+8:00' 
```

​		假如安装过程中失败了，则可通过docker ps -a 查看以前的容器，假如已存在，则通过docker rm 镜像id 删除再重新安装即可，若已经部署过，删除容器后重新部署，依然保留原本的数据库设置和数据。



### 8.4 启动MySQL服务

​		若没有自动启动，也可以通过此命令启动

```mysql
[root@localhost /]# su
[root@localhost /]# docker start mysql
```

​		假如希望查看mysql启动时的日志，可以执行 docker container logs mysql 这个指令。

### 8.5 停止MySQL服务

​		执行完第3个步骤（启动运行mysql镜像），mysql就会自动启动了，假如需要停止这个

```cmd
[root@localhost /]# docker stop mysql
```

### 8.6 查看docker启动的服务

```cmd
[root@localhost /]# docker ps
```

![](/查看docker启动程序.png)

### 8.7 进入docker容器 (退出exit)

​		进入容器后才能访问容器内部署的程序。

```cmd
[root@localhost /]# sudo docker exec -it ${docker数据库线程号} bash
```

### 8.8 进入MySQL容器

​		默认密码：root，输入exit; 退出

```cmd
[root@localhost local]# docker exec -it mysql bash
root@6951b7340adf:/# mysql -uroot -proot
```

![](/进入MySQL容器.png)

### 8.9 设置MySQL开机自启动

​		此步可选

```CMD
[root@localhost local]# docker update mysql --restart=always
```

### 8.10 查看MySQL安装路径

```cmd
[root@localhost /]# whereis mysql // 查看MySQL安装路径
mysql: /usr/lib64/mysql
```

### 8.11 Navacat连接MySQL

​		若连接遇到：1251-client does **not** support authentication protocol requesred by server;consider upgrading MySQL Client异常，解决办法：

**1）进入MySQL容器**

```cmd
[root@localhost local]# docker exec -it mysql bash
root@6951b7340adf:/# mysql -uroot -proot
```

**2）查看用户信息**

```mysql
mysql> select host,user,plugin,authentication_string from mysql.user; 
```

![](/查看mysql访问路径.png)

​		备注：host为 % 表示不限制ip localhost表示本机使用 plugin非mysql_native_password 则需要修改密码

**3）修改用户密码**

```mysql
mysql> ALTER USER 'root'@'%' IDENTIFIED WITH mysql_native_password BY '123456'; 
```

**4）更新user为root，host为% 的密码为123456**

```mysql
mysql> LTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY '123456'; 
```

**5）再次连接即可成功**

## 9 docker部署Flink1.13.6

### 9.1 拉取Flink镜像

```cmd
[root@localhost admin]# docker pull flink:1.13.6-scala_2.11 # (容器名: 版本号)
```

### 9.2 检查Flink镜像

```cmd
[root@localhost admin]# docker images
```

### 9.3 安装docker-compose

​		**解决方法一：使用pip安装**

```cmd
# 第一个, 更新yum源
[root@localhost /]# sudo yum -y install epel-release
# 然后安装pip
[root@localhost /]# sudo yum -y install python-pip
# 使用pip安装docker-compose
...
```

​		**解决方法二：更新yum源(获取系统支持且最新的epel-release)**

```cmd
[root@localhost /]# sudo yum -y install epel-release
```

![](/更新yum命令.png)

```cmd
# 然后可以直接安装
[root@localhost /]# yum install docker-compose
```

![](/安装docker-compose.png)

```cmd
# 查看版本
[root@localhost /]# docker-compose -version
docker-compose version 1.18.0, build 8dd22a9
```

### 9.4 查看远程flink文件目录

​		docker都是使用别人已经部署好的环境，无法对其目录和文件进行修改，初始化前需要对读写文件目录进行挂载到本地服务器。

```cmd
[root@localhost docker]# docker ps
```

![](/查看docker任务进程号.png)

​	exit退出

```cmd
[root@localhost docker]# docker exec -it 6eb8a488e5be /bin/bash
root@6eb8a488e5be:/opt/flink# 
```

### 9.5 创建文件夹, 编辑yml文件

```cmd
[root@localhost /]# cd /usr/local/docker
[root@localhost docker]# mkdir -p flink
[root@localhost docker]# vim flink/docker-compose.yml
```

​	**docker-compose.yml内容**

```yml
version: "2.1"
services:
  jobmanager:
    image: flink:1.13.6-scala_2.11 # # (容器名: 版本号)
    expose:
      - "6123"
    ports:
      - "8081:8081"  # 确保端口号没有被其他占用
    command: jobmanager
    environment:
      - JOB_MANAGER_RPC_ADDRESS=jobmanager
    volumes: 
      # 将flink服务器的文件挂载到本地服务器, 格式: 本地地址:远程地址:读写方式
      # - /usr/local/docker/flink/flinkdir:/opt/flink:ro
      # 此处挂载有点问题, 暂时注释掉, 若需要挂载需要考虑其他方式
  taskmanager:
    image: flink:1.13.6-scala_2.11
    expose:
      - "6121"
      - "6122"
    depends_on:
      - jobmanager
    command: taskmanager
    links:
      - "jobmanager:jobmanager"
    environment:
      - JOB_MANAGER_RPC_ADDRESS=jobmanager
```



### 9.6 创建、运行flink容器

```cmd
[root@localhost docker]# cd flink
[root@localhost flink]# docker-compose up -d # 在后台启动并运行所有的容器
```

```
Creating network "flink_default" with the default driver
Pulling jobmanager (flink:latest)...
latest: Pulling from library/flink
74ac377868f8: Pull complete
b9cabe75b440: Pull complete
9f3fc5460b4b: Pull complete
a7a805490095: Pull complete
33a8846a9f32: Pull complete
e1244e0de82a: Pull complete
4c60df233834: Pull complete
d3d64557e6f2: Pull complete
6403a7b6e78d: Pull complete
63b1a03a60e6: Pull complete
Digest: sha256:92f54911fe0cca9bcc140927db41494701033c42063b8ebd515a1664722e7e52
Creating flink_jobmanager_1  ... done
Creating flink_jobmanager_1  ... 
Creating flink_taskmanager_1 ... done
```

### 9.7 docker-compose up -d 介绍

 		将会在后台启动并运行所有的容器。一般推荐生产环境下使用该选项。

​		默认情况，如果服务容器已经存在， docker-compose up 将会尝试停止容器，然后重新创建（保持使用 volumes-from 挂载的卷） ，以保证新启动的服务匹配 docker-compose.yml 文件的最新内容。如果用户不希望容器被停止并重新创建，可以使用 docker-compose up --norecreate 。这样将只会启动处于停止状态的容器，而忽略已经运行的服务。如果用户只想重新部署某个服务，可以使用 docker-compose up --no-deps -d <SERVICE_NAME> 来重新创建服务并后台停止旧服务，启动新服务，并不会影响到其所依赖的服务。		选项：

​			-d 在后台运行服务容器。

​			--no-color 不使用颜色来区分不同的服务的控制台输出。
​			--no-deps 不启动服务所链接的容器。
​			--force-recreate 强制重新创建容器，不能与 --no-recreate 同时使用。
​			--no-recreate 如果容器已经存在了，则不重新创建，不能与 --force-recreate 同时使用。

### 9.8 关闭flink

![](/关闭flink1.png)

```cmd
[root@localhost flink]# docker stop flink_taskmanager_1 flink_jobmanager_1
```

### 9.9 常见问题

1）启动后，slot为0，检查docker-compose.yml文件内容是否多加了符号, eg: jobmanager 写成: jobmanagers。

## 10 部署SuccBI

​	公司部署文档：https://docs.succbi.com/ops/install/docker/#install-docker

### 10.1 拉取镜像

```cmd
[root@localhost admin]# docker pull succbi/succbi:latest
[root@localhost admin]# docker images | grep succbi  # 查看拉取情况
```

### 10.2 创建挂载目录

​	SuccBI在运行过程中，会在[工作目录](https://docs.succbi.com/devops/install/workdir-and-defdb)中存放配置文件、上传文件等，而Docker容器重启后，会清空容器中的所有数据，因此需要将这些文件从宿主机挂载到容器中，挂载后容器对目录的修改会同步到宿主机中，保证了容器重启后，工作目录仍可以正常保留。

​	**挂载目录会替换容器内的目录**

### 10.3 启动容器

docker run -it -d --name SuccBI -p 8080(外部端口):8080(容器内端口) \

```cmd
[root@localhost admin]# docker run -it -d --name SuccBI -p 8080:8080 \
-v /docker/SuccBI/clusters-share:/opt/workdir/clusters-share \
-v /docker/SuccBI/conf:/opt/workdir/conf \
succbi/succbi:latest
```

### 10.4 进入容器

```cmd
[root@localhost admin]# docker ps  # 查看线程信息
[root@localhost admin]# docker exec -it ${线程编码} /bin/bash
```

​	示例：

```cmd
[root@localhost admin]# docker exec -it 437de05a08bc  /bin/bash
[root@437de05a08bc /]# 
```

### 10.4 查看容器内War包目录

```cmd
[root@e146ff20d56d /]# ps -ef | grep tomcat

root         17      1 20 18:29 pts/0    00:00:29 /usr/local/jdk/bin/java -Djava.util.logging.config.file=/usr/local/tomcat/conf/logging.properties -Djava.util.logging.manager=org.apache.juli.ClassLoaderLogManager -Djdk.tls.ephemeralDHKeySize=2048 -Djava.protocol.handler.pkgs=org.apache.catalina.webresources -Dorg.apache.catalina.security.SecurityListener.UMASK=0027 -Djava.awt.headless=true -Dsucc.workdir=/opt/workdir -Djava.net.preferIPv4Stack=true -Dfile.encoding=UTF-8 -Dsun.jnu.encoding=UTF-8 -DLANG=zh_CN.UTF-8 -server -Xmx1g -Xms1g -XX:+UseG1GC -XX:+AlwaysPreTouch -XX:+ScavengeBeforeFullGC -XX:+UnlockDiagnosticVMOptions -XX:MetaspaceSize=256m -XX:MaxMetaspaceSize=1024m -XX:MaxDirectMemorySize=2048m -Dsun.nio.MaxDirectMemorySize=2048m -XX:NativeMemoryTracking=detail -Dorg.apache.tomcat.util.buf.UDecoder.ALLOW_ENCODED_SLASH=true -Dorg.apache.catalina.connector.CoyoteAdapter.ALLOW_BACKSLASH=true -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/opt/workdir/ -Dignore.endorsed.dirs= -classpath /usr/local/tomcat/bin/bootstrap.jar:/usr/local/tomcat/bin/tomcat-juli.jar -Dcatalina.base=/usr/local/tomcat -Dcatalina.home=/usr/local/tomcat -Djava.io.tmpdir=/usr/local/tomcat/temp org.apache.catalina.startup.Bootstrap start
```

### 10.5 退出容器

```cmd
[root@e146ff20d56d /]# exit
```

### 10.6 更改挂载war目录和重启

```cmd
[root@localhost docker]# docker stop SuccBI
[root@localhost docker]# docker rm SuccBI  # 删除容器
```

1 服务器上创建目录：/docker/SuccBI/webapps

2 目录/docker/SuccBI/webapps/ 下放置ROOT.war文件

```cmd
[root@localhost docker]# docker run -it -d --name SuccBI -p 9999:8080 \
-v /docker/SuccBI/clusters-share:/opt/workdir/clusters-share \
-v /docker/SuccBI/conf:/opt/workdir/conf \
-v /docker/SuccBI/webapps:/usr/local/tomcat/webapps \
succbi/succbi:latest
```

​	**ctrl + c 直接取消命令**

### 10.7 启动失败排查

```cmd
[root@localhost admin]# docker exec -it ${线程编码} /bin/bash # 进入容器
[root@437de05a08bc tomcat]# tail -100 /usr/local/tomcat/logs/catalina.out # 查看启动日志最新100行
```

​	示例：磁盘空间不够

​    解决办法：增加空间或者释放服务器上不需要的文件。

![](/bi启动失败.png)

### 10.8 将虚拟机BI代理到外网

​	参考地址：https://wiki.succez.com/pages/viewpage.action?pageId=140902493

#### 1 代理内网到外网

```
java -jar jTCPfwd.jar ${外网访问端口} ${虚拟机IP地址}:${内网BI端口号}
```

​	启动jar包

```cmd
PS C:\Users\86135\Desktop> java -jar jTCPfwd.jar 8081 192.168.144.129:9999
JTCPfwd 0.5

8081->192.168.144.129:9999
All forwarders started.
```

#### 2 外网访问

​	查看自己本地主机IP地址

```cmd
ipconfig
```

![](/本地服务器IP地址.png)

​	访问地址

```cmd
http://172.23.126.232:8081
```





# 四 部署JDK11

​	参考网址：https://www.cnblogs.com/xiaoyiStudent/p/12250305.html

## 1 下载JDK

（1）TUNA镜像 ： https://mirrors.tuna.tsinghua.edu.cn/AdoptOpenJDK/

（2）HUAWEI镜像 ： https://repo.huaweicloud.com/java/jdk/ 

（3）injdk ： https://www.injdk.cn/

​	注意: 不要下载open JDK11

## 2 环境说明

```cmd
# 创建存放压缩包文件文件夹(若存在则不用创建)
[root@localhost /]$ mkdir -p /usr/home/zipfile  
# 创建存放安装（解压后）文件文件夹
[root@localhost /]$ mkdir -p /usr/local/install 
```

## 3 上传JDK到指定目录下

### 1 创建存放压缩文件夹

![](/fileZilla上传文件.png)

### 2 上传JDK文件

```cmd
# Filezilla无法启动传输的问题
[root@localhost home]# sudo chmod -R 777 zipfile
[root@localhost home]# 
```

![](/传输无权限.png)

给了权限后再重新传输即可成功

## 4 解压并移动到指定目录

​	解压当前当前文件夹下

```cmd
[root@localhost /]# cd /usr/home/zipfile
[root@localhost zipfile]# ls
jdk-11_linux-x64_bin.tar.gz

[root@localhost zipfile]# tar -zxvf jdk-11_linux-x64_bin.tar.gz   
jdk-11/README.html
jdk-11/bin/jaotc
jdk-11/bin/jar
.....

查看解压后的文件
[root@localhost zipfile]# ls  
jdk-11  jdk-11_linux-x64_bin.tar.gz 
```

​	移动到指定文件夹下

```cmd
[root@localhost zipfile]# mv jdk-11 /usr/local/install/   

查看移动是否成功
[root@localhost install]# ls  
jdk-11
```

## 5 配置环境变量

​	通过vim /etc/profile命令，打开etc目录下的环境变量配置文件profile，按“i”键在最底端输入如下代码

```cmd
[root@localhost /]# vim /etc/profile
```

​	配置内容: 

~~~
export JAVA_HOME=/usr/local/install/jdk-11
export PATH=$JAVA_HOME/bin:$PATH
~~~

​	输入完毕后，按“esc”键退出编辑，输入“：wq”保存并退出profile配置文件操作如图：

![](/配置JAVA环境.png)

​	通过source /etc/profile命令**更新配置**文件：

```cmd
[root@localhost /]# source /etc/profile
```

## 6 验证结果

```cmd
[root@localhost /]# java -version
java version "11" 2018-09-25
Java(TM) SE Runtime Environment 18.9 (build 11+28)
Java HotSpot(TM) 64-Bit Server VM 18.9 (build 11+28, mixed mode)
```

# 五 Java 实现MySQL同步 flink cdc 做数据收集

## 1 注意事项

​		MySQL: 数据同步由flink读取binlog日志数据实现，若flink部署无误, 数据无法同步或者增量同步, 需要检查连接的MySQL数据库binlog日志内容是否有写入。

## 2 数据同步实现方式

​		参考：https://blog.csdn.net/icefox_zhaoyt/article/details/114767689



# 八 flink cdc 集成蹚坑合集

<!--修改后需要重启数据库、重新连接数据库-->

## 1 数据包太大

​		**异常：Packet for query is too large (4,739,923 > 65,535). You can change this value on the server by setting the 'max_allowed_packet' variable.**

​	注解: 用于查询的数据包太大（4739923>65535）。您可以通过设置“max_allowed_packet”变量在服务器上更改此值。

```mysql
# 地址：https://blog.csdn.net/wxyf2018/article/details/106115678
show VARIABLES like '%max_allowed_packet%';
```

## 2 MySQL版本太低

​		**异常： The server time zone value 'PDT' is unrecognized or represents more than one time zone. You must configure either the server or JDBC driver (via the 'serverTimezone' configuration property) to use a more specifc time zone value if you want to utilize time zone support**

参考: https://blog.csdn.net/dreamboy_w/article/details/96505068

使用了Mysql最新版驱动所以报错

新版驱动名字为 driverClass=“com.mysql.cj.jdbc.Driver”

**解决方案：**

方案1、在项目代码-数据库连接URL后，加上 ?serverTimezone=UTC（注意大小写必须一致）

```java
DebeziumSourceFunction<String> soureFunction = MySqlSource.<String>builder()
                .hostname("192.168.144.129")
                .port(3306)
                .serverTimeZone("UTC")
                .username("root")
                .password("123456")
                .databaseList("test")
                .tableList("student1")
                .deserializer(new StringDebeziumDeserializationSchema())
                .startupOptions(StartupOptions.initial())
                .build();
```

方案2、在mysql中设置时区，默认为SYSTEM（推荐）

```mysql
mysql>set global time_zone='+8:00';
```

## 3 binlog日志未开启

​		**异常： Cannot read the binlog filename and position via 'SHOW MASTER STATUS'. Make sure your server is correctly configured**

​	注解：无法通过“SHOW MASTER STATUS”读取binlog文件名和位置。确保服务器配置正确。

​	参考：https://blog.csdn.net/qq_42657496/article/details/122393821

​	注意：需要将配置写到[mysqld]下，否则不生效

```mysql
-- SHOW MASTER STATUS
show variables like '%log_bin%';
```

## 4 类加载机制顺序冲突

​	地址：https://www.cnblogs.com/quchunhui/p/12506037.html

```java
Caused by: java.lang.ClassCastException: cannot assign instance of java.util.ArrayList to field org.apache.flink.runtime.jobgraph.JobVertex.results of type java.util.Map in instance of org.apache.flink.runtime.jobgraph.JobVertex
	at java.base/java.io.ObjectStreamClass$FieldReflector.setObjFieldValues(ObjectStreamClass.java:2190)
	at java.base/java.io.ObjectStreamClass$FieldReflector.checkObjectFieldValueTypes(ObjectStreamClass.java:2153)
	at java.base/java.io.ObjectStreamClass.checkObjFieldValueTypes(ObjectStreamClass.java:1407)
	at java.base/java.io.ObjectInputStream.defaultCheckFieldValues(ObjectInputStream.java:23
```

​	原因（可能是启动的版本不对）：

```
LinkedMap class is being loaded from two different packages, and those are being assigned to each other.
LinkedMap类从两个不同的包中加载，这些包被相互分配
```

## 5 无法通过“显示主控状态”读取binlog文件名和位置 

```java
Cannot read the binlog filename and position via 'SHOW MASTER STATUS'. Make sure your server is correctly configured
```

![](/无法读取binlog日志名称和位置.png)

**问题**：没有获取binlog文件名和起始位置权限

**参考**：[MySQL Binlog 权限](https://www.cnblogs.com/chenzechao/p/15839542.html)

- MySQL Binlog权限需要三个权限 SELECT, REPLICATION SLAVE, REPLICATION CLIENT，同时增加超级复制客户端权限

```mysql
SHOW MASTER STATUS;  # 可查看binlog文件名和起始位置。

# 执行异常
# Access denied; you need (at least one of) the SUPER, REPLICATION CLIENT privilege(s) for this operation（访问；此操作需要（至少一个）超级复制客户端权限）
```





# end 

