

/**
 * 获取标准库中所有标准表对应的实例表
 */
private getStdInstTable() {
	let stdLibIds = this.stdLibIds;
	let instTableInfos = dw.queryDwTableData({
		sources: [{
			id: 'model1',
			path: "/sysdata/data/tables/dg/DG_STD_TABLES_INST.tbl"//标准实例关联表
		}, {
			id: "model2",
			path: "/sysdata/data/tables/meta/META_FILES.tbl"//文件表
		}, {
			id: "model3",
			path: "/sysdata/data/tables/dg/DG_STD_TABLES.tbl"//标准内容表
		}],
		fields: [{
			name: "STD_LIB_ID",
			exp: `model3.STD_LIB_ID`
		}, {
			name: "INST_TABLE_ID",
			exp: `model1.INST_TABLE_ID`
		}, {
			name: "NAME",
			exp: `model2.NAME`
		}, {
			name: "DESC",
			exp: `model2.DESC`
		}, {
			name: "PARENT_DIR",
			exp: `model2.PARENT_DIR`
		}, {
			name: "STD_TABLE_FOLD_ID",
			exp: `model3.STD_TABLE_FOLD_ID`
		}, {
			name: "STD_TABLE_ID",
			exp: `model3.STD_TABLE_ID`
		}],
		joinConditions: [{
			leftTable: 'model1',
			rightTable: 'model2',
			joinType: SQLJoinType.LeftJoin,
			clauses: [{
				leftExp: 'model1.INST_TABLE_ID',
				operator: ClauseOperatorType.Equal,
				rightExp: 'model2.ID'
			}]
		}, {
			leftTable: 'model1',
			rightTable: 'model3',
			joinType: SQLJoinType.LeftJoin,
			clauses: [{
				leftExp: 'model1.STD_TABLE_ID',
				operator: ClauseOperatorType.Equal,
				rightExp: 'model3.STD_TABLE_ID'
			}]
		}],
		filter: [{
			clauses: [{
				leftExp: 'model3.STD_LIB_ID',
				operator: ClauseOperatorType.In,
				rightValue: stdLibIds.join(",")
			}]
		}, {
			clauses: [{
				leftExp: 'model2.TYPE',
				operator: ClauseOperatorType.Equal,
				rightValue: "tbl"
			}]
		}],
		select: true
	});
	for (let i = 0; i < instTableInfos.length; i++) {
		const element = instTableInfos[i];
		let info = new StdInstTableService({
			stdLibId: <string>element[0], resid: <string>element[1], name: <string>element[2], desc: <string>element[3], parentDir: <string>element[4], catalogId: <string>element[5], stdTableId: <string>element[6]
		});
		this.stdInstInfos.push(info);
	}
	return instTableInfos;
}