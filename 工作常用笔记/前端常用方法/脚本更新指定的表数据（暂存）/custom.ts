import { ctx, SZEvent, Component, throwInfo, throwError, showConfirmDialog, showMessage, showFormDialog, showErrorMessage, showMenu, isEmpty, deepAssign, uuid, message, quoteExpField, showDialog, showSuccessMessage, showProgressDialog, rc, downloadFile, waitRender, showWaiting, assign, IDataProvider, formatDateFriendly, parseDate, formatDate, tryWait, rc_get, showWarningMessage, timerv, showWaitingMessage, hideMessage, DataChangeEvent, EventListenerManager, GroupInfo, IFindArgs, ServiceTaskPromise, ServiceTaskInfo, ServiceTaskState, rc_task } from 'sys/sys';
import { IMetaFileCustomJS, InterActionEvent, IAppCustomJS, IDwCustomJS, DatasetDataPackageRowInfo, IDataset, IVComponent } from "metadata/metadata-script-api";
import { showDrillPage, getMetaRepository, MetaFileViewer, getCurrentUser, getSysSettings } from 'metadata/metadata';
import { getDwTableDataManager, labelData, refreshModelState, exportQuery, importData, ImportDataResult, getQueryManager, pushData, importDbTables, DwDataChangeEvent } from 'dw/dwapi';
import { DwPseudoDimName, DwTableModelFieldProvider } from 'dw/dwdps';
import { Combobox, Button, showUploadDialog, Toolbar, ToolbarItemAlign, ComboboxArgs, DataProvider, ButtonArgs, TXT_FILETYPE, IMAGE_FILETYPE, showMessageBox, MessageBoxArgs, MessageContentInfo, MessageBoxType } from 'commons/basic';
import { Tree } from 'commons/tree';
import { Menu, MenuItem } from "commons/menu";
import { SuperPageBuilder } from "app/superpage/superpagebuilder";
import { SuperPage } from 'app/superpage/superpage';
import { AnaModelBuilder } from 'ana/builder';
import { IDwTableEditorPanel, DwTableEditor } from 'dw/dwtable';
import { showLabelMgrDialog } from 'dw/dwdialog';
import { Form } from "commons/form";
import { ExDTable } from "commons/dtable";
import { Dialog } from 'commons/dialog';
import { PortalModulePageArgs, PortalHeadTitle, PortalHeadToolbar } from 'app/templatepage';
import { html2pdf, ExportReusltType } from 'commons/export';
import { modifyClass, getAppPath, showScheduleDialog } from './commons/commons.js';
import { SuperPageData } from 'app/superpage/superpagedatamgr';
import { EasyFilter } from 'commons/extra.js';
import { ExpCompiler, ExpVar, Token, ExpTokenIndex, IExpDataProvider } from 'commons/exp/expcompiler';
import { SpgEmbedSuperPage } from 'app/superpage/components/embed';
import { MetaFileRevisionView, showResourceDialog } from 'metadata/metamgr';
import { MetaFileViewer_miniapp } from 'app/miniapp';
import { ScheduleEditor, getScheduleMgr, TaskLogInfo } from "schedule/schedulemgr";
import { showDbConnectionDialog, DbConnectionPanel } from 'datasources/datasourcesmgr';
import { ProgressLogs, ProgressPanel } from 'commons/progress';
import { ExpDialog, ExpDialogValueInfo, IFieldsDataProvider, showExpDialog } from 'commons/exp/expdialog';
import { getCamelChars } from 'commons/pinyin';
import { ExpContentType } from 'commons/exp/exped';

import { InputDialog, showInputDialog, ShowInputDialogArgs } from 'commons/dialog';
import { checkCstmRule, runCheckTask, checkField, CheckCstmRuleArgs } from 'dg/dgapi';

/**
 * 数据接入spg定制脚本
 */
const Access_CustomActions = {
	/**
	 * 202111222 tuzw
	 * 将新创建的物理表信息：插入到  数据接入数据表配置信息（暂存，不改变数据库值），并自动配置源表和目标表之间的字段映射关系
	 * 帖子：https://jira.succez.com/browse/CSTM-17082?focusedCommentId=191030&page=com.atlassian.jira.plugin.system.issuetabpanels%3Acomment-tabpanel
	 
	   暂存使用：IDataset对象（暂时写入缓存中，不插入到数据库）
	 */
	button_ac_autoMatchDataTableConf: (event: InterActionEvent) => {
		let page = event.page;
		let params = event.params;
		/** 来源库 */
		let sourceDb: string = params.sourceDb;
		/** 目标库 */
		let targetDb: string = params.targetDb;
		/** 列表中没有匹配的表（已引入但未匹配源表）：当前脚本设置的参数，用于返回：SPG */
		let noMatchTable = params.noMatchTable;
		/** 指定创建对应的：源表（可为NULL） */
		let inputValue: string = params.inputValue;
		/** 过滤条件：（1）已引入但未匹配: existAdd （2）未引入的源表：noAdd */
		let comboboxValue: string = params.comboboxValue;
		/** 数据接入数据表配置信息：主键 */
		let dataBaseId: string = params.dataBaseId;
		/** 获取自定义物理表名前缀 */
		let targetTablePrefix = !!params.tablePrefix ? params.tablePrefix : "";
		
		/** 进行操作的数据源：数据接入数据表配置信息（增、删、改 ： 暂存） */
		let tableConfDataset: IDataset = page.getDataset('model4');
		/** 
			返回【当前模型】暂存的数据
			getRows() 获取当前页数据（包含所有暂存的数据）
			注意：【暂存数据】会插入到当前页，eg：当前页100条数据，暂存一条，则101条，无视分页组件（翻页会自动放到下一页）
		 */
		let allRows = tableConfDataset.getRows();
		/** 查询当前配置列表中目标表为NULL的数据 */
		let matchedRows = allRows.filter(r => {
			let data = r.getData();
			return data["TARGET_TABLE"] == null;
		});
		
		/**  
			查询【已确认创建的物理表】：获取当前列表显示的数据
		*/
		let queryInfo: QueryInfo = {
			/** 源库，目标库，源表名，目标表名 */
			fields: [{ name: "SOURCE_DB" }, { name: "TARGET_DB" }, { name: "SOURCE_TABLE" }, { name: "STATUE" }],
			// FilterInfo[]
			filter: [],
			/**
			 * params：Array<QueryParamInfo> ：对应SPG数据集在【设置】的过滤条件
				  
			 * name：【参数在SPG中的唯一标识：ID】，eg：全局参数3：param3 、 inputValue（输入控件）：id：input26
			   
			   注意：
			   （1）过滤的静态值不用写入param中
			   （2）【设置】中有多少个【动态值】，就写多个【name】值，【从上到下】依次往下
			   （3）若存在【判断】语句，则：有多少个的结果，就写多少个name
			 */
			params: [{
				/**
					这里的param 不是SPG全局变量对应，类似一个：变量，不过要与SPG对应模型设置的过滤条件顺序一致
				*/
				name: "param1", value: sourceDb // [来源库中未匹配上目标库中的物理表_确认].[来源数据库]=[sourceDataBase]
			}, {
				name: "param2", value: targetDb
			}, {
				name: "input26", value: inputValue
			}, {
				// IF([创建目标表_来源表].[值]='existAdd',[来源库中未匹配上目标库中的物理表_确认].[来源表]=[noMathTable],TRUE)
				name: "combobox10", value: comboboxValue
			}, {
				// IF([创建目标表_来源表].[值]='existAdd',[来源库中未匹配上目标库中的物理表_确认].[来源表]=[noMathTable],TRUE)
				name: "param16", value: noMatchTable
			}],
			queryId: "model31",
			resid: "/sysdata/app/zt.app/data-access/data-access-create.spg",
			select: true,
			sort: [],
			/**
				若指定了sources，则必须指定：filter
				eg：filter: [{
						exp: `model1.fileId=param1 and model1.isExist=param2`
					}]
				若没有指定sources，默认与指定SPG对应的模型过滤条件一致
			*/
			sources: null
		};
		/** 
		 * getQueryManager()：QueryManager：获取一个查询结果管理员（支持查询结果缓存管理）
		 */
		return getQueryManager().queryData(queryInfo, uuid()).then(result => {
			/** 刚刚创建的物理表信息集，eg: [["sqlserver2017", "SJZX_ODS", "FACT_TAX_RD_NSRZGXX", 1], ...] */
			let queryData = result.data;
			/** 待插入数据组 */
			let insertRow: SDI_DATA_ACCESS_TABLES_CONF[] = [];
			let user = getCurrentUser();
			let userId = user.userId;
			/** 记录当前操作的数据，用于自动设置字段映射配置关系 */
			let tableInfos = [];
			for (let index = 0; index < queryData.length; index++) {
				let data = queryData[index];
				// 校验当前列表的【来源表】是否已经存在：数据集"数据接入数据表配置信息"
				let exist = matchedRows.find(r => {
					let d = r.getData();
					let sourceTable = data[2];
					return d["SOURCE_TABLE"] == sourceTable;
				});
				let keys: string = "";
				if (exist != null) {
					keys = exist.getKeyString();
					// 存在，修改配置关系（暂存）
					tableConfDataset.modify(
						// 注意：更新的值需要准确，否则会更新失败，但是不会报错，eg：字符串格式为：'1,2,3' 和 '[1,2,3]'两种，后者则会失败 
						{ "UUID": keys },
						{ 'DATABASE_ID': dataBaseId, 'SOURCE_TABLE': data[2], 'TARGET_TABLE': `${targetTablePrefix}${data[2]}`, 'SCHEDULE_CODE': 'default', 'POLICY': 'TOTAL', 'CREATE_TIME': new Date().getTime(), 'CREATOR': userId }
					)
				} else {
					keys = uuid();
					// 列表【来源表】不存在，则记录到：待插入数据组（来源和目标表 名字一致）
					insertRow.push(
						{ 'UUID': keys, 'DATABASE_ID': dataBaseId, 'SOURCE_TABLE': data[2], 'TARGET_TABLE': `${targetTablePrefix}${data[2]}`, 'SCHEDULE_CODE': 'default', 'POLICY': 'TOTAL', 'CREATE_TIME': new Date().getTime(), 'CREATOR': userId });
				}
				tableInfos.push({
					tableId: keys,
					sourceDs: sourceDb,
					targetDs: targetDb,
					sourceTable: `${targetTablePrefix}${data[2]}`,
					targetTable: data[2]
				});
			}
			// 写入暂存中
			tableConfDataset.insert(insertRow);
			// 自动设置列表中：源表和目标表的字段匹配关系
			return rc({
				url: "/zt/dataAccess?method=autoMatchAllSourceAndTargetTable",
				method: "POST",
				data: {
					dataBaseId, tableInfoStr: JSON.stringify(tableInfos)
				}
			});
		})
	}
}

/**
 * 数据接入数据表配置信息
 * (SDI_DATA_ACCESS_TABLES_CONF)
 */
interface SDI_DATA_ACCESS_TABLES_CONF {
	/** 主键ID  */
	UUID?: string;
	/** 接入数据库ID */
	DATABASE_ID?: string;
	/** 是否同步 */
	IS_SYNC?: number;
	/** 应用数据库表配置ID */
	USED_CONF_TABLE_ID?: string;
	/** 来源表名 */
	SOURCE_TABLE?: string;
	/** 目标表名 */
	TARGET_TABLE?: string;
	/** 计划任务 */
	SCHEDULE_CODE?: string;
	/** 同步策略 */
	POLICY?: string;
	/** 创建时间 */
	CREATE_TIME?: number;
	/** 创建者 */
	CREATOR?: string;
	[propname: string]: any,
}


/** -----------------------------------------------《后端脚本》----------------------------------------------------*/

/**
 * 自动匹配列表所有的表字段映射关系
 * @param request
 * @param response
 * @param params.tableInfoStr [{id,sourceDs,sourceTable,targetDs,targetTable}]
 */
export function autoMatchAllSourceAndTargetTable(request: HttpServletRequest, response: HttpServletResponse, params: { dataBaseId: string, tableInfoStr: string }) {
	let { dataBaseId, tableInfoStr } = params;
	let tableInfos = JSON.parse(tableInfoStr);
	for (let i = 0; i < tableInfos.length; i++) {
		let tableInfo = tableInfos[i];
		let { tableId, sourceDs, sourceTable, targetDs, targetTable } = tableInfo;
		let middleTable = ds.openTableData('SDI_DATA_ACCESS_FIELDS_MIDDLE');
		middleTable.del({ "TABLE_ID": tableId });
		insertFieldToMiddleTable(null, null, { tableId, dataBase: sourceDs, tableName: sourceTable, type: "source" });
		insertFieldToMiddleTable(null, null, { tableId, dataBase: targetDs, tableName: targetTable, type: "target" });
		print("匹配的表和数据库配置为" + dataBaseId + "," + tableId);
		matchTargetandSourceFields(null, null, { dataBaseId, tableId });
	}
	return true;
}