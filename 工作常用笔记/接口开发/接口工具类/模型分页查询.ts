
import db from "svr-api/db";
import dw from "svr-api/dw";
import http from "svr-api/http";
import metadata from "svr-api/metadata";
import sys from "svr-api/sys";

const Timestamp = Java.type('java.sql.Timestamp');

/**
 * 模型分页查询工具类
 * @description
 * 1) 内置dw模块查询, 不支持多线程
 */
class ModelQueryUtils {

    /** 查询模型路径 */
    private modelPath: string;
    /** 每次查询数据量(limit), 默认： 10 */
    private queryMaxNum: number;

    /** 排序字段 */
    private sortField: string;
    /** 排序方式, 默认: asc、desc */
    private sortMethod: string;

    /** 查询方式, 默认：全量 */
    private queryMethod: ModelQueryMethod;
    /** 增量字段 */
    private incrementField: string;

    /** 
     * 查询字段信息, eg: [{
     *   name: "TASK_ID", exp: "model1.TASK_ID.SCHEDULE"
     * }] 
     */
    private queryFields: QueryFieldInfo[];
    /** 
     * 模型过滤条件, eg: [{
     *   exp: `model1.ADCODE IS NULL AND model1.TOWNCODE IS NULL`
     * }]
     */
    private filterConditions: FilterInfo[];

    /** 满足条件的数据总数量 */
    private querySumNums: number;
    /** 查询的queryInfo参数 */
    private queryInfo: QueryInfo;

    constructor(args: {
        modelPath: string,
        queryFields: QueryFieldInfo[],
        sortField: string,
        sortMethod?: string,
        queryMethod?: ModelQueryMethod,
        incrementField?: string,
        filterConditions?: FilterInfo[],
        queryMaxNum?: number
    }) {
        this.modelPath = args.modelPath;
        this.queryFields = args.queryFields;
        this.sortField = args.sortField;

        this.sortMethod = !!args.sortMethod ? args.sortMethod : "asc";
        this.queryMethod = !!args.queryMethod ? args.queryMethod : ModelQueryMethod.FullQuery;
        this.incrementField = args.incrementField as string;
        this.queryMaxNum = !!args.queryMaxNum ? args.queryMaxNum : 100;
        this.filterConditions = !!args.filterConditions ? args.filterConditions : [];
        this.initParams();
    }

    /**
     * 初始内部参数
     */
    private initParams(): void {
        this.builderQueryInfo();
        this.queryFulfilConditionNums();
    }

    /**
     * 分页查询模型数据
     * @param queryStartIndex 从第几行开始, 0: 是第一行,不传递此参数表示从第一行开始, 默认值为0
     * @return {
     *   result: true,
     *   data: [{
     *     JSON数据
     *   }]
     * }
     */
    public queryModelDatas(queryStartIndex: number): ResultInfo {
        if (!queryStartIndex) {
            queryStartIndex = 0;
        }
        let querySumNums: number = this.querySumNums;
        if (queryStartIndex > querySumNums) {
            return { result: false, message: `当前查询行数超过查询的总行数, 总行数: ${querySumNums}` };
        }
        let preCheckResult: ResultInfo = this.preCheck();
        if (!preCheckResult.result) {
            return preCheckResult;
        }
        let queryInfo: QueryInfo = this.queryInfo;
        queryInfo.options = {
            limit: this.queryMaxNum,
            offset: queryStartIndex,
            queryTotalRowCount: false
        }
        let datas: JSONObject[] = [];
        let queryDatas = dw.queryData(queryInfo).data;
        for (let i = 0; i < queryDatas.length; i++) {
            let queryData = queryDatas[i] as string[];
            let data: JSONObject = {};
            for (let fieldIndex = 0; fieldIndex < this.queryFields.length; fieldIndex++) { // 查询的结果根查询字段顺序一致, 反向将数组转换为JSON数据
                let fieldName: string = this.queryFields[fieldIndex].name;
                data[fieldName] = queryData[fieldIndex];
            }
            datas.push(data);
        }
        return { result: true, data: datas };
    }

    /**
     * 获取查询数量的总数
     */
    public getQuerySumNums(): number {
        return this.querySumNums;
    }

    /**
     * 预检查
     */
    private preCheck(): ResultInfo {
        if (!this.modelPath) {
            return { result: false, message: "模型地址为空" };
        }
        if (!this.queryFields || this.queryFields.length == 0) {
            return { result: false, message: "查询字段未指定" };
        }
        if (this.queryMethod == ModelQueryMethod.Increment && !this.incrementField) {
            return { result: false, message: `增量查询, 未指定增量字段` };
        }
        if (!this.sortField) {
            return { result: false, message: "未指定排序字段" };
        }
        return { result: true };
    }

    /**
     * 构造queryInfo信息
     */
    private builderQueryInfo(): void {
        let queryFields: QueryFieldInfo[] = this.queryFields;
        if (!this.modelPath || !queryFields || queryFields.length == 0 || !this.sortField) {
            return;
        }
        this.getQueryFilterCondition();
        this.queryInfo = {
            fields: queryFields,
            select: true,
            sources: [{
                id: "model1",
                path: this.modelPath
            }],
            filter: this.filterConditions,
            sort: [{
                exp: `model1.${this.sortField}`,
                type: this.sortMethod
            }],
            options: {
                limit: 1,
                offset: 0,
                queryTotalRowCount: true
            }
        };
        console.info(`查询的queryInfo信息为: `);
        console.info(this.queryInfo);
    }

    /**
     * 查询满足条件的数据总数
     */
    private queryFulfilConditionNums(): void {
        if (!this.queryInfo) {
            return;
        }
        let queryData = dw.queryData(this.queryInfo);
        if (queryData != null) {
            this.querySumNums = queryData.totalRowCount as number;
        }
        console.info(`共需要查询数量为: ${this.querySumNums} 条`);
    }

    /** 
     * 根据查询方式构造查询条件
     * @description
     * 1) 增量默认为执行时间的前一天0点到当天0点, 数据范围为：24小时
     */
    private getQueryFilterCondition(): void {
        let sdfTime = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        if (this.queryMethod == ModelQueryMethod.Increment) {
            let filterField: string = this.incrementField;
            if (!filterField) {
                return;
            }
            let now: number = new Date().setHours(0, 0, 0);
            let startTime: string = sdfTime.format(now);
            let filterNums: number = now - 24 * 60 * 60 * 1000 - 1 * 60 * 1000;
            let lastSendTime: string = sdfTime.format(new java.util.Date(filterNums));
            let filterExp: string = `model1.${filterField} >= '${lastSendTime}' and model1.${filterField} < ${startTime}`;
            console.info(`当前请求方式是: 增量请求, 过滤表达式: [${filterExp}]`);
            this.filterConditions.push({
                exp: filterExp
            });
        }
    }
}

/** 返回值 */
interface ResultInfo {
    /** 返回值 */
    result?: boolean;
    /** 处理消息 */
    message?: string;
    /** 返回数据 */
    data?: any;
}

/** 模型查询方式 */
enum ModelQueryMethod {
    /** 全量 */
    FullQuery = "fullQuery",
    /** 增量 */
    Increment = "increment"
}