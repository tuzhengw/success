# JSOUP

```
jsoup 是一款Java 的HTML解析器，可直接解析某个URL地址、HTML文本内容。
它提供了一套非常省力的API来取出和操作数据
```

官方地址：https://jsoup.org/cookbook/input/parse-document-from-string

中文地址：https://www.sunjs.com/doc/jsoup-44df0d5c8c974b9ab03f400457961bc1.html



# 产品HTML操作

```ts
function main() {
     let cotent = `
    <!--SUCC-RICHTEXT--><p>123</p>
    <p><img width="123" height="97" data-file-id="33e2e4ee85645fa847cf989b8caabc26a9f8"> &nbsp;
       <img width="123" height="97" data-file-id="33e2866b529f30e846c497b1e4b0387ea810"> 
       <img width="134" height="106" data-file-id="33e2ad26f38f7105404f8eea187b96e6915f">
    </p>
    `;
    dealSingalHtmlCotent(cotent);
}


/**
 * 处理单个富文本的HTML值, 根据HTML所存附件ID, 读取服务器的图片Base64值 
 * <p>
 * 编译HTML内容后, 让其HTML的附件(eg: img) 可获取到对应的src值
      <img width="279" height="220" 
         data-file-id="33e23337c1fda85a4ea185ab4e274e1d2fed" // 富文本对应的HTML值仅记录附件ID
         src="Base64:XXXX"> // 编译后HTML 可获取src值=附件的BASE64值
 * </p>
 * @param htmlContent 未编译过的HTML内容 
 * @return 返回JAVA字符串
 */
function dealSingalHtmlCotent(htmlContent: string): JavaString {
    if (htmlContent) {
        return new JavaString("");
    }
    let customZTFile: MetaFileInfo = metadata.getFile('/sysdata/app/note.app/notePcMgr/noteDownloadModule.html', false);
    let doc = Jsoup.parse(customZTFile.getContentAsInputStream(), "UTF-8", "");
    doc.outputSettings(new Document.OutputSettings().prettyPrint(true));
    let startDiv = doc.select("div[type='topLabel']");
    let lastElement = startDiv.first();
    let afterElement = lastElement.after(htmlContent);
    afterElement.nextElementSibling();

    let elements = doc.getElementsByTag("img");
    for (let i = 0; i < elements.length; i++) {
        let element = elements[i];
        let attachId: string = element.attr("data-file-id");
        let attachBase64: string = getSignAttachBase64(attachId);
        element.attr("src", attachBase64); // 设置对应附件图片的BASE 64值
    }
    let dealedHtml: string = doc.body().outerHtml();
    return dealedHtml;
}
```

# API介绍

网址：https://blog.csdn.net/java_xxxx/article/details/86089881

```ts
public static void main(String[] args) {
// 根据全国各个高校的地址，获取视频中相应列表的URL和要抓取的值
        Document document = null;
        try {
            document = Jsoup
                    .connect("http://gaokao.chsi.com.cn/sch/search.do?start=0")
                    .get();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Element element = document.body();
        //System.out.println(element);
        Elements ths = document.select("th");
        System.out.println(ths);
        Elements tds = document.select("td");
        System.out.println(tds);
}
 
一、简介
该类是位于select包下，直接继承自Object，所有实现的接口有Cloneable, Iterable<Element>, Collection<Element>, List<Element>类声明：public class Elements extends Object implements List<Element>, Cloneable可以使用Element.select(String) 方法去得到Elements 对象。
二、构造方法
1、public Elements()  默认构造方法
2、public Elements(int initialCapacity) 指定一个初始容量创建一个Elements对象。
3、public Elements(Collection<Element> elements)  使用已知元素集创建一个Elements对象。
4、public Elements(List<Element> elements)  使用已知元素的List集合创建一个Elements对象。
5、public Elements(Element... elements) 使用已知元素的可变参数列表创建一个Elements对象。
三、方法详细
1、public Elements clone()  克隆
2、public String attr(String attributeKey) 根据键得到第一个匹配的元素（匹配即为有这个属性）。
3、public boolean hasAttr(String attributeKey)   元素集中存在任何一个元素匹配（有这属性）则返回true。
4、public Elements attr(String attributeKey, String attributeValue) 将 所有匹配attributeKey键的元素的值设置为attributeValue。
5、public Elements removeAttr(String attributeKey)  移除元素集中任何一个匹配的元素
6、public Elements addClass(String className)  将className增加到每一个匹配的元素的class属性上。
7、public Elements removeClass(String className)  从每一个匹配的元素上移除该className
8、public Elements toggleClass(String className)   对每一个匹配的元素的class属性上进行反转。（有则移除，没有则新增）。
9、public boolean hasClass(String className)  检测是否有任何一个匹配的元素在class属性有给定的className值。
10、public String val()  得到第一个匹配元素的表单的值。
11、public Elements val(String value)   对每一个匹配的元素设置表单值。
12、public String text()  得到所有匹配元素文本的综合。该方法在某些情况下会得到重复数据。
13、public boolean hasText() 检测是否有文本内容
14、public String html()  得到所有匹配元素的内部html的综合。
15、public String outerHtml()  得到所有匹配元素的外部html的综合。
16、public String toString()  得到所有匹配元素的外部html的综合。
17、public Elements tagName(String tagName)  更新每个匹配元素的tag name.   如想把每个<i>变成<em>，可以这样：doc.select("i").tagName("em");
18、public Elements html(String html)  设置每个匹配元素的内部html。
19、public Elements prepend(String html)   将指定html增加到每个匹配元素的内部html开头。
20、public Elements append(String html)   将指定html增加到每个匹配元素的内部html末尾。
21、public Elements before(String html)   在每个匹配元素的外部html前面插入指定html。
22、public Elements after(String html)   在每个匹配元素的外部html后面插入指定html。
23、public Elements wrap(String html)  用指定html包装每个匹配的元素。
例如，对于这个html：<p><b>This</b> is <b>Jsoup</b></p>，执行这个包装：doc.select("b").wrap("<i></i>")后就变成：<p><i><b>This</b></i> is <i><b>jsoup</b></i></p>
24、public Elements unwrap()   移除匹配的元素但保留他们的内容。示例：<div><font>One</font> <font><a href="/">Two</a></font></div>   执行 doc.select("font").unwrap()   变成：<div>One <a href="/">Two</a></div>
25、public Elements empty()  清空每个匹配元素的内容。示例：<div><p>Hello <b>there</b></p> <p>now</p></div>  执行doc.select("p").empty()  变成<div><p></p> <p></p></div>
26、public Elements remove()  从DOM树中移除匹配的元素。示例：<div><p>Hello</p> <p>there</p> <img /></div>   执行doc.select("p").remove()后 变成<div> <img /></div>
27、public Elements select(String query)  根据query选择器查询匹配的元素集。
28、public Elements not(String query)  移除匹配选择器的元素集   返回过滤后的元素集。
29、public Elements eq(int index)  根据index得到匹配的元素
30、public boolean is(String query)  检测是否有一个元素匹配给定的选择器。
31、public Elements parents()   得到匹配元素集的所有父类元素和祖先元素集
32、public Element first()  得到第一个匹配的元素
33、public Element last()   得到最后一个匹配的元素
34、public Elements traverse(NodeVisitor nodeVisitor)  对被查询的元素执行一次深度优先的遍历。
35、public int size()  元素集的长度。
36、public boolean isEmpty()   检测是否为空
37、public boolean contains(Object o)  检测是否包含指定对象
38、public Iterator<Element> iterator()   得到迭代器对象
39、public Object[] toArray()  将元素集转换为数组
40、public <T> T[] toArray(T[] a)
41、public boolean add(Element element)  新增元素
42、public boolean remove(Object o)  移除指定元素
43、public boolean containsAll(Collection<?> c)  参照java中的List或Collection用法.
44、public boolean addAll(Collection<? extends Element> c)  参照java中的List或Collection用法.
45、public boolean addAll(int index, Collection<? extends Element> c)  参照java中的List或Collection用法.
46、public boolean removeAll(Collection<?> c)     参照java中的List或Collection用法.
47、public boolean retainAll(Collection<?> c)  参照java中的List或Collection用法.
48、public void clear() 清空元素集
49、public Element get(int index)  根据索引得到指定元素
50、public Element set(int index, Element element)  根据索引设置指定元素
51、public void add(int index, Element element) 在指定位置增加元素
52、public Element remove(int index)  移除指定位置的元素
53、public int indexOf(Object o)  得到指定元素的索引（第一次出现的位置）
54、public int lastIndexOf(Object o) 得到指定元素最后一次出现的位置。
55、public ListIterator<Element> listIterator() 具体参照List<Element>
56、public ListIterator<Element> listIterator(int index)  具体参照List<Element>
57、public List<Element> subList(int fromIndex, int toIndex)  根据起始点得到子集
```



# 运用

网址：https://blog.csdn.net/zhangmingziliao/article/details/77479866

## 1 获取html文档中的img标签

```java
public static void  parseHtmlImgElement(){
		String url = "https://www.qq.com/";
		try {
			Document document = Jsoup.connect(url).get();
			//获取html body元素
			Element element = document.body();
			Elements elements = element.getElementsByTag("img");
			for(Element e : elements){
				System.out.print("网页中图片地址:" + "http:" + e.attr("src"));
				System.out.print(";宽:" + e.attr("width"));
				System.out.println(";高:" + e.attr("height"));
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
```

