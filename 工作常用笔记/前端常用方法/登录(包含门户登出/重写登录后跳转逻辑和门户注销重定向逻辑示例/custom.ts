
/**
 * =====================================================
 * 作者：tuzw
 * 审核人员：liuyz
 * 创建日期: 2023-01-10
 */
import { ITemplatePagePartRenderer, PortalHeadbar, PortalHeadToolbar } from "app/templatepage";
import { Component, ctx, message, showWarningMessage } from "sys/sys";
import { IMetaFileCustomJS } from "metadata/metadata";

/**
 * 公管平台综合报表门户脚本
 */
export class PublicTubePlatformIndex {

	/**
	 * 开发注意: 门户扩展点会执行多次，处理前先判断是否是需要处理的类
	 */
	onDidInitFrameComponent(frame: Component, portalHeadBar: ITemplatePagePartRenderer, rootRes?: ResourceRefInfo) {
		this.overridePortalLogoutEvent(frame, portalHeadBar);
	}

	/**
	 * 基于数据中台开发的应用公管平台门户-重写注销按钮，跳转到指定页面
	 * 2023-01-10 tuzw
	 * 地址：https://jira.succez.com/browse/CSTM-21769
	 */
	private overridePortalLogoutEvent(frame: Component, portalHeadBar: ITemplatePagePartRenderer): void {
		if (portalHeadBar instanceof PortalHeadbar == false) {
			return;
		}
		let portalTempLatePage = frame;
		portalHeadBar.refresh().then(() => {
			let portalHeadToolbar: PortalHeadToolbar = portalHeadBar.toolbar;
			portalHeadToolbar.owner.logout = () => {
				if (portalTempLatePage.builder.viewMode === DesignerViewMode.Edit) {
					showWarningMessage(message('dsn.tpg.logout.warningInfo'));
					return;
				}
				location.replace(ctx('/GLYLJGGLPT/login')); // 重写注销逻辑跳转重定向地址
			}
		});
	}
}

export const CustomJS: { [key: string]: IMetaFileCustomJS } = {
	app: new PublicTubePlatformIndex(),
}