/**
 * 作者：tuzw
 * 审核者: liuyz
 * 创建日期：2022-12-01
 * 脚本用途：指标综合查询开发脚本
 */
import {
	throwError, assign, message, rc, Component, showSuccessMessage, isEmpty, showWarningMessage, SZEvent
} from "sys/sys";
import { IMetaFileCustomJS } from "metadata/metadata";
import { InterActionEvent } from "metadata/metadata-script-api";
import { ReportData } from "ana/rpt/reportdatamgr";
import { Report } from "ana/rpt/report";
import { Combobox, ComboboxFloatPanel } from "commons/basic";
import { ListItem } from "commons/tree";

/**
 * 销售指标--零售金融统计报表
 */
export class SPG_SaleBaseRetailDimension {

	public CustomExpFunctions: any;
	public CustomActions: any;

	constructor() {
		this.CustomActions = {
			autoSetEmbedreportParams: this.autoSetEmbedreportParams.bind(this)
		};
	}

	/**
	 * 控件发生渲染前调用
	 */
	public onRender(event: InterActionEvent): void {
		this.expandComboxOnCheck(event);
	}

	/**
	 * 扩展下拉框的勾选框勾选|取消事件, 按勾选的顺序记录勾选的值({@link #autoSetEmbedreportParams})
	 * @param event 
	 * 
	 * @description 
	 * 1) 下拉框db初始化数据也会触发onRender事件, 但数据触发的onRender事件中，下拉框对象未创建
	 * 2) 根据getInnerComoponent返回值来判断当前是否为下拉框渲染
	 * 
	 * 3) 下拉框不会根据勾选的顺序记录选中值, 只是记录最终勾选的item
	 * 4) 给下拉框对象新增一个数组, 根据顺序记录勾选值, 取消则将其从数组删除
	 */
	private expandComboxOnCheck(event: InterActionEvent): void {
		let comp = event.component;
		let compId = comp?.getId() as string;
		if (compId != "combobox1") {
			return;
		}
		let outCombobox: Combobox = comp?.component?.getInnerComoponent();
		if (isEmpty(outCombobox)) {
			return;
		}
		/**
		 * 给下拉框对象新增一个数组属性, 记录勾选节点(按顺序)
		 * 注意: 
		 * 1) 判断是否已经添加了oncheck事件，避免无限嵌套执行oncheck事件
		 * 2) 勾选 | 取消 全选框不会触发此事件, 需要外部通过全选框DOM的class来判断
		 */
		if (isEmpty(outCombobox.checkedItemValues)) {
			outCombobox.getData().then((itemInfos) => { // 获取默认数据
				let checkedItemValues: string[] = [];
				for (let i = 0; i < itemInfos.length; i++) {
					let itemValue: string = itemInfos[i].value;
					checkedItemValues.push(itemValue);
				}
				outCombobox.checkedItemValues = checkedItemValues;
				outCombobox.oncheck = (e: SZEvent, combobox: Combobox, items: ListItem[]) => {
					/** 当前勾选|取消的节点值 */
					let checkedItem: ListItem = items[0];
					let checked = checkedItem.checked;
					let itemValue: string = checkedItem.getValue();
					if ((checked || checked == 'true') && !combobox.checkedItemValues.includes(itemValue)) {
						combobox.checkedItemValues.push(itemValue);
					} else {
						combobox.checkedItemValues.remove(itemValue);
					}
				}
			});
		}
	}

	/**
	 * 2022-11-30 tuzw
	 * 根据下拉框勾选顺序记录勾选值,并设置报表指定全局参数中
	 * 地址：https://jira.succez.com/browse/CSTM-21426
	 * 
	 * @param reportId 报表ID
	 * 
	 * @description 
	 * 1) SPG中报表组件，不需要再设置全局参数去渲染报表, 否则会导致SPG设置的值覆盖掉脚本set的参数值
	 * 1) 报表维度参数名提前固定为: WD+序号, eg: WD1
	 * 2) 下拉框ID需要和onRender一致, 脚本已写死为: combobox1
	 */
	private autoSetEmbedreportParams(event: InterActionEvent): void {
		let page = event.page;
		let params = event.params;
		let reportId = params?.reportId;
		if (isEmpty(reportId)) {
			showWarningMessage(`初始化参数未空`);
			return;
		}
		let reportComp = page.getComponent(reportId)?.component?.getInnerComoponent();
		if (isEmpty(reportComp)) {
			showWarningMessage(`报表不存在[${reportId}]`);
			return;
		}
		let combobox: Combobox = page.getComponent("combobox1")?.component?.getInnerComoponent();
		if (isEmpty(combobox)) {
			showWarningMessage(`下拉框不存在[combobox1]`);
			return;
		}
		let comboboxPanel = combobox.panel as ComboboxFloatPanel;
		let domCheckAll = comboboxPanel.domCheckAll;
		if (domCheckAll.classList.contains('icon-checkbox-ckd')) { // 判断是否勾选全选框
			combobox.checkedItemValues = combobox.getValue();
		}
		if (combobox.getValue().length == 0) { // 若当前勾选个数为0, 则清空记录的勾选值
			combobox.checkedItemValues = [];
		}
		/** 下拉框勾选值(按勾选顺序记录) */
		let comboboxValues: string[] = combobox.checkedItemValues;
		let report: Report = reportComp.getAnaObject();
		let reportData: ReportData = report.getData();
		let allParamNmaes: ParamInfo[] = reportData.getParameterNames();
		for (let i = 0; i < allParamNmaes.length; i++) {
			let paramName = allParamNmaes[i].name;
			if (paramName.indexOf("WD") == 0) { // 判断全局参数名开头是否为WD, 若是则清空其值
				reportData.setParameter(paramName, null);
			}
		}
		for (let i = 0; i < comboboxValues.length; i++) {
			let paramValue: string = comboboxValues[i];
			reportData.setParameter(`WD${i + 1}`, paramValue);
		}
	}
}