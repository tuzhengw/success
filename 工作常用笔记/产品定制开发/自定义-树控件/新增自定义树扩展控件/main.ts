/**
 * =====================================================
 * 作者：tuzhengw
 * 审核人员：liuyz
 * 创建日期: 2022-05-12
 * 功能描述
 * 		此脚本是数据中台定制树控件前端文件-新增右击菜单功能
 * =====================================================
 */
import "css!./main.css";
import { Dialog } from "commons/dialog";
import {
    showMenu, assign, Component, ComponentArgs, ctx, ctxIf, setIcon,
    showWarningMessage, isEmpty, message, rc, SZEvent, showFormDialog, uuid
} from "sys/sys";
import { Tree, TreeItem, List, ListItem } from "commons/tree";
import { Form } from "commons/form";
import { getDwTableDataManager } from "dw/dwapi";
import { getDwTableChangeEvent } from "dw/dwapi";
import { SpgTree, SpgTreeBuilder, SpgTreePptDataProvider } from "app/superpage/components/tables";
import { SpgComponentArgs } from "app/superpage/superpage";
import { AnaStyleConvertor } from "ana/builder";
import { MenuItemArgs } from "commons/menu";
import { AnaNodeData } from "ana/datamgr";

export class CustomTreeBuilder extends SpgTreeBuilder {

}

/** 新增右击菜单选项 */
enum ExpandMenuProperties {
    /** 是否隐藏右击点击菜单 */
    ShowRightOnClickMenu = "isDisplayRightOnClickMenu",
    /** 顶层节点是否禁用新增同级节点 */
    FirstParentIsDisplayAddLevelNode = "firstParentIsDisplayAddLevelNode",
    /** 是否右键选中节点，若为true，右键时，当前节点未选中则选择它，已选中则不改变选择，默认false。 */
    RightClickSelect = 'customTreeRightClickSelect',
    /** 树左侧图标 */
    RightIcon = 'prefixIcon',
    /** 顶层根节点允许删除, 默认: 允许 */
    RootNodeEnableDelete = 'rootNodeEnableDelete',
    /** 外部条件来控制是否可删除当前节点, 值为空则默认: true */
    TreeNodeDeleteStatus = 'treeNodeDeleteStatus'
}

/**
 * 自定义树控件
 * 20220512 tuzw
 * 1 在产品的树控件上增加右击点击菜单功能
 * 
 * 20220718 tuzw
 * 帖子：https://jira.succez.com/browse/CSTM-19648
 * 1 增加设置树控件行节点的左侧前缀图标样式
 * 
 * 20220720 tuzw
 * 跟踪帖子：https://jira.succez.com/browse/BI-45483
 * 1 产品树不支持设置右击点击交互，暂时还未解决
 * 
 * 20220824 tuzw
 * 研发已支持产品部实现节点左侧图标显示，将其定制图标脚本删除
 * 
 * 注意：
 * 1 若需要扩展，则在onRender内扩展
 * 2 spg的扩展类必须继承SpgComponent，但不一定要直接继承，可间接继承
 */
export class CustomTree extends SpgTree {

    /** 控件(或者子组件)的数据对象, 查询计算后的控件属性数据集合, eg: 获取编译后的表达式值 */
    private anaNodeData: AnaNodeData
    /** 当前右击点击的树节点 */
    private rightOnClickNode: TreeItem;
    /** 是否隐藏右击点击菜单 */
    private isDisplayRightOnClickMenu: boolean;
    /** 顶层节点是否禁用新增同级节点，默认：显示 */
    private firstParentIsDisplayAddLevelNode: boolean;
    /** 顶层根节点是否允许删除, 默认: 允许 */
    private rootNodeEnableDelete: boolean;

    /** 自定义删除事件，默认为：NULL */
    public onDeleteEvent?: (item: ListItem) => void;
    /** 自定义右击事件 */
    public onCustomTreeMenu?: (sze: SZEvent, list: List, item: ListItem) => void | boolean;

    protected _init(args: SpgComponentArgs): HTMLElement {
        let domBase = super._init(args);
        domBase.classList.add("sdi-custom-tree"); // 外层新增一个class，方便更改样式
        return domBase;
    }

    /**
     * 创建内部组件tree
     * @description 表达式值需编译, 从AnaNodeData对象获取编译后的值
     */
    protected createInnerComponent(): Tree {
        /** SPG构建对象, 可获取SPG组件的值 */
        let compBuilder = this.compBuilder;
        this.anaNodeData = this.getComponentData().getNodeData();

        this.setTreeProperty();
        let showCount = compBuilder.getProperty(PropertyNames.ShowCount);
        let enableSelect = compBuilder.getProperty(PropertyNames.CheckboxIconVisible);
        let rightClickSelect: boolean = compBuilder.getProperty(ExpandMenuProperties.RightClickSelect);
        let treeIconVisible: boolean = compBuilder.getProperty(ExpandMenuProperties.RightIcon);

        let tree = this.innerComponent = new this.treeClass({
            domParent: this.getDomBody(),
            placeholder: compBuilder.getProperty(PropertyNames.Placeholder),
            itemDataCountVisible: !!showCount,
            itemDescVisible: true,
            multipleSelect: !!compBuilder.getProperty(PropertyNames.Multiple),
            multipleCheckable: true,
            checkBoxVisible: !!enableSelect,
            selectable: true,
            clickToExpand: !!compBuilder.getProperty(PropertyNames.SelectLeaf),
            clickBlankClearSelected: true,
            onbeforeselection: (sze: SZEvent, list: List, item: ListItem) => {
                return this.doSelection(sze, list, item, ActionTriggerType.BeforeChange);
            },
            onselectionchange: this.doSelection.bind(this),
            onwillcheck: (sze: SZEvent, list: List, items: ListItem[]) => {
                return this.doCheck(sze, list, items, ActionTriggerType.BeforeChange);
            },
            iconVisible: treeIconVisible,
            oncheck: this.doCheck.bind(this),
            onexpand: () => {
                this.getParentComponent()?.doParentResize();
            },
            rightClickSelect: rightClickSelect,
            oncontextmenu: this.doTreeContextMenu.bind(this), // 仅增加一个右击点击事件
        });
        return tree;
    }

    /**
     * 读取SPG设置的自定义树设置属性
     */
    private setTreeProperty(): void {
        this.isDisplayRightOnClickMenu = this.compBuilder.getProperty(ExpandMenuProperties.ShowRightOnClickMenu);
        this.firstParentIsDisplayAddLevelNode = this.compBuilder.getProperty(ExpandMenuProperties.FirstParentIsDisplayAddLevelNode);
        this.rootNodeEnableDelete = this.compBuilder.getProperty(ExpandMenuProperties.RootNodeEnableDelete);
    }

    /**
     * 右击树 节点，弹出的可选菜单
     * @param treeItem 当前右击选中的节点
     */
    private doTreeContextMenu(sze: SZEvent, list: List, treeItem: TreeItem): void | boolean {
        if (!!this.isDisplayRightOnClickMenu) { // 校验是否开启右击菜单事件
            this.setRightOnclickTreeNode(treeItem);
            if (this.onCustomTreeMenu == null) { // 校验外部是否有自定义右击菜单事件
                if (treeItem != null) {
                    this.generatorDefaultRightNodeMenu(sze, treeItem);
                } else {
                    this.generatorRightTreeMenu(sze);
                }
            } else {
                this.onCustomTreeMenu(sze, list, treeItem);
            }
            return false; // 隐藏浏览器自带的右击菜单选项
        } else {
            if (this.onCustomTreeMenu != null) { // 树没有开启右击菜单事件，也校验外部是否提供自定义菜单事件
                return this.onCustomTreeMenu(sze, list, treeItem);
            }
        }
    }

    /**
     * 新增同级节点
     * @param treeItem 当前选择的节点
     */
    public addEqualTreeNode(event): void {
        this.showAdjustTreeNodeDialog({
            "optionType": "addEqualNode",
            "optionCaption": "新增同级节点"
        });
    }

    /**
     * 新增下级节点
     * @param treeItem 当前选择的节点
     */
    public addChildeTreeNode(event): void {
        this.showAdjustTreeNodeDialog({
            "optionType": "addChildeNode",
            "optionCaption": "新增下级节点"
        });
    }

    /**
     * 修改树节点
     * @param treeItem 当前选择的节点
     */
    public updateTreeNode(event): void {
        this.showAdjustTreeNodeDialog({
            "optionType": "updateNode",
            "optionCaption": "修改节点"
        });
    }

    /**
     * 删除树节点
     * @param treeItem 当前选择的节点
     */
    public removeTreeNode(event): void {
        /** 外部条件来控制是否可删除当前节点, 值为空则默认: true */
        let treeNodeDeleteStatus = this.anaNodeData.getProperty(ExpandMenuProperties.TreeNodeDeleteStatus); // 条件是动态的, 值动态取
        if (!treeNodeDeleteStatus) {
            showWarningMessage(`当前节点与其他模型数据存在关联, 不允许删除`);
            return;
        }
        this.saveDimItem({
            id: this.rightOnClickNode.getId(),
            modelPath: this.getDataProvider().dataProvider.dwTablePath,
            option: "delete"
        }).then((result) => {
            if (result.result && this.onDeleteEvent != null) { // 校验外部是否有指定自定义删除事件
                this.onDeleteEvent(this.rightOnClickNode);
            }
        });
    }

    /**
     * 记录当前右击点击的树节点
     * @param treeItem 当前右击选择的节点
     */
    public setRightOnclickTreeNode(treeItem: TreeItem): void {
        this.rightOnClickNode = treeItem;
    }

    /**
     * 获取当前右击选中的树节点
     */
    public getRightOnclickTreeNode(): TreeItem {
        return this.rightOnClickNode;
    }

    /**
     * 展示编辑节点信息对话框页面
     * @param optionCaption 当前操作标题，eg：新增同级节点
     * @param optionType 操作类型，eg：updateNode
     */
    public showAdjustTreeNodeDialog(args: {
        optionCaption: string,
        optionType: string
    }): void {
        let optionType: string = args.optionType;
        let dialogCaption: string = args.optionCaption;

        let ownerTree = this; // 保留tree对象
        showFormDialog({
            className: "adjustTreeNode",
            caption: dialogCaption,
            content: {
                captionWidth: 72,
                items: [{
                    id: "treeNodeId",
                    caption: "代码",
                    formItemType: 'edit',
                    required: true,
                    validRegex: ((inputText: string) => { // 获取当前输入框输入的值
                        let nodeIsExist: boolean = this.checkNodeIdIsExist(inputText, ownerTree);
                        if (nodeIsExist && optionType == "updateNode") { // 若节点存在，并且当前操作为修改节点，进一步校验当前ID是否有发生变化
                            if (!!this.rightOnClickNode && this.rightOnClickNode.getId() == inputText) {
                                return { result: true };
                            }
                        }
                        if (nodeIsExist) {
                            return { result: false, errorMessage: "节点已存在" }
                        }
                        return { result: true };
                    })
                }, {
                    id: "treeNodeCaption",
                    caption: "标题",
                    formItemType: 'edit',
                    required: true
                }]
            },
            onshow: (event: SZEvent, dialog: Dialog) => {
                let form = <Form>dialog.content;
                form.waitInit().then(() => {
                    let treeNodeIdComp = form.getFormItemComp('treeNodeId');
                    let treeNodeCaptionComp = form.getFormItemComp('treeNodeCaption');
                    if (optionType == "updateNode") { // 编辑节点，输入框显示当前选中内容
                        treeNodeIdComp.setValue(this.rightOnClickNode.getId());
                        treeNodeCaptionComp.setValue(this.rightOnClickNode.getCaption());
                    } else { // 新增节点，则给节点ID随机生成一个ID值
                        treeNodeIdComp.setValue(uuid());
                    }
                });
            },
            buttons: [{
                id: "ok",
                layoutTheme: "defbtn",
                onclick: (event: SZEvent, dialog: Dialog, item: any) => {
                    let form: Form = dialog.content;
                    this.treeNodeMenuFunction(optionType, form, this.rightOnClickNode);
                }
            }, 'cancel']
        });
    }

    /**
     * 校验当前节点 是否 存在树节点中
     * @param nodeId 校验的节点ID值
     * @param ownerTree 树对象本身，因为有时候脱离上下文，导致this丢失，这里根据情况传入
     * return true | false
     */
    public checkNodeIdIsExist(nodeId: string, ownerTree?: CustomTree): boolean {
        if (!!this) { // 校验this是否存在
            ownerTree = this;
        }
        // 格式为：001（id值）: DwTableDataItem
        let treeAllNode = ownerTree.getInnerComoponent().dataProvider.dataProvider.originalDwDataHierarcy.itemMap;
        /** 校验当前输入的节点ID是否存在 */
        let nodeIsExist: boolean = false;
        let treeNodeIds: string[] = Object.keys(treeAllNode);
        for (let i = 0; i < treeNodeIds.length; i++) {
            if (nodeId == treeNodeIds[i]) {
                nodeIsExist = true;
                break;
            }
        }
        return nodeIsExist;
    }

    /**
     * 右击弹出菜单功能实现、
     * 实现：直接将数据写入库，重新fetchData数据，渲染树
     * @param optionType 操作类型，eg：updateNode
     * @param form 右击菜单表单对象
     * @param rightChoseTreeNode 当前右击树节点对象
     */
    private treeNodeMenuFunction(optionType: string, form: Form, rightChoseTreeNode: TreeItem): void {
        let validate: boolean = form.validate();
        if (validate) {
            let data = form.getData();
            let treeNodeId: string = data['treeNodeId'];
            let treeNodeCaption: string = data['treeNodeCaption'];

            switch (optionType) {
                case "addEqualNode":  // 新增同级节点
                    let params = {
                        id: treeNodeId,
                        modelPath: this.getDataProvider().dataProvider.dwTablePath,
                        caption: treeNodeCaption,
                        option: "insert"
                    }
                    if (!!rightChoseTreeNode) { // 若在某节点下增加同级节点，则指定父节点ID
                        let nodeParent: TreeItem = rightChoseTreeNode.getParent();
                        if (!!nodeParent && !!nodeParent.getId()) {
                            params.parentId = nodeParent.getId();
                        }
                    }
                    this.saveDimItem(params);
                    break;
                case "addChildeNode": // 新增下级节点，当前节点为父节点
                    this.saveDimItem({
                        id: treeNodeId,
                        modelPath: this.getDataProvider().dataProvider.dwTablePath,
                        caption: treeNodeCaption,
                        option: "insert",
                        parentId: rightChoseTreeNode.getId()
                    });
                    break;
                case "updateNode": // 更新节点
                    this.saveDimItem({
                        id: treeNodeId,
                        modelPath: this.getDataProvider().dataProvider.dwTablePath,
                        caption: treeNodeCaption,
                        option: "update",
                        originalId: rightChoseTreeNode.getId()
                    });
                    break;
                default:
                    showWarningMessage(`当前操作不存在`);
            }
        }
    }

    /**
     * 调整树节点（新增、修改、删除）
     * @param id 维项代码（主键值）
     * @param modelPath 树绑定的模型路径
     * @param option 当前操作，delete insert update
     * @param caption 节点绑定的文字字段值
     * @param originalId 修改维项代码时原来的代码
     * @param parentId  父节点的ID，用于在某节点下新增子节点
     * @param timePoint  时间点，用于修改缓慢变化的维项
     * @param data 维项的数据
     */
    private saveDimItem(args: {
        id: string,
        modelPath: string;
        option: string;
        caption?: string;
        originalId?: string;
        parentId?: string;
        timePoint?: string;
        data?: {
            [fieldName: string]: string;
        }
    }): Promise<any> {
        return rc({
            url: '/sysdata/extensions/succ-dataVisualization-customTree/customTree.action?method=adjustTreeNode',
            data: args
        }).then((result) => {
            if (!result.result) {
                showWarningMessage(result.message);
                return result;
            }
            getDwTableDataManager().updateCache([getDwTableChangeEvent(args.modelPath, { updateAll: true })], true);
            return result;
        });
    }

    /**
     * 生成默认-右击节点-菜单
     * @param sze 
     * @param treeItem 当前右击的节点
     */
    public generatorDefaultRightNodeMenu(sze: SZEvent, treeItem: TreeItem): void {
        let menuItems: Array<MenuItemArgs> = [];
        if (treeItem.getLevel() != 0 || (treeItem.getLevel() == 0 && !this.firstParentIsDisplayAddLevelNode)) { // 校验最高级父节点是否显示新增同级节点
            menuItems.push({
                id: 'addEqualTreeNode',
                cmd: "addEqualTreeNode",
                caption: message("customTree.addLevelNode")
            });
        }
        menuItems.pushAll([{
            id: 'addChildeTreeNode',
            cmd: "addChildeTreeNode",
            caption: message("customTree.addChildrenNode")
        }, {
            id: 'updateTreeNode',
            cmd: "updateTreeNode",
            caption: message("customTree.updateTreeNode")
        }]);
        if (treeItem.getLevel() != 0 || (treeItem.getLevel() == 0 && this.rootNodeEnableDelete)) { // 顶层根节点是否允许删除
            menuItems.push({
                id: 'deleteNode',
                cmd: "removeTreeNode",
                caption: message("customTree.deleteTreeNode")
            });
        }
        showMenu({
            id: 'custom-tree-combobox-treeNode-menu',
            items: menuItems,
            showAt: <MouseEvent>sze.srcEvent,
            layoutTheme: "button-load",
            /** 命令状态提供者，提供菜单项的数据和处理点击事件，会执行item设置的cmd对应的方法，注意：id和cmd值一致 */
            commandProvider: this
        });
    }

    /**
     * 生成 默认右击-树-空白区域右击菜单
     * @param sze 
     */
    public generatorRightTreeMenu(sze: SZEvent): void {
        if (!this.firstParentIsDisplayAddLevelNode) { // 校验最高级父节点是否显示新增同级节点
            showMenu({
                id: 'custom-tree-combobox-tree-menu',
                items: [{
                    id: 'addEqualTreeNode',
                    cmd: "addEqualTreeNode",
                    caption: message("customTree.addLevelNode")
                }],
                showAt: <MouseEvent>sze.srcEvent,
                layoutTheme: "button-load",
                commandProvider: this
            });
        }
    }
}

/**
 * 由于main路径时当前main文件，pptDataProvider对应的类不存在于当前文件，这里继承原有的类，然后设置
 */
export class CustomSpgTreePptDataProvider extends SpgTreePptDataProvider {

}