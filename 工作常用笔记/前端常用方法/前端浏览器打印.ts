

/**
 * 浏览器打印指定区域DOM
 * @param printCompId 打印组件Id, 未指定默认打印整个页面
 * 默认布局 横向
 * <p>
 *  采用方式: DOM替换。
 *  步骤：
 *  1) 把要打印的内容放到一个新的div里，div宽高设置100%。
 *  2) position设为fixed或者absolute占满屏(把当前页面内容全部覆盖住), 若页面存在滚动, 则忽略此步。
 *  3) 调用window.print()。
 *  4）调用完成后, 将该div删除, 重新展示被覆盖的原页面。
 * </p>
 * <p>
 *  参考地址: http://www.qiutianaimeili.com/html/page/2019/11/dfvmp25rvz.html
 *  可使用插件: printJS
 *  printJS({ 
		printable: 'containerPrint', // 元素id,不支持多个
		type: "html",
		targetStyle: ['* '],
		targetStyles: ['*'],
		maxWidth: '', // 最大宽度，默认800,仅支持数字
		style: '@page{size:auto; margin: 0;}' + 
		'@media print { @page {size: landscape } }'// landscape  默认横向打印
		});
 * </p>
 */
private printSignCompDom(event: InterActionEvent): void {
	let params = event.params;
	let printCompId: string = params?.printCompId;
	if (isEmpty(printCompId)) {
		print();
		return;
	}
	let printDiv = document.createElement('div');
	let compHtml = document.getElementById(printCompId);
	if (!compHtml) {
		showWarningMessage(`打印控件[${printCompId}]不存在`);
		return;
	}
	printDiv.innerHTML = compHtml.innerHTML;
	// printDiv.style.position = 'fixed';
	printDiv.style.left = '0';
	printDiv.style.top = '0';
	printDiv.style.width = '2000px';
	printDiv.style.height = 'auto';
	printDiv.style.zIndex = '100000';
	printDiv.style.background = '#fff';
	printDiv.style.overflow = 'overflow';

	document.body.appendChild(printDiv);
	window.print()
	document.body.removeChild(printDiv);
}