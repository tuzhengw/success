import { ctx, SZEvent, Component, throwInfo, throwError, showConfirmDialog, showFormDialog, showErrorMessage, showMenu, 
	isEmpty, deepAssign, rc_task, uuid, message, quoteExpField, showDialog, showSuccessMessage, showProgressDialog, rc, 
	downloadFile, waitRender, showWaiting, assign, IDataProvider, formatDateFriendly, parseDate, formatDate, tryWait, rc_get, 
	showWarningMessage, timerv, showWaitingMessage, hideMessage, DataChangeEvent, EventListenerManager, GroupInfo, IFindArgs, 
	ServiceTaskPromise, ServiceTaskInfo, ServiceTaskState } from 'sys/sys';
import { showDrillPage, getMetaRepository, MetaFileViewer, getCurrentUser, getSysSettings } from 'metadata/metadata';
import { getDwTableDataManager, labelData, refreshModelState, exportQuery, importData, ImportDataResult, getQueryManager, pushData, importDbTables, DwDataChangeEvent, complementDataset } from 'dw/dwapi';
import { Dialog } from 'commons/dialog';

/**
 * CSTM-11820 导出数据字典，传入需要导出的模型表id，支持输入文件名称、选择文件类型（csv、xlsx、xls）
 * 地址：http://192.168.9.199:8080/sysdata/app/zt.app?:edit=true&:file=custom.ts
 * @params resids  导出表的id值
 */
function exportDataDictionary(resids: Array<string>): void {
	let dataDictionaryTablePath = "/sysdata/data/tables/sdi/data-asset/SDI_DATA_DICTIONARY.tbl";
	let exp = "\'" + resids.join("\',\'") + "\'";

	let exportFile = (fileName: string, format: DataFormat): Promise<void> => {
		return getMetaRepository().getFileBussinessObject<DwTableModelFieldProvider>(dataDictionaryTablePath).then(dwbo => {
			if (!dwbo) {
				showErrorMessage("数据字典模型表不存在");
				return;
			}
			let resid = dwbo.getFileInfo().id;
			let queryFields: QueryFieldInfo[] = dwbo.getFields().filter(field => field.dbfield != "ID").map(field => {
				// return {
					// name: field.name,
					// exp: `[t0].${quoteExpField(field.name)}`
				// }
				return { // 20220411  产品导出格式去掉了exp
					name: field.name
				}
			});
			let filters: FilterInfo[] = [
				{
					"matchAll": true,
					"enabled": true,
					"clauses": [{
						"exp": `t0.id in (${exp})`
					}]
				}];
			let queryInfo = {
				fields: queryFields,
				sources: [{
					id: "t0",
					path: dataDictionaryTablePath
				}],
				filter: filters,
				sort: [],
				options: {
					limit: null,
					offset: null,
					queryTotalRowCount: true
				}
			};

			return exportQuery({
				resid: resid,
				query: queryInfo,
				fileFormat: format,
				fileName: fileName
			})
		})
	}

	showFormDialog({
		id: 'spg.exportdatadictionary',
		caption: '导出数据字典',
		content: {
			items: [{
				id: 'fileName',
				caption: '文件名称',
				formItemType: 'edit',
				captionPosition: 'left'
			}, {
				id: 'format',
				caption: '文件类型',
				formItemType: 'selectpanel',
				captionPosition: 'left',
				compArgs: {
					items: [{
						value: 'xlsx',
						caption: 'xlsx'
					}, {
						value: 'xls',
						caption: 'xls'
					}, {
						value: 'csv',
						caption: 'csv'
					}]
				}
			}],
		},
		onshow: (event: SZEvent, dialog: Dialog) => {
			let form = <Form>dialog.content;
			form.loadData({ fileName: '数据字典', format: 'xlsx' });
		},
		buttons: [{
			id: "ok",
			layoutTheme: "defbtn",
			onclick: (event: SZEvent, dialog: Dialog, item: any) => {
				let data = (<any>dialog).form.getData();
				let format = data.format;
				let fileName = data.fileName;
				let range = data.range;
				dialog.waitingClose(exportFile(fileName, format));
			}
		}, 'cancel']
	});
}