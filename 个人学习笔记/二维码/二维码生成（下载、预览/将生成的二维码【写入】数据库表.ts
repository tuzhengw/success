/**
 * 批处理脚本模版
 */
function main() {
    batchUpdateQrCodePath();
}
import BufferedImage = java.awt.image.BufferedImage;
import File_ = java.nio.file.File;
import ImageIO = javax.imageio.ImageIO;
import BarcodeFormat = com.google.zxing.BarcodeFormat;
import EncodeHintType = com.google.zxing.EncodeHintType;
import MultiFormatWriter = com.google.zxing.MultiFormatWriter;
import BitMatrix = com.google.zxing.common.BitMatrix;
import ErrorCorrectionLevel = com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import ByteArrayInputStream = java.io.ByteArrayInputStream;
import ByteArrayOutputStream = java.io.ByteArrayOutputStream;

import { getDataSource } from "svr-api/db"; //  数据库相关API

const DATASOURCE = "zhjg_vtc";  //  指定数据源
let ds = getDataSource(DATASOURCE);
const DW_JGC_WAREHOUSINGOUT = "dw_jgc_warehousingout_copy";
const DW_JGC_WAREHOSEINGOUT_SCHEM = "epidemic_prevention";
const alDs = getDataSource("JZJGC");

/**
 * 批量更新
 * （1）二维码存储路径：qrCodePath
 * （2）二维码生成时间：qrCodeCreateTime
 */
function batchUpdateQrCodePath(): void {
    let certificateNos: DW_JGC_WAREHOUSINGOUT[] = getCertificateNo();
    if (certificateNos.length > 0) {
        for (let i = 0; i < certificateNos.length; i++) {
            let certificateNo: string = certificateNos[i].CERTIFICATENO;
            if (!!certificateNo) {
                updateOneWarehouseOutQrcode(certificateNos[i].WAREHOUSEOUTNO, certificateNo);
            }
        }
    }
}

/** 出仓管理表描述 */
interface DW_JGC_WAREHOUSINGOUT {
    /** 国家出仓报告备案号 */
    WAREHOUSEOUTNO: string;
    /** 检验检疫证号 */
    CERTIFICATENO: string;
    [propname: string]: any;
}

/**
 * 获取出仓管理-备案号
 */
function getCertificateNo(): DW_JGC_WAREHOUSINGOUT[] {
    let conn = alDs.getConnection();
    let queryData: DW_JGC_WAREHOUSINGOUT[] = [];
    try {
        let table = alDs.openTableData(DW_JGC_WAREHOUSINGOUT, DW_JGC_WAREHOSEINGOUT_SCHEM, conn);
        queryData = table.select(["WAREHOUSEOUTNO", "CERTIFICATENO"]) as DW_JGC_WAREHOUSINGOUT[];
    } catch (e) {
        console.error(`获取出仓管理-国家出仓报告备案号：error`);
        console.error(e);
    } finally {
        conn.close();
    }
    return queryData;
}

/** 服务器地址 */
const SERVER_URL = "https://datapid.cn/cc/";
/**
 * 通过（出仓管理-备案号）生成链接，并转换为二维码，将其字节流写入数据库表字段
 * @param warehouseOutNo 出仓管理-备案号 
 * @param certificateNo 检验检疫证号
 * @return 
 */
function updateOneWarehouseOutQrcode(warehouseOutNo: string, certificateNo: string): void {
    if (!warehouseOutNo || !certificateNo) {
        console.debug(`updateOneWarehouseOutQrcode()， 参数不正确，出仓管理-备案号：${warehouseOutNo}，检验检疫证号：${certificateNo}`);
        return;
    }
    let conn = alDs.getConnection();
    let preparestatement = null;
    try {
        conn.setAutoCommit(false);
        let requestUrl = `${SERVER_URL}${warehouseOutNo}`;
        let filtBytes: number[] = qRCodeUtils.generateQRCode(requestUrl); // 生成二维码
        let updateSQL: string = `update ${DW_JGC_WAREHOSEINGOUT_SCHEM}.${DW_JGC_WAREHOUSINGOUT} set QRCODEPATH=?, QRCODECREATETIME=? where CERTIFICATENO=?`; // 锁行
        preparestatement = conn.prepareStatement(updateSQL);

        let bytesBlob = conn.createBlob();
        preparestatement.setBlob(1, bytesBlob);
        preparestatement.setBytes(1, filtBytes);

        preparestatement.setTimestamp(2, new java.sql.Timestamp(new Date().getTime()));
        preparestatement.setString(3, certificateNo);
        let isUpdateSuccess: number = preparestatement.executeUpdate();
        if (isUpdateSuccess) {
            console.debug(`出仓管理-备案号：${warehouseOutNo}所生成二维码的字节流【成功】写入数据库表字段`);
        } else {
            console.debug(`出仓管理-备案号：${warehouseOutNo}所生成二维码的字节流【失败】写入数据库表字段`);
        }
        conn.commit();
    } catch (e) {
        console.error(`出仓管理-备案号：${warehouseOutNo}所生成二维码的字节流【失败】写入数据库表字段`);
        console.error(e);
    } finally {
        if (preparestatement != null) {
            preparestatement.close();
        }
        conn && conn.close();
    }
}

/**
 * 二维码工具类
 */
class QRCodeUtils {
    /** 图案的颜色，默认：0xFF000000   */
    private BLACK: string = "0xFF000000";
    /** 背景色，默认：0xFFFFFFFF */
    private WHITE: string = "0xFFFFFFFF";
    /** 二维码宽度，默认：30 */
    private DEFAULT_WIDTH: number = 300;
    /** 二维码高度，默认：300 */
    private DEFAULT_HEIGHT: number = 300;
    /** 二维码图片格式，默认：png */
    private DEFAULT_TYPE: string = "png";

    /**
     * 生成二维码，并返回对应的二进制流
     * @param codePath 二维码内容链接
     * @param width 二维码宽
     * @param height 二维码高
     * @param type 二维码图片类型，eg："jpg"
     * @return 二进制流数组（无Base64加密）
     */
    public generateQRCode(codePath: string, width?: number, height?: number, codeType?: string): number[] {
        if (!codePath) {
            console.debug(`generateQRCode()，生成二维码，并返回对应的二进制流：生成二维码的链接不能为NULL`);
            return [];
        }
        width = (!!width && width > 0) ? width : this.DEFAULT_WIDTH;
        height = (!!height && height > 0) ? height : this.DEFAULT_HEIGHT;
        codeType = !!codeType ? codeType : this.DEFAULT_TYPE;
        try {
            let bitMatrix: BitMatrix = this.getBitMarix(codePath, width, height); // 生成二维码 
            let image: BufferedImage = this.toBufferedImage(bitMatrix);
            let out = new ByteArrayOutputStream();
            ImageIO.write(image, codeType, out);
            let bytes: number[] = out.toByteArray();
            return bytes;
        } catch (e) {
            console.error(`generateQRCode()，生成二维码，并返回对应的二进制流：error`);
            console.error(e);
        }
    }

    /**
     * 将文件二进制流转换为图片
     * @param bytes 文件二进制流
     * @param fileSaveAbsolutePath 文件的绝对路径（包含：文件类型）
     * @param imageType 图片类型，eg：jpg、png、gif
     * 参考：https://www.cnblogs.com/oumyye/p/4306219.html
     */
    public fileBufferToImage(bytes: number[], fileSaveAbsolutePath: string, imageType: string): void {
        if (!bytes || !fileSaveAbsolutePath || !imageType) {
            console.debug(`fileBufferToImage()，将文件二进制流转换为图片：参数存在不合法`);
            return;
        }
        try {
            let bais = new ByteArrayInputStream(bytes);
            let image: BufferedImage = ImageIO.read(bais);
            let file = new File_(fileSaveAbsolutePath);
            ImageIO.write(image, imageType, file);
        } catch (e) {
            console.error(`fileBufferToImage()，将文件二进制流转换为图片：error`);
            console.error(e);
        }
    }

    /**
     * 设置二维码信息
     * @param content 二维码内容链接
	 * @param width 图片宽度
	 * @param height 图片高度
     * @return BitMatrix对象
     */
    private getBitMarix(codePath: string, width: number, height: number): BitMatrix {
        let hints = new java.util.Hashtable();
        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);// 指定纠错等级，纠错级别（L 7%、M 15%、Q 25%、H 30%）  
        hints.put(EncodeHintType.CHARACTER_SET, "utf-8");// 内容所使用字符集编码  
        hints.put(EncodeHintType.MARGIN, 1);//  设置二维码边的空度，非负数  
        width = width <= 0 ? this.DEFAULT_WIDTH : width;
        height = height <= 0 ? this.DEFAULT_HEIGHT : height;
        let bitMatrix: BitMatrix = new MultiFormatWriter().encode(codePath, BarcodeFormat.QR_CODE, width, height, hints);
        return bitMatrix;
    }

    /**
     * 将二维码信息写入文件
	 * @param matrix 二维码信息
	 * @param format 文件格式
	 * @param file 文件对象
     * @return 图片流
     */
    private writeToFile(bitMatrix: BitMatrix, format: string, file: File_): void {
        let image: BufferedImage = this.toBufferedImage(bitMatrix);
        if (!ImageIO.write(image, format, file)) {
            console.debug(`生成二维码图片失败`);
            throw new Error("生成二维码图片失败");
            return;
        }
        console.debug(`生成二维码图片成功`);
    }

    /**
     * 将二维码对象信息生成图片
     * @param bitMatrix 二维码信息对象
     * @return 二维码转换后的图片流
     */
    private toBufferedImage(bitMatrix: BitMatrix): BufferedImage {
        let width: number = bitMatrix.getWidth();
        let height: number = bitMatrix.getHeight();
        let image: BufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        for (let x = 0; x < width; x++) {
            for (let y = 0; y < height; y++) {
                image.setRGB(x, y, (bitMatrix.get(x, y) ? this.BLACK : this.WHITE));
            }
        }
        return image;
    }

    /**
     * 将二维码信息写入输出流
     * @param matrix 二维码信息
	 * @param format 图片格式，eg：png
	 * @param stream 输出流
     */
    private writeToStream(matrix: BitMatrix, format: string, stream: OutputStream): void {
        let image: BufferedImage = this.toBufferedImage(matrix);
        if (!ImageIO.write(image, format, stream)) {
            console.debug(`生成二维码图片失败`);
            throw new Error("生成二维码图片失败");
            return;
        }
        console.debug(`生成二维码图片成功`);
    }
}
/** 全局二维码工具对象 */
const qRCodeUtils = new QRCodeUtils();