
load("JHTJ:/collections/resources/sendMsg.action");
import HttpClients = org.apache.http.impl.client.HttpClients;
import HttpPost = org.apache.http.client.methods.HttpPost;
import StringEntity = org.apache.http.entity.StringEntity;
import ContentType = org.apache.http.entity.ContentType;
import EntityUtils = org.apache.http.util.EntityUtils;
import LocalDateTime = java.time.LocalDateTime;
import Duration = java.time.Duration;
import Timestamp = java.sql.Timestamp;
import SimpleDateFormat = java.text.SimpleDateFormat;
import Types = java.sql.Types;

const RequestConfig = Java.type("org.apache.http.client.config.RequestConfig");
const PoolingHttpClientConnectionManager = org.apache.http.impl.conn.PoolingHttpClientConnectionManager;

// ----------------------------------------------------------------------------------------------
// http://2.20.103.234:8080/zhjg/ZHJG/tasks

/**
 * 20210302 zhangd 最新短信服务地址
 * let url = "http://sms.shetuan365.cn:88/services/smsService";
 */
const SmsServiceURL = "http://2.22.67.1:88/SmsService";

/**
 * 脚本主入口
 */
function main() {
    sendMedicMessage();
}

/**
 * 发送短信信息
 */
function sendMedicMessage(): void {
    print(`start----开始执行发送短信脚本`);
    let startTime: number = Date.now();
    let sendMaxNums: number = getSendMaxNums();
    let sendCount: number = Math.ceil(sendMaxNums / ONCE_SEND_MAX_NUMS);
    if (sendCount > 10) {
        print(`当前推送次数[${sendCount}], 超过推送限制将其重置为10(对方要求每次最多推送1w条数据)`);
        sendCount = 10;
    }
    let recordErrorIds: string[] = [];
    print(`当前共需要处理：${sendCount} 次，每次处理：${ONCE_SEND_MAX_NUMS} 条数据`);
    /**
     * 连接池管理对象
	 * 场景：短时间内请求大量的http请求
     * 改成连接池的方式来管理请求, 避免打开的客户端过多无法释放资源(关闭也无法真正关闭, 必须借助连接池)。
     * 产品本身的http方法内部已支持连接池管理。
	 * 参考地址：https://jira.succez.com/browse/CSTM-22792
     */
    let connectionManager = new PoolingHttpClientConnectionManager();
    let httpclient;
    try {
		/* 
		 在Apache HttpComponents的HttpClients实现中，不同的URL可以共享同一个HttpClient实例。这是因为HttpClient实例本身是线程安全的，
		 并且可以重复使用来处理多个请求。通过重复使用同一个HttpClient实例，可以避免创建新的连接和重新验证身份等开销，
		 从而提高应用程序的性能和效率。
		*/
		httpclient = HttpClients.custom().setConnectionManager(connectionManager).build(); 
		
        for (let queryIndex = 0; queryIndex < sendCount; queryIndex++) {
            let isStop: string = getString(isStopExecuteFilePath);
            if (isStop == "true") {
                print(`手动停止发送`);
                break;
            }
            print(`${queryIndex + 1}) 开始处理第 ${queryIndex + 1} 次短信数据`);
            let sendMessageContents: MESSAGES_SMS_TABLE_NEW[] = getSendMessages(queryIndex);
            for (let i = 0; i < sendMessageContents.length; i++) {
                let isStop: string = getString(isStopExecuteFilePath);
                if (isStop == "true") {
                    print(`手动停止发送`);
                    break;
                }
                if (i % 100 == 0) {
                    print(`当前正在发送第[${i + 1}]条短信消息`);
                }
                let messageContent: MESSAGES_SMS_TABLE_NEW = sendMessageContents[i];
                let rs = sendSMS(httpclient, [messageContent.PHONES], messageContent.SMS_CONTENT);
                if (!rs.result) {
                    print(`第[${i + 1}]条短信发送失败, 内容：`);
                    recordErrorIds.push(messageContent.SMS_ID);
                    print(messageContent);
                    continue;
                }
                let returnMessage: string = rs.returnMessage;
                let sIdx = returnMessage.indexOf("<return>");
                let e = returnMessage.indexOf("</return>")
                let mess = returnMessage.substring(sIdx + 8, e);
                writeLog(messageContent.SMS_ID, mess.split(",")[1], mess.split(",")[0]);
            }
        }
    } catch (e) {
        print(`发送短信失败[${e.toString()}]`);
        print(e);
    } finally {
        httpclient && httpclient.close();
        connectionManager && connectionManager.close();
    }
    if (recordErrorIds.length != 0) {
        print(`当前推送失败条数为：${recordErrorIds.length} 个，ID为：`);
        print(recordErrorIds);
    }
    print(`end----发送短信脚本执行结束，总耗时：${(Date.now() - startTime) / 1000} s`);
}


/**
 * 发送短信
 * @param phones 手机号
 * @param messageInfo 消息主体，可能是一个json，也可能是一个 string，调用 sys 模块的发送短信接口 `sendSMS` 时传递的参数是什么，这里就是什么
 * @returns 返回短信服务商服务器发送短信后的返回数据，数据将会被记录到日志表中，为方便调试以及定位错误，请务必要设置返回值
 */
function sendSMS(
    httpclient,
    phones: string[],
    messageInfo: JSONObject | string
): { result: boolean, returnMessage: string } {
    let title = `应用支撑平台验证码：`;
    let xmlStr = getXMLStr(phones, messageInfo);
    let sendResult = postXml(httpclient, xmlStr);
    return { result: sendResult.result, returnMessage: sendResult.returnMessage };
}

/**
 * httpPost方式请求webService
 * @param json 短信内容
 * @retrn
 */
function postXml(httpclient, json: string): { result: boolean, returnMessage: string } {
    let httpPost = new HttpPost(SmsServiceURL);

    let requestConfig = RequestConfig.custom().setConnectionRequestTimeout(1000)
        .setSocketTimeout(3000).setConnectTimeout(1000).build();
    httpPost.setConfig(requestConfig);

    let returnMessage = "";
    let result: boolean = true;
    httpPost.setHeader("Content-Type", "text/xml;charset=utf-8");
    httpPost.setHeader("SOAPAction", "");
    httpPost.setHeader("User-Agent", "Apache-HttpClient/4.1.1 (java 1.5)");
    httpPost.setHeader("Accept-Encoding", " gzip,deflate");

    let s = new StringEntity(json.toString(), 'utf-8');
    httpPost.setEntity(s);
    let res;
    try {
        res = httpclient.execute(httpPost);
        let status = res.getStatusLine();
        if (status.getStatusCode() == 200) {
            /**
             * <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
             * <soap:Body><ns2:sendSmsResponse xmlns:ns2="http://client.webservices.kaiyun.com/">
             * <return>true,68195fab0c9b4cbe9ef8083e1f6e0858</return>
             * </ns2:sendSmsResponse></soap:Body></soap:Envelope>
             */
            returnMessage = EntityUtils.toString(res.getEntity(), 'utf-8');
        }
    } catch (e) {
        print(` httpPost方式请求webService失败[${e.toString()}]`);
        returnMessage = e.toString();
        result = false;
        print(e);
    } finally {
        res && res.close();
    }
    return { result: result, returnMessage: returnMessage };
}

// ----------------------------------------------------------------------------------------------------------------

/**
 * 产品没法设置请求体为GBK编码，会使得接收到的短信乱码
 */
function post(url: string, data: Record<string, string>): { content: string; statusCode: number; } {
    const client = HttpClients.createDefault();
    try {
        const post = new HttpPost(url);
		
		let requestConfig = RequestConfig.custom().setConnectionRequestTimeout(1000)
        .setSocketTimeout(3000).setConnectTimeout(1000).build();
		
		post.setConfig(requestConfig);
		
		
        const keys = Object.keys(data);
        const params: BasicNameValuePair[] = [];
        for (const key of keys) {
            params.push(new BasicNameValuePair(key, data[key]));
        }
        post.setEntity(new UrlEncodedFormEntity(params, "GBK"));
        const response = client.execute(post);
        try {
            const statusCode = response.getStatusLine().getStatusCode();
            const entity = response.getEntity();
            const content = EntityUtils.toString(entity);
            return { statusCode, content };
        } finally {
            response && response.close();
        }
    } finally {
        client && client.close();
    }
}


// -----------------------------------------------------------------------------------------------
import HttpClients = org.apache.http.impl.client.HttpClients;
import HttpPost = org.apache.http.client.methods.HttpPost;
import ArrayList = java.util.ArrayList;
import NameValuePair = org.apache.http.NameValuePair;
import BasicNameValuePair = org.apache.http.message.BasicNameValuePair;
import UrlEncodedFormEntity = org.apache.http.client.entity.UrlEncodedFormEntity;
import EntityUtils = org.apache.http.util.EntityUtils;

function post(url: string, params: { [key: string]: string; }, headers?: { [key: string]: string; }): string {
    const httpPost = new HttpPost(url);
    const postParams = new ArrayList<NameValuePair>();
    const keys = Object.keys(params);
    for (const key of keys) {
        const p = new BasicNameValuePair(key, params[key]);
        postParams.add(p);
    }
    if (headers) {
        const headerKeys = Object.keys(headers);
        for (const header of headerKeys) {
            httpPost.setHeader(header, headers[header]);
        }
    }
    const formEntity = new UrlEncodedFormEntity(postParams, "GBK");
    httpPost.setEntity(formEntity);
    const client = HttpClients.createDefault();
    try {
        const res = client.execute(httpPost);
        try {
            const entity = res.getEntity();
            return EntityUtils.toString(entity, "GBK");
        } finally {
            res?.close();
        }
    } finally {
        client?.close();
    }
}