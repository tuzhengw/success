

/**
 * 触发产品的设置参数交互
 */
private setParamAction() {
	// this.customTree：树控件，通过getComponent获取
	let actionMgr = this.customTree.renderer.getActionManager();
	let compId: string = this.customTree.getId();
	actionMgr.triggerActions({
		compId: compId,
		triggerType: ActionTriggerType.Click,
		value: [{ // 设置参数交互
			id: compId,
			value: this.rightOnClickNode.getId(),
			txt: "当前右击选中的节点ID"
		}],
		valueChange: true
	});
}