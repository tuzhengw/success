import { clone, deepEqual, isEmpty, ready, ServiceTaskPromise } from "sys/sys";
import { DwTableBuilder, DwTableDataFlowBuilder, DwTableDataFlowNodeBuilder, DwTableFieldsBuilder, MoveDataFlowNodeType } from "dw/dwbuilder";
import { inputData, saveModel } from "dw/dwapi";


export function test(name: string) {
    return ready().then(() => {
        return createDataFlow();
    });
}

/** 
 * 创建数据加工
 */
export async function createDataFlow() {
    let dwTableBuilder = new DwTableBuilder({
        modelDataType: ModelDataType.DataFlow,
        projectName: "sdi"
    });;

    let customDwTableBuilder = new CustomDwTableBuilder(dwTableBuilder, "sdi");
    let startNodeAlias: string = customDwTableBuilder.addNodeDbTable("DX_TUZW", "testdb", "_dim_pc");
    let startNode: DwTableDataFlowNodeBuilder = customDwTableBuilder.getNodeByAlias(startNodeAlias); // 通过节点别名获取节点对象
    startNode.setProperty("devQueryFixedRows", true);
    startNode.setProperty("checkRows", true);
    startNode.setProperty("checkRowCount", 1);
    startNode.setProperty("devFixedRowCount", 100000);
    startNode.setProperty("checkFields", true);
    startNode.setProperty("checkFreshData", false);

    /** 列加工节点别名 */
    let columnWorkAlias = customDwTableBuilder.addNodeSelect(startNodeAlias, "列加工");
    if (isEmpty(columnWorkAlias)) {
        return JSON.stringify({ result: false, message: "起始节点不存在" });
    }
    customDwTableBuilder.addStepCalcField(columnWorkAlias, {
        name: "DEFAULT_SYS_DATASOURCE",
        exp: "'default'",
        dataType: FieldDataType.C
    });
    customDwTableBuilder.addStepCalcField(columnWorkAlias, {
        name: "DEFAULT_SYS_UPDATE_TIME",
        exp: "NOW()",
        dataType: FieldDataType.P
    });

    let distinctAlias: string = "去重";
    customDwTableBuilder.addNodeDistinct(columnWorkAlias, distinctAlias);
    customDwTableBuilder.setDistinctFields(distinctAlias, ["ID_"]); // 可获取节点信息, 调用setProperty方法更改属性值

    customDwTableBuilder.addNodeOutput(distinctAlias, "输出节点");

    customDwTableBuilder.setDbTableInfo("DX_TUZW", "testdb", "_dim_pc_dataflow");
    dwTableBuilder.setProperty("rollbackEnabled", false);
    dwTableBuilder.setProperty("rebuildIndexAndKey", false);
    dwTableBuilder.setProperty("primaryKeys", ["ID_"]); // 此处设置主键字段描述值, 无描述则主键物理字段名

    await customDwTableBuilder.saveFile(`TEST_DATAFLOW.tbl`, "/sdi/data/tables/test");
}


/**
 * 定制扩展数据加工构造类
 * <p>
 *  1 此类方法都是迁移DwTableBuilderTest类方法
 *  2 后端调用的前端文件中不能引入: DwTableBuilderTest类, 因为此类涉及到前端页面的操作, 后端调用找不到页面DOM信息
 * </p>
 */
export class CustomDwTableBuilder {

    /** 管理和维护一个数据表对象 */
    private builder: DwTableBuilder;
    /** 记录加工所有节点信息 */
    private nodes: { [nodeId: string]: DwTableDataFlowNodeBuilder };
    /** 数据加工流程图的Builder对象 */
    private flowBuilder: DwTableDataFlowBuilder;

    constructor(builder?: DwTableBuilder, projectName?: string, ignoreDevProp?: boolean) {
        if (builder) {
            this.builder = builder;
        } else {
            projectName = projectName || "sdi"
            this.builder = new DwTableBuilder({
                modelDataType: ModelDataType.DataFlow,
                projectName: projectName
            });
        }
        this.builder.ignoreDevProp = !!ignoreDevProp; // 测试用例里默认只查询调试数据
        this.flowBuilder = this.builder.getDataFlow();
        this.nodes = this.flowBuilder.getNodes();
    }

    /**
     * 从左侧树上拖入一个数据表到绘图区域
     * @param preNodeAlias 前序节点别名，添加到SQL\脚本节点后
     */
    public addNodeDbTable(datasource: string, dbSchema: string, tableName: string, preNodeAlias?: string): string {
        let node = this.flowBuilder.addNode({
            type: DataFlowNodeType.DbTable,
            datasource: datasource,
            dbSchema: dbSchema,
            dbTableName: tableName,
            alias: tableName
        })
        this.nodes[node.getId()] = node;
        if (preNodeAlias) {
            let preNode = this.getNodeByAlias(preNodeAlias);
            preNode.linkTo(node.getId());
        }
        return node.getAlias();
    }

    /**
     * 从左侧树上拖入一个模型表到绘图区域
     */
    public addNodeModelTable(path: string, alias?: string): string {
        let node = this.flowBuilder.addNode({
            type: "ModelTable",
            alias: alias,
            moduleTablePath: path,
        });
        this.nodes[node.getId()] = node;
        return node.getAlias();
    }

    public hideFields(alias: string, fields: Array<string>): Promise<void> {
        return this.getFieldsBuilder(alias).then(fieldsBuilder => {
            fields.forEach(name => {
                let field = fieldsBuilder.getField(name);
                field && field.setProperty("hidden", true);
            })
        });
    }

    public showFields(alias: string, fields: Array<string>): Promise<void> {
        return this.getFieldsBuilder(alias).then(fieldsBuilder => {
            let allFields = fieldsBuilder.getFields();
            allFields.forEach(f => {
                if (fields.indexOf(f.getName()) == -1) {
                    f.setProperty("hidden", true);
                }
            })
        });
    }

    /**
     * 获取模型或节点的fieldsBuilder对象
     */
    public getFieldsBuilder(alias?: string): Promise<DwTableFieldsBuilder> {
        return this.builder.waitReady().then(() => {
            let node = alias && this.getNodeByAlias(alias);
            if (node) {
                return <Promise<DwTableFieldsBuilder>>node.getFields();
            }
            return this.builder.getFields()
        });
    }

    public getNodeByAlias(alias: string): DwTableDataFlowNodeBuilder {
        let nodes = this.nodes;
        let keys = Object.keys(nodes);
        let key = keys.find(key => nodes[key].getAlias() === alias);
        if (key) {
            return nodes[key];
        } else {
            return null;
        }
    }

    /**
     * 保存加工
     * @param name? 保存的元数据名称,可选，若传入该参数，表示为新建，否则为修改，默认赋值为上一次打开的文件名
     * @param ppath? 保存元数据的父路径, eg: /sdi
     * @param desc? 
     * @param synsInfo?
     * @param newCreate 是否新建数据加工, 默认为true
     */
    public async saveFile(name: string, ppath: string, desc?: string, syncInfo?: DwTableSyncToDbInfo, newCreate?: boolean): MetaFileInfo {
        if (!isEmpty(syncInfo)) {
            await this.builder.getFields().loadSyncInfo();
        }
        if (isEmpty(newCreate)) {
            newCreate = true;
        }
        await this.builder.waitReady(); // 等待节点ready，只有默认节点ready后，整个模型的输出字段才能确定下来
        let dataFlow: MetaFileInfo = await this.save(name, ppath, desc, syncInfo, newCreate);
        return dataFlow;
    }

    /**
     * 保存用户所做的修改
     * <p>
     *  1 dwTableBuilder的save方法内部会打开进度条对话框, 后端调用前端方法创建是需要进度条对话框
     *  2 重写save方法内部实现
     * </p>
     * @param name 保存的元数据名称,可选，若传入该参数，表示为新建，否则为修改，默认赋值为上一次打开的文件名
     * @param ppath 保存元数据的父路径
     * @param syncInfo 设置需要往物理表同步的差异信息，为空时，默认全量同步
     * @param newCreate 是否新建数据加工
     */
    private save(name: string, ppath: string, desc?: string, syncInfo?: DwTableSyncToDbInfo, newCreate?: boolean): Promise<any> {
        let dataFlow = this.builder.getDataFlow();
        let beforeSavePromise: Promise<any> = Promise.resolve();
        if (dataFlow && dataFlow.getOutputNode() && dataFlow.getUpdateBeforeSave()) {
            let outputNode = this.builder.getDataFlow().getOutputNode();
            beforeSavePromise = outputNode.queryData({ forceQuery: true }).then(() => {
                outputNode.refreshNodeInfo();
                return outputNode.refreshPreNodeFields();
            });
        }
        // 开始前刷新保存按钮的状态，保存过程可能很长，避免这段时间用户继续点击保存
        this.builder.refreshCommands("saveFile", true);
        // 未提取时，SQL模型数据源与SQL节点保持一致。
        let modelType = this.builder.getModelDataType();
        let extractDataEnabled = this.builder.getProperty("extractMethod");
        if (modelType == ModelDataType.Sql && !extractDataEnabled) {
            let sqlNode = this.builder.getDataFlow().getOutputNode().getInputNodes()[0];
            if (sqlNode) {
                let sqlNodeInfo = sqlNode.getNodeInfo();
                this.builder.setPropertyQuietly("dbSchema", sqlNodeInfo.dbSchema);
                this.builder.setPropertyQuietly("datasource", sqlNodeInfo.datasource);
            }
        }
        return beforeSavePromise.then(() => {
            let meta = this.builderSaveTableMetaData(name, ppath, desc, syncInfo, newCreate);
            let savePromise: ServiceTaskPromise<DwTableSaveResultInfo>;
            if (this.builder.isInputData()) {
                // 录入数据时，需要保存元数据和模型表，并向模型表导入数据
                let dataEditMgr = this.builder.dataEditMgr;
                let dataPackage = dataEditMgr.getInputDataPackage();
                let saveArgs: SaveInputDataArgs = {
                    data: dataPackage,
                    meta: meta
                };
                savePromise = inputData(saveArgs);
            }
            else {
                savePromise = saveModel(meta);
            }
            return savePromise;
        });
    }

    /**
     * 构建请求参数 @see save-保存数据表元数据.md
     */
    private builderSaveTableMetaData(
        name: string,
        ppath: string,
        desc?: string,
        syncInfo?: DwTableSyncToDbInfo,
        newCreate?: boolean
    ): SaveModelArgs {
        let fileInfo = this.builder.fileInfo;
        /**
         * 无论是部分同步还是全量同步，当元数据有错误时只更新元数据，不同步物理表。
         * 前端判断物理字段无差异时，设置syncDbTable=false，否则后台还会再执行一遍同步检查，且前后端差异判断逻辑不一致。
         */
        let syncFieldsInfo: any = syncInfo?.syncDbFields;
        if (syncFieldsInfo) {
            syncInfo.syncDbFields = this.builder.getFilterSyncInfos(syncFieldsInfo);
        }
        let syncDbTable = false;
        syncDbTable = !this.builder.hasError() && (!!newCreate || !syncInfo || syncInfo.syncTable);
        let renameFields = this.builder.getRenameFields();
        let content = this.builder.toJSON(ppath, true);
        let meta: SaveModelArgs = {
            name: name,
            ppath: ppath,
            newborn: !!newCreate,
            props: {
                desc: desc,
                syncDbTable: syncDbTable,
                dwSyncInfos: syncInfo,
                renameFields: renameFields,
                scriptInfo: this.builder.getScriptInfo()
            },
            content,
            revision: fileInfo && fileInfo.revision // 为了乐观锁判断
        }
        // 如果ODS模型内部添加了节点，则类型转为dataflow
        if (this.builder.getModelDataType() === ModelDataType.Ods && this.builder.getDataFlow().getNodeCount() > 2) {
            content.properties.modelDataType = ModelDataType.DataFlow;
        }
        // 如果调度信息没有发生改变，则不传递调度信息
        let currentSchedule = content.properties.schedule;
        if (deepEqual(currentSchedule, this.builder.originalSchedule)) {
            delete content.properties.schedule;
        }
        // 删除了计划需要传递该属性，以便调用者请求当前模型取消了定时提取
        if (!currentSchedule) {
            content.properties.schedule = [];
        }
        return meta;
    }

    /**
     * 两个节点做关联，形成一个新的关联节点
     */
    public addNodeJoin(leftNodeAlias: string, rightNodeAlias: string, alias?: string): string {
        let leftNode = this.getNodeByAlias(leftNodeAlias);
        let rightNode = this.getNodeByAlias(rightNodeAlias);
        let node = null;
        if (leftNode && rightNode) {
            if (alias) {
                node = rightNode.moveTo(leftNode.getId(), MoveDataFlowNodeType.NewJoin, alias);
            } else {
                node = rightNode.moveTo(leftNode.getId(), MoveDataFlowNodeType.NewJoin);
            }
            this.nodes[node.getId()] = node;
            return node.getAlias();
        }
        return node;
    }

    /**
     * 给join节点添加前序节点
     */
    public addJoinTable(joinAlias: string, rightNodeAlias: string): boolean {
        let join = this.getNodeByAlias(joinAlias);
        let rightNode = this.getNodeByAlias(rightNodeAlias);
        if (join && rightNode && join.getType() === DataFlowNodeType.Join) {
            rightNode.linkTo(join);
            return true;
        }
        return false;
    }

    public addJoinClause(join: string, inputNode: string, clause: FilterClauseInfo): void {
        let node = this.getNodeByAlias(join);
        if (!node) {
            return
        }
        let joinConditions: SQLJoinConditionInfo[] = clone(node.getNodeInfo().joinConditions);
        let condition = joinConditions.find((joinCondition) => joinCondition.rightTable === inputNode);
        condition.clauses.push(clause);
        condition.conditionType = "clause";
        node.setProperty("joinConditions", joinConditions);
    }

    /**
     * 添加模型输出节点
     */
    public addNodeOutput(preNodeAlias: string, alias?: string) {
        let preNode = this.getNodeByAlias(preNodeAlias);
        let node = null;
        if (preNode) {
            if (alias) {
                node = preNode.addOutputNode(DataFlowNodeType.Output, alias);
            } else {
                node = preNode.addOutputNode(DataFlowNodeType.Output);
            }
            this.nodes[node.getId()] = node;
            return node.getAlias();
        }
        return node;
    }

    public setDbTableInfo(dataSource: string, schema?: string, tableName?: string): void {
        let builder = this.builder;
        builder.setProperty("extractDataEnabled", true);
        builder.setProperty("datasource", dataSource);
        builder.setProperty("dbSchema", schema);
        builder.setProperty("dbTableName", tableName);
    }

    /**
     * 两个节点做union操作，将unionNode节点的union到targetNode的节点。
     */
    public addNodeUnion(targetNodeAlias: string, unionNodeAlias: string, alias?: string): string {
        let targetNode = this.getNodeByAlias(targetNodeAlias);
        let unionNode = this.getNodeByAlias(unionNodeAlias);
        if (!targetNode || !unionNode) {
            return null;
        }
        let union = targetNode.addOutputNode(DataFlowNodeType.Union, alias);
        unionNode.linkTo(union);
        return union.getAlias();
    }

    /**
     * 设置join节点表格模式的方式
     */
    public setJoinjoinType(joinAlias: string, joinType: SQLJoinType, rightTable: string): boolean {
        let join = this.getNodeByAlias(joinAlias);
        if (join) {
            let joinConditions: SQLJoinConditionInfo[] = clone(join.getNodeInfo().joinConditions);
            let condition = joinConditions.find((joinCondition) => joinCondition.rightTable === rightTable);
            condition.joinType = joinType;
            join.setProperty("joinConditions", joinConditions);
            return true;
        }
        return false;
    }

    /**
     * 添加列加工节点
     * @param preNodeAlias? 列加工节点的前节点别名
     * @param alias? 列加工节点别名, 默认: 列加工
     * @param insert 添加方式，“step”添加为步骤，“branch”添加为分支
     * @return 列加工别名
     */
    public addNodeSelect(preNodeAlias: string, alias?: string, insert?: string): string {
        let preNode = this.getNodeByAlias(preNodeAlias);
        let node: DwTableDataFlowNodeBuilder = null;
        if (preNode) {
            if (insert === "step") {
                node = preNode.insertAfter({
                    type: DataFlowNodeType.Select,
                    alias: alias
                })
            } else {
                node = preNode.addOutputNode(DataFlowNodeType.Select, alias);
            }
            this.nodes[node.getId()] = node;
            return node.getAlias();
        }
        return null;
    }

    /**
     * 列加工节点添加一个计算字段
     * @param alias 列加工节点别名
     * @param addField 计算字段信息
     */
    public addStepCalcField(alias: string, addField: DwTableFieldInfo): void {
        let targetNode = this.getNodeByAlias(alias);
        let fields = [addField.name];
        if (targetNode && targetNode.getType() === DataFlowNodeType.Select) {
            targetNode.addStep({
                type: DataFlowSelectNodeStepType.AddField,
                fields: fields,
                addField: addField
            });
        }
    }

    /**
     * 添加去重节点
     * @param preNodeAlias? 去重节点的前节点别名
     * @param alias? 去重节点别名, 默认: 去重
     * @return 去重节点别名
     */
    public addNodeDistinct(preNodeAlias: string, alias?: string): string {
        let preNode = this.getNodeByAlias(preNodeAlias);
        if (!preNode) {
            return null;
        }
        let node = preNode.addOutputNode(DataFlowNodeType.Distinct, alias);
        this.nodes[node.getId()] = node;
        return node.getAlias();
    }

    /**
     * 设置去重分组字段
     * @param alias 去重节点别名
     * @param fields 去重字段, eg: ["ID"]
     */
    public setDistinctFields(alias: string, fields: string[]): void {
        let node = this.getNodeByAlias(alias);
        node.setProperty("distinctFields", fields);
    }
}