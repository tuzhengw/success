import { ctx, SZEvent, Component, throwInfo, throwError, showConfirmDialog, showFormDialog, showErrorMessage, 
showMenu, isEmpty, deepAssign, uuid, message, quoteExpField, showDialog, showSuccessMessage, showProgressDialog,
rc, downloadFile, waitRender, showWaiting, assign, IDataProvider, formatDateFriendly, parseDate, formatDate, 
tryWait, rc_get, showWarningMessage, timerv, showWaitingMessage, hideMessage, DataChangeEvent, 
EventListenerManager, GroupInfo, IFindArgs, ServiceTaskPromise, ServiceTaskInfo, ServiceTaskState } from 'sys/sys';

import { IMetaFileCustomJS, InterActionEvent, IAppCustomJS, IDwCustomJS, DatasetDataPackageRowInfo, IDataset, IVComponent } from "metadata/metadata-script-api";
import { showDrillPage, getMetaRepository, MetaFileViewer, getCurrentUser, getSysSettings } from 'metadata/metadata';
import { getDwTableDataManager, labelData, refreshModelState, exportQuery, importData, ImportDataResult, getQueryManager, pushData, importDbTables, DwDataChangeEvent } from 'dw/dwapi';
import { DwPseudoDimName, DwTableModelFieldProvider } from 'dw/dwdps';
import { Combobox, Button, showUploadDialog, Toolbar, ToolbarItemAlign, ComboboxArgs, DataProvider, ButtonArgs, TXT_FILETYPE, IMAGE_FILETYPE } from 'commons/basic';
import { Tree } from 'commons/tree';
import { Menu, MenuItem } from "commons/menu";
import { SuperPageBuilder } from "app/superpage/superpagebuilder";
import { SuperPage } from 'app/superpage/superpage';
import { AnaModelBuilder } from 'ana/builder';
import { IDwTableEditorPanel, DwTableEditor } from 'dw/dwtable';
import { showLabelMgrDialog } from 'dw/dwdialog';
import { Form } from "commons/form";
import { ExDTable } from "commons/dtable";
import { Dialog } from 'commons/dialog';
import { PortalModulePageArgs, PortalHeadTitle, PortalHeadToolbar } from 'app/templatepage';
import { html2pdf, ExportReusltType } from 'commons/export';
import { modifyClass, getAppPath } from './commons/commons.js';
import { SuperPageData } from 'app/superpage/superpagedatamgr';
import { EasyFilter } from 'commons/extra.js';
import { checkCstmRule, runCheckTask, checkField } from 'dg/dgapi';
import { ExpCompiler, ExpVar, Token, ExpTokenIndex, IExpDataProvider } from 'commons/exp/expcompiler';
import { SpgEmbedSuperPage } from 'app/superpage/components/embed';
import { MetaFileRevisionView, showResourceDialog } from 'metadata/metamgr';
import { MetaFileViewer_miniapp } from 'app/miniapp';
import { ScheduleEditor, getScheduleMgr, TaskLogInfo } from "schedule/schedulemgr";
import { showDbConnectionDialog, DbConnectionPanel } from 'datasources/datasourcesmgr';
import { ProgressLogs, ProgressPanel } from 'commons/progress';
import { ExpDialog, ExpDialogValueInfo, IFieldsDataProvider, showExpDialog } from 'commons/exp/expdialog';
import { getCamelChars } from 'commons/pinyin';
import { ExpContentType } from 'commons/exp/exped';

let hard_label_filter; // 硬标签过滤条件，用于返回时重新设置进去

let label_match_primaryKey;//获取匹配主键字段列表

let label_code; // 标签编码，用于打标

let label_lib_code; // 标签库编码，用于打标

let label_hard_path;//标签库对应导入excel模型或打标数据表模型

let uploadfile;//血统文件上传文件

let ex_filter;//数据交换过滤条件

let lastSelectedRow = -1;

//这个模型应该是每个标签库都创建一个
const EXCEL_MODEL_PATH = "/sysdata/data/tables/custom/主数据表对应excel/";
//过滤条件存储表
const TABLE_FILTER_PATH = "/sysdata/data/tables/sdi/SDI_TABLE_FILTER.tbl";
//数据源状态表
const DATASOURCESTATE = "/sysdata/data/tables/sys/DATASOURCESTATE.tbl";
//字段引用关系表
const META_FIELD_LINK_PATH = "/sysdata/data/tables/meta/META_FIELDS_LINKS.tbl";
//血统分析结果表
const TABEL_LINEAGE_PATH = "/sysdata/data/tables/sdi/SDI_PEDIGREE_ANALYSIS.tbl";
//元数据表
const SDI_META_TABLES_EXT = "/sysdata/data/tables/sdi/SDI_META_TABLES_EXT.tbl";
//字段表
const META_FIELDS_PATH = "/sysdata/data/tables/meta/META_FIELDS.tbl";
//数据交换通道表
const DX_CHANNLES = "/sysdata/data/tables/dx/DX_CHANNELS.tbl";

const PROJECTURL = '/sysdata/app/zt.app/';

const JDBCFILE = '/sysdata/data/sources/';

const TABLE_INFO = '/sysdata/data/tables/sdi/data-access/SDI_DATASOURCE_INFO.tbl';

const LINEAGE_QUERY_FIELDS = [
	{ name: "resid", exp: "model1.MODEL_RESID" },
	{ name: "domain", exp: "model1.DOMAIN" },
	{ name: "domainDesc", exp: "model1.DATA_DOMAIN_DESC" },
	{ name: "modelName", exp: "model1.MODEL_NAME" },
	{ name: "modelDesc", exp: "model1.MODEL_CN_NAME" },
	{ name: "parentPath", exp: "model1.PARENT_DIR" }
];

const LABEL_SPGS = [
	PROJECTURL + "data-label/数据打标/软标签.spg",
	PROJECTURL + "data-label/数据打标/硬标签.spg",
	PROJECTURL + "data-label/数据打标/添加数据.spg",
	PROJECTURL + "data-label/数据打标/导入excel.spg",
	PROJECTURL + "data-label/数据打标/数据表打标.spg",
	PROJECTURL + "data-label/data_labelView.spg"
];
//申请api接口时需要的字段
const APIFIELD = ['DX_CHANNEL_ID', 'DX_RES_ID', 'DW_TABLE_ID', 'API_ENABLE', 'DATA_FIELDS', 'DATA_FILTER', 'CUSTOM_FILTER', 'CUSTOM_FILTER_DESC', 'DX_CHANNEL_DESC', 'CHANNEL_TYPE', 'USERS', 'APPLICATION_TIME', 'COMMITER', 'COMMITER_PHONE', 'COMMITER_DX_ORG_ID', 'ENABLE', 'EXPIRED_DATE', 'AUTHOR_TIME', 'API_AUTH_TYPE', 'LOG_ENABLE'];
//申请推送或回放资源时需要的字段
const PUSHORREVIEWFIELD = ['DX_CHANNEL_ID', 'DX_RES_ID', 'DW_TABLE_ID', 'SB_USE_INCREMENT', 'PUSH_SCHEDULE', 'SB_INC_FIELD_TYPE', 'SB_INC_FIELD', 'SB_ENABLE_REGULAR_WHOLE', 'SB_REGULAR_WHOLE_PERIOD_N', 'SB_ENABLE_BACKUP', 'SB_BACKUP_COUNT', 'SB_REPOSITORY_ID', 'PUSH_TARGET_TABLE', 'PUSH_ENABLE', 'DX_CHANNEL_DESC', 'CHANNEL_TYPE', 'USERS', 'APPLICATION_TIME', 'COMMITER', 'COMMITER_PHONE', 'COMMITER_DX_ORG_ID', 'ENABLE', 'EXPIRED_DATE', 'AUTHOR_TIME', 'API_AUTH_TYPE', 'LOG_ENABLE'];

let current_label_model: string;

/**
 * 数据标签页面动态加载模型表
 */
function Label_onDidLoadFile(viewer: MetaFileViewer_miniapp, file: MetaFileInfo): void | Promise<void> {
	if (!viewer) { return; }
	let page: SuperPage = <SuperPage>viewer.getActivePage();
	let builder: SuperPageBuilder = page && page.builder;
	let currentSpgResId = builder && builder.getResid();
	if (!page) { return; }
	let urlParams = page.getData().getUrlParams();
	let resId: string = urlParams.table; // 主数据表
	let lib: string = urlParams.lib; // 标签库id
	let label: string = urlParams.label; // 标签编码
	let fileId: string = urlParams.fileId; // 导入excel的文件id
	let filterExp: string = urlParams.filterExp; // 导入excel后的匹配数据过滤条件
	let unmatchFilter: string = urlParams.unmatchFilter; // 导入excel后的不匹配数据过滤条件\
	let filePath: string = urlParams.filePath;//选择用于匹配的数据表路径
	let targetFields: string[] = urlParams.targetFields && urlParams.targetFields.split(",") || []; // 导入excel的目标字段
	let fileFields: string[] = urlParams.fileFields && urlParams.fileFields.split(",") || []; // excel的列头字段名

	let spgIndex = LABEL_SPGS.indexOf(currentSpgResId);
	let keys: string[];
	let labelInfo: DwDataLabel;
	let filters: FilterInfo[];
	// page.setVisible(false);

	return getDwTableDataManager().getDwDataHierarchyProvider({ dwTablePath: resId, pseudoDim: DwPseudoDimName.Labels }).then(dp => dp && dp.getItem(label)).then((labelItem) => {
		labelInfo = labelItem && labelItem.getValue();
		return getMetaRepository().getFileBussinessObject(resId);
	}).then((bo: DwTableModelFieldProvider) => {
		let pks = bo.getPrimaryKeys();
		if (!isEmpty(pks)) {
			keys = bo.getPrimaryKeys().map(key => key.dbfield);
		}
		let anaModelInfo: AnaModelInfo = { path: bo.getFileInfo().path, pageSize: 50 };
		// 给硬标签和添加数据spg加上默认过滤条件
		if (labelInfo) {
			if (spgIndex == 1 || spgIndex == 5) {
				anaModelInfo.filter = {
					clauses: [{
						exp: `model1.$labels='${labelInfo.code}'`
					}]
				};
			}
			else if (spgIndex == 2) {
				anaModelInfo.filter = {
					clauses: [{
						exp: `model1.$labels!='${labelInfo.code}'`
					}]
				};
			}
		}
		// 查看导入文件结果，加上过滤条件，查看数据表匹配结果
		if (spgIndex == 3 || spgIndex == 4) {
			anaModelInfo.filter = {
				clauses: [{
					exp: filterExp
				}]
			};
			current_label_model = anaModelInfo.path;
		}
		let promises = [builder.createModelAsync(anaModelInfo)];
		/* 查看导入文件结果，需要额外添加导入excel的模型，这样才能查看未匹配的数据
		 * 这里创建了四个数据源，目的是为了能够正常显示匹配数据和未匹配数据
		*/
		if (spgIndex == 3) {
			label_hard_path = EXCEL_MODEL_PATH + resId + ".tbl";
			promises.push(builder.createModelAsync({
				path: label_hard_path
			}));
			unmatchFilter = unmatchFilter.replaceAll("model2", "model4").replaceAll("model1", "model3");
			promises.push(builder.createModelAsync({
				path: current_label_model
			}))
			promises.push(builder.createModelAsync({
				path: label_hard_path, filter: {
					clauses: [{ exp: unmatchFilter }]
				}
			}));
		}
		//查看数据表匹配结果，需要添加选择的数据表模型，查询出匹配结果
		if (spgIndex == 4) {
			label_hard_path = filePath;
			promises.push(builder.createModelAsync({
				path: label_hard_path
			}));
		}
		return Promise.all(promises);
	}).then((models) => {
		let model = models[0]
		let fieldsFilter1 = builder.getComponent('fieldsFilter1');
		let list1 = builder.getComponent('list1');
		let list2 = builder.getComponent('list2');

		builder.beginUpdate();
		try {
			builder.addModel(model);
			fieldsFilter1.setProperty(PropertyNames.DataSet, model.getId());
			let fieldsSubComps = fieldsFilter1.getSubComponents();
			fieldsSubComps.forEach(s => {
				s.setProperty("includeAllWhenEmpty", false);
			})
			list1.setProperty(PropertyNames.DataSet, model.getId());
			// 设置导入excel的未匹配数据模型
			if (spgIndex == 3 && models[1]) {
				builder.addModel(models[1]);
				builder.addModel(models[2]);
				builder.addModel(models[3]);
				list2.setProperty(PropertyNames.DataSet, models[3].getId());
				let subComps = list2.getSubComponents();
				for (let i = subComps.length - 1; i >= 0; i--) {
					let comp = subComps[i];
					let field: string = comp.getProperty("field");
					let fieldName = field.substr(field.indexOf('.') + 1);
					let index = targetFields.indexOf(fieldName);
					if (index == -1) {
						subComps.remove(i);
					}
					else {
						comp.setProperty("caption", fileFields[index]);
					}
				}
			}
			//设置选择用于匹配数据模型
			if (spgIndex == 4 && models[1]) {
				builder.addModel(models[1]);
			}
			// 把主键列放到最前面
			let subComps = list1.getSubComponents();
			let keyComp = subComps.find((comp) => {
				let field: string = comp.getProperty("field");
				return field == `model1.${keys[0]}`;
			});
			subComps.remove(keyComp);
			subComps.splice(0, 0, keyComp);

			// 除了软标签spg，其他spg给第一列显示勾选框
			if (spgIndex > 0 && spgIndex != 5) {
				let column = list1.createSubComponent({
					type: "list.column",
					showAllSelect: true,
					showCheckbox: true,
					captionRow: {
						colWidthType: "fix",
						colWidth: 30
					}
				}, true);
				list1.addSubComponent(column, 0);
			}
			// 给软标签加上默认过滤条件
			else if (spgIndex == 0) {
				filters = !isEmpty(labelInfo.filters) ? labelInfo.filters.map(filter => {
					let newFilter: FilterInfo = deepAssign({}, filter);
					newFilter.clauses.forEach(clause => {
						clause.leftExp && (clause.leftExp = "model1." + clause.leftExp);
						clause.exp && (clause.exp = clause.exp.replace("t0.", "model1."));
						if (clause.rightValue && typeof clause.rightValue == "string") {
							try {
								// 后台记录的rightValue都是string，这里试着转下json，让前台控件能正常渲染
								//如果是时间类型的数据，被转换后变成了数字，前台控件无法解析，如果转后为数字就不转了
								let rightValue = JSON.parse(clause.rightValue);
								if (typeof rightValue !== "number") {
									clause.rightValue = rightValue;
								}
							}
							catch (e) {
								console.log(`rightValue无法转换为json：${clause.rightValue}`);
							}
						}
					})
					return newFilter;
				}) : [];
				let fieldsNode = page.getData().getNodeData(fieldsFilter1);
				fieldsNode.setProperty("value", JSON.stringify(filters));
			}
		} finally {
			builder.endUpdate();
		}
		return page.waitRender().then(() => {
			let datasets = page.getData().getDatasets();
			let totalRowCount = datasets.length > 0 && datasets[0].getTotalRowCount();
			if (totalRowCount == 0) {
				let filterComp = page.getComponent("fieldsFilter1");
				filterComp && filterComp.setVisible(false);
				let fieldsNode = filterComp && page.getData().getNodeData(fieldsFilter1);
				fieldsNode && fieldsNode.setProperty("visibile", false);
			}
		});
	})
}

/**
 * 数据标签spg页面定制脚本
 */
const Label_CustomActions = {
	/** 硬标签打标 */
	button_labelData: (event: InterActionEvent) => {
		let builder = (<any>event.page).builder;
		let urlParams = event.page.getUrlParams();
		label_code = urlParams.label;
		label_lib_code = urlParams.lib;
		let resId = urlParams.table;
		let mode = urlParams.mode;
		let filePath = urlParams.filePath;
		let dataset = (<any>event.page).getDatasets()[0];
		let totalRowCount = dataset.getTotalRowCount();
		let vc = event.page.getComponent('list1');
		let checkedRows = vc.getCheckedDataRows();
		if (checkedRows.length === 0) {
			return throwInfo("请选择希望打标的数据！");
		}
		let fieldFilter = event.page.getComponent('fieldsFilter1');
		let value = fieldFilter.getValue() ? JSON.parse(fieldFilter.getValue()) : [];
		for (let i = 0; i < value.length; i++) {
			if (value[i] == null) {
				value = [];
				break;
			}
		}
		if (hard_label_filter) {
			value.push({ clauses: [hard_label_filter] });
		}

		let filters: Array<FilterInfo> = !isEmpty(value) ? value.map(filter => {
			let newFilter: FilterInfo = deepAssign({}, filter);
			newFilter.clauses.forEach(clause => {
				clause.leftExp && (clause.leftExp = "t0." + clause.leftExp);
				clause.exp && (clause.exp = clause.exp.replaceAll("model1", "t0").replaceAll("model2", "t1"));
			})
			return newFilter;
		}) : [];
		let idColumn = builder.getComponent("list1").getSubComponents()[1].getId();
		let ids = checkedRows.map(r => r[idColumn].field);
		showFormDialog({
			id: "spg.dialog.confirmLabel",
			caption: "数据打标",
			content: {
				items: [{
					id: "message",
					formItemType: 'text',
					value: "是否对勾选的数据进行打标？",
					captionVisible: false,

				}, {
					id: 'selectwriteMode',
					formItemType: 'selectpanel',
					captionVisible: false,
					formItemCaptionVisible: false,
					compArgs: {
						items: [{
							value: 'all',
							caption: '全覆盖'
						}, {
							value: 'append',
							caption: '只追加'
						}]
					}
				}]
			},
			onshow: (event: SZEvent, dialog: Dialog) => {
				let form = <Form>dialog.content;
				form.loadData({ selectwriteMode: 'all' });
			},
			buttons: [{
				id: 'currentPage',
				caption: '打标当前勾选',
				layoutTheme: "defbtn",
				onclick: (event: SZEvent, dialog: Dialog, item: any) => {
					let form = <Form>dialog.content;
					let overwriteMode = form.getFormItem("selectwriteMode").getValue();
					labelData({
						labelCode: label_code,
						labelLibCode: label_lib_code,
						labelKeys: ids,
						overwriteMode: overwriteMode
					}).then(() => {
						throwInfo("打标成功！")
						dialog.close();
						return getDwTableDataManager().getLabelLib(label_lib_code);
					}).then(lib => {
						if (lib) {
							return refreshModelState(lib.mainTableId).then(() => {
								return getMetaRepository().getFile(lib.mainTableId);
							}).then(file => {
								getDwTableDataManager().updateCache([{ path: file.path, type: "refreshall" }], true);
							});
						}
					});
				}
			}, {
				id: 'allPage',
				caption: '打标所有页面',
				layoutTheme: "defbtn",
				onclick: (event: SZEvent, dialog: Dialog, item: any) => {
					//判断是否超过10w行
					if (totalRowCount > 100000) {
						throwError("硬标签打标个数不能超过10万行");
						return;
					}
					let form = <Form>dialog.content;
					let overwriteMode = form.getFormItem("selectwriteMode").getValue();
					if (mode == 'table') {
						getMetaRepository().getFileBussinessObject(filePath).then((bo: DwTableModelFieldProvider) => {
							let fileId = bo.getFileInfo().id;
							let query: QueryInfo = {
								fields: [{ name: "TABLE_SOURCES", exp: "model1.TABLE_SOURCES" }],
								filter: [{ exp: `model1.LABEL_LIB_CODE='${label_lib_code}'` }],
								sources: [{
									id: "model1",
									path: "/sysdata/data/tables/dw/DW_DATA_LABELS_LIB.tbl"
								}]
							};
							getQueryManager().queryData(query, uuid()).then((result: QueryDataResultInfo) => {
								let data = result.data;
								let tableSources = JSON.parse(<string>data[0][0]);
								let tableId = "t1";
								let source: JSONObject = {
									path: fileId,
									dwTableFieldCount: 0,
									forLabFilter: false,
									forSelectFunc: 0,
									sourceType: 0,
									joinType: -1
								};
								if (tableSources.length == 0) {
									source.id = "t1";
									tableSources.push({
										id: "t0", path: resId,
										dwTableFieldCount: 0,
										forLabFilter: false,
										forSelectFunc: 0,
										sourceType: 0,
										joinType: -1
									})
									tableSources.push(source);
								} else {
									let notExit = true;
									tableSources.forEach(i => {
										if (i.path == fileId) {
											notExit = false;
											tableId = i.id;
											return;
										}
									});
									if (notExit) {
										tableId = "t" + tableSources.length;
									}
									source.id = tableId;
									notExit && tableSources.push(source);
									filters = !isEmpty(filters) ? filters.map(filter => {
										let newFilter: FilterInfo = deepAssign({}, filter);
										newFilter.clauses.forEach(clause => {
											clause.exp && (clause.exp = clause.exp.replaceAll("t1", tableId));
											clause.leftExp && (clause.leftExp = clause.leftExp.replaceAll("t1", tableId))
										})
										return newFilter;
									}) : [];
								}
								rc({
									url: getAppPath("/commons/commons.action?method=updateDwTableData"),
									method: "POST",
									data: {
										tablePath: "/sysdata/data/tables/dw/DW_DATA_LABELS_LIB.tbl",
										datapackage: {
											modifyRows: [{
												fieldNames: ["TABLE_SOURCES"],
												rows: [{
													keys: [label_lib_code],
													row: [JSON.stringify(tableSources)]
												}]
											}]
										}
									}
								}).then(() => {
									labelData({
										labelCode: label_code,
										labelLibCode: label_lib_code,
										filters: filters,
										overwriteMode: overwriteMode
									}).then(() => {
										throwInfo("打标成功！")
										dialog.close();
										return getDwTableDataManager().getLabelLib(label_lib_code);
									}).then(lib => {
										if (lib) {
											return refreshModelState(lib.mainTableId).then(() => {
												return getMetaRepository().getFile(lib.mainTableId);
											}).then(file => {
												getDwTableDataManager().updateCache([{ path: file.path, type: "refreshall" }], true);
											});
										}
									});
								})
							})
						});
					} else {
						labelData({
							labelCode: label_code,
							labelLibCode: label_lib_code,
							filters: filters,
							overwriteMode: overwriteMode
						}).then(() => {
							throwInfo("打标成功！")
							dialog.close();
							return getDwTableDataManager().getLabelLib(label_lib_code);
						}).then(lib => {
							if (lib) {
								return refreshModelState(lib.mainTableId).then(() => {
									return getMetaRepository().getFile(lib.mainTableId);
								}).then(file => {
									getDwTableDataManager().updateCache([{ path: file.path, type: "refreshall" }], true);
								});
							}
						});
					}
				}
			}, 'cancel']
		})
	},
	/** 硬标签删除标签数据 */
	button_unlabelData: (event: InterActionEvent) => {
		let builder: SuperPageBuilder = (<any>event.page).builder;
		let page = event.page;
		let vc = event.page.getComponent('list1');
		let checkedRows = vc.getCheckedDataRows();
		if (checkedRows.length === 0) {
			return throwInfo("请选择希望移除标签的数据！");
		}
		let idColumn = builder.getComponent("list1").getSubComponents()[1].getId();
		let ids = checkedRows.map(r => r[idColumn].field);
		// let urlParams = builder.getUrlParams();
		let urlParams = event.page.getUrlParams();
		label_code = urlParams.label;
		label_lib_code = urlParams.lib;
		showFormDialog({
			id: "spg.dialog.confirmDelLabel",
			caption: "解除标签",
			content: {
				items: [{
					id: "message",
					formItemType: 'text',
					value: "是否对勾选的数据取消打标？",
					formItemCaptionVisible: false,
					captionVisible: false
				}]
			}, buttons: [{
				id: 'currentPage',
				caption: '取消当前勾选',
				layoutTheme: "defbtn",
				onclick: (event: SZEvent, dialog: Dialog, item: any) => {
					let form = <Form>dialog.content;
					labelData({
						labelCode: label_code,
						labelLibCode: label_lib_code,
						delabelKeys: ids
					}).then(() => {
						throwInfo("取消打标成功！");
						dialog.close();
						return getDwTableDataManager().getLabelLib(label_lib_code);
					}).then(lib => {
						if (lib) {
							refreshModelState(lib.mainTableId).then(() => {
								return getMetaRepository().getFile(lib.mainTableId);
							}).then(file => {
								getDwTableDataManager().updateCache([{ path: file.path, type: "refreshall" }], true);
							});
						}
					});
				}
			}, {
				id: 'allPage',
				caption: '取消所有页面',
				layoutTheme: "defbtn",
				onclick: (event: SZEvent, dialog: Dialog, item: any) => {
					let fieldFilter = page.getComponent('fieldsFilter1');
					let value = fieldFilter.getValue() ? JSON.parse(fieldFilter.getValue()) : [];
					let dataset = page.getDatasets()[0];
					let key = dataset.getPrimaryKeys()[0];
					let source = dataset.getQuerySourcesInfo();
					let queryInfo: QueryInfo = {
						sources: source,
						fields: [{ name: key, exp: `model1.${key}` }],
						filter: value,
						select: true
					};
					getQueryManager().queryData(queryInfo, uuid()).then(result => {
						let data = result.data;
						ids = [];
						data.forEach(i => {
							ids.push(i[0])
						})
						labelData({
							labelCode: label_code,
							labelLibCode: label_lib_code,
							delabelKeys: ids
						}).then(() => {
							throwInfo("打标成功！")
							dialog.close();
							return getDwTableDataManager().getLabelLib(label_lib_code);
						}).then(lib => {
							if (lib) {
								return refreshModelState(lib.mainTableId).then(() => {
									return getMetaRepository().getFile(lib.mainTableId);
								}).then(file => {
									getDwTableDataManager().updateCache([{ path: file.path, type: "refreshall" }], true);
								});
							}
						});
					})
				}
			}, 'cancel']
		})
	},
	/** 软标签打标 */
	button_labelData_soft: (event: InterActionEvent) => {
		let builder = (<any>event.page).builder;
		let urlParams = event.page.getUrlParams();
		label_code = urlParams.label;
		label_lib_code = urlParams.lib;
		let vc = event.page.getComponent('fieldsFilter1');
		let value = vc.getValue();
		if (typeof (value) === 'string') {
			value = JSON.parse(vc.getValue())
		}
		let filters: Array<FilterInfo> = !isEmpty(value) ? value.map(filter => {
			let newFilter: FilterInfo = deepAssign({}, filter);
			newFilter.clauses.forEach(clause => {
				clause.leftExp && (clause.leftExp = clause.leftExp.replace("model1.", ""));//软标签在查询时不支持带有t0，此处暂时去掉前缀。
				clause.exp && (clause.exp = clause.exp.replace("model1.", ""));
			})
			newFilter.clauses = newFilter.clauses.filter(f => { return f.rightValue != null || f.operator == "is null" || f.operator == "is not null" });//如果值为null需要把此过滤条件给去掉
			return newFilter;
		}) : [];
		showConfirmDialog({
			caption: "数据打标",
			message: `将要从对指定条件的数据进行打标，是否确定？`,
			onok: () => {
				labelData({ labelCode: label_code, labelLibCode: label_lib_code, filters: filters }).then(() => {
					throwInfo("保存成功！")
					return getDwTableDataManager().getLabelLib(label_lib_code);
				}).then(lib => {
					lib && refreshModelState(lib.mainTableId);
				});
			}
		})
	},
	/** 下载excel模板文件 */
	button_label_download: (event: InterActionEvent) => {
		let model: AnaModelBuilder = (<any>event.page).getDatasets()[0].getModel();
		let keyField: DwTableFieldInfo = model.getOrignalField(model.getPrimaryKeys()[0]);
		let sourceInfo: QuerySourceInfo = model.getQuerySourceInfo();
		let fields: QueryFieldInfo[] = [{ name: keyField.name, exp: `${sourceInfo.id}.${keyField.dbfield}` }];
		//将所有的字段全部下载到模板中
		//let allField = model.getOriginFields();
		/*for(let i =0;i<allField.length;i++){
			let field = allField[i];
			if (keyField.name == field.getName()){
				continue;
			}
			fields.push({name:field.getName(),exp:`${sourceInfo.id}.${field.getDbField()}`});
		}*/
		let textField = keyField.textField;
		if (textField) {
			let field = model.getMetaFieldByName(textField);
			fields.push({ name: textField, exp: `${sourceInfo.id}.${field.getId()}` })
		}
		let query: QueryInfo = {
			fields: fields,
			sources: [model.getQuerySourceInfo()],
			options: {
				limit: 10,
				offset: 0
			}
		}
		exportQuery({ resid: sourceInfo.path, query: query, fileFormat: DataFormat.xlsx, fileName: `${model.getName()}打标模版` });
	},
	button_label_reset: (event: InterActionEvent) => {
		let vc = event.page.getComponent('fieldsFilter1');
		vc.setValue([]);
		throwInfo("已重置");
	},
	/**人工勾选进行硬标签打标 */
	button_label_select: (event: InterActionEvent) => {
		let builder = (<any>event.page).builder;
		let urlParams = event.page.getUrlParams();
		label_code = urlParams.label;
		label_lib_code = urlParams.lib;

		let domFileViewer = builder.getRenderer().domBase.closest('.metafileviewer-base');
		if (!domFileViewer) {
			return;
		}
		let fileViewer: MetaFileViewer = domFileViewer.szobject as MetaFileViewer;
		fileViewer.showDrillPage({
			url: {
				path: getAppPath(`/data-label/数据打标/添加数据.spg`),
				params: { ':runtimecompile': true, 'label': label_code, 'lib': label_lib_code, 'table': urlParams.table, 'labelName': urlParams.labelName }
			},
			breadcrumb: true,
			target: ActionDisplayType.Container,
			title: `添加数据`
		});
		/*
		hard_label_filter = event.page.getComponent("fieldsFilter2").getValue();
		label_code = builder.urlParams.label;
		label_lib_code = builder.urlParams.libCode;
		builder.setUrlParams({ "mode": "label_hard" });
		let promise: Promise<void> = Promise.resolve();
		promise.then(function () { builder.getRenderer().refresh(); });
		return promise;
		*/
	},
	/**根据关联数据表进行打标 */
	button_label_table: (event: InterActionEvent) => {
		let builder = (<any>event.page).builder;
		let urlParams = event.page.getUrlParams();
		let resId: string = urlParams.table;
		let lib: string = urlParams.lib;
		let label: string = urlParams.label;
		let selectResid: string = event.page.getComponent("resSelector1").getValue();
		let modelRangeComp = event.page.getComponent("customModelDataRange1");
		let resFilter = !isEmpty(modelRangeComp.getValue()) ? modelRangeComp.getValue() : null;
		let filterExp: string; // 匹配列表的过滤条件
		let unmatchFilter: string; // 不匹配列表的过滤条件
		let rowCount: number;//匹配列表条数
		let joinTableQuery: QueryInfo;//过滤后的关联总行数
		let fileName: string;
		let tableFieldDesc: string;
		let targtFieldDesc: string;
		let selectBo: DwTableModelFieldProvider;
		let targetFields: string[] = [];
		let fields: string[] = [];
		getMetaRepository().getFileBussinessObject(selectResid).then((bo: DwTableModelFieldProvider) => {
			selectBo = bo;
			let selectFields = bo.getFields();
			for (let i = 0; i < selectFields.length; i++) {
				fields.push(selectFields[i].name);
				targetFields.push(selectFields[i].dbfield);
			}
			return getMetaRepository().getFileBussinessObject(resId);
		}).then((bo: DwTableModelFieldProvider) => {
			let promise = new Promise<JSONObject>((resolve) => {
				// 计算默认的匹配字段
				let targetField = selectBo.getPrimaryKeys()[0];
				let tableField = bo.getPrimaryKeys()[0];
				showFormDialog({
					id: "spg.dialog.labelTable",
					caption: "匹配字段",
					buttons: ["ok"],
					content: {
						items: [{
							id: "label-table-tableField",
							caption: "来源表字段",
							desc: "选择用于匹配数据的来源表字段",
							formItemType: "combobox",
							type: "tree",
							value: tableField,
							itemDataConf: { valueField: "id" },
							searchBoxVisible: true,
							validRegex: (v: string) => {
								if (!v) {
									return { result: true };
								}
							}
						}, {
							id: "label-table-targetField",
							caption: "目标表字段",
							formItemType: "combobox",
							type: "tree",
							value: targetField,
							desc: "选择用于匹配数据的目标表字段",
							searchBoxVisible: true,
							itemDataConf: { valueField: "id" },
							validRegex: (v: string) => {
								if (!v) {
									return { result: true };
								}
							}
						}]
					},
					onshow: (event: SZEvent, dialog: Dialog) => {
						let form = <Form>dialog.content;
						form.waitInit().then(() => {
							form.getFormItem('label-table-tableField').setValue(tableField.dbfield);
							targetField && form.getFormItem('label-table-targetField').setValue(targetField.dbfield);
							form.getFormItem('label-table-tableField').setDataProvider(selectBo);
							form.getFormItem('label-table-targetField').setDataProvider(bo);
						})
					},
					onok: (sze, dialog, btn, form) => {
						let tableFieldComp = form.getFormItem("label-table-tableField");
						let targetFieldComp = form.getFormItem("label-table-targetField")
						let tableField = tableFieldComp.getValue();
						tableFieldDesc = tableFieldComp.comp.getCaption();
						let targetField = targetFieldComp.getValue();
						targtFieldDesc = targetFieldComp.comp.getCaption();
						resolve({
							bo: bo,
							tableField: tableField,
							targetField: targetField
						});
					}
				});
			});
			return promise;
		}).then((result: JSONObject) => {
			// 根据导入结果生成关联匹配和不匹配的过滤条件
			let bo: DwTableModelFieldProvider = result.bo;
			let key = bo.getPrimaryKeys()[0];
			let matchField = result.tableField;
			fileName = bo.getFileInfo().desc;
			let filter = `model1.${result.targetField}=model2.${matchField}`;
			if (!isEmpty(resFilter)) {
				filter += (` and ${resFilter}`);
			}
			if (filter.length) {
				unmatchFilter = `not exists select(model2, ${filter})`;
				filterExp = `exists select(model2, ${filter})`;
			}
			else {
				filterExp = '1>2';
			}
			hard_label_filter = { exp: filterExp };
			// 查询匹配数量
			let queryInfo: QueryInfo = {
				fields: [{ name: "count", exp: `count(model1.${result.targetField})` }],
				filter: [{ exp: filterExp }],
				sources: [{
					id: "model1",
					path: bo.getFileInfo().path
				}, {
					id: "model2",
					path: selectResid
				}]
			};
			//获取匹配主键
			/*label_match_primaryKey = {
				fields: [{ name: `${result.targetField}`, exp: `model1.${result.targetField}` }],
				filter: [{ exp: filterExp }],
				sources: [{
					id: "model1",
					path: bo.getFileInfo().path
				}, {
					id: "model2",
					path: selectResid
				}]
			};*/
			//关联上的数量
			joinTableQuery = {
				fields: [{ name: "count", exp: `count(model2.${matchField})` }],
				filter: [{ exp: resFilter }],
				sources: [{
					id: "model2",
					path: selectResid
				}]
			};
			return getQueryManager().queryData(queryInfo, uuid());
		}).then((result: QueryDataResultInfo) => {
			rowCount = <number>result.data[0][0];
			return getQueryManager().queryData(joinTableQuery, uuid());
		}).then((result: QueryDataResultInfo) => {
			let joinTableResult = <number>result.data[0][0];
			showConfirmDialog({
				caption: "提示",
				message: `本次【${selectBo.getFileInfo().desc}】过滤后共${joinTableResult}条，通过${tableFieldDesc}与【${fileName}】${targtFieldDesc}匹配后。共匹配到${rowCount}条数据`,
				onok: () => {
					let domFileViewer = builder.getRenderer().domBase.closest('.metafileviewer-base');
					if (!domFileViewer) {
						return;
					}
					let fileViewer: MetaFileViewer = domFileViewer.szobject as MetaFileViewer;
					return fileViewer.showDrillPage({
						url: {
							path: getAppPath(`/data-label/数据打标/数据表打标.spg`),
							params: {
								':runtimecompile': true,
								'label': label, 'lib': lib, 'table': resId, 'filterExp': filterExp, 'unmatchFilter': unmatchFilter, 'filePath': selectResid, mode: "table", 'labelName': urlParams.labelName
							}
						},
						breadcrumb: true,
						target: ActionDisplayType.Container,
						title: `关联数据表打标`
					});
				}
			});
		})

	},
	/** 通过导入excel进行硬标签打标 */
	button_label_excel: (event: InterActionEvent) => {
		let builder = (<any>event.page).builder;
		let urlParams = event.page.getUrlParams();
		let resId: string = urlParams.table;
		let lib: string = urlParams.lib;
		let label: string = urlParams.label;
		let filterExp: string; // 匹配列表的过滤条件
		let unmatchFilter: string; // 不匹配列表的过滤条件
		let rowCount: number;//匹配列表条数
		let unmatchQuery: QueryInfo;//不匹配query
		let fileName: string;
		showUploadDialog({
			fileTypes: ["xls", "xlsx"], //只上传excel文档
			multiple: false,       //上传单个文档
			maxSize: 10 * 1024 * 1024, //限制大小10M
			onchange: (e) => {
				let file = e.value[0];
				let excelResult: ImportDataResult;
				// 导入excel到临时模型
				importData({
					fileInfo: {
						fileId: file.id,
						fileType: file.fileType,
						headsStart: 1,
						headsEnd: 1
					},
					importSameFields: true,
					resIdOrPath: EXCEL_MODEL_PATH + resId + ".tbl",
					importUnmatchedFields: true,
					fieldValues: [{
						name: "UUID",
						valueType: "const",
						value: file.id
					}]
				}).then((result) => {
					excelResult = result;
					return getMetaRepository().getFileBussinessObject(resId);
				}).then((bo: DwTableModelFieldProvider) => {
					let promise = new Promise<JSONObject>((resolve) => {
						// 计算默认的匹配字段
						let targetField;
						let fileField = excelResult.fileFields.find((field) => {
							let fieldInfo = bo.getField(field) || bo.getFieldByDbField(field);
							if (fieldInfo && (fieldInfo.dataType == FieldDataType.C || fieldInfo.dataType == FieldDataType.I)) {
								targetField = fieldInfo.dbfield;
								return true;
							}
						})
						showFormDialog({
							id: "spg.dialog.labelExcel",
							caption: "匹配字段",
							buttons: ["ok"],
							content: {
								items: [{
									id: "label-excel-fileField",
									caption: "文件字段",
									desc: "选择用于匹配数据的文件字段",
									formItemType: "combobox",
									items: excelResult.fileFields,
									value: fileField,
									validRegex: (v: string) => {
										if (!v) {
											return { result: true };
										}
									}
								}, {
									id: "label-excel-targetField",
									caption: "目标表字段",
									formItemType: "combobox",
									type: "tree",
									dataProvider: bo,
									value: targetField,
									desc: "选择用于匹配数据的目标表字段",
									searchBoxVisible: true,
									itemDataConf: { valueField: "id" },
									validRegex: (v: string) => {
										if (!v) {
											return { result: true };
										}
									}
								}]
							},
							onshow: (event: SZEvent, dialog: Dialog) => {
								let form = <Form>dialog.content;
								form.waitInit().then(() => {
									form.getFormItem('label-excel-fileField').setValue(fileField);
									targetField && form.getFormItem('label-excel-targetField').setValue(targetField);
									form.getFormItem('label-excel-fileField').comp.setItems(excelResult.fileFields);
									form.getFormItem('label-excel-targetField').setDataProvider(bo);
								})
							},
							onok: (sze, dialog, btn, form) => {
								let fileField = form.getFormItem("label-excel-fileField").getValue();
								let dbField = excelResult.fields[excelResult.fileFields.indexOf(fileField)];
								resolve({
									bo: bo,
									fileField: dbField,
									targetField: form.getFormItem("label-excel-targetField").getValue(),
								});
							}
						});
					});
					return promise;
				}).then((result: JSONObject) => {
					// 根据导入结果生成关联匹配和不匹配的过滤条件
					let bo: DwTableModelFieldProvider = result.bo;
					let key = bo.getPrimaryKeys()[0];
					let matchField = result.fileField;
					fileName = bo.getFileInfo().desc;
					let filter = `model1.${result.targetField}=model2.${matchField}`;
					/*
					let matchFields = excelResult.targetFields;
					excelResult.fields.forEach((field, i) => {
						if (matchFields[i]) {
							let fieldInfo = bo.getField(field) || bo.getFieldByDbField(field);
							if (fieldInfo && (fieldInfo.dataType == FieldDataType.C || fieldInfo.dataType == FieldDataType.I)) {
								if (filter.length) {
									filter += ' and ';
								}
								filter += `model1.${fieldInfo.dbfield}=model2.${matchFields[i]}`
							}
						}
					});
					*/
					if (filter.length) {
						unmatchFilter = `not exists select(model1, ${filter}) and model2.uuid='${file.id}'`;
						filter += ` and model2.uuid='${file.id}'`;
						filterExp = `exists select(model2, ${filter})`;
					}
					else {
						unmatchFilter = `model2.uuid='${file.id}'`;
						filterExp = '1>2';
					}
					hard_label_filter = { exp: filterExp };
					// 查询匹配数量
					let queryInfo: QueryInfo = {
						fields: [{ name: "count", exp: `count(model1.${result.targetField})` }],
						filter: [{ exp: filterExp }],
						sources: [{
							id: "model1",
							path: bo.getFileInfo().path
						}, {
							id: "model2",
							path: EXCEL_MODEL_PATH + resId + ".tbl"
						}]
					};
					//获取匹配主键
					/*label_match_primaryKey = {
						fields: [{ name: `${result.targetField}`, exp: `model1.${result.targetField}` }],
						filter: [{ exp: filterExp }],
						sources: [{
							id: "model1",
							path: bo.getFileInfo().path
						}, {
							id: "model2",
							path: EXCEL_MODEL_PATH + resId + ".tbl"
						}]
					};*/
					//未匹配数量
					unmatchQuery = {
						fields: [{ name: "count", exp: `count(model2.${matchField})` }],
						filter: [{ exp: unmatchFilter }],
						sources: [{
							id: "model1",
							path: bo.getFileInfo().path
						}, {
							id: "model2",
							path: EXCEL_MODEL_PATH + resId + ".tbl"
						}]
					};
					return getQueryManager().queryData(queryInfo, uuid());
				}).then((result: QueryDataResultInfo) => {
					rowCount = <number>result.data[0][0];
					return getQueryManager().queryData(unmatchQuery, uuid());
				}).then((result: QueryDataResultInfo) => {
					let unMatchCount = <number>result.data[0][0];
					showConfirmDialog({
						caption: "提示",
						message: `本次Excel共导入${excelResult.rowCount}条数据；其中${excelResult.rowCount - unMatchCount}条数据共匹配到${fileName}中${rowCount}条记录，剩余${unMatchCount}条数据未匹配到信息`,
						onok: () => {
							// 进入到导入excel结果界面
							let domFileViewer = builder.getRenderer().domBase.closest('.metafileviewer-base');
							if (!domFileViewer) {
								return;
							}
							let fileViewer: MetaFileViewer = domFileViewer.szobject as MetaFileViewer;
							return fileViewer.showDrillPage({
								url: {
									path: getAppPath(`/data-label/数据打标/导入excel.spg`),
									params: {
										':runtimecompile': true,
										'label': label, 'lib': lib, 'table': resId, 'filterExp': filterExp, 'unmatchFilter': unmatchFilter, targetFields: excelResult.fields, fileFields: excelResult.fileFields
									}
								},
								breadcrumb: true,
								target: ActionDisplayType.Container,
								title: `导入Excel数据`
							});
						}
					});
				})
			}
		})
	}
}
let dgRuleFieldDp: CustomRuleFieldDp;
/**
 * 数据检测spg页面定制脚本
 */
const DG_CustomActions = {
	button_dg_batchImportStan_new(obj) {
		let data = [];
		let ids = obj.params.param7;
		let names = obj.params.param1;
		if (ids.length > 1) {
			showConfirmDialog({
				caption: '提示',
				message: `只能勾选一条资源`,
				buttons: ["ok"]
			})
			return false;
		}
		if (ids.length <= 0) {
			showConfirmDialog({
				caption: '提示',
				message: `您还未选择资源`,
				buttons: ["ok"]
			})
			return false;
		}
		for (let i = 0; i < ids.length; i++) {
			data.push({
				id: ids[i],
				caption: names[i]
			})
		}
		return rc({
			url: getAppPath(`/data-govern/dataGovern.action?method=batchImportStandard_new`),
			method: 'POST',
			data: {
				datas: JSON.stringify(data)
			}
		}).then((result: any) => {
			let state = result.state;
			let ignoreResInfo = result.ignoreResInfo;
			let addResInfo = result.addResInfo;
			if (ignoreResInfo && ignoreResInfo.length > 0) {
				let info_ignore_arr = [];
				for (let i = 0; i < ignoreResInfo.length; i++) {
					let info = ignoreResInfo[i];
					info_ignore_arr.push(`<li>${i + 1}:模型名称：${info.name}，地址:${info.path}</li>`)
				}
			}
			else if (addResInfo && addResInfo.length > 0) {
				// 	showConfirmDialog({
				// 		caption: '提示',
				// 		message: `批量导入数据表标准成功，已成功导入${addResInfo.length}条资源`,
				// 		buttons: ["ok"]
				// 	})
			} else {
				showConfirmDialog({
					caption: '提示',
					message: `勾选的标准没有关联实例表，无须导入`,
					buttons: ["ok"]
				})
				return false;
			}
		})
	},
	/**
	 * 根据自定义规则进行检测
	 * 20210702 liuyz
	 * 刷新交互会有问题现在脚本中触发模型刷新
	*/
	button_dg_customRule: (event: InterActionEvent) => {
		let builder = (<any>event.page).getBuilder();
		let params = event.params;
		let models = builder.getModels();
		let needRefreshPath: DwDataChangeEvent[] = [];
		models.forEach(model => {
			let path = model.path;
			let json: DwDataChangeEvent = {
				path,
				type: DataChangeType.refreshall
			};
			needRefreshPath.push(json);
		});
		let rule_id = params["rule_id"];
		let promise = checkCstmRule({ ruleId: rule_id }, uuid());
		showProgressDialog({
			caption: "自定义检测执行日志",
			uuid: rule_id,
			promise: promise,
			buttons: [{
				id: "cancel",
				caption: "关闭"
			}],
			logsVisible: true,
			width: 700,
			height: 500
		});
		return promise.then(() => {
			throwInfo("检测完毕");
			getDwTableDataManager().updateCache(needRefreshPath, true);
		})
	},
	/**根据内置规则进行检测 */
	button_dg_nzgzRule: (event: InterActionEvent) => {
		let builder = (<any>event.page).getBuilder();
		let params = event.params;
		let rule_id = params['rule_id'];
		let resid = params['resid'];
		let list = rule_id.split(".");
		let models = builder.getModels();
		let needRefreshPath: DwDataChangeEvent[] = [];
		models.forEach(model => {
			let path = model.path;
			let json: DwDataChangeEvent = {
				path,
				type: DataChangeType.refreshall
			};
			needRefreshPath.push(json);
		});
		let promise = checkField({ fieldName: list[2], ruleId: list[1], ruleFrom: list[0], resId: resid }, uuid());
		showProgressDialog({
			caption: "内置规则检测执行日志",
			uuid: rule_id,
			promise: promise,
			buttons: [{
				id: "cancel",
				caption: "关闭"
			}],
			logsVisible: true,
			width: 700,
			height: 500
		});
		return promise.then(() => {
			throwInfo("检测完毕");
			getDwTableDataManager().updateCache(needRefreshPath, true);
			return true;
		})
	},
	/**检测任务中执行整个任务的检测*/
	button_dg_task_Rule: (event: InterActionEvent) => {
		let builder = (<any>event.page).getBuilder();
		let path = builder.getModel("model4").path;
		let urlParam = event.page.getUrlParams();
		let task_id = urlParam.task_id;
		let promise = runCheckTask(task_id, uuid());
		showProgressDialog({
			caption: "任务执行日志",
			uuid: task_id,
			promise: promise,
			buttons: [{
				id: "cancel",
				caption: "关闭"
			}],
			logsVisible: true,
			width: 700,
			height: 500
		});
		return promise.then(() => {
			throwInfo("检测完毕");
			getDwTableDataManager().updateCache([{ path: path, type: "refreshall" }], true);
		})
	},
	button_dg_task_fromview: (event: InterActionEvent) => {
		let params = event.params;
		let builder = (<any>event.page).getBuilder();
		let models = builder.getModels();
		let needRefreshPath: DwDataChangeEvent[] = [];
		models.forEach(model => {
			let path = model.path;
			let json: DwDataChangeEvent = {
				path,
				type: DataChangeType.refreshall
			};
			needRefreshPath.push(json);
		});
		let task_id = params.task_id;
		let promise = runCheckTask(task_id, uuid());
		showProgressDialog({
			caption: "任务执行日志",
			uuid: task_id,
			promise: promise,
			buttons: [{
				id: "cancel",
				caption: "关闭"
			}],
			logsVisible: true,
			width: 700,
			height: 500
		});
		return showWaiting(promise).then(() => {
			throwInfo("检测完毕");
			getDwTableDataManager().updateCache(needRefreshPath, true);
		})
	},
	/**检测任务中某个单独的规则 */
	button_dg_task_one_Rule: (event: InterActionEvent) => {
		let builder = (<any>event.page).getBuilder();
		let rule_id = event.dataRow.RULE_ID;
		let path = builder.getModel("model1").path;
		return rule_id && showWaiting(checkCstmRule({ ruleId: rule_id }, uuid()).then(() => {
			throwInfo("检测完毕");
			getDwTableDataManager().updateCache([{ path: path, type: "refreshall" }], true);
		}))
	},
	/**提交自定规则前检测别名是否正确 */
	button_dg_check_custom_bm: (event: InterActionEvent) => {
		let page = event.page;
		let combobox = page.getComponent('input12');
		let input = event.page.getComponent("input4");
		let inputValue = input.getValue();
		let typeCombobox = page.getComponent('combobox5');
		let type = typeCombobox.getValue();
		let value = combobox.getValue();
		if (value && type == 'EXP') {
			let expCompiler = new TestCompiler({});
			let expression = expCompiler.compile(value);
			let tokens = expCompiler.getAllParentVar(value);
			console.log("tokens：" + JSON.stringify(tokens));
			return rc({
				url: getAppPath(`/data-govern/dataGovern.action?method=updateRuleRef`),
				method: 'POST',
				data: {
					names: tokens.join(",")
				}
			}).then((data: JSONObject) => {
				if (data && !data.result) {
					let name = data.name_error.join(",");
					showConfirmDialog({
						caption: '提示',
						message: `资源或规则表达式书写错误，请检测资源和表达式`,
						buttons: ["ok"]
					})
					return false;
				}
				return true;
			});
		} else if (inputValue) {
			return rc({
				url: getAppPath(`/data-govern/dataGovern.action?method=updateRuleRef`),
				method: 'POST',
				data: {
					names: inputValue
				}
			}).then((data: JSONObject) => {
				if (data && !data.result) {
					let name = data.name_error.join(",");
					showConfirmDialog({
						caption: '提示',
						message: `别名 ${name} 无效，请检查对应检测资源是否存在`,
						buttons: ["ok"]
					})
					return false;
				}
				return true;
			});
		}
		return true;
	},
	/**在创建检测任务前，将内置规则的别名写入关联关系表中 */
	button_dg_task_bm: (event) => {
		let page = event.page;
		let list = page.getComponent("list2");
		let value = list.getCheckedDataRows();
		let tokens = [];
		value.forEach(v => {
			if (tokens.length == 0 || tokens.indexOf(v['BM']) == -1) {
				tokens.push(v['BM']);
			}
		})
		if (tokens.length > 0) {
			return rc({
				url: getAppPath(`/data-govern/dataGovern.action?method=updateRuleRef`),
				method: 'POST',
				data: {
					names: tokens.join(",")
				}
			}).then((data: JSONObject) => {
				if (data && !data.result) {
					let name = data.name_error.join(",");
					showConfirmDialog({
						caption: '提示',
						message: `别名 ${name} 无效，请检查对应检测资源是否存在`,
						buttons: ["ok"]
					})
					return false;
				}
				return true;
			});
		}
		return true;
	},
	/**在检测任务列表中检测任务 */
	button_dg_check_task_fromList: (event: InterActionEvent) => {
		let task_id = event.dataRow.CHECK_TASK_ID;
		let builder = (<any>event.page).getBuilder();
		let path = builder.getModel("model1").path;
		showConfirmDialog({
			message: "确认要执行选择的检测任务吗？",
			onok: (event, dialog) => {
				let promise = runCheckTask(task_id, uuid());
				showProgressDialog({
					caption: "任务执行日志",
					uuid: task_id,
					promise: promise,
					buttons: [{
						id: "cancel",
						caption: "关闭"
					}],
					logsVisible: true,
					width: 700,
					height: 500
				});
				return showWaiting(promise).then(() => {
					throwInfo("检测完毕");
					getDwTableDataManager().updateCache([{ path: path, type: "refreshall" }], true);
				})
			}
		})
	},
	/**在检测规则中删除自定义规则 */
	button_dg_deleteCSTMRule: (event: InterActionEvent) => {
		let dataRow = event.dataRow;
		let rule_id = dataRow['RULE_ID'];
		let rule_name = dataRow['RULE_NAME'];
		let dataset = event.page.getDataset('model3');
		let queryInfo: QueryInfo = {
			sources: [{
				id: "model1",
				path: "/sysdata/data/tables/dg/DG_CHECK_TASK_RULES.tbl"
			}],
			fields: [{
				name: "CHECK_TASK_ID",
				exp: "model1.CHECK_TASK_ID"
			}],
			filter: [{
				exp: `model1.RULE_ID='${rule_id}'`
			}],

		}
		getQueryManager().queryData(queryInfo, uuid()).then(result => {
			let data = result.data;
			if (!isEmpty(data)) {
				showConfirmDialog({
					caption: '提示',
					message: `该检测自定义规则有检测任务，不允许删除，请先删除检测任务再删除自定义规则`,
					buttons: ['ok']
				});
			} else {
				showConfirmDialog({
					caption: '确认删除',
					message: `确认要删除${rule_name}这个自定义规则吗？`,
					onok: () => {
						rc({
							url: getAppPath("/commons/commons.action?method=updateDwTableData"),
							method: "POST",
							data: {
								tablePath: "/sysdata/data/tables/dg/DG_RULES.tbl",
								datapackage: {
									deleteRows: [[rule_id]]
								}
							}
						}).then(() => {
							dataset.refresh({ notifyChange: true, force: true });
							throwInfo(`删除自定义规则${dataRow['RULE_NAME']}成功`);
						});
					}
				});
			}
		})
	},
	/**删除某个检测任务 */
	button_dg_deleteCheckTask: (event: InterActionEvent) => {
		let dataRow = event.dataRow;
		let task_id = dataRow['CHECK_TASK_ID'];
		let task_name = dataRow['CHECK_TASK_NAME'];
		let dataset = event.page.getDatasets()[0];
		showConfirmDialog({
			caption: '确认删除',
			message: `确认要删除${task_name}这个检测任务吗？`,
			onok: () => {
				rc({
					url: getAppPath(`/data-govern/dataGovern.action?method=deleteTask`),
					method: 'POST',
					data: {
						task_id: task_id
					}
				}).then(() => {
					dataset.refresh({ notifyChange: true, force: true });
					throwInfo(`${task_name}该检测任务已经删除`);
				})
			}
		})
	},
	/**新建一个检测结构任务，往检测任务规则表中添加值 */
	button_dg_createStructureRule: (event: InterActionEvent) => {
		let data = event.dataRow;
		let task_id = data.input1.value;
		let floatpanel = event.page.getComponent("floatpanel1");
		let floatDetailInfo = floatpanel.getFloatInstances();
		let insertRow = [];
		let bmList = [];
		floatDetailInfo.forEach(info => {
			let dataViewRow = info.getDataViewRow();
			let rowData = dataViewRow.getData();
			let checkElem = rowData.checkbox1.getValue();
			let checkSTD = rowData.checkbox2.getValue();
			let resid = rowData.text23.getValue();
			let bm = rowData.text24.getValue();
			bmList.push(bm);
			if (checkElem == '1') {
				let row = [];
				row.push(task_id);
				row.push(resid);
				row.push('ELEM');
				row.push('STRUCT');
				insertRow.push(row);
			}
			if (checkSTD == '1') {
				let row = [];
				row.push(task_id);
				row.push(resid);
				row.push('STD');
				row.push('STRUCT');
				insertRow.push(row);
			}
		});
		rc({
			url: getAppPath(`/data-govern/dataGovern.action?method=updateRuleRef`),
			method: 'POST',
			data: {
				names: bmList.join(",")
			}
		})
		return rc({
			url: getAppPath("/commons/commons.action?method=updateDwTableData"),
			method: "POST",
			data: {
				tablePath: "/sysdata/data/tables/dg/DG_CHECK_TASK_RULES.tbl",
				datapackage: {
					addRows: [{
						fieldNames: ['CHECK_TASK_ID', 'DW_TABLE_ID', 'RULE_FROM', 'RULE_ID'],
						rows: insertRow
					}]
				}
			}
		}).then(() => {
			return true;
		})
	},
	/**检查是否勾选了数据 */
	button_dg_check_select_row: (event: InterActionEvent) => {
		let floatpanel = event.page.getComponent("floatpanel1");
		let floatDetailInfo = floatpanel.getFloatInstances();
		let result = false;
		floatDetailInfo.forEach(info => {
			let dataViewRow = info.getDataViewRow();
			let rowData = dataViewRow.getData();
			let checkElem = rowData.checkbox1.getValue();
			let checkSTD = rowData.checkbox2.getValue();
			if (checkElem == '1') {
				result = true;
			}
			if (checkSTD == '1') {
				result = true;
			}
		});
		if (result == false) {
			showConfirmDialog({
				caption: "未勾选数据",
				message: "暂未勾选数据，请求数据后再提交。",
				buttons: ["cancel"]
			})
		}
		return result;
	},
	/**新建一个检测任务类型的计划任务 */
	button_dg_create_check_schedule: (event: InterActionEvent) => {
		let list6 = event.page.getComponent("list6");
		let rows = list6.getDataRows();
		let scheduleNames = [];
		rows.forEach(r => {
			scheduleNames.push(r['NAME']);
		})
		let valid = (value: string) => {
			let name = value.trim();

			if (scheduleNames.includes(name)) {
				return {
					result: false,
					errorMessage: message("schedulemgr.schedule.name.alreadyExistMsg")
				}
			};
		}
		let nameValidateArgs = {
			required: true,
			validRegex: valid
		}
		showDialog({
			id: "schedulemgr.dilaog.newchedule",
			buttons: ["ok", "cancel"],
			needWaitInit: true,
			resizable: false,
			content: {
				implClass: ScheduleEditor,
				nameValidateArgs: nameValidateArgs
			},
			onshow: (e: SZEvent, dialog: Dialog) => {
				let editor = <ScheduleEditor>dialog.content;
				editor.form.getFormItem("type").setVisible(false)
				editor.setData({ enable: true, concurrency: false, notifyWhen: "never", retryEnable: true, retryMaxtimes: 1, retryInterval: 1, threadsCount: 2, type: "check" });
			},
			onok: (e: SZEvent, dialog: Dialog) => {
				let editor = <ScheduleEditor>dialog.content;
				if (!editor.validate()) {
					return false;
				}
				dialog.waitingClose(getScheduleMgr().addSchedule((<ScheduleEditor>dialog.content).getData()));
			}
		})
	},
	/**
	 * liuyz 20211018
	 * 获取当前数据源的最新数据
	 *
	*/
	button_dg_fetchExistData: (event: InterActionEvent) => {
		let params = event.params;
		let taskId = params.taskId;
		let modelId = params.modelId;
		let type = params.type;
		let storeDataset = event.page.getDataset(modelId);
		let comp = event.page.getComponent(type == "nzgz" ? "list2" : "list5");
		let compData = comp.getDataRows();
		let queryInfo: QueryInfo = {
			sources: [{
				id: "model1",
				path: "/sysdata/data/tables/dg/DG_CHECK_TASK_RULES.tbl",
				filter: `model1.CHECK_TASK_ID ='${taskId}' and model1.RULE_FROM ${type == 'nzgz' ? "!='CSTM'" : "='CSTM'"}`
			}],
			fields: [{ name: "RULE_ID", exp: "model1.RULE_ID" }, { name: "RULE_FROM", exp: "model1.RULE_FROM" }, { name: "CHECK_TASK_ID", exp: "model1.CHECK_TASK_ID" }, { name: "DW_TABLE_ID", exp: "model1.DW_TABLE_ID" }]
		};
		return getQueryManager().queryData(queryInfo, uuid()).then(result => {
			let datas = result.data;
			if (isEmpty(compData)) {//列表本身没有数据，表示没有任何规则可以选择，此时不进行任何操作
				return;
			}
			let arrInsert: DatasetDataPackageRowInfo[] = [];
			datas.forEach(data => {
				let row: DatasetDataPackageRowInfo = {
					data: { "RULE_ID": data[0], "RULE_FROM": data[1], "CHECK_TASK_ID": data[2], "DW_TABLE_ID": data[3] }
				}
				arrInsert.push(row);
			});
			storeDataset.saveDraft(arrInsert);
		})
	},
	/**
	 * 20211101 liuyz
	 * 展示自定义规则表达式对话框
	 */
	button_dg_ruleExp_showExpDialog: (event: InterActionEvent) => {
		let page = event.page;
		let params = event.params;
		let currentSource = params.currentSource;
		function showExp() {
			dgRuleFieldDp.setcurrentSource(currentSource);
			dgRuleFieldDp.setAssginSource(null);
			const displayInput = page.getComponent('multipleinput1');
			const trueExpInput = page.getComponent('input12');
			showExpDialog({
				readonly: false,
				newborn: true,
				defaultTab: "fields",
				expRequired: true,
				buttons: ['ok'],
				expDataProvider: dgRuleFieldDp.getExpDp(),
				descValidateArgs: {
					required: true,
				},
				fieldPanelImpl: {
					depends: "dsn/datasourceeditor",
					implClass: "DataSourceEditor",
					sourceTheme: SourceTheme.Combobox
				},
				fieldsDataProvider: dgRuleFieldDp,
				caption: "设置自定义规则表达式",
				value: {
					exp: trueExpInput.getValue()
				},
				needTransformValue: true,
				onok: function(e: SZEvent, dialog: ExpDialog) {
					displayInput.setValue(dialog.exped.originValue);
					trueExpInput.setValue(dialog.getValue().exp);
				}
			});
		}
		if (dgRuleFieldDp == null) {
			dgRuleFieldDp = new CustomRuleFieldDp();
			dgRuleFieldDp.initTableAndFieldsInfo().then(() => {
				showExp();
			});
		} else {
			showExp();
		}

	},
	/**
	 * 20211101 liuyz
	 * 设置错误显示表达式对话框
	 */
	button_dg_errExp_showExpDialog: (event: InterActionEvent) => {
		let page = event.page;
		let params = event.params;
		let sourceId = params.sourceId;
		let errInputComp = page.getComponent("multipleinput3");
		if (dgRuleFieldDp == null) {
			dgRuleFieldDp = new CustomRuleFieldDp();
		}
		dgRuleFieldDp.setAssginSource(sourceId);
		showExpDialog({
			readonly: false,
			newborn: true,
			defaultTab: "fields",
			expRequired: true,
			buttons: ['ok'],
			expDataProvider: dgRuleFieldDp.getExpDp(),
			descValidateArgs: {
				required: true,
			},
			contentType: ExpContentType.MACRO,
			fieldPanelImpl: {
				depends: "dsn/datasourceeditor",
				implClass: "DataSourceEditor",
				sourceTheme: SourceTheme.Combobox
			},
			fieldsDataProvider: dgRuleFieldDp,
			caption: "设置错误显示表达式",
			value: {
				exp: errInputComp.getValue()
			},
			needTransformValue: true,
			onok: function(e: SZEvent, dialog: ExpDialog) {
				errInputComp.setValue(dialog.getValue().exp);
			}
		});
	},
	/**
	 * 设置规则详情描述到控件中
	 */
	button_setRuleDesc: (event: InterActionEvent) => {
		let page = event.page;
		let input = page.getComponent("input13");
		let jsComponent = page.getComponent("jsComponent1");
		let value = jsComponent.component.innerComponent.getValue();
		input.setValue(JSON.stringify(value));
	}
}

/**
 * 数据接入spg定制脚本
 */
const Access_CustomActions = {
	/**新增数据源对话框 */
	button_show_dataSource: (event: InterActionEvent) => {
		let pName = event.page.builder.projectName;
		addDataSource(pName);
	},
	/**删除数据源 */
	button_delete_dataSource: (event: InterActionEvent) => {
		let name = event.params.name;
		deleteDataSource(name);
	},
	/**克隆数据源 */
	button_clone_dataSource: (event: InterActionEvent) => {
		let name = event.params.name;
		let pName = event.page.builder.projectName;
		cloneDataSource(pName, name);
	},
	/**编辑数据源 */
	button_edit_dataSource: (event: InterActionEvent) => {
		let pName = event.page.builder.projectName;
		let name = event.params.name;
		let dbType = event.params.dbType;
		let desc = event.params.desc;
		let ip = event.params.ip;
		let sid = event.params.sid;
		let user = event.params.user;
		let url = event.params.url;
		let dataType = event.params.dataType;
		let port = event.params.port;
		let purpose = event.params.purpose;
		let connectionInfo: DbConnectionInfo = {
			name: name,
			dbType: dbType,
			host: ip,
			sid: sid,
			user: user,
			desc: desc,
			url: url,
			port: port,
			purpose: purpose
		};
		editDataSource(pName, connectionInfo, dataType);
	},
	/**刷新数据 */
	button_refresh_dataSource: (event: InterActionEvent) => {
		getDwTableDataManager().updateCache([{ path: '/sysdata/data/tables/sdi/data-access/script/SDI_DATA_ACCESS_DATASOURCE_SYSTEM.tbl', type: 'refreshall' }], true);
	},
	/**引入系统数据源 */
	button_import_sys_datasource: (event: InterActionEvent) => {
		let name = event.params.name;
		let dataType = event.params.dataType;
		rc({
			url: getAppPath("/data-access/dataSourceMgr.action?method=addSysDatasource"),
			method: "POST",
			data: {
				name: name,
				dataType: dataType
			}
		}).then(() => {
			getDwTableDataManager().updateCache([{ path: TABLE_INFO, type: 'refreshall' }], true);
		});
	},
	/**创建一个计划任务 */
	button_create_schedule: (event: InterActionEvent) => {
		let list6 = event.page.getComponent("list1");
		let rows = list6.getDataRows();
		let scheduleNames = [];
		rows.forEach(r => {
			scheduleNames.push(r['NAME']);
		})
		let valid = (value: string) => {
			let name = value.trim();

			if (scheduleNames.includes(name)) {
				return {
					result: false,
					errorMessage: message("schedulemgr.schedule.name.alreadyExistMsg")
				}
			};
		}
		let nameValidateArgs = {
			required: true,
			validRegex: valid
		}
		showDialog({
			id: "schedulemgr.dilaog.newchedule",
			buttons: ["ok", "cancel"],
			needWaitInit: true,
			resizable: false,
			content: {
				implClass: ScheduleEditor,
				nameValidateArgs: nameValidateArgs
			},
			onshow: (e: SZEvent, dialog: Dialog) => {
				let editor = <ScheduleEditor>dialog.content;
				editor.form.getFormItem("type").setVisible(false)
				editor.setData({ enable: true, concurrency: false, notifyWhen: "never", retryEnable: true, retryMaxtimes: 1, retryInterval: 1, threadsCount: 2, type: "etl" });
			},
			onok: (e: SZEvent, dialog: Dialog) => {
				let editor = <ScheduleEditor>dialog.content;
				if (!editor.validate()) {
					return false;
				}
				dialog.waitingClose(getScheduleMgr().addSchedule((<ScheduleEditor>dialog.content).getData()));
			}
		})
	},
	/**创建一个目标物理表 */
	button_create_targetDbTable: (event: InterActionEvent) => {
		let params = event.params;
		let sourceTable = params.sourceTable;
		let targetTable = params.targetTable;
		let targetTableDesc = params.targetTableDesc;
		let sourceDataBase = params.param2;
		let targetDataBase = params.param3;
		return rc({
			url: getAppPath('/data-access/dataAccessMgr.action?method=createDbTable'),
			data: {
				sourceTable: sourceTable,
				targetTable: targetTable,
				targetTableDesc: targetTableDesc,
				sourceDataBase: sourceDataBase,
				targetDataBase: targetDataBase
			}
		}).then(result => {
			if (result.result) {
				showSuccessMessage("创建成功");
				getDwTableDataManager().updateCache([{ path: "/sysdata/data/tables/sdi/data-access/SDI_DATA_ACCESS_SOURCE_TABLE.tbl", type: "refreshall" }, {
					path: "/sysdata/data/tables/sdi/data-access/SDI_DATA_ACCESS_FIELDS_MIDDLE.tbl", type: "refreshall"
				}, { path: "/sysdata/data/tables/sdi/data-access/script/SDI_DATA_ACCESS_FIELDS_LIST.tbl", type: "refreshall" }], true);
				return true;
			} else {
				showErrorMessage(result.message);
				return false;
			}
		})
	},
	/**运行一个指定的加工 */
	button_runETL: (event: InterActionEvent) => {
		let builder = (<any>event.page).builder;
		let params = event.params;
		let taskId = params.taskId;
		let schedule = params.schedule;
		let now = new Date().getTime();
		return getScheduleMgr().runTasks([taskId]).then(() => {
			return getScheduleMgr().getSchedule(schedule).then((info) => {
				let scheduleLogId = info.lastScheduleLogId;
				showWaitingMessage("正在执行中");
				let doCheckState = (): Promise<boolean> => {
					return getScheduleMgr().queryScheduleTaskLogs(schedule, scheduleLogId, false).then(logs => {
						let result = false;
						for (let i = 0; i < logs.length; i++) {
							let log = logs[i];
							if (log.state != 1) {
								result = true;
							}
						}
						if (!result) {
							let eventList: DwDataChangeEvent[] = [{ path: "/sysdata/data/tables/sdi/data-access/SDI_DATA_ACCESS_VIEW_TABLE.tbl", type: "refreshall" },
							{ path: "/sysdata/data/tables/sdi/data-access/SDI_DA_DW_TASK_LOG_EXT.tbl", type: "refreshall" },
							{ path: "/sysdata/data/tables/sdi/data-access/SDI_DATA_ACCESS_DW_TASK_EXT.tbl", type: "refreshall" }]
							waitRender(null, 500);
							getDwTableDataManager().updateCache(eventList, true);
							hideMessage();
							showSuccessMessage("执行成功！");
						}
						return result;
					})
				}
				let time = timerv({
					interval: 1000,
					action: doCheckState
				});
			})
		});
	},
	/**通过计划任务的接口来运行一个或一批加工，这样可以获取到日志信息 */
	button_runETLs: (event: InterActionEvent) => {
		let params = event.params;
		let id = params.selectId;
		if (id == null) {
			showWarningMessage("请勾选至少一个再运行");
			return;
		}
		let idList = id.split(",");
		const now = Date.now();
		getScheduleMgr().runTasks(idList).then(async () => {
			const promise = new EtlRunTaskPromise({
				uuid: uuid(),
				startTime: now,
				tasks: idList
			})
			showProgressDialog({
				id: "globalLogsDialog",
				caption: message("commons.logs.dialog.title"),
				uuid: uuid(),
				promise: promise,
				expand: true,
				height: 550
			})
		});
	},
	/**查看最近一次执行的日志 */
	button_viewETLlog: (event: InterActionEvent) => {
		let params = event.params;
		let schedule = params.schedule;
		let logId = params.logId;
		let taskId = params.taskId;
		return getScheduleMgr().queryScheduleTaskLogs(schedule, logId).then((result: TaskLogInfo[]) => {
			let data: TaskLogInfo;
			for (let i = 0; i < result.length; i++) {
				let r = result[i];
				if (r.taskid == taskId) {
					data = r;
					break;
				}
			}
			let logs = data.logs;
			let log = logs ? JSON.parse(logs).logs : [];
			showDialog({
				id: "globalLogsDialog",
				caption: message("commons.logs.dialog.title"),
				content: { implClass: ProgressLogs },
				height: 550
			}).then((dialog: Dialog) => {
				let logs = <ProgressLogs>dialog.content;
				logs.clear();
				logs.addLogs(log);
				return Promise.resolve(dialog);
			});
		})
	},
	/**显示来源表的自由查询界面 */
	button_showDwTableFilter: (event: InterActionEvent) => {
		let params = event.params;
		let tableId = params.tableId;
		let sourceDataBase = params.sourceDataBase;
		let sourceTable = params.sourceTable;
		let path = `/sysdata/data/tables/sdi/data-access/tmp/${tableId}.tbl`;
		return getMetaRepository().getFile(path, false, false, false).then((fileInfo: MetaFileInfo) => {
			let promise = Promise.resolve();
			if (!fileInfo) {//临时模型不存在
				promise = importDbTables({
					projectName: 'sysdata',
					tables: [{ datasource: sourceDataBase, dbTable: sourceTable, name: tableId, desc: sourceTable }],
					targetFolder: '/sysdata/data/tables/sdi/data-access/tmp',
					connectionType: 'readonly',
					taskId: uuid()
				})
			}
			return promise
		}).then(() => {
			return true;
		})
	},
	button_getEasyFilter: (event: InterActionEvent) => {
		let params = event.params;
		let compName = params.compName;
		let emb = event.page.getComponent('embeddatamodel1');
		let dwTableEditor = emb.component.innerComponent;
		let easyFilter = <EasyFilter>dwTableEditor.getPanels().getCurrentPanel().getComponent("easyFilter");
		let value = easyFilter.getValue();
		let input = event.page.getComponent(compName);
		input.setValue(JSON.stringify(value));
	},
	//在配置完成后，需要显示一个等待的icon
	showWaitingIconBeforeFinish: (event: InterActionEvent) => {
		showWaitingMessage('保存中');
	},
	hideWaitingIconAfterFinish: (event: InterActionEvent) => {
		hideMessage();
	},
	//自动匹配相同名字的字段，并且刷新模型
	button_ac_matchField: (event: InterActionEvent) => {
		let params = event.params;
		let dataBaseId = params.dataBaseId;
		let tableId = params.tableId;
		rc({
			url: "/zt/dataAccess?method=matchTargetandSourceFields",
			data: {
				dataBaseId, tableId
			}
		}).then(result => {
			if (result) {
				let dwDataEvent: DwDataChangeEvent[] = [{
					path: "/sysdata/data/tables/sdi/data-access/SDI_DATA_ACCESS_FIELDS_MIDDLE.tbl", type: "refreshall"
				}, {
					path: "/sysdata/data/tables/sdi/data-access/FIELDS_CONF_TMP.tbl", type: "refreshall"
				}]
				getDwTableDataManager().updateCache(dwDataEvent, true);
			}
		})
	},
	//将仅存在于来源库的表写入表中
	button_ac_insertNoMatchTable: (event: InterActionEvent) => {
		let params = event.params;
		let sourceDb = params.sourceDb;
		let targetDb = params.targetDb;
		return rc({
			url: "/zt/dataAccess?method=insertNoMatchTable",
			data: {
				sourceDb, targetDb
			}
		}).then(result => {
			if (result) {
				getDwTableDataManager().updateCache([{ path: "/sysdata/data/tables/sdi/data-access/SDI_NO_MATCH_TABLE.tbl", type: "refreshall" }], true);
				return true;
			}
		})
	},
	//自动匹配来源库和目标库的内容
	button_ac_autoMatchTable: (event: InterActionEvent) => {
		let params = event.params;
		let sourceDb = params.sourceDb;
		let targetDb = params.targetDb;
		return rc({
			url: "/zt/dataAccess?method=matchTargetandSourceTables",
			data: {
				sourceDb, targetDb
			}
		}).then(result => {
			if (result) {
				getDwTableDataManager().updateCache([{ path: "/sysdata/data/tables/sdi/data-access/SDI_AUTO_MATCH_TABLE.tbl", type: "refreshall" }], true);
				return true;
			}
		})
	},
	//打开一个表达式编辑对话框
	button_ac_showExpDialog: (event: InterActionEvent) => {
		let params = event.params;
		let dataBase = params.dataBase;
		let dbTable = params.dbTable;
		let tableId = params.tableId;
		let fieldName_ = params.fieldName;
		let fieldExp = params.fieldExp;
		let oldFieldName = params.oldFieldName;
		let dp = new CustomFieldDp({ dataBase, dbTable });
		let valid = (value: string) => {
			let name = value.trim();
			let errorMessage = dp.getExpDp().validateFieldName(getCamelChars(name, false).toUpperCase());
			if (errorMessage) {
				return {
					result: false, errorMessage
				}
			}
		}
		/**
		 * 获取表达式对话框默认值
		 */
		let getDefaultValue = () => {
			let defaultName = fieldName_;
			let dataProvider = dp.getExpDp();
			if (dataProvider && !defaultName) {//如果没有传入字段名称，则这里自动生成一个字段默认名称(自动尾数增加)
				return rc({
					url: getAppPath('/data-access/dataAccessMgr.action?method=getSourceFIelds'),
					data: {
						tableId, dbTable
					}
				}).then((result: string[]) => {
					let fields = result;
					let newName = message("dataflow.nodeImpl.coltorow.name", "");
					let namePrefix = getCamelChars(newName, false).toUpperCase();
					let pl = namePrefix.length;
					let nameSuffix = 0;
					let flag = false;
					fields.forEach(f => {
						let name = f;
						if (name.indexOf(namePrefix) == 0 && name.length >= pl) {
							if (name.length == pl) {
								flag = true;
								return;
							}
							let s = name.substr(pl);
							let num = parseInt(s);
							if (isNaN(num) || num <= nameSuffix) {
								return;
							}
							nameSuffix = num;
						}
					});
					defaultName = (nameSuffix == 0 && !flag) ? newName : newName + (nameSuffix + 1);
					return {
						exp: fieldExp == null ? "" : fieldExp,
						extractable: true,
						name: defaultName
					}
				})
			} else {
				return Promise.resolve({
					exp: fieldExp,
					extractable: true,
					name: defaultName
				})
			}
		}
		showExpDialog({
			readonly: false,
			newborn: true,
			defaultTab: "fields",
			newFieldEnabled: true,
			expRequired: true,
			nameValidateArgs: {
				required: true,
				errorMessage: "字段名不合法",
				validRegex: valid,
			},
			descValidateArgs: {
				required: true,
			},
			expDataProvider: dp.getExpDp(),
			fieldPanelImpl: {
				depends: "dsn/datasourceeditor",
				implClass: "DataSourceEditor",
				sourceTheme: SourceTheme.Combobox

			},
			fieldsDataProvider: dp,
			caption: "新增计算字段",
			needTransformValue: false,
			onok: function(e: SZEvent, dialog: ExpDialog) {
				let validateResult = dialog.getValidateResult();
				if (validateResult.exp == false) {
					return false;
				}
				let value = dialog.getValue();
				let fieldDesc = value.name.toUpperCase();
				let fieldName = getCamelChars(fieldDesc, false).toUpperCase();
				let exp = value.exp;
				let dataType = value.dataType;
				rc({
					url: getAppPath("/data-access/dataAccessMgr.action?method=createCalculatedField"),
					data: {
						tableId, dbTable, fieldName, fieldDesc, exp, dataType, oldFieldName
					}
				}).then(result => {
					getDwTableDataManager().updateCache([{ path: "/sysdata/data/tables/sdi/data-access/SDI_DATA_ACCESS_FIELDS_MIDDLE.tbl", type: "refreshall" }], true);
				})
			}
		}).then((dialog: ExpDialog) => {
			getDefaultValue().then(result => {
				dialog.setValue(result);
				dialog.setExpDataProvider(dp.getExpDp());
			})
		})
	},
	//复制自动匹配目标表暂存到数据表配置表中
	button_ac_copyDataToTableConf: (event: InterActionEvent) => {
		let page = event.page;
		let params = event.params;
		let sourceDb = params.sourceDb;
		let targetDb = params.targetDb;
		let dataBaseId = params.dataBaseId;
		let tableConfDataset = page.getDataset('model4');//获取待写入的数据源
		let queryInfo: QueryInfo = {
			fields: [{ name: "SOURCE_DB" }, { name: "TARGET_DB" }, { name: "SOURCE_TABLE" }, { name: "TARGET_TABLE" }],
			filter: [],
			params: [{
				name: "param2", value: sourceDb
			}, {
				name: "param3", value: targetDb
			}],
			queryId: "model26",
			resModifyTime: new Date().getTime(),
			resid: "/sysdata/app/zt.app/data-access/data-access-create.spg",
			select: true,
			sort: [],
			sources: null
		};
		return getQueryManager().queryData(queryInfo, uuid()).then(result => {
			let data = result.data;
			let insertRow: JSONObject[] = [];
			let user = getCurrentUser();
			let userId = user.userId;
			for (let index = 0; index < data.length; index++) {
				const d = data[index];
				insertRow.push({ 'UUID': uuid(), 'DATABASE_ID': dataBaseId, 'SOURCE_TABLE': d[2], 'TARGET_TABLE': d[3], 'SCHEDULE_CODE': 'default', 'POLICY': 'TOTAL', 'CREATE_TIME': new Date().getTime(), 'CREATOR': userId });
			}
			return tableConfDataset.insert(insertRow);
		})
	}
}

export const CustomJS: { [type: string]: IMetaFileCustomJS } = {

	tbl: <IDwCustomJS>{
		onDidInitPanel: (panel: IDwTableEditorPanel): void | Promise<void> => {
			let toolbar = <Toolbar>panel.getComponent("toolbar");
			// 把“选择字段”，改成“输出数据项”
			toolbar.getItem("fields").setDesc("输出字段:");
			let easyFilter = <EasyFilter>panel.getComponent("easyFilter");
			easyFilter.toolbar.getItem("clear").data.onclick = function(sze, btn) {
				easyFilter.setValue([]);
			}
			let user = getCurrentUser().userId;
			// 在工具栏上添加保存查询条件按钮和其他按钮
			toolbar.addItems([{
				id: "exportListData",
				align: ToolbarItemAlign.LEFT,
				visible: false,
				component: new Button({
					caption: "导出",
					onclick: () => {
						// 导出查询出的数据
						showFormDialog({
							id: 'spg.exportlistdata',
							caption: '导出数据',
							content: {
								items: [{
									id: 'fileName',
									caption: '文件名称',
									formItemType: 'edit',
									captionPosition: 'left',
								}, {
									id: 'format',
									formItemType: 'selectpanel',
									captionPosition: 'left',
									compArgs: {
										items: [{
											value: 'xlsx',
											caption: 'xlsx'
										}, {
											value: 'csv',
											caption: 'csv'
										}]
									}
								}, {
									id: 'range',
									formItemType: 'selectpanel',
									captionPosition: 'left',
									compArgs: {
										items: [{
											value: 'currentPage',
											caption: '当前页'
										}, {
											value: 'allPage',
											caption: '所有页'
										}]
									}
								}],
							},
							onshow: (event: SZEvent, dialog: Dialog) => {
								let form = <Form>dialog.content;
								let dwTable = panel.getDwTable();
								let filename = dwTable.getFileInfo().name;
								form.loadData({ fileName: filename.substring(0, filename.lastIndexOf('.')), format: 'xlsx', range: 'currentPage' });
							},
							buttons: [{
								id: "ok",
								layoutTheme: "defbtn",
								onclick: (event: SZEvent, dialog: Dialog, item: any) => {
									let data = (<any>dialog).form.getData();
									let dtable = panel.getViewPanel().dtable;
									let format = data.format;
									let fileName = data.fileName;
									let range = data.range;
									let promise: Promise<void>;
									let pagintor = panel.getViewPanel().paginator;
									promise = Promise.resolve(exportData(panel, fileName, format, range));
									let prop = panel.getProperties();
									let resid = prop['resid'];
									let rowCount = 0;
									if (range == "all" && format == "csv") {
										rowCount = pagintor.getTotalCount();
									} else {
										rowCount = pagintor.getPageSize();
									}
									let queryInfo: QueryInfo = {
										fields: [{ name: "EX_APPLY_UUID", exp: `model1.EX_APPLY_UUID` }],
										filter: [{ exp: `model1.DW_TABLE_ID='` + resid + `' and model1.CREATOR='` + user + `'` }],
										sources: [{
											id: "model1",
											path: "/sysdata/data/tables/sdi/data-exchange/SDI_EXCHANGE_VIEW_PERMISION.tbl"
										}]
									};
									getQueryManager().queryData(queryInfo, uuid()).then((result: QueryDataResultInfo) => {
										let data = result.data;
										let time = new Date().getTime();
										let user = getCurrentUser();
										let apply_uuid = (user.isAdmin && isEmpty(data) && data.length == 0) ? null : data[0][0];
										apply_uuid && rc({
											url: getAppPath("/commons/commons.action?method=updateDwTableData"),
											method: "POST",
											data: {
												tablePath: "/sysdata/data/tables/dx/DX_CHANNEL_LOGS.tbl",
												datapackage: {
													addRows: [{
														fieldNames: ["CHANNEL_LOG_ID", "CHANNEL_ID", "TYPE", "START_TIME", "OPERATOR", "RESULT", "ROWCOUNT"],
														rows: [[uuid(), apply_uuid, 3, time, user.userId, 1, rowCount]]
													}]
												}
											}
										})
									})
									dialog.waitingClose(promise);
								}
							}, 'cancel']
						});
					}
				})
			}, {
				id: "saveFilter",
				align: ToolbarItemAlign.LEFT,
				component: new Button({
					caption: "保存查询条件",
					onclick: () => {
						// 保存查询条件
						let dwTable = panel.getDwTable();
						let fileInfo = dwTable.getFileInfo();
						let filters = dwTable.getEasyFilter();

						let filterStr = filters && JSON.stringify(filters);
						let resid = fileInfo.id;
						//获取下拉框应该显示的值
						let queryInfo: QueryInfo = {
							fields: [{ name: "desc", exp: `model1.FILTER_DESC` }, { name: "fitler", exp: `model1.DATA_FILTER` }],
							filter: [{ exp: `model1.resid='` + resid + `' and model1.creator='` + user + `'` }],
							sources: [{
								id: "model1",
								path: TABLE_FILTER_PATH
							}]
						};
						getQueryManager().queryData(queryInfo, uuid()).then((result: QueryDataResultInfo) => {
							let data = result.data;
							let items = converFilterResultData(data);
							let listDp = new DataProvider({
								data: items
							});
							showFormDialog({
								id: 'spg.datafiltersave',
								caption: '保存查询条件',
								modal: true,
								resizable: false,
								content: {
									items: [{
										id: 'filterName',
										formItemType: 'edit',
										captionPosition: 'left',
										caption: '过滤条件名称',
										required: true
									}]
								},
								onshow: (event: SZEvent, dialog: Dialog) => {
									let form = <Form>dialog.content;
									form.loadData({ filterName: null });
								},
								buttons: [{
									id: "ok",
									layoutTheme: "defbtn",
									onclick: (event: SZEvent, dialog: Dialog, item: any) => {
										let data = (<any>dialog).form.getData();
										let time = new Date().getTime();
										let resid = fileInfo.id;
										let filterName = data.filterName;
										rc({
											url: getAppPath("/commons/commons.action?method=updateDwTableData"),
											method: "POST",
											data: {
												tablePath: TABLE_FILTER_PATH,
												datapackage: {
													addRows: [{
														fieldNames: ['RESID', 'FILTER_DESC', 'CREATOR', 'DATA_FILTER', 'CREATE_TIME'],
														rows: [[resid, filterName, user, filterStr, time]]
													}]
												}
											}
										})
									}
								}, 'cancel'],
								onok: () => {
									showSuccessMessage("保存查询条件成功");
								}
							})
						})
					}
				})
			}, {
				id: "importFilter",
				align: ToolbarItemAlign.LEFT,
				component: new Button({
					caption: "导入查询条件",
					onclick: () => {
						let dwTable = panel.getDwTable();
						let resid = dwTable.getFileInfo().id;
						//获取下拉框应该显示的值
						let queryInfo: QueryInfo = {
							fields: [{ name: "desc", exp: `model1.FILTER_DESC` }, { name: "fitler", exp: `model1.DATA_FILTER` }],
							filter: [{ exp: `model1.resid='` + resid + `' and model1.creator='` + user + `'` }],
							sources: [{
								id: "model1",
								path: TABLE_FILTER_PATH
							}]
						};
						getQueryManager().queryData(queryInfo, uuid()).then((result: QueryDataResultInfo) => {
							let data = result.data;
							let items = converFilterResultData(data);
							let listDp = new DataProvider({
								data: items
							});
							showFormDialog({
								id: 'spg.datafilterimport',
								caption: '载入查询条件',
								modal: true,
								resizable: false,
								content: {
									items: [{
										id: 'filterDesc',
										compArgs: <ComboboxArgs>{
											cleanIconVisible: true,
										},
										formItemType: 'combobox',
										captionPosition: 'left',
										caption: '过滤条件名称',
										required: true
									}]
								},
								onshow: (event: SZEvent, dialog: Dialog) => {
									let form = <Form>dialog.content;
									form.waitInit().then(() => {
										form.getFormItem('filterDesc').setDataProvider(listDp);
									})
								},
								buttons: [{
									id: "ok",
									layoutTheme: "defbtn",
									onclick: (event: SZEvent, dialog: Dialog, item: any) => {
										let data = (<any>dialog).form.getData();
										let value = data ? data.filterDesc : [];
										let comp = <EasyFilter>panel.getComponent('easyFilter');
										let labelFilter = [];
										let filters = !isEmpty(JSON.parse(value)) ? JSON.parse(value).map(filter => {
											let newFilter: FilterInfo = deepAssign({}, filter);
											newFilter.clauses = newFilter.clauses.filter(clause => {
												if (clause.leftExp == '$LABELS') {
													labelFilter.push(clause.rightValue)
												}
												return clause.leftExp != '$LABELS';
											});
											return newFilter;
										}) : [];
										dwTable.setEasyFilter(JSON.parse(value));
										comp.setValue(filters);
										panel.labelFilterComp.setValue(labelFilter);
										panel.refresh(true);//加载完查询条件后，立即查询数据
									}
								}, 'cancel'],
								onok: () => {
									showSuccessMessage("载入查询条件成功");
								}
							})
						})

					}
				})
			}])
		},
		onRefreshPanel: (panel: IDwTableEditorPanel): void | Promise<void> => {
			let dwtable = panel.getDwTable();
			let prop = panel.getProperties();
			let list = ["dimensionPathValue", "textField", "valueExp", "valueFilter", "inputField", "displayFormat", "originalField", "labels", "isAutoInc", "defaultValue", "物理字段状态", "默认展示", "defaultValueExp"];
			let filedType = prop['filedType'];
			if (filedType == 'exchange' || filedType == "MD") {//如果是数据交换或主数据页面要隐藏数据元这一列
				list.push("dataElement");
				list.push("businessDesc");
			}
			//字段列表界面不展示以下字段
			dwtable.metaProperties.invisibleFieldProperties = list;
			dwtable.setReadonly(true);

			let clickFunciton = (e: SZEvent, fieldName: string, data?: any, fieldProperty?: string) => {
				if (fieldProperty == "dataElement" && data != null) {
					showDrillPage({
						url: getAppPath('/meta-data/file-element-viewer.spg?element_id=' + data),
						title: '数据元信息',
						target: ActionDisplayType.Dialog,
						dialogArgs: {
							buttons: [{
								id: 'close',
								caption: '取消'
							}]
						},
					})
				}
			}
			prop["onclick"] = clickFunciton;
		},
		onDidRefreshPanel: (panel: IDwTableEditorPanel): void | Promise<void> => {
			let toolbar = <Toolbar>panel.getComponent("toolbar");
			toolbar.setItemVisible("saveSoftLabel", false);
			let easyFilter = <EasyFilter>panel.getComponent("easyFilter");
			ex_filter = easyFilter.getValue();
			//设置筛选框体验
			easyFilter['convertExpEnabled'] && easyFilter.setConvertExpEnabled(false);
			!easyFilter['andVisible'] && easyFilter.setAndVisible(true);
			easyFilter['operatorVisible'] == 'auto' && easyFilter.setOperatorVisible(true);

			let user = getCurrentUser().userId;
			let groups = getCurrentUser().groups;
			let isAdmin = getCurrentUser().isAdmin;
			let prop = panel.getProperties();
			let resid = prop['resid'];
			let viewer = prop[':viewer'];
			let type = prop['type'];
			let flexLayout = panel.flexLayout;
			let calcErrCount = prop['calcErrCount'];
			//加载出内置规则的检测结果页面的数据
			if (calcErrCount) {
				let text24 = document.getElementById("text24").szobject;
				let text25 = document.getElementById("text25").szobject;;
				panel.getDwTable().data.queryTotalRowCount().then(result => {
					text24.setValue({ text: "问题数据：" + result });
				})
				let path = panel.getDwTable().getFileInfo().path;
				let queryInfo: QueryInfo = {
					fields: [{ name: "totalRowCount", exp: "count()", source: "model1" }],
					sources: [{
						id: "model1",
						path: path
					}]
				};
				getQueryManager().queryData(queryInfo, uuid()).then((result) => {
					let data = result.data;
					text25.setValue({ text: "总行数：" + data[0][0] });
				})
			}
			// if (!isAdmin) {//在数据脱敏实现之前将导出按钮全部隐藏
			// 	toolbar.setItemVisible("exportListData", false);
			// }
			//根据类型去判断当前页面应该展示的内容
			if (type == 'check') {
				toolbar.setItemVisible("exportListData", true);
				toolbar.setItemVisible("saveSoftLabel", false);
				toolbar.setItemVisible("importFilter", false);
				toolbar.setItemVisible("saveFilter", false);
				easyFilter.toolbar.getItem('append').setVisible(false)
				easyFilter.toolbar.getItem('clear').setVisible(false)
				let items = easyFilter.getItems();
				let existGroup = groups.filter(g => { return g.groupId == "weijianwei" });
				let xzqh = panel.dwTable.getFields().fieldItemsMap['数据来源'];
				if (items.length == 0 && xzqh && (existGroup.length > 0 || isAdmin)) {
					easyFilter.appendItem({ leftExp: xzqh.getDbField(), operator: "=", rightValue: [] });
					panel.refresh(true);
				}
				return;
			} else if (type == 'setFilter') {
				toolbar.setItemVisible("saveSoftLabel", false);
				toolbar.setItemVisible("importFilter", false);
				toolbar.setItemVisible("saveFilter", false);
				return;
			} else if (type == 'onlyView') {
				flexLayout.hideComponent("toolbar");
				flexLayout.hideComponent("easyFilter");
				return;
			} else if (type == 'onlyExport') {
				flexLayout.hideComponent("easyFilter");
				toolbar.setItemVisible("saveSoftLabel", false);
				toolbar.setItemVisible("importFilter", false);
				toolbar.setItemVisible("saveFilter", false);
				toolbar.setItemVisible("fields", false);
				toolbar.setItemVisible("refresh", false);
				toolbar.setItemVisible("exportListData", true);
				return;
			} else if (type == 'viewApi') {
				toolbar.setItemVisible("saveSoftLabel", false);
				toolbar.setItemVisible("importFilter", false);
				toolbar.setItemVisible("saveFilter", false);
			} else if (!isAdmin && viewer == 'data' && type == 'exchange') {
				let queryInfo: QueryInfo = {
					fields: [{ name: "RESOURCE_TYPE", exp: "model1.RESOURCE_TYPE" }, { name: "APPLY_STATUE", exp: `model1.APPLY_STATUE` }, { name: "IS_EXPORT", exp: "model1.IS_EXPORT" }],
					filter: [{ exp: `model1.DW_TABLE_ID='` + resid + `' and model1.CREATOR='` + user + `'` }],
					sources: [{
						id: "model1",
						path: "/sysdata/data/tables/sdi/data-exchange/SDI_EXCHANGE_VIEW_PERMISION.tbl"
					}]
				};
				return getQueryManager().queryData(queryInfo, uuid()).then((result: QueryDataResultInfo) => {
					let data = result.data;
					let isOnlyView = true;
					let allowExport = false;
					data.forEach(d => {
						if (d[0] == '400' && d[1] == '1') {
							isOnlyView = false;
							d[2] == '1' && (allowExport = true);
						}
					})
					if (isOnlyView) {
						flexLayout.hideComponent("toolbar");
						flexLayout.hideComponent("filter");
						panel.getViewPanel().getComponent("paginator").setVisible(false)
					}
					if (allowExport) {
						toolbar.setItemVisible("exportListData", true);
					}
					return;
				});
			} else if (type == "labelModel") {//如果是标签首页则不需要其他多余的按钮，只需要查询
				flexLayout.hideComponent("filter");
				toolbar.setItemVisible("saveSoftLabel", false);
				toolbar.setItemVisible("importFilter", false);
				toolbar.setItemVisible("saveFilter", false);
				toolbar.setItemVisible("exportListData", true);
			} else if (type == 'accessFilter') {//数据接入过滤，不需要导出等工具按钮
				easyFilter.setConvertExpEnabled(true);
				toolbar.setItemVisible("saveSoftLabel", false);
				toolbar.setItemVisible("importFilter", false);
				toolbar.setItemVisible("saveFilter", false);
			}
			let filter = <string>prop[":filter"];
			if (filter) {
				easyFilter.setValue(JSON.parse(filter));
			}

		},
		onDidLoadFile: (viewer: DwTableEditor, file: MetaFileInfo) => {
			let currentPanel = viewer.getPanels().getCurrentPanel();
			let dtable: ExDTable = <any>currentPanel.getComponent("dtable", "modelFields");
			if (dtable) {//调整字段展示的样式
				let tableBuilder = dtable.getBuilder();
				tableBuilder.getRows().forEach(row => {
					row.setHeight(30);
				});
				/**
				 * https://jira.succez.com/browse/CSTM-15976
				 * liuyz 20210720
				 * 调整行高后要去请求渲染
				 */
				dtable.requestRender();
			}
		}
	},

	spg: {
		CustomActions: {
			/**文件列表中点击添加标签按钮弹出添加标签对话框。*/
			file_list_showAddLabel: (event: InterActionEvent) => {
				let vc = event.page.getComponent('list1');
				let checkedRows = vc.getCheckedDataRows();
				if (checkedRows.length === 0) {
					showErrorMessage("请先勾选要添加标签的数据表！");
					return;
				}
				let resids = checkedRows.map(r => r['MODEL_RESID']);
				showLabelMgrDialog({
					resIds: resids
				}).then(result => {
					throwInfo("打标成功");
				});
			},
			/** 文件列表工具栏点击导出数据字典 */
			file_list_exportDataDictionarg: (event: InterActionEvent) => {
				let vc = event.page.getComponent('list1');
				let checkedRows = vc && vc.getCheckedDataRows();
				if (!checkedRows || checkedRows.length === 0) {
					showErrorMessage("请先勾选要导出的数据表！");
					return;
				}
				let resids = checkedRows.map(r => r['MODEL_RESID']);
				exportDataDictionary(resids);
			},
			/**文件列表中点击列表的行。 */
			file_list_clickListRow: (event: InterActionEvent) => {
				let vc = event.component;
				let selectedRows = vc.getSelectedDataRows();
				let parentDir = vc.getPage().getFileInfo().parentDir;
				if (parentDir.endsWith("meta-data") && selectedRows.length === 0) {
					return;
				} else if (parentDir.endsWith("master-data") && selectedRows.length === 0) {
					let row = vc.getDataViewRow()["dataView"].getRow();
					let value = row.data.param2.getValue();
					if (value != null) {
						return getZTDataMgrPage(parentDir).then((ztMetaDataPage) => {
							if (!ztMetaDataPage) {
								return;
							}
							let resid = value;
							ztMetaDataPage.openFile(resid, true);
						});
					} else {
						return;
					}
				}

				return getZTDataMgrPage(parentDir).then((ztMetaDataPage) => {
					if (!ztMetaDataPage) {
						return;
					}
					let resid = selectedRows[0]['MODEL_RESID'];
					ztMetaDataPage.openFile(resid, true);
				});
			},
			button_common_setSortBtn: (event: InterActionEvent) => {
				let component = event.component;
				let id = component.getId();
				let dataRow = event.dataRow;
				let select = dataRow[id].selected
				let renderer = event.renderer;
				let buttonCom = renderer.getComponent(id);
				let builder = buttonCom.getCompBuilder();
				builder.setProperty("prefixIcon", { image: select ? "$PROJECTIMG:/icons/4d6ee5ad3f6bea214215d1123c741236.svg" : "$PROJECTIMG:/icons/09057f5a6f31398485824569761a14e0.svg" });
			},
			/**
			 * 数据交换申请通过后，需要添加审批
			 * 此处判断该任务的所有审批都为通过，如果是则根据api去生成数据，插入到通道表中
			 */
			button_exchange_passApproval: (event: InterActionEvent) => {
				let builder = (<any>event.page).builder;
				let urlParams = event.page.getUrlParams();
				let task_id = urlParams.task_id;
				let supply_id = urlParams.supply_id;
				let is_setTask = urlParams.is_setTask;
				let applyStep = urlParams.applyStep;
				let ex_type = urlParams["ex-type"];
				let residData;
				let residQueryInfo: QueryInfo;
				let rows: (any)[][] = [];
				let modfiyRows = [];
				//查询是否该任务是否全部通过
				let queryInfo: QueryInfo = {
					fields: [{ name: "APPLY_STATUE", exp: `model1.APPLY_STATUE` }],
					filter: [{ exp: `model1.TASK_ID= '` + (supply_id ? supply_id : task_id) + `' and model1.APPLY_TYPE='tech'` }],
					sources: [{
						id: "model1",
						path: "/sysdata/data/tables/sdi/data-exchange/ex-task/SDI_EX_APPROVAL.tbl"
					}]
				};
				(applyStep == '1' || !applyStep) && getQueryManager().queryData(queryInfo, uuid()).then((result: QueryDataResultInfo) => {
					let data = result.data;
					for (let i = 0; i < data.length; i++) {
						let statue = data[i][0];
						if (statue != '1') {
							return;
						}
					}
					//全部通过则将申请基本数据写入到通道表中
					let taskQuery: QueryInfo = {
						fields: [
							{ name: "EX_APPLY_NAME", exp: `model1.EX_APPLY_NAME` },
							{ name: "EXCHANGE_MODE", exp: `model1.EXCHANGE_MODE` },
							{ name: "CREATOR", exp: `model1.CREATOR` },
							{ name: "CREATE_TIME", exp: `model1.CREATE_TIME` },
							{ name: "CONTACTOR", exp: `model1.CONTACTOR` },
							{ name: "CONTACTOR_PHONE", exp: `model1.CONTACTOR_PHONE` },
							{ name: "EX_DEPT", exp: `model1.EX_DEPT` }
						],
						filter: [{ exp: `model1.EX_TASK_ID= '` + task_id + `'` }],
						sources: [{
							id: "model1",
							path: "/sysdata/data/tables/sdi/data-exchange/ex-task/SDI_EX_APPLY.tbl"
						}]
					};
					if (ex_type == 'api') {
						let filter = `model1.EX_TASK_ID= '${task_id}'`;
						if (supply_id != null) {
							filter += `and model1.BCSQRWID ='${supply_id}'`
						}
						//将api的相关信息写入通道表
						residQueryInfo = {
							fields: [
								{ name: "EX_APPLY_UUID", exp: `model1.EX_APPLY_UUID` },
								{ name: "RESID", exp: `model1.RESID` },
								{ name: "DW_TABLE_ID", exp: `model1.DW_TABLE_ID` },
								{ name: "API_ID", exp: `model1.API_ID` },
								{ name: "SCZD", exp: `model1.SCZD` },
								{ name: "CUSTOM_FILTER", exp: `model1.CUSTOM_FILTER` },
								{ name: "CUSTOM_FILTER_DESC", exp: `model1.CUSTOM_FILTER_DESC` },
								{ name: "DATA_FILTER", exp: `model1.DATA_FILTER` },
								{ name: "DATA_FILTER_DESC", exp: `model1.DATA_FILTER_DESC` }
							],
							filter: [{ exp: filter }],
							sources: [{
								id: "model1",
								path: "/sysdata/data/tables/sdi/data-exchange/ex-res/SDI_EX_APPLY_RESOURCE.tbl"
							}]
						};
					} else if (ex_type == 'push' || ex_type == 'review') {
						let libType = ex_type == 'push' ? "推送库" : "回放库";
						let filter = `model1.EX_TASK_ID= '${task_id}' and model1.RESID = model2.DX_RES_ID and model1.EX_DEPT=model2.DX_ORG_ID and model2.LIB_TYPE='${libType}'`;
						if (supply_id != null) {
							filter += `and model1.BCSQRWID='${supply_id}'`;
						}
						//将推送信息写入通道表中
						residQueryInfo = {
							fields: [
								{ name: "EX_APPLY_UUID", exp: `model1.EX_APPLY_UUID` },
								{ name: "RESID", exp: `model1.RESID` },
								{ name: "DW_TABLE_ID", exp: `model1.DW_TABLE_ID` },
								{ name: "SB_USE_INCREMENT", exp: `model1.SB_USE_INCREMENT` },
								{ name: "PUSH_SCHEDULE", exp: `model1.SB_SCHEDULE` },
								{ name: "SB_INC_FIELD_TYPE", exp: `model1.SB_INC_FIELD_TYPE` },
								{ name: "SB_INC_FIELD", exp: `model1.SB_INC_FIELD` },
								{ name: "SB_ENABLE_REGULAR_WHOLE", exp: `model1.SB_ENABLE_REGULAR_WHOLE` },
								{ name: "SB_REGULAR_WHOLE_PERIOD_N", exp: `model1.SB_REGULAR_WHOLE_PERIOD_N` },
								{ name: "SB_ENABLE_BACKUP", exp: `model1.SB_ENABLE_BACKUP` },
								{ name: "SB_BACKUP_COUNT", exp: `model1.SB_BACKUP_COUNT` },
								{ name: "REPOSITORY_ID", exp: `model2.REPOSITORY_ID` },
								{ name: "PUSH_TARGET_TABLE", exp: `model1.PUSH_TARGET_TABLE` }
							],
							filter: [{ exp: filter }],
							sources: [{
								id: "model1",
								path: "/sysdata/data/tables/sdi/data-exchange/ex-res/SDI_EX_APPLY_RESOURCE.tbl"
							}, {
								id: "model2",
								path: "/sysdata/data/tables/sdi/data-exchange/SDI_EX_REVIEW_EXT.tbl"
							}]
						};
					}
					//当交换方式不为在线浏览时才需要将数据写入通道表中
					if (ex_type != 'browser') {
						getQueryManager().queryData(residQueryInfo, uuid()).then((result: QueryDataResultInfo) => {
							residData = result.data;
							let filterList = [];
							for (let i = 0; i < residData.length; i++) {
								filterList.push(residData[i][0]);
								residData[i].push(1);
							}
							let dxQuery: QueryInfo = {
								fields: [{ name: "DX_CHANNEL_ID", exp: `model1.DX_CHANNEL_ID` }],
								filter: [{ exp: `model1.DX_CHANNEL_ID in  ('` + filterList.join("','") + `')` }],
								sources: [{
									id: "model1",
									path: "/sysdata/data/tables/dx/DX_CHANNELS.tbl"
								}]
							}
							return getQueryManager().queryData(dxQuery, uuid());
						}).then((result: QueryDataResultInfo) => {
							let data = result.data;
							let existUUID = [];
							for (let i = 0; i < data.length; i++) {
								existUUID.push(data[i][0]);
							}
							if (ex_type == 'push' || ex_type == 'review') {
								rows = residData;
							} else {
								for (let i = 0; i < residData.length; i++) {
									let row = [];
									let item = residData[i];
									let filter = [];
									let custom_filter = item[5] && JSON.parse(<string>item[5]);
									let data_filter = item[7] && JSON.parse(<string>item[7]);
									//将过滤条件给合并
									if (!isEmpty(custom_filter) && !isEmpty(data_filter)) {
										data_filter.push(custom_filter[0]);
										filter = data_filter;
									} else if (isEmpty(custom_filter) && !isEmpty(data_filter)) {
										filter = data_filter;
									} else if (!isEmpty(custom_filter) && isEmpty(data_filter)) {
										filter = custom_filter;
									}
									for (let i = 0; i < filter.length; i++) {
										let clauses = filter[i].clauses;
										for (let j = 0; j < clauses.length; j++) {
											let clause = clauses[j];
											let leftExp = clause["leftExp"];
											if (!leftExp.startsWith("t0.")) {
												clause["leftExp"] = "t0." + clause["leftExp"];
											}
										}
									}
									row.push(item[0]);
									row.push(item[1]);
									row.push(item[2]);
									row.push(item[3]);
									row.push(item[4]);
									row.push(JSON.stringify(filter));
									row.push(item[5]);
									row.push(item[6]);
									if (existUUID.indexOf(item[0]) == -1) {
										rows.push(row);
									} else {
										modfiyRows.push({ keys: [item[0]], row: row });
									}

								}
							}
							return getQueryManager().queryData(taskQuery, uuid());
						}).then((result: QueryDataResultInfo) => {
							let data = result.data;
							for (let i = 0; i < rows.length; i++) {
								let row = rows[i];
								row.push(data[0][0]);
								row.push(data[0][1] == 'api' ? 1 : (data[0][1] == 'review' ? '2' : '3'));
								row.push(data[0][2]);
								row.push(data[0][3]);
								row.push(data[0][4]);
								row.push(data[0][5]);
								row.push(data[0][6]);
								row.push(1)
								row.push(4086748774000);
								row.push(Date.now());
								row.push("user");
								row.push(1);//默认启用日志记录
							}
							modfiyRows.forEach(row => {
								row.row.push(data[0][0]);
								row.row.push(data[0][1] == 'api' ? 1 : 2);
								row.row.push(data[0][2]);
								row.row.push(data[0][3]);
								row.row.push(data[0][4]);
								row.row.push(data[0][5]);
								row.row.push(data[0][6]);
								row.row.push(1);
							});
							let updateDate: DwDataPackage = {};
							if (!isEmpty(rows)) {
								updateDate.addRows = [{
									fieldNames: ex_type == 'api' ? APIFIELD : PUSHORREVIEWFIELD,
									rows: rows
								}];
							}
							if (!isEmpty(modfiyRows)) {
								updateDate.modifyRows = [{
									fieldNames: ['DX_CHANNEL_ID', 'DX_RES_ID', 'DW_TABLE_ID', 'API_ENABLE', 'DATA_FIELDS', 'DATA_FILTER', 'CUSTOM_FILTER', 'CUSTOM_FILTER_DESC', 'DX_CHANNEL_DESC', 'CHANNEL_TYPE', 'USERS', 'APPLICATION_TIME', 'COMMITER', 'COMMITER_PHONE', 'COMMITER_DX_ORG_ID', 'ENABLE'],
									rows: modfiyRows
								}]
							}
							!isEmpty(updateDate) && rc({
								"url": getAppPath("/commons/commons.action?method=updateDwTableData"),
								method: "POST",
								data: {
									tablePath: '/sysdata/data/tables/dx/DX_CHANNELS.tbl',
									datapackage: updateDate
								}
							}).then(() => {
								if (is_setTask == '1') {
									throwInfo('保存完成，相关接口已经可以使用');
								} else {
									throwInfo('审批完成，相关接口已经可以使用');
								}
							})
						});
					}
					//通过审批后将数据写入申请资源表，便于判断某资源是否被用户申请
					rc({
						"url": getAppPath("/data-exchange/exApplyMgr.action?method=updateExDwTable"),
						method: "POST",
						data: {
							tablePath: '/sysdata/data/tables/sdi/data-exchange/ex-task/SDI_EX_APPLY.tbl',
							datapackage: {
								modifyRows: [{
									fieldNames: ['STATUS'],
									rows: [{
										keys: [task_id],
										row: ['1']
									}]
								}]
							}
						}
					})
				})
			},
			/**否决该申请，进入否决状态后，必须重新申请 */
			button_exchange_reject: (event: InterActionEvent) => {
				let builder = (<any>event.page).builder;
				let urlParams = event.page.getUrlParams();
				let task_id = <string>urlParams.task_id;
				rc({
					url: getAppPath("/data-exchange/exApplyMgr.action?method=rejectApproval"),
					method: "POST",
					data: {
						task_id: task_id
					}
				}).then(() => {
					throwInfo('该申请已被否决');
				})
			},
			/**设置交换任务的状态 */
			button_exchange_handleApply: (event: InterActionEvent) => {
				let dataRow = event.dataRow;
				let datasets = event.page.getDatasets();
				let task_id = dataRow.EX_TASK_ID;
				let buttonName = event.component.getTxt();
				let type = 5;
				showConfirmDialog({
					caption: `使用结束`,
					message: `确定"${dataRow.EX_APPLY_NAME}"已使用结束吗？确认之后${dataRow.DX_ORG_DESC}将不能再使用`,
					onok: () => {
						rc({
							url: getAppPath("/data-exchange/exApplyMgr.action?method=handleApply"),
							method: "POST",
							data: {
								task_id: task_id,
								type: type
							}
						}).then(() => {
							throwInfo(`结束使用交换任务成功`);
							datasets.forEach(d => {
								d.refresh({ notifyChange: true, force: true });
							})
						})
					}
				})
			},
			/**在提交新的申请时也需要额外将数据插入到审批表中 */
			button_exchange_sumbit: (event: InterActionEvent) => {
				let page = <SuperPageData>event.page;
				let embedPage = event.dataRow.embedsuperpage1;
				let builder = (<any>event.page).builder;
				let supply_id;
				let task_id = embedPage.input1;
				let applyType = embedPage.combobox1;
				let userid = getCurrentUser().userId;
				let urlParams = event.page.getUrlParams();
				let applyStep = urlParams && urlParams.applyStep;
				let isSupply = urlParams && urlParams.isSupply;
				if (isSupply == '1') {
					supply_id = event.dataRow.input4.value ? event.dataRow.input4.value : uuid();
				}
				//是否需要进行业务审批
				let isOutqueryInfo: QueryInfo = {
					fields: [{ name: "IS_OTHERORG", exp: `model1.IS_OTHERORG` }],
					filter: [{ exp: `model1.USER='` + userid + `'` }],
					sources: [{
						id: "model1",
						path: "/sysdata/data/tables/dx/DX_ORGS.tbl"
					}]
				};
				!applyStep && getQueryManager().queryData(isOutqueryInfo, uuid()).then((result: QueryDataResultInfo) => {
					let data = result.data;
					let isOtherOrg = !isEmpty(data) && data[0][0];
					let applyStatue;
					let queryInfo: QueryInfo;
					let residList = [];
					if (isOtherOrg == '1') {
						applyStatue = '2';
					} else {
						applyStatue = '3';
					}
					if (applyType == 'push' || applyType == 'review') {
						let list = applyType == 'push' ? page.getComponent('list4') : page.getComponent('list3');
						let checkRow = list.getCheckedDataRows();
						checkRow.forEach(row => {
							residList.push(row.DX_RES_ID1);
						})
						queryInfo = {
							fields: [{ name: "YWSPBM", exp: `model1.YWSPBM` }],
							filter: [{ exp: `model1.DX_RES_ID in ('` + residList.join("','	") + `')` }],
							sources: [{
								id: "model1",
								path: "/sysdata/data/tables/dx/DX_RESOURCES.tbl"
							}]
						};
					} else {
						let filter = isSupply == '1' ? `model1.BCSQRWID= '` + supply_id + `' and model1.RESID=model2.DX_RES_ID` : `model1.EX_TASK_ID= '` + task_id + `' and model1.RESID=model2.DX_RES_ID`;
						queryInfo = {
							fields: [{ name: "YWSPBM", exp: `model2.YWSPBM` }],
							filter: [{ exp: filter }],
							sources: [{
								id: "model1",
								path: "/sysdata/data/tables/sdi/data-exchange/ex-task/SDI_EX_APPLE_TASK.tbl"
							}, {
								id: "model2",
								path: "/sysdata/data/tables/dx/DX_RESOURCES.tbl"
							}],
							joinConditions: [{
								leftTable: "model1",
								rightTable: "model2",
								joinType: SQLJoinType.LeftJoin,
								exp: "model1.RESID = model2.DX_RES_ID"
							}]
						};
					}
					getQueryManager().queryData(queryInfo, uuid()).then((result: QueryDataResultInfo) => {
						let data = result.data;
						let busDept = [];
						let techDept = [];
						let rows = [];
						if (applyType == 'push' || applyType == 'review') {
							let rows = [];
							for (let i = 0; i < residList.length; i++) {
								let row = [];
								row.push(uuid());
								row.push(task_id);
								row.push(residList[i]);
								row.push(applyType);
								row.push(userid);
								row.push(new Date().getTime());
								if (isSupply == '1') {
									row.push('1');
									row.push(supply_id);
								}
								rows.push(row);
							}
							let fields = ['EX_APPLY_UUID', 'EX_TASK_ID', 'RESID', 'EXCHANGE_MODE', 'CREATOR', 'CREATER_TIME'];
							if (isSupply == '1') {
								fields.push('BCSQ');
								fields.push('BCSQRWID')
							}
							rc({
								"url": getAppPath("/data-exchange/exApplyMgr.action?method=updateExDwTable"),
								method: "POST",
								data: {
									tablePath: "/sysdata/data/tables/sdi/data-exchange/ex-task/SDI_EX_APPLE_TASK.tbl",
									datapackage: {
										addRows: [{
											fieldNames: fields,
											rows: rows
										}]
									}
								}
							})
						}
						//如果没有设置申请资源，则直接进入技术审批
						if (!isEmpty(data) && (applyType != 'push' && applyType != 'review')) {
							for (let i = 0; i < data.length; i++) {
								if (busDept.indexOf(data[i][0]) == -1) {
									busDept.push(data[i][0]);
									rows.push([isSupply == '1' ? supply_id : task_id, data[i][0] ? data[i][0] : "ywsp", applyStatue, 'bus'])
								}
								if (techDept.indexOf('jsbm') == -1) {
									techDept.push('jsbm');
									rows.push([isSupply == '1' ? supply_id : task_id, "jsbm", '3', 'tech'])
								}
							}
							rc({
								"url": getAppPath("/data-exchange/exApplyMgr.action?method=updateExDwTable"),
								method: "POST",
								data: {
									tablePath: "/sysdata/data/tables/sdi/data-exchange/ex-task/SDI_EX_APPROVAL.tbl",
									datapackage: {
										addRows: [{
											fieldNames: ['TASK_ID', 'APPROAL_DEPT', 'APPLY_STATUE', 'APPLY_TYPE'],
											rows: rows
										}]
									}
								}
							})
						} else {
							rc({
								"url": getAppPath("/data-exchange/exApplyMgr.action?method=updateExDwTable"),
								method: "POST",
								data: {
									tablePath: "/sysdata/data/tables/sdi/data-exchange/ex-task/SDI_EX_APPROVAL.tbl",
									datapackage: {
										addRows: [{
											fieldNames: ['TASK_ID', 'APPROAL_DEPT', 'APPLY_STATUE', 'APPLY_TYPE'],
											rows: [[isSupply == '1' ? supply_id : task_id, "jsbm", '3', 'tech']]
										}]
									}
								}
							})
						}
					})
				})
			},
			/**在创建新的任务时判断需不需要补充申请 */
			button_exchange_needSetTask: (event: InterActionEvent) => {
				let spg1 = <SpgEmbedSuperPage>event.renderer.getComponent("embedsuperpage1");
				let combobox1 = spg1.getSuperPage().getComponent("combobox1");
				let combobox2 = spg1.getSuperPage().getComponent("combobox2");
				let ex_type = combobox1.getInnerComoponent().value;
				let ex_dept = combobox2.getInnerComoponent().value;
				let queryInfo: QueryInfo = {
					fields: [{ name: "EX_TASK_ID", exp: "model1.EX_TASK_ID" }, { name: "CREATE_TIME", exp: "model1.CREATE_TIME" }],
					sources: [{
						"id": "model1",
						"path": "/sysdata/data/tables/sdi/data-exchange/ex-task/SDI_EX_APPLY.tbl"
					}],
					filter: [{ exp: `model1.EX_DEPT='${ex_dept}' and model1.EXCHANGE_MODE='${ex_type}'` }]
				}
				return getQueryManager().queryData(queryInfo, uuid()).then((result: QueryDataResultInfo) => {
					let data = result.data;
					if (data.length == 0) {
						return true;
					} else {
						let time = data[0][1];
						let date = new Date().setTime(time);
						showConfirmDialog({
							caption: "提示",
							message: `交换单位“${combobox2.getInnerComoponent().caption}”在${formatDate(parseDate(date), "yyyy年mm月dd日")}提交过“${combobox1.getInnerComoponent().caption}”的交换任务，如果要增加新的${combobox1.getInnerComoponent().caption}，为了方便管理，建议您直接补充申请这个交换任务。`,
							buttons: [{
								id: "custom.ex.supplyApply",
								caption: "申请任务",
								layoutTheme: "defbtn",
								onclick: (event: SZEvent, dialog: Dialog, item: any) => {
									dialog.close();
								}
							}, {
								id: "custom.ex.continue",
								caption: "忽略提示，继续创建",
								layoutTheme: "defbtn",
								onclick: (event: SZEvent, dialog: Dialog, item: any) => {
									dialog.close();
									return true;
								}
							}]
						})
					}
				})
			},
			/**上传血统文件 */
			button_uploadSqlFile: (event: InterActionEvent) => {
				let builder = (<any>event.page).builder;
				let queryInfo: QueryInfo = {
					fields: [{ name: "name", exp: `model1.DATASOURCE_NAME` }],
					filter: [{ exp: `model1.STATE= 'WORKING'` }],
					sources: [{
						id: "model1",
						path: DATASOURCESTATE
					}]
				};
				getQueryManager().queryData(queryInfo, uuid()).then((result: QueryDataResultInfo) => {
					let data = result.data;
					let items: any[] = [];
					for (let i = 0; i < data.length; i++) {
						items.push(data[i][0]);
					}
					let listDp = new DataProvider({
						data: items
					});
					showFormDialog({
						id: 'lineageUploadFile',
						caption: '载入查询条件',
						modal: true,
						resizable: false,
						content: {
							items: [{
								id: 'datasource',
								compArgs: <ComboboxArgs>{
									cleanIconVisible: true,
								},
								formItemType: 'combobox',
								captionPosition: 'left',
								caption: '数据库',
								required: true
							}, {
								id: 'dependentDatasource',
								compArgs: <ComboboxArgs>{
									cleanIconVisible: true,
								},
								formItemType: 'combobox',
								captionPosition: 'left',
								caption: '依赖库',
								required: true
							}, {
								id: 'uploadSqlFile',
								formItemType: "button",
								static: true,
								required: true,
								className: 'custom-uploadSql',
								compArgs: <ButtonArgs>{
									id: 'custom-upload-btn',
									caption: '上传sql文件',
									layoutTheme: 'linkbtn'
								},
								onclick: (e: SZEvent, form: Form) => {
									let uploadButton = <Button>form.getFormItemComp("uploadSqlFile");
									let uploadPromiseResolver: () => void;
									let stopWaiting = () => uploadPromiseResolver && uploadPromiseResolver();
									showUploadDialog({
										fileTypes: ["sql"],//只上传sql文件
										multiple: false,
										maxSize: 10 * 1024 * 1024, //限制大小10M
										onchange: (e) => {
											uploadfile = e.value[0];
											stopWaiting();
										},
										onbeforeupload: () => {
											uploadButton.showWaiting(new Promise(resolve => uploadPromiseResolver = resolve), 200);
										}
									})
								}
							}]
						},
						onshow: (event: SZEvent, dialog: Dialog) => {
							let form = <Form>(dialog.content);
							form.waitInit().then(() => {
								form.getFormItem('datasource').setDataProvider(listDp);
								form.getFormItem('dependentDatasource').setDataProvider(listDp);
								form.loadData({ datasource: null, dependentDatasource: null })
							});
						},
						buttons: [{
							id: 'ok',
							caption: '提交',
							layoutTheme: "defbtn",
							onclick: (event: SZEvent, dialog: Dialog, item: any) => {
								let data = (<any>dialog).form.getData();
								if (uploadfile == null) {
									throwInfo('请上传sql文件');
									return;
								}
								let fileId = uploadfile.id;
								let datasource = data.datasource;
								let dependentDatasource = data.dependentDatasource;
								let info: JSONObject = { fileId: fileId, datasource: datasource, dependentDatasource: dependentDatasource }
								rc({
									url: getAppPath("/meta-data/data-lineage-manage/pedigreeAnalysis.action?method=parserSql"),
									data: info
								}).then((result) => {
									if (result.success) {
										let domFileViewer = builder.getRenderer().domBase.closest('.metafileviewer-base');
										if (!domFileViewer) {
											return;
										}
										uploadfile = null;
										dialog.close();
										let fileViewer: MetaFileViewer = domFileViewer.szobject as MetaFileViewer;
										fileViewer.showDrillPage({
											url: {
												path: getAppPath(`/meta-data/data-lineage-manage/lineage-add.spg`),
												params: {
													':runtimecompile': true
												}
											},
											breadcrumb: false,
											target: ActionDisplayType.Container,
											title: `新增血统关系`
										});
									} else {
										throwInfo("接口调用失败");
									}
								});
							}
						}, {
							id: 'close',
							caption: '取消'
						}]
					})
				});
			},
			/**保存分析后血统内容 */
			button_saveSqlLineage: (event: InterActionEvent) => {
				let builder = (<any>event.page).builder;
				let vc = event.page.getComponent('list1');
				let checkedRows = vc.getCheckedDataRows();
				if (checkedRows.length === 0) {
					return throwInfo("请选择希望保存的血统数据");
				}
				let idColumn = builder.getComponent("list1").getSubComponents();
				let results = getCheckedValues(checkedRows, idColumn);
				let deleteIds = getDeleteIds(checkedRows, idColumn);
				showConfirmDialog({
					caption: " 保存血统",
					message: `要保存勾选的${checkedRows.length}条数据，是否确定？`,
					onok: () => {
						rc({
							url: getAppPath("/commons/commons.action?method=updateDwTableData"),
							method: "POST",
							data: {
								tablePath: META_FIELD_LINK_PATH,
								datapackage: {
									addRows: [{
										fieldNames: ['SRC_FILE_ID', 'SRC_FIELD_NAME', 'TARGET_PARENT', 'TARGET_TABLE_TYPE', 'TARGET_TABLE_NAME', 'TARGET_FIELD_NAME', 'REF_TYPE', 'IS_MANUAL'],
										rows: results
									}]
								}
							}
						}).then(() => {
							return rc({
								url: getAppPath("/commons/commons.action?method=updateDwTableData"),
								method: "POST",
								data: {
									tablePath: TABEL_LINEAGE_PATH,
									datapackage: {
										deleteRows: deleteIds
									}
								}
							});
						}).then(() => {
							throwInfo('血统保存成功');
							refreshModelState('CcjxdgJ004BFtJxMoFr5yF').then(() => {
								return getMetaRepository().getFile('CcjxdgJ004BFtJxMoFr5yF');
							}).then(file => {
								getDwTableDataManager().updateCache([{ path: file.path, type: "refreshall" }], true);
							});
						})
					}
				});
			},
			/**删除勾选的血统内容 */
			button_deleteSqlLineage: (event: InterActionEvent) => {
				let builder = (<any>event.page).builder;
				let vc = event.page.getComponent('list1');
				let checkedRows = vc.getCheckedDataRows();
				if (checkedRows.length === 0) {
					return throwInfo("请选择希望删除的血统数据");
				}
				let idColumn = builder.getComponent("list1").getSubComponents();
				let deleteIds = getDeleteIds(checkedRows, idColumn);
				showConfirmDialog({
					caption: " 保存血统",
					message: `要保存勾选的${checkedRows.length}条数据，是否确定？`,
					onok: () => {
						rc({
							url: getAppPath("/commons/commons.action?method=updateDwTableData"),
							method: "POST",
							data: {
								tablePath: TABEL_LINEAGE_PATH,
								datapackage: {
									deleteRows: deleteIds
								}
							}
						}).then(() => {
							throwInfo('删除血统成功');
							refreshModelState('CcjxdgJ004BFtJxMoFr5yF').then(() => {
								return getMetaRepository().getFile('CcjxdgJ004BFtJxMoFr5yF');
							}).then(file => {
								getDwTableDataManager().updateCache([{ path: file.path, type: "refreshall" }], true);
							});;
						})
					}
				});
			},
			/**
			 * 手工添加新的血统
			 * 此处要考虑添加的对象为物理表的情况
			 */
			button_addNewLineage: (event: InterActionEvent) => {
				let manualDomainDp;
				let manualDepTableDp;
				let manualDepFieldsDp;
				let manualFieldDp;
				let domainCobox;
				let depTableCobox;
				let depFieldsCobox;
				let tableResult;
				let page = event.page;
				let params = page.getParameterNames();
				let info: JSONObject = {};
				params.forEach(p => {
					info[p.name] = p.value;
				});
				let id = info.resid;
				let manualFields = [];
				let setType = info.setType;
				let fieldQueryInfo: QueryInfo;
				let datasource = info.parentDir;
				let target_table = info.target_table_name;
				let query;//对于物理表的和模型数据节点的添加血统采用不同的方式
				if (setType == 'db') {
					let dbInfo = target_table.split('.');
					query = rc({
						url: "/api/dw/services/queryFields",
						data: {
							projectName: "sdi",
							dataSource: datasource,
							schema: dbInfo[0],
							table: dbInfo[1]
						}
					})
				} else {
					fieldQueryInfo = {
						fields: [{ name: "fieldName", exp: "model1.FIELD_NAME" }],
						sources: [{
							id: "model1",
							path: META_FIELDS_PATH
						}],
						filter: [{ "clauses": [{ "leftExp": "model1.FILE_ID", "operator": ClauseOperatorType.DoubleEqual, "rightValue": id }] }]
					}
					query = getQueryManager().queryData(fieldQueryInfo, uuid());
				}
				query.then((result) => {
					let data = setType == 'db' ? result : result.data;
					data.map(d => {
						if (setType == 'db') {
							manualFields.push(d.name);
						} else {
							manualFields.push(d[0]);
						}
					})
					manualFieldDp = new DataProvider({
						data: manualFields
					});
					let queryInfo: QueryInfo = {
						fields: LINEAGE_QUERY_FIELDS,
						sources: [{
							id: "model1",
							path: SDI_META_TABLES_EXT
						}]
					};
					return getQueryManager().queryData(queryInfo, uuid());
				}).then((result: QueryDataResultInfo) => {
					let data = result.data;
					tableResult = praseTableResult(data);
					manualDomainDp = new DataProvider({
						data: tableResult.domains
					});
					let tables = getTableFromTableResult(tableResult, tableResult.domains[0].value);
					manualDepTableDp = new DataProvider({
						data: tables
					});
					manualDepFieldsDp = new DataProvider({
						data: []
					});
					showFormDialog({
						id: "addManualLineage",
						caption: '新增血统关系',
						modal: true,
						resizable: false,
						content: {
							items: [{
								id: 'manualField',
								compArgs: <ComboboxArgs>{
									cleanIconVisible: true,
								},
								formItemType: 'combobox',
								captionPosition: 'left',
								caption: '数据项:',
								required: true
							}, {
								id: 'manualDomain',
								compArgs: <ComboboxArgs>{
									cleanIconVisible: true,
									onchange: (e: SZEvent, combobox: Combobox, value: any) => {
										let v = e.newValue;
										let tables = getTableFromTableResult(tableResult, v);
										manualDepTableDp = new DataProvider({
											data: tables
										});
										depTableCobox.setDataProvider(manualDepTableDp);
									}
								},
								formItemType: 'combobox',
								captionPosition: 'left',
								caption: '上游数据表:',
								required: true
							}, {
								id: 'manualDepTable',
								compArgs: <ComboboxArgs>{
									cleanIconVisible: true,
									onchange: (e: SZEvent, combobox: Combobox, value: any) => {
										let v = e.newValue;
										let item = depTableCobox.getComponent().getItemByValue(v);
										let resid = item.data.resid;
										let fieldQueryInfo: QueryInfo = {
											fields: [{ name: "fieldName", exp: "model1.FIELD_NAME" }],
											sources: [{
												id: "model1",
												path: META_FIELDS_PATH
											}],
											filter: [{ "clauses": [{ "leftExp": "model1.FILE_ID", "operator": ClauseOperatorType.DoubleEqual, "rightValue": resid }] }]
										}
										getQueryManager().queryData(fieldQueryInfo, uuid()).then((result: QueryDataResultInfo) => {
											let data = result.data;
											let items = [];
											data.forEach(d => {
												items.push(d[0]);
											})
											manualDepFieldsDp = new DataProvider({
												data: items
											});
											depFieldsCobox.setDataProvider(manualDepFieldsDp);
										});
									}
								},
								formItemType: 'combobox',
								captionVisible: false,
								newRow: false
							}, {
								id: 'manualDepFields',
								compArgs: <ComboboxArgs>{
									cleanIconVisible: true,
									multipleSelect: true
								},
								formItemType: 'combobox',
								captionPosition: 'left',
								caption: '上游数据项：',
								required: true
							}]
						},
						onshow: (event: SZEvent, dialog: Dialog) => {
							let form = <Form>dialog.content;
							form.waitInit().then(() => {
								form.getFormItemComp('manualField').setDataProvider(manualFieldDp);
								domainCobox = form.getFormItemComp('manualDomain');
								domainCobox.setDataProvider(manualDomainDp);
								domainCobox.setValue(tableResult.domains[0].value);
								depTableCobox = form.getFormItemComp('manualDepTable');
								depTableCobox.setDataProvider(manualDepTableDp);
								depFieldsCobox = form.getFormItemComp('manualDepFields');
								depFieldsCobox.setDataProvider(manualDepFieldsDp);
							})
							form.loadData({ manualField: null, manualDomain: null, manualDepTable: null, manualDepFields: null });
						},
						buttons: [{
							id: "ok",
							layoutTheme: "defbtn",
							onclick: (event: SZEvent, dialog: Dialog, item: any) => {
								let data = (<any>dialog).form.getData();
								let manualField = data.manualField;
								let manualDomain = data.manualDomain;
								let manualDepTable = data.manualDepTable;
								let info = depTableCobox.getComponent().getItemByValue(manualDepTable);
								let parentPath = info.data.id;
								let manualDepFields = data.manualDepFields;
								let resid = id;
								let rows = [];
								for (let i = 0; i < manualDepFields.length; i++) {
									let row = [];
									if (setType == 'db') { //如果是物理则要把上游表记录到被引用表中
										row.push(info.data.resid);
										row.push(manualDepFields[i]);
										row.push(datasource);
										row.push(1);
										row.push(target_table);
										row.push(manualField);
										row.push('w');
										row.push(1);
									} else {
										row.push(resid);
										row.push(manualField);
										row.push(parentPath);
										row.push(0);
										row.push(manualDepTable);
										row.push(manualDepFields[i]);
										row.push('r');
										row.push(1);
									}

									rows.push(row);
								}
								rc({
									url: getAppPath("/commons/commons.action?method=updateDwTableData"),
									method: "POST",
									data: {
										tablePath: META_FIELD_LINK_PATH,
										datapackage: {
											addRows: [{
												fieldNames: ['SRC_FILE_ID', 'SRC_FIELD_NAME', 'TARGET_PARENT', 'TARGET_TABLE_TYPE', 'TARGET_TABLE_NAME', 'TARGET_FIELD_NAME', 'REF_TYPE', 'IS_MANUAL'],
												rows: rows
											}]
										}
									}
								})
							}
						}, 'cancel'],
						onok: (sze, dialog, btn, form) => {
							throwInfo('保存血统成功');
						}
					})
				});
			},
			/**修改手工添加的血统关系 */
			button_modifyLineage: (event: InterActionEvent) => {
				let row = event.dataRow;
				throwInfo('暂未实现');
			},
			/**删除手工添加的血统关系 */
			button_deleteLineage: (event: InterActionEvent) => {
				let dataRow = event.dataRow;
				let dataset = (<any>event.page).getDatasets()[0];
				let info = {
					"SRC_FILE_ID": dataRow['SRC_FILE_ID'],
					"SRC_FIELD_NAME": dataRow['SRC_FIELD_NAME'],
					"TARGET_PARENT": dataRow['TARGET_PARENT'],
					"TARGET_TABLE_TYPE": dataRow['TARGET_TABLE_TYPE'],
					"TARGET_TABLE_NAME": dataRow['TARGET_TABLE_NAME'],
					"TARGET_FIELD_NAME": dataRow['TARGET_FIELD_NAME'],
					"REF_TYPE": "r",
					"IS_MANUAL": 1
				}
				//由于字段关联关系表没有主主键，只能由脚本处理
				rc({
					url: getAppPath("/meta-data/data-lineage-manage/pedigreeAnalysis.action?method=deleteLineage"),
					method: "POST",
					data: {
						deleteInfo: info
					}
				}).then((result: JSONObject) => {
					if (result.success) {
						throwInfo("删除成功");
						dataset.refresh({ notifyChange: true, force: true });
					}
				})
			},
			/**下载标准文档 */
			button_downloadStandardFile: (event: InterActionEvent) => {
				let fileName = event.params.fileName;
				let standard_id = event.params.standard_Id;
				let version = event.params.version;
				showFormDialog({
					id: 'spg.downloadStandardList',
					caption: '下载标准文档',
					content: {
						items: [{
							id: 'fileType',
							caption: '下载类型',
							formItemType: 'selectpanel',
							captionPosition: 'left',
							compArgs: {
								items: [{
									value: 'pdf',
									caption: 'PDF'
								}, {
									value: 'html',
									caption: 'HTML'
								}]
							}
						}],
					},
					onshow: (event: SZEvent, dialog: Dialog) => {
						let form = <Form>dialog.content;
						form.loadData({ fileType: 'pdf' });
					},
					buttons: [{
						id: "ok",
						layoutTheme: "defbtn",
						onclick: (event: SZEvent, dialog: Dialog, item: any) => {
							let data = (<any>dialog).form.getData();
							let fileType = data.fileType;
							let url = getAppPath("/data-standard/standard-doc/standardHTML.action?method=downloadFile&fileType=" + fileType + "&standardId=" + standard_id + "&version=" + version + "&fileName=" + fileName);
							location.href = ctx(encodeURI(url));
						}
					}, 'cancel']
				})
			},
			/**收藏当前文件 */
			button_favorite: (event: InterActionEvent) => {
				let metaRepository = getMetaRepository();
				let page = event.page;
				let compont = event.component;
				let urlParams = page.getParameterNames();
				let resid = null;
				urlParams.forEach(p => {
					if (p.name == 'resid') {
						resid = p.value;
					}
				})
				let isFavorite = metaRepository.isFavorited(resid);
				return isFavorite.then(result => {
					return (result ? metaRepository.deleteFavorites([resid]) : metaRepository.addFavorites([resid])).then(() => {
						throwInfo(result ? "取消收藏成功" : "收藏成功");
						let statue = result ? "收藏" : "已收藏";
						let iconStatue = result ? "5438" : "5439"
						let builder = compont.getDataNode();
						//builder.setProperty("value", statue);
						builder.setProperty("prefixIcon", { code: iconStatue });
					})
				})
			},
			//下载引入的文档
			button_downloadImport: (event: InterActionEvent) => {
				let residOrPath = event.params.residOrPath;
				let attachment = event.params.attachment;
				let fileName = event.params.fileName;
				showConfirmDialog({
					caption: '下载确认',
					message: '点击"确定"按钮后将开始下载"' + fileName + '"',
					onok: () => {
						downloadFile(`/api/fapp/services/downLoadAttachment?resId=${residOrPath}&fileId=${attachment}`);
					}
				})
			},
			/**发送查询api请求 */
			button_queryAPI: (event: InterActionEvent) => {
				let startTime = new Date().getTime();
				let page = <any>event.page;
				let builder = page.getBuilder();
				let dataRow = event.dataRow;
				let url = dataRow.input1.value;
				let folat = page.getComponent("floatpanel1");
				let items = folat.getDataRows();
				let params: JSONObject = {};
				for (let i = 0; i < items.length; i++) {
					let data = items[i];
					let title = data['input2.title'].value;
					let value = data['input2'].value;
					!isEmpty(value) && (params[title] = value);
				}
				let numberInput = page.getComponent("numberinput1");
				params["pageSize"] = numberInput.getValue();
				rc({
					url: url,
					method: "POST",
					data: params
				}).then(result => {
					let endTime = new Date().getTime();
					let panelTime = page.getComponent("panel7");
					let textTime = page.getComponent("text4");
					let html = builder.getComponent("html1");
					if (panelTime.isVisible() == "false") {
						panelTime.setVisible(true);
					}
					textTime.setValue((endTime - startTime) + "毫秒");
					html.setProperty("html", JSON.stringify(result));
				})
			},
			/**下载api文档 */
			button_downloadApi: (event: InterActionEvent) => {
				let page = event.page;
				let params = event.params;
				let file_name = params.file_name != null ? params.file_name + "接口文档" : 'api接口文档';
				let redener = event.renderer;
				let fileInfo = page.getFileInfo();
				let resid = fileInfo.id;
				let html = redener.getComponent("html1");
				let domBase = html.getDomBody();
				html2pdf(resid, domBase, file_name, ExportReusltType.DOWNLOAD, { width: (domBase.widthNoPadding + 140), margin: { all: 70 } });
			},
			/**测试数据推送 */
			button_dx_testPushData: (event: InterActionEvent) => {
				let builder = (<any>event.page).getBuilder();
				let input = event.component.getComponent('input1');
				let id = input.getValue();
				if (!id) {
					showErrorMessage('推送计划id不能为空', 3000);
					return false;
				}
				let task_id = uuid();
				let promise = pushData({
					schedule: id,
					parallel: 1
				}, task_id)
				showProgressDialog({
					caption: "数据推送执行日志",
					uuid: task_id,
					promise: promise,
					buttons: ["cancel"],
					logsVisible: true,
					width: 700,
					height: 500,
					onfinish: (sze: SZEvent, p: ProgressPanel) => {
						throwInfo("数据推送完毕");
					}
				});
				return showWaiting(promise).then(() => {
					// throwInfo("数据推送完毕");
				})
			},
			/**批量导入交换资源 */
			button_ex_batchSelectSource: (event: InterActionEvent) => {
				let builder = (<any>event.page).getBuilder();
				showResourceDialog({
					id: "doBatchImportAsset_EX",
					caption: "选择资源目录",
					rootPath: "/sdi/data/tables/domains",
					returnTypes: ['fold', 'tbl'],
					checkBoxVisible: true,
					multipleCheckable: true,
					onok: function(event1: SZEvent, component?: Component, items?: any) {
						let input = event.component.getComponent('multipleinput1');
						input.setValue(items);
					}
				})
			},
			/**数据交换重新推送 */
			button_ex_rePushData: (event: InterActionEvent) => {
				let sdate = event.page;
				let list1 = sdate.getComponent('list1');
				let rows = list1.getCheckedDataRows();
				if (rows.length == 0) {
					showConfirmDialog({
						caption: '提示',
						message: `请勾选要推送的资源再提交`,
						buttons: ["ok"]
					})
					return false;
				}
				let checkbox1 = sdate.getComponent('checkbox1');
				let useIncrement = (checkbox1.getValue() == '1') ? true : false;
				let channel_ids = [];
				let schedules = [];
				let schedules_json = {};
				for (let i = 0; i < rows.length; i++) {
					let row = rows[i];
					let channel_id = row['DX_CHANNEL_ID'];
					channel_ids.push(channel_id);
					if (schedules.indexOf(row['PUSH_SCHEDULE']) == -1) {
						schedules.push(row['PUSH_SCHEDULE']);
					}
				}
				for (let j = 0; j < schedules.length; j++) {
					let sche = schedules[j];
					let channel_ids_sche = [];
					for (let p = 0; p < rows.length; p++) {
						let row = rows[p];
						if (row['PUSH_SCHEDULE'] == sche && channel_ids_sche.indexOf(row['DX_CHANNEL_ID']) == -1) {
							channel_ids_sche.push(row['DX_CHANNEL_ID']);
						}
					}
					schedules_json[sche] = channel_ids_sche;
				}
				console.log(`要推送的数据如下：`)
				console.log(schedules);
				console.log(schedules_json);
				if (schedules.length >= 1) {
					console.warn(`有多个推送计划会有多个进度对话框!,暂时只推送一个计划！`);
					let sche = schedules[0];
					let args = {
						schedule: sche,
						org: rows[0]['COMMITER_DX_ORG_ID'],
						channels: schedules_json[sche],
						useIncrement: useIncrement
					}
					let task_id = uuid();
					let promise = pushData(args, task_id);
					showProgressDialog({
						caption: "数据推送执行日志",
						uuid: task_id,
						promise: promise,
						buttons: ["cancel"],
						logsVisible: true,
						width: 700,
						height: 500,
						onfinish: (sze: SZEvent, p: ProgressPanel) => {
							throwInfo(`计划${sche}数据推送完毕`);
						}
					});
				}
			},
			/*重新推送——全选 */
			button_ex_repushData_allChecked: (event: InterActionEvent) => {
				let builder = (<any>event.page).getBuilder();
				let listcomp = event.page.getComponent('list1');
				let spglist = listcomp.component;
				let rowIndexs = [];
				spglist.tableBuilder.getCol(0).getCells().forEach((cell, i) => {
					if (cell.row.index !== 0) {
						cell.setValue(true);
						rowIndexs.push(i - 1);//index减去标题行才能和dataViewRows对应
					} else {
						cell.setValue(true);
					}
				});
				let sdate = builder.getData();
				let list_data = sdate.getComponent('list1');
				spglist.lastCheckedRows = rowIndexs;
				list_data.setCheckedDataRows(rowIndexs);
				spglist.tableEditor.requestRender();
			},
			/*重新推送——全不选 */
			button_ex_repushData_allUnchecked: (event: InterActionEvent) => {
				let builder = (<any>event.page).getBuilder();
				let listcomp = event.page.getComponent('list1');
				let spglist = listcomp.component;
				let rowIndexs = [];
				spglist.tableBuilder.getCol(0).getCells().forEach((cell, i) => {
					if (cell.row.index !== 0) {
						cell.setValue(false);
						rowIndexs.push(i - 1);//index减去标题行才能和dataViewRows对应
					} else {
						cell.setValue(false);
					}
				});
				let sdate = builder.getData();
				let list_data = sdate.getComponent('list1');
				spglist.lastCheckedRows = rowIndexs;
				list_data.setCheckedDataRows(rowIndexs);
				spglist.tableEditor.requestRender();
			},
			/*重新推送——只选推送失败的 */
			button_ex_repushData_onlyErrorchecked: (event: InterActionEvent) => {
				let builder = (<any>event.page).getBuilder();
				let listcomp = event.page.getComponent('list1');
				let spglist = listcomp.component;
				let rowIndexs = [];
				let sdate = builder.getData();
				let list_data = sdate.getComponent('list1');
				let rows = list_data.getDataRows();
				for (let i = 0; i < rows.length; i++) {
					let row = rows[i];
					let state = row['ZT'];
					let cell = spglist.tableBuilder.getCell(i + 1, 0);
					if (state == '失败') {
						cell.setValue(true);
						rowIndexs.push(i);
					} else {
						cell.setValue(false);
					}
				}

				spglist.lastCheckedRows = rowIndexs;
				list_data.setCheckedDataRows(rowIndexs);
				spglist.tableEditor.requestRender();
			},
			/**显示pdf搜索框 */
			button_std_file_showFindbar: (event: InterActionEvent) => {
				let builder = (<any>event.page).getBuilder();
				let local = event.page.getUrlParams().local;
				let comp = event.page.getComponent('document1');
				let embed = comp.getUIComponent().embed;
				let pdf_view = embed.currentView;
				pdf_view.showFindbar().then((findbar) => {
					findbar.keyword = local;
					findbar.findValueSearch.setValue(local);
					findbar.doFind();
				})
			}
		},
		onRender: (event: InterActionEvent) => {
			let page = <SuperPage>event.renderer;
			let builder = page && page.builder;
			let resid = builder && builder.getResid();
			let fileInfo = builder.getFileInfo();
			let task_id = event.page.getUrlParams().task_id;
			let isModify = event.page.getUrlParams().isModify;
			if (!page) {
				return Promise.resolve();
			}
			if (fileInfo.name == 'ex-task-list.spg') {
				let component = event.component;
				let dataView = component && component.getDataViewRow();
				if (component && (component.getId() == 'button11' || component.getId() == 'text17') && dataView && dataView.getKeys()) {
					let value = <string>component.getValue();
					value = value.trim();
					let color = null;
					if (value == "申请通过") {
						color = "#32BF72";
					} else if (value !== "待业务部门审批" && value !== "待技术部门审批" && value.indexOf('待审批') == -1) {
						color = "#FF1616";
					}
					let font = assign({}, component.getProperty("font"));
					color && (font.color = color);
					color && component.setProperty("font", font)
				}
			}
		},
		onRefresh: (event: InterActionEvent) => {
			let page = <SuperPage>event.renderer;
			let builder = page && page.builder;
			let resid = builder && builder.getResid();
			let fileInfo = builder.getFileInfo();
			if (fileInfo.name == 'data-process-plan.spg') {
				let component = event.component;
				let id = component && component.getId();
				if (id === 'floatpanel1.panel1') {
					let row = component.getDataViewRow();
					let index = row && row.getIndex();
					if (index != null) {
						if (index % 2) {
							component.setProperty('fill', { type: 'color', value: { color: '#315B8A ', opacity: 5 } });
						} else {
							component.setProperty('fill', { type: 'color', value: '#ffffff' });
						}
					}
				}
			}
		},
		onDidRefresh: (event: InterActionEvent) => {
			let page = <SuperPage>event.renderer;
			let builder = page && page.builder;
			let fileInfo = builder && builder.getFileInfo();
			if (fileInfo && fileInfo.name == 'master-doc-viewer.spg') {
				let comp = event.component;
				if (comp && comp.getId() == 'document1') {
					let embed = comp.getUIComponent().embed;
					let pdfView = embed.currentView;
					tryWait(() => {
						return pdfView.needRender == false;
					}).then(() => {
						pdfView.waitRender().then(() => {
							pdfView.showFindbar().then((findbar) => {
								findbar.domBase.style.right = "";
								findbar.domBase.style.left = "10px";
								findbar.updateState({
									keyword: pdfView.info.autoLocateText,
									ignoreCase: true,
									wholeWord: false,
									resultCount: null,
									currentIndex: null
								});
								pdfView.find({
									keyword: pdfView.info.autoLocateText,
									ignoreCase: true,
									wholeWord: false,
								});
							});
						})
					});
				}
			}
		},
		onDidLoadFile: (event: InterActionEvent): Promise<void> => {
			let standardViewSpg = getAppPath("/data-standard/file-online-view.spg");
			let pdfViewSpg = getAppPath("/data-standard/standard_pdf_view.spg");
			let onlineViewApiSpg = getAppPath("/data-exchange/ex-task/ex-view-api.spg");
			let lineageSpgs = getAppPath("/meta-data/data-lineage-manage/lineage-add.spg");
			let fileViewSpgs = [
				PROJECTURL + "data-asset/file-viewer.spg",
				PROJECTURL + "data-exchange/ex-file-view.spg",
				PROJECTURL + "master-data/master-file-view.spg"
			];
			let checkResultSpg = getAppPath("/data-govern/check-task/check-doc/check_report.spg");
			let page: SuperPage = (<any>event).activePage;
			let builder: SuperPageBuilder = page && page.builder;
			let currentSpgResId = builder && builder.getResid();

			let urlParams = page.getData().getUrlParams();
			let resId: string = urlParams.table; // 主数据表

			if (!resId) {
				if (lineageSpgs == currentSpgResId) {
					return renderLineageAddSpg(page);
				} else if (fileViewSpgs.indexOf(currentSpgResId) != -1) {
					return renderTableViewSpg(page);
				} else if (currentSpgResId == standardViewSpg || pdfViewSpg == currentSpgResId) {
					return renderStandardViewSpg(page);
				} else if (currentSpgResId == onlineViewApiSpg) {
					return renderAPIViewSpg(page);
				} else if (currentSpgResId == checkResultSpg) {
					return renderCheckViewSpg(page);
				}
				return;
			}
		}
	},
	"导入excel.spg": {
		onDidLoadFile: Label_onDidLoadFile,
		CustomActions: Label_CustomActions
	},
	"软标签.spg": {
		onDidLoadFile: Label_onDidLoadFile,
		CustomActions: Label_CustomActions
	},
	"硬标签.spg": {
		onDidLoadFile: Label_onDidLoadFile,
		CustomActions: Label_CustomActions
	},
	"数据表打标.spg": {
		onDidLoadFile: Label_onDidLoadFile,
		CustomActions: Label_CustomActions
	},
	"添加数据.spg": {
		onDidLoadFile: Label_onDidLoadFile,
		CustomActions: Label_CustomActions
	},
	"data_labelView.spg": {
		onDidLoadFile: Label_onDidLoadFile,
		CustomActions: Label_CustomActions
	},
	"check_rules.spg": {
		CustomActions: DG_CustomActions,
		onRender: (event: InterActionEvent) => {
			let data = event.data;
			let treeData = data && data.dataset;
			if (treeData && Array.isArray(treeData) && treeData.length) {
				treeData.forEach((d) => {
					let fields = d.fields;
					let index = fields && fields.findIndex(f => f === '资源地址');
					if (index !== undefined && index !== -1) {
						let isTable = d.data[index];
						if (isTable != null) {
							d.html = `<span class = "custom_check_task_istable"></span>${d.caption}`;
						} else {
							d.html = `${d.caption}`;
						}
					}
				});
			}
		}
	},
	"create_check_task.spg": {
		CustomActions: DG_CustomActions,
		/**
		 * 20211015 liuyz
		 * 检测任务内置规则和自定义规则列表界面，能够加载数据库勾选的值、记忆翻页前的勾选值
		 * 1.渲染列表的时候判断是否初次打开页面，如果是初次渲染列表，则开始下载数据库中勾选的值，将值暂存到对应的模型中
		 * 2.根据模型中暂存的值渲染列表控件勾选项
		 * 3.每次点击勾选项时，判断哪些新增，哪些删除，将这些数据从暂存数据中移除或者是新增
		 * @param event
		 * @returns
		 */
		onRender: (event: InterActionEvent) => {
			let isModify = event.page.getUrlParams().isModify;
			let task_id = event.page.getUrlParams().task_id;
			let isFirstRender;
			event.page.getParameterNames().forEach(param => {
				if (param.name === 'isFirstRender') {
					isFirstRender = param.value;
					return;
				}
			});
			if (isModify == 'true') {
				let comp = event.component;
				if (comp && comp.getId() == 'list2') {//对于内置规则列表控件
					let storeDataset = event.page.getDataset('model3');
					return loadExistDataToList(storeDataset, comp, task_id, 'nzgz');
				}
				if (comp && comp.getId() == 'list5') {//自定义规则列表控件
					let storeDataset = event.page.getDataset('model12');
					return loadExistDataToList(storeDataset, comp, task_id, 'cstm');
				}
			}
		},
		onDidRefresh: (event: InterActionEvent) => {
			let component = event.component;
			let id = component && component.getId();
			if (id === 'list3') {
				let comp = event.page.getComponent('list3').component;
				let len = event.page.getComponent('list3').component.lastCheckedRows.length;
				let new_index;
				if (lastSelectedRow == -1 && len > 0) {
					lastSelectedRow = comp.lastCheckedRows[0];
				}
				let newChecked = [];
				if (len > 1) {
					let index = comp.lastCheckedRows.indexOf(lastSelectedRow);
					new_index = 1 - index;
					newChecked.push(comp.lastCheckedRows[new_index]);
					lastSelectedRow = comp.lastCheckedRows[new_index];
				}
				len > 1 && component.setCheckedDataRows(newChecked);
			}
		}
	},
	"task_list.spg": {
		CustomActions: DG_CustomActions
	},
	"task_view.spg": {
		CustomActions: DG_CustomActions
	},
	'data-access-target-mgr.spg': {
		CustomActions: Access_CustomActions
	},
	'data-access-db-mgr.spg': {
		CustomActions: Access_CustomActions
	},
	"data-access-create.spg": {
		CustomActions: Access_CustomActions
	},
	"data-access-view.spg": {
		CustomActions: Access_CustomActions
	},
	"data-access-used-other-db.spg": {
		CustomActions: Access_CustomActions
	},
	"custom_rule_manager.spg": {
		CustomActions: DG_CustomActions
	},
	app: {
		onDidInitFrame: (frame: Component) => {
			//CSTM-11881 数据资产分析子节点展现方式优化
			let old_createPortalModulePage = frame.createPortalModulePage;
			frame.createPortalModulePage = (args: PortalModulePageArgs) => {
				let comp = old_createPortalModulePage.call(frame, args);
				if (comp.flexLayout) {
					function _changeSize() {
						if (comp.flexLayout && comp.flexLayout.getItem('left').component) {
							comp.timer && clearInterval(comp.timer);
							comp.flexLayout.getItem('left').component.toggleCollapsed();
						}
					}
					comp.timer = setInterval(_changeSize, 10);
				}
				return comp;
			}
		},
		onDidInitFrameComponent: (frame, comp) => {
			let isAdmin = getCurrentUser().isAdmin;
			if (comp instanceof PortalHeadTitle) {
				rc_get("/sysdata/app/zt.app/项目定制内容/template-conf.json").then(result => {
					let templateMenu = result.templateMenu;
					for (let keys in templateMenu) {
						if (location.pathname.endsWith(keys) && frame.headbar) {
							let domTitleText = comp.domTitleText;
							let menus = templateMenu[keys];
							domTitleText.style.cursor = 'pointer';
							domTitleText.classList.add('sz-portal-datamgr');
							domTitleText.onclick = () => {
								let menuItem = isAdmin ? menus.admin : menus.notAdmin;
								if (!menuItem) {
									menuItem = menus.all;
								}
								showMenu({
									id: 'custom_naviga_menu',
									colorTheme: 'succ-portalTemplate-greenblue-blue',
									showAt: domTitleText,
									items: menuItem,
									onclick: (e: SZEvent, menu: Menu, item: MenuItem) => {
										location.href = ctx(item.value);
									}
								})
							}
						}
					}
				})

			} else if (comp instanceof PortalHeadToolbar) {
				rc_get("/sysdata/app/zt.app/项目定制内容/template-conf.json").then(result => {
					//给对应页面添加管理按钮
					let buttonsTpg = result.templateButtons;
					for (let keys in buttonsTpg) {
						if (isAdmin && location.pathname.endsWith(keys)) {
							let domB = comp.toolbar.domBase;
							let domP = document.createElement("div");
							domB.insertBefore(domP, domB.childNodes[0]);
							new Button({
								domParent: domP,
								layoutTheme: 'smallbtn',
								colorTheme: frame.colorTheme,
								icon: 'icon-settings',
								id: 'mgr',
								caption: '管理',
								onclick: () => {
									location.href = ctx("/zt" + buttonsTpg[keys].onclick);
								}
							})
							break;
						}
					}
				})
			}
		}
	} as IAppCustomJS,
	fapp: {
		CustomActions: {
			ex_apply_list_showFormSpg: (event: InterActionEvent) => {
				let vc = event.component;
				let selectedRows = vc.getSelectedDataRows();
				if (selectedRows.length === 0) {
					return;
				}
			}
		}
	},
	dash: {
		onRender: (event) => {
			let page = <SuperPage>event.renderer;
			let builder = page && page.builder;
			let resid = builder && builder.getResid();
			let fileInfo = builder.getFileInfo();
			if (fileInfo.name == 'index.dash') {
				let comp = event.component;
				let value = comp.getValue();
				if (comp.getId() == 'text17' && value.text == 0) {
					page.getComponent('text17').setVisible(false)
				}
			}
		},
		onRefresh: (event) => {
			let fileInfo = event.page.getFileInfo();
			let name = fileInfo && fileInfo.name;
			if (name === 'data-rules.dash') {
				let comp = event.component;
				if (comp && comp.getId() == 'columnTable1') {
					let uiComp: any = comp.getUIComponent();
					let container = uiComp && uiComp.olapTable && uiComp.olapTable.domContainer;
					if (container && container.style && container.style.display === 'none') {
						uiComp.setNeedResize(true);
					}
				}
			}
		},
		onDidLoadFile: (viewer, file) => {
			if (file.path == '/sdi/ana/home.dash' || file.path == getAppPath('/index.dash')) {
				viewer.domBase.classList.add('home-dash');
				hidePanel(viewer, ['panel28']);
			}
			if (file.path == getAppPath('/data-asset/数据资产业务-数据归集.dash')) {
				let menuArr = ['menu1_1', 'menu1_2', 'menu1_3', 'menu1_4', 'menu1_5',
					'menu1_26', 'menu1_16', 'menu1_18', 'menu1_19',
					'menu1_20', 'menu1_21', 'menu1_22', 'menu1_23', 'menu1_24', 'menu1_25'
				];
				hidePanel(viewer, menuArr);
			}
		},
		CustomActions: {
			showMyMessageDialog: (event) => {
				import('me/smessage').then(m => m.showMyMessageDialog({ visibleTypes: ['announcement', 'notice'] }));
			},
			showMgrPanel: (event) => {
				showPanel(event, 'panel28');
			},
			showItemDlg: (event) => {
				let textArr = ['text11', 'text12', 'text13', 'text14', 'text15',
					'text26', 'text16', 'text18', 'text19',
					'text20', 'text21', 'text22', 'text23', 'text24', 'text25'];
				let menuArr = ['menu1_1', 'menu1_2', 'menu1_3', 'menu1_4', 'menu1_5',
					'menu1_26', 'menu1_16', 'menu1_18', 'menu1_19',
					'menu1_20', 'menu1_21', 'menu1_22', 'menu1_23', 'menu1_24', 'menu1_25'
				];
				let targetId = event.component.getId();
				if (textArr.indexOf(targetId) < 0) return;
				showPanel(event, menuArr[textArr.indexOf(targetId)]);
			},
			logout: (event) => {
				location.replace(ctx('/api/auth/signout'));
			},
			showModifyDialog: (event: InterActionEvent) => {
				getSysSettings().then(settings => {
					return import("me/personalcenter").then(m => m.showModifyPWDialog({
						colorTheme: this.colorTheme,
						passwordRegex: settings["security.password.validRegex"],
						passwordRegexMessage: settings["security.password.validErrorMessage"]
					}));
				})
			},
			changePanel: (event: InterActionEvent) => {
				let timeStr = event.component.values.value;
				if (timeStr.indexOf('[') != -1) {//取值范围
					event.page.getComponent('panelbook3').setValue('panel2');
				} else {//时间值
					event.page.getComponent('panelbook3').setValue('panel1');
				}
			},
			changePanel3: (event: InterActionEvent) => {
				let timeStr = event.component.values.value;
				if (timeStr.indexOf('[') != -1) {//取值范围
					event.page.getComponent('panel2').setValue('panel2');
				} else {//时间值
					event.page.getComponent('panel2').setValue('panel1');
				}
			}
		}
	}
}


function hidePanel(viewer, panelIds) {
	panelIds.forEach((panelId) => {
		var panel = viewer.anaObject.getComponent(panelId);
		panel && panel.domBase && (panel.domBase.style.display = 'none');
	})
}
function showPanel(event, panelId) {
	let panel = document.getElementById(panelId);
	panel.style.display = !panel.style.display ? 'none' : '';
}

/**
 * 加载新增数据血统spg
 */
function renderLineageAddSpg(page: SuperPage) {
	let builder: SuperPageBuilder = page && page.builder;
	let anaModelInfo: AnaModelInfo = { path: "/sysdata/data/tables/sdi/SDI_PEDIGREE_ANALYSIS_EXT.tbl", pageSize: 50 };
	builder.createModelAsync(anaModelInfo).then((model) => {
		let list1 = builder.getComponent('list1');
		builder.beginUpdate();
		try {
			builder.addModel(model);
			list1.setProperty(PropertyNames.DataSet, model.getId());
			let column = list1.createSubComponent({
				type: "list.column",
				showAllSelect: true,
				showCheckbox: true,
				captionRow: {
					colWidthType: "fix",
					colWidth: 30
				}
			}, true);
			list1.addSubComponent(column, 0);
			//隐藏最后两列
			let cols = list1.getSubComponents();
			let length = cols.length;
			cols[length - 1].setProperty("visible", 'false');
			cols[length - 2].setProperty("visible", 'false');
		} finally {
			builder.endUpdate();
		}
		return page.waitRender().then(() => {
		});
	});
}

/**
 * 加载标注文档在线查看spg
 * 修改为加载完整个文档后查看，避免url地址过长
 */
function renderStandardViewSpg(page: SuperPage): Promise<void> {
	let builder: SuperPageBuilder = page && page.getBuilder();
	let standard_Id = page.getData().getUrlParams().standard_Id;
	let isHistory = page.getData().getUrlParams().isHistory;
	let u = getAppPath("/data-standard/standard-doc/standardHTML.action");
	let data: JSONObject = {};
	data.standard_Id = standard_Id;
	if (isHistory == "true") {
		let versionId = page.getData().getUrlParams().versionId;
		u = getAppPath("/data-standard/standard-doc/standardHTML.action?method=getHistoryStandard");
		data.versionId = versionId;
	}
	return rc({
		url: u,
		data: data
	}).then((result: JSONObject) => {
		let html = page.getData().getComponent("html1");
		let jsComponent = page.getData().getComponent("jsComponent1");
		html.setProperty("html", result.html);
		jsComponent && jsComponent.setProperty("params", [{ name: "value", value: result.catalog }]);
		return;
	})
}

/**
 * 加载api在线浏览文档
 * @param page
 */
function renderAPIViewSpg(page: SuperPage) {
	let builder: SuperPageBuilder = page && page.getBuilder();
	let task_id = page.getData().getUrlParams().task_id;
	let u = getAppPath("/data-exchange/ex-task/ex-api-doc/apiHTML.action");
	let data: JSONObject = {};
	data.task_id = task_id;
	return rc({
		url: u,
		data: data
	}).then((result: JSONObject) => {
		let html = page.getData().getComponent("html1");
		let jsComponent = page.getData().getComponent("jsComponent1");
		html.setProperty("html", result.html);
		jsComponent.setProperty("params", [{ name: "value", value: result.catalog }]);
		return;
	})
}

/**
 * 加载检测报告页面
 */
function renderCheckViewSpg(page: SuperPage) {
	let builder: SuperPageBuilder = page && page.getBuilder();
	let task_id = page.getData().getUrlParams().task_id;
	let u = getAppPath("/data-govern/check-task/check-doc/checkReport.action");
	let data: JSONObject = {};
	data.task_id = task_id;
	return rc({
		url: u,
		data: data
	}).then((result: JSONObject) => {
		let html = page.getData().getComponent("html1");
		html.setProperty("html", result.html);
		return;
	})
}

/**
 * 加载数据资产目录文档时处理收藏按钮
 */
function renderTableViewSpg(page: SuperPage) {
	let builder: SuperPageBuilder = page && page.getBuilder();
	let resid = page.getData().getUrlParams().resid;
	let isFavorite = getMetaRepository().isFavorited(resid);
	isFavorite.then(result => {
		if (result) {
			let button = page.getData().getComponent("button1");
			button.getDataNode().setProperty("prefixIcon", { code: "5439" });
		}
	})
}

/**
 * 获取ZT应用中js、css资源的url地址。在import的时候需要
 * 参数relativeUrl的写法：
 * 1、meta-data/meta-data-mgr.js
 * @param relativeUrl
 */
function getZTResourceUrl(relativeUrl: string): string {
	return ctx(getAppPath(relativeUrl));
}

function getZTDataMgrPage(param: string): Promise<JSONObject> {
	let path = null;
	if (param.endsWith("master-data")) {
		path = getZTResourceUrl("/master-data/master-data-mgr.js");
	} else {
		path = getZTResourceUrl("/meta-data/meta-data-mgr.js");
	}
	return import(path).then((m) => {
		return m.getZTDataMgrPage();
	});
}
/**
 * 自定义控件，用于动态显示每个表的标签
 */
export class CustomJs_labelFolatePanel extends Component {
	public _reInit(args: any): void {
		let domBase = args.domParent.children[0];
		let count = domBase.childElementCount;
		let labels = args.value;
		let list = typeof labels === 'string' ? labels.split(",") : labels;
		if (count > 0) {
			let children = domBase.childNodes;
			for (let i = 0; children.length > 0;) {
				children[i].remove()
			}
		}

		for (let i = 0; labels != "" && list && i < list.length; i++) {
			let labelDiv: HTMLElement = document.createElement('div');
			let labelSpan: HTMLElement = document.createElement('span');
			labelSpan.innerText = list[i];
			labelDiv.classList.add('labelFolatePanel');
			labelDiv.classList.add('labelFolatePanel_color_' + i % 3);
			labelDiv.appendChild(labelSpan);
			domBase.appendChild(labelDiv);
		}
		domBase.classList.add('labelFolate_base');
	}
	protected _init(args: any): HTMLElement {
		let domBase = super._init(args);
		let labels = args.value;
		let list = typeof labels === 'string' ? labels.split(",") : labels;
		for (let i = 0; labels != "" && list && i < list.length; i++) {
			let labelDiv: HTMLElement = document.createElement('div');
			let labelSpan: HTMLElement = document.createElement('span');
			labelSpan.innerText = list[i];
			labelDiv.classList.add('labelFolatePanel');
			labelDiv.classList.add('labelFolatePanel_color_' + i % 3);
			labelDiv.appendChild(labelSpan);
			domBase.appendChild(labelDiv);
		}
		domBase.classList.add('labelFolate_base');
		return domBase;
	}

	public dispose() {
		super.dispose();
	}
}

/**
 * 标准文档浏览页面左侧树
 */
export class CustomJs_StandardCatalogTree extends Component {
	public tree: Tree;
	public domBase: HTMLElement;
	public _reInit(args: any): void {
		let value = args.value;
		if (typeof value == "string" && value != "") {
			value = JSON.parse(value);
		}
		//this.tree.dispose();
		let dp = new DataProvider({
			data: value
		});
		this.tree.setDataProvider(dp);
		this.tree.waitRender().then(() => {
			this.tree.expandAll(true);
		})
	}
	protected _init(args: any): HTMLElement {
		let domBase = this.domBase = super._init(args);
		let value = args.value;
		if (typeof value == "string" && value != "") {
			value = JSON.parse(value);
		}
		let dp = new DataProvider({
			data: value
		});
		let catalogTree = this.tree = new Tree({
			domParent: domBase,
			cssText: "min-width: 400px;",
			className: "standard_catalog",
			iconVisible: true,
			itemDescVisible: true,
			onclick: (args, tree) => {
				let item = args.item;
			},
			dataProvider: dp
		});
		this.tree.expandAll(true);
		return domBase;
	}

	public dispose() {
		super.dispose();
	}
}

//将后台的计划任务界面嵌入到前台页面
export class CustomJS_HistoryView extends MetaFileRevisionView {
	public domBase;
	public _reInit(args: any): void {

	}
	protected _init(args: any): HTMLElement {
		let domBase = this.domBase = super._init(args);
		let resid = args.resid;
		this.checkbox.setVisible(false);
		this.rebuildContent();
		return domBase;
	}

	protected rebuildContent() {
		let dp = new VersionGridDataProvider();
		this.initPromise = import('commons/tree').then(m => {
			if (this.grid) {
				this.grid.dispose();
			}
			this.grid = new m.Grid({
				id: "history-revision-grid",
				layoutTheme: "dividerlarge",
				keyPrefix: "meta.historyGrid",
				overflowY: 'auto',
				dataProvider: dp,
				domParent: this.domBase,
				onclick: (sze: SZEvent) => {
					if (sze.data) {
						let targetClassList = sze.target.classList;
						let data = sze.data;
						if (targetClassList.contains("fileinfo-revisionoperation-edit")) {
							this.editFile(data);
						} else if (targetClassList.contains("fileinfo-revisionoperation-reset")) {
							this.resetVersion(data);
						} else if (targetClassList.contains("fileinfo-revisionoperation-showdiff")) {
							this.showDiff(data);
						} else if (targetClassList.contains("fileinfo-revisionoperation-check")) {
							this.viewImageFile(data);
						} else if (targetClassList.contains("fileinfo-revisionoperation-download")) {
							this.download(data);
						} else if (targetClassList.contains("fileinfo-revisionoperation-showmodeldiff")) {
							this.showModelDiff(data);
						}
					}
				},
				columns: [
					// revision不展示，做排序用
					{
						id: 'revision',
						width: '0px'
					}, {
						id: 'revisionDesc',
						flexGrow: 1
					}, {
						id: 'revisionCaption',
						flexGrow: 2
					}, {
						id: 'revisionModifyTime',
						flexGrow: 1.5
					}, {
						id: 'revisionModifyUser',
						flexGrow: 1
					},
					// 隐藏未完成项
					//  {
					// 	id: 'revisionNote',
					// 	width: '250px',
					// } ,
					{
						id: 'revisionOperation',
						flexGrow: 1,
						dataType: "html",
						htmlField: "revisionOperation"
					}]
			});

			this.initPromise = null;
		});
	}
}

/**
 * 文件历史版本对话框中Grid的dataProvider
 */
class VersionGridDataProvider implements IDataProvider<JSONObject> {
	/**
	 * 文件路径或id
	 */
	private pathOrId: string;
	/**
	 * 文件信息
	 */
	private fileInfo: MetaFileInfo;

	/** 是否可编辑版本 */
	private editable: boolean;

	/** 是否可还原版本 */
	private resettable: boolean;

	/** 版本操作 - 编辑按钮html */
	private editBtnHtml: string;
	/** 版本操作 - 还原按钮html */
	private resetBtnHtml: string;
	/** 版本操作 - 对比按钮html */
	private showDiffBtnHtml: string;
	/** 版本操作 - 模型结构对比按钮html */
	private showModelDiffBtnHtml: string;
	/** 版本操作 - 查看按钮html */
	private checkBtnHtml: string;
	/** 版本操作 - 下载按钮html */
	private downloadBtnHtml: string;

	constructor() {
		this.editBtnHtml = `<span class="fileinfo-revisionoperation-edit fileinfo-revisionoperation-btn">${message("meta.historyGrid.column.revisionOperation.edit")}</span>`;
		//this.resetBtnHtml = `<span class="fileinfo-revisionoperation-reset fileinfo-revisionoperation-btn">${message("meta.historyGrid.column.revisionOperation.reset")}</span>`;
		this.showDiffBtnHtml = `<span class="fileinfo-revisionoperation-showdiff fileinfo-revisionoperation-btn">${message("meta.historyGrid.column.revisionOperation.showDiff")}</span>`;
		this.showModelDiffBtnHtml = `<span class="fileinfo-revisionoperation-showmodeldiff fileinfo-revisionoperation-btn">${message("meta.historyGrid.column.revisionOperation.showModelDiff")}</span>`;
		this.checkBtnHtml = `<span class="fileinfo-revisionoperation-check fileinfo-revisionoperation-btn">${message("meta.historyGrid.column.revisionOperation.check")}</span>`;
		this.downloadBtnHtml = `<span class="fileinfo-revisionoperation-download fileinfo-revisionoperation-btn">${message("meta.historyGrid.column.revisionOperation.download")}</span>`;
	}

	/**
	 * 传入文件path或者id来改变内部文件状态
	 * @param pathOrId
	 */
	public setState(pathOrId: string): Promise<any> {
		let user = getCurrentUser();
		return getMetaRepository().getFile(pathOrId, false, false, false).then(file => {
			if (file) {
				this.fileInfo = file;
				let path = this.pathOrId = file.path;
				return user.checkAllowed([{ path: path, operation: PermissionOperation.Mgr_Edit }, { path: path, operation: "mgr-m-save" }]).then((flags: boolean[]) => {
					this.editable = flags[0];
					this.resettable = flags[1];
				});
			}
			this.pathOrId = null;
			this.fileInfo = null;
			this.editable = this.resettable = false;
		})
	}

	/**
	 * 获取数据
	 */
	public fetchData(): Promise<any[]> {
		if (this.pathOrId) {
			return Promise.all([getMetaRepository().getFile(this.pathOrId, false, true), getCurrentUser().ensureFetchData()]).then(([res, user]) => {
				this.fileInfo = res;
				let nowRevision = parseInt(res.revision);
				let type = res.type;
				return getMetaRepository().getRevisions(this.fileInfo.id).then(res => {
					return this.handleData(res, nowRevision, type, user.szTypes);
				});
			})
		} else {
			return Promise.resolve([]);
		}
	}
	/**
	 * 将获取到的数据处理为MetaRevisionGridDataInfo格式的数据
	 */
	private handleData(res: MetaRevisionInfo[], nowRevision: number, type: string, szTypes: string[]): MetaRevisionGridDataInfo[] {
		return res.map((item) => {
			return this.fillData(type, nowRevision, item, szTypes);
		}).sort((a, b) => {
			return b.revision - a.revision;
		});
	}

	/**
	 * 填充数据，根据文件类型渲染不同的按钮
	 * sztype和文本文件类型显示编辑，还原，差异按钮
	 * 图片类型显示查看，还原，差异按钮
	 * 其他类型文件显示下载，还原按钮
	 * revision为0不显示差异
	 * revision为最新版本不显示还原
	 * @param type
	 * @param isNowVersion
	 * @param item
	 */
	private fillData(type: string, nowRevision: number, item: MetaRevisionInfo, szTypes: string[]) {
		let isFirstVersion = item.revision == 0;
		let isNowVersion = item.revision === nowRevision;
		let operation: string;
		/** 编辑需要有编辑权限，还原版本需要有保存权限，查看差异需要查看权限，打开版本控制对话框就需要有查看权限，这里不载判断查看权限 */
		if (szTypes.includes(type) || TXT_FILETYPE.includes(type)) {
			if (isFirstVersion && isNowVersion) {
				operation = this.editable ? this.editBtnHtml : "";
			} else if (isFirstVersion) {
				operation = `${this.editable ? this.editBtnHtml : ""}`;
			} else if (isNowVersion) {
				operation = `${this.editable ? this.editBtnHtml : ""}${this.showDiffBtnHtml}${type == "tbl" ? this.showModelDiffBtnHtml : ""}`;
			} else {
				operation = `${this.editable ? this.editBtnHtml : ""}${this.showDiffBtnHtml}${type == "tbl" ? this.showModelDiffBtnHtml : ""}`;
			}
		} else if (IMAGE_FILETYPE.includes(type)) { /** 有查看权限就可以下载 */
			if (isFirstVersion && isNowVersion) {
				operation = this.checkBtnHtml;
			} else if (isFirstVersion) {
				operation = `${this.checkBtnHtml}`;
			} else if (isNowVersion) {
				operation = `${this.checkBtnHtml}${this.showDiffBtnHtml}${type == "tbl" ? this.showModelDiffBtnHtml : ""}`;
			} else {
				operation = `${this.checkBtnHtml}${this.showDiffBtnHtml}${type == "tbl" ? this.showModelDiffBtnHtml : ""}`;
			}
		} else {
			operation = `${this.downloadBtnHtml}`;
		}
		return {
			fileType: this.fileInfo.type,
			filePath: this.fileInfo.path,
			revision: item.revision,
			revisionDesc: isNowVersion ? `r${item.revision}*` : `r${item.revision}`,
			revisionCaption: this.fileInfo.name,
			revisionModifyTime: formatDateFriendly(new Date(item.modifyTime)),
			revisionModifyUser: item.modifier,
			revisionOperation: operation
		}
	}
}

//以csv的形式导出数据探查的内容
function exportData(panel: IDwTableEditorPanel, fileName: string, format: DataFormat, range?: string): Promise<void> {
	return import("dw/dwapi").then((m) => {
		let dwTable = panel.getDwTable();
		let dwTableData = dwTable.getData();
		let queryInfo = dwTableData.getEasyQueryInfo({ limit: 20 });
		let filters = dwTable.getEasyFilter();
		let fileInfo = dwTable.getFileInfo();
		let paginator = panel.panels.modelData.paginator;
		queryInfo.options.limit = undefined;
		if (range === 'currentPage' && paginator) {
			let pageIndex = paginator.getPageIndex();
			let pageSize = paginator.getPageSize();
			queryInfo.options.limit = pageSize;
			queryInfo.options.offset = pageSize * pageIndex;
		}
		queryInfo.options.needCodeDesc = true;
		let resid = fileInfo.id;
		m.exportQuery({
			resid: resid,
			query: queryInfo,
			fileFormat: format,
			fileName: fileName
		});
	});
}
//将查询的结果转化为下拉框支持的格式
function converFilterResultData(data: (string | number | boolean)[][]): JSONObject[] {
	let results: JSONObject[] = [];
	for (let i = 0; i < data.length; i++) {
		let info: JSONObject = {};
		info.caption = data[i][0];
		info.value = data[i][1];
		results.push(info);
	}
	return results;
}
//将勾选框的内容转化成query可以识别形式进行提交
function getCheckedValues(checkedRows: JSONObject[], idColumn: any): (string | number)[][] {
	let listColum: string[] = [];
	let result: (string | number)[][] = [];
	for (let i = 1; i < (idColumn.length - 2); i++) {
		listColum.push(idColumn[i].getId());
	}
	for (let i = 0; i < checkedRows.length; i++) {
		let info = [];
		let row = checkedRows[i];
		for (let j = 0; j < listColum.length; j++) {
			info.push(row[listColum[j]].field);
		}
		info.push(1);
		result.push(info);
	}
	return result;
}

function getDeleteIds(checkedRows: JSONObject[], idColumn: any): (string | number)[][] {
	let length = idColumn.length;
	let listColum: string = idColumn[length - 1].getId();
	let result: (string | number)[][] = [];
	for (let i = 0; i < checkedRows.length; i++) {
		let info = [];
		let row = checkedRows[i];
		info.push(row[listColum].field);
		result.push(info);
	}
	return result;
}


/**
 * CSTM-11820 导出数据字典，传入需要导出的模型表id，支持输入文件名称、选择文件类型（csv、xlsx、xls）
 * @params resids
 */
function exportDataDictionary(resids: Array<string>): void {
	let dataDictionaryTablePath = "/sysdata/data/tables/sdi/data-asset/SDI_DATA_DICTIONARY.tbl";
	let exp = "\'" + resids.join("\',\'") + "\'";

	let exportFile = (fileName: string, format: DataFormat): Promise<void> => {
		return getMetaRepository().getFileBussinessObject<DwTableModelFieldProvider>(dataDictionaryTablePath).then(dwbo => {
			if (!dwbo) {
				showErrorMessage("数据字典模型表不存在");
				return;
			}
			let resid = dwbo.getFileInfo().id;
			let queryFields: QueryFieldInfo[] = dwbo.getFields().filter(field => field.dbfield != "ID").map(field => {
				return {
					name: field.name,
					exp: `[t0].${quoteExpField(field.name)}`
				}
			});
			let filters: FilterInfo[] = [
				{
					"matchAll": true,
					"enabled": true,
					"clauses": [{
						"exp": `t0.id in (${exp})`
					}]
				}];
			let queryInfo = {
				fields: queryFields,
				sources: [{
					id: "t0",
					path: dataDictionaryTablePath
				}],
				filter: filters,
				sort: [],
				options: {
					limit: null,
					offset: null,
					queryTotalRowCount: true
				}
			};

			return exportQuery({
				resid: resid,
				query: queryInfo,
				fileFormat: format,
				fileName: fileName
			})
		})
	}

	showFormDialog({
		id: 'spg.exportdatadictionary',
		caption: '导出数据字典',
		content: {
			items: [{
				id: 'fileName',
				caption: '文件名称',
				formItemType: 'edit',
				captionPosition: 'left'
			}, {
				id: 'format',
				caption: '文件类型',
				formItemType: 'selectpanel',
				captionPosition: 'left',
				compArgs: {
					items: [{
						value: 'xlsx',
						caption: 'xlsx'
					}, {
						value: 'xls',
						caption: 'xls'
					}, {
						value: 'csv',
						caption: 'csv'
					}]
				}
			}],
		},
		onshow: (event: SZEvent, dialog: Dialog) => {
			let form = <Form>dialog.content;
			form.loadData({ fileName: '数据字典', format: 'xlsx' });
		},
		buttons: [{
			id: "ok",
			layoutTheme: "defbtn",
			onclick: (event: SZEvent, dialog: Dialog, item: any) => {
				let data = (<any>dialog).form.getData();
				let format = data.format;
				let fileName = data.fileName;
				let range = data.range;
				dialog.waitingClose(exportFile(fileName, format));
			}
		}, 'cancel']
	});
}

//将query结果构造成dp需要的格式
function praseTableResult(data: (string | number | boolean)[][]): JSONObject {
	let list = [];//用于数据域的去重
	let results: JSONObject = {
		domains: [],
		tables: []
	};
	data.forEach(d => {
		let domain: JSONObject = {};
		let table: JSONObject = {};
		domain.value = d[1];
		domain.caption = d[2];
		if (list.indexOf(d[1]) == -1) {
			results.domains.push(domain);
			list.push(d[1]);
		}
		table.id = d[5];
		table.value = d[3];
		table.caption = d[4];
		table.domain = d[1];
		table.resid = d[0];
		results.tables.push(table);
	})
	return results;
}

//获取相应数据域下的数据表
function getTableFromTableResult(tableResult: JSONObject, domain: string): any[] {
	let tables = tableResult.tables;
	let result = [];
	tables.forEach(t => {
		if (t.domain == domain) {
			result.push(t);
		}
	});
	return result;
}

/**
 * 20200617 liuyongz
 * https://jira.succez.com/browse/CSTM-12183
 * 让血统维护界面显示来源系统
 * 重构该类
 *
 * 0915 liuyz
 * 重构右键事件，不让其显示定位打开等属性
 */
class SDI_DwLineageAnalysisViewData {
	private doContextMenu(sze: SZEvent, node, graphNode): void {

	}
}

modifyClass("dw/dwview", "DwLineageAnalysisGraph", SDI_DwLineageAnalysisViewData);

class TestCompiler extends ExpCompiler {
	public forDemo: boolean;
	/**
	 * 测试专用
	 */
	getFunc(varname: string, parent?: any) {
		var result = super.getFunc(varname, parent);
		if (this.forDemo === false) {
			return result;
		}
		if (result)
			return assign({}, result, {
				checkParams: function() {
					return true;
				}
			});
		return { // 测试用，保证所有函数都是可用的
			checkParams: function() {
				return true;
			},
			name: "test",
			desc: "返回一个测试用的函数json，保证不报错",
			group: "",
		};
	}

	/**
	 * 测试专用
	 */
	getVar(varname: string, parent?: any) {
		var result = super.getVar(varname, parent);
		if (this.forDemo === false) {
			return result;
		}
		if (result)
			return result;
		return new ExpVar({
			name: varname,
			caption: varname,
		});
	}

	getParentVar(prevToken: Token, compiler: ExpCompiler, varnames?: Array<string>): any {
		if (!varnames)
			varnames = [];
		while (prevToken) {
			if (prevToken.getIndex() === ExpTokenIndex.DOT) {
				prevToken = prevToken.getPrevToken();
				if (prevToken) {
					if (prevToken.getIndex() === ExpTokenIndex.IDENTIFIER) {
						// varnames.unshift(prevToken.getName());
						varnames.unshift(prevToken.orginToken);
					} else {
						return false; // 点前面不是标识符
					}
					prevToken = prevToken.getPrevToken();
					continue;
				} else { // 点前面没有token，
					return false;
				}
			}
			break;
		}
		return varnames[0];
	}

	getAllParentVar(exp: string) {
		let expression = this.compile(exp);
		let tokens = expression.getAllTokens();
		let parentVars = [];
		for (let i = 0; i < tokens.length; i++) {
			let token = tokens[i];
			let parentVar = this.getParentVar(token.getPrevToken(), this);
			if (parentVar && parentVars.indexOf(parentVar) == -1) {
				parentVars.push(parentVar);
			}
		}
		return parentVars;
	}
}

function addDataSource(projectName: string) {
	showDbConnectionDialog({ projectName: projectName }).then(dbName => {
		rc({
			url: getAppPath("/data-access/dataSourceMgr.action?method=mgrDataSource"),
			method: "POST",
			data: {
				dbName: dbName
			}
		}).then(() => {
			getDwTableDataManager().updateCache([{ path: TABLE_INFO, type: 'refreshall' }], true);
		});
	})
}

function editDataSource(projectName: string, info: DbConnectionInfo, dataType: string) {
	showDbConnectionDialog({
		projectName: projectName,
		dbConnectionInfo: info
	}).then(dbName => {
		rc({
			url: getAppPath("/data-access/dataSourceMgr.action?method=mgrDataSource"),
			method: "POST",
			data: {
				dbName: dbName,
				dataType: dataType
			}
		}).then(() => {
			getDwTableDataManager().updateCache([{ path: TABLE_INFO, type: 'refreshall' }], true);
		});
	})
}

function deleteDataSource(dbName: string) {
	rc({
		url: getAppPath("/data-access/dataSourceMgr.action?method=deleteDataSource"),
		method: "POST",
		data: {
			dbName: dbName
		}
	})
}

/**
 * 克隆数据源
 */
function cloneDataSource(projectName: string, dbName: string) {
	rc({
		url: getAppPath("/data-access/dataSourceMgr.action?method=getCloneDataSource"),
		method: "POST",
		data: {
			dbName: dbName
		}
	}).then((info) => {
		return new Promise((resolve) => {
			return showDialog({
				id: "dbConnectionDialog",
				className: "ds-dbConnectionDialog",
				dragHandler: ".ds-dbConnectionPanel-toolbar",
				resizable: false,
				modal: true,
				closeIconVisible: false,
				width: 1020,
				height: 582,
				content: {
					implClass: DbConnectionPanel,
				},
				onshow: (sez: SZEvent, dialog: Dialog) => {
					let dbConnectionPanel = <DbConnectionPanel>dialog.content;
					dbConnectionPanel.initPromise.then(() => {
						dbConnectionPanel.setOwner(dialog);
						dbConnectionPanel.setProjectName(projectName);
						dbConnectionPanel.isSelectDsView = !info || !info.dbType;
						dbConnectionPanel.refreshPanel();
						dbConnectionPanel.dbConnectionEditPanel.loadData(undefined).then(() => {
							dbConnectionPanel.getDbConnectionEditPanel().setItemValue('name', info.name);
							dbConnectionPanel.getDbConnectionEditPanel().setItemValue('driver', info.driver);
							dbConnectionPanel.getDbConnectionEditPanel().setItemValue('databaseName', info.databaseName);
							dbConnectionPanel.getDbConnectionEditPanel().setItemValue('port', info.port);
							dbConnectionPanel.getDbConnectionEditPanel().setItemValue('host', info.host);
							dbConnectionPanel.getDbConnectionEditPanel().setItemValue('dbType', info.dbType);
							dbConnectionPanel.getDbConnectionEditPanel().dbType = info.dbType;
							dbConnectionPanel.getDbConnectionEditPanel().refreshDbCaption();
							dbConnectionPanel.getDbConnectionEditPanel().setItemValue('purpose', info.purpose);
							dbConnectionPanel.getDbConnectionEditPanel().setItemValue('nameOrSid', info.nameOrSid);
							dbConnectionPanel.getDbConnectionEditPanel().setItemValue('user', info.user);
							dbConnectionPanel.getDbConnectionEditPanel().setItemValue('password', info.password);
							dbConnectionPanel.getDbConnectionEditPanel().passwordValue = info.password;
							dbConnectionPanel.getDbConnectionEditPanel().setItemValue('driver', info.driver);
							dbConnectionPanel.getDbConnectionEditPanel().setItemValue('url', info.url);
							dbConnectionPanel.getDbConnectionEditPanel().setItemValue('waitTime', info.waitTime);
							dbConnectionPanel.getDbConnectionEditPanel().setItemValue('testConnectionOnCheckout', info.testConnectionOnCheckout);
							dbConnectionPanel.getDbConnectionEditPanel().setItemValue('desc', info.desc);
						});
					})
				},
				onok: (e: SZEvent, dialog: Dialog) => {
					let dbConnectionPanel = <DbConnectionPanel>dialog.content;
					let savePromise = dbConnectionPanel.getDbConnectionEditPanel().save();
					let dbName = dbConnectionPanel.getDbConnectionEditPanel().getFormItemValue("name");
					savePromise && dialog.waitingClose(savePromise).then(resolve => {
						console.log(resolve);
						rc({
							url: getAppPath("/data-access/dataSourceMgr.action?method=mgrDataSource"),
							method: "POST",
							data: {
								dbName
							}
						}).then(() => {
							getDwTableDataManager().updateCache([{ path: TABLE_INFO, type: 'refreshall' }], true);
						});
					}, err => {
						if (err) {
							throw err;
						} else {

						}
					});
				}
			})
		})
	});

}


interface CustomFieldItem {
	id: string;
	caption: string;//字段标题
	pickValue: string[];//选择值
	value: string;
	dataType: string;//字段类型
	dbfield: string;//物理字段名称
	isDimension: boolean;//是否为维度
	isField: boolean;//是否为字段
	leaf: boolean;//是否为叶子节点
	icon: string;//图标
	name: string;
	exp?: string;
	desc?: string;
}
/**
 * 20210720 liuyz
 * https://jira.succez.com/browse/CSTM-15853
 * 创建字段获取的dp
 */
class CustomFieldDp implements IFieldsDataProvider {
	private dataBase: string;
	private dbTable: string;
	private expDp: CustomExpDp;
	public constructor(args: { dataBase: string, dbTable: string }) {
		this.dataBase = args.dataBase;
		this.dbTable = args.dbTable;
		this.expDp = new CustomExpDp();
	}
	fetchDataSources(): Promise<{ id: string; desc?: string; isCurrentDataSource?: boolean; }[]> {
		return Promise.resolve([{
			id: this.dbTable,
			isCurrentDataSource: true,
			desc: this.dbTable
		}])
	}

	fetchFields(sourceId: string, fetchArgs: { [propName: string]: any; isDimension?: boolean; }): JSONObject {
		return fetchArgs?.isDimension && rc({
			url: getAppPath("/data-access/dataAccessMgr.action?method=getFieldInfos"),
			data: {
				dataBase: this.dataBase,
				dbTable: this.dbTable
			}
		}).then(result => {
			let data: CustomFieldItem[] = [];
			for (let i = 0; i < result.length; i++) {
				let info = result[i];
				let name = info.name.toUpperCase();
				let id = "[" + name + "]";
				let json: CustomFieldItem = {
					id,
					caption: name,
					name: name,
					pickValue: [name],
					value: id,
					dataType: info.dataType,
					dbfield: name,
					isDimension: true,
					isField: true,
					leaf: true,
					icon: "icon-string-dim"
				};
				data.push(json);
				this.expDp.setField(json);
			};
			return data;
		})
	}
	searchFields(sourceId: string, searchArgs: { [propName: string]: any; keyword?: string; isDimension?: boolean; }): Promise<JSONObject[]> {
		let key = searchArgs.keyword.toUpperCase();
		return searchArgs?.isDimension && rc({
			url: getAppPath("/data-access/dataAccessMgr.action?method=getFieldInfos"),
			data: {
				dataBase: this.dataBase,
				dbTable: this.dbTable
			}
		}).then(result => {
			let data: CustomFieldItem[] = [];
			for (let i = 0; i < result.length; i++) {
				let info = result[i];
				let name = info.name.toUpperCase();
				let id = "[" + name + "]";
				if (name.indexOf(key) != -1) {
					let json: CustomFieldItem = {
						id,
						caption: name,
						name: name,
						pickValue: [name],
						value: id,
						dataType: info.dataType,
						dbfield: name,
						isDimension: true,
						isField: true,
						leaf: true,
						icon: "icon-string-dim"
					};
					data.push(json);
				}
			};
			return data;
		})
	}
	public getExpDp() {
		return this.expDp;
	}
}

/**
 * 定制的表达式dp
 * liuyz 20210721
 */
class CustomExpDp implements IExpDataProvider {
	private tableAndFieldsExpVar: ExpVar[];
	private fields: string[];
	private tableFieleMap: JSONObject;//数据表和模型对应关系
	public getVarsByParent(parentVar?: IExpVar): Array<IExpVar> {
		if (parentVar != null && !isEmpty(this.tableFieleMap)) {
			return this.tableFieleMap[parentVar.getName()];
		}
		return this.tableAndFieldsExpVar;
	}

	public setTable(tableInfo) {
		let expVar = new ExpVar(tableInfo);
		this.tableAndFieldsExpVar == null && (this.tableAndFieldsExpVar = []);
		this.tableAndFieldsExpVar.push(expVar);
	}

	/**
	 * 设置字段表达式
	 * 字段的表达式需要记录下对应的模型id,方便后面进行获取
	 * @param fieldInfo
	 * @param sourceId
	 */
	public setField(fieldInfo, sourceId?: string) {
		let expVar = new ExpVar(fieldInfo);
		this.tableAndFieldsExpVar == null && (this.tableAndFieldsExpVar = []);
		this.fields == null && (this.fields = []);
		this.tableAndFieldsExpVar.push(expVar);
		this.fields.push(fieldInfo.caption);
		if (!isEmpty(sourceId)) {
			isEmpty(this.tableFieleMap) && (this.tableFieleMap = {});
			isEmpty(this.tableFieleMap[sourceId]) && (this.tableFieleMap[sourceId] = []);
			this.tableFieleMap[sourceId].push(expVar);
		}
	}

	public validateFieldName(name: string) {
		if (!name) {
			return message("dataflow.fieldname.null");
		}
		if (name && name.match(/[`"\[\]]+/)) {
			return message("dataflow.fieldname.existInvalidChar");
		}
		let upperName = name.toUpperCase();
		// 可以修改名称的大小写
		if (this.fields.indexOf(upperName) != -1) {
			return message("dataflow.fieldname.repeat");
		}
		return null;
	}
}
const DIMENSION_VAR_ICON: { [key: string]: string } = {
	"C": "icon-string-dim",
	"N": "icon-float-dim",
	"I": "icon-int-dim",
	"D": "icon-date-dim",
	"T": "icon-date-dim",
	"P": "icon-date-dim",
	"M": "icon-clob-dim",
	"X": "icon-blob-dim"
};

const MEASURE_VAR_ICON: { [key: string]: string } = {
	"C": "icon-string",
	"N": "icon-float",
	"I": "icon-int",
	"D": "icon-date",
	"T": "icon-date",
	"P": "icon-date",
	"M": "icon-clob",
	"X": "icon-blob"
};
/**
 * 20211015 kongwq
 * 创建自定义规则字段的dp
 */
class CustomRuleFieldDp implements IFieldsDataProvider {
	private tableInfos: { id: string, desc?: string, isCurrentDataSource?: boolean }[];
	private ruleTableInfo: Record<string, string[]>; // 表别名和id对应对象
	private ruleTabelField: Record<string, CustomFieldItem[]>
	private expDp: CustomExpDp;
	private assginSourceId: string;//设置指定的数据源
	private currentSourceId: string;//当前查看的数据源
	constructor() {
		this.ruleTableInfo = {};
		this.ruleTabelField = {};
		this.expDp = new CustomExpDp();
	}

	/**
	 * 获取规则表id和别名
	 * @returns
	 */
	public fetchDataSources(): Promise<{ id: string, desc?: string, isCurrentDataSource?: boolean }[]> {
		if (!isEmpty(this.tableInfos)) {
			if (!isEmpty(this.assginSourceId)) {
				return Promise.resolve(this.tableInfos.filter(t => { return t.id == this.assginSourceId }));
			} else {
				return Promise.resolve(this.tableInfos.map(t => {
					if (t.id == this.currentSourceId) {
						t.isCurrentDataSource = true;
					}
					return t;
				}));
			}
		}
		return rc({
			url: getAppPath('/data-govern/dataGovern.action?method=getRuleTableInfos'),
		}).then(results => {
			let tableInfos = this.tableInfos = [];
			results.forEach((result: Array<string>) => {
				if (result[2] === null) {
					result[2] = result[1];
				}
				this.ruleTableInfo[result[1]] = [];
				this.ruleTableInfo[result[1]].push(result[0]);
				this.ruleTableInfo[result[1]].push(result[2])
				tableInfos.push({
					id: result[1],
					isCurrentDataSource: false,
					desc: result[2]
				})
				this.expDp.setTable({ name: result[1], caption: result[2] });
			});
			if (!isEmpty(this.assginSourceId)) {
				return tableInfos.filter(t => { return t.id == this.assginSourceId });
			} else {
				return tableInfos.map(t => {
					if (t.id == this.currentSourceId) {
						t.isCurrentDataSource = true;
					}
					return t;
				});
			}
		});
	}

	public fetchFields(sourceId: string, fetchArgs: { [propName: string]: any; isDimension?: boolean; }): JSONObject {
		if (this.ruleTabelField[sourceId]) {
			return this.ruleTabelField[sourceId].filter(f => { return f.isDimension == fetchArgs.isDimension });
		} else {
			return rc({
				url: getAppPath('/data-govern/dataGovern.action?method=getFieldInfos'),
				data: {
					tableId: this.ruleTableInfo[sourceId][0],
					isDimension: fetchArgs.isDimension
				}
			}).then(results => {
				const fieldItems: CustomFieldItem[] = [];
				if (!this.ruleTabelField[sourceId]) {
					this.ruleTabelField[sourceId] = [];
				}
				results.forEach((result: DwTableFieldInfo) => {
					const name = result.name.toUpperCase();
					const dbFieldName = result.dbFieldName;
					const icon = fetchArgs.isDimension ? DIMENSION_VAR_ICON[result.dataType] : MEASURE_VAR_ICON[result.dataType];
					const customFieldItem: CustomFieldItem = {
						id: dbFieldName,
						caption: name,
						name: dbFieldName,
						desc: name,
						pickValue: [sourceId, dbFieldName],
						value: name,
						dataType: result.dataType,
						dbfield: dbFieldName,
						isDimension: result.innerWrappedObject.dimension,
						isField: true,
						leaf: true,
						icon,
						exp: `${sourceId}.${dbFieldName}`
					};
					fieldItems.push(customFieldItem);
					this.ruleTabelField[sourceId].push(customFieldItem);
					this.expDp.setField(customFieldItem, sourceId);
				});
				return fieldItems;
			})
		}
	}

	public searchFields(sourceId: string, searchArgs: { [propName: string]: any; keyword?: string; isDimension?: boolean; }): Promise<JSONObject[]> {
		const key = searchArgs.keyword.toUpperCase();
		const fieldItems: CustomFieldItem[] = [];
		this.ruleTabelField[sourceId].forEach((item: CustomFieldItem) => {
			if ((item.name.indexOf(key) != -1 || item.caption.indexOf(key) != -1) && item.isDimension == searchArgs.isDimension) {
				fieldItems.push(item);
			}
		})
		return Promise.resolve(fieldItems);
	}

	public getExpDp() {
		return this.expDp;
	}

	/**
	 * 设置指定要查看的数据源
	 * @param assginSourceId
	 */
	public setAssginSource(assginSourceId: string) {
		this.assginSourceId = assginSourceId;
	}

	/**
	 * 设置当前要查看的数据源
	 * @param currentSourceId
	 */
	public setcurrentSource(currentSourceId: string) {
		this.currentSourceId = currentSourceId;
	}

	/**
	 * 初始化字段信息和
	 */
	public initTableAndFieldsInfo() {
		return this.fetchDataSources().then(sources => {
			return rc({
				url: getAppPath('/data-govern/dataGovern.action?method=getAllTableFieldInfos')
			}).then(results => {
				let sources = Object.keys(results);
				for (let i = 0; i < sources.length; i++) {
					const sourceId = sources[i];
					if (!this.ruleTabelField[sourceId]) {
						this.ruleTabelField[sourceId] = [];
					}
					let data = results[sourceId];
					data.dimensions.forEach((result: DwTableFieldInfo) => {
						this.setFieldInfo(result, sourceId, true)
					});
					data.measures.forEach((result: DwTableFieldInfo) => {
						this.setFieldInfo(result, sourceId, false)
					});
				}
			})
		})
	}

	private setFieldInfo(fieldInfo: DwTableFieldInfo, sourceId: string, isDimensions: boolean) {
		const name = fieldInfo.name.toUpperCase();
		const dbFieldName = fieldInfo.dbFieldName;
		const icon = isDimensions ? DIMENSION_VAR_ICON[fieldInfo.dataType] : MEASURE_VAR_ICON[fieldInfo.dataType];
		const customFieldItem: CustomFieldItem = {
			id: dbFieldName,
			caption: name,
			name: dbFieldName,
			desc: name,
			pickValue: [sourceId, dbFieldName],
			value: name,
			dataType: fieldInfo.dataType,
			dbfield: dbFieldName,
			isDimension: fieldInfo.innerWrappedObject.dimension,
			isField: true,
			leaf: true,
			icon,
			exp: `${sourceId}.${dbFieldName}`
		};
		this.ruleTabelField[sourceId].push(customFieldItem);
		this.expDp.setField(customFieldItem, sourceId);
	}
}


/**
 * 重构ServiceTaskPromise实现显示轮询进度条
 * kongwq 20210730
 */
class EtlRunTaskPromise extends ServiceTaskPromise<any> {
	private tasks: Array<string>;// 需要查询的任务列表
	private completedTasks: Array<string>;// 完成状态的任务ID列表
	private failedTasks: Array<string>;// 失败状态的任务ID列表
	private cancelledTasks: Array<string>;// 取消状态的任务ID列表
	private completedTaskResurces: Array<string>;// 完成状态的任务资源列表
	private failedTaskResurces: Array<string>;// 失败状态的任务资源列表
	private cancelledTaskResurces: Array<string>;// 取消状态的任务资源列表
	private startTime: number;// 轮询开始时间
	private progress: number;// 总进度
	constructor(args) {
		super(args);
		this.tasks = args.tasks;
		this.completedTasks = [];
		this.failedTasks = [];
		this.cancelledTasks = [];
		this.completedTaskResurces = [];
		this.failedTaskResurces = [];
		this.cancelledTaskResurces = [];
		this.startTime = args.startTime;
		this.progress = 0;
	}

	private startPoll1() {
		let poll = setInterval(async () => {
			this.queryTask().then((result) => {
				let currentTasks = [];
				let currentTaskResurces = [];
				result.data.forEach((data) => {
					if (data[3] === 1) {
						currentTasks = this.completedTasks;
						currentTaskResurces = this.completedTaskResurces;
					} else if (data[3] === 2) {
						currentTasks = this.failedTasks;
						currentTaskResurces = this.failedTaskResurces;
					} else if (data[3] === 3) {
						currentTasks = this.cancelledTasks;
						currentTaskResurces = this.cancelledTaskResurces;
					}
					this.changeProgress(data, currentTasks, currentTaskResurces);
				})
				if ((this.completedTasks.length + this.failedTasks.length + this.cancelledTasks.length) === this.tasks.length) {
					clearInterval(poll);
					// 完成任务后刷新数据
					getDwTableDataManager().updateCache([{ path: "/sysdata/data/tables/sdi/data-access/SDI_DATA_ACCESS_VIEW_TABLE.tbl", type: 'refreshall' }], true);
					const serviceTaskInfo = {
						state: ServiceTaskState.DONE,
						logs: [
							{ time: Date.now(), log: `完成任务${this.completedTasks.length}个` },
							{ time: Date.now(), log: `完成任务资源: "${this.completedTaskResurces.join('", "')}"` },
							{ time: Date.now(), log: `失败任务${this.failedTasks.length}个` },
							{ time: Date.now(), log: `失败任务资源: "${this.failedTaskResurces.join('", "')}"` },
							{ time: Date.now(), log: `完成任务${this.cancelledTasks.length}个` },
							{ time: Date.now(), log: `取消任务资源: "${this.cancelledTaskResurces.join('", "')}"` }
						],
						startTime: this.startTime,
						progress: 100
					}
					this.setResult(null, serviceTaskInfo);
				}
			})
		}, 1000)
	}

	private queryTask() {
		const query: QueryInfo = {
			fields: [{ name: "TASKID", exp: "model1.TASKID" }, { name: "LOGS", exp: "model1.LOGS" }, { name: "RESID.NAME", exp: "model1.RESID.NAME" }, { name: "STATE", exp: "model1.STATE" }],
			sources: [{
				id: "model1",
				path: "/sysdata/data/tables/sys/SCHEDULETASK_LOG.tbl"
			}],
			filter: [{
				exp: `model1.START_TIME > ${this.startTime}`
			}, {
				exp: `model1.TASKID in "${this.tasks.join(',')}"`
			}]
		}
		return getQueryManager().queryData(query, uuid());
	}

	private changeProgress(data, currentTasks, curentTaskResurces) {
		let flag = false;
		for (let i = 0; i < currentTasks.length; i++) {
			if (currentTasks[i] === data[0]) {
				flag = true;
				break;
			}
		}
		if (!flag) {
			currentTasks.push(data[0]);
			curentTaskResurces.push(data[2]);
			const logs = JSON.parse(data[1]).logs;
			this.progress += 100 / this.tasks.length;
			// 在结束轮询前进度不能为100
			if (this.progress >= 100) {
				this.progress = 99;
			}
			const serviceTaskInfo: ServiceTaskInfo = {
				state: ServiceTaskState.RUNNING,
				logs: logs,
				startTime: this.startTime,
				progress: Math.floor(this.progress)
			}
			this.doProgressChange(serviceTaskInfo);
		}
	}
}

/**
 * 加载数据库已存在的数据
 */
function loadExistDataToList(storeDataset: IDataset, comp: IVComponent, task_id: string, type: string) {
	let tableEditor = comp.component.tableEditor;
	let compData = comp.getDataRows();
	if (!tableEditor.setNewFunction) {//列表初次加载时下载数据库表数据到暂存表中
		let oninlinecompchange_old = tableEditor.oninlinecompchange;
		tableEditor.oninlinecompchange = oninlinecompchange_new.bind(tableEditor);
		tableEditor.setNewFunction = true;
		function oninlinecompchange_new(event, cell, checkComp) {
			let addRows = [];
			let delRows = [];
			let lastCheckIndex = comp.component.lastCheckedRows;
			let lastCompData = comp.getCheckedDataRows();
			oninlinecompchange_old(event, cell, checkComp);
			let newCheckIndex = comp.component.lastCheckedRows;
			let newCompData = comp.getCheckedDataRows();
			if (lastCompData.length == null) {
				addRows.pushAll(newCompData);
			} else if (newCompData.length == null) {
				delRows.pushAll(lastCompData);
			} else {
				let difElement = lastCheckIndex.concat(newCheckIndex).map(function(v, i, arr) {
					if (arr.indexOf(v) === arr.lastIndexOf(v)) {
						i < lastCheckIndex.length ? delRows.push(lastCompData[i]) : addRows.push(newCompData[i - lastCheckIndex.length]);
						return v;
					}
				})
			}
			let deleteRows = delRows.map(function(v) {
				if (type == "nzgz") {
					let keys = { "CHECK_TASK_ID": task_id, "RULE_ID": v["GZDM"], "DW_TABLE_ID": v["MAIN_TABLE_ID"], "RULE_FROM": v["GZLY2"] };
					return keys;
				} else if (type == "cstm") {
					let keys = { "CHECK_TASK_ID": task_id, "RULE_ID": v["RULE_ID"], "DW_TABLE_ID": v["MAIN_TABLE_ID"], "RULE_FROM": 'CSTM' };
					return keys;
				}
			})
			let newAddRows = addRows.map(function(v) {
				if (type == "nzgz") {
					let keys = { "CHECK_TASK_ID": task_id, "RULE_ID": v["GZDM"], "DW_TABLE_ID": v["MAIN_TABLE_ID"], "RULE_FROM": v["GZLY2"] };
					return keys;
				} else if (type == "cstm") {
					let keys = { "CHECK_TASK_ID": task_id, "RULE_ID": v["RULE_ID"], "DW_TABLE_ID": v["MAIN_TABLE_ID"], "RULE_FROM": 'CSTM' };
					return keys;
				}
			})
			storeDataset.insert(newAddRows, true);
			storeDataset.delete(deleteRows, true);
		}
		let queryInfo: QueryInfo = {
			sources: [{
				id: "model1",
				path: "/sysdata/data/tables/dg/DG_CHECK_TASK_RULES.tbl",
				filter: `model1.CHECK_TASK_ID ='${task_id}' and model1.RULE_FROM ${type == "nzgz" ? "!='CSTM'" : "='CSTM'"}`
			}],
			fields: [{ name: "RULE_ID", exp: "model1.RULE_ID" }, { name: "RULE_FROM", exp: "model1.RULE_FROM" }, { name: "CHECK_TASK_ID", exp: "model1.CHECK_TASK_ID" }, { name: "DW_TABLE_ID", exp: "model1.DW_TABLE_ID" }],
		}
		return getQueryManager().queryData(queryInfo, uuid()).then(result => {
			let datas = result.data;
			if (isEmpty(compData)) {//列表本身没有数据，表示没有任何规则可以选择，此时不进行任何操作
				return;
			}
			let arrInsert: DatasetDataPackageRowInfo[] = [];
			let checkList = [];
			datas.forEach(data => {
				let row: DatasetDataPackageRowInfo = {
					data: { "RULE_ID": data[0], "RULE_FROM": data[1], "CHECK_TASK_ID": data[2], "DW_TABLE_ID": data[3] }
				}
				arrInsert.push(row);
				for (let i = 0; i < compData.length; i++) {
					if (type == "nzgz" && data[0] == compData[i]['GZDM'] && data[1] == compData[i]['GZLY2'] && data[3] == compData[i]["MAIN_TABLE_ID"]) {
						checkList.push(i);
					} else if (data[0] == compData[i]['RULE_ID']) {
						checkList.push(i);
					}
				}
			});
			storeDataset.saveDraft(arrInsert);
			comp.setCheckedDataRows(checkList);
		})
	} else {
		if (comp.getCheckedDataRows().length > 0) {
			return;
		}
		let storeRows = storeDataset.getRows();
		let checkList = [];
		storeRows.forEach(row => {
			let data = row.getData();
			for (let i = 0; i < compData.length; i++) {
				if (type == "nzgz" && data["RULE_ID"] == compData[i]['GZDM'] && data["RULE_FROM"] == compData[i]['GZLY2'] && data["DW_TABLE_ID"] == compData[i]["MAIN_TABLE_ID"]) {
					checkList.push(i);
				} else if (type == "cstm" && data['RULE_ID'] == compData[i]['RULE_ID']) {
					checkList.push(i);
				}
			}
		});
		comp.setCheckedDataRows(checkList);
		return;
	}
}
