
import dw from "svr-api/dw";

/**
 * 查询批量导入的标准表信息
 */
function queryImportStdTableInfo(importFileId: string): void {
    let query: QueryInfo = {
        sources: [{
            id: "model1",
            path: "/sysdata/data/tables/sdi2/query/data-standard/STD_TABLES_IMPORT_EXT.tbl", // 主要是否有提取数据（视图没有）
			// 当前加工所设置的全局参数
            params: [{
                name: "param_file_id",  // 参数名
                value: importFileId
            }]
        }],
        fields: [{
            name: "IMPORT_STD_TABLE_ID", exp: "model1.BZBDM_1"
        }],
        filter: [{
            exp: `model1.FILE_ID = '${importFileId}'`
        }],
        distinct: true
    }
    let importStdTableInfo = dw.queryData(query).data;
    print(importStdTableInfo);
    return;
}

{
	"sources": [{
		"id": "t0",
		"path": "/sdi/data/tables/sys/student.tbl"
	}],
	"fields": [{
		"name": "序号",
		"exp": "t0.XH"
	}, {
		"name": "姓名",
		"exp": "t0.XM"
	}, {
		"name": "年龄",
		"exp": "t0.NL"
	}],
	"filter": [{ // 这里的过滤表达式参数对应过滤值回根据params给定值匹配
		"exp": "t0.XM=XM and t0.XH=XH"
	}],
	"params": [{
		"name": "XM",
		"value": "帅比"
	}, {
		"name": "XH",
		"value": 2
	}]
}