import "css!./data-process.css";
import { ScheduleMgrPage } from "schedule/schedulemgr";
import { MetaModulePageArgs } from "metadata/metamgr";
import { wait } from 'sys/sys';

let ztScheulePage;

const enum SCHEDULETYPE {
    ETL = "etl",
    SUB = "sub",
    BACKUP = "backup",
    CHECK = "check"
}
export function getZTDataMgrPage() {
    return ztScheulePage;
}
/**计划任务界面 */
export default class ZTSchedulePage extends ScheduleMgrPage {
    constructor(args: MetaModulePageArgs) {
        args.layoutTheme = "dark";
        super(args);
        ztScheulePage = this;
    }

    public getTitle(): string {
        return '计划任务管理';
    }

    protected _init_default(args: MetaModulePageArgs) {
        super._init_default(args);
    }

    /**
     * 20220402 tuzw
	 * 4.20 版本出现的问题
     * 父类init()中初始化成员属性是一个异步方法，导致父类还未初始化结束，子类就调用了父类的closeScheduleInfoPage()方法，出现内部部分属性还未定义报错
     * 解决办法：等待父类加载结束
     */
    protected _init(args: MetaModulePageArgs): HTMLElement {
        let domBase = super._init(args);
        this.initPromise.then(() => { 
            this.closeScheduleInfoPage(false);
        });
        domBase.classList.add("zt-schedule");
        return domBase;
    }
	
	
	/**
     * 4.19.3 版本
     */
    // protected _init(args: MetaModulePageArgs): HTMLElement {
    //     let domBase = super._init(args);
    //     domBase.classList.add("zt-schedule");
    //     return domBase;
    // } 
} 