import db from "svr-api/db"; //数据库相关API
import meta from "svr-api/metadata"; //元数据、模型表相关API
import utils from "svr-api/utils"; //通用工具函数
import bean from "svr-api/bean"; //spring bean相关api
import { getThreadProcessMonitor, sleep } from "svr-api/sys";
import sys from "svr-api/sys";
import security from "svr-api/security";

/**
 * 将当前脚本执行的日志，展示到前端指定对话框页面
 * 案例地址：https://jira.succez.com/browse/BI-41963
 * @param taskId 执行任务ID
 */
export function showProcess(request: HttpServletRequest, response: HttpServletResponse, params: { taskId: string }) {
    let taskId: string = params.taskId;
    let currentProcess: ProgressMonitor = getThreadProcessMonitor();
    console.info(`showProcess()，测试进度条内容--start`);
    updateInitStatusTableInfos(1, taskId, "执行中");

    currentProcess.setProgress(0); // 控制页面进度条进度
    currentProcess.setMaximum(100); // 注意进度大于设置的数值，则完成不会变成绿色样式
    for (let i = 0; i < 5; i++) { // 前端获取日志，是需要发请求（getTaskInfo），避免循环太快，可能还未发请求，程序就结束，使用sleep()等待
        currentProcess.setProgress(i * 10)
        sleep(2000);
        console.info(`添加一条普通的表示详细信息的log：${i}`);
    }
    console.info(`showProcess()，测试进度条内容--end`); // 不一定要用进度条对象来记录日志
    updateInitStatusTableInfos(0, taskId, "执行结束");
	/**
	 * 休眠一会, 避免前端请求日志还未结束, 后端就执行结束将脚本销毁, 导致前端请求日志找不到
	 * 此问题: 5.0已修复
	 */
	sleep(1000);
    currentProcess.setProgress(100);
    return { result: true }
}


/** 初始化执行状态记录表 */
interface INIT_EXECUTE_STATUS_TABLES {
    /** 任务序号 */
    TASK_ID: string;
    /** 任务名称 */
    TASK_NAME: string;
    /** 当前是否正在执行 */
    IS_RUNING: number;
    /** 是否已初始化过 */
    IS_INITEN: number;
    /** 上次执行状态 */
    LAST_TASK_STATUS: string;
    /** 当前执行生成的日志序号 */
    LOG_UUID: string;
}

/**
 * 获取记录初始化脚本进行信息表信息
 * 注意：状态表信息是固定好的，仅一条信息
 * @return true | false
 */
export function getInitExecuteStatusTableInfos(request: HttpServletRequest, response: HttpServletResponse, params): INIT_EXECUTE_STATUS_TABLES {
    let ds: Datasource = db.getDataSource("default");
    let statusTableName: string = "init_execute_status_tables";
    let schema: string = ds.getDefaultSchema();
    if (!ds.isTableExists(statusTableName, schema)) {  // 若表不存在，则先创建物理表
        console.debug(`初始化执行状态记录物理表不存在，开始创建`);
        let fileds: TableFieldMetadata[] = [{
            name: "TASK_ID",
            desc: "任务序号",
            dataType: FieldDataType.C,
            length: 50
        }, {
            name: "TASK_NAME",
            desc: "任务名称",
            dataType: FieldDataType.C,
            length: 50
        }, {
            name: "IS_RUNING",
            desc: "当前是否正在执行",
            dataType: FieldDataType.I,
            length: 50
        }, {
            name: "LOG_UUID",
            desc: "当前执行的日志序号",
            dataType: FieldDataType.C,
            length: 50
        }, {
            name: "IS_INITEN",
            desc: "是否已初始化过",
            dataType: FieldDataType.I,
            length: 50
        }, {
            name: "LAST_TASK_STATUS",
            desc: "上次执行状态",
            dataType: FieldDataType.C,
            length: 50
        }, {
            name: "EXECUTE_TIME",
            desc: "开始执行时间",
            dataType: FieldDataType.P,
            length: 50
        }, {
            name: "LAST_EXECUTE_TIME",
            desc: "上次结束执行时间",
            dataType: FieldDataType.P,
            length: 50
        }, {
            name: "EXECUTE_PEOPLE",
            desc: "执行人",
            dataType: FieldDataType.C,
            length: 50
        }];
        ds.createTable({
            tableName: "init_execute_status_tables",
            primaryKeys: ["TASK_ID"],
            desc: "初始化执行状态记录表",
            fields: fileds
        });
    }
    let table: TableData = ds.openTableData("init_execute_status_tables", schema);
    let queryData = table.select("TASK_ID, IS_RUNING, IS_INITEN, LAST_TASK_STATUS, LOG_UUID", { "TASK_ID": "TTytkOwc8uKudsZbX0swhB" }, 1) as INIT_EXECUTE_STATUS_TABLES[];
    if (queryData.length == 0) {
        table.insert({
            "TASK_ID": "TTytkOwc8uKudsZbX0swhB",
            "TASK_NAME": "初始化脚本进程信息",
            "IS_RUNING": 0,
            "IS_INITEN": 0
        });
        queryData = table.select("TASK_ID, IS_RUNING, IS_INITEN, LAST_TASK_STATUS, LOG_UUID", { "TASK_ID": "TTytkOwc8uKudsZbX0swhB" }, 1) as INIT_EXECUTE_STATUS_TABLES[];
    }
    return queryData[0];
}

/**
 * 修改记录初始化脚本进程信息表状态
 * @param isRuning 是否正在执行
 * @param logUuid 当前执行的日志序号
 * @param lastTaskStatus 上次执行的状态
 * @return 
 */
function updateInitStatusTableInfos(isRuning: number, logUuid?: string, lastTaskStatus?: string): {
    result: boolean,
    message?: string
} {
    let ds: Datasource = db.getDataSource("default");
    let schema: string = ds.getDefaultSchema();
    let table: TableData = ds.openTableData("init_execute_status_tables", schema);
    let currentUserId: string = security.getCurrentUser().userInfo.userId;
    let isUpdateSuccess: number = 0;
    let nowTime: string = utils.formatDate("yyyy-MM-dd HH:mm:ss", Date.now());
    if (isRuning == 0) {
        isUpdateSuccess = table.update(
            { "IS_RUNING": isRuning, "IS_INITEN": 1, "LAST_TASK_STATUS": lastTaskStatus, "EXECUTE_TIME": nowTime, "EXECUTE_PEOPLE": currentUserId, "LOG_UUID": logUuid },
            { "TASK_ID": "TTytkOwc8uKudsZbX0swhB" }
        );
    } else {
        isUpdateSuccess = table.update(
            { "IS_RUNING": isRuning, "IS_INITEN": 1, "LAST_TASK_STATUS": lastTaskStatus, "LAST_EXECUTE_TIME": nowTime, "EXECUTE_PEOPLE": currentUserId, "LOG_UUID": logUuid },
            { "TASK_ID": "TTytkOwc8uKudsZbX0swhB" }
        );
    }
    return {
        result: isUpdateSuccess != 0 ? true : false,
        message: `修改记录初始化脚本进程信息表状态：${isUpdateSuccess != 0 ? "成功" : "失败"}`
    };
}