
import { isEmpty, showWarningMessage, downloadFile } from "sys/sys";
import { InterActionEvent, IVPage } from "metadata/metadata-script-api";
import { ListMultipageCheckedRows, getAppPath, setCanvasClass, CustomTinyMCETextFunc } from "../commons/commons.js?v=20220606712142164";
import { ExportReusltType, html2pdf, url2pdf } from "commons/export";
import { MetaFileViewer, showDrillPage, SuccRichtextRender } from "metadata/metadata";

/**
 * 笔记文件管理
 */
export class SpgNoteFileMgr {

    public CustomActions: any;

    constructor() {
        this.CustomActions = {
            button_batchImportNoteFiles: this.batchImportNoteFiles.bind(this)
        };
    }

    /**
     * 批量导出笔记文件 historyCheckedPks
     */
    private batchImportNoteFiles(event: InterActionEvent): void {
        let page = event.page;
        let listComp = page.getComponent("list1");
        if (isEmpty(listComp)) {
            showWarningMessage(`当前页面不存在list1对象, 无法获取列表勾选值`);
            return;
        }

        let listCheckedPks = listComp.historyCheckedPks;
        if (listCheckedPks.length == 0) {
            showWarningMessage(`请勾选需要导出的行`);
            return;
        }
        let pkFieldName: string = Object.keys(listCheckedPks[0])[0]; // 第一个key为主键字段
        let noteIds: string[] = [];
        for (let i = 0; i < listCheckedPks.length; i++) {
            let checkedPk = listCheckedPks[i];
            let nodeId: string = checkedPk[pkFieldName];
            noteIds.push(nodeId);
        }
        let url = getAppPath(`/notePcMgr/noteScriptMgr.action?method=batchImportNoteFile&noteIds=${noteIds.join(",")}`);
        downloadFile(encodeURI(url));
    }
	
}