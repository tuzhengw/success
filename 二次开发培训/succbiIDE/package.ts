{
    //服务器配置列表
    "serverConfigs": [
        {
            //服务器名称
			"serverName": "localhost",
 			//服务器Url
            "serverUrl": "http://localhost:8080",
            //用户名
            "user": "admin",
            //密码
            "password": "admin"
        }
    ]，
	//同步服务配置
    "syncConfig": {
		//当前连接的服务器
        "currentServer": "localhost",
		//当前开发
        "currentDevelopment": {
			//开发名称，即目录名
            "name": "DEMO",
			//开发类型，有datsource、extension、project
            "type": "project",
			//开发项目名
            "project": "DEMO"
        }
    },
	// 指定同步的目录，为空，则同步所有的
	"projects.include": [
		"/ZHJG/app/home.app/custom.ts",
		"/ZHJG/app/home.app/custom.js",
		"/XYJG/app",
		"/ZHJG/app/home.app/JsComponent"
	],
    //同步忽略文件，用于配置忽略不需要同步的文件
    //文件格式：元数据路径即可
	"projects.exclude": [
        "/DEMO/ana",
        "/DEMO/data",
        "/DEMO/fapp",
        "/sysdata/app/default.app",
        "/sysdata/settings/themes"
    ],
    //数据源配置文件，用于配置哪些数据表需要下载数据内容
    //文件格式：/{数据源名称}/{数据表名称}
	"datasources.include": [
		"/default/cn_adcode",
		"/succbi/countriesgeo",
		"/succbidw/country",
		"/succbiyw/dw_field_roles"
	]
}