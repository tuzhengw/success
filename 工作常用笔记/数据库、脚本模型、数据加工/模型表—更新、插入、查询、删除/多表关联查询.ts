/**
 * 创建批处理脚本，并且添加到对应的计划任务中
 * @param taskId 交换任务id
 * @param scheduleId 计划任务id
 */
function createBatchScript(taskId: string, scheduleId: string) {
	let queryInfo: QueryInfo = {
		fields: [{
			name: "PUSH_SCHEDULE", exp: "model2.PUSH_SCHEDULE"
		}],
		filter: [{
			exp: "model1.EX_TASK_ID=param1 and model1.RESID=model2.DX_RES_ID"
		}],
		params: [{
			name: "param1", value: taskId
		}],
		sources: [{
			id: "model1",
			path: "/sysdata/data/tables/sdi/data-exchange/ex-task/SDI_EX_APPLE_TASK.tbl"
		}, {
			id: "model2",
			path: "/sysdata/data/tables/dx/DX_RESOURCES.tbl"
		}],
		joinConditions: [{
			leftTable: "model1",
			rightTable: "model2",
			exp: "model1.RESID=model2.DX_RES_ID"
		}]
	};
	let data = dw.queryData(queryInfo).data;
}