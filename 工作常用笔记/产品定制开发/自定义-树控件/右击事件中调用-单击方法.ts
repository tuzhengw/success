/**
 * ============================================================
 * 作者:tuzhengw
 * 审核人员：liuyz
 * 创建日期：2022-07-15
 * 功能描述：
 *      该脚本主要是用于数据资产的前端功能
 * ============================================================
 */
import { Edit } from "commons/basic";
import { List, ListItem, TreeItem } from "commons/tree";
import { InterActionEvent } from "metadata/metadata";
import { Component, isEmpty, SZEvent, wait, rc, showSuccessMessage, showWarningMessage, showDialog } from "sys/sys";
import { CustomTree } from "/sysdata/extensions/succ-dataVisualization-customTree/main.js?v=20220606712124";

/**
 * 20220718 tuzw
 * @description 数据资产维护
 */
export class SpgDataAssetMgr extends Component {

    public CustomActions: any;
    constructor() {
        super();
        this.CustomActions = {};
    }

    public onRender(event: InterActionEvent): void {
        this.onWriteCustomTreeOnContextMenu(event);
    }

    /**
     * 重写树控件右击点击菜单逻辑
     * 
     * 20220720 tuzw
     * 原因：目前树不支持右击事件，无法直接在spg页面获取右击选中的节点信息
     * 解决办法：右击内部调用——单击节点方法，使其：右击==单击
     */
    private onWriteCustomTreeOnContextMenu(event: InterActionEvent): void {
        let comp = event?.component;
        if (comp?.getId() == "customTree1") {
            let customTree: CustomTree = comp.getUIComponent();
            customTree.onCustomTreeMenu = (sze: SZEvent, list: List, item: ListItem) => { // 重写树右击点击事件
                // return this.customTree.doSelection(sze, list, item, ActionTriggerType.BeforeChange);  // doSelection：点击后触发的事件
                customTree.getInnerComoponent().select1(item, true, sze); // select1：选择指定的节点，并清除其它节点的选择。
            }
        }
    }

}
