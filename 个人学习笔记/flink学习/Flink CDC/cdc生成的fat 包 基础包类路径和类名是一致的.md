---
typora-root-url: images
---

# 构建`CDC fat` 包 

​	`maven-shade-plugin` 是一个 Maven 插件，用于将项目及其所有依赖项打包成一个可执行的胖` JAR（fat JAR）`。

​	这个插件不仅可以合并多个依赖，还支持过滤、排除、修改 MANIFEST 文件等高级功能。

​	主要是==避免==：多个包，同类名同包路径一致，导致匹配到非当前引用类，从而导致报错，例如：==相同类名，但构造参数不一致等==。

---

## 1 `MySQL`和`Oracle fat`包示例

​	基于`flink-cdc-connectors`【https://github.com/apache/flink-cdc】仓库生成的胖`JAR`，其内大部分==基础的包==是一样的，也是**同类名同包路径，但其内容和实现是一致的，无论匹配谁都不影响执行**，以`MySQL`和`Oracle`为示例：

![](/mysql和oracle的fat包对比.png)

​	例如：`com.ververica.cdc.debezium.DebeziumSourceFunction`类，内容是一致的，执行`Oracle -> GaussDb`，本地调式走的是`flink-sql-connector-mysql-cdc-2.4-SNAPSHOT.jar`内的`com.ververica.cdc.debezium.DebeziumSourceFunction`类。

## 2 结论和应用

> 项目中的一些基础jar包, 例如：`flink-connector-debezium`等.
> 不同数据库生成的fat包中这些基础文件都是一样的, '包路径类名'都是一样.
> 若一个环境放置多个数据库fat包, 执行的时，例如：`MySQL -> Oracle`,  **可能会走其他数据库生成的fat包内的基础文件代码.**

​	由于基础文件都是一样的,  多个环境下，只**需要放置一个包含基础文件的`fat jar`即可**，其他`fat jar`可以将基础包在打包的时候排除掉，减少整体`jar`的大小，到时候这些`fat jar`走包含基础文件的`fat jar`即可，毕竟：==基础文件都是同包路径同类名==。

![](/排除基础文件示例配置.png)



# end 

