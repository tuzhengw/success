/**
 * 20211210 tuzw test
 * 测试‘批量同步修改方法’
 */
button_ac_dealDataConfig: (event: InterActionEvent) => {
	let page = event.page;
	let listJsComponent = page.getComponent('jsComponent1');
	/** 获取自定义控件对象 */
	let customJs_List = listJsComponent.component.getInnerComoponent().customJs_List;
	/** 进行操作的数据源：数据接入数据表配置信息（增、删、改 ： 暂存） */
	let tableConfDataset: IDataset = page.getDataset('model4');
	let tableAllRows = customJs_List.getValue();
	if (tableAllRows != null) {
		tableAllRows.forEach(row => {
			// key：对应列的id值
			let uuid: string = row.row_uuid;
			/** 增量同步字段 */
			let increment_fields: string = row.add_condition ? row.add_condition.join(",") : "";
			/** 同步策略 */
			let policy: string = row.update_strategy;
			let primaryKey_fields: string = row.table_all_column ? row.table_all_column.join(",") : "";
			/** 是否去重 */
			let isDistict = !!row.is_distict ? row.is_distict.toString() : "0";
			
			// 若缓存更新失败，检查更新的值是否正确
			tableConfDataset.modify(
				{ "UUID": `${uuid}` },
				{ 'POLICY': `${policy}`, 'INCREMENT_FIELDS': `${increment_fields}`, 'PRIMARYKEY_FIELDS': `${primaryKey_fields}`, 'IS_DISCTINCT': `${isDistict}` }
			)
		});
	}