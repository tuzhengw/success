
import metadata from "svr-api/metadata";

/**
 * 获取配置目录下所有配置表路径
 * @return 
 * 
 * 20220916 tuzw
 * 配置目录下增加了其他文件夹和模型，需要递归寻找配置目录下所有的模型，去除文件夹等
 */
function getAllConfTablePaths(): string[] {
    print(`start---获取配置目录下所有配置表路径`);
    let confFilePath: string = "/sysdata/data/tables/sdi2/sys-mgr";
    let metaFileInfos: MetaFileInfo[] = metadata.searchFiles({
        parentDir: confFilePath,
        type: "tbl",
        recur: true
    });
    if (metaFileInfos.length == 0) {
        print(`配置目录不存在`);
        return [];
    }
    let modelPaths: string[] = [];
    for (let i = 0; i < metaFileInfos.length; i++) {
        modelPaths.push(metaFileInfos[i].path);
    }
    print(modelPaths)
    print(`end---获取配置目录下所有配置表路径`);
    return modelPaths;
}