

/**
 * 元数据对象对外提供了一个接口, 可查看产品源码实现（ScriptObject_MetaFile.java)
 */
function main() {
	let customZTFile: MetaFileInfo  = metadata.getFile('/sysdata/app/customZT.app/data-standard/standard-doc/mould.html', false)
	if (customZTFile == null) {
		customZTFile = metadata.getFile('/sysdata/app/zt.app/data-standard/standard-doc/mould.html', false);
	}
	let doc = Jsoup.parse(customZTFile.getContentAsInputStream(), "UTF-8", "");
}