/**
 * =====================================================================
 * 作者：tuzw
 * 审核人员：liuyz
 * 创建日期：20220727
 * 脚本用途：无锡数据中台支持新版标签打标
 * 地址：https://jira.succez.com/browse/CSTM-19806
 * ====================================================================
 */
import { MultipleInput, MultipleInputArgs } from "commons/basic";
import { getLabels, labelData, LabelDataArgs, LabelDataResult } from "dw/dwapi";
import { Component, showWarningMessage, SZEvent, isEmpty, showSuccessMessage, rc } from "sys/sys";

/** 当前项目根路径 */
const ProjectRootPath = '/dsjpxpt/app/Home.app';
/** 
 * 定制标签
 */
export class SdiCustomLabel extends Component {

    /** 内置控件 */
    private innerComponent: MultipleInput;
    /** 将其控件值赋值到的全局变量名 */
    private needOptionGlobelParamName: string;

    /** 模型ID值 */
    private modelResidOrPath: string;
    /** 标签种类类别，eg：001：上级下发标签，002：自定义标签 */
    private labelClassType: string[];
    /** 当前模型-标签库ID */
    private libCodeId: string;
    /** 外部勾选的待打标签的主键值 */
    private rowPrimaryKeys: string[];

    /** 记录当前所选多行的硬标签信息（不重复） */
    private recordHardLabels: {};
    /** 记录首次打开对话框的标签ID数组，eg：["code_001"] */
    public initLabelIds: string[];
    /** 标签编码和标签名之间的映射关系，eg: { "code_001": { lableName:  "测试标签", lableClassType: "001" } } */
    public labelCodeAndNameRelation: {};

    /** renderer对象 */
    public renderer;
    /** 是否正在执行刷新 */
    public refreshPromise: Promise<any>;
    /** 是否首次加载 */
    private isFristInit: boolean;
    /** 是否正在执行渲染逻辑 */
    public isExecuteInit: boolean;

    constructor(args) {
        super(args);
        this.isExecuteInit = false;
    }
    /**
     * @param args.modelResidOrPath 模型ID值
     * @param args.labelClassType 标签种类类别
     * @param args.rowPrimaryKeys 外部勾选的待打标签的主键值，eg：'002' or 'type1,type2'
     * @param args.recordOpenDialogStatusParamName 记录打开对话框状态的参数名，eg：'publicLabelDialogOpenState'
     * @param args.libCodeId 标签库编码
     * 
     * @param args.recordPublicLableValue? 记录上级下发标签信息，eg：'code_001, code_002'
     * @param args.recordCustomLableValue? 记录自定义标签信息，eg：'code_003'
     * 
     * 20220805 tuzw
     * 由于对话框中包含多个JS组件，共用一个全局参数来记录当前是否打开对话框，会出现：
     *      第一个渲染完后，第二个因为第一个渲染完后将其对话框设为打开状态，导致第二个没有走查库的逻辑，无法将当前最新的标签显示
     * 解决办法：一个对话框对应一个全局参数记录对话框打开状态
     */
    protected _init(args: any): HTMLElement {
        let domBase = super._init(args);

        if (this.isExecuteInit) {
            return domBase;
        }
        this.isExecuteInit = true;
        this.isFristInit = true;
        this.renderer = args.renderer;
        this.modelResidOrPath = args.modelResidOrPath;
        this.labelClassType = this.dealStrAvailable(args.labelClassType);
        this.labelCodeAndNameRelation = {};
        this.initLabelIds = [];
        this.libCodeId = args.libCodeId;

        if (this.labelClassType.includes(LabelClassType.CustomLableType)) {
            this.needOptionGlobelParamName = GlobalParamName.RrecordCustomLableValue;
        } else if (this.labelClassType.length != 0) { // 若类型不为空，且不为002，则为上级下发标签
            this.needOptionGlobelParamName = GlobalParamName.RecordPublicLableValue;
        }
        this.parseExtendParam(args);
        this.getLabelCodeAndNameRelation().then(() => {
            this.getRowsHardLabels(this.rowPrimaryKeys).then(() => {
                let multipleValues: MultipleInputValue[] = this.getMultipleValues();
                this.createInnerComponent(multipleValues);
                this.setGloblValue();

                let recordOpenDialogStatusParamName: string = args.recordOpenDialogStatusParamName;
                this.setSignGlobalParamValue(recordOpenDialogStatusParamName, "opened"); // 设置对话框为打开状态
                this.isExecuteInit = false;
            });
        });
        return domBase;
    }

    /**
     * 重新渲染 
     * @param args.recordOpenDialogStatusParamName 记录打开对话框状态的参数名
     * @param args.openDialogStateValue 记录打开对话框状态值，eg：open | opened
     * @description 注意：
     * 1）考虑到要动态刷新多值输入控件，则利用全局参数的内容变更 刷新，若是内部获取全局参数的值则达不到动态刷新
     * 
     * @description 校验渲染的标签来源（查标签内容表 或 读取全局参数记录的标签信息）
     * 1）只有打开或者重新打开对话框加载的时候，才会查询库中的标签信息
     * 2）打开页面后，对标签进行的修改，都是暂存状态，此刻应该读取的是页面暂存变动的节点，而不是从库中查询
     * 3）校验【打开对话框参数值：open | opened】—— 若当值值为默认值为指定的【特殊值：open】，则查库，否则，获取全局变量的值进行渲染标签
     */
    public _reInit(args: any): void {
        if (this.isFristInit) { // 初次渲染执行_init()方法，也走了此方法，避免重复执行，这里增加一个参数校验是否首次执行
            this.isFristInit = false;
            return;
        }
        if (this.isExecuteInit) {
            return;
        }
        this.isExecuteInit = true;
        if (this.refreshPromise != null) {
            return;
        }
        this.setValue([]); // 先清空历史节点值
        super._reInit(args);
        this.parseExtendParam(args);

        let recordOpenDialogStatusParamName: string = args.recordOpenDialogStatusParamName;
        let openDialogStateValue: string = args.openDialogStateValue;
        if (openDialogStateValue == 'open') {
            this.initLabelIds = []; // 记录首次打开对话框时的标签信息
            this.getLabelCodeAndNameRelation().then(() => {
                this.refreshPromise = this.getRowsHardLabels(this.rowPrimaryKeys).then(() => {
                    let multipleValues: MultipleInputValue[] = this.getMultipleValues();
                    this.setValue(multipleValues);
                    this.setGloblValue();
                    this.refreshPromise = null;
                    this.setSignGlobalParamValue(recordOpenDialogStatusParamName, "opened"); // 设置对话框为打开状态
                    this.isExecuteInit = false;
                });
            });
        } else { // 根据全局记录的显示标签信息修改【多值输入控件】value（注意：此步操作仅是暂存修改，没有改变真实的标签信息）
            let multipleInputValue: string[] = [];
            if (this.labelClassType.includes(LabelClassType.CustomLableType)) {
                multipleInputValue = this.dealStrAvailable(args.recordCustomLableValue);
            } else if (this.labelClassType.length != 0) { // 若类型不为空，且不为002，则为上级下发标签
                multipleInputValue = this.dealStrAvailable(args.recordPublicLableValue);
            }
            this.reSetMultipleInputValue(multipleInputValue);
        }
    }

    /**
     * 根据全局参数设置【多值输入框对象】的value值
     * @param multipleInputValue 当前页面所选最新标签节点信息
     */
    private reSetMultipleInputValue(multipleInputValue: string[]): void {
        /** 记录新增的标签（若未在标签编码和名称映射关系中找到此标签，则视为新增标签） */
        let addLabelCodes: string[] = [];
        let multipleValues: MultipleInputValue[] = [];
        for (let i = 0; i < multipleInputValue.length; i++) {
            let labelCode: string = multipleInputValue[i];
            let labelInfo = this.labelCodeAndNameRelation[labelCode];
            if (isEmpty(labelInfo)) {
                addLabelCodes.push(labelCode);
                continue;
            }
            let name: string = labelInfo.labelName;
            let multipleValue: MultipleInputValue = {
                value: labelCode,
                caption: name
            }
            multipleValues.push(multipleValue);
        }
        if (addLabelCodes.length != 0) {
            this.getLabelCodeAndNameRelation().then(() => { // 更新映射关系缓存数据
                /** 记录已被删除的节点（考虑到页面会被多人使用，显示的节点不一定是真实存在的） */
                let deleteLabelCodes: string[] = [];
                for (let i = 0; i < addLabelCodes.length; i++) {
                    let labelCode: string = addLabelCodes[i];
                    let labelInfo = this.labelCodeAndNameRelation[labelCode];
                    if (isEmpty(labelInfo)) {
                        deleteLabelCodes.push(labelCode);
                        continue;
                    }
                    let multipleValue: MultipleInputValue = {
                        value: labelCode,
                        caption: labelInfo.labelName
                    }
                    multipleValues.push(multipleValue);
                }
                this.setValue(multipleValues);
                if (deleteLabelCodes.length > 0) {
                    showWarningMessage(`页面标签已被删除，请刷新页面数据信息`);
                }
                this.isExecuteInit = false;
            });
        } else {
            this.setValue(multipleValues);
            this.isExecuteInit = false;
        }
    }

    /**
     * 获取初始【多值输入控件】value值
     */
    private getMultipleValues(): MultipleInputValue[] {
        let initMultipleValues: MultipleInputValue[] = [];
        let hardLabelCodes: string[] = Object.keys(this.recordHardLabels);
        for (let i = 0; i < hardLabelCodes.length; i++) {
            let lableInfo = this.recordHardLabels[hardLabelCodes[i]];
            if (lableInfo.labelShowCount == this.rowPrimaryKeys.length) { // 判断当前标签是否为当前勾选主键行共同的标签
                this.initLabelIds.push(hardLabelCodes[i]); // 记录符合的标签ID
                initMultipleValues.push({
                    value: hardLabelCodes[i],
                    caption: lableInfo.labelName
                });
            }
        }
        if (initMultipleValues.length == 0) {
            // showWarningMessage("当前所选行没有绑定标签信息");
        }
        return initMultipleValues;
    }

    /**
     * 处理每次打开都需要初始化的参数值
     */
    private parseExtendParam(args: {
        rowPrimaryKeys: string
    }): void {
        this.refreshPromise = null;
        this.recordHardLabels = {};
        this.rowPrimaryKeys = this.dealStrAvailable(args.rowPrimaryKeys);
    }

    /**
     * 创建内置对象—多值输入框
     * @param initMultipleValues 多值输入框值
     * @return 
     */
    protected createInnerComponent(initMultipleValues: MultipleInputValue[]): void {
        let multipleInputArgs: MultipleInputArgs = {
            domParent: this.domBase,
            value: initMultipleValues,
            arbitraryInputEnabled: true,
            inputVisible: true,
            arrowVisible: false,
            dropPanelVisible: false,
            borderVisible: false,
            onchange: this.closeMultipleInputNode.bind(this)
        }
        let multipleInput = new MultipleInput(multipleInputArgs);
		this.innerComponent =  multipleInput;
		
        this.setMultipleInputStyle(multipleInput);
		this.setMultipleInputItemStyle();
    }

    /**
     * 多值输入框对象内部节点-关闭事件
     * 关闭节点不执行任何操作，对标签的修改，单独外部调用
     */
    private closeMultipleInputNode(e: SZEvent, component: Component): void {
        let newValue: string[] = e.value;
        this.setValue(newValue); // 删除某个节点后，更新value值
        this.setSignGlobalParamValue(this.needOptionGlobelParamName, newValue.join(","));
    }

    /**
     * 获取多行数据共同的硬标签信息
     * @param pkRows 行主键集
     * @return 
     */
    private getRowsHardLabels(pkRows: string[]): Promise<void> {
        this.recordHardLabels = {};
        let labelPromises: Promise<any>[] = [];
        if (pkRows.length == 0) {
            showWarningMessage(`未获取到勾选的主键值`);
        }
        for (let i = 0; i < pkRows.length; i++) {
            labelPromises.push(this.getRowLable(this.modelResidOrPath, pkRows[i]));
        }
        return Promise.all(labelPromises).then((allLabels: DwDataLabel[][]) => {
            for (let i = 0; i < allLabels.length; i++) {
                let labels: DwDataLabel[] = allLabels[i];
                for (let j = 0; j < labels.length; j++) {
                    let labelCode: string = labels[j].code as string;
                    if (isEmpty(this.recordHardLabels[labelCode])) {
                        this.recordHardLabels[labelCode] = {
                            labelName: labels[j].name,
                            labelShowCount: 1
                        };
                    } else {
                        this.recordHardLabels[labelCode].labelShowCount++; // 记录当前标签出现的次数，用于校验标签是否属于当前勾选行共同的标签
                    }
                }
            }
        });
    }

    /**
    * 获取单行硬标签信息
    * 标签表：/sysdata/data/tables/dw/DW_DATA_LABELS_LIB.tbl
    * @param modelResidOrPath 模型Resid 或 路径
    * @param rowKey? 行主键值
    * return 
    */
    public getRowLable(modelResidOrPath: string, rowKey?: string): Promise<any> {
        if (isEmpty(modelResidOrPath)) {
            showWarningMessage("未指定需要查询的标签模型ID或者路径");
            return Promise.resolve(false);
        }
        let labelParam = {
            path: modelResidOrPath
        }
        if (!isEmpty(rowKey)) {
            labelParam["key"] = rowKey;
        }
        /** 硬标签数据 */
        let hardLabelDatas: DwDataLabel[] = [];
        return getLabels(labelParam).then((data: DwDataLabel[]) => {
            if (data.length == 0) {
                return [];
            }
            for (let i = 0; i < data.length; i++) {
                let labelCode: string = data[i].code;
                let relation = this.labelCodeAndNameRelation[labelCode];
                if (isEmpty(relation)) {
                    continue;
                }
                if (data[i].labelType == 2 && this.labelClassType.includes(relation.labelClassType)) { // 校验是否硬标签 并且 标签种类类别为指定类型
                    hardLabelDatas.push(data[i]);
                }
            }
            return hardLabelDatas;
        });
    }

    /**
     * 设置多选输入框对象值，v不存在时清空输入框
     * @param v
     */
    public setValue(v: Array<string | JSONObject>): Promise<void> {
        return this.innerComponent.setValue(v);
    }

    /**
     * 调整外部勾选的企业标签
     */
    public adjustCompanyLabel(): Promise<void> {
        if (isEmpty(this.libCodeId)) {
            showWarningMessage(`标签库编码为空，无法调整企业标签`);
            return;
        }
        let { newAddLableIds, deleteLableIds } = this.getAddAndDeleteLabelIds();
        let labelDataArgs: LabelDataArgs;
        let labelPromises: Promise<any>[] = [];
        for (let i = 0; i < newAddLableIds.length; i++) { // 新增标签
            labelDataArgs = {
                labelCode: newAddLableIds[i],
                labelLibCode: this.libCodeId,
                labelKeys: this.rowPrimaryKeys,
                overwriteMode: "append"
            }
            let promise: Promise<LabelDataResult> = labelData(labelDataArgs);
            labelPromises.push(promise);
        }
        for (let i = 0; i < deleteLableIds.length; i++) { // 删除标签
            labelDataArgs = {
                labelCode: deleteLableIds[i],
                labelLibCode: this.libCodeId,
                delabelKeys: this.rowPrimaryKeys,
                overwriteMode: "append"
            }
            let promise: Promise<LabelDataResult> = labelData(labelDataArgs);
            labelPromises.push(promise);
        }
        return Promise.all(labelPromises).then((values) => {
            showSuccessMessage("调整完成");
            this.initLabelIds = this.innerComponent.getValue(); // 更新完成后，重新赋值记录的ID集
        });
    }

    /**
     * 对比-初始记录标签编码数组，根据勾选差异，获取新增、删除的标签编码集
     * 注意：根据差异更新标签信息后，根据当前勾选状态，重新赋值-初始记录的编码集
     * @return 
     */
    public getAddAndDeleteLabelIds(): { newAddLableIds: string[], deleteLableIds: string[] } {
        let currentCheckedNodeIds: string[] = this.innerComponent.getValue();
        let newAddLableIds: string[] = [];
        let deleteLableIds: string[] = [];
        for (let i = 0; i < this.initLabelIds.length; i++) {
            if (!currentCheckedNodeIds.includes(this.initLabelIds[i])) { // 若节点不存在最新的勾选ID中，则视为删除
                deleteLableIds.push(this.initLabelIds[i]);
            }
        }
        for (let i = 0; i < currentCheckedNodeIds.length; i++) {
            if (!this.initLabelIds.includes(currentCheckedNodeIds[i])) { // 若节点不存在之前记录的勾选ID集中，则视为新增
                newAddLableIds.push(currentCheckedNodeIds[i]);
            }
        }
        return { newAddLableIds: newAddLableIds, deleteLableIds: deleteLableIds };
    }

    /**
     * 初次获取当前模型对应的所有硬标签：标签编码和名称之间的映射关系
     */
    private getLabelCodeAndNameRelation(): Promise<void> {
        if (isEmpty(this.modelResidOrPath)) {
            showWarningMessage(`未指定模型ID`);
            return;
        }
        return rc({
            url: `${ProjectRootPath}/script/jsCustomLable/labelScript.action?method=getLabelCodeAndNameRelation`,
            method: 'POST',
            data: {
                modelResidOrPath: this.modelResidOrPath
            }
        }).then((result: { LABEL_CODE: string, LABEL_NAME: string, LABEL_CLASS_TYPE: string }[]) => {
            this.labelCodeAndNameRelation = {};
            for (let i = 0; i < result.length; i++) {
                this.labelCodeAndNameRelation[result[i].LABEL_CODE] = {
                    labelName: result[i].LABEL_NAME,
                    labelClassType: result[i].LABEL_CLASS_TYPE
                }
            }
        });
    }

    /**
     * 设置内置对象-多值输入框样式
     * @param multipleInput 多值输入框对象
     */
    private setMultipleInputStyle(multipleInput: MultipleInput): void {
        multipleInput.domBase.style.minWidth = '400px';
        multipleInput.domBase.style.width = '100%';
        multipleInput.domBase.style.minHeight = '60px';
        multipleInput.domBase.style.color = 'red';
    }
	
	 /**
     * 给每个多值输入控件选项随机添加背景颜色
     */
    private setMultipleInputItemStyle(): void {
        let colorValues: string[] = ["#A8EDB6", "#FAC871", "#FA9185", "#C3C1F4", "#FFE66F", "#DFE0E4", "#06C8E9"];
        let multipleInput: MultipleInput = this.innerComponent;
        let items = multipleInput.getItems();
        items.forEach(((item: HTMLElement) => {
            let randomColorIndex: number = Math.floor(Math.random() * colorValues.length);
            item.style.backgroundColor = colorValues[randomColorIndex];
        }));
    }

    /**
     * 将【多值输入框对象的值】赋值到全局变量上
     */
    private setGloblValue(): void {
        let multipleValues: string = this.innerComponent.getValue().join(",");

        if (this.labelClassType.includes(LabelClassType.CustomLableType)) {
            this.setSignGlobalParamValue(GlobalParamName.RrecordCustomLableValue, multipleValues);
        } else if (this.labelClassType.length != 0) {
            this.setSignGlobalParamValue(GlobalParamName.RecordPublicLableValue, multipleValues);
        }
    }

    /**
     * 设置当前SPG指定全局参数值
     * @param signParams 指定的参数名
     * @param globleValue 全局参数值
     */
    private setSignGlobalParamValue(signParams: string, globleValue: string): void {
        this.renderer.getData().setParameter(signParams, globleValue);
    }

    /**
     * 处理可能为字符串或者字符串数组的变量，返回字符串数组
     * @param strAvailable 处理的变量
     * @return 
     */
    private dealStrAvailable(strAvailable: string | string[]): string[] {
        if (isEmpty(strAvailable)) {
            return [];
        }
        if (typeof strAvailable != 'string') {
            return strAvailable;
        }
        return strAvailable.split(",");
    }
}

/** 多值控件value属性 */
interface MultipleInputValue {
    /** ID值 */
    value: string;
    /** 标题 */
    caption: string;
}

/** 全局参数常量 */
enum GlobalParamName {
    /** 记录【上级下发标签】打开对话框状态，open | opened */
    PublicLabelDialogOpenState = 'publicLabelDialogOpenState',
    /** 记录【自定义标签】打开对话框状态，open | opened */
    CustomDialogOpenState = 'customDialogOpenState',
    /** 记录上级下发标签信息变量名 */
    RecordPublicLableValue = 'recordPublicLableValue',
    /** 记录自定义标签信息变量名 */
    RrecordCustomLableValue = 'recordCustomLableValue'
}

/** 标签种类类型 */
enum LabelClassType {
    /** 上级下发标签 */
    PublicLableType = '001',
    /** 自定义标签 */
    CustomLableType = '002'
}