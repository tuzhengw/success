import { IMetaFileCustomJS, InterActionEvent } from "metadata/metadata";
import { downloadFile, enhanceHtml2canvas, ajaxUploadFile, ctx, showErrorMessage, browser } from "sys/sys";
import { exportReportPDF } from "ana/rpt/report";
import { url2pdf, ExportReusltType } from "commons/export";

export const CustomJS: { [type: string]: IMetaFileCustomJS } = {
	spg: {
		CustomActions: {
			/**
			 * 20211102 liuyz
			 * 将html转为图片上传到服务器上，然后再下载
			 * https://jira.succez.com/browse/CSTM-16830
			 */
			exportDomToImg: (event: InterActionEvent) => {
				let page = event.page;
				let params = event.params;
				let compId = params.compId;
				let panelComp = page.getComponent(compId);
				let domBase = panelComp.component.getDomBase();
				enhanceHtml2canvas(domBase, null).then(canvas=>{
					canvas.toBlob(function(blob){
						blob.name="商户二维码.png";
						let file = new File([blob], "商户二维码.png",{type:"image/png"})
						ajaxUploadFile({
							url:null,
							file: file,
							fileName:"商户二维码.png",
							genMd5:false,
							maxSize:-1
						}).then(result=>{
							downloadFile("/api/fileupload/services/download?fileId=" + result.fileId,'商户二维码.png');
						});
					},'image/png');
				});
			},
			/**
			 * 20211112 liuyz
			 * 将指定的控件或者dom导出为pdf
			 * https://jira.succez.com/browse/CSTM-16987
			 */
			exportReportToPdf:(event: InterActionEvent)=>{
				let page = event.page;
				let fileInfo = page.getFileInfo();
				let params = event.params;
				let jypzh = params.jypzh;
				url2pdf(fileInfo.id, ctx("/SPZS/ana/DZPZ.rpt?JYPZH=" + jypzh ), "电子凭证", ExportReusltType.DOWNLOAD);
			},
			/**
			 * 20211130 liujingj 
			 * 下载承诺书 
			 * https://jira.succez.com/browse/CSTM-17139
			 */
			downloadFile_cns: (event: InterActionEvent) => {
				let cnsid = event.params.cnsid;
				if (!cnsid) {
					showErrorMessage(`没有承诺书id参数，无法下载`);
					return;
				}
				let url = `/public/task.action?method=downloadFile_CNS&cnsid=${encodeURIComponent(cnsid)}`;
				downloadFile(url, "承诺书.pdf");
			}
		}
	}
}