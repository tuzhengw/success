import { CustomJs_List } from '../../commons/commons.js';
import { rc, Component, isEmpty, MacScrollBar, showErrorMessage, showWarningMessage } from 'sys/sys';


/**
 * 20211209 tuzw
 * 帖子：https://jira.succez.com/browse/CSTM-16915
 * 定制列表控件--用于批量同步更新策略
 * 问题腾讯文档：https://docs.qq.com/doc/DZk5FV2l1dHJyZUNj?friendUin=ltKCU%252Ft88e%252FAoirDbSj3Og%253D%253D
 * 注意规范：https://wiki.succez.com/pages/viewpage.action?pageId=63832069
 */
export class CustomJs_Update_Strategy_List extends Component {
	/** 列表对象 */
	private customJs_List: CustomJs_List;
	/** 页面的Renderer对象 */
	private renderer;
	/** _reInit方法是否正在执行，避免重复加载 */
	private isTableRefresh: boolean = false;

	public _reInit(args): void {
		let currentChoseDataRows = this.renderer.getData().getComponent('list2').getCheckedDataRows();
		let currentChoseDataBase: string = args.dataBase;
		let currentChoseSchema: string = args.schema;
		let table = this.customJs_List.table;
		if (table != null) {
			let len = this.customJs_List.rows.length;
			this.customJs_List.rows = [];
			// 先删除之前的所有数据行（除标题行）
			for (let i = 1; i < len + 1; i++) {
				table.tBodies[0].deleteRow(-1);
			}
			if (!this.isTableRefresh) {
				this.addRowData(currentChoseDataRows, currentChoseDataBase, currentChoseSchema, this.customJs_List.headerParams);
			}
		}
	}

	protected _init(args): HTMLElement {
		let domBase = super._init(args);
		this.renderer = args.renderer;
		let currentChoseDataRows = this.renderer.getData().getComponent('list2').getCheckedDataRows();
		let currentChoseDataBase: string = args.dataBase;
		let currentChoseSchema: string = args.schema;
		if (!currentChoseDataBase) {
			showWarningMessage(`未传递参数【数据源名】，请检查参数！！`);
			return domBase;
		}
		let listHead: CustomJs_List_Head[] = [
			{ id: "row_uuid", caption: '配置库主键', visible: false },
			{ id: "table_name", caption: '表名' },
			{
				id: "update_strategy",
				caption: '更新策略',
				type: `combobox`,
				comboxArgs: [{
					value: "TOTAL", caption: "全量同步"
				}, {
					value: "INCREMENT_PRIMARYKEY", caption: "增量同步"
				}]
			},
			{ id: "add_condition", caption: '增量依据', type: `combobox`, isMultipleSelect: true },
			{ id: "table_all_column", caption: '主键', type: `combobox`, isMultipleSelect: true },
			{ id: "is_distict", caption: '是否去重', type: `checkbox` }
		]
		this.addRowData(currentChoseDataRows, currentChoseDataBase, currentChoseSchema, listHead, domBase);
		return domBase;
	}

	/**
	 * 新增列表数据
	 * 实现：调用后端方法getPrimaryKeysAndTimesColumns获取所选表的主键集和时间字段
	 * @param domBase：列表对象的父DOM
	 * @param rowsData：构成列表行数据集
	 * @param dataBase：数据源名
	 * @param schema：schema名
	 * @param listHead：列标题对象
	 */
	public addRowData(rowsData, dataBase: string, schema: string, listHead: CustomJs_List_Head[], domBase?: HTMLElement): Promise<any> {
		this.isTableRefresh = true;
		/** 构造的行数据是否有目标表 */
		let isNochoseTargetTable = false;
		let currentNewChoseData: Array<DB_CONFIG_INFO> = [];
		for (let i = 0; i < rowsData.length; i++) {
			if (!rowsData[i].TARGET_TABLE) {
				isNochoseTargetTable = true;
				continue;
			}
			let temp: DB_CONFIG_INFO = {
				UUID: rowsData[i].UUID,
				TABLE_NAME: rowsData[i].TARGET_TABLE,
				UPDATE_STRATEGY: rowsData[i].POLICY,
				ADD_CONDITION: rowsData[i].INCREMENT_FIELDS ? rowsData[i].INCREMENT_FIELDS.split(",") : [],
				PRIMARY_KEY: rowsData[i].PRIMARYKEY_FIELDS ? rowsData[i].PRIMARYKEY_FIELDS : [],
				TABLE_ALL_COLUMN: [],
				ALL_TIME_FIELDS: [],
				PRIMARY_KEY_DEL_REPEAT: !!rowsData[i].IS_DISCTINCT ? parseInt(rowsData[i].IS_DISCTINCT) : 0
			}
			currentNewChoseData.push(temp);
		}
		if (isNochoseTargetTable) {
			showWarningMessage("构造的部分行数据没有选择目标表");
		}
		return rc({
			url: "/zt/dataAccess/getPrimaryKeysAndTimesColumns",
			method: "POST",
			data: {
				dataBase: dataBase,
				schema: schema,
				choseData: currentNewChoseData
			}
		}).then((result: Array<DB_CONFIG_INFO>) => {
			if (this.customJs_List == null) {
				this.customJs_List = new CustomJs_List({
					domBase: domBase,
					headerParams: listHead,
					isNeedInitOneRow: false,
					isNeedAddDelFunction: false
				});
			}
			result.forEach(rowItem => {
				let row = {
					"row_uuid": rowItem.UUID,
					"table_name": rowItem.TABLE_NAME,
					"update_strategy": rowItem.UPDATE_STRATEGY,
					"add_condition": rowItem.ADD_CONDITION,
					"table_all_column": rowItem.PRIMARY_KEY, // rowItem.PRIMARY_KEY：为当前【暂存表】中已选（已有）数据
					"is_distict": rowItem.PRIMARY_KEY_DEL_REPEAT
				};
				let row_listHead = this.adjustComboboxItem(listHead, [3, 4], [rowItem.ALL_TIME_FIELDS, rowItem.TABLE_ALL_COLUMN]);
				this.customJs_List.doAddRow(row, row_listHead);
			});
			this.isTableRefresh = false;
			/**
			 * 20211214 tuzw
			 * 滚动条由于内部列表还未渲染，导致判定内部没有超过指定高度，就没有加载滚动条
			 * 当子类的内容变化导致滚动条高度变高时，需要子类在适当的时候调用【此函数】，以调整滚动条的显示
			 * updateScrollBar(isScroll = false)
			 * 注意：当isScroll = true的时候，所有的属性会从滚动条【MacScrollBar】上获取，MacScrollBar对象init时高度就固定了
			 * 
			 * panel43  js控件所在面板
			 */
			let scrollBar: MacScrollBar = this.renderer.getData().getComponent("panel43").component.scrollBar;
			scrollBar.updateScrollBar();
		}).catch(error => {
			this.isTableRefresh = false;
			throw error;
		});
	}

	/**
	* 20211215 tuzw
	* 配置每行指定列的下拉框的选项数据
	* 需求：由于每行【目标表】的字段不同，需要根据当前行【所选表】来构造【下拉框】选项数据
	* 实现：对【CustomJs_List_Head】对象的comboxArgs属性重构
	* @param 列标题对象数组[CustomJs_List_Head]
	* @param columnIds 配置列数组
	* @param choseValue 跟配置列数组对应的下拉框选项值
	* @return 修改后的列标题对象数组[CustomJs_List_Head]
	*/
	public adjustComboboxItem(custromJs_list: CustomJs_List_Head[], columnIds: number[], choseValue: string[][]): CustomJs_List_Head[] {
		if (columnIds == null || custromJs_list == null || choseValue == null) {
			return custromJs_list;
		}
		if (columnIds.length != choseValue.length) {
			showWarningMessage(`--adjustDownFrameChose()，列与设置下拉框数量不一致--`);
			return custromJs_list;
		}
		for (let i = 0; i < columnIds.length; i++) {
			// 将其下拉框选项重置
			custromJs_list[columnIds[i]].comboxArgs = [];
			for (let j = 0; choseValue[i] != null && j < choseValue[i].length; j++) {
				let chose = {
					value: choseValue[i][j],
					caption: choseValue[i][j]
				}
				custromJs_list[columnIds[i]].comboxArgs.push(chose);
			}
		}
		return custromJs_list;
	}
}


/**
 * 列表列标题对象
 * 注意：可引入通用模块已经定义的
 */
export interface CustomJs_List_Head {
    /** 列DOM的id */
    id: string;
    /** 列标题名称，如果不填写默认为id */
    caption?: string;
    // （注意：取变量名注意不要跟原有属性冲突，eg：isDisplay）
    /** 是否隐藏列 */
    noDisplay?: boolean;
    /** 控件类型，如果不填写则默认为输入框控件，目前支持：combobox（多选框），resselect，checkbox：单选框 */
    type?: string;
    /** 下拉框是否支持多选 */
    isMultipleSelect?: boolean,
    /** 下拉框选项（数组） */
    comboxArgs?: {
        value: string, caption: string
    }[],
    resArgs?: { id: string, caption?: string, rootPath?: string, returnTypes?: string[], enableEmptySelect?: boolean }
}

/**
 * 数据库配置信息
 */
interface DB_CONFIG_INFO {
    /** 【数据接入数据表配置信息表】主键值 */
    UUID?: string;
    /** 表名 */
    TABLE_NAME?: string;
    /** 更新策略 */
    UPDATE_STRATEGY?: string;
    /** 增量依据时间字段 */
    ADD_CONDITION?: Array<string>,
    /** 当前表所有时间字段 */
    ALL_TIME_FIELDS?: Array<string>;
    /** 当前表的主键字段 */
    PRIMARY_KEY?: Array<string>;
    /** 当前表所有字段 */
    TABLE_ALL_COLUMN?: Array<string>;
    /** 是否根据主键去重 */
    PRIMARY_KEY_DEL_REPEAT?: number;
}

