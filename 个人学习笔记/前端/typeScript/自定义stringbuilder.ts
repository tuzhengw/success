
/**
 * 20210914
 * 网址：https://codereview.stackexchange.com/questions/230865/stringbuilder-to-build-or-not-to-build
 */
export class Stringbuilder {
    strArray: Array<string> = new Array<string>();

    constructor() {

    }

    get(index: number): string {
        var str: string = "";
        if ((this.strArray.length > index) && index > 0) {
            str = this.strArray[index];
        }
        return str;
    }

    /**
     * 添加
     * @param {string} element
     */
    append(element: string): void{
        if (element) {
            this.strArray.push(element);
        }
    }
    isEmpty(): boolean {
        if (this.strArray.length == 0) {
            return false;
        }
        return true;
    }

    toString(): string {
        return this.strArray.join("");
    }

    /**
     * @param {string} delimeter 给定划分的字符串
     * @returns {string}
     */
    toArrayString(delimeter: string): string {
        return this.strArray.join(delimeter);
    }

    clear() {
        this.strArray.length = 0;
    }
}

function print(...args: any[]){
    console.log(...args)
}


function main(){
    let stringbuilder = new Stringbuilder();
    stringbuilder.append("1");
    stringbuilder.append("2");
    print(stringbuilder.toString());
}

main();
