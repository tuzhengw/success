import {
	SZEvent, Component, waitRender, wait, ComponentArgs, showMenu, uuid, isEmpty, rc, formatDate, parseDate, showConfirmDialog,
	showErrorMessage, showProgressDialog, showSuccessMessage, showWaiting, showWarningMessage, MacScrollBar
} from 'sys/sys';
import { getAppPath, CustomJs_List, CustomJs_List_Head } from '../../commons/commons.js';

/**
 * 20220124 tuzw
 * 需求：数据中台API接口添加界面UI美化
 * 帖子：https://jira.succez.com/browse/BI-43101
 */
export class CustomJs_API_List extends Component {
	/** 列表对象 */
	private customJs_List: CustomJs_List;
	/** 页面的Renderer对象 */
	public renderer;
	/** 列表所在面板的滚动条对象 */
	listPanelScrollBar?: MacScrollBar;

	public _reInit(args: any): void {
		let domParent = args.domParent;
	}
	/**
	 * 初始化列表信息
	 * @args：当前JS组件的父控件
	 */
	protected _init(args): HTMLElement {
		let domBase = super._init(args);
		this.renderer = args.renderer;
		this.listPanelScrollBar = this.renderer.getData().getComponent("panel13").component.scrollBar;

		let currentSetData: ApiParams[] = this.pareseData(args.currentSetData);

		/** 列标题 */
		let listHead: CustomJs_List_Head[] = [
			{ id: "row_uuid", caption: '序号', visible: false },
			{ id: "row_field", caption: '字段' },
			{ id: "row_params", caption: '参数名' },
			{ id: "row_params_desc", caption: '参数描述' },
			{
				id: "row_params_type",
				caption: '参数类型',
				type: `combobox`,
				comboxArgs: [{
					value: "string", caption: "字符型"
				}, {
					value: "int", caption: "整型"
				}, {
					value: "float", caption: "浮点型"
				}, {
					value: "long", caption: "长整型"
				}, {
					value: "boolean", caption: "布尔型"
				}]
			},
			{ id: "row_params_len", caption: '参数长度' },
			{
				id: "row_param_is_null",
				caption: '是否允许为空',
				type: `combobox`,
				comboxArgs: [{
					value: "true", caption: "是"
				}, {
					value: "false", caption: "否"
				}]
			},
			{
				id: "row_is_model_param",
				caption: '是否模型参数',
				type: `combobox`,
				comboxArgs: [{
					value: "true", caption: "是"
				}, {
					value: "false", caption: "否"
				}]
			},
		]
		return this.addRowData(currentSetData, listHead, domBase);
	}

	/**
	* 新增列表数据
	* 实现：调用后端方法getPrimaryKeysAndTimesColumns获取所选表的主键集和时间字段
	* @param currentSetData 构成列表行数据集
	* @param listHead 列标题对象
	* @param domBase 列表对象的父DOM
	*/
	public addRowData(currentSetData: ApiParams[], listHead: CustomJs_List_Head[], domBase: HTMLElement): HTMLElement {
		if (this.customJs_List == null) { // 初始列表不构造【行数据】
			let initListParams = {
				domBase: domBase,
				headerParams: listHead,
				customJsObject: this,
				isNeedAddDelFunction: true,
				isNeedInitOneRow: currentSetData.length == 0 ? true : false,
				outsideListPanelScrollBar: this.listPanelScrollBar
			}
			this.customJs_List = new CustomJs_List(initListParams);
		}
		let apiParamId: number = this.customJs_List.rows.length + 1;
		if (currentSetData.length != 0) {
			currentSetData.forEach(item => {
				let row = {
					"row_uuid": apiParamId++,
					"row_field": item.name,
					"row_params": item.caption,
					"row_params_desc": item.desc,
					"row_params_type": item.type,
					"row_params_len": item.len,
					"row_param_is_null": item.nullable,
					"row_is_model_param": item.isModelParam
				}
				this.customJs_List.doAddRow(row);
			});

		}
		return domBase;
	}

	/**
	 * 解析构造的数据（JSON）
	 * @param jsonData 构造列表行的数据
	 * @return
	 */
	public pareseData(jsonData): ApiParams[] {
		if (!jsonData) {
			return [];
		}
		try {
			if (typeof jsonData == "string") {
				return JSON.parse(jsonData);
			}
			return jsonData;
		} catch (e) {
			console.error(e);
			showWarningMessage("构造API参数列表失败，构造数据格式必须是JSON");
			console.error(`构造API参数列表失败，构造数据格式必须是JSON`);
		}
		return [];
	}

	/**
	 * 获取列表的值
	 * @return eg：[{"name":"ID","caption":"ID","desc":"测试ID","type":"float","len":"","nullable":false}]
	 */
	public getValue(): ApiParams[] {
		let listAllRows = this.customJs_List.rows;
		if (isEmpty(listAllRows)) {
			return [];
		}
		let returnApiParams: ApiParams[] = [];
		listAllRows.forEach(item => {
			let row = item.getValue();
			let temp: ApiParams = {
				"name": this.checkParam(row.row_field),
				"caption": this.checkParam(row.row_params),
				"desc": this.checkParam(row.row_params_desc),
				"type": this.checkParam(row.row_params_type),
				"len": this.checkParam(row.row_params_len),
				"nullable": this.checkParam(row.row_param_is_null),
				"isModelParam": this.checkParam(row.row_is_model_param)
			}
			returnApiParams.push(temp);
		});
		return returnApiParams;
	}

	/**
	 * 校验当前是否有设置此参数，若无，默认：""
	 * @param param 校验的参数值
	 */
	public checkParam(param) {
		if (!param) {
			return "";
		}
		return param;
	}
}