
import metadata from 'svr-api/metadata';

/**
 * 创建一个数据加工元数据文件
 *
 * 2022-02-24 liuyz
 * https://jira.succez.com/browse/CSTM-17939
 * 更改生成模型描述，贴近业务
 */
private createDataFlowMetaFile() {
	let tableInfo = this.tableInfo;
	let resid = tableInfo.targetResid;
	let fileExist = metadata.getFile(resid);
	let table = ds.openTableData('SDI_DATA_ACCESS_TABLES_CONF');
	console.debug(`创建一个数据加工元数据文件，resid：${resid}`);
	if (resid == null || fileExist == null) {
		let parentPath = this.parentPath;
		let tableName = this.getTableName(tableInfo.targetTable, parentPath);
		/**
		 * 20220228 tuzw
		 * 问题：自动生成的归集配置（数据加工）所在父文件夹没有描述（源库的描述信息）
		 * 地址：https://jira.succez.com/browse/CSTM-17919
		 */
		let parentFileName: string = "元数据文件夹名";
		let parentFileDesc: string = "元数据文件夹描述";
		metadata.mkdirs({   // 创建元数据文件夹
			name: parentFileName,
			path: parentPath,
			type: "fold",
			desc: `${parentFileDesc}`
		});
		let path = parentPath + "/" + tableName + ".tbl";
		let desc = `${this.getSourceTableDesc()} `;
		console.debug(`当前数据加工的模型名为：【${tableName} 】，描述：【${desc} 】`);
		try {
			let file = metadata.createFile({ // 创建元数据文件
				name: tableName,
				path: path,
				type: "tbl",
				desc: desc,
				content: JSON.stringify(this.dataFlowContent)
			});
			resid = file.id;
			this.setTargetResid(resid);
			table.update({ "TARGET_TABLE_RESID": resid }, { "UUID": tableInfo.tableId });
		} catch (e) {
			console.error("创建数据加工异常");
			console.error(e);
			return;
		}
	} else {
		metadata.modifyFile(resid, { content: JSON.stringify(this.dataFlowContent) });
	}
	refreshState(resid);
	table.update({ "STATE": '0' }, { "UUID": tableInfo.tableId });
	this.insertTableToSchedule();
}