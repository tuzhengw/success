

import Files_ = java.nio.file.Files;
import Paths_ = java.nio.file.Paths;
import JavaFile = java.io.File;
import ZipOutputStream = java.util.zip.ZipOutputStream;
import ZipEntry = java.util.zip.ZipEntry;
import FileOutputStream = java.io.FileOutputStream;
import FileInputStream = java.io.FileInputStream;
import IOUtils = org.apache.commons.io.IOUtils;
import { SDILog, LogType, isEmpty, ResultInfo, ZipUtils, convertStrToArr, getXMLSavePath } from "/sysdata/app/zt.app/commons/commons.action";

/**
 * 2022-12-30 tuzw
 * 压缩指定的XML文档附件文件到压缩包
 * 地址：https://jira.succez.com/browse/CSTM-21694
 * 
 * @param params xmlAttachInfos 压缩包的附件信息, eg: ["workdir:2022122717480669101_1.病历概要.xml"]
 * @return eg: {
 *   result: true,
 *   zipPath: "/opt/workdir/clusters-share/tempSaveImportXmlZipFord/XML文档_20221230135637.zip"
 * }
 */
export function zipSignXmlWordFiles(request: HttpServletRequest, response: HttpServletResponse, params: {
    xmlAttachInfos: string[]
}): ResultInfo {
    console.info(`开始执行[zipSignXmlWordFiles], 压缩指定的XML文档附件文件到压缩包`);
    console.info(params);

    let xmlAttachInfos: string[] = params.xmlAttachInfos;
    if (xmlAttachInfos.length == 0) {
        return { result: false, message: "未获取需要处理的XML附件文件" };
    }
    let resultInfo: ResultInfo = { result: true };

    /** 压缩包工具类对象 */
    let zipUtils = new ZipUtils();

    let shareDir: string = sys.getClusterShareDir();
    let tempZipName: string = `XML文档_${utils.formatDate("yyyyMMddHHmmss")}`;
    let zipFilePath: string = `${shareDir}/tempSaveImportXmlZipFord/${tempZipName}.zip`;
	/*
	 let workDir: string = sys.getWorkDir();
	 let zipFilePath: string = `${workDir}/temp/${utils.formatDate("yyyyMMdd")}/${tempZipName}.zip`;
	 可将其存储路径放置服务器临时目录下。
	 */
    console.info(`当前临时记录导出文件压缩包路径: ${zipFilePath}`);
    let zipFile: JavaFile = zipUtils.createZipFile(zipFilePath);

    let fileInputStream: FileInputStream = null;
    let zipOutputStream: ZipOutputStream = null;
    let xmlNums: number = xmlAttachInfos.size();
    console.info(`当前导出XML文档个数为: ${xmlNums}`);
    /** 导出XML文件不存在服务器个数 */
    let noExistXmlFileNums: number = 0;
    try {
        zipOutputStream = new ZipOutputStream(new FileOutputStream(zipFile));
        for (let i = 0; i < importXmlNums; i++) {
            let xmlAttachInfo: string[] = importXmlAttachInfos[i]; // eg: workdir:202212081624485901_1.病历概要.xml
            let xmlFileName: string = xmlAttachInfo[0];
            let attachFileType: string = xmlFileName.substring(xmlFileName.lastIndexOf(".") + 1);
            if (attachFileType != "xml") {
                console.info(`文件: [${xmlFileName}] 不是XML文件, 不执行压缩导出`);
                continue;
            }
            if (!xmlFileName || xmlFileName.length < 7) { // 文件名长度至少大于7(包含文件后缀)
                xmlFileName = `defaultPrx_${xmlFileName}`;
            }
            let xmlFileStrContent = xmlAttachInfo[1];
            if (!xmlFileStrContent) {
                console.info(`XML文件: ${xmlFileName} 无XML内容, 不执行压缩`);
                noExistXmlFileNums++;
                continue;
            }
            let tempXmlFile = fs.createTempFile();
            tempXmlFile.writeString(xmlFileStrContent, "utf-8");

            console.info(`开始压缩文件: ${xmlFileName}`);
            fileInputStream = tempXmlFile.openInputStream();
            let ztNoteFileEntry = new ZipEntry(xmlFileName);
            zipOutputStream.putNextEntry(ztNoteFileEntry);
            IOUtils.copy(fileInputStream, zipOutputStream);
            zipOutputStream.closeEntry();
            fileInputStream.close();
            fileInputStream = null; // 重新更改指针指向
            tempXmlFile.delete(); // 压缩后删除临时文件
        }
        resultInfo.zipPath = zipFilePath; // 记录压缩包路径
    } catch (e) {
        resultInfo.result = false;
        resultInfo.message = "XML附件文件压缩失败";
        console.error(`XML附件文件压缩执行失败[${e.toString()}], 删除临时压缩包文件[${fs.deleteFile(zipFilePath)}]`);
        console.error(e);
    } finally {
        fileInputStream && fileInputStream.close();
        zipOutputStream && zipOutputStream.close();
		
		/**
		 * 判断实际压缩数量和应压缩数量是否一致, 
		   1) 若==0, 则没有压缩文件, 删除压缩包。
		   2) 等于、小于 则需要根据业务情况来定。
		 */
        if (noExistXmlFileNums == xmlNums) {
            console.info(`导出的XML文件都不存在, 删除压缩包文件, 删除状态: ${zipFile.delete()}`);
            resultInfo.result = false;
            resultInfo.message = "导出XML文件都不存在, 无法导出";
        }
    }
    console.info(resultInfo);
    console.info(`执行结束[zipSignXmlWordFiles], 压缩指定的XML文档附件文件到压缩包`);
    return resultInfo;
}

/**
 * 导出指定路径压缩包文件, 导出后可选择是否删除压缩包
 * @param params.zipFilePath 压缩包路径, eg: /opt/workdir/clusters-share/file-storage/relativePath/XML文档_20221230135637.zip
 * @param params.isDeleteZip 是否导出后删除zip文件, 默认: 不删除
 * @param params.zipFileName 导出压缩包名称, 默认: 导出压缩包名
 */
export function importSignZipFile(request: HttpServletRequest, response: HttpServletResponse, params: {
    zipFilePath: string,
    isDeleteZip?: boolean,
    zipFileName?: string
}): void {
    print(`开始执行[importSignZipFile], 导出指定路径压缩包文件, 导出后删除压缩包`);
    print(params);

    let zipFilePath: string = params.zipFilePath;
    if (!zipFilePath) {
        return;
    }
    let isDeleteZip: boolean = params.isDeleteZip != undefined ? params.isDeleteZip : false;
    let tempZipName: string = params.zipFileName;
    if (!tempZipName) {
        tempZipName = zipFilePath.substring(zipFilePath.lastIndexOf("/") + 1);
    }
    if (tempZipName.lastIndexOf(".zip") == -1) {
        tempZipName = `${tempZipName}.zip`;
    }
    let outPut = response.getOutputStream();
    let zipFile: JavaFile = new JavaFile(zipFilePath);
    try {
        let fileBytes = Files_.readAllBytes(Paths_.get(zipFile.getAbsolutePath()));
        response.setContentType("application/zip");
        response.addHeader("Content-Disposition", `attachment; filename=${java.net.URLEncoder.encode(tempZipName, "UTF-8")}`);
        response.addHeader("Content-transfer-Encoding", "binary");
        outPut.write(fileBytes);
        print(`${zipFilePath} 压缩包导出成功`);
    } catch (e) {
        print(`[importSignZipFile], 导出指定路径压缩包文件失败`);
        print(e);
    } finally {
        outPut && outPut.close();
        if (isDeleteZip) {
            print(`删除压缩包文件, 删除状态: ${zipFile.delete()}`);
        }
    }
}