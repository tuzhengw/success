---
typora-root-url: images
---

# Declare关键字

​	增加：<button>智能提示</button> 效果

---

## * 使用场景：用于声明文件

## * 无声明（无提示）

​	例如：自己写一个`svr-api/File.d.ts`（`d：declare`）

```ts
class File {
	/**根据路径创建文件对象 */
	constructor(path: string);

	/** 获取文件或目录的完整路径 */
	getPath(): string;

	/** 获取文件或目录名称 */
	getName(): string;
}
```

​	在其他`TS`文件导入写好的：声明文件（`file.d.ts`）

```ts
// import { getFile } from "svr-api/`file";
import { fs } from "svr-api/`file";
function main(){
    // 若没有加：declare声明，则不会有提示信息
    let file = fs.getFile();
}
```

## * 有声明（有提示）

​	例如：自己写一个`svr-api/File.d.ts`（`d：declare`）

```ts
declare class File {
	/**根据路径创建文件对象 */
	constructor(path: string);

	/** 获取文件或目录的完整路径 */
	declare getPath(): string;

	/** 获取文件或目录名称 */
	getName(): string;
}
```

​		在其他`TS`文件导入写好的：声明文件（`file.d.ts`）

```ts
// import { getFile } from "svr-api/`file";
import { fs } from "svr-api/`file";
function main(){
    // 若没有加：declare声明，则会有提示信息
    let file = fs.getFile();
}
```

​	效果：

![](../images/提示信息.png)