/**
 * 此调用方式，需要注意传递参数会通过java处理，最终类型是：class java.util.LinkedHashMap
 * 内置若存在数组，类型则是：class java.util.ArrayList
 
 * 注意：需要将其转换为前端可以识别的对象才可以处理，否则，将其作为：java 处理
 */
await rc({
	url: "/SPZS/test/deleteTestData.action?method=deleteTestData",
	method: "POST",
	data: {
		dataSource: "zhjg_ods",
		tableName: `ODS_SPJG_SPKJ_BSZ`,
		primaryColumn: "OGANIZATIONID",
		primaryIds: [
			"1003"
		]
	}
});

// ------------------------《deleteTestData.action.ts》--------------------------------《后端脚本文件》

import { getDataSource } from "svr-api/db"; //数据库相关API
/**
 * 请求参数通过java处理后，params类型为：class java.util.LinkedHashMap
 * 引入java的：fastJson，将其map转换为：jsonString
 */
const FastJson = Java.type("com.alibaba.fastjson.JSONObject");
/**
	let jsonString = new FastJson(params);
	let jsonObject = JSON.prase(jsonString);
*/

/**
 * 根据主键删除数据库的测试数据（白沙洲食品快检记录表）
 * @params {
        dataSource: "zhjg_ods",
        tableName: `ODS_SPJG_SPKJ_BSZ`,
        primaryColumn: "OGANIZATIONID",
        primaryIds: [
            "1002"
        ]
 * }
 * @return { success: true }
 * 调用：/SPZS/test/deleteTestData.action.ts?method=deleteTestData
 */
export function deleteTestData(request: HttpServletRequest, response: HttpServletResponse, params: any): ResultInfo {
    console.debug(`--根据主键删除数据库的测试数据（白沙洲食品快检记录表）-- start`);
    /**
     * 若从外部调用，传递过来的params类型为：class java.util.LinkedHashMap
     * 解决办法：
     * （1）使用get获取，eg：params.get("dataSource")
     * （2）调用第三方工具，eg：
     * let jsonString = new FastJson(params);
     * let jsonObject = JSON.prase(jsonString);
     * 
	 * let dataSource: string = params.dataSource; 使用"."的方式也可获取
     */
    /** 数据库源，eg：default */
    let dataSource: string = params.get("dataSource");
    let db = getDataSource(dataSource);
    let tableName: string = params.get("tableName");
    let primaryColumn: string = params.get("primaryColumn");
    /**
     * 注意：
     * 若 POSTMAN传递 或者 使用get()得到的 数组值[] 类型为：class java.util.ArrayList
     * 需要将其List转换为Array
     */
    let testIds: Array<string> = [...params.get("primaryIds")];
    if (testIds == null || testIds.length == 0) {
        return { success: false, message: "primaryIds为空" };
    }
    let resultInfo: ResultInfo = { success: true };
    let deleteCondition: Array<string> = [];
    for (let i = 0; i < testIds.length; i++) {
        deleteCondition.push("?");
    }
    let quickFootCheckTable = db.openTableData(tableName);
    try {
        quickFootCheckTable.executeUpdate(`delete from ${tableName} where ${primaryColumn} in (${deleteCondition.join(",")})`, testIds);
    } catch (e) {
        console.error(e);
        resultInfo.success = false;
        resultInfo.message = "请联系管理员";
    }
    console.debug(`--根据主键删除数据库的测试数据（白沙洲食品快检记录表）-- end`);
    return resultInfo;
}



/**
 * 返回结果格式
 */
interface ResultInfo {
    /**
     * 结果
     */
    success: string | boolean;
    /**
     * 错误码
     */
    errorCode?: string;
    /**
     * 错误提示
     */
    message?: string;
}
