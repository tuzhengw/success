# `CSS`

## 1 鼠标移动到选项上修改背景颜色

```CSS
li a:hover {
    background-color: #555;
    color: white;
}
```

## 2  激活/当前点击DOM

```css
li a.active {
    background-color: #4CAF50;
    color: white;
}
```