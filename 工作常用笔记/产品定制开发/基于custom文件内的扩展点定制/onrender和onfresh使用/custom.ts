
import { IMetaFileCustomJS, InterActionEvent, IAppCustomJS, IDwCustomJS, FAppInterActionEvent, IFApp, IDataset, IFAppForm } from "metadata/metadata-script-api";
import { showDrillPage, getMetaRepository, MetaFileViewer, getCurrentUser, getSysSettings } from 'metadata/metadata';
import { IVPage } from 'metadata/metadata-script-api';

/** 身份证号 */
let cardValues: string = "";
/** 页面日期过滤值，默认时间：当天 */
let filterDateValue: string = formatDate();


export const CustomJS: { [type: string]: IMetaFileCustomJS } = {
    spg: {
        CustomActions: {
            /**
             * 20211203 tuzw
             * 记录页面设置的身份证号码，使之设置其值应用于多个SPG过滤
             * 地址：https://jira.succez.com/browse/CSTM-17239
             */
            setCardValues: (event: InterActionEvent) => {
                cardValues = event.params.cardValues;
            },
            setFilterDateValues: (event: InterActionEvent) => {
                filterDateValue = event.params.filterDateValue;
            },
        },
        /**
         * onRefresh：
			第一次页面渲染不会触发，只有页面渲染后，发生页面切换或者刷新，才会触发
		 
         *  页面内发生刷新行为【 前 】调用（不会被缓存，只要页面变动就刷新）
         */
        onRefresh: (event: InterActionEvent): void | Promise<void> => {
            updateCardValues(event);
            // return Promise.resolve();
        },
        // onRender：只有页面第一次渲染的时候执行，第二次及以后会读取缓存
        onRender: (event: InterActionEvent): void | Promise<void>  => {
            updateCardValues(event);
            // return Promise.resolve();
        },
    }
}

/**
 * 20211206 tuzw
 * 页面内发生刷新行为或者初次渲染时，给当前页面的全局变量：身份证号-更新-最新状态值
 */
function updateCardValues(event: InterActionEvent): void {

    let spgPage: IVPage = event.page;
    let pageParams: ParamInfo[] = event.page.getParameterNames();
    /** 全局参数：身份证号 */
    let currentPageCardValues: string = "";
	
    for (let i = 0; i < pageParams.length; i++) {
        if (pageParams[i].name == "@SFZH") {
            let currentPageCardValues: string = pageParams[i].value;
            /*
				(1) 若当前身份证号发生变化，则更新
				(2) cardValues == null：允许清空
			*/
            if (currentPageCardValues != cardValues || cardValues == null ) {
                // param1：全局参数（@SFZH）
                spgPage.setParameter("param1", cardValues);
				 // input1：对应SPG身份证值控件
                let cardInputDom = event.page.getComponent("input1");
                cardInputDom.setValue(cardValues);
            }
        }
        if (pageParams[i].name == "@date") {
            let currentPageDateValue: string = pageParams[i].value;
            /*
				(1) 若当前页面过滤日期值发生变化，则更新
				(2) filterDateValue == null：允许清空
			*/
            if (currentPageDateValue != filterDateValue  || filterDateValue == null) {
                spgPage.setParameter("param2", filterDateValue);
                let currentPageFilterDateComponent = event.page.getComponent('datecombobox1');
                // eg：'2021年12月5日'
                currentPageFilterDateComponent.setValue(filterDateValue);
            }
        }
    }
}


/**
 * 格式化时间戳
 * @parmas dateString：时间戳字符串
 */
function formatDate(): string {
    let dates = new Date();
    let Year: number = dates.getFullYear();
    let Months = (dates.getMonth() + 1) < 10
        ? '0' + (dates.getMonth() + 1)
        : (dates.getMonth() + 1);
    let Day = dates.getDate() < 10
        ? '0' + dates.getDate()
        : dates.getDate();
    return `${Year}年${Months}月${Day}日`;
}
