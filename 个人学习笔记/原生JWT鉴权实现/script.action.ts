/**
 * ================================================
 * 作者：tuzw
 * 审核员：liuyz
 * 创建日期：2022-11-24
 * 脚本用途：数据服务支持生成jwt token生成以及验证
 * 地址：https://jira.succez.com/browse/CSTM-21344
 * 参考地址：
 * 1) https://blog.csdn.net/qq_42402854/article/details/122896137
 * 2) https://blog.csdn.net/w1673492580/article/details/113616858
 * 批处理地址：https://dev.succez.com/sdi2/tasks/script?:open=liuyz
 * 
 * 验证token网址：https://jwt.io/
 * ================================================
 */
import StandardCharsets = java.nio.charset.StandardCharsets;
import MessageDigest = java.security.MessageDigest;
import Mac = javax.crypto.Mac;
import SecretKeySpec = javax.crypto.spec.SecretKeySpec;
import Base64 = org.apache.commons.codec.binary.Base64;
import JAVAString = java.lang.String;
import ObjectMapper = com.fasterxml.jackson.databind.ObjectMapper;
import Integer = java.lang.Integer;
import dw from "svr-api/dw";
import utils from 'svr-api/utils';

import sys from "svr-api/sys";

/**
 * 批处理脚本模版
 */
function main() {

    let jwtUtil = new JwtToken("FLwICYJBUdLqnLLq0mObeB");
    let token = jwtUtil.createToken();
    console.info(token);
    print(jwtUtil.verifyToken(token.token));

    // let tokenContent = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJGTHdJQ1lKQlVkTHFuTExxMG1PYmVCIiwiZXhwIjoxNjY5Mzc1MzcwfQ.An5AeBwpunIlHSr-5zbEtxzv8nax3rYDWlK_PFLlO1o";
    // let jwtUtil = new JwtToken("FLwICYJBUdLqnLLq0mObeB");
    // print(jwtUtil.verifyToken(tokenContent));
    // sys.sleep(2500);
    // let result = jwtUtil.verifyToken(toeknContent);
    // print(result);
}


/**
 * 2022-11-25 tuzw
 * JWT工具类
 * 
 * @description 
 * 使用jwt生成/验证token（jwt JSON Web Token）
 * jwt由三部分组成: 头部（header).载荷（payload).签证（signature)
 * <p>
 * 1.header头部承载两部分信息：{
 *   “type”: “JWT”, 声明类型，这里是jwt
 *   “alg”: “HS256” 声明加密的算法 通常直接使用 HMAC SHA256
 * }
 * 将头部进行base64加密, 构成了第一部分
 * <p>
 * 
 * 2.payload载荷就是存放有效信息的地方
 *  (1) 标准中注册的声明
 *  (2) 公共的声明 （一般不建议存放敏感信息）
 *  (3) 私有的声明 （一般不建议存放敏感信息）
 * 将其进行base64加密，得到Jwt的第二部分
 * <p>
 * 
 * 3.signature签证信息由三部分组成：
 * (1) header (base64后的)
 * (2) payload (base64后的)
 * (3) secret 加密盐
 * 需要base64加密后的header和base64加密后的payload连接组成的字符串，
 * 然后通过header中声明的加密方式进行加盐secret组合加密，构成了jwt的第三部分
 */
export class JwtToken {

	/** 加密盐(内容已解密) */
	private saltSecret: JAVAString;
	/** 算法, 默认: HmacSHA256 */
	private algorithm: JAVAString = "HmacSHA256";

	/** 有效时间/s (2小时) */
	private Expiration: number = 7200;
	/** 授权AppId */
	private authorizeId: string;
	/** appId启用状态 */
	private enableStatus: number;

	constructor(authorizeId?: string) {
		this.authorizeId = authorizeId as string;
	}

	/**
	 * 预检查
	 * @description 
	 * 1) 校验过程中查询app相关信息
	 */
	private preCheck(): ResultInfo {
		if (!this.authorizeId) {
			return { result: false, errorCode: TokenErrorCode.ParamError, message: "未传入授权ID" };
		}
		this.querySecretkey();
		if (!this.saltSecret) {
			return { result: false, errorCode: TokenErrorCode.AppIdNoExist, message: "appId不存在" };
		}
		if (this.enableStatus != 1) {
			return { result: false, errorCode: TokenErrorCode.AppNoEnable, message: "app未启用" };
		}
		return { result: true };
	}

	/**
	 * 创建token
	 * @param authorizeId 授权ID
	 * @return 
	 */
	public createToken(): ResultInfo {
		let preCheckResult: ResultInfo = this.preCheck();
		if (!preCheckResult.result) {
			return preCheckResult;
		}
		let headerMap: JwtHeader = {
			alg: "HS256",
			typ: "JWT"
		}
		let expireTime = new Integer((Date.now() / 1000) + this.Expiration); // 使用JAVA的整形包装类, 避免科学计数法数字
		let payLoadMap: JwtPayLoad = {
			key: this.authorizeId,
			exp: expireTime
		}
		let resultInfo: ResultInfo = { result: true };
		try {
			let objMapper = new ObjectMapper();
			let headerJson: JAVAString = objMapper.writeValueAsString(headerMap);
			let payloadJson: JAVAString = objMapper.writeValueAsString(payLoadMap);
			let headerB64 = Base64.encodeBase64URLSafeString(headerJson.getBytes(StandardCharsets.UTF_8));
			let payloadB64 = Base64.encodeBase64URLSafeString(payloadJson.getBytes(StandardCharsets.UTF_8));
			let content = JAVAString.format("%s.%s", headerB64, payloadB64);
			/** 摘要信息 */
			let signatureBytes: number[] = this.createSignature(content.getBytes(StandardCharsets.UTF_8));
			let signature = Base64.encodeBase64URLSafeString((signatureBytes));
			let signContent = JAVAString.format("%s.%s", content, signature);

			resultInfo.exp = expireTime;
			resultInfo.token = signContent;
		} catch (e) {
			resultInfo.result = false;
			resultInfo.errorCode = TokenErrorCode.TokenError;
			resultInfo.message = `token创建失败`;
			console.error(`创建token执行失败----error`);
			console.error(e);
		}
		return resultInfo;
	}

	/**
	 * 验证token
	 * @param token token值
	 * @return {
	 *    result: true
	 * }
	 */
	public verifyToken(token: string): ResultInfo {
		if (!token) {
			return { result: false, errorCode: TokenErrorCode.ParamError, message: "未传入token值" };
		}
		let signatureParts: string[] = token.split(".");
		if (signatureParts.length < 3) {
			return { result: false, errorCode: TokenErrorCode.TokenError, message: "token内容不合法, 请重新获取" };
		}
		let resultInfo: ResultInfo = { result: true };
		let headerB64: string = signatureParts[0];
		let payloadB64: string = signatureParts[1];
		let signatureB64: string = signatureParts[2];
		try {
			let payloadJson = new JAVAString(Base64.decodeBase64(payloadB64), StandardCharsets.UTF_8); // 解码载荷部分内容, 获取记录的token有效期内容
			let payLoadMap: JwtPayLoad = JSON.parse(payloadJson);
			this.authorizeId = payLoadMap.key as string; // 从载荷中获取到appId值

			let preCheckResult: ResultInfo = this.preCheck();
			if (!preCheckResult.result) {
				return preCheckResult;
			}

			console.info(`开始token签名校验`);
			let contentBytes: number[] = JAVAString.format("%s.%s", headerB64, payloadB64).getBytes(StandardCharsets.UTF_8);
			/** 实际的签名字节内容 */
			let signatureBytes: number[] = Base64.decodeBase64(signatureB64);
			let validResult: boolean = this.verifySignature(contentBytes, signatureBytes);
			if (!validResult) {
				return { result: false, errorCode: TokenErrorCode.SignatureError, message: "验签失败" };
			}
			console.info(`开始token有效期校验`);
			if (!payLoadMap.exp) {
				return { result: false, errorCode: TokenErrorCode.TokenError, message: "token有效期不合法, 请重新获取" };
			}
			let expireTime: number = payLoadMap.exp;
			let nowTime: number = Math.ceil(Date.now() / 1000);
			if (nowTime > expireTime) {
				return { result: false, errorCode: TokenErrorCode.EnpireTimeError, message: "token失效, 请重新获取" };
			}
		} catch (e) {
			resultInfo.result = false;
			resultInfo.message = "验证token失败";
			console.error(`验证token执行失败----error`);
			console.error(e);
		}
		return resultInfo;
	}

	/**
	 * 获取授权id
	 */
	public getAuthorizeId(): string {
		return this.authorizeId;
	}

	/**
	* 创建签名
	* @param contentBytes base64加密后的header和payload数据
	* @return 
	* @description 
	* 1) 对base64编码后的header和payload数据，通过指定的算法生成哈希签名，以确保数据不会被篡改
	*/
	private createSignature(contentBytes: number[]): number[] {
		let mac: Mac = Mac.getInstance(this.algorithm);
		let saltSecret = new JAVAString(utils.decodeBase64(this.saltSecret)); // 解密密钥
		let secretBytes: number[] = saltSecret.getBytes(StandardCharsets.UTF_8);
		mac.init(new SecretKeySpec(secretBytes, this.algorithm));
		return mac.doFinal(contentBytes);
	}

	/**
	 * 验证签名是否相等
	 * @param contentBytes 验证传入token的base64加密后的header和payload数据
	 * @param oldSignatureBytes 正确的签名内容
	 * @return 
	 */
	private verifySignature(contentBytes: number[], oldSignatureBytes: number[]): boolean {
		let newSignatureBytes: number[] = this.createSignature(contentBytes);
		return MessageDigest.isEqual(newSignatureBytes, oldSignatureBytes); // 新旧签名字节对比
	}

	/**
	 * 查询AppId对应的密钥
	 * @description 查询出来的密钥已被Base64加密过
	 */
	private querySecretkey(): void {
		let defaultDb = db.getDefaultDataSource();
		let trustedApp = defaultDb.openTableData("SZSYS_4_TRUSTED_APPS");
		let queryData = trustedApp.selectFirst(["APP_SECRET", "ENABLED"], { "APP_ID": this.authorizeId });
		if(queryData != null) {
			this.saltSecret = queryData['APP_SECRET'];;
			this.enableStatus = queryData['ENABLED'];
		}
		console.info(`appId: [${this.authorizeId}] 对应的密钥(未解密)为: ${this.saltSecret}, 启用状态: ${this.enableStatus}`);
	}

}

/** TOKEN校验错误码 */
enum TokenErrorCode {
    /** 参数错误 */
    ParamError = "paramError",
    /** 验签错误 */
    SignatureError = "SignatureError",
    /** token失效 */
    EnpireTimeError = "enpireTimeError",
    /** token内容错误 */
    TokenError = "tokenContentError",
    /** appId不存在 */
    AppIdNoExist = "appIdNoExist",
    /** appId未启用 */
    AppNoEnable = "appNoEnable"
}

/** JWT标头 */
interface JwtHeader {
    /** 签名使用的算法，eg: HmacSHA256, 写: HS256 */
    alg: string;
    /** 令牌的类型，JWT令牌统一写为JWT */
    typ: string;
}

/** 
 * JWT有效载荷
 * (自定义私有字段，一般会把包含用户信息的数据放到 payload中)
 */
interface JwtPayLoad {
    /** 发行人 */
    iss?: string;
    /** 到期时间 */
    exp?: number;
    /** 主题 */
    sub?: string;
    /** 授权ID */
    key?: string;
    /** 发布时间 */
    iat?: number;
}

/** 返回值 */
interface ResultInfo {
    /** 结果 */
    result: boolean;
    /** 消息 */
    message?: string;
    /** 状态码 */
    errorCode?: TokenErrorCode;
    /** token值 */
    token?: string;
    /** 失效时间(s) */
    exp?: number;
    data?: any;
}