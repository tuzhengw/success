# JAVA-爬虫-JSOUP

```
jsoup 是一款Java 的HTML解析器，可直接解析某个URL地址、HTML文本内容。
它提供了一套非常省力的API来取出和操作数据
```

官方地址：https://jsoup.org/cookbook/input/parse-document-from-string

中文地址：https://www.sunjs.com/doc/jsoup-44df0d5c8c974b9ab03f400457961bc1.html

# 实例1：爬取省区块代码示例(PC端

1 了解爬取的网页结构, 多个网页情况下, 将相同结构的网页分类

2 针对不通结构的网页, 针对性编写爬取代码

3 通过请求获取网页, 频繁请求需要注意时间间隔, 避免被网页识别为爬虫, 导致IP被封

```ts
import sys from "svr-api/sys";
import http from "svr-api/http";
import Jsoup = org.jsoup.Jsoup;

/**
 * 对比部署表和标准表模型差异（根据模型名来比较）
 */
function main() {
    let webCrawler = new WebCrawler();
    webCrawler.crawlerDistrictInfo();
}

/**
 * 爬取国家地区行政代码
 * 地址: https://jira.succez.com/browse/CSTM-21291
 * 国家统计局地址：http://www.stats.gov.cn/tjsj/tjbz/tjyqhdmhcxhfdm/
 */
class WebCrawler {

    /** 爬取主地址 */
    private mainWebAddress = "http://www.stats.gov.cn/tjsj/tjbz/tjyqhdmhcxhfdm/2021";
    /** 
     * 爬取省份信息,eg: {
     *   "42: { // 省份URL代码, eg: http://www.stats.gov.cn/tjsj/tjbz/tjyqhdmhcxhfdm/2021/42.html
     *      "provinceName": '湖北省'
     *   }, 
     *   "43": {
     *      ...
     *   }
     * } */
    private provinceInfos: JSONObject;
    /** 爬取的数据集合 */
    private crawlerDistrictInfos: DistrictInfo[];

    constructor() {
        this.crawlerDistrictInfos = [];
        this.crawlerProvinceInfos();
    }

    /**
     * 爬取区块信息入口
     */
    public crawlerDistrictInfo(): void {
        let provinceInfos = this.provinceInfos;
        let provinceKey: string[] = Object.keys(provinceInfos);
        for (let index = 0; index < 1; index++) {
            let key: string = provinceKey[index];
            let hrefValue: string = key;
            let provinceName: string = provinceInfos[key]['provinceName'];
            this.crawlerDistrictInfos.push({
                name: provinceName,
                parentId: "",
                districtId: hrefValue,
                level: 0
            });
            this.dealSingalProvinceIds(hrefValue, [], hrefValue, `${hrefValue}.html`, 0);
            console.info(`${provinceName} 地区代码以及子区块代码获取完成`);
        }
        console.info(this.crawlerDistrictInfos);
    }

    /**
     * 递归处理单个省份的区块代码
     * @param districtParentId 区块父级id值
     * @param hrefContentPaths 地址上下文路径, eg: ["11", "01", ...]
     * @param currentPageIndex 当前页面下标, eg: 42
     * @param linkHtmlName 跳转的html文件名, eg: 42.html
     * @param level 下钻页面层级, 初始: 0
     */
    private dealSingalProvinceIds(
        districtParentId: string,
        hrefContentPaths: string[],
        currentPageIndex: string,
        linkHtmlName: string,
        level: number
    ): void {
        hrefContentPaths[level] = currentPageIndex; // 利用下标赋值实现剪枝
        let urlAddress: string = this.dealRequestUrl(hrefContentPaths, linkHtmlName, level);
        if (!urlAddress) {
            return;
        }
        /** class org.jsoup.select.Elements */
        let pageDocument = this.requestWebContent(urlAddress);
        if (!pageDocument) { // 地址未请求成功, 当前页面为最底层
            return;
        }
        let tableTrComps = pageDocument.select(`table >tbody >tr >td >table >tbody >tr >td >table >tbody >tr >td >table >tbody >tr`);
        level++; // 层级+1, 不能放到循环内部递增, 避免同一个循环一直递增, 破坏层级
        for (let i = 1; i < tableTrComps.length; i++) { // 第一行为列表标头, 从第二行开始爬取数据
            let trComp = tableTrComps[i];
            let firstChild = trComp.select(">td:nth-child(1)"); // 子节点下标从1开始
            let secondChild = trComp.select(`>td:nth-child(2)`);
            let thirdChild = trComp.select(`>td:nth-child(3)`);

            let districtId: string = firstChild?.text();
            if (thirdChild.toString() != "") {
                this.crawlerDistrictInfos.push({
                    name: thirdChild?.text(),
                    parentId: districtParentId,
                    cityClassId: secondChild?.text(),
                    districtId: districtId,
                    level: level
                });
            } else {
                this.crawlerDistrictInfos.push({
                    name: secondChild?.text(),
                    parentId: districtParentId,
                    districtId: districtId,
                    level: level
                });
            }
            /** 文本绑定的超链接值(仅跳转后的html名, eg: 123.html) */
            let textBindHrefValue: string = this.getHrefValue(firstChild.toString(), false);
            let hrefName: string = `${i}`;
            if (i < 10) {
                hrefName = `0${i}`;
            }
            this.dealSingalProvinceIds(districtId, hrefContentPaths, hrefName, textBindHrefValue, level);
        }
    }

    /**
     * 处理请求的地址
     * @param hrefContentPaths 地址上下文路径, eg: ["11", "01", ...]
     * @param linkHtmlName 跳转的html文件名, eg: 1101001.html
     * @param level 下钻页面层级, 初始: 0
     * 
     * @return eg: http://www.stats.gov.cn/tjsj/tjbz/tjyqhdmhcxhfdm/2021/11/1101.html
     * 
     * @description 超链接地址规律
     * 1) .../tjbz/tjyqhdmhcxhfdm/2021/11.html
     * 1) .../tjbz/tjyqhdmhcxhfdm/2021/11/1101.html
     * 1) .../tjbz/tjyqhdmhcxhfdm/2021/11/01/110101.html
     * 4) .../tjbz/tjyqhdmhcxhfdm/2021/11/01/01/110101001.html
     * 
     * 思路: 
     * 1) 数组记录每次访问的上下文路径(不包含主地址路径)
     * 2) 最后访问的html文件名, 从跳转的DOM上取(a标签的href值即跳转的html文件名)
     */
    private dealRequestUrl(hrefContentPaths: string[], linkHtmlName: string, level: number): string {
        if (level == 0) {
            return `${this.mainWebAddress}/${linkHtmlName}`;
        }
        let currentUrlContent: string[] = []; // eg: 11/01
        for (let i = 0; i < level && i < hrefContentPaths.length; i++) { // 访问当前页面, 不包含当前层级记录的上下文路径
            let contentPath: string = hrefContentPaths[i];
            currentUrlContent.push(contentPath);
        }
        let urlAddress: string = `${this.mainWebAddress}/${currentUrlContent.join("/")}/${linkHtmlName}`;
        return urlAddress;
    }

    /**
     * 爬取所有需要爬取省份信息
     */
    private crawlerProvinceInfos(): void {
        this.provinceInfos = {};
        let document = this.requestWebContent(this.mainWebAddress);
        if (!document) {
            return;
        }
        let tableTrComps = document.select(`table > tbody > tr > td > table > tbody > tr > td > table > tbody >tr`);
        for (let trIndex = 4; trIndex <= 6; trIndex++) { // 省级信息在表格第4行——6行
            let trComp = tableTrComps[trIndex];
            let aComps = trComp.select('td>a');
            for (let aIndex = 0; aIndex < aComps.length; aIndex++) { // 页面省级名称都设置了对应的超链接交互, 从A标签中获取需要爬取的省级信息 
                let aComp = aComps[aIndex];
                let provinceName: string = aComp.text();
                let hrefValue: string = this.getHrefValue(aComp.toString(), true);
                if (hrefValue == "") {
                    continue;
                }
                if (!this.provinceInfos[hrefValue]) {
                    this.provinceInfos[hrefValue] = {};
                }
                this.provinceInfos[hrefValue] = {
                    provinceName: provinceName
                }
            }
        }
        console.info(`需要爬取的省份共: ${Object.keys(this.provinceInfos).length} 个, 信息为: `);
        console.info(this.provinceInfos);
    }

    /**
     * 请求指定的URL地址, 获取网页HTML内容
     * @param urlAddress 请求URL地址 
     * @return 
     * 
     * @description
     * 1) 直接请求某个网页, 会将网页的HTML内容返回
     * 2) 部分网页设有反爬虫机制
     * 3) 增加headers(身份信息)，增加爬取的稳定性
     */
    private requestWebContent(urlAddress: string) {
        if (!urlAddress) {
            return;
        }
        sys.sleep(500); // 递归请求, 每次请求前休眠0.5秒, 避免频繁请求导致IP被封
        let requetResult = http.request({
            url: urlAddress,
            method: 'GET',
            headers: {
                'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
                'Host': 'www.stats.gov.cn',
                'Cookie': 'SF_cookie_1=15502425',
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36 Edg/107.0.1418.42'
            }
        });
        if (requetResult.httpCode != 200) {
            return;
        }
        return Jsoup.parse(requetResult.responseText);
    }

    /**
     * 获取a标签的href值(无后缀.html)
     * @param aCompContent a标签DOM内容, eg: <td><a href="42/4202.html">420200000000</a></td>
     * @param removeSuffix 是否删除文件后缀
     * @return eg: 123
     * 
     * @description 返回的内容仅跳转后的HTML页面名, 不包含上下位路径
     */
    private getHrefValue(aCompContent: string, removeSuffix: boolean): string {
        if (!aCompContent) {
            return "";
        }
        let reg = /((\w+):\/\/)?([\w.]+[.]{1})[\w]+/;
        let matchResult = aCompContent.match(reg); // 正则获取a标签的href, eg: 42.html
        if (!matchResult) {
            return "";
        }
        let hrefContent: string = matchResult[0];
        let hrefValue: string = hrefContent.substring(hrefContent.lastIndexOf("/") + 1); // 避免href包含上下文短路径, 这里将其替换调
        if (removeSuffix) {
            hrefValue = hrefContent.substring(0, hrefContent.indexOf(".html")); // 去掉HTML后缀
        }
        return hrefValue;
    }
}

/** 区划代码 */
interface DistrictInfo {
    /** 名称 */
    name: string;
    /** 统计用区划代码	*/
    districtId?: string;
    /** 城乡分类代码 */
    cityClassId?: string;
    /** 父节点信息 */
    parentId?: string;
    /** 层级 */
    level?: number;
}
```





# 实例2: 蔬菜行情和水产行情内的近两年数据示例(小程序

地址：https://jira.succez.com/browse/CSTM-21213

```ts
package com.hayasi

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import kong.unirest.Unirest
import org.jsoup.Jsoup
import java.io.File
import java.nio.file.Files
import kotlin.io.path.Path


private fun main1() {
    val mapper = jacksonObjectMapper()
    var index = 5400
    while (true) { // 因为不知道要爬取多少页, 手动控制循环（debug）
        val title = writeFileByIndex(index, mapper)
        println("$index $title")
        index--;
    }
}

private fun writeFileByIndex(index: Int, mapper: ObjectMapper): String {
    val body = Unirest.get("http://baishazhou.21itcn.cn/Price/Detail/$index").asString().body
    val document = Jsoup.parse(body)
    val title = document.select("title").text()
    val table = document.select("table#market")
    val headMap = hashMapOf<Int, String>()
    for ((i, key) in table.select("thead th").withIndex()) {
        headMap[i] = key.text()
    }
    val rows = table.select("tbody tr")
    val valueMaps = arrayListOf<Map<String, String>>()
    for (row in rows) {
        val cells = row.select("td")
        val map = hashMapOf<String, String>()
        for ((i, key) in headMap) {
            val value = cells[i].text().trim()
            map[key] = value
        }
        valueMaps.add(map)
    }
    mapper.writeValue(File("D:\\images", "$title.json"), valueMaps)
    return title
}


private fun main2() {
    val files = File("D:\\images").listFiles()!!
    val mapper = jacksonObjectMapper()
    val list = ArrayList<Map<String, String>>()
    val set = HashSet<String>()
    for (file in files) {
        val name = file.name
        val type = name.substring(11, 13)
        val date = name.substring(0, 11)
        val maps = mapper.readValue<List<Map<String, String>>>(file)
        list.addAll(maps.map {
            val map = HashMap(it)
            map["date"] = date
            map["type"] = type
            set.addAll(map.keys)
            map
        })
    }
    println(mapper.writeValueAsString(set))
    mapper.writeValue(File("D:\\CLionProjects\\综合数据.json"), list)
}


fun main(){
    val mapper = jacksonObjectMapper()
    val list = mapper.readValue<List<Map<String, String>>>(File("D:\\CLionProjectsCLionProjects\\综合数据.json"))
    val typeData = list.groupBy { it["type"] }
    for ((type, data) in typeData) {
        val fields =
            data.fold(HashSet()) { acc: HashSet<String>, map: Map<String, String> -> acc.addAll(map.keys);return@fold acc }
                .toList()
        val writer = Files.newBufferedWriter(Path("$type.txt"))
        writer.use {
            it.appendLine(fields.joinToString("|"))
            for (map in data) {
                it.appendLine(fields.map { f -> map[f] }.joinToString("|"))
            }
        }
    }
}
```

