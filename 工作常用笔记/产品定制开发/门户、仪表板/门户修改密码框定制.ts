/**
 * 重载掉门户中默认的修改密码对话框
 */
class SDI_BaseTemplatePage {
	
	/**
	 * 重写原本打开逻辑
	 */
	protected showModifyPasswordDialog() {
		if (this.builder.viewMode === DesignerViewMode.Edit) {
			showWarningMessage(message('dsn.tpg.modifyPassword.warningInfo'));
			return;
		}
		return showDrillPage({
			url: "/dsjpxpt/app/Home.app/User_Setting/PERSONAL_PASSWORD.spg",
			title: "修改密码",
			target: ActionDisplayType.Dialog,
			dialogArgs: {
				width: 730,
				height: 450
			}
		})
	}
}

modifyClass("app/templatepage", "BaseTemplatePage", SDI_BaseTemplatePage);