```
# **GET_DB_TABLE_LEN(dataBaseName: string): number**

 获取指定数据库允许的最大表名长度。

**类型必须是string，且不为NULL!**

## 参数
***dataBaseName***：数据库类型。

## 示例
1. `GET_DB_TABLE_LEN(NULL)` -1
2. `GET_DB_TABLE_LEN("oracle")` 30
3. `GET_DB_TABLE_LEN("other")` 64
```

