
import { getDataSources, getDataSource, getDefaultDataSource } from 'svr-api/db';

/**
 * 获取JDBC信息
 * 前提：数据库【连接成功】
 * @param dataSourceName 数据源名
 */
function getJdbcInfo(dataSourceName: string): void {
    let dataSource = getDataSource(dataSourceName);
    let conn;
    try {
        conn = dataSource.getConnection();
        let dbMetaData = conn.getMetaData();
        print("数据库的URL: " + dbMetaData.getURL());
        print("数据库用户名: " + dbMetaData.getUserName());
        print("数据库产品名称: " + dbMetaData.getDatabaseProductName());
        print("数据库产品版本: " + dbMetaData.getDatabaseProductVersion());
        print("JDBC驱动程序名称: " + dbMetaData.getDriverName());
        print("JDBC驱动版本: " + dbMetaData.getDriverVersion());
        print("JDBC驱动程序主要版本: " + dbMetaData.getDriverMajorVersion());
        print("JDBC驱动程序次要版本: " + dbMetaData.getDriverMinorVersion());
        print("最大连接数量: " + dbMetaData.getMaxConnections());
        print("最大表名长度: " + dbMetaData.getMaxTableNameLength());
        print("表中的最大列数: " + dbMetaData.getMaxColumnsInTable());
    } catch (e) {
        print(e);
    } finally {
        conn && conn.close();
    }
}


import { getString, modifyFile } from 'svr-api/metadata';
/**
 * 获取指定的数据源JDBC信息
 * @param dbSourceNames 数据源名数组
 * 注意：
 * （1）由于getJdbc方法只能获取链接成功的数据源信息，这里改为获取数据源元数据信息
 * （2）元数据没有数据库版本 和 JDBC版本
 * @return 
 */
function getDbSourceJdbcInfos(dbSourceNames: string[]): DbConnectionInfo[] {
    if (dbSourceNames.length == 0) {
        print(`getAllDbSourceInfos()，参数为空`);
        return [];
    }
    print("开始---获取指定的数据源JDBC信息");
    let dbSourceInfo: DbConnectionInfo[] = [];
    let dbMetadataRootPath: string = "/sysdata/data/sources";
    for (let i = 0; i < dbSourceNames.length; i++) {
        let dbSourcePath: string = `${dbMetadataRootPath}/${dbSourceNames[i]}.jdbc`;
        let metadataFileContent: string = getString(dbSourcePath);
        if (!!metadataFileContent) {
            dbSourceInfo.push(JSON.parse(metadataFileContent));
        }
    }
    print("获取指定的数据源JDBC信息---完成");
    return dbSourceInfo;
}
 