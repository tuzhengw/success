---
typora-root-url: images
---

此处部署是独立部署, 可自己使用docker来部署，详细可见当前文件夹：docker部署软件。

参考笔记：https://juejin.cn/post/7031188920511660063

# 一台部署两个MySQL

参考地址：https://blog.csdn.net/hyj_king/article/details/128940870?spm=1001.2101.3001.6650.4&utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7ECTRLIST%7ERate-4-128940870-blog-124042810.235%5Ev28%5Epc_relevant_t0_download&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7ECTRLIST%7ERate-4-128940870-blog-124042810.235%5Ev28%5Epc_relevant_t0_download&utm_relevant_index=9

下载地址：https://downloads.mysql.com/archives/installer/

## 1 卸载mariadb

​	参考地址：https://www.jianshu.com/p/288947ac7c55

​	centos7 自带mariadb数据库，跟 MySQL 冲突,  所以一般先卸载这个没用的数据库。

```cmd
[root@node2 opt]# rpm -qa | grep mariadb

mariadb-libs-5.5.68-1.el7.x86_64
[root@node2 opt]# rpm -e mariadb-libs-5.5mariadb-libs-5.5.68-1.el7.x86_64
错误：未安装软件包 mariadb-libs-5.5mariadb-libs-5.5.68-1.el7.x86_64 
[root@node2 opt]# rpm -e mariadb-libs-5.5.68-1.el7.x86_64
错误：依赖检测失败：
    libmysqlclient.so.18()(64bit) 被 (已安裝) postfix-2:2.10.1-9.el7.x86_64 需要
    libmysqlclient.so.18(libmysqlclient_18)(64bit) 被 (已安裝) postfix-2:2.10.1-9.el7.x86_64 需要
[root@node2 opt]# ^C
[root@node2 opt]# rpm -ev mariadb-libs-5.5.68-1.el7.x86_64
错误：依赖检测失败：
    libmysqlclient.so.18()(64bit) 被 (已安裝) postfix-2:2.10.1-9.el7.x86_64 需要
    libmysqlclient.so.18(libmysqlclient_18)(64bit) 被 (已安裝) postfix-2:2.10.1-9.el7.x86_64 需要
[root@node2 opt]# rpm -ev --nodeps mariadb-libs-5.5.68-1.el7.x86_64
软件包准备中...
mariadb-libs-1:5.5.68-1.el7.x86_64

[root@node2 opt]# rpm -qa | grep mariadb
[root@node2 opt]# 
```

​	若提示没有权限,  解决办法(进入超级用户模式）：

```cmd
[root@node2 opt]# rpm -e mariadb-libs-5.5mariadb-libs-5.5.68-1.el7.x86_64
error: package mariadb-libs-5.5.68-1.el7.x86_6 is not installed
admin is not in the sudoers file.  This incident will be reported 

[admin@localhost ~]$ su   // 切换到root用户下
Password: 
[root@localhost admin]# rpm -qa|grep mariadb
mariadb-libs-5.5.68-1.el7.x86_64
[root@localhost admin]# rpm -e --nodeps mariadb-libs-5.5.68-1.el7.x86_64
[root@localhost admin]# rpm -qa|grep mariadb
[root@localhost admin]# 
```

## 2 环境说明

 	mysql版本：mysql-5.7.39 （重新部署为8.0及以上版本）

​		linux系统版本：centos7.6

​		位置规划：

```cmd
mysql_3306: /urs/local/install/mysql/mysql3306

mysql_3307: /urs/local/install/mysql/mysql3307
```
​	<!--版本5.7和8.0属于大版本更换，部署有一定的区别，可见本章节第5点-->

## 3 安装步骤-mysql3306为例

### 3.1 上传MySQL到指定目录

```cmd
[root@localhost /]# cd /usr/home/zipfile
[root@localhost zipfile]# ls
mysql-5.7.29-linux-glibc2.12-x86_64.tar.gz
```

![](/上传mysql.png)

### 3.2 解压

```cmd
[root@localhost /]# cd /usr/home/zipfile
[root@localhost zipfile]# ls
mysql-5.7.29-linux-glibc2.12-x86_64.tar.gz

[root@localhost zipfile]# tar -zxvf  mysql-5.7.29-linux-glibc2.12-x86_64.tar.gz
```

​	解压erlang时遇到下列错误：

```
gzip: stdin: not in gzip format
tar: Child returned status 1
tar: Error is not recoverable: exiting now
```

```
[root@localhost zipfile]# tar -vxf memcached-1.4.34.tar.gz
tar包压缩的时候用cvf参数，解压的时候用xvf参数（用此命令解决）
或压缩的时候用czvf参数，解压的时候用xzvf参数（常用，这次报了这个错）
```

### 3.3 移动到指定目录下

```cmd
[root@localhost zipfile]# mv flink-1.16.1 /usr/local/install/mysql ${重名命名: 可选}

[root@localhost zipfile]# cd /usr/local/install
[root@localhost install]# ls
mysql-5.7.29-linux-glibc2.12-x86_64
```

### 3.4 重新命名

```cmd
[root@localhost /]# cd /usr/local/instal/mysql
[root@localhost mysql]# mv mysql-5.7.29-linux-glibc2.12-x86_64 mysql3306
```

### 3.5 新建数据存放目录

```cmd
[root@localhost mysql]# mkdir -p mysql3306/data
[root@localhost mysql3306]# chown -R root:root data // 赋予root用户权限
```

### 3.6 添加用户、用户组

​	创建用户和用户组

```cmd
[root@localhost /]# groupadd mysql  // 新建一个用户组
[root@localhost /]# useradd -r -g mysql mysql  // 用户组下新增一个用户(全局): mysql
```

​	将安装目录所有者及所属组改为mysql ，这个根据自己的目录来

​	mysql启动时基于mysql用户来创建的, 此处赋予存储mysql3306的父文件夹给与权限。

​	若后续增加子文件，若不是通过mysql账户创建的文件，则需要重新执行一次赋权

```cmd
[root@localhost /]# chown -R mysql.mysql /usr/local/install/mysql
```

### 3.7 新建cnf文件

<!--注意: 若需要开启binlog日志, 参考：3.13-->

​	**mysql 多个参数选项文件my.cnf优先级**：https://blog.csdn.net/jolly10/article/details/79662726

```cmd
[root@localhost install]# ls
flink-1.16.1  jdk-11  mysql
[root@localhost install]# cd mysql
[root@localhost mysql]# ls
mysql3306

# 若需要配置binlog日志，需要创建个父文件夹存储, 避免每次启动都创建一个新的log日志文件
[root@localhost mysql3306]# mkdir -p mysqlbin
# 创建bin日志文件索引文件
[root@localhost mysqlbin]# touch mysql-bin.index
# 赋予读写权限
[root@localhost mysql3306]# sudo chmod -R 777 mysqlbin  

# 注意不要创建为my3306.cnf.text, 可以通过FileZilla_3.63.2.1软件下载下来看看, 若是vim方式无法创建，可以外部传入一个文件
[root@localhost mysql]# vim mysql3306/my3306.cnf
```

​	**文件内容**：

```cmd
[mysqld]   # 注意部分配置需要写到[mysqlId]模块下
bind-address=0.0.0.0
port=3306
user=mysql
basedir=/usr/local/install/mysql/mysql3306
datadir=/usr/local/install/mysql/mysql3306/data
socket=/usr/local/install/mysql/mysql3306/data/mysql.sock
log-error=/usr/local/install/mysql/mysql3306/data/mysql.err
character_set_server=utf8mb4
symbolic-links=0
explicit_defaults_for_timestamp=true
default-time-zone='+08:00'

# binlog 日志配置
#设置日志路径, 注意路径需要mysql用户权限写, 这里可写绝对路径
log_bin=/usr/local/install/mysql/mysql3306/mysqlbin/mysql-bin
#设置binlog清理时间, 默认: 0
# expire_logs_days = 1
server_id=1

# 需要同步的数据库名称
binlog-do-db=test
max_allowed_packet = 20M
 
[mysqld_safe]
log-error=/var/log/mysqld3306.log
#pid-file=/var/run/mysqld/mysqld3306.pid
pid-file=/usr/local/install/mysql/mysql3306/mysqld3306.pid
 
[client]
socket=/usr/local/install/mysql/mysql3306/data/mysql3306.sock
 
[mysqldump]
socket=/usr/local/install/mysql/mysql3306/data/mysql3306.sock
  
[mysqladmin]
socket=/usr/local/intall/mysql/mysql3306/data/mysql.sock
 
#
# include all files from the config directory
#
!includedir /etc/my.cnf.d
```

### 3.8 初始化数据库(配置环境变量)

```
在MySQL中，是允许存在多个 my.cnf 配置文件的，有的能对整个系统环境产生影响，例如：/etc/my.cnf。有的则只能影响个别用户，例如：~/.my.cnf。

MySQL读取各个my.cnf配置文件的先后顺序是：

/etc/my.cnf
/etc/mysql/my.cnf
/usr/local/mysql/etc/my.cnf
~/.my.cnf

[root@localhost mysql3306]# mysql --help|grep 'my.cnf'
```

​	**指定cnf存储文件**：

```cmd
[root@localhost mysql3306]# bin/mysqld --defaults-file=/usr/local/install/mysql/mysql3306/my3306.cnf --basedir=/usr/local/install/mysql/mysql3306 --datadir=/usr/local/install/mysql/mysql3306/data --user=mysql --initialize
```

​	**异常**：

```
mysqld: Can't read dir of '/etc/my.cnf.d' (Errcode: 2 - No such file or directory)
```

​	**解决办法**：

```cmd
[root@localhost mysql3306]# mkdir /etc/my.cnf.d
[root@localhost mysql3306]# rm -rf /etc/my.cnf.
[root@localhost mysql3306]# bin/mysqld --defaults-file=/usr/local/install/mysql/mysql3306/my3306.cnf --basedir=/usr/local/install/mysql/mysql3306 --datadir=/usr/local/install/mysql/mysql3306/data --user=mysql --initialize
[root@localhost mysql3306]# 
```

​	**配置环境变量**：

```cmd
[root@localhost mysql3306]# vim ~/.bash_profile  
// 刷新(若配置了环境变量, 依然提示没有找到命令, 则重新执行刷新：source)
[root@localhost mysql3306]# source ~/.bash_profile

export PATH=$PATH:/usr/local/install/mysql/mysql3306/bin
```

​	**异常（原因cnf文件不对，可能是my3306.cnf.text，文件类型错误）**：

```
[root@localhost mysql3307]# bin/mysqld --defaults-file=/usr/local/install/mysql/mysql3307/my3307.cnf --basedir=/usr/local/install/mysql/mysql3307 --datadir=/usr/local/install/mysql/mysql3307/data --user=mysql --initialize
mysqld: Can't read dir of '/etc/my.cnf.d                                                                                                           26,8          Topi' (OS errno 2 - No such file or directory)
mysqld: [ERROR] Fatal error in defaults handling. Program aborted!
```

### 3.9 启动MySQL

```cmd
[root@localhost mysql3306]# cd /usr/local/install/mysql/mysql3306
[root@localhost mysql3306]# bin/mysqld_safe --defaults-file=/usr/local/install/mysql/mysql3306/my3306.cnf &
```

​	启动日志(从/usr/local/install/mysql/mysql3306/data启动数据库的mysqld守护进程), 启动失败：

```
[root@localhost mysql3306]# 2023-04-15T06:17:32.013292Z mysqld_safe Logging to '/var/log/mysqld3306.log'.
2023-04-15T06:17:32.029620Z mysqld_safe Starting mysqld daemon with databases from /usr/local/install/mysql/mysql3306/data
2023-04-15T06:17:32.388107Z mysqld_safe mysqld from pid file /var/run/mysqld/mysqld3306.pid ended

[1]+  Done                    bin/mysqld_safe --defaults-file=/usr/local/install/mysql/mysql3306/my3306.cnf
```

​	 **注意**：这里如果启动不成功需要对日志进行排查，常见错误为：

​		Can't start server: can't check PID filepath: No such file or directory

​	解决办法：在my3306.cnf的文件中查看pid-file的位置，创建对应的目录并修改777权限 

​	**启动成功，进行mysql连接**：

```cmd
[root@localhost mysql3306]# mysql -S /usr/local/install/mysql/mysql3306/data/mysql.sock  -P 3306 -u root -p
Enter password: 
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 2
Server version: 5.7.29

Copyright (c) 2000, 2020, Oracle and/or its affiliates. All rights reserved.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.
```

### 3.10 查看默认数据库密码

```cmd
[root@localhost mysql3306]# cat /usr/local/install/mysql/mysql3306/data/mysql.err

2023-04-15T06:15:41.658138Z 0 [Warning] InnoDB: New log files created, LSN=45790
2023-04-15T06:15:41.677372Z 0 [Warning] InnoDB: Creating foreign key constraint system tables.
2023-04-15T06:15:41.731742Z 0 [Warning] No existing UUID has been found, so we assume that this is the first time that this server has been started. Generating a new UUID: f2e70b7f-db54-11ed-86db-000c2991b255.
2023-04-15T06:15:41.732469Z 0 [Warning] Gtid table is not ready to be used. Table 'mysql.gtid_executed' cannot be opened.
2023-04-15T06:15:42.039366Z 0 [Warning] CA certificate ca.pem is self signed.
2023-04-15T06:15:42.151354Z 1 [Note] A temporary password is generated for root@localhost: xltt(dSv=7Ny
[root@localhost mysql3306]# 
```

​	默认密码: xltt(dSv=7Ny

### 3.11 登录MySQL并修改密码

```CMD
// 登录
[root@localhost mysql3306]# mysql -S /usr/local/install/mysql/mysql3306/data/mysql.sock  -P 3306 -u root -p
# [root@localhost mysql3306]# bin/mysql -S /usr/local/install/mysql/mysql3306/data/mysql.sock  -P 3306 -u root -p  若上述命令不成功，则用此命令
// 重设密码
mysql> SET PASSWORD = PASSWORD('123456');
Query OK, 0 rows affected, 1 warning (0.00 sec)

// 赋予远程登录权限
mysql> GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY '123456' WITH GRANT OPTION;
Query OK, 0 rows affected, 1 warning (0.00 sec)

// 生效
mysql> flush privileges;
Query OK, 0 rows affected (0.00 sec)

// 退出数据库（未关闭数据库）
mysql> exit
Bye
```

​	解决bash: mysql: command not found 的方法

```cmd
// 最下面写 export PATH=$PATH:/usr/local/install/mysql/mysql3306/bin
[root@localhost mysql3306]# vim ~/.bash_profile  
// 刷新(若配置了环境变量, 依然提示没有找到命令, 则重新执行刷新：source)
[root@localhost mysql3306]# source ~/.bash_profile
```

### 3.12 关闭数据库

​	关闭密码：123456

```cmd
[root@localhost mysql3306]# cd /usr/local/install/mysql/mysql3306
[root@localhost mysql3306]# bin/mysqladmin -S /usr/local/install/mysql/mysql3306/data/mysql.sock -u root shutdown -p

mysqladmin: [Warning] Using a password on the command line interface can be insecure.
Enter password: 
2023-04-15T07:56:39.819284Z mysqld_safe mysqld from pid file /usr/local/install/mysql/mysql3306/mysqld3306.pid ended
[1]+  Done                    bin/mysqld_safe --defaults-file=/usr/local/install/mysql/mysql3306/my3306.cnf
```

​	或者

```cmd
[root@localhost mysql3306]# bin/mysqladmin -S /usr/local/install/mysql/mysql3306/data/mysql.sock -P 3306 -u root shutdown -p
```

​	**注意：**kill -9 pid 在这里时kill不掉mysql进程，需要使用上面语句关闭数据库。数据库关闭之后再启动需要执行下面语句：

```cmd
[root@localhost mysql3306]# cd /usr/local/install/mysql/mysql3306
[root@localhost mysql3306]# bin/mysqld_safe --defaults-file=/usr/local/install/mysql/mysql3306/my3306.cnf &
```

### 3.13 mysql的binlog开启方式

​	参考地址：https://blog.csdn.net/weixin_43944305/article/details/108620849

​	判断MySQL是否已经开启binlog（**若需要开启最好在3.7就进行配置**）

```mysql
-- SHOW MASTER STATUS

show variables like '%log_bin%';
```

![](/bin日志位置.png)

​	开启后可在数据库查看

![](/bin日志开启状态.png)

## 4 远程连接

​	注意：需要先关闭linux防火墙(**注意防火墙若没有永久关闭, 每次启动都会自动打开**)

![](/远程连接数据库.png)



## 5 部署8.0注意事项

### 5.1 初次登录拒绝访问

```cmd
[root@localhost mysql3307]# bin/mysql -S /usr/local/install/mysql/mysql3307/data/mysql.sock  -P 3307 -u root -p
Enter password: 
ERROR 1045 (28000): Access denied for user ‘root‘@‘localhost‘ (using password: NO/YES)
```

**解决办法**：

​	1）修改my3307.cnf配置文件，在[mysqld]下添加skip-grant-tables

```cmd
mysqld]
...
skip-grant-tables  # 登录和退出忽视密码
...
```

​	2）修改配置后需要重启数据库

```cmd
[root@localhost mysql3307]# ps -ef | grep mysql  # 第二列数字为线程编号
[root@localhost mysql3307]# kill -9 3591 3865  # 直接强制删除指定的线程
```

​	3）重新登录MySQL

```cmd
[root@localhost mysql3307]# bin/mysqld_safe --defaults-file=/usr/local/install/mysql/mysql3307/my3307.cnf &
[root@localhost mysql3307]# bin/mysql -S /usr/local/install/mysql/mysql3307/data/mysql.sock  -P 3307 -u root -p
Enter password:   # 直接回车
```

​	4）修改localhost

```mysql
mysql> use mysql;
mysql> update user set host='%' where user ='root';
```

​	5）修改密码

```cmd
mysql> UPDATE mysql.user SET authentication_string=null WHERE User='root';
mysql> FLUSH PRIVILEGES;

# mysql8.0 引入了新特性 caching_sha2_password；这种密码加密方式Navicat 12以下客户端不支持；
# Navicat 12以下客户端支持的是mysql_native_password
# 查看当前加密方式：select host,user,plugin from user; 

# mysql> ALTER USER 'root'@'%' IDENTIFIED WITH caching_sha2_password BY '123456';
mysql> ALTER USER 'root'@'%' IDENTIFIED WITH mysql_native_password BY '123456';

mysql> FLUSH PRIVILEGES;
exit;
```

​	6）关闭数据库，去除skip-grant-tables属性后重新启动数据库，再登录数据库。

### 5.2 MySQL8.0允许外部访问

```sql
// 参考：https://blog.csdn.net/h996666/article/details/80921913
mysql> GRANT ALL PRIVILEGES ON *.* TO 'root'@'%'WITH GRANT OPTION;
```

# end 