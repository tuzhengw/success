import { getCurrentUser } from 'svr-api/security';

export function TEST_USER_EXTPROPERTY_SYNC(userIdOrProperty: string, property?: string) {
	if (!property) {
		property = userIdOrProperty;
		userIdOrProperty = getCurrentUser().userInfo.userId;
	}
	return userIdOrProperty + ' ' + property + '属性值';
}

export function TEST_USER_EXTPROPERTY_ASYNC(userIdOrProperty: string, property?: string) {
	if (!property) {
		property = userIdOrProperty;
		userIdOrProperty = getCurrentUser().userInfo.userId;
	}
	return userIdOrProperty + ' ' + property + '属性值'; // 后端不能返回异步结果
}