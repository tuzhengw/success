
dw-types.ts

/**
 * 导出数据参数
 */
interface ExportDataParams {
	/**
	 * 项目名称，导出物理表时必填
	 */
	projectName?: string;
	/**
	 * 数据源名称，导出物理表时必填
	 */
	datasourceName?: string;
	/**
	 * 物理表模式名称，导出物理表时必填
	 */
	schema?: string;
	/**
	 * 要导出的物理表名称，导出物理表时必填
	 */
	tableNames?: string[];

	/**
	 * 要导出数据的sql
	 */
	sql?: string;

	/**
	 * 导出query数据时传递
	 */
	resid?: string;
	/**
	 * 根据query信息导出数据时传递
	 */
	query?: QueryInfo,

	/**
	 * 支持导出多个query结果到一个excel文件，每个query结果对应一个sheet页
	 */
	queryMap?: { [queryName: string]: QueryInfo };

	/**
	 * 导出的列标题
	 */
	headNames?: string[];

	/**
	 * 导出多个query结果时，定制每个导出的列标题
	 */
	headNamesMap?: { [queryName: string]: string[] },

	/**
	 * 导出模型表的路径，导出模型数据时必填
	 */
	dwTablePath?: string;

	/**
	 * 编码方式
	 */
	encoding?: EncodingType;
	/**
	 * 导出格式
	 */
	fileFormat?: DataFormat;
	/**
	 * 导出文件的名称，不含后缀，后缀后台根据导出格式自动生成
	 */
	fileName?: string;

	/**
	 * 是否进行压缩,一次导出多个文件默认压缩
	 */
	compressed?: boolean;

	/**
	 * 任务id
	 */
	taskId?: string;

	/**
	 * 导出excel的表格样式。
	 * 
	 * spg 导出列表所有页数据时，有限的支持excel样式，不要求完全“保真”，比如包含条件样式之类的，主要用途是导出数据，
	 * 避免和报表现在的实现一样需要用无头浏览器在后端导出所有分页数据。
	 */
	exportTableStyle?: ExportTableStyle;
}

/**
 * 导出excel表格的列样式
 */
interface ExportColumnStyle {
	/** 列宽 */
	width?: number;
	/** 列的对其方式 */
	align?: "right" | "center" | "left" | "justify";
	/** 缩进 */
	indent?: number;
	/** 数据列的显示格式 */
	format?: string;
}

/**
 * 导出excel的表格样式。
 * 
 * spg 导出列表所有页数据时，有限的支持excel样式，不要求完全“保真”，比如包含条件样式之类的，主要用途是导出数据，
 * 避免和报表现在的实现一样需要用无头浏览器在后端导出所有分页数据。
 */
interface ExportTableStyle {
	/** 是否显示网格线 */
	showGridLine?: boolean;
	/** 填充区域边框 */
	bordersInfo?: CellsBordersInfo;

	/** 导出列样式，是一个数组，可以跟每行设置列样式，按导出的列字段顺序依依对应 */
	columns?: { [queryName: string]: ExportColumnStyle[] } | ExportColumnStyle[];
	header?: {
		/** 颜色填充 */
		backgroundColor: string;
		rowHeight: number;
		font: FontInfo;
	};
	/** body的样式会循环使用，比如body定义了三行的样式，那么后端导出的excel数据行会循环使用这三行的样式 */
	body?: {
		// 数组长度大于一表示多个颜色隔行换色循环使用
		backgroundColor: string | string[];
		rowHeights: number[];
		font: FontInfo;
	}
}