export const CustomJS: { [type: string]: IMetaFileCustomJS } = {
    /**
     * 当前项目的所有spg都会进到此方法
     */
    spg: {
        CustomActions: {
           /**
             * tuzw test
             * event为点击事件后触发的
             * 测试获取不同模型表主键
             */
            test_getKeys: (event: InterActionEvent) => {
                let superPageBuilder: SuperPageBuilder = event.component && event.component.node.builder;
                /** SPG路径 */
                let spgPath = superPageBuilder.getFileInfo().path;
                let disPlayListObj: SpgListBuilder = superPageBuilder.getComponent("list1") as SpgListBuilder;
                /** 获取模型表的ID */
                let modelId = disPlayListObj.getQueries()[0].getMainModel().getId();
                /** SPG的模型的数据对象（包含：模型表的主键），动态获取List引入模型表主键集 */
                let currentModelKeys: Array<string> = superPageBuilder.getData().getDataset(modelId).getPrimaryKeys();
                let temp = {};
                currentModelKeys.forEach(item => {
                    /** 
                     * temp.item：表示给item属性赋值，会覆盖前面的值，改为：temp[`${temp}`]
                     * eg：['SFZJHM', 'SJQQ', 'SJQZ']
                     * 变成：
                     *  SFZJHM: "SFZJHM"
                        SJQQ: "SJQQ"
                        SJQZ: "SJQZ"
                     */
                    temp[`${item}`] = item;
                })
                console.log(temp);
            }
        }
    }
}
