
import { Button, Combobox, Edit, Link, Checkbox, DataProvider } from "commons/basic";
import { Dialog } from "commons/dialog";
import { getCurrentUser, getMetaRepository, IMetaFileViewer, getFileImplClass } from "metadata/metadata";
import { showResourceDialog } from "metadata/metamgr";
import { getScheduleMgr, ScheduleEditor } from "schedule/schedulemgr";
import {
    showMenu, assign, Component, ComponentArgs, ctx, ctxIf, getBaseName, IImportComponentInfo, showWaiting, rc1, BASIC_EVENT,
    showWarningMessage, showWaitingMessage, isEmpty, message, rc, showDialog, showErrorMessage, SZEvent, showSuccessMessage, showFormDialog
} from "sys/sys";
import { IFieldsDataProvider } from "commons/exp/expdialog";
import { IExpDataProvider, ExpVar } from "commons/exp/expcompiler";
import { InterActionEvent } from "metadata/metadata-script-api";
import { TableCellEditor } from "commons/table";
import { Tree, TreeVisualRoot, TreeArgs, ListArgs, TreeItem, List, ListItem } from "commons/tree";
import { FAppViewlet } from "fapp/browser/fappviewlets";
import { Menu, MenuItem } from "commons/menu";
import { Form } from "commons/form";
import { getDwTableDataManager } from "dw/dwapi";
import { DwDataHierarchyProviderArgs, DwDataHierarchyProvider } from "dw/dwdps";
import { getDwTableChangeEvent } from "dw/dwapi";
import { SpgTree } from "app/superpage/components/tables";

/**
 * 自定义树控件
 * 20220512 tuzw
 * 1 在产品的树控件上增加右击点击菜单功能
 * 
 * 功能重写：
 * 1 可外部覆盖已有的右击点击事件 和 删除节点进行额外的操作
 * 
 * 扩展SPG可视化控件：
 * 1 扩展类必须继承与SpgXXXX（控件名）Component，eg：SpgTreeComponent
 * 2 不需要自己获取dp对象，其自带SpgTreePptDataProvider对象
 * 
 * 注意：
 * 1 树的最外层样式 被隐藏，暂时解决办法：全局样式覆盖原有的【高和宽】度
 */
export class Custom_Tree extends Tree {

    /** 自定义树控件绑定的DataProvider对象 */
    public customTreeDataProvater: CustomTreeDataProvater;
    /** 树节点渲染数据来自的模型表路径，eg：/sysdata/data/tables/sdi/data-standard/dims/DIM_STD_TYPE.tbl */
    public treeBindModelTablePath: string;
    /** 当前右击点击的树节点 */
    public rightOnClickNode: TreeItem;
    /** 是否隐藏右击点击菜单 */
    public isDisplayRightOnClickMenu: boolean;

    /** 自定义删除事件，默认为：NULL */
    public onDeleteEvent?: (item: ListItem) => void;
    /** 自定义右击事件 */
    public onCustomTreeMenu?: (sze: SZEvent, list: List, item: ListItem) => void | boolean;

    public constructor(args) {
        super(args);
        this.onDeleteEvent = null;
        this.onCustomTreeMenu = null;
        this.isDisplayRightOnClickMenu = false;
    }

    /**
	 * _init_default 先 _init方法执行
     * args 可获取外部（spg）传入的参数
     */
    protected _init_default(args: TreeArgs): void {
        args.id = "custom_tree_base_id"; // 给当前自定义树定义dom ID值
        this.treeBindModelTablePath = args.treeBindModelTablePath;
        if (!this.treeBindModelTablePath) {
            showWarningMessage(`树未绑定模型，检查传递参数`);
            return;
        }

        this.addEventListeners();
        this.setTreeDataProvider(this.treeBindModelTablePath); // 改成全局获取，则无法权限访问：downloadDimData方法
        args.dataProvider = this.customTreeDataProvater;
        super._init_default(args);
    }

    protected _init(args: TreeArgs): HTMLElement {
        this.oncontextmenu = this.doTreeContextMenu.bind(this); // 覆盖原有的右击菜单事件
        const domBase = super._init(args);
        return domBase;
    }
	
	protected _init_data(args: TreeArgs): Promise<void> { // 注意：push 和 pushAll
		args.items = [{ // 若未绑定模型，则初始静态数据
            "caption": "父节点1",
            "checked": false,
            "className": "custom_Tree-row",
            "id": "110000",
            "value": "110000",
            "items": [{
                "caption": "子节点",
                "checked": false,
                "className": "custom_Tree-row",
                "id": "110001",
                "value": "110001",
                "level": 1
            }]
        }, {
            "caption": "父节点2",
            "checked": false,
            "className": "custom_Tree-row",
            "id": "110002",
            "value": "110002",
            "level": 0
        }];
		return <Promise<void>>super._init_data(args, true);
	}

    /**
     * 右击树 节点，弹出的可选菜单
     * @param treeItem 当前右击选中的节点
     */
    private doTreeContextMenu(sze: SZEvent, list: List, treeItem: TreeItem): void | boolean {
        if (this.onCustomTreeMenu == null) { // 校验外部是否有自定义右击菜单事件
            if (treeItem != null) {
                this.setRightOnclickTreeNode(treeItem);
                this.generatorDefaultRightNodeMenu(sze);
            } else {
                this.generatorRightTreeMenu(sze);
            }
        } else {
            this.onCustomTreeMenu(sze, list, treeItem);
        }
        return false; // 隐藏浏览器自带的右击菜单选项
    }

    /**
     * 新增同级节点
     * @param treeItem 当前选择的节点
     */
    public addEqualTreeNode(event): void {
        this.showAdjustTreeNodeDialog({
            "optionType": "addEqualNode",
            "optionCaption": "新增同级节点"
        });
    }

    /**
     * 新增下级节点
     * @param treeItem 当前选择的节点
     */
    public addChildeTreeNode(event): void {
        this.showAdjustTreeNodeDialog({
            "optionType": "addChildeNode",
            "optionCaption": "新增下级节点"
        });
    }

    /**
     * 修改树节点
     * @param treeItem 当前选择的节点
     */
    public updateTreeNode(event): void {
        this.showAdjustTreeNodeDialog({
            "optionType": "updateNode",
            "optionCaption": "修改节点"
        });
    }

    /**
     * 删除树节点
     * @param treeItem 当前选择的节点
     */
    public removeTreeNode(event): void {
        this.saveDimItem({
            id: this.rightOnClickNode.getId(),
            modelPath: this.treeBindModelTablePath,
            option: "delete"
        }).then((result) => {
            if (result.result && this.onDeleteEvent != null) { // 校验外部是否有指定自定义删除事件
                this.onDeleteEvent(this.rightOnClickNode);
            }
        });
    }

    /**
     * 记录当前右击点击的树节点
     * @param treeItem 当前右击选择的节点
     */
    public setRightOnclickTreeNode(treeItem: TreeItem): void {
        this.rightOnClickNode = treeItem;
    }

    /**
     * 展示调整树节点对话框页面
     * @param optionCaption 当前操作标题，eg：新增同级节点
     * @param optionType 操作类型，eg：updateNode
     */
    public showAdjustTreeNodeDialog(args: {
        optionCaption: string,
        optionType: string
    }): void {
        let optionType: string = args.optionType;
        let dialogCaption: string = args.optionCaption;

        let ownerTree = this; // 保留tree对象
        showFormDialog({
             // id: "customTree-Dialog-id", // 不指定ID，否则对话框会缓存，导致读取的optionType一直为第一次打开对话框的值
            className: "adjustTreeNode",
            caption: dialogCaption,
            content: {
                captionWidth: 72,
                items: [{
                    id: "treeNodeId",
                    caption: "代码",
                    formItemType: 'edit',
                    required: true,
                    validRegex: ((inputText: string) => { // 获取当前输入框输入的值
                        let nodeIsExist: boolean = this.checkNodeIdIsExist(inputText, ownerTree);
                        if (nodeIsExist && optionType == "updateNode") { // 若节点存在，并且当前操作为修改节点，进一步校验当前ID是否有发生变化
                            if (!!this.rightOnClickNode && this.rightOnClickNode.getId() == inputText) {
                                return { result: true };
                            }
                        }
                        if (nodeIsExist) {
                            return { result: false, errorMessage: "节点已存在" }
                        }
                        return { result: true };
                    })
                }, {
                    id: "treeNodeCaption",
                    caption: "标题",
                    formItemType: 'edit',
                    required: true
                }]
            },
            onshow: (event: SZEvent, dialog: Dialog) => {
                let form = <Form>dialog.content;
                form.waitInit().then(() => {
                    let treeNodeIdComp = form.getFormItemComp('treeNodeId');
                    let treeNodeCaptionComp = form.getFormItemComp('treeNodeCaption');
                    if (optionType == "updateNode") { // 输入框显示当前选中内容
                        treeNodeIdComp.setValue(this.rightOnClickNode.getId());
                        treeNodeCaptionComp.setValue(this.rightOnClickNode.getCaption());
                    }
                });
            },
            buttons: [{
                id: "ok",
                layoutTheme: "defbtn",
                onclick: (event: SZEvent, dialog: Dialog, item: any) => {
                    let form: Form = dialog.content;
                    this.treeNodeMenuFunction(optionType, form, this.rightOnClickNode);
                }
            }, 'cancel']
        });
    }

    /**
     * 校验当前节点 是否 存在树节点中
     * 
     * 注意：SPG扩展组件此处实现不一样，它使用的DP对象为：SpgTreePptDataProvider，需要重写
     * 
     * @param nodeId 校验的节点ID值
     * @param ownerTree 树对象本身，因为有时候脱离上下文，导致this丢失，这里根据情况传入
     * return true | false
     */
    public checkNodeIdIsExist(nodeId: string, ownerTree?: Custom_Tree): boolean {
        if (!!this) { // 校验this是否存在
            ownerTree = this;
        }
        let treeAllNode: TreeItem[] = ownerTree.dataProvider.getTreeAllItems();
        /** 校验当前输入的节点ID是否存在 */
        let nodeIsExist: boolean = false;
        for (let i = 0; i < treeAllNode.length; i++) {
            let treeNode: TreeItem = treeAllNode[i];
            if (nodeId == treeNode.getId()) {
                nodeIsExist = true;
                break;
            }
        }
        return nodeIsExist;
    }

    /**
     * 右击弹出菜单功能实现、
     * 实现：
     * （1）方法一：直接将数据写入库，重新fetchData数据，渲染树
     * （2）方法二：添加dom方法，不适合，eg：
     * let treeItem = new TreeItem(this, {
            "caption": treeNodeCaption,
            "checked": false,
            "className": "custom_Tree-row",
            "id": treeNodeId,
            "value": treeNodeId
        });
     * @param optionType 操作类型，eg：updateNode
     * @param form 右击菜单表单对象
     * @param rightChoseTreeNode 当前右击树节点对象
     */
    private treeNodeMenuFunction(optionType: string, form: Form, rightChoseTreeNode: TreeItem): void {
        let validate: boolean = form.validate();
        if (validate) {
            let data = form.getData();
            let treeNodeId: string = data['treeNodeId'];
            let treeNodeCaption: string = data['treeNodeCaption'];

            switch (optionType) {
                case "addEqualNode":  // 新增同级节点
                    let params = {
                        id: treeNodeId,
                        modelPath: this.treeBindModelTablePath,
                        caption: treeNodeCaption,
                        option: "insert"
                    }
                    if (!!rightChoseTreeNode.getParent().getId()) { // 若当前是子节点新增同级节点，则指定父节点ID
                        params.parentId = rightChoseTreeNode.getParent().getId();
                    }
                    this.saveDimItem(params);
                    break;
                case "addChildeNode": // 新增下级节点，当前节点为父节点
                    this.saveDimItem({
                        id: treeNodeId,
                        modelPath: this.treeBindModelTablePath,
                        caption: treeNodeCaption,
                        option: "insert",
                        parentId: rightChoseTreeNode.getId()
                    });
                    break;
                case "updateNode": // 更新节点
                    this.saveDimItem({
                        id: treeNodeId,
                        modelPath: this.treeBindModelTablePath,
                        caption: treeNodeCaption,
                        option: "update",
                        originalId: rightChoseTreeNode.getId()
                    });
                    break;
                default:
                    showWarningMessage(`当前操作不存在`);
            }
        }
    }

    /**
     * 调整树节点（新增、修改、删除）
     * @param id 维项代码（主键值）
     * @param modelPath 树绑定的模型路径
     * @param option 当前操作，delete insert update
     * @param caption 节点绑定的文字字段值
     * @param originalId 修改维项代码时原来的代码
     * @param parentId  父节点的ID，用于在某节点下新增子节点
     * @param timePoint  时间点，用于修改缓慢变化的维项
     * @param data 维项的数据
     */
    private saveDimItem(args: {
        id: string,
        modelPath: string;
        option: string;
        caption?: string;
        originalId?: string;
        parentId?: string;
        timePoint?: string;
        data?: {
            [fieldName: string]: string;
        }
    }): Promise<any> {
        return rc({
            url: '/api/customTree/adjustTreeNode',
            data: args
        }).then((result) => {
            if (!result.result) {
                showWarningMessage(result.message);
                return result;
            }
            if (!!result.message) { // 部分成功时，也有提示信息
                showWarningMessage(result.message);
            }
            getDwTableDataManager().updateCache([getDwTableChangeEvent(args.modelPath, { updateAll: true })], true);
            return result;
        });
    }

    /**
     * 生成默认-右击节点-菜单
     * @param sze 
     */
    public generatorDefaultRightNodeMenu(sze: SZEvent): void {
        showMenu({
            id: 'custom-tree-combobox-treeNode-menu',
            items: [{
                id: 'addEqualTreeNode',
                cmd: "addEqualTreeNode",
                caption: "新增同级节点"
            }, {
                id: 'addChildeTreeNode',
                cmd: "addChildeTreeNode",
                caption: "新增下级节点"
            }, {
                id: 'updateTreeNode',
                cmd: "updateTreeNode",
                caption: "修改节点"
            }, {
                id: 'deleteNode',
                cmd: "removeTreeNode",
                caption: "删除节点"
            }],
            // onclick: (event: SZEvent, menu: Menu, menuItem: MenuItem) => {
            //     let menuItemId: string = menuItem.id;
            //     this.choseMenuFunction(menuItemId, treeItem);
            // },
            showAt: <MouseEvent>sze.srcEvent,
            layoutTheme: "button-load",
            /** 命令状态提供者，提供菜单项的数据和处理点击事件，会执行item设置的cmd对应的方法，注意：id和cmd值一致 */
            commandProvider: this
        });
    }

    /**
     * 生成 默认右击-树-空白区域右击菜单
     * @param sze 
     */
    public generatorRightTreeMenu(sze: SZEvent): void {
        showMenu({
            id: 'custom-tree-combobox-tree-menu',
            items: [{
                id: 'addEqualTreeNode',
                cmd: "addEqualTreeNode",
                caption: "新增同级节点"
            }],
            showAt: <MouseEvent>sze.srcEvent,
            layoutTheme: "button-load",
            commandProvider: this
        });
    }

    /**
     * 获取自定义树控件绑定的DataProvider对象
     */
    private setTreeDataProvider(bindModelPath: string): void {
        if (this.customTreeDataProvater == null) {
            this.customTreeDataProvater = new CustomTreeDataProvater({
                dwTablePath: bindModelPath
            });
        }
    }

    /**
     * 给树添加 重新刷新监听事件
     */
    private addEventListeners(): void {
        getDwTableDataManager().getEventListeners().on(BASIC_EVENT.datachange, () => { // 给树添加监听 刷新事件
            this.refresh();
        });
    }
}

/** 右击菜单选项 */
interface Right_Menu_item {
    /** 选项Dom id值 */
    id: string;
    /** 点击触发的方法名，与id值保持一致 */
    cmd: string;
    /** 选项中文描述 */
    caption: string;
}

/**
 * 自定义树控件绑定的DataProvider对象
 * 功能：用于获取渲染树的数据
 * 注意：
 * （1）若模型设置了层级，内部会自己读取设置好的层级来渲染目录树
 */
class CustomTreeDataProvater extends DataProvider<any> {

    /** 绑定的模型路径 */
    public bindModelPath: string;
    /** 层级dp对象，查询树节点数据 */
    public dwDataHierarchyProvider: DwDataHierarchyProvider;

    constructor(args) {
        super(args);
        this.bindModelPath = args.dwTablePath;
    }

    /**
     * 构建树控件绑定的模型dataProvider对象
     */
    private getDwDataHierarchyProvider(): Promise<DwDataHierarchyProvider> {
        return getDwTableDataManager().getDwDataHierarchyProvider(this.getFilterDwProviderArgs()).then(dataProvider => {
            this.dwDataHierarchyProvider = dataProvider;
            return dataProvider;
        });
    }

    /**
     * 初始化DataProvider对象参数
     */
    public getFilterDwProviderArgs(): DwDataHierarchyProviderArgs {
        let dataProviderArgs: DwDataHierarchyProviderArgs = {
            dwTablePath: this.bindModelPath
        }
        return dataProviderArgs;
    }

    /**
     * @override 
     * 重写fetchData方法，每次新增、删除、修改节点时，重写获取表最新数据
     */
    public fetchData(args?: { [propName: string]: any; startIndex?: number; fetchCount?: number; parentData?: DwTableDataItem; }): Promise<DwTableDataItem[]> {
        return this.getDwDataHierarchyProvider().then((dp) => {
            return dp.fetchData(args).then((result) => {
                return result;
            });
        })
    }

    /**
     * 获得当前树节点所有节点信息
     * @return 
     */
    public getTreeAllItems(): DwTableDataItem[] {
        if (!this.dwDataHierarchyProvider) {
            return [];
        }
        /**
         * 由于itemMap格式为：001: DwTableDataItem，这里将其格式转换，去掉key
         */
        let itemMap = this.dwDataHierarchyProvider.originalDwDataHierarcy.itemMap;
        let itemMapKeys = Object.keys(itemMap);
        let dwTableDataItems: DwTableDataItem[] = [];
        for (let i = 0; i < itemMapKeys.length; i++) {
            let item: DwTableDataItem = itemMap[`${itemMapKeys[i]}`];
            dwTableDataItems.push(item);
        }
        return dwTableDataItems;
    }
}