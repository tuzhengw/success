/**
 * 作者：tuzw、dingy、yuey、lijingx
 * 审核员: liuyz
 * 创建时间：2022-12-01
 * 脚本用途：数据中台分模块备份及恢复功能
 * 总贴：https://jira.succez.com/browse/CSTM-21398
 * 
 * 实现：类似VS code 同步功能
 * 导出的压缩包结构，根据顶层文件 递归创建导出结构
 * 
 * 导出模块（磁盘文件）：
 * 1 zt.app 、 data 、extensions、theme
 * 2 必导出模块: 
 * (1) zt.app/common
 * (2) zt.app/setting.json
 * (3) zt.app/data（导出前需要丁煜那边获取最新维表数据）
 * (4) zt.app/API
 * (5) data/tables/sdi2/common
 * 
 * 3 恢复
 * (1) 若是指定恢复模块, zt.app/data 恢复维表内部过滤
 * 
 * 
 * 导出流程：
 * 1) 导出根据外部参数控制, 未传，则默认备份所有（SPG、脚本、维表、模型、脚本模型（脚本+模型）、主题样式等）
 * 2) 部分导出：eg：导出数据交换，仅导出数据交换模块以及它的父模块 以及通用模块
 * 3) 导出的压缩包结构见帖子（动态递归读取zt.app的文件结构）
 * 
 * 参考：
 * 1) 走产品流程，元数据导出压缩包功能
 * 
 * 恢复实现（读取磁盘上的文件, 可通过根据产品提供uploadFile，返回一个文件ID，来读取（丁瑜）内容）：
 * 1）恢复元数据内容和对应metaFile文件
 * 
 * 注意点：
 * 1) 备份和恢复包含一个隐藏文件: meta
 */
import { SDILog, LogType, ResultInfo } from "/sysdata/app/zt.app/commons/commons.action";
import db from "svr-api/db"
import utils from "svr-api/utils";
import bean from "svr-api/bean";
import metadata from 'svr-api/metadata';
import fs from "svr-api/fs";
import sys from "svr-api/sys";
import security from "svr-api/security";
import dw from "svr-api/dw";

import JavaFile = java.io.File;
import ZipOutputStream = java.util.zip.ZipOutputStream;
import ZipEntry = java.util.zip.ZipEntry;
import ArrayUtils = org.apache.commons.lang3.ArrayUtils
import FileOutputStream = java.io.FileOutputStream;
import FileInputStream = java.io.FileInputStream;
import IOUtils = org.apache.commons.io.IOUtils;
import ZipFile = java.util.zip.ZipFile;
import StandardCharsets = java.nio.charset.StandardCharsets;
import JavaString = java.lang.String;
import FileUtils = org.apache.commons.io.FileUtils;
import ZipInputStream = java.util.zip.ZipInputStream;
import BufferedInputStream = java.io.BufferedInputStream;
import ByteArrayOutputStream = java.io.ByteArrayOutputStream;
import StringUtils = org.apache.commons.lang3.StringUtils;
import Byte = java.lang.Byte;

import MetaFileContentImpl = com.succez.metadata.service.impl.entity.MetaFileContentImpl;

let logger: SDILog = new SDILog({ name: "sdi.sysMgr" });
const Timestamp = Java.type('java.sql.Timestamp');

function main() {
    // let path = "/sysdata/app/zt.app/images/commons/logo.png";
    // let f = metadata.getFile(path).getContentAsInputStream();
    // print(f.asBytes());
    // f.close();
    // return;

    // let filePath = `${sys.getClusterShareDir()}/file-storage/relativePath/`;
    // let file = fs.getFile(filePath);
    // print(file.listFiles())
    // print(file.delete());
    // print(fs.deleteFile(filePath))
    // return;

    // let shareDir: string = sys.getClusterShareDir();
    // let attachParentDir: string = `${shareDir}/file-storage/relativePath`;
    // let filePath: string = `${attachParentDir}/sdi_2.0_20221206110133.zip`;
    // let f = new JavaFile(filePath);
    // print(f.delete());
    // return;

    // metadata.modifyFile("/sysdata/extensions/commons/commons", {
    //     desc: "我"
    // });
    // metadata.createFile({
    //     path: "/sysdata/app/tuzw.app",
    //     type: "app",
    //     name: "tuzw"
    // });
    // let content = {
    //     "version": "4.15.0"
    // };
    // metadata.createFile({
    //     path: "/sysdata/app/tuzw.app/settings.json",
    //     type: "json",
    //     name: "tuzw",
    //     content: JSON.stringify(content)
    // });
    // return;

    // let sdiBackupModuleInfos = querySdiAllModuleInfo();
    let restoreMgr = new SdiRestoreMetaMgr({
        backupZipInfo: {
            FILE_SAVE_PATH: "D:/success/workspace4.0/workdir/clusters-share/file-storage/relativePath/sdi_2.0_20221208155857.zip",
            BACKUP_ATTACH_ID: "4",
        },
        // signRestoreModules: ["data"],
        logger: logger
    });
    print(restoreMgr.restoreSDI());
    return;

    let mgr = new SdiModuleBackupMgr({
        backupSetIds: [2],
        logger: logger
    });
    mgr.executeAutoBackup();
    return;

    let meta = new SdiBackupMetaMgr({
        sdiBackupModuleInfos: sdiBackupModuleInfos,
        signBackModules: ["data-asset"],
        logger: logger
    });
    let backupResult = meta.backupSDI();
    console.info(backupResult);
}

/**
 * 获取磁盘的一个文件内容, 并将其写入到环境
 * <p>
 *  1 直接获取字符串来修改文件, 无法处理图片
 *   IOUtils.toString(zipFile.getInputStream(zipFile.getEntry(zipEntryName)), StandardCharsets.UTF_8); // 获取文件内容()
 *  2 通过产品对外提供的元数据仓库对象, 调用其创建方法修改元数据文件
 *   metaServiceRepository.forceCreateOrUpdateFile(路径, 内容, 文件属性配置)：
 *   (1) 可将内存字节流写入到元数据文件中,
 *   (2) 文件属性配置中的ID若在当前环境已存在, 则会忽略配置文件的ID，随机生成一个新的ID
 * </p>
 */
function test(): void {
    let project = "/sysdata/script/Slice 23.png";
    let metaServiceRepository = bean.getBean("com.succez.metadata.service.MetaServiceRepository");

    let path: string = "D:/success/workspace4.0/workdir/clusters-share/file-storage/relativePath/Slice 23.zip";
    let backupFile = new JavaFile(path);
    let zipEntry;
    let zipInputStream: ZipInputStream = null;
    /** byte数组的缓冲区 */
    let byteArrayOutputStream: ByteArrayOutputStream = null;
    try {
        zipInputStream = new ZipInputStream(new BufferedInputStream(new FileInputStream(backupFile)));
        byteArrayOutputStream = new ByteArrayOutputStream(8 * 1024);
        let zipFile = new ZipFile(backupFile);

        let buf: number[] = [new Byte(8 * 1024)];
        while ((zipEntry = zipInputStream.getNextEntry()) != null) {
            let zipEntryName: string = zipEntry.getName();
            let isFolder = StringUtils.endsWith(zipEntryName, "/");
            if (isFolder) {
                continue;
            }
            if (zipEntryName != "Slice 23.png") {
                continue;
            }
            byteArrayOutputStream.reset(); // 重置此流(即，它将删除此流中所有当前消耗的输出，并将变量计数重置为0)
            IOUtils.copyLarge(zipFile.getInputStream(zipEntry), byteArrayOutputStream, buf);
            let newContent = new MetaFileContentImpl(byteArrayOutputStream.toByteArray(), false);
            metaServiceRepository.forceCreateOrUpdateFile(project, newContent, { // 若文件不存在, 内部会自己创建
                "id": "WPiahojlyFLMCJqISUrhtD", "creator": "system", "createTime": 1670340112567, "modifier": "system", "modifyTime": 1670374767616, "option": {}, "customOption": {}
            });
        }
    } catch (e) {
        logger.addLog(`恢复项目失败----error`);
        logger.addLog(e);
    } finally {
        zipInputStream && zipInputStream.close();
        byteArrayOutputStream && byteArrayOutputStream.close();
    }
}

/**
 * 备份管理类
 * <p>
 *  1 备份元数据信息：根据外部传入的备份配置表信息, 循环依次根据配置进行备份
 *  2 删除失效备份包: 自动读取所有失效包进行删除备份压缩包和表数据
 *  3 自动执行备份: 自动读取所有设置自动备份的配置信息, 循环依次备份
 * </p>
 */
export class SdiModuleBackupMgr {

    /** 备份信息模型路径 */
    private BACKUP_INFO_MODEL_PATH = "/sysdata/data/tables/sdi2/sys-mgr/sdiMetaMgr/SYS_SDI_MODULE_BACKUP_INFO.tbl";

    /** 备份配置ID */
    private backupSetIds: string[] | number[];
    /** 日志对象 */
    private logger: SDILog;

    /** 备份配置信息 */
    private backupSetInfos: SysBackupSetMgr[];
    /** 备份实现类 */
    private sdiBackupMetaMgr: SdiBackupMetaMgr;
    /** 默认数据源对象 */
    private defaultDb: Datasource;

    constructor(args: {
        backupSetIds?: string[] | number[],
        logger: SDILog
    }) {
        this.backupSetIds = !!args.backupSetIds ? args.backupSetIds : [];
        this.logger = args.logger;
        this.defaultDb = db.getDefaultDataSource();

        this.queryAllBackupSetInfo();
        this.sdiBackupMetaMgr = new SdiBackupMetaMgr({
            logger: this.logger,
            sdiBackupModuleInfos: querySdiAllModuleInfo()
        });
    }

    /**
     * 根据传入的指定配置ID, 依次进行中台元数据模块备份
     * <p>
     *  1 根据传入要备份的配置ID, 与当前环境所有备份配置信息进行匹配
     *  2 仅执行匹配成功的备份配置 
     * </p>
     */
    public executeBackup(): ResultInfo {
        if (!this.backupSetIds || this.backupSetIds.length == 0) {
            return { result: false, message: "未指定备份配置ID" };
        }
        this.logger.addLog(`开始执行[executeBackup], 备份中台元数据`);

        let backupInfos: SysSdiModuleBackupInfo[] = [];
        let backupSetInfos = this.backupSetInfos;
        for (let i = 0; i < backupSetInfos.length; i++) {
            let backupSetInfo: SysBackupSetMgr = backupSetInfos[i];
            if (!this.backupSetIds.includes(backupSetInfo.SET_ID)) { // 判断当前备份配置是否为指定备份ID
                continue;
            }
            let backupResult: ResultInfo = this.singalSetBackup(backupSetInfo);
            if (backupResult.result) {
                backupInfos.push(backupResult.data);
            }
        }
        let insertResult: ResultInfo = this.insertBackupInfos(backupInfos);
        this.logger.addLog(`执行结束[executeBackup], 备份中台元数据`);
        return insertResult;
    }

    /**
     * 执行设置自动备份
     * <p>
     *  1 读取当前备份配置表所有数据, 筛选出所有设有自动备份的配置信息
     *  2 依次处理自动备份的配置
     * </p>
     * @return
     */
    public executeAutoBackup(): ResultInfo {
        this.logger.addLog(`开始执行[executeAutoBackup], 执行设置自动备份`);
        let backupInfos: SysSdiModuleBackupInfo[] = [];
        let backupSetInfos = this.backupSetInfos;
        for (let i = 0; i < backupSetInfos.length; i++) {
            let backupSetInfo: SysBackupSetMgr = backupSetInfos[i];
            if (backupSetInfo.AUTO_BACKUP == "0") {
                continue;
            }
            let backupResult: ResultInfo = this.singalSetBackup(backupSetInfo);
            if (backupResult.result) {
                backupInfos.push(backupResult.data);
            }
        }
        let insertResult: ResultInfo = this.insertBackupInfos(backupInfos);
        this.logger.addLog(`执行结束[executeAutoBackup], 执行设置自动备份`);
        return insertResult;
    }

    /**
     * 重新设置备份配置ID
     * @param backupSetIds 备份配置ID
     */
    public setBackupSetIds(backupSetIds: string[]): void {
        this.backupSetIds = backupSetIds;
    }

    /**
     * 根据指定配置表信息进行备份
     * @param backupSetInfo 备份配置信息
     * @return 
     */
    private singalSetBackup(backupSetInfo: SysBackupSetMgr): ResultInfo {
        let setId: string = backupSetInfo.SET_ID;
        this.logger.addLog(`开始依据备份配置表: ${setId} 信息进行备份`);
        this.logger.addLog(backupSetInfo);

        let backupModules: string[] = backupSetInfo.BACKUP_MODULES;
        let keepDay: number = backupSetInfo.KEEP_DAYS;
        this.sdiBackupMetaMgr.setBackupModules(backupModules);
        this.sdiBackupMetaMgr.setBackupKeepDay(keepDay);

        let backupResult: ResultInfo = this.sdiBackupMetaMgr.backupSDI();
        if (!backupResult.result) {
            this.logger.addLog(`备份配置: ${setId} 执行备份失败`);
            return backupResult;
        }
        this.logger.addLog(`备份配置表: ${setId} 备份执行完成`);
        return backupResult;
    }

    /**
     * 处理当前环境所有失效的备份包
     */
    public dealAllInvalidPackage(): ResultInfo {
        this.logger.addLog(`开始执行[deleteAllInvalidPackage], 删除当前环境所有失效的备份包`);
        /** 已失效的所有备份包信息 */
        let allInvaildPackageInfos: string[][] = this.queryInvalidPackageInfos();
        let invaildNums: number = allInvaildPackageInfos.length;

        this.deleteBackupInfos(allInvaildPackageInfos)

        for (let i = 0; i < invaildNums; i++) {
            let invaildInfo: string[] = allInvaildPackageInfos[i];
            let backupId = invaildInfo[0];
            let backPath: string = invaildInfo[1]; // eg: ../clusters-share/file-storage/relativePath/sdi_2.0_20221206110133.zip
            if (!backupId || !backPath) {
                continue;
            }
            this.logger.addLog(`${i + 1} 开始删除备份包: ${backPath}`);
            let backupFile = new JavaFile(backPath);
            let backupFileDelete: boolean = true;
            if (backupFile.exists()) {
                backupFileDelete = backupFile.delete();
            }
            let dotMetaFileDelete: boolean = true;
            let backupDotMetaFile = new JavaFile(`${backPath}.meta`);
            if (backupDotMetaFile.exists()) {
                dotMetaFileDelete = backupDotMetaFile.delete();
            }
            if (backupFileDelete && dotMetaFileDelete) {
                this.logger.addLog(`备份包: ${backupId} 压缩包文件删除失败`);
            }
        }
        this.logger.addLog(`执行结束[deleteAllInvalidPackage], 删除当前环境所有失效的备份包`);
        return;
    }

    /**
     * 查询当前环境所有失效备份包
     * @return eg: [
     *   ["001", ".../file-storage/relativePath/sdi_2.0_20221206110133.zip"]
     * ]
     */
    private queryInvalidPackageInfos(): string[][] {
        let allInvaildPackageInfos: string[][] = [];
        let queryInfo: QueryInfo = {
            sources: [{
                id: "model1",
                path: this.BACKUP_INFO_MODEL_PATH
            }],
            fields: [{
                name: "BACKUP_ID", exp: "model1.BACKUP_ID"
            }, {
                name: "FILE_SAVE_PATH", exp: "model1.FILE_SAVE_PATH"
            }, {
                name: "KEEP_DAYS", exp: "model1.KEEP_DAYS"
            }, {
                name: "CREATE_TIME", exp: "model1.CREATE_TIME"
            }],
            filter: [{
                exp: `model1.BACKUP_STATUS = 'SUCCESS'`
            }]
        }
        let queryData = dw.queryData(queryInfo).data;
        let oneDay: number = 24 * 60 * 60 * 1000;
        let now = Date.now() / oneDay;
        for (let i = 0; i < queryData.length; i++) {
            let data = queryData[i] as string[];
            let keepDays: number = data[2];
            let backupTime = data[3].getTime() / oneDay; // 父类不能向子类转换，先将时间戳转换为日期
            if (now - backupTime > keepDays) { // 若当前时间-备份时间 > 备份包保留天数, 则视为失效
                let invalidInfo: string[] = [data[0], data[1]];
                allInvaildPackageInfos.push(invalidInfo);
            }
        }
        this.logger.addLog(`当前执行时间为: [${utils.formatDate("yyyy-MM-dd HH:mm:ss")}], 有: ${allInvaildPackageInfos.length} 个失效的备份包`);
        this.logger.addLog(allInvaildPackageInfos);
        return allInvaildPackageInfos;
    }

    /**
     * 删除备份数据
     * @param backupInfos 备份信息
     * @return 
     */
    private deleteBackupInfos(backupInfos: string[][]): ResultInfo {
        if (!backupInfos || backupInfos.length == 0) {
            return { result: false, message: "没有需要删除的备份数据" };
        }
        let resultInfo: ResultInfo = { result: true };
        let deleteKey: string[][] = [];
        for (let i = 0; i < backupInfos.length; i++) {
            let backupInfo: string[] = backupInfos[i];
            deleteKey.push([backupInfo[0]]);
        }
        try {
            let dataPackage: CommonTableSubmitDataPackage = {
                deleteRows: {
                    keyNames: ['BACKUP_ID'],
                    keyRows: deleteKey
                }
            }
            dw.updateDwTableData(this.BACKUP_INFO_MODEL_PATH, dataPackage);
            this.logger.addLog(`成功删除失效备份数据: ${deleteKey.length} 条`);
            this.logger.addLog(deleteKey);
        } catch (e) {
            resultInfo.result = false;
            resultInfo.message = "删除备份数据失败";
            this.logger.addLog(`删除备份数据失败----error`);
            this.logger.addLog(e);
        }
        return resultInfo;
    }

    /**
     * 写入备份信息
     * @param backupInfos 备份信息
     * @return 
     * 
     * <p>
     *  1 本地环境dw无法写入附件内容, eg: workdir:sdi_2.0_20221206164504.zip
     * </p>
     */
    private insertBackupInfos(backupInfos: SysSdiModuleBackupInfo[]): ResultInfo {
        if (!backupInfos || backupInfos.length == 0) {
            return { result: false, message: "没有要写入的备份数据" };
        }
        let resultInfo: ResultInfo = { result: true };
        // let addRows: FieldValue[][] = [];

        // let backupTableFields: string[] = ["BACKUP_MODULE_ID", "BACKUP_MODULE_NAME", "BACKUP_TIME", "BACKUP_ATTACH_ID", "FILE_SAVE_PATH", "BACKUP_STATUS", "BACKUP_MESSAGE", "KEEP_DAYS"];
        // for(let i = 0; i < backupInfos.length; i++) {
        //     let backupInfo: SysSdiModuleBackupInfo = backupInfos[i];
        //     let addRow: string[] = [];
        //     for(let fieldIndex = 0; fieldIndex < backupTableFields.length; fieldIndex++) {
        //         let fieldName: string = backupTableFields[fieldIndex];
        //         addRow.push(backupInfo[fieldName]);
        //     }
        //     addRows.push(addRow);
        // }
        try {
            // let dataPackage: CommonTableSubmitDataPackage = {
            //     addRows: [{
            //         fieldNames: backupTableFields,
            //         rows: addRows
            //     }]
            // }
            // dw.updateDwTableData(this.BACKUP_INFO_MODEL_PATH, dataPackage);
            // this.logger.addLog(`成功写入${addRows.length} 条备份数据`);

            let table = this.defaultDb.openTableData("SDI_2_PROJECT_BACKUP_INFO");
            table.insert(backupInfos);
        } catch (e) {
            resultInfo.result = false;
            resultInfo.message = "数据库写入备份表信息失败";
            this.logger.addLog(`写入备份信息失败----error`);
            this.logger.addLog(e);
        }
        return resultInfo;
    }

    /**
     * 查询当前环境所有的备份设置管理
     */
    private queryAllBackupSetInfo(): void {
        this.backupSetInfos = [];
        let queryInfo: QueryInfo = {
            sources: [{
                id: "model1",
                path: "/sysdata/data/tables/sdi2/sys-mgr/sdiMetaMgr/SYS_BACKUP_PAGEAGE_INFO.tbl"
            }],
            fields: [{
                name: "SET_ID", exp: "model1.SET_ID"
            }, {
                name: "BACKUP_MODULES", exp: "model1.BACKUP_MODULES"
            }, {
                name: "AUTO_BACKUP", exp: "model1.AUTO_BACKUP"
            }, {
                name: "BACKUP_SCHEDULE", exp: "model1.BACKUP_SCHEDULE"
            }, {
                name: "KEEP_DAYS", exp: "model1.KEEP_DAYS"
            }]
        }
        let queryData = dw.queryData(queryInfo).data;
        for (let i = 0; i < queryData.length; i++) {
            let data = queryData[i] as string[];
            let backupModules: string = data[1];
            this.backupSetInfos.push({
                SET_ID: data[0],
                BACKUP_MODULES: !!backupModules ? backupModules.split(",") : [],
                AUTO_BACKUP: data[2],
                BACKUP_SCHEDULE: data[3],
                KEEP_DAYS: data[4]
            });
        }
        this.logger.addLog(`当前环境所有的备份配置信息(${this.backupSetInfos.length} 条): `);
        this.logger.addLog(this.backupSetInfos);
    }
}

/**
 * 备份数据中台元数据功能
 * <p>
 *  备份中台zt.app、data、extensions、thems四个模块内容
 *  实现思路:
 *  1) 参考产品备份逻辑, 使用第三方压缩包类：ZipEntry将文件写入到压缩包中
 *  2) 处理文件夹时,在文件夹内创建一个.meta文件, 将文件夹首层的文件信息记录到.meta中
 *  3) 备份的压缩包存放在附件目录下, 最后备份完成后, 在附件目录下创建一个.meta文件, 记录备份压缩包的信息, 用于页面下载
 * </p>
 */
export class SdiBackupMetaMgr {

    /** 元数据服务工厂类 */
    private metaServiceRepository;
    /** 元数据服务内容处理类 */
    private metaServiceExportImpl;

    /** 指定备份模块名, 未指定默认备份全部, eg: ['data-access', 'extensions'] */
    private signBackModules: string[];
    /** 备份文件保留天数, 默认: 30天 */
    private keepDays: number;
    /** 
     * 所有备份模块信息(仅用于存储备份信息, 实际备份还是根据目录动态读取), {
     *    "data-access": "数据接入"
     * }
     */
    private sdiBackupModuleInfos: JSONObject;

    /** 属于文件夹类型 */
    private FoldTypes: string[] = ["fold", "app", "project"];
    /** 备份进度对象 */
    private backupProgress: ProgressMonitor;
    /** 日志对象 */
    private logger: SDILog;

    constructor(args: {
        sdiBackupModuleInfos: JSONObject,
        keepDays?: number,
        signBackModules?: string[],
        logger: SDILog
    }) {
        this.sdiBackupModuleInfos = args.sdiBackupModuleInfos;
        this.keepDays = !!args.keepDays ? args.keepDays : 30;
        this.signBackModules = !!args.signBackModules ? args.signBackModules : [];
        this.logger = args.logger;

        this.backupProgress = sys.getThreadProcessMonitor();
        this.metaServiceRepository = bean.getBean("com.succez.metadata.service.MetaServiceRepository");
        this.metaServiceExportImpl = bean.getBean("com.succez.metadata.service.impl.MetaServiceExportImpl");
    }

    /**
     * 预检查
     */
    private preCheck(): ResultInfo {
        if (!this.sdiBackupModuleInfos) {
            return { result: false, message: "没有要备份的模块" };
        }
        return { result: true };
    }

    /**
     * 备份项目
     * 
     * <p>
     * 1 压缩包本身是一个空盒子, 可通过添加条目写入文件夹和文件, eg: 
     *    new ZipEntry("ztApp/text.txt"), 压缩包内创建一个text.txt文件, 并创建对应的父目录(ztApp作为压缩包第一层文件)
     * 2 由于指定备份模块和必导模块, 每个meta存储文件数量不一致, 最外层模块改为外部处理
     * </p>
     */
    public backupSDI(): ResultInfo {
        let preCheck = this.preCheck();
        if (!preCheck.result) {
            return preCheck;
        }
        this.backupProgress.setMaximum(100);
        this.backupProgress.setProgress(0);

        this.logger.addLog(`开始执行[backupProject], 备份项目`);
        this.logger.addLog(`指定备份: [${this.signBackModules.join(",")}], 为空则备份全部`);

        let resultInfo: ResultInfo = { result: true };
        let backStartTime: number = Date.now();

        let { backupPath, backupName } = this.getBackupFilePath();
        let attachName: string = `workdir:${backupName}`;
        let backupFile: JavaFile = this.createBackupFile(backupPath);

        let zipOutputStream: ZipOutputStream = null;
        try {
            zipOutputStream = new ZipOutputStream(new FileOutputStream(backupFile)); // FileOutputStream 压缩包会自己关闭

            let backupResult: ResultInfo = this.backupZtAppModule(zipOutputStream);
            if (backupResult.result) {
                backupResult = this.backupTablesModelModule(zipOutputStream);
            }
            if (backupResult.result) {
                backupResult = this.backupExtensionExpModule(zipOutputStream);
            }
            if (backupResult.result) {
                backupResult = this.backupSdiThemesModule(zipOutputStream);
            }
            let backupTime: number = Math.ceil((Date.now() - backStartTime) / 1000);
            if (backupResult.result) {
                this.logger.addLog(`备份成功`);
                this.createAttchDotMetaFile(backupPath, attachName, backupFile.length());
                resultInfo.data = this.createBackupInfo(backupTime, backupResult, backupPath, attachName);
            } else {
                this.logger.addLog(`备份失败, ${backupResult.message}, 压缩包删除状态: ${backupFile.delete()}`);
                resultInfo.data = this.createBackupInfo(backupTime, backupResult, backupPath);
            }
        } catch (e) {
            resultInfo.result = false;
            resultInfo.message = `备份失败, ${e.toString()}`;
            this.logger.addLog(`备份失败, 压缩包删除状态: ${backupFile.delete()}-- error`);  // 压缩失败则删除备份文件
            this.logger.addLog(e.toString());
        } finally {
            try {
                if (zipOutputStream != null) {
                    zipOutputStream.closeEntry();
                    zipOutputStream.close();
                }
            } catch (e) {
                this.logger.addLog(`文件流关闭异常----error`);
                this.logger.addLog(e.toString());
                this.logger.addLog(e);
            }
        }
        this.logger.addLog(`备份结束, 共耗时: ${(Date.now() - backStartTime) / 1000} s`);
        this.backupProgress.setProgress(100);
        return resultInfo;
    }

    /**
     * 重新设置需要备份的模块
     * @param backModules 备份模块, 为空则备份全部, eg: ["data-access"]
     */
    public setBackupModules(backModules: string[]): void {
        if (!backModules) {
            return;
        }
        this.signBackModules = backModules;
    }

    /**
     * 重新设置备份包有效天数
     * @param keepDays 保持天数
     */
    public setBackupKeepDay(keepDays: number): void {
        if (!keepDays) {
            return;
        }
        this.keepDays = keepDays;
    }

    /**
     * 创建配置文件(压缩包名.meta)以供附件预览时读取文件
     * @param filePath 文件所存路径, eg: /afa/ata/text.xml
     * @param fileName 文件名
     * @param fileSize 文件大小
     * 
     * <p>
     *  1 存储文件的附件信息（文件名、文件大小、文件路径）
     * </p>
     */
    private createAttchDotMetaFile(filePath: string, fileName: string, fileSize: number): void {
        let metaFilePath: string = `${filePath}.meta`; // meta名为：对应的文件名.meta
        let dotMetaFile = new java.io.File(metaFilePath);
        if (!dotMetaFile.exists()) {
            dotMetaFile.createNewFile();
        }
        dotMetaFile = fs.getFile(metaFilePath);
        let zipMetaFileInfo = {
            name: fileName,
            size: fileSize,
            lastModifyTime: new Timestamp(Date.now())
        }
        this.logger.addLog(`当前备份文件附件对应的.meta内容: `);
        this.logger.addLog(zipMetaFileInfo);
        dotMetaFile.writeJson(zipMetaFileInfo);
    }

    /**
     * 构建当前备份的信息
     * @param backupTime 备份耗时
     * @param backupResult 备份结果
     * @param attachId 附件名, eg: workdir:sdi.zip
     * @Param backupFilePath 备份压缩包存储磁盘路径
     */
    private createBackupInfo(
        backupTime: number,
        backupResult: ResultInfo,
        backupFilePath: string,
        attachId?: string
    ): SysSdiModuleBackupInfo {
        let backupModuleIds: string[] = [];
        let backupModuleDescs: string[] = [];

        let backupModules: string[] = Object.keys(this.sdiBackupModuleInfos);
        if (this.signBackModules.length != 0) {
            backupModules = this.signBackModules;
        }
        for (let i = 0; i < backupModules.length; i++) {
            let moduleName: string = backupModules[i];
            let moduleDesc: string = this.sdiBackupModuleInfos[moduleName];
            backupModuleIds.push(moduleName);
            backupModuleDescs.push(moduleDesc);
        }
        let backupInfo: SysSdiModuleBackupInfo = {
            BACKUP_MODULE_ID: backupModuleIds.join(","),
            BACKUP_MODULE_NAME: backupModuleDescs.join(","),
            BACKUP_TIME: backupTime,
            BACKUP_ATTACH_ID: attachId as string,
            FILE_SAVE_PATH: backupFilePath,
            BACKUP_STATUS: backupResult.result ? "SUCCESS" : "FAIL",
            BACKUP_MESSAGE: backupResult.message,
            KEEP_DAYS: this.keepDays,
            CREATOR: security.getCurrentUser().userInfo.userId,
            CREATE_TIME: new Timestamp(Date.now())
        }
        return backupInfo;
    }

    /**
     * 备份zt.app下元数据信息
     * @param zipOutputStream 导出压缩文件对象
     * @return 
     */
    private backupZtAppModule(zipOutputStream: ZipOutputStream): ResultInfo {
        this.logger.addLog(`开始执行[backupZtAppModule], 备份zt.app下元数据信息`);

        let ztAppZipPath: string = BackupPackageMainDirName.SdiApp;
        let ztAppEntry = new ZipEntry(`${ztAppZipPath}/`);
        zipOutputStream.putNextEntry(ztAppEntry); // 将创建好的条目加入到压缩文件中

        let ztAppProjectPath: string = SdiProjectMainModule.ZtAppPath;
        /** zt.app下必导出模块名 */
        let mustBackupModules: string[] = ["images", "commons", "initialize", "API", "data"];

        if (this.signBackModules.length != 0) {
            mustBackupModules.pushAll(this.signBackModules); // 若指定了备份模块, 将其放入到必导出模块内
            this.createDotMetaFile(zipOutputStream, `${ztAppZipPath}/.meta`, ztAppProjectPath, mustBackupModules);
        } else {
            this.createDotMetaFile(zipOutputStream, `${ztAppZipPath}/.meta`, ztAppProjectPath);
        }
        this.backupProgress.step(30);
        return this.backupMetaFiles(zipOutputStream, ztAppProjectPath, ztAppZipPath, mustBackupModules);
    }

    /**
     * 备份data下模型元数据信息
     * @param zipOutputStream 导出压缩文件对象
     * <p>
     * 1 若是脚本模型, 对应批处理脚本也要备份
     * </p>
     */
    private backupTablesModelModule(zipOutputStream: ZipOutputStream): ResultInfo {
        this.logger.addLog(`开始执行[backupTablesModelModule], 备份中台data模块`);
        this.backupProgress.step(40);

        let sdiDataZipPath: string = BackupPackageMainDirName.SdiData;
        let sdiDataEntry = new ZipEntry(`${sdiDataZipPath}/`);
        zipOutputStream.putNextEntry(sdiDataEntry);
        zipOutputStream.closeEntry();

        let ztDataProjectPath: string = SdiProjectMainModule.SdiDataModulePath;
        this.createDotMetaFile(zipOutputStream, `${sdiDataZipPath}/.meta`, ztDataProjectPath, ["batch", "script", "tables"]);

        let resultInfo = this.backupBatchModule(zipOutputStream, ztDataProjectPath, sdiDataZipPath);
        if (!resultInfo.result) {
            return resultInfo;
        }
        resultInfo = this.backScriptModule(zipOutputStream, ztDataProjectPath, sdiDataZipPath);
        if (!resultInfo.result) {
            return resultInfo;
        }
        resultInfo = this.backSdiTableModule(zipOutputStream, ztDataProjectPath, sdiDataZipPath);
        if (!resultInfo.result) {
            return resultInfo;
        }
        return { result: true };
    }

    /**
     * 备份中台数据-批处理模块(batch)
     * @param zipOutputStream 导出压缩文件对象
     * @param ztDataProjectPath 中台项目数据目录路径, eg: /data
     * @param sdiDataZipPath 数据模块在压缩包的位置, eg: data
     * @return 
     */
    private backupBatchModule(zipOutputStream: ZipOutputStream, ztDataProjectPath: string, sdiDataZipPath: string): ResultInfo {
        if (this.signBackModules.length == 0) { // 仅备份全部时进行备份
            this.logger.addLog(`开始执行[backupBatchModule], 备份中台数据-批处理模块`);

            let batchZipPath: string = `${sdiDataZipPath}/batch`;
            let batchEntry = new ZipEntry(`${batchZipPath}/`);
            zipOutputStream.putNextEntry(batchEntry);
            zipOutputStream.closeEntry();

            let batchProjectPath: string = `${ztDataProjectPath}/batch`;
            let foldChildrenFilePaths: string[] = this.getDirChildrenFilePaths(batchProjectPath, "SYS_");

            let createMetaFileNames: string[] = this.getFirstLevelChildrenName(batchProjectPath, "SYS_");
            this.createDotMetaFile(zipOutputStream, `${batchZipPath}/.meta`, batchProjectPath, createMetaFileNames);

            return this.backupMetaFile(zipOutputStream, batchProjectPath, foldChildrenFilePaths, batchZipPath);
        }
        this.backupProgress.step(50);
        return { result: true };
    }

    /**
     * 备份中台数据——脚本模块(script)
     * @param zipOutputStream 导出压缩文件对象
     * @param ztDataProjectPath 中台项目数据目录路径, eg: /data
     * @param sdiDataZipPath 数据模块在压缩包的位置, eg: data
     * @return 
     */
    private backScriptModule(zipOutputStream: ZipOutputStream, ztDataProjectPath: string, sdiDataZipPath: string): ResultInfo {
        this.logger.addLog(`开始执行[backScriptModule], 备份中台数据——脚本模块`);

        let scriptZipPath: string = `${sdiDataZipPath}/script`;
        let batchEntry = new ZipEntry(`${scriptZipPath}/`);
        zipOutputStream.putNextEntry(batchEntry);
        zipOutputStream.closeEntry();
        this.createDotMetaFile(zipOutputStream, `${scriptZipPath}/.meta`, `${ztDataProjectPath}/script`, ["sdi"]);

        let sdiScriptZipPath: string = `${sdiDataZipPath}/script/sdi`;
        batchEntry = new ZipEntry(`${sdiScriptZipPath}/`);
        zipOutputStream.putNextEntry(batchEntry);
        zipOutputStream.closeEntry();
        this.createDotMetaFile(zipOutputStream, `${sdiScriptZipPath}/.meta`, `${ztDataProjectPath}/script/sdi`, this.signBackModules);

        this.backupProgress.step(60);
        let sdiScriptProjectPath: string = `${ztDataProjectPath}/script/sdi`;
        return this.backupMetaFiles(zipOutputStream, sdiScriptProjectPath, sdiScriptZipPath);
    }

    /**
     * 备份中台数据——表模型模块(sdi | sdi2)
     * @param zipOutputStream 导出压缩文件对象
     * @param ztDataProjectPath 中台项目数据目录路径, eg: /data
     * @param sdiDataZipPath 数据模块在压缩包的位置, eg: data
     */
    private backSdiTableModule(zipOutputStream: ZipOutputStream, ztDataProjectPath: string, sdiDataZipPath: string): ResultInfo {
        this.logger.addLog(`开始执行[backSdiTableModule], 备份中台数据——表模型模块`);
        let backupResult: ResultInfo = { result: true };

        let tableModelProjectPath: string = `${ztDataProjectPath}/tables`;
        let tableModelZipPath: string = `${sdiDataZipPath}/tables`;
        let tableModelEntry = new ZipEntry(`${tableModelZipPath}/`);
        zipOutputStream.putNextEntry(tableModelEntry);
        zipOutputStream.closeEntry();

        let sdiTableDirInfos: JSONObject = {
            "sdi": {
                mustBackupModules: ["dims"],
                ignoreModules: []
            },
            "sdi2": {
                mustBackupModules: ["dim", "dims", "commons"],
                ignoreModules: ["etl", "query"] // 忽略备份模块, 但.meta文件不可忽略
            },
            "sdi2/etl": {
                mustBackupModules: [],
                ignoreModules: []
            },
            "sdi2/query": {
                mustBackupModules: ["commons"],
                ignoreModules: []
            }
        };
        let sdiTableDirs: string[] = Object.keys(sdiTableDirInfos);

        for (let i = 0; i < sdiTableDirs.length; i++) {
            let dirName: string = sdiTableDirs[i];

            let dirProjectPath: string = `${tableModelProjectPath}/${dirName}`;
            let dirZipPath: string = `${tableModelZipPath}/${dirName}`;

            let dirEntry = new ZipEntry(`${dirZipPath}/`);
            zipOutputStream.putNextEntry(dirEntry);
            zipOutputStream.closeEntry();

            let mustBackupModules: string[] = sdiTableDirInfos[dirName]['mustBackupModules'];
            let ignoreModules: string[] = sdiTableDirInfos[dirName]['ignoreModules'];

            if (this.signBackModules.length != 0) {
                mustBackupModules.pushAll(this.signBackModules);
                this.createDotMetaFile(zipOutputStream, `${dirZipPath}/.meta`, dirProjectPath, mustBackupModules);
            } else {
                this.createDotMetaFile(zipOutputStream, `${dirZipPath}/.meta`, dirProjectPath);
            }
            this.backupMetaFiles(zipOutputStream, dirProjectPath, dirZipPath, mustBackupModules, ignoreModules);
        }
        this.backupProgress.step(70);
        return backupResult;
    }

    /**
     * 备份扩展表达式元数据信息
     * @param zipOutputStream 导出压缩文件对象
     * @return 
     */
    private backupExtensionExpModule(zipOutputStream: ZipOutputStream): ResultInfo {
        if (this.signBackModules.length != 0 && !this.signBackModules.includes(BackupPackageMainDirName.SdiExtensions)) {
            this.logger.addLog(`----指定备份模块不包含: 扩展表达式, 忽略`);
            return { result: true };
        }
        this.logger.addLog(`开始执行[backupExtensionExpModule], 备份扩展表达式元数据信息`);
        /** 扩展表达式压缩包存放位置 */
        let extensionModuleZipPath: string = BackupPackageMainDirName.SdiExtensions;
        let extensionEntry = new ZipEntry(`${extensionModuleZipPath}/`);
        zipOutputStream.putNextEntry(extensionEntry);
        zipOutputStream.closeEntry();

        let extensionRootPath: string = SdiProjectMainModule.SdiExtensionsPath;
        let foldChildrenFilePaths: string[] = this.getDirChildrenFilePaths(extensionRootPath);
        this.createDotMetaFile(zipOutputStream, `${extensionModuleZipPath}/.meta`, extensionRootPath);

        this.backupProgress.step(80);
        return this.backupMetaFile(zipOutputStream, extensionRootPath, foldChildrenFilePaths, extensionModuleZipPath);
    }

    /**
     * 备份中台样式元数据信息
     * @param zipOutputStream 导出压缩文件对象
     * @return 
     */
    private backupSdiThemesModule(zipOutputStream: ZipOutputStream): ResultInfo {
        if (this.signBackModules.length != 0 && !this.signBackModules.includes(BackupPackageMainDirName.SdiThemes)) {
            this.logger.addLog(`----指定备份模块不包含: 中台样式, 忽略`);
            return { result: true };
        }
        this.logger.addLog(`开始执行[backupSdiThemesModule], 备份中台样式元数据信息`);
        /** 中台样式压缩包存放位置 */
        let themesModuleZipPath: string = BackupPackageMainDirName.SdiThemes;

        let themeEntry = new ZipEntry(`${themesModuleZipPath}/`);
        zipOutputStream.putNextEntry(themeEntry);
        zipOutputStream.closeEntry();

        let sdiStyleRootPath: string = SdiProjectMainModule.SdiThemesPath;
        let foldChildrenFilePaths: string[] = this.getDirChildrenFilePaths(sdiStyleRootPath);
        this.createDotMetaFile(zipOutputStream, `${themesModuleZipPath}/.meta`, sdiStyleRootPath);

        this.backupProgress.step(90);
        return this.backupMetaFile(zipOutputStream, sdiStyleRootPath, foldChildrenFilePaths, themesModuleZipPath);
    }

    /**
     * 备份指定目录下所有文件和文件夹
     * @param zipOutputStream 导出压缩文件对象
     * @param projectDir 备份目录路径, eg: /sysdata/data/script/sdi
     * @param dirZipPath 备份目录在压缩包的路径, eg: data/script/sdi
     * @param mustBackupModules 首层必备份模块名, eg: ["data-access", "custom.ts"]
     * @param ignoreModules 忽略的备份模块名, eg: ["data-standard", "custom.ts"]
     * @return 
     */
    private backupMetaFiles(
        zipOutputStream: ZipOutputStream,
        projectDir: string,
        dirZipPath: string,
        mustBackupModules?: string[],
        ignoreModules?: string[]
    ): ResultInfo {
        this.logger.addLog(`开始备份: ${projectDir}`);
        let backupResult: ResultInfo = { result: true };

        if (!mustBackupModules) {
            mustBackupModules = []; // 为空则备份全部
        }
        if (!ignoreModules) {
            ignoreModules = [];
        }
        let backupModules: string[] = this.getFirstLevelChildrenName(projectDir);
        for (let i = 0; i < backupModules.length; i++) {
            let backupModule: string = backupModules[i];
            if (ignoreModules.includes(backupModule)) {
                continue;
            }
            if (!mustBackupModules.includes(backupModule)
                && (this.signBackModules.length != 0 && !this.signBackModules.includes(backupModule))) {
                continue; // 若指定了备份模块, 若模块既不是：必备份, 又不是指定备份模块, 则越过
            }
            let moduleProjectPath: string = `${projectDir}/${backupModule}`;
            let metaFile: MetaFileInfo = metadata.getFile(moduleProjectPath);

            if (this.FoldTypes.includes(metaFile.getType())) {
                let foldChildrenFilePaths: string[] = this.getDirChildrenFilePaths(moduleProjectPath);
                backupResult = this.backupMetaFile(zipOutputStream, projectDir, foldChildrenFilePaths, dirZipPath, backupModule);
            } else {
                backupResult = this.writeFileToEntry(zipOutputStream, `${dirZipPath}/${backupModule}`, metaFile);
            }
            if (!backupResult.result) {
                this.logger.addLog(`模块[${backupModule}] 备份失败, 执行结束`);
                break;
            }
        }
        return backupResult;
    }

    /**
     * 备份单个文件夹模块
     * 
     * @param zipOutputStream 导出压缩文件对象 
     * @param rootPath 备份文件夹所在根目录路径, eg: /sysdata/app/zt.app
     * @param foldChildrenFilePaths 备份文件夹内所有需要备份的子文件(文件夹和文件)路径, eg: ['/sysdata/extensions/commons']
     * @param backupModuleParentPath 当前备份文件夹在压缩包的父路径, eg: zt.app
     * @param foldName? 备份文件夹名(不传则默认根目录[rootPath], 并将备份文件放到根目录下[backupModuleParentPath]), eg: data-access 
     */
    private backupMetaFile(
        zipOutputStream: ZipOutputStream,
        rootPath: string,
        foldChildrenFilePaths: string[],
        backupModuleParentPath: string,
        foldName?: string
    ): ResultInfo {
        let resultInfo: ResultInfo = { result: true };
        let moduleProjectPath: string = rootPath;
        if (!!foldName) {
            moduleProjectPath = `${rootPath}/${foldName}`;
        }
        this.logger.addLog(`开始备份, 模块[${moduleProjectPath}]`);

        let backFile = metadata.getFile(moduleProjectPath);
        if (backFile == null) {
            return { result: false, message: `当前备份模块不存在: ${moduleProjectPath}` };
        }
        let backFileType = backFile.type as string;
        if (!this.FoldTypes.includes(backFileType)) { // 判断当前模块是否文件夹
            return { result: false, message: `模块: ${moduleProjectPath} 不是文件夹, 执行结束` };
        }
        let backupFilePaths: string[] = foldChildrenFilePaths;

        /** 备份模块压缩包位置 */
        let moduleZipPath: string = `${backupModuleParentPath}/`;
        if (!!foldName) { // 父模块外部已写入压缩包 
            moduleZipPath = `${backupModuleParentPath}/${foldName}/`;
            let ztAppEntry = new ZipEntry(moduleZipPath);
            zipOutputStream.putNextEntry(ztAppEntry);
            zipOutputStream.closeEntry();

            this.createDotMetaFile(zipOutputStream, `${moduleZipPath}.meta`, moduleProjectPath);
        }
        for (let i = 0; i < backupFilePaths.length; i++) {
            let metaPath: string = backupFilePaths[i];
            let metaFile: MetaFileInfo = metadata.getFile(metaPath);
            if (metaFile == null) {
                continue;
            }
            let metaFileType = metaFile.type as string;
            /** 文件存放压缩包位置, (去除根目录路径, 将剩余路径作为条目写入压缩包, eg: zt.app/data-access/) */
            let zipEntryPath: string = `${backupModuleParentPath}/${metaPath.replace(`${rootPath}/`, "")}`;
            /**
             * 这里是根据路径依次处理的, 目录是不会出现重复的
             * 1) 首次处理目录, 创建一个.meta文件, 记录当前目录第一层子文件的信息
             */
            if (this.FoldTypes.includes(metaFileType)) {
                let ztAppEntry = new ZipEntry(`${zipEntryPath}/`);
                zipOutputStream.putNextEntry(ztAppEntry);
                zipOutputStream.closeEntry();

                this.createDotMetaFile(zipOutputStream, `${zipEntryPath}/.meta`, metaPath);
            } else {
                let singalFileResult: ResultInfo = this.writeFileToEntry(zipOutputStream, zipEntryPath, metaFile);
                if (!singalFileResult.result) { // 有一个文件压缩失败, 则结束
                    resultInfo.result = false;
                    resultInfo.message = singalFileResult.message;
                    break;
                }
            }
        }
        this.logger.addLog(`备份完成, 模块[${moduleProjectPath}]`);
        return resultInfo;
    }

    /**
     * 将元数据信息写入到压缩包对应的条目中
     * @param zipOutputStream 导出压缩文件对象
     * @param zipEntryPath 文件在压缩包位置, eg: zt.app/data-access/test.ts
     * @param metaFile 元数据对象
     * 
     * <p>
     *  1 文件写入产品使用的编码为: utf-8, 使用方式将文件内容转换为JAVA字符串.getBytes(MetaConst.UTF8)
     * </p>
     */
    private writeFileToEntry(zipOutputStream: ZipOutputStream, zipEntryPath: string, metaFile: MetaFileInfo): ResultInfo {
        let resultInfo: ResultInfo = { result: true };
        /**
         * 将创建好的条目加入到压缩文件中后就可开始往当前创建文件（条目）内写入内容（压缩包往最后创建的条目中写入）。
         * 若文件已存在, 则覆盖已有的内容。
         */
        let ztAppEntry = new ZipEntry(zipEntryPath);
        zipOutputStream.putNextEntry(ztAppEntry);
        if (metaFile.getContentAsString() == null) { // 判断文件内容是否为空
            return { result: true };
        }
        let input: InputStream = null;
        try {
            input = metaFile.getContentAsInputStream();
            IOUtils.copy(input, zipOutputStream);
        } catch (e) {
            resultInfo.result = false;
            resultInfo.message = `元数据信息: [ ${metaFile.name}] 写入到压缩包对应的条目中失败`;
            this.logger.addLog(`writeFileToEntry执行失败, 当前处理的元数据为: ${metaFile.name}`);
            this.logger.addLog(e);
        } finally {
            input && input.close();
        }
        zipOutputStream.closeEntry();
        return resultInfo;
    }

    /**
     * 创建一个.meta文件, 记录指定目录第一层文件和文件夹信息,并将文件放置压缩包对应层级
     * @param zipOutputStream
     * @param zipEntryPath 创建的.meta存放压缩包位置, eg: zt.app/data-access/.meta
     * @param dirPath 目录路径, eg: "/sysdata/app"
     * @param recordFileNames 指定记录的(文件夹|文件)名, 为空, 则记得全部文件, eg: ['data-access', "custom.ts", "custom.js"]
     * @param ignoreFileNames 忽略文件, eg: ['data-access', "custom.ts", "custom.js"]
     * 
     * <p>
     *  .meta内容, eg: {
            "sdi": {
                "id": "eq6btkEQf5Dh6oNiwUo2uC",
                "desc": "数据中台",
                "type": "project",
                "creator": "admin",
                "createTime": 1650795543077
            },
            "sysdata": {
                "id": "SM2mWVuRjXMUzZxQJhaF7E",
                "desc": "系统数据",
                "type": "project"
            }
        }
     * </p>
     */
    private createDotMetaFile(
        zipOutputStream: ZipOutputStream,
        zipEntryPath: string,
        dirPath: string,
        recordFileNames?: string[],
        ignoreFileNames?: string[]
    ): void {
        let file = this.metaServiceRepository.getFile(dirPath, false);
        if (file == null || !this.FoldTypes.includes(file.getType())) {
            return;
        }
        if (!recordFileNames) {
            recordFileNames = [];
        }
        if (!ignoreFileNames) {
            ignoreFileNames = [];
        }
        let metaFile: JSONObject = {};
        let metaFiles = this.metaServiceRepository.listFiles(dirPath, false);
        for (let i = 0; i < metaFiles.size(); i++) {
            let file = metaFiles.get(i);
            let fileName: string = file.getName();
            if (recordFileNames.length != 0 && !recordFileNames.includes(fileName)) {
                continue; // 若指定了记录的文件, 则判断当前文件是否在指定文件中, 不在则越过
            }
            if (ignoreFileNames.includes(fileName)) {
                continue;
            }
            let dotMetaFilePropertiest: DotMetaFileProperties = {
                id: file.getId(),
                name: file.getName(),
                path: file.getPath(),
                type: file.getType(),
                icon: file.getIcon(),
                desc: file.getDesc(),
                comment: file.getComment(),
                creator: file.getCreator(),
                createTime: file.getCreateTime(),
                modifier: file.getModifier(),
                modifyTime: file.getModifyTime(),
                folder: file.isFolder(),
                hidden: file.isHidden(),
                order: file.getOrder(),
                subFileType: file.getSubFileType(),
                state: file.getState(),
                option: file.getOption(),
                customOption: file.getCustomOption()
            }
            metaFile[fileName] = dotMetaFilePropertiest;
        }
        let metaStrContent = new JavaString(JSON.stringify(metaFile));
        let entry = new ZipEntry(zipEntryPath);
        zipOutputStream.putNextEntry(entry);
        let fileBytes: number[] = metaStrContent.getBytes(StandardCharsets.UTF_8);
        zipOutputStream.write(fileBytes);
        zipOutputStream.closeEntry();
    }

    /**
     * 获取根目录下指定文件夹内的子文件路径
     * @param rootPath 根目录路径, eg: /sysdata/app/zt.app, eg: /sysdata/app/zt.app/system-mgr/systemMgr
     * @param selectFilePrefix? 搜索文件前缀
     * @return eg: ["/sysdata/app/zt.app/system-mgr/systemMgr.action.ts", ...]
     * 
     * <p>
     *  1 metadata.searchFiles查询出来的路径, 可能存在部分路径中含有双斜杆, eg: extensions/commons//commons
     * </p>
     */
    public getDirChildrenFilePaths(parentDir: string, selectFilePrefix?: string): string[] {
        let file = metadata.getFile(parentDir);
        if (file == null) {
            return [];
        }
        let fileType = file.type as string;
        if (!this.FoldTypes.includes(fileType)) {
            return [parentDir];
        }
        let childrenFiles: MetaFileInfo[] = metadata.searchFiles({
            parentDir: parentDir,
            recur: true
        });
        let fileSize: number = childrenFiles.length;
        this.logger.addLog(`目录: [${parentDir}] 有: [${fileSize}] 个子文件`);

        let childrenFilePaths: string[] = [];
        for (let fileIndex = 0; fileIndex < fileSize; fileIndex++) {
            let metaFile: MetaFileInfo = childrenFiles[fileIndex];
            let filePath: string = metaFile.path;
            let fileType = metaFile.type as string;
            let fileName: string = metaFile.name;
            if (!this.FoldTypes.includes(fileType)
                && (!!selectFilePrefix && fileName.indexOf(selectFilePrefix) != 0)) {
                continue; // // 判断当前文件是否为指定前缀开头, 若不是则越过
            }
            filePath = filePath.replaceAll("//", "/");
            childrenFilePaths.push(filePath);
        }
        return childrenFilePaths;
    }

    /**
     * 获取指定目录下首层文件和文件夹名
     * @param dirPath 目录路径, eg: /sysdata/app/zt.app
     * @param selectFilePrefix? 搜索文件前缀
     * @return 
     */
    private getFirstLevelChildrenName(dirPath: string, selectFilePrefix?: string): string[] {
        let file = metadata.getFile(dirPath);
        if (file == null) {
            return [];
        }
        let fileType = file.type as string;
        if (!this.FoldTypes.includes(fileType)) {
            return [];
        }
        let childrenFileNames: string[] = [];
        let metaFiles = this.metaServiceRepository.listFiles(dirPath, false);
        for (let i = 0; i < metaFiles.size(); i++) {
            let file = metaFiles.get(i);
            let fileName: string = file.getName();
            if (!!selectFilePrefix && fileName.indexOf(selectFilePrefix) != 0) {
                continue; // // 判断当前文件是否为指定前缀开头, 若不是则越过
            }
            childrenFileNames.push(file.getName());
        }
        this.logger.addLog(`目录: ${dirPath} 第一层存在: ${childrenFileNames.length} 个子文件`);
        this.logger.addLog(childrenFileNames.join(","));
        return childrenFileNames;
    }

    /**
     * 创建备份文件
     * @param backupFilePath 备份文件路径
     * @return 
     */
    private createBackupFile(backupFilePath: string): JavaFile {
        let backupFile = new JavaFile(backupFilePath);
        let parentFile: JavaFile = backupFile.getParentFile();
        if (!parentFile.exists()) {
            parentFile.mkdirs(); // 递归创建所有不存在的父级目录
        }
        if (!backupFile.exists()) {
            backupFile.createNewFile();
        }
        this.logger.addLog(`备份文件创建完成, 开始压缩文件内容`);
        return backupFile;
    }

    /**
     * 获取磁盘备份的文件路径
     * <p> 
     * 1) 备份包名为: 当前时间_数据中台项目模块备份.zip
     * 2) 备份包存储路径为附件路径（将备份包作为附件存储）
     * </p>
     */
    private getBackupFilePath(): { backupPath: string, backupName: string } {
        let shareDir: string = sys.getClusterShareDir();
        let attachParentDir: string = `${shareDir}/file-storage/relativePath`;
        let backupName: string = `sdi_2.0_${utils.formatDate("yyyyMMddHHmmss")}.zip`;
        let backupPath: string = `${attachParentDir}/${backupName}`;
        this.logger.addLog(`当前备份包路径: ${backupPath}`);
        return { backupPath: backupPath, backupName: backupName };
    }
}

/**
 * 恢复中台元数据功能
 * 参考：https://wsbi.succez.com/ynsjztwh/HLHT/app/shareWord.app?:edit=true&:file=xmlPaser.action.ts
 * <p>
 *  1 功能: 获取备份包包含的模块信息
 *  构造参数: (1) 上传文件ID或者当前环境某个备份包信息; (2) 当前环境所有备份模块的信息
 *  2 功能: 恢复指定备份包或者上传包文件内容 
 *  构造参数: (1) 上传文件ID或者当前环境某个备份包信息;
 * </p>
 */
export class SdiRestoreMetaMgr {

    /** 指定恢复模块名, 未指定默认备份全部, eg: ['data-access', 'extensions'] */
    private signRestoreModules: string[];
    /** 外部上传压缩包文件ID */
    private uploadFileId: string;
    /** 内部备份包信息 */
    private backupZipInfo: SysSdiModuleBackupInfo;

    /** 
     * 所有备份模块信息(仅用于存储备份信息, 实际备份还是根据目录动态读取), {
     *    "data-access": "数据接入"
     * }
     */
    private sdiBackupModuleInfos: JSONObject;
    /**
     * 恢复包里面所有的.meta文件属性((外层key为文件在压缩包位置路径, 避免文件名重名问题), eg: {
     *      "zt.app/": {
     *          "id":"WPiahojlyFLMCJqISUrhtD",
     *          "creator":"system",
     *          "createTime":1670340112567,
     *          "modifier":"system",
     *          "modifyTime":1670374767616,
     *          "option":{},
     *          "customOption":{}
     *      },
     *      "zt.app/custom.ts": {
     *          ...
     *      }
     * }
     */
    private dotMetaFileProperties: JSONObject;
    /**
     * 分类恢复包各模块子路径, eg: {
     *     "zt.app": ["zt.app/data-access/", "zt.app/custom.ts", ...],
     *     "extensions": [...],
     *     ...
     * }
     */
    private restoreMetaFilePaths: JSONObject;

    /** 元数据服务工厂类 */
    private metaServiceRepository;
    /** 处理进度对象 */
    private progress: ProgressMonitor;
    /** 日志对象 */
    private logger: SDILog;

    constructor(args: {
        uploadFileId?: string,
        backupZipInfo?: SysSdiModuleBackupInfo,
        signRestoreModules?: string[],
        sdiBackupModuleInfos?: JSONObject,
        logger: SDILog
    }) {
        this.uploadFileId = args.uploadFileId as string;
        this.sdiBackupModuleInfos = !!args.sdiBackupModuleInfos ? args.sdiBackupModuleInfos : {};
        this.backupZipInfo = args.backupZipInfo as SysSdiModuleBackupInfo;
        this.signRestoreModules = !!args.signRestoreModules ? args.signRestoreModules : [];
        this.logger = args.logger;

        this.dotMetaFileProperties = {};
        this.restoreMetaFilePaths = {};
        this.metaServiceRepository = bean.getBean("com.succez.metadata.service.MetaServiceRepository");
        this.progress = sys.getThreadProcessMonitor();
    }

    /**
     * 参数预检查
     */
    private preCheck(): ResultInfo {
        if (!this.uploadFileId && !this.backupZipInfo) {
            return { result: false, message: "没有获取恢复的备份包信息" };
        }
        return { result: true };
    }

    /**
     * 获取压缩包备份的模块
     * <p> 
     *  1 模块名以zt.app目录下首层文件夹名为主, eg: zt.app/data-access/
     * </p> 
     * @return {
     *    "data-access": "数据接入"
     * }
     */
    public getBackupIncludeModules(): ResultInfo {
        let preCheckResult: ResultInfo = this.preCheck();
        if (!preCheckResult.result) {
            return preCheckResult;
        }
        if (!this.sdiBackupModuleInfos || Object.keys(this.sdiBackupModuleInfos).length == 0) {
            return { result: false, message: "未获取到备份模块信息" };
        }
        let backupFile: JavaFile = this.getRestoreFile();
        if (!backupFile.exists()) {
            return { result: false, message: "未获取到备份的文件信息" };
        }
        let resultInfo: ResultInfo = { result: true };

        let includeModuleInfos: JSONObject = {};
        let includeModuleNames: string[] = [];
        let moduleRootName: string = "zt.app/";

        let zipInputStream: ZipInputStream = null;
        let zipEntry: ZipEntry = null;
        try {
            zipInputStream = new ZipInputStream(new BufferedInputStream(new FileInputStream(backupFile)));
            while ((zipEntry = zipInputStream.getNextEntry()) != null) {
                let zipEntryName: string = zipEntry.getName(); // eg: zt.app/commons/ 或 zt.app/custom.ts
                if (!StringUtils.startsWith(zipEntryName, moduleRootName)) { // 越过不是zt.app目录下的文件和文件夹
                    continue;
                }
                zipEntryName = zipEntryName.replace(moduleRootName, ""); // 先去掉开头目录名
                let moduelName: string = zipEntryName.substring(0, zipEntryName.indexOf("/"));  // 获取首个斜杆前的内容
                if (!moduelName) {
                    continue;
                }
                if (!includeModuleNames.includes(moduelName)) {
                    includeModuleNames.push(moduelName);
                    includeModuleInfos[moduelName] = this.sdiBackupModuleInfos[moduelName];
                }
            }
            resultInfo.data = includeModuleInfos;

            this.logger.addLog(`当前恢复包包含模块信息为: `);
            this.logger.addLog(includeModuleInfos);
        } catch (e) {
            resultInfo.result = false;
            resultInfo.message = "恢复项目失败";
            this.logger.addLog(`恢复项目失败----error`);
            this.logger.addLog(e);
        } finally {
            zipInputStream && zipInputStream.close();
        }
        return resultInfo;
    }

    /**
     * 读取备份包内容, 恢复各个模块
     * <p>
     *  1 备份包可来自: 当前环境的备份包和上传的备份包(临时包, 根据上传的文件ID获取文件对象)
     *  
     *  2 print(this.dotMetaFileProperties["zt.app/settings.json"]); 初次需要创建app和对应的settings
     * </p>
     */
    public restoreSDI(): ResultInfo {
        let preCheckResult: ResultInfo = this.preCheck();
        if (!preCheckResult.result) {
            return preCheckResult;
        }
        let backupFile: JavaFile = this.getRestoreFile();
        if (backupFile == null || !backupFile.exists()) {
            return { result: false, message: "未获取到备份的文件信息" };
        }
        if (!backupFile.getName().endsWith(".zip")) {
            return { result: false, message: "恢复包不是压缩包文件" };
        }
        let startRestoreTime: number = Date.now();
        this.progress.setMaximum(100);
        this.progress.step(0);

        this.logger.addLog(`开始执行[restoreSDI], 读取备份包内容, 恢复各个模块`);
        this.logger.addLog(`当前指定恢复模块: [${this.signRestoreModules.join(",")}]`);
        let resultInfo: ResultInfo = { result: true };

        let zipInputStream: ZipInputStream = null;
        let byteArrayOutputStream: ByteArrayOutputStream = null;
        let buf: number[] = [new Byte(8 * 1024)];
        try {
            zipInputStream = new ZipInputStream(new BufferedInputStream(new FileInputStream(backupFile)));
            byteArrayOutputStream = new ByteArrayOutputStream(8 * 1024);
            let zipFile = new ZipFile(backupFile);
            resultInfo = this.loadZipMetaFileInfos(zipInputStream, zipFile);

            if (resultInfo.result) {
                this.restoreZtAppModule(zipFile, byteArrayOutputStream, buf);
                this.restoreDataModelModule(zipFile, byteArrayOutputStream, buf);
                this.restoreExtensionExpModule(zipFile, byteArrayOutputStream, buf);
                this.restoreSdiThemesModule(zipFile, byteArrayOutputStream, buf);
            }
        } catch (e) {
            resultInfo.result = false;
            resultInfo.message = "恢复项目失败";
            this.progress.addLog(`恢复项目失败----error`);
            this.progress.addLog(e);
        } finally {
            zipInputStream && zipInputStream.close();
            byteArrayOutputStream && byteArrayOutputStream.close();
        }
        this.logger.addLog(`执行结束[restoreSDI], 读取备份包内容, 恢复各个模块, 共耗时: ${(Date.now() - startRestoreTime) / 1000} s`);
        this.progress.step(100);
        return resultInfo;
    }

    /**
     * 恢复ztApp下元数据信息
     * @param zipFile 压缩包对象
     * @param byteArrayOutputStream byte数组的缓冲区
     * @param buf 每次读取字节大小
     * 
     * @return 
     */
    private restoreZtAppModule(zipFile: ZipFile, byteArrayOutputStream: ByteArrayOutputStream, buf: number[]): void {
        this.logger.addLog(`开始执行[restoreZtAppModule], 恢复ztApp下元数据信息`);
        /** zt.app下必导出模块名(恢复时都恢复) */
        let mustBackupModules: string[] = ["images", "commons", "initialize", "API", "data"];

        let zipZtAppPath: string = BackupPackageMainDirName.SdiApp;
        let ztAppRestorePaths: string[] = this.restoreMetaFilePaths[zipZtAppPath];

        if (this.signRestoreModules.length != 0) {
            mustBackupModules.pushAll(this.signRestoreModules); // 将指定恢复模块放入必恢复模块中
            let filterRestorePaths: string[] = this.filterRestorePaths(ztAppRestorePaths, zipZtAppPath, mustBackupModules);
            this.restoreMetaFiles(zipFile, byteArrayOutputStream, buf, filterRestorePaths);
        } else {
            this.restoreMetaFiles(zipFile, byteArrayOutputStream, buf, ztAppRestorePaths);
        }
        this.progress.step(50);
        this.logger.addLog(`执行结束[restoreZtAppModule], 恢复ztApp下元数据信息`);
    }

    /**
     * 恢复data下模型元数据信息
     * @param zipFile 压缩包对象
     * @param byteArrayOutputStream byte数组的缓冲区
     * @param buf 每次读取字节大小
     * 
     * <p>
     *  1 恢复data目录时要注意, 备份包只包含了data下的：batch、script、tables三个模块, 不能直接一次性恢复, 需一个一个模块恢复
     * </p>
     */
    private restoreDataModelModule(zipFile: ZipFile, byteArrayOutputStream: ByteArrayOutputStream, buf: number[]): void {
        this.logger.addLog(`开始执行[restoreDataModelModule], 恢复data下模型元数据信息`);
        let metaProperties: DotMetaFileProperties = {};

        let sdiDataProjectPath: string = SdiProjectMainModule.SdiDataModulePath;
        let dataZipPath: string = BackupPackageMainDirName.SdiData;
        let dataRestorePaths: string[] = this.restoreMetaFilePaths[dataZipPath];

        let signRestoreModules: string[] = this.signRestoreModules;
        if (signRestoreModules.length == 0) {
            let batchProjectRootPath: string = `${sdiDataProjectPath}/batch`;
            let batchZipPath: string = `${dataZipPath}/batch`;
            this.logger.addLog(`开始恢复[${batchProjectRootPath}]下批处理脚本文件(前缀SYS_`);
            let restoreBatchPaths: string[] = this.filterRestorePaths(dataRestorePaths, batchZipPath);
            this.restoreMetaFiles(zipFile, byteArrayOutputStream, buf, restoreBatchPaths);
        }

        let sdlScriptProjectPath: string = `${sdiDataProjectPath}/script/sdi`;
        this.logger.addLog(`开始恢复[${sdlScriptProjectPath}]下的中台脚本模型文件`);
        metaProperties = this.dotMetaFileProperties[`${dataZipPath}/script/sdi/`];
        let scriptFoldDesc: string = metaProperties != undefined && metaProperties.type != undefined ? metaProperties.type : "中台脚本模型";
        this.createFolder(sdlScriptProjectPath, "sdi", scriptFoldDesc);

        let scriptZipPath: string = `${dataZipPath}/script/sdi`;
        let restoreScriptPaths: string[] = this.filterRestorePaths(dataRestorePaths, scriptZipPath, signRestoreModules);
        this.restoreMetaFiles(zipFile, byteArrayOutputStream, buf, restoreScriptPaths);


        this.restoreSdiTableInfo(zipFile, byteArrayOutputStream, buf);
        this.progress.step(50);
        this.logger.addLog(`执行结束[restoreDataModelModule], 恢复data下模型元数据信息`);
    }

    /**
     * 恢复sysdata/tables下的sdi和sdi2模块下的模型文件
     * @param zipFile 压缩包对象
     * @param byteArrayOutputStream byte数组的缓冲区
     * @param buf 每次读取字节大小
     */
    private restoreSdiTableInfo(zipFile: ZipFile, byteArrayOutputStream: ByteArrayOutputStream, buf: number[]): void {
        this.logger.addLog(`开始恢复sysdata/tables下的sdi和sdi2模块下的模型文件`);

        let dataZipPath: string = BackupPackageMainDirName.SdiData;
        let dataRestorePaths: string[] = this.restoreMetaFilePaths[dataZipPath];
        let sdiTableDirInfos: JSONObject = {
            "sdi": {
                mustRestoreModules: ["dims"],
                ignoreModules: []
            },
            "sdi2": {
                mustRestoreModules: ["dim", "dims", "commons"],
                ignoreModules: ["etl", "query"]
            },
            "sdi2/etl": {
                mustRestoreModules: [],
                ignoreModules: []
            },
            "sdi2/query": {
                mustRestoreModules: ["commons"],
                ignoreModules: []
            }
        };
        let sdiTableDirs: string[] = Object.keys(sdiTableDirInfos);
        for (let i = 0; i < sdiTableDirs.length; i++) {
            let tableDir: string = sdiTableDirs[i];
            let sdiTableDirInfo = sdiTableDirInfos[tableDir];
            let mustRestoreModules: string[] = sdiTableDirInfo["mustRestoreModules"];
            if (this.signRestoreModules.length != 0) {
                mustRestoreModules.pushAll(this.signRestoreModules);
            }
            let ignoreModules: string[] = sdiTableDirInfo["ignoreModules"];
            let tableZipPath: string = `${dataZipPath}/tables/${tableDir}`;

            let restoreScriptPaths: string[] = this.filterRestorePaths(dataRestorePaths, tableZipPath, mustRestoreModules, ignoreModules);
            this.restoreMetaFiles(zipFile, byteArrayOutputStream, buf, restoreScriptPaths);
        }
    }

    /**
     * 恢复扩展表达式元数据信息
     * @param zipFile 压缩包对象
     * @param byteArrayOutputStream byte数组的缓冲区
     * @param buf 每次读取字节大小
     */
    private restoreExtensionExpModule(zipFile: ZipFile, byteArrayOutputStream: ByteArrayOutputStream, buf: number[]): void {
        if (this.signRestoreModules.length != 0 && !this.signRestoreModules.includes(BackupPackageMainDirName.SdiExtensions)) {
            this.logger.addLog(`----指定恢复模块不包含: 扩展表达式, 忽略`);
            return;
        }
        this.logger.addLog(`开始执行[restoreExtensionExpModule], 恢复扩展表达式元数据信息`);
        let restorePaths: string[] = this.restoreMetaFilePaths[BackupPackageMainDirName.SdiExtensions];
        this.restoreMetaFiles(zipFile, byteArrayOutputStream, buf, restorePaths);

        this.logger.addLog(`执行结束[restoreExtensionExpModule], 恢复扩展表达式元数据信息`);
        this.progress.step(70);
    }

    /**
     * 恢复中台样式元数据信息
     * @param zipFile 压缩包对象
     * @param byteArrayOutputStream byte数组的缓冲区
     * @param buf 每次读取字节大小
     */
    private restoreSdiThemesModule(zipFile: ZipFile, byteArrayOutputStream: ByteArrayOutputStream, buf: number[]): void {
        if (this.signRestoreModules.length != 0 && !this.signRestoreModules.includes(BackupPackageMainDirName.SdiThemes)) {
            this.logger.addLog(`----指定恢复模块不包含: 中台样式文件, 忽略`);
            return;
        }
        this.logger.addLog(`开始执行[restoreSdiThemesModule], 恢复中台样式元数据信息`);

        let sdiThemesPath: string = SdiProjectMainModule.SdiThemesPath;
        this.createFolder(sdiThemesPath, "sdi", "中台样式"); // 压缩包内存在样式文件路径没有包含: sdi文件夹, 这里恢复前需要检查sdi文件是否存在

        let restorePaths: string[] = this.restoreMetaFilePaths[BackupPackageMainDirName.SdiThemes];
        this.restoreMetaFiles(zipFile, byteArrayOutputStream, buf, restorePaths);

        this.logger.addLog(`执行结束[restoreSdiThemesModule], 恢复中台样式元数据信息`);
        this.progress.step(90);
    }

    /**
     * 在给定的路径数组(压缩包内的路径)中，过滤出指定压缩包目录下的文件夹内的文件和文件夹路径
     * 
     * @param dealZipFilePaths 处理的路径集合, eg: ["zt.app/images/", "zt.app/commons/"]
     * @param zipDirPath 过滤的文件夹在【压缩包】中路径(最后不携带：斜杆), eg: zt.app
     * @param dirChildrenFileNames? 仅包含过滤文件夹下首层子文件夹和文件名, eg:  ["commons", "custom.ts"]
     * @param ingnoreChildrenFileNames? 忽略过滤文件夹下首层文件夹和文件名, eg: ["data-access"]
     * 
     * @return eg: ["zt.app/commons/"]
     * <p>
     *   1 指定目录下的文件夹和文件名, 则会返回指定目录下对应的文件夹路径信息(包含文件夹内部所有文件夹和子文件)
     *   2 未指定目录下的文件夹名(未传或者为空), 则返回当前目录下所有文件夹和文件路径
     * </p>
     */
    private filterRestorePaths(
        dealZipFilePaths: string[],
        zipDirPath: string,
        dirChildrenFileNames?: string[],
        ingnoreChildrenFileNames?: string[]
    ): string[] {
        if (!ingnoreChildrenFileNames) {
            ingnoreChildrenFileNames = [];
        }
        if (!dirChildrenFileNames) {
            dirChildrenFileNames = [];
        }
        let filterRestorePaths: string[] = [];
        zipDirPath = `${zipDirPath}/`;

        for (let i = 0; i < dealZipFilePaths.length; i++) {
            let zipFilePath: string = dealZipFilePaths[i]; // eg: zt.app/commons/ 或 zt.app/custom.ts

            let assistFilePath: string = zipFilePath.replace(zipDirPath, ""); // 去掉文件的父目录路径
            /** 首个斜杆前的内容(文件|文件夹名), eg: commons 或 custom.ts */
            let fileOrFoldName: string = assistFilePath.substring(0, assistFilePath.indexOf("/"));
            if (ingnoreChildrenFileNames.includes(fileOrFoldName)) {
                continue;
            }
            if (dirChildrenFileNames.includes(fileOrFoldName)) {
                filterRestorePaths.push(zipFilePath); // 存储的路径应该为压缩包路径, 截取后的路径不存
            } else {
                if (StringUtils.startsWith(zipFilePath, zipDirPath)) { // 判断路径是否指定目录开头
                    filterRestorePaths.push(zipFilePath);
                }
            }
        }
        return filterRestorePaths;
    }

    /**
     * 恢复传入的路径元数据信息(文件和文件夹)
     * @param zipFile 压缩包对象
     * @param byteArrayOutputStream byte数组的缓冲区
     * @param buf 每次读取字节大小
     * @param restorePaths 恢复的路径, eg: ["zt.app/data-access/", "zt.app/custom.ts", ...]
     * 
     * <p>
     *  1 此方法会恢复传入的文件(外部传入前可以根据需求对恢复的路径进行过滤)
     * </p>
     */
    private restoreMetaFiles(
        zipFile: ZipFile,
        byteArrayOutputStream: ByteArrayOutputStream,
        buf: number[],
        restorePaths: string[]
    ): void {
        if (restorePaths == null) {
            this.logger.addLog(`没有需要恢复的路径信息, 结束执行`);
            return;
        }
        let zipEntry: ZipEntry = null;
        this.logger.addLog(`恢复文件有: ${restorePaths.length} 个`);
        for (let i = 0; i < restorePaths.length; i++) {
            let fileZipPath: string = restorePaths[i];
            /** 文件|文件夹在当前项目绝对路径 */
            let projectPath: string = this.getRestoreActualPath(fileZipPath);

            let metaProperties: DotMetaFileProperties = this.dotMetaFileProperties[fileZipPath];
            if (!metaProperties) {
                continue;
            }
            zipEntry = zipFile.getEntry(fileZipPath);
            let isFloder = metaProperties.folder as boolean;
            let metaFileName = metaProperties.name as string;
            let metaFileDesc = metaProperties.desc as string;

            if (isFloder) { // 此处的文件夹路径最后携带: /, metadata.createFile入参path不支持最后携带: /
                projectPath = projectPath.substring(0, projectPath.length - 1); // 去掉最后的: /
                this.createFolder(projectPath, metaFileName, metaFileDesc);
            } else {
                byteArrayOutputStream.reset(); // 重置此流(即它将删除此流中所有当前消耗的输出，并将变量计数重置为0)
                IOUtils.copyLarge(zipFile.getInputStream(zipEntry), byteArrayOutputStream, buf);
                let newContent = new MetaFileContentImpl(byteArrayOutputStream.toByteArray(), false);
                this.metaServiceRepository.forceCreateOrUpdateFile(projectPath, newContent, metaProperties); // 若文件不存在, 内部会自己创建
            }
        }
    }

    /**
     * 获取文件在项目的真实路径
     * @param fileZipPath 恢复文件在压缩包的路径, eg: extensions/common/
     * @return  eg: /sysdata/extensions/common/
     */
    private getRestoreActualPath(fileZipPath: string): string {
        let metaFilePath: string = "";
        /** 压缩路径首个斜杆位置 */
        let firstSlantIndex: number = fileZipPath.indexOf("/");
        let foldName: string = fileZipPath.substring(0, firstSlantIndex); // 首个斜杆前内容为压缩包四大主模块名, eg: data
        fileZipPath = fileZipPath.substring(firstSlantIndex); // 去掉主模块名, eg: data/commons/ ——> /commons/
        switch (foldName) {
            case BackupPackageMainDirName.SdiApp:
                metaFilePath = `${SdiProjectMainModule.ZtAppPath}${fileZipPath}`;
                break;
            case BackupPackageMainDirName.SdiData:
                metaFilePath = `${SdiProjectMainModule.SdiDataModulePath}${fileZipPath}`;
                break;
            case BackupPackageMainDirName.SdiExtensions:
                metaFilePath = `${SdiProjectMainModule.SdiExtensionsPath}${fileZipPath}`;
                break;
            case BackupPackageMainDirName.SdiThemes: // 样式文件夹嵌套太多, 备份包没有根据环境的路径来备份, 而是直接创建themes文件存储
                metaFilePath = `${SdiProjectMainModule.SdiThemesPath}${fileZipPath}`;
                break;
        }
        return metaFilePath;
    }

    /**
     * 从备份包里面获取所有的文件路径和对应的文件属性信息
     * @param zipInputStream 压缩包输入流
     * @param zipFile 压缩包对象
     * 
     * <p>
     *  1 压缩包的文件和文件夹路径 
     *   (1) 外层为四大主文件夹名;
     *   (2) 根据主文件夹名, 对压缩包所有文件和文件夹进行分类, 记录的值为：条目值(文件在压缩包的存储路径), 可通过zipFile.getEntry(条目名) 获取指定文件对象;
     *     eg: {
     *     "zt.app": ["zt.app/custom.ts"]
     *     "extensions": [],
     *     "data": [],
     *     "themes": []
     *  }
     *  2 获取压缩包里面所有的.meta属性信息(一个.meta: 记录文件夹首层文件和文件夹属性信息)
     *   (1) 外层key为文件在压缩包的绝对路径;
     *   (2) 为了和压缩包记录的路径一致, 文件夹路径最后都加一个: / ;
     *    eg: {
     *     "zt.app/API/": {
     *          "id":"WPiahojlyFLMCJqISUrhtD",
     *          "creator":"system",
     *          "createTime":1670340112567,
     *          "modifier":"system",
     *          "modifyTime":1670374767616,
     *          "option":{},
     *          "customOption":{}
     *     },
     *     "zt.app/custom.ts": {
     *          ...
     *     }
     * }
     * </p>
     */
    private loadZipMetaFileInfos(zipInputStream: ZipInputStream, zipFile: ZipFile): ResultInfo {
        this.logger.addLog(`开始执行[loadZipMetaFileInfos], 从备份包里面获取所有的文件路径和对应的文件属性信息`);
        let zipEntry: ZipEntry;
        let resultInfo: ResultInfo = { result: true };
        /** 备份主文件名 */
        let backMainFileNames: string[] = ["zt.app/", "data/", "extensions/", "themes/"];
        try {
            while ((zipEntry = zipInputStream.getNextEntry()) != null) {
                let fileZipPath: string = zipEntry.getName();  // eg: zt.app/.meta
                if (StringUtils.endsWith(fileZipPath, ".meta")) { // 处理文件属性
                    let dotMetaFileStr = IOUtils.toString(zipFile.getInputStream(zipEntry), StandardCharsets.UTF_8);
                    if (!dotMetaFileStr) {
                        continue;
                    }
                    let dotMetaFileParentPath: string = fileZipPath.substring(0, fileZipPath.lastIndexOf(".meta")); // 去掉.meta即可获取到父路径
                    let dotMetaFileJSON = JSON.parse(dotMetaFileStr);
                    let fileNames: string[] = Object.keys(dotMetaFileJSON);
                    for (let i = 0; i < fileNames.length; i++) {
                        let fileName: string = fileNames[i];
                        let fileProperties: DotMetaFileProperties = dotMetaFileJSON[fileName]; // 文件属性
                        let filePath: string = `${dotMetaFileParentPath}${fileName}`;
                        if (fileProperties.folder) {
                            filePath = `${dotMetaFileParentPath}${fileName}/`;
                        }
                        this.dotMetaFileProperties[filePath] = fileProperties;
                    }
                } else { // 记录需要处理的文件路径, 按主模块分类(主模块不处理)
                    if (backMainFileNames.includes(fileZipPath)) {
                        continue;
                    }
                    let fileMainDir: string = fileZipPath.substring(0, fileZipPath.indexOf("/")); // 截取首个/前面的字符为主目录名
                    if (!this.restoreMetaFilePaths[fileMainDir]) {
                        this.restoreMetaFilePaths[fileMainDir] = [];
                    }
                    this.restoreMetaFilePaths[fileMainDir].push(fileZipPath);
                }
            }
        } catch (e) {
            resultInfo.result = false;
            resultInfo.message = "从备份包里面获取所有的文件路径和对应的文件属性信息失败";
            this.logger.addLog(`[loadMetaFileInfos]执行失败, ${e?.toString()}`);
            this.logger.addLog(e);
        }
        this.logger.addLog(`执行结束, 从备份包里面获取所有的文件路径和对应的文件属性信息完成`);
        this.progress.step(10);
        return resultInfo;
    }

    /**
     * 获取当前环境的备份包文件对象
     * @return 
     */
    private getRestoreFile(): JavaFile {
        let file: JavaFile;
        if (this.uploadFileId) {
            file = getUploadFile(this.uploadFileId);
        } else {
            let zipPath = this.backupZipInfo.FILE_SAVE_PATH as string;
            file = new JavaFile(zipPath);
        }
        return file;
    }

    /**
     * 创建文件夹, 若存在则修改描述信息
     * @param path 文件夹路径, eg: /sysdata/script/sdi
     * @param name 文件夹名
     * @param desc 文件夹描述
     */
    private createFolder(path: string, name: string, desc: string): void {
        this.logger.addLog(`创建文件夹: [${path}], 文件夹名: ${name}, 描述: ${desc}`);
        if (!name || name.lastIndexOf(".") != -1) { // 判断是否为文件
            return;
        }
        let metaFile = metadata.getFile(path);
        if (metaFile == null) {
            metadata.createFile({
                path: path,
                name: name,
                desc: desc,
                type: "fold",
                isFolder: true
            })
        } else {
            metadata.modifyFile(path, {
                desc: desc
            });
        }
    }
}

/**
 * 获取中台所有模块信息
 * @return eg: [{
 *   "mis-mgr": "信息系统管理"
 * }]
 */
function querySdiAllModuleInfo(): JSONObject {
    let query: QueryInfo = {
        sources: [{
            id: "model1",
            path: "/sysdata/data/tables/sdi2/sys-mgr/sdiMetaMgr/SYS_SDI_MODULE_STRUCTURE.tbl"
        }],
        fields: [{
            name: "MODULE_NAME", exp: "model1.MODULE_NAME"
        }, {
            name: "MODULE_DESC", exp: "model1.MODULE_DESC"
        }],
    }
    let sdiModuleInfo: JSONObject = {};
    let queryData = dw.queryData(query).data;
    for (let i = 0; i < queryData.length; i++) {
        let data = queryData[i];
        let modeleName = data[0] as string;
        let moduleDesc = data[1] as string;
        sdiModuleInfo[modeleName] = moduleDesc;
    }
    return sdiModuleInfo;
}

/** 中台项目主模块路径 */
enum SdiProjectMainModule {
    /** 中台SPG模块根目录 */
    ZtAppPath = "/sysdata/app/zt.app",
    /** 中台数据模块根目录 */
    SdiDataModulePath = "/sysdata/data",
    /** 中台扩展表达式根目录 */
    SdiExtensionsPath = "/sysdata/extensions",
    /** 中台样式根目录 */
    SdiThemesPath = "/sysdata/settings/themes/spg/sdi"
}

/** 备份压缩包主目录名 */
enum BackupPackageMainDirName {
    /** 中台APP模块 */
    SdiApp = "zt.app",
    /** 中台数据模块 */
    SdiData = "data",
    /** 中台扩展表达式模块 */
    SdiExtensions = "extensions",
    /** 中台样式模块 */
    SdiThemes = "themes"
}

/** 项目备份信息 */
interface SysSdiModuleBackupInfo {
    /** 备份ID */
    BACKUP_ID?: string;
    /** 备份模块(代码) */
    BACKUP_MODULE_ID?: string;
    /** 备份模块(描述） */
    BACKUP_MODULE_NAME?: string;
    /** 备份耗时(s) */
    BACKUP_TIME?: number;
    /** 备份文件附件ID,eg: `workdir:FileName` */
    BACKUP_ATTACH_ID?: string;
    /** 备份文件存储路径 */
    FILE_SAVE_PATH?: string;
    /** 备份状态 */
    BACKUP_STATUS?: string;
    /** 备份消息 */
    BACKUP_MESSAGE?: string,
    /** 保留天数 */
    KEEP_DAYS?: number;
    /** 备份者 */
    CREATOR?: string;
    /** 备份时间 */
    CREATE_TIME?: Date;
}

/** 中台项目备份设置管理 */
interface SysBackupSetMgr {
    /** 备份配置ID  */
    SET_ID: string;
    /** 备份模块(为空, 则备份全部), eg: ["data-access"] */
    BACKUP_MODULES: string[];
    /** 是否定时备份 */
    AUTO_BACKUP: string;
    /** 时间计划 */
    BACKUP_SCHEDULE: string;
    /** 保留天数 */
    KEEP_DAYS: number;
    /** 创建时间 */
    CREATE_TIME?: Date;
    /** 创建者 */
    CREATOR?: string;
}

/** 元数据配置信息 */
interface DotMetaFileProperties {
    /** 唯一标识 */
    id?: string;
    /** 文件名 */
    name?: string;
    /** 路径 */
    path?: string;
    /** 类型 */
    type?: string;
    /** 图标 */
    icon?: string;
    /** 描述 */
    desc?: string;
    comment?: string;
    creator?: string;
    createTime?: number;
    modifier?: string;
    modifyTime?: number;
    /** 是否文件夹 */
    folder?: boolean;
    /** 是否隐藏 */
    hidden?: boolean;
    order?: number;
    subFileType?: string;
    state?: number;
    option?: JSONObject;
    customOption?: JSONObject;
}

/**
 * 获取上传的文件
 * 最新包是通过fs模块调用getUploadFile方法
 */
function getUploadFile(fileId: string) {
    const user = security.getCurrentUser();
    let userId = user?.userInfo.userId;
    if (!userId) {
        userId = "admin"
    }
    const tempUploadFileService = bean.getBean("com.succez.commons.service.impl.io.TempUploadFileServiceImpl");
    const fileObject = tempUploadFileService.getUploadFile(userId, fileId);
    const filePath = fileObject.getPath();
    let file = fs.getFile(filePath);
    return file;
}