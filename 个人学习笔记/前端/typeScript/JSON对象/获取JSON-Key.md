# * 获取JSON对应的key

> 网址：https://www.cnblogs.com/sylarmeng/p/6817388.html

​	测试数据

```ts
let test = {
    "HZDJH": "3622",
    "HZXM": "开心",
    "HZZJLX": 10,
    "HZZJHM": "420101198101019971",
    "HZBQ": "510000001150_3",
    "HZCH": "15",
    "HZKSMC": "妇产科",
    "HZZZYS": "李四四四",
    "BZSM": "轻微病症,多喝热水",
    "RYSJ": "2021-07-19 17:08:11"
}
isExistColumn(test);
```

---

​	**方法一：**for... in

```ts
/**
 * 获取对象的所有key
 */
 for(let key in test){
 	console.debug(key);
 }
/**
 HZDJH
 HZXM
 HZZJLX
 ...
*/
```

​	**方法二：** javascript中，Object具有一个key属性，可以返回json对象的key的数组

```ts
// 不推荐使用这种，可能部分浏览器不支持
print(Object.keys(test));
["HZDJH", "HZXM", "HZZJLX", "HZZJHM", "HZBQ", "HZCH", ...]
```



# * 根据key《获取value》

```ts
for (let key in checkRowResult.data) {
	patientError[`${key}`] = `${checkRowResult.data[key]}`;
}
```