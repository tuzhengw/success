---
typora-root-url: images
---

# FlinkSQL学习

## 1 Flink 固有API



## 2 异步提交job

**参考**：https://www.cnblogs.com/gcwell/p/15570295.html

解决了flink任务提交依赖传统Jar提交的问题，改为Java应用程序获取`RemoteEnvironment`方式提交，便于维护管理等。

通过次提交方式，可以做进一步的延伸，通过Flink版本管理，Sql管理。只需要简单的存储版本信息，某个任务的Sql信息，就能快速实现任务提交，以此来摒弃传统的Jar任务提交。进一步来讲，Flink越来越重视FlinkSql，从Flink的更新，以及维护来看，Flink的未来将着重于SQL，以高级的SQL API取缔其他API。所以，总的来说Flink Sql有无限前景。

```java
StreamExecutionEnvironment env = StreamExecutionEnvironment.createRemoteEnvironment("gcw1", 8081);
        StreamTableEnvironment stEnv = StreamTableEnvironment.create(env);
        String kafkaFK = "CREATE TABLE test_fk (    " +
                "  `id` BIGINT,    " +
                "  `num` INT,    " +
                "   `ts` TIMESTAMP(3) METADATA FROM 'timestamp'    " +
                ") WITH (    " +
                "  'connector' = 'kafka',    " +
                "  'topic' = 'TEST_FK',    " +
                "  'properties.bootstrap.servers' = 'gcw1:9092',    " +
                "  'scan.startup.mode' = 'earliest-offset',    " +
                "  'format' = 'csv'    " +
                ")";
        String mysqlFK = " CREATE TABLE test_demo (  " +
                "  id BIGINT,  " +
                "  ct_num BIGINT,  " +
                "  submit_time  TIMESTAMP(3) ,  " +
                "  PRIMARY KEY (id) NOT ENFORCED  " +
                ") WITH (  " +
                "   'connector' = 'jdbc',  " +
                "   'url' = 'jdbc:mysql://gcw3:3306/test',  " +
                "   'table-name' = 'test_demo',  " +
                "   'username' = 'root',  " +
                "   'password' = '123456'  " +
                ")";
        stEnv.executeSql(kafkaFK);
        stEnv.executeSql(mysqlFK);
        TableResult tableResult = stEnv.executeSql("insert into test_demo select id,sum(num),max(ts) from test_fk group by id");

        //获取任务id
        Optional<JobClient> jobClient = tableResult.getJobClient();
        JobClient jobClient1 = jobClient.get();
        JobID jobID = jobClient1.getJobID();
        System.out.println(jobID);

```



## 3 根据jobId重新提交

​	例使用 Flink 的 Java API 创建一个新的 `ClusterClient`，并使用该客户端来取消旧的作业并重新提交具有相同参数的新作业。最后，你可以根据需要对代码进行修改和扩展。

​	注意：

​	（1）在使用此方法时，请确保你的 Flink 集群具有适当的权限和访问控制，以便执行取消和重新提交操作;

​	（2）在 Flink 1.13.6 版本中，如果你使用的是直接提交 SQL 的方式（而非通过 DataStream 或 DataSet API 编写作业），则**无法通过 Java 代码直接重新启动 executeSQL 作业**。因为直接提交的 SQL 作业没有对应的 JobGraph 对象，无法通过编程方式重新提交。

```java
import org.apache.flink.api.common.JobID;
import org.apache.flink.client.program.ClusterClient;
import org.apache.flink.client.program.PackagedProgram;
import org.apache.flink.client.program.ProgramInvocationException;
import org.apache.flink.client.program.rest.RestClusterClient;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.runtime.jobgraph.JobGraph;

public class ExecuteSQLRestartExample {
    public static void main(String[] args) throws Exception {
        final String jobId = "9dc39aa54ed1d18c8653783a42298d52"; // 替换为实际的 jobId
        final String flinkRestUrl = "http://localhost:8081"; // 替换为实际的 Flink REST URL

        Configuration configuration = new Configuration();
        PackagedProgram packagedProgram = PackagedProgram.newBuilder()
                .setJarFile("/path/to/your/job.jar") // 替换为你的作业的 JAR 文件路径
                .setEntryPointClassName("com.example.YourJobClass") // 替换为你的作业的入口类名
                .build();

        ClusterClient<?> clusterClient = new RestClusterClient<>(configuration, flinkRestUrl);
        try {
            JobID jobID = JobID.fromHexString(jobId);
            JobGraph jobGraph = packagedProgram.getJobGraph();
            jobGraph.setAllowQueuedScheduling(true); // 如果作业已在队列中，则允许重新提交
            jobGraph.setJobID(jobID);

            clusterClient.cancel(jobID).get(); // 取消之前的作业
            clusterClient.submitJob(jobGraph).get(); // 重新提交作业

            System.out.println("Successfully restarted the executeSQL job.");
        } catch (ProgramInvocationException e) {
            System.err.println("Failed to restart the executeSQL job: " + e.getMessage());
        } finally {
            clusterClient.close();
        }
    }
}
```

**高版本 使用 REST API**：

1. 获取作业状态：使用 `/jobs/overview` 接口获取作业的概述信息，并找到要重新启动的作业的 `jobId`。
2. 取消作业：使用 `/jobs/:jobid/cancel` 接口取消作业。将 `:jobid` 替换为要取消的作业的 `jobId`。
3. 重新启动作业：使用 `/jobs/:jobid/run` 接口重新启动作业。将 `:jobid` 替换为要重新启动的作业的 `jobId`。

----

## 4 FlinkSQL校验类 - 注释版

```ts
/**
 * FlinkSQL校验类
 * 解析&校验 FlinkSQL语法(多条会自动关联上下文)是否正确
 * @author tuzw
 * @createDate 2023-06-11
 * @achieve 实现
 * 1. 校验是由flink的[SqlToOperationConverter]对象校验, 校验前提是: 处理的FlinkSQL能过被[SqlParser]对象所识别并解析,
 * 否则整个流程则视为失败, 示例:
 * 	- 正确SQL: insert into MyTable2 select * from MyTable3; 可以解析并校验, 校验结果: 无法识别内部的表对象[MyTable2]和[MyTable3]。
 * 	- 错误SQL: into MyTable2 select * from MyTable3; 无法解析和校验, 并抛出异常无法识别SQL。
 * 
 * @description 注意事项
 * 1. 单个字符串可能存在多条FlinkSQL语句, 内部由分号(;)分割, flink对象[SqlParser]会自己内部解析, 并识别上下文之间的关系;
 * 2. 不会验证 SQL 语义部分是否正确, 比如表名、列名、列数据类型等信息的检验会在 Validate 阶段;
 * 3. 仅检验 SQL 语法是否正确, 识别关键字、标识符等, 关键字有: select、from、where、<、=等;
 * 4. 考虑到校验只要是基于flink环境就行, 内部连接本地flink环境校验;
 * 5. 校验支持多数据库;
 * 6. 一次性校验多组(多条SQL), 内部会关联多组SQL的上下文, 多组SQL内的表名必须唯一, 否则CREATE TABLE 异常(Could not execute 
 * CreateTable in path `default_catalog`.`default_database`.`MyTable2`"), 单条SQL校验不会出现此情况。
 * - 参考博客
 * 1. 解析字符串中的FlinkSQL语句: https://blog.csdn.net/u013539342/article/details/111382538
 * 2. 解析并校验FlinkSQL语句: https://blog.csdn.net/April_Lie/article/details/105980108
 */
export class FlinkSqlCheckService {

    /** 日志类对象 */
    private logger: FlinkLog;
    /** SQL解析器配置 */
    private sqlParserConfig;
    /** 流计算实例 */
    private tableEnv;

    constructor(args: {
        logger: FlinkLog
    }) {
        this.logger = args.logger;
        this.init();
    }

    /**
     * 初始化成员属性
     */
    private init(): void {
        let flinkPlatform = flinkUtils.getFlinkEvnConf();
        /**
         * 本地Flink执行环境
         * 校验不需要连接到启动flink平台, 直接获取有个本地环境的Flink环境;
         * 根据当前运行的上下文直接得到正确的结果：如果程序是独立运行的，就返回一个本地执行环境。
         */
        // let flinkEnv = StreamExecutionEnvironment.getExecutionEnvironment();
        let flinkEnv = StreamExecutionEnvironment.createRemoteEnvironment(flinkPlatform.flinkHostIp, flinkPlatform.flinkPort);
        /** Flink环境配置 */
        let tableEnvSettings = EnvironmentSettings
            .newInstance()
            .inStreamingMode()
            .useBlinkPlanner()
            .build();
        this.tableEnv = StreamTableEnvironment.create(flinkEnv, tableEnvSettings);
    }

    /**
     * 批量解析&校验多组 FlinkSQL语法(多条会自动关联上下文)是否正确
     * @param flinkSqls 校验的SQL语句(每组SQL语句分号分隔), eg:
     * ```
     * [
     *    `CREATE TABLE MyTable1 (
                id INT,
                account VARCHAR,
                password VARCHAR,
                PRIMARY KEY (id) NOT ENFORCED
            ) WITH (
                'connector' = 'mysql-cdc',
                'hostname' = '192.168.144.129',
                'port' = '3306',
                'username' = 'root',
                'password' = '123456',
                'database-name' = 'test1',
                'table-name' = 'student1'
            );
            CREATE TABLE MyTable2 (
                id INT,
                account VARCHAR,
                password VARCHAR,
                PRIMARY KEY (id) NOT ENFORCED
            ) WITH (
                'connector' = 'jdbc',
                'url' = 'jdbc:mysql://192.168.144.129:3306/test2',
                'port' = '3306',
                'username' = 'root',
                'password' = '123456',
                'table-name' = 'student2'
            );
            insert into MyTable2 select * from MyTable3;`
     * ]
     * ```
     * @return sql语句, eg:
     * ```
     * {
     *   result: true, // 若SQL都没有问题, 则返回: true
     *   data: [{
     *     rowIndex: 0, // 行下标
     *     checkResult: false,
     *     errrorMessage: ["org.apache.flink.table.api.ValidationException: SQL validation failed. From line 27, column 48 to line 27, column 55: Object 'mytable3' not found"]
     *   }, {
     *     rowIndex: 1,
     *     checkResult: true,
     *     executeSqls: ["xxxx"] 
     *   }]
     * }
     * ```
     */
    public checkFlinkSqlSyntax(flinkSqls: string[] | string): ResultInfo {
        if (!flinkUtils.checkParamIsArray(flinkSqls)) {
            flinkSqls = [flinkSqls as string];
        }
        if (flinkSqls.length == 0) {
            return { result: false, message: `校验SQL为空` };
        }
        /** 
         * 管理元数据对象
         * 使其可以从 Table API 和 SQL 查询语句中来访问;
         * Catalog 提供了元数据信息，例如数据库、表、分区、视图以及数据库或其他外部系统中存储的函数和信息。
         */
        let catalogManager = this.tableEnv.getCatalogManager();
        let planner = this.tableEnv.getPlanner();
        /** 
         * 查询处理器对象
         * 沟通Flink与Calcite的桥梁，为Table/SQL API提供完整的解析、优化和执行环境。
         */
        let flinkPlanner = planner.createFlinkPlanner();
        let checkResults: FlinkSqlCheckResult[] = [];
        let resultInfo: ResultInfo = { result: true };
        try {
            this.sqlParserConfig = this.getSqlParserConfig();
            this.logger.addLog(`校验SQL组共[${flinkSqls.length}]组`);
            for (let sqlIndex = 0; sqlIndex < flinkSqls.length; sqlIndex++) {
                /** 处理的SQL可能包含多条, 通过parser解析器获取每条SQL, 循环校验  */
                let flinkSql: string = flinkSqls[sqlIndex];
                this.logger.addLog(`开始校验第[${sqlIndex + 1}]组Flink SQL语法正确性`);
                let checkResult = this.flinkSqlParser(flinkSql, sqlIndex, flinkPlanner, catalogManager);
                checkResults.push(checkResult);
            }
            resultInfo.data = checkResults;
        } catch (e) {
            resultInfo.result = false;
            resultInfo.message = `解析Flink SQL语句失败[${e.toString()}]`;
            this.logger.addLog(`FlinkSQL本身不满足FlinkSQL规范, 无法解析其语法正确性`);
            this.logger.addLog(e);
        }
        return resultInfo;
    }

    /**
     * 解析单组SQL语法
     * 1. SQL区分字段、表名大小写。
     * @param flinkSql 单组SQL
     * @param sqlIndex 第几组
     * @param flinkPlanner 查询处理器对象
     * @param catalogManager 管理元数据对象
     * @return 
     */
    private flinkSqlParser(flinkSql: string, sqlIndex: number, flinkPlanner, catalogManager): FlinkSqlCheckResult {
        let checkResult: FlinkSqlCheckResult = { rowIndex: sqlIndex, checkResult: true, executeSqls: [], errorMessage: [] };
        try {
            let parser = SqlParser.create(flinkSql, this.sqlParserConfig);  // 使用大小写敏感的解析器进行解析
            let sqlNodeList: SqlNode[] = parser.parseStmtList().getList();
            if (sqlNodeList != null) {
                /**
                 * 考虑到处理的SQL是多条, 并且存在上下文关联关系, 循环校验并对操作进行判断不同处理
                 * 单条SQL校验: SqlToOperationConverter.convert(flinkPlanner, catalogManager, sqlNodeList.get(i)).isPresent();
                 */
                for (let i = 0; i < sqlNodeList.length; i++) {
                    let sqlNode: string = sqlNodeList[i];
                    checkResult.executeSqls.push(sqlNode.toString());
                    try {
                        let operation = SqlToOperationConverter
                            .convert(flinkPlanner, catalogManager, sqlNode)
                            .orElseThrow(() => new TableException(`Unsupported query: ${sqlNode}`));
                        if (operation instanceof CreateTableOperation) { // 左边的对象是否是它右边的类的实例
                            if (operation.isTemporary()) { // 创建临时表
                                catalogManager.createTemporaryTable(
                                    operation.getCatalogTable(),
                                    operation.getTableIdentifier(),
                                    operation.isIgnoreIfExists());
                            } else {
                                catalogManager.createTable(
                                    operation.getCatalogTable(),
                                    operation.getTableIdentifier(),
                                    operation.isIgnoreIfExists());
                            }
                        }
                    } catch (e) {
                        checkResult.checkResult = false;
                        checkResult.errorMessage.push(e.toString());
                    }
                }
            }
        } catch (e) {
            checkResult.checkResult = false;
            checkResult.executeSqls.push(flinkSql);
            checkResult.errorMessage.push(`SQL本身不满足Flink规范, 无法编译并解析其语法是否正确[${e.toString()}]`);
            this.logger.addLog(`解析和校验第[${sqlIndex}]组SQL失败[${e.toString()}]`);
        }
        return checkResult;
    }

    /**
     * 获取解析SQL的配置
     * @description 注意事项
     * - 解析配置不能将其作为成员变量, 原因: 
     * 1. 在使用 SqlParser 进行 SQL 解析时，通常需要连续执行 SqlParser.configBuilder() 和 SqlParser.create() 方法;
     * 2. SqlParser.configBuilder() 用于创建一个配置构建器对象，可以使用该对象设置解析器的各种选项和参数，例如：
     * 大小写规则、解析模式等。然后，通过调用 build() 方法，将配置构建器转换为实际的解析器配置对象;
     * 3. 之后, 可以使用 SqlParser.create(sql, config) 方法来创建一个 SqlParser 对象，并指定要解析的 SQL 字符串和解析器配置;
     * 4. 这种连续执行的方式确保了可以先构建解析器的配置，然后将配置应用于具体的解析操作，从而更好地控制解析器的行为;
     */
    private getSqlParserConfig() {
        return SqlParser.configBuilder()
            .setCaseSensitive(true)  //  大小是写否敏感，比如说列名、表名、函数名
            .setParserFactory(FlinkSqlParserImpl.FACTORY)
            .setQuoting(Quoting.BACK_TICK) // 设置引用一个标识符，比如说MySQL中的是``, Oracle中的""
            /**
             * setUnquotedCasing(配置)
             * - 作用: 方法是用于设置解析器在处理未加引号的字段时的大小写规则。
             * 1. Casing.TO_LOWER 将未加引号的字段名转换为小写形式（默认）
             * 2. Casing.TO_UPPER 将未加引号的字段名转换为大写形式
             * 3. Casing.UNCHANGED 保持未加引号的字段名的大小写不变
             * - 注意: 建议原样输出, 避免字段区分大小写
             */
            .setUnquotedCasing(Casing.UNCHANGED)
            .setQuotedCasing(Casing.UNCHANGED)
            .setConformance(FlinkSqlConformance.DEFAULT) // 特定语法支持，比如是否支持差集等
            .build();
    }
}
```





# end 