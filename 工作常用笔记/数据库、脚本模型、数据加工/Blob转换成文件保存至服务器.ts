/**
 * 作者：刘永卓
 * 创建时间：2022-05-24
 * 脚本用途：读取数据库内的blob生成文件，并且推送到远程ftp服务器上
 * 相关帖子：https://jira.succez.com/browse/CSTM-18894
 */
import db from "svr-api/db";
import fs from "svr-api/fs";
import sys from "svr-api/sys";
import utils from "svr-api/utils";
import ftp from "svr-api/ftp";
import { FtpClient } from "svr-api/types/ftp.types";
const Timestamp = Java.type('java.sql.Timestamp');
/**
 * 实现思路：从数据库读取数据，将blob字段的内容存储到文件中，这个文件位置为工作目录下，workDir/CSTM-18894
 * 各个文件按照推送日期区分
 */
function main() {
    let service = new UploadFileToFtpService();
    //service.execute();
    service.watchWorkDirFile("/2022-05-25");
    service.watchFtpFile("/group1/M00/00/60/")
}

interface UploadFileInfo {
    fileId: string;
    /**目标服务器路径地址 */
    targetPath: string;
    /**文件名称 */
    fileName: string;
    /**文件内容 */
    fileContent: JdbcBlob;
}

/**
 * 上传文件至ftp服务器类
 */
class UploadFileToFtpService {
    private dataSourceName = "default";
    private tableName = "SPYX_FJGHSJ_TEMP";
    private logTableName = "CSTM18894_LOG";
    private localFolderPath: string;
    private ds: Datasource;
    private table: TableData;
    private pageSize = 100;
    /**出现错误的数据 */
    private logData: any[];
    private ftpClient: FtpClient;
    constructor() {
        this.logData=[];
        let ds = this.ds = db.getDataSource(this.dataSourceName);
        let table = this.table = ds.openTableData(this.tableName);
        let workDir = sys.getWorkDir();
        this.localFolderPath = workDir + "/CSTM-18894";
        fs.mkdirs(this.localFolderPath);
        this.connectFTPClient();
    }

    /**
     * 查看工作目录下的文件信息
     */
    public watchWorkDirFile(folder?:string){
        let path = this.localFolderPath;
        if(folder!=null){
            path +=folder;
        }
        let file = fs.getFile(path);
        print("服务器上"+path+"下文件夹和文件")
        print(file.listDirs())
        print(file.listFiles());
    }

    /**
     * 查看服务器上指定目录文件
     */
    public watchFtpFile(path:string){
        print("ftp服务器上"+path+"目录下文件");
        let file = this.ftpClient.list(path);
        print(file);
    }

    public execute() {
        let totalRowCount = this.getTotalRowCount();
        let pageNum = Math.floor(totalRowCount / this.pageSize);
        print(`本次查询出${totalRowCount}条数据,总共要查询${pageNum}次`);
        for (let i = 0; i < 1; i++) {
            print("开始执行第" + i + "次查询");
            let rows = this.queryData(i);
            for(let j=0;j<1;j++){
                let row = rows[j];
                let uploadResult = this.createFileAndUpload(row);
                let logRow = [utils.uuid(), uploadResult.fileId, uploadResult.filePath, uploadResult.result, uploadResult.message, new Timestamp(new Date().getTime())];
                this.logData.push(logRow);
            }
        }
        this.insertlogData();
    }

    /**
     * 创建文件，并且将文件上传
     */
    private createFileAndUpload(data: UploadFileInfo) {
        let result = true;
        let message = "";
        let now = new Date();
        let date = utils.formatDate("yyyy-MM-dd", now);
        let folderPath = this.localFolderPath + "/" + date;
        fs.mkdirs(folderPath);
        let fileName = data.fileName;
        let filePath = folderPath + "/" + fileName;
        let file = fs.getFile(filePath);
        if (!file.exists()) {//如果文件不存在，那么创建一个真实文件
            file.touch();
        }
        let outStream = file.openOutputStream();
        try {
            let length = data.fileContent.length();
            let bytes = data.fileContent.getBytes(1,length);
            outStream.write(bytes as number[]);
        } catch (e) {
            result = false;
            message = "写入文件内容出现异常";
            print(data.fileId+"出现异常:"+message);
            print(e);
        } finally {
            outStream && outStream.close();
        }
        if (result) {//如果正常那么就尝试将文件发送至ftp服务器
            try {
                let ftpFile =this.ftpClient.get("/"+data.targetPath+"/"+data.fileName);
                if(ftpFile.exists()){
                    result = false;
                    message = "文件存在，忽略不上传";
                    print(data.fileId+"文件存在，忽略不上传");
                    return {result,message,filePath,fileId:data.fileId}
                }
                this.ftpClient.mkdir("/" + data.targetPath);
                this.ftpClient.put("/"+data.targetPath, filePath);
            }catch(e){
                result=false;
                message = "上传至ftp服务器出现异常";
                print(data.fileId + "出现异常:" +message);
                print(e);
            }
        }
        return { result, message, filePath,fileId:data.fileId};
    }

    private connectFTPClient() {
        let ftpClient = this.ftpClient = ftp.connectFTP({
            host: "172.16.1.111",
            port: 21,
            username: "SPYX_JS",
            password: "4yPS9DBD"
        })
    }

    /**
     * 查询数据库中的数据
     */
    private queryData(pageNum: number): UploadFileInfo[] {
        let rows: UploadFileInfo[]= [];
        let result = this.table.select("*", '', [pageNum, (pageNum + 1) * this.pageSize]);
        for (let i = 0; i < 1; i++) {
            let data = result[i];
            let filePath = data["FASTDFS_PATH"];
            let fileId = data["FILE_IDENTIFICATION"];
            let fileContent = data["FILE_INFO"];
            let list = filePath.split("/");
            let fileName = list[list.length - 1];
            let targetPath = list.splice(0, list.length - 1).join("/");
            rows.push({ fileId, fileName, fileContent, targetPath });
        }
        return rows;
    }

    /**
     * 记录日志信息
     */
    private insertlogData(){
        let logTable = this.ds.openTableData(this.logTableName);
        logTable.insert(this.logData, ["ZJID", "WJID", "WJZJ", "SCJG", "SCMS","SCSJ"])
    }
    
    /**
     * 获取总行数
     */
    private getTotalRowCount() {
        let totalRowCount = this.ds.executeQuery(`select count(*) AS TOTAL from DSJYY.${this.tableName}`);
        return totalRowCount[0]["TOTAL"];
    }
}
