/**
 * =====================================================
 * 作者：tuzw
 * 审核人员：liuyz
 * 创建日期:2022-01-10
 * 脚本入口:CustomJs
 * 功能描述
 * 		此脚本是【病案质控系统沙盘演练】所有spg、表单、dash和门户定制功能入口
 * =====================================================
 */
import {
    ctx, SZEvent, Component, throwInfo, throwError, showConfirmDialog, showFormDialog, showErrorMessage, showMenu, isEmpty, deepAssign, uuid, message,
    quoteExpField, showDialog, showSuccessMessage, showProgressDialog, rc, downloadFile, waitRender, showWaiting, assign, IDataProvider, formatDateFriendly,
    parseDate, formatDate, tryWait, rc_get, showWarningMessage, CommandItemInfo
} from 'sys/sys';
import { IMetaFileCustomJS, InterActionEvent } from "metadata/metadata-script-api";
import { ExpDialog, ExpDialogValueInfo, IFieldsDataProvider, showExpDialog } from 'commons/exp/expdialog';
import { ExpCompiler, ExpVar, IExpDataProvider } from 'commons/exp/expcompiler';
import { ExpContentType } from 'commons/exp/exped';
import { runCheckTask } from 'dg/dgapi';
import { getDwTableDataManager, getQueryManager, updateDwTableDatas, getDwTableChangeEvent } from 'dw/dwapi';
import { Grid, GridArgs, List, ListItem, TreeItem } from 'commons/tree';
import { MetaFileViewer, showDrillPage } from 'metadata/metadata';
import { showResourceDialog } from 'metadata/metamgr';
import { FAppCommandRendererId, FAppInterActionEvent, ICustomJS, IFApp, IFAppCustomJS } from 'metadata/metadata-script-api';

/** 返回信息 */
interface ResultInfo {
    /** 返回结果 */
    result: boolean;
    /** 消息 */
    message?: string;
    /** 返回数据 */
    data?: any;
    [propname: string]: any;
}


/**全局参数，用于判断导入数据标准的时候是否需要将目录添加进去 */
let isAddFolder = '';
/**
 * 表单应用自定义脚本接口。
 * 通过编辑内部方法可开发个性化功能。
 */
const fappCustomJS: IFAppCustomJS = {

    onSubmitData: (event: FAppInterActionEvent): boolean | Promise<boolean> => {
        let datapackage = event.app.formsDataMgr.getModifiedDataPackage();
        if (!datapackage) {
            return;
        }
        let data = datapackage.data;
        let data_ruleLib = data["DG_RULES_LIB"];
        let resid = data_ruleLib['MAIN_TABLE_ID'];
        let bm = data_ruleLib['ALIAS'];
        let catalog = data_ruleLib['DG_RES_CATALOG'];
        let rule_lib_id = datapackage.orgId;
        return rc({
            url: `/rule/ruleCheck/addResToTask`,
            method: 'POST',
            data: {
                catalog: catalog,
                resid: resid,
                bm: bm,
                rule_lib_id: rule_lib_id
            }
        }).then(result => {
            if (result.success) {
                return true;
            } else {
                event.app.showMessage(result.message, 'error');
                return false;
            }
        })
    },
    onFetchCommands: (args: {
        app: IFApp,
        rendererId: FAppCommandRendererId,
        commands: Array<string | CommandItemInfo>
    },): Array<string | CommandItemInfo> | Promise<Array<string | CommandItemInfo>> => {
        let commands = args.commands;
        args.commands[1] && (args.commands[1].visible = false);
        if (args.rendererId == FAppCommandRendererId.FormToolbar) {
            let item = args.app.filterTree.tree && args.app.filterTree.tree.getSelectedItem();
            if (item != null && item.length > 0) {
                let data = item.getData();
                let mllxIndex = data.getDbfields().indexOf("MLLX");
                let mllx = data.getData()[mllxIndex];
                let standardIndex = data.getDbfields().indexOf("STD_LIB_ID");
                let standard_id = data.getData()[standardIndex];
                if (mllx != "STD" || isEmpty(standard_id)) {
                    args.commands[1].visible = true;
                }
            }
        }
        return args.commands;
    },

    onRenderDataList: (args: {
        app: IFApp;
        data: JSONObject[];
        list: Grid;
    }): void | Promise<Array<JSONObject>> => {
        args.app.formsDataMgr.configs.autoResetExpparams = false;
        args.data.forEach(item => {
            let html = `<a class='link-pointer' oper='edit'>编辑</a>&nbsp;&nbsp;<a class='link-pointer' oper='remove'>删除</a>`;
            item.operateHTML = html;
            if (!item.DATA4) {
                item.DATA4 = 0;
            }
        });
    },

    onInitDataList: (args: {
        app: IFApp,
        initArgs: GridArgs,
        tabId?: string
    }): void | Promise<GridArgs> => {
        let listArgs = args.initArgs;
        let columns = listArgs.columns;
        columns[columns.length - 1].flexGrow = 1.5; // 默认最后一列增大比例会比较大，自己新增列后，需要调小一些
        listArgs.columns.push({
            id: 'operate',
            caption: '操作',
            flexGrow: 9,
            htmlField: 'operateHTML'
        });
        let onclick = listArgs.onclick; // TODO 不必记录之前的onclick，表单内部处理
        listArgs.onclick = (sze: SZEvent, list: List, item: ListItem) => {
            let domLink = (<HTMLElement>sze.srcEvent.target).closest('.link-pointer');
            let target = <HTMLElement>sze.srcEvent.target;
            if (!item || !domLink || !domLink.getAttribute('oper')) {
                return;
            }
            let domFileViewer = domLink.closest('.metafileviewer-base');
            if (!domFileViewer) {
                return;
            }
            let fileViewer: MetaFileViewer = domFileViewer.szobject as MetaFileViewer;
            let oper = domLink.getAttribute('oper');
            if (oper === 'edit') {
                onclick && onclick(sze, list, item);
            } else {
                if (oper === 'remove') {
                    let rule_lib_id = item.getId();
                    let itemData = item.getData();
                    let datamgr = args.app.formsDataMgr;
                    let queryInfo: QueryInfo = {
                        fields: [{ name: "RULE_ID", exp: `model1.RULE_ID` }],
                        filter: [{ exp: `model1.RULES_LIB_ID= '${rule_lib_id}'` }],
                        sources: [{
                            id: "model1",
                            path: "/sysdata/data/tables/dg/DG_RULES.tbl"
                        }]
                    };
                    getQueryManager().queryData(queryInfo, uuid()).then((result: QueryDataResultInfo) => {
                        if (result && result.data && result.data.length > 0) {
                            showConfirmDialog({
                                caption: '提示',
                                message: '该检测资源有自定义规则，不允许删除，请先删除自定义规则再删除资源',
                                buttons: ['ok']
                            });
                        } else {
                            showConfirmDialog({
                                caption: '删除',
                                message: '删除后无法恢复，确认要继续删除选中的资源？',
                                onok: () => {
                                    datamgr.deleteSvrData({ deleteOrgs: { orgIds: [rule_lib_id] } }).then(() => {
                                        return (<any>args).app.showMessage('删除成功', 'info');
                                    }).catch(e => {
                                        throw e;
                                    });
                                }
                            });
                        }
                    });
                } else {
                    // if (oper == 'viewRes') {
                    //     let itemData = item.getData();
                    //     let resid = itemData['DATA4']
                    //     fileViewer.showDrillPage({
                    //         url: {
                    //             path: getAppPath(`/data-asset/file-viewer.spg`),
                    //             params: { 'hideReturnBtn': true, "resid": resid }
                    //         },
                    //         breadcrumb: false,
                    //         target: ActionDisplayType.Dialog,
                    //         dialogArgs: {
                    //             width: 1200,
                    //             height: 600
                    //         }
                    //     })
                    // }
                }
            }
            return;
        }
        let filterTree = args.app.filterTree;
        filterTree?.renderPromise.then(() => {
            let oncontextmenu = filterTree.tree.oncontextmenu;
            isAddFolder = args.app.formsDataMgr.expparams.isAddFolder;
            filterTree.tree.oncontextmenu = doFilterTreeContextMenu.bind(args.app.filterTree);
            function doFilterTreeContextMenu(sze: SZEvent, list: List, item: ListItem): void | boolean {
                let filterTree = args.app.filterTree;
                let selectedItem = filterTree.tree.getSelectedItem();
                let menuItems = [{
                    id: 'addBrotherDimItem',
                    caption: "新增同级",
                    icon: 'icon-add'
                }, {
                    id: 'addChildDimItem',
                    caption: "新增下级",
                    icon: 'icon-add'
                }, {
                    id: 'editDimItem',
                    caption: "编辑",
                    icon: 'icon-edit'
                }, {
                    id: 'deleteDimItem',
                    caption: "删除",
                    icon: 'icon-delete'
                }, {
                    id: 'batchImportAsset',
                    caption: "批量导入数据资产",
                    icon: 'icon-add'
                }, {
                    id: 'batchImportStandard_new',
                    caption: '批量导入数据标准',
                    icon: 'icon-add'
                }];
                if (!selectedItem || selectedItem.length == 0) {//当没有任何节点时也要允许新增节点
                    menuItems = [{
                        id: 'addBrotherDimItem',
                        caption: "新增同级",
                        icon: 'icon-add'
                    }, {
                        id: 'batchImportAsset',
                        caption: "批量导入数据资产",
                        icon: 'icon-add'
                    }];
                    if (isAddFolder != 'false') {
                        menuItems.push({
                            id: 'batchImportStandard_new',
                            caption: '批量导入数据标准',
                            icon: 'icon-add'
                        });
                    }
                }
                showMenu({
                    id: 'fapp-custom-NewGovernResource',
                    items: menuItems,
                    layoutTheme: 'small',
                    autoRestoreFocus: false,
                    showAt: sze.srcEvent,
                    commandProvider: {
                        getAllAvailableCommands: () => {
                            let filterTree = args.app.filterTree;
                            let selectedItem = filterTree.tree.getSelectedItem();
                            if (!selectedItem) {
                                return menuItems.map(item => item.id);
                            }
                            let queryInfo: QueryInfo = {
                                fields: [{ name: "name", exp: `model1.RULES_LIB_ID` }],
                                filter: [{ exp: `model1.DG_RES_CATALOG= '${selectedItem.id}'` }],
                                sources: [{
                                    id: "model1",
                                    path: "/sysdata/data/tables/dg/DG_RULES_LIB.tbl"
                                }]
                            };
                            return getQueryManager().queryData(queryInfo, uuid()).then((result: QueryDataResultInfo) => {
                                let data = result.data;
                                if (data.length > 0) {//目录下有数据，不能删除
                                    menuItems.remove(3);
                                }
                                // let maxLevel = args.app.renderer.browseDataMgr.filterTreeMaxLevel;
                                // if (selectedItem.level == maxLevel) {
                                // 	menuItems.remove(1);//去掉新增下级
                                // }
                                return menuItems.map(item => item.id);
                            });
                        },
                        cmd: (event: SZEvent, comp: Component, menuItem) => {
                            let id = menuItem.getId();
                            switch (id) {
                                case 'batchImportAsset': {
                                    doBatchImportAsset(args);
                                    break;
                                }
                                case 'batchImportStandard_new': {
                                    doBatchImportStandard_new(args);
                                    break;
                                }
                                case 'addBrotherDimItem': {
                                    let filterTree = args.app.filterTree;
                                    let selectedItem = filterTree.tree && filterTree.tree.getSelectedItem() as TreeItem;
                                    let parentId = selectedItem && selectedItem.getParent() && selectedItem.getParent().getId();
                                    filterTree.popupDimItemDialog({
                                        caption: `新增同级`,
                                        isNew: true,
                                        parentId: parentId
                                    }).then((dialog) => {
                                        let d = dialog;
                                        console.log(`dialog:${dialog}`);
                                        // d.content.items[0].setVisible(false);
                                        d.content.items[0].setValue(uuid());
                                    })
                                    break;
                                }
                                case 'addChildDimItem': {
                                    let filterTree = args.app.filterTree;
                                    let selectedItem = filterTree.tree && filterTree.tree.getSelectedItem() as TreeItem;
                                    let parentId = selectedItem ? selectedItem.getId() : '';
                                    filterTree.popupDimItemDialog({
                                        caption: `新增下级`,
                                        isNew: true,
                                        parentId: parentId
                                    }).then((dialog) => {
                                        let d = dialog;
                                        console.log(`dialog:${dialog}`);
                                        // d.content.items[0].setVisible(false);
                                        d.content.items[0].setValue(uuid());
                                    })
                                    break;
                                }
                                case 'editDimItem': {
                                    let filterTree = args.app.filterTree;
                                    let selectedItem = filterTree.tree && filterTree.tree.getSelectedItem() as TreeItem;
                                    let id = selectedItem ? selectedItem.getId() : null;
                                    if (id != null) {
                                        filterTree.popupDimItemDialog({
                                            caption: `编辑`,
                                            id: id
                                        }).then((dialog) => {
                                            let d = dialog;
                                            console.log(`dialog:${dialog}`);
                                            // d.content.items[0].setVisible(false);
                                        })
                                    }
                                    break;
                                }
                                case 'deleteDimItem': {
                                    let filterTree = args.app.filterTree;
                                    let selectedItem = filterTree.tree && filterTree.tree.getSelectedItem() as TreeItem;
                                    if (!selectedItem) {
                                        return;
                                    }
                                    filterTree.deleteDimItem();
                                    let data = selectedItem.getData().getData();
                                    let isSTD = data[17];
                                    if (isSTD == 'STD') {
                                        let task_id = 'STD_' + data[18];
                                        rc({
                                            url: `/rule/ruleCheck/deleteTask`,
                                            method: "POST",
                                            data: {
                                                task_id: task_id
                                            }
                                        })
                                    }
                                }
                            }
                        }
                    }
                })
                return false;
            }
        })
    },
    /**
    * 在批量删除之前检查是否有资源有未删除的自定义规则，如果有则弹出对话框提示删除其自定义规则，且不执行批量删除操作。
    *
    * @createdate 20210830
    * @author kongwq
     */
    onDeleteData: (event: FAppInterActionEvent): Promise<boolean> => {
        const allRulesLibID = [];
        event.app.dataList.dataList.ditems.forEach((item) => {
            if (item.checked === true) {
                allRulesLibID.push(item.getId())
            }
        });
        const queryInfo: QueryInfo = {
            fields: [{ name: "RULES_LIB_NAME", exp: `model1.RULES_LIB_ID.RULES_LIB_NAME` }],
            filter: [{ exp: `model1.RULES_LIB_ID in ("${allRulesLibID.join('", "')}")` }],
            sources: [{
                id: "model1",
                path: "/sysdata/data/tables/dg/DG_RULES.tbl"
            }]
        };
        const promise = getQueryManager().queryData(queryInfo, uuid()).then((result) => {
            let flag = true;
            const failDeleteSources = [];
            if (result && result.data && result.data.length > 0) {
                result.data.forEach((data) => {
                    failDeleteSources.push(data[0])
                })
                flag = false;
            }
            if (!flag) {
                showConfirmDialog({
                    caption: '提示',
                    message: `检测资源 [${failDeleteSources.toString()}] 中有自定义规则，不允许删除，请先删除自定义规则再删除资源`,
                    buttons: ['ok']
                });
            }
            return flag;
        });
        return promise;
    },
    CustomExpFunctions: {
        /**获取资源别名，如果有重复的就在后面加上序号的后缀 */
        getNewResidOhterName: (context: IExpEvalDataProvider, resid: string, modelpath: string): string | Promise<string> => {
            if (!resid || !modelpath) {
                return;
            }
            let queryInfo: QueryInfo = {
                fields: [{ name: "newName", exp: `max(model1.ALIAS)` }],
                filter: [{ exp: `model1.MAIN_TABLE_ID= '${resid}' and model1.ALIAS like '%_%'` }],
                sources: [{
                    id: "model1",
                    path: "/sysdata/data/tables/dg/DG_RULES_LIB.tbl"
                }, {
                    id: "model2",
                    path: "/sysdata/data/tables/meta/META_TABLES.tbl"
                }]
            };
            return getQueryManager().queryData(queryInfo, uuid()).then((result: QueryDataResultInfo) => {
                let data = result.data;
                if (!data || !data[0][0]) {
                    let path = modelpath;
                    let index1 = path.lastIndexOf('/');
                    let name_ = path.substring(index1 + 1);
                    let index2 = name_.lastIndexOf('.');
                    let name = name_.substr(0, index2);
                    return `${name}_1`;
                } else {
                    let maxName = data[0][0];
                    let index1 = maxName.lastIndexOf('_');
                    let maxIndex = parseInt(maxName.substring(index1 + 1));
                    let name_prex = maxName.substr(0, index1);
                    if (maxIndex == NaN) {
                        return `${name_prex}_${uuid().substr(0, 5)}_1`
                    }
                    return `${name_prex}_${maxIndex + 1}`
                }
            });
        },
        /**没有主键的资源不能检测 */
        checkResidPrimaryKey: (context: IExpEvalDataProvider, resid: string): string | Promise<string> => {
            if (!resid) {
                return "false";
            }
            return rc({
                url: `/rule/ruleCheck/checkResidPrimaryKey`,
                method: 'POST',
                data: {
                    resid: resid
                }
            }).then((result: string) => {
                if (result) {
                    return 'true'
                }
                return "false";
            });
        },
        /**获取资源名称，有描述就获取描述没有就获取名称 */
        getModelName: (context: IExpEvalDataProvider, resid: string, modelpath: string): string | Promise<string> => {
            if (!resid || !modelpath) {
                return;
            }
            let queryInfo: QueryInfo = {
                fields: [{ name: "name", exp: `model1.MODEL_NAME` }, { name: "desc", exp: "model1.MODEL_DESC" }],
                filter: [{ exp: `model1.MODEL_RESID= '${resid}' ` }],
                sources: [{
                    id: "model1",
                    path: "/sysdata/data/tables/meta/META_TABLES.tbl"
                }]
            };
            return getQueryManager().queryData(queryInfo, uuid()).then((result: QueryDataResultInfo) => {
                let data = result.data;
                let name = data[0][0];
                let desc = data[0][1];
                return desc != null ? desc : name;
            });
        },
        getTableFields: (context: IExpEvalDataProvider, resid: string): string | Promise<string> => {
            if (resid == null || resid.isBlank()) {
                return null;
            }
            console.log('resid---------------------' + resid);
            let queryInfo: QueryInfo = {
                sources: [{
                    id: 'model1',
                    path: '/sysdata/data/tables/meta/META_FIELDS.tbl'
                }],
                filter: [{ exp: `model1.FILE_ID='${resid}'` }],
                fields: [{
                    name: 'DB_FIELD',
                    exp: 'model1.DB_FIELD',
                }, {
                    name: 'FIELD_NAME',
                    exp: 'model1.FIELD_NAME',
                }]
            }
            return getQueryManager().queryData(queryInfo, uuid()).then((results: QueryDataResultInfo) => {
                let data = results.data;
                if (isEmpty(data)) {
                    return null;
                }
                let result = [];
                for (let i = 0; i < data.length; i++) {
                    let name = data[i][0];
                    let desc = data[i][1];
                    result.push(`${name}=${desc}`);//设置显示字段描述，并在数据表中存储字段名称
                }
                return result.join(',');
            });
        }
    },

    CustomActions: {
        syncDataStandardTable: (event: FAppInterActionEvent) => {
            let filterTree = event.app.filterTree;
            let selectItems = tree.getSelectedItems();
            if (selectItems.length == 0) {
                showWarningMessage("请选择一个数据标准目录再进行同步");
                return;
            }
            let item = selectItems[0];
            let data = item.getData();
            let mllxIndex = data.getDbfields().indexOf("MLLX");
            let mllx = data.getData()[mllxIndex];
            let standardIndex = data.getDbfields().indexOf("STD_LIB_ID");
            let standard_id = data.getData()[standardIndex];
            if (mllx == "STD" && !isEmpty(standard_id)) {
                rc({
                    url: `/rule/ruleCheck/syncDataStandardTable`,
                    method: "POST",
                    data: {
                        standard_id: standard_id
                    }
                }).then(result => {
                    let needAddTables = result.needAddTable;
                    let needDeleteTable = result.needDeleteTable;
                    showSuccessMessage(`本次新增${needAddTables.length}张表，删除${needDeleteTable.length}张表`);
                })
            } else {
                showWarningMessage("请选择数据标准目录进行同步");
            }
        }
    }
}

export const CustomJS: { [key: string]: ICustomJS } = {
    fapp: fappCustomJS as ICustomJS
}