# 常用语法

| 符号  |                     作用                      |
| :---: | :-------------------------------------------: |
|   y   |                      年                       |
|   M   |                      月                       |
|   d   |                      日                       |
| h / H |                  1-12 / 0-23                  |
|   m   |                      分                       |
| s / S |                   秒 / 毫秒                   |
|   E   |                     星期                      |
|   D   |                 一年中第几天                  |
|   F   |              一月中第几个星期几               |
| w / W |           一年 / 一月 中第几个星期            |
|   a   |              上午 / 下午 标记符               |
| k / K | 在一天中（1-24）时 / 在上午或者下午（0-11）时 |
|   z   |                     时区                      |



# String转Date

> 注意：需要将日期转换为：<button>时间毫秒数</button>

```ts
/**
 * 将日期字符串转换为时间毫秒数
 * @params strTime：日期字符串
 * @return 转换日期的毫秒数
 */
function dateStringToDate(strTime: string): number {
    const SimpleDateFormat = Java.type('java.text.SimpleDateFormat');
    const sdf = new SimpleDateFormat("yyyy-dd-mm hh:mm:ss");
    let time = 0;
    try {
        time = sdf.parse(strTime).getTime();
    } catch (e) {
        console.error(e);
    }
    return time;
}
```



```ts
/**
 * 注意：直接从PostMan复制出来的字符串空格，是一个：补换行空格（NBSP），会导致转换失败
 */
let SimpleDateFormat = Java.type('java.text.SimpleDateFormat');
let dateTimeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
let v = dateTimeStamp.parse("2022-06-08NBSP17:37:23.normalize('NFC'));
print(v);
```



```ts
"2022-06-08 17:37:23".normalize('NFC'); // es5不支持，将字符串格式化
```

```ts
// 将补换行空格替换为：普通空格
let removeSpace: string = "2022-06-08 17:37:23".replace("NBSP", " ");
or
let removeSpace: string = "2022-06-08 17:37:23".replace("\u00a0", " ");
```





