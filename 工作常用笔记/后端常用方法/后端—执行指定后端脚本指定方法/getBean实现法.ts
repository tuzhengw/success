/**
 * 2022-09-16 丁煜
 * https://jira.succez.com/browse/CSTM-20210?focusedCommentId=202231&page=com.atlassian.jira.plugin.system.issuetabpanels%3Acomment-tabpanel
 * 运行指定路径脚本并返回运行结果
 
   其他方法：https://jira.succez.com/browse/BI-46278
 */
import bean from "svr-api/bean";
import metadata from "svr-api/metadata";
function main() {
    let path = "/sysdata/data/batch/返回结果测试.action";
    let execute = new ExecuteScript(path);
    let result = execute.runFunction();
    print(result)
}

class ExecuteScript {
    constructor(path: string) {
        this.path = path;
        this.implementFunction();
    }

    /**脚本运行结果*/
    private result;
    /**脚本路径*/
    private path: string;

    private implementFunction() {
        let path: string = this.path;
        let repo = bean.getBean("com.succez.metadata.service.MetaServiceRepository");
        let file: MetaFile = metadata.getFile(path, false).getInnerWrappedObject();
        let serviceCache = bean.getBean("com.succez.metadata.service.MetaServiceCache")
        if (!file) {
            this.result = null;
        }
        let cjs = serviceCache.getBusinessObject(file, com.succez.dev.script.engine.CompiledJSScript.class);
        print(cjs.getClass().getMethods());
        let cls = cjs.getClass();
        print(cjs.t("main",false))
        if (cjs != null) {
            //let runner: CompiledJSScriptRunner;

            let runner = cjs.c(null);
            print(runner.getClass().getMethods());
            let func = runner.getFunction("main", false);
            print(func.getClass())
            print(func.getClass().getMethods())
            print(func.F())
        }
    }

    /**
     * 运行指定路径脚本并返回运行结果
     */
    public runFunction() {
        return this.result;
    }
}