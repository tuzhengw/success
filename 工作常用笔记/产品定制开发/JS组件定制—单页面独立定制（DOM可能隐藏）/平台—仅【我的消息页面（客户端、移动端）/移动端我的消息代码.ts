import { Component, ComponentArgs, ctx, message, SZEvent } from 'sys/sys';
import 'css!./customComponent.css';
import { MMySMessagePage } from "me/smessage";

/**
 * 20220304 tuzw
 * 问题：单独将其移动端我的消息页面抽离出来
 * 帖子：https://jira.succez.com/browse/CSTM-17807
 * 
 * 20220613 tuzw
 * 问题：PC端显示不需要搜索栏和选项卡
 */
export class SoleMyMessagePage extends MMySMessagePage {

    /** 是否隐藏顶部搜索栏 */
    public isNoDisplaySearch: boolean;
    /** 是否隐藏顶部选项卡 */
    public isNoDisplayChoseItem: boolean;

    protected _init(args: any): HTMLElement {
        let domBase = super._init(args);
        this.isNoDisplaySearch = args.isNoDisplaySearch == 'true';
        this.isNoDisplayChoseItem = args.isNoDisplayChoseItem == 'true';
        this.setMessagePageDom(domBase);
        this.hidePageDom(domBase);
        return domBase;
    }

    /**
     * 将产品模块原所在父DOM样式迁移到现在所在父DOM下
     * 由于单独抽取消息模块显示，最外层父DOM样式丢失，导致整体无法显示，这里需要将原父DOM样式重新设置
     * @param domBase: HTMLElement
     */
    private setMessagePageDom(domBase: HTMLElement): void {
        domBase.style.position = "relative";
        domBase.style.height = "100%";
    }

    /**
     * 校验是否需要隐藏顶部搜索栏和选项卡
     */
    public hidePageDom(domBase: HTMLElement): void {
        let messagePageDoms: NodeListOf<ChildNode> = domBase.firstChild.firstChild.childNodes;
        for (let i = 0; i < messagePageDoms.length; i++) {
            let childrenClassName: string = messagePageDoms[i].className;
            if (this.isNoDisplaySearch && childrenClassName == "mmysmessagepanel-search" ||
                this.isNoDisplayChoseItem && childrenClassName == "mmysmessagepanel-tabbar") {
                domBase.firstChild.firstChild.childNodes[i].style.display = 'none';
            }
        }
    }
}
