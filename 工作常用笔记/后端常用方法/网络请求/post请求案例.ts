/**
 * 20210302 季淼淼
 *
 * 20220815 tuzw
 * 1 由于发送数据量很大，查询过慢，改为分页查询发送
 * 2 计划任务执行间隔20分钟太短，导致执行冲突，调整每隔3小时执行
 * 地址：https://jira.succez.com/browse/CSTM-20045
 * 
 * 注意：
 * 1）要指定综合监管节点执行脚本，否则：IP未授权无法发送短信
 * 2）地址：http://2.20.103.167:8080/zhjg/ZHJG/tasks
 */
const HttpClients = Java.type("org.apache.http.impl.client.HttpClients");
const HttpPost = Java.type("org.apache.http.client.methods.HttpPost");
const StringEntity = Java.type("org.apache.http.entity.StringEntity");
const EntityUtils = Java.type("org.apache.http.util.EntityUtils");
const RequestConfig = Java.type("org.apache.http.client.config.RequestConfig");

import db from "svr-api/db";
import { getJSON, getString, modifyFile } from "svr-api/metadata";

/** 单次发送数量 */
const ONCE_SEND_MAX_NUMS = 1000;
/** 每次执行最高发送条数 */
const SUM_SEND_NUMS = 10000;
/** 外部控制脚本是否停止文件路径 */
const isStopExecuteFilePath: string = '/ZHJG/script/isStopSendMessage.txt';
/**
 * 脚本主入口
 */
function main() {
    print(`start----开始执行发送短信脚本`);
    let startTime: number = Date.now();
    let sendMaxNums: number = getSendMaxNums();
    let sendCount: number = Math.ceil(sendMaxNums / ONCE_SEND_MAX_NUMS);
    print(`当前共需要处理：${sendCount} 次，每次处理：${ONCE_SEND_MAX_NUMS} 条数据`);
    for (let queryIndex = 0; queryIndex < sendCount; queryIndex++) {
        let isStop: string = getString(isStopExecuteFilePath);
        if (isStop == "true") {
            print(`手动停止发送`);
            break;
        }
        print(`（${queryIndex + 1}）开始处理第 ${queryIndex + 1} 次短信数据`);
        let sendMessageContents: MESSAGES_SMS_TABLE_NEW[] = getSendMessages(queryIndex);
        for (let i = 0; i < sendMessageContents.length; i++) {
            let isStop: string = getString(isStopExecuteFilePath);
            if (isStop == "true") {
                print(`手动停止发送`);
                break;
            }
            if (i % 300 == 0) {
                print(`当前正在发送第：${i + 1} 条短信消息`);
            }
            let messageContent: MESSAGES_SMS_TABLE_NEW = sendMessageContents[i];
            let rs = sendSMS([messageContent.PHONES], messageContent.SMS_CONTENT);
            if (!rs.result) {
                print(`第【${i + 1}】 条短信发送失败，内容：`);
                print(messageContent);
                continue;
            }
            let returnMessage: string = rs.returnMessage;
            let sIdx = returnMessage.indexOf("<return>");
            let e = returnMessage.indexOf("</return>")
            let mess = returnMessage.substring(sIdx + 8, e);
            writeLog(messageContent.SMS_ID, mess.split(",")[1], mess.split(",")[0]);
        }
    }
    print(`end----发送短信脚本执行结束，总耗时：${(Date.now() - startTime) / 1000} s`);
}

/**
 * 发送短信
 *
 * @param phones 手机号
 * @param messageInfo 消息主体，可能是一个json，也可能是一个 string，调用 sys 模块的发送短信接口 `sendSMS` 时传递的参数是什么，这里就是什么
 * @returns 返回短信服务商服务器发送短信后的返回数据，数据将会被记录到日志表中，为方便调试以及定位错误，请务必要设置返回值
 */
function sendSMS(phones: string[], messageInfo: JSONObject | string): { result: boolean, returnMessage: string } {
    let title = `应用支撑平台验证码：`;
    /**
     * 20210302 zhangd 最新短信服务地址
     * let url = "http://sms.shetuan365.cn:88/services/smsService";
     */
    let url = "http://2.22.67.1:88/SmsService";
    let xmlStr = getXMLStr(phones, messageInfo);
    let sendResult = postXml(url, xmlStr);
    return { result: sendResult.result, returnMessage: sendResult.returnMessage };
}

/** 
 * 获取请求体内容
 * @param phones 发送手机号
 * @param messageInfo 消息内容
 * @return 
 */
function getXMLStr(phones: string[], messageInfo: JSONObject | string): string {
    const username = "shichangjgj";
    const password = "nHFXCQ";
    const senderid = "231";
    const isback = "false";
    let phoneStr = ``;
    let beforePhone = `<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:cli="http://client.webservices.kaiyun.com/">
                <soapenv:Header/>
                <soapenv:Body>
                    <cli:sendSms>
                        <arg0>`+ username + `</arg0>
                        <arg1>`+ password + `</arg1>
                        <arg2>
                        <content>`+ messageInfo + `</content>
                        <isback>`+ isback + `</isback>`;
    for (let i = 0; i < phones.length; i++) {
        phoneStr += `<mobiles>` + phones[i] + `</mobiles> `;
    }
    let afterPhone = `<senderid>` + senderid + `</senderid>
                    </arg2>
                    </cli:sendSms>
                </soapenv:Body>
            </soapenv:Envelope>`;
    return `${beforePhone}${phoneStr}${afterPhone}`;
}

/**
 * httpPost方式请求webService
 * @param url 发送短信服务地址
 * @param json 短信内容
 * @retrn
 */
function postXml(url: string, json: string): { result: boolean, returnMessage: string } {
    let httpclient = HttpClients.createDefault();
    let httpPost = new HttpPost(url);

    let requestConfig = RequestConfig.custom().setConnectionRequestTimeout(1000)
        .setSocketTimeout(3000).setConnectTimeout(1000).build();
    httpPost.setConfig(requestConfig);

    let returnMessage = "";
    let result: boolean = true;
    httpPost.setHeader("Content-Type", "text/xml;charset=utf-8");
    httpPost.setHeader("SOAPAction", "");
    httpPost.setHeader("User-Agent", "Apache-HttpClient/4.1.1 (java 1.5)");
    httpPost.setHeader("Accept-Encoding", " gzip,deflate");

    let s = new StringEntity(json.toString(), 'utf-8');
    httpPost.setEntity(s);
    try {
        let res = httpclient.execute(httpPost);
        let status = res.getStatusLine();
        if (status.getStatusCode() == 200) {
            /**
             * <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
             * <soap:Body><ns2:sendSmsResponse xmlns:ns2="http://client.webservices.kaiyun.com/">
             * <return>true,68195fab0c9b4cbe9ef8083e1f6e0858</return>
             * </ns2:sendSmsResponse></soap:Body></soap:Envelope>
             */
            returnMessage = EntityUtils.toString(res.getEntity(), 'utf-8');
        }
    } catch (e) {
        print(` httpPost方式请求webService失败----error`);
        returnMessage = e.toString();
        result = false;
        print(e);
    }
    return { result: result, returnMessage: returnMessage };
}

/**
 * 获取总共需要发送的数据量
 */
function getSendMaxNums(): number {
    let ds = db.getDataSource("zhjg_ods");
    let querySQL: string = `
        select 
            count(1) as MAXNUMS
        from 
            MESSAGES_SMS_TABLE_NEW 
        where 
            CREATE_TIME > trunc(sysdate-1) and SMS_ID not in (select SMS_ID from MESSAGES_SMS_LOG where zt='true') and SJLY='1'
        order by CREATE_TIME ASC
    `;
    let maxNums: number = ds.executeQuery(querySQL)[0]["MAXNUMS"];
    print(`当前需要发送的消息的总数量为：${maxNums}`);
    return maxNums;
}

/**
 * 获取发送的消息内容
 * @description 过滤掉综合监管的短信信息，只发当天失败的消息或者未成功的消息
 * @description 由于数据量很大，每页最大查询1000条
 * @param pageIndex 查询起始分页号，eg：0，1，2
 * @return 
 */
function getSendMessages(pageIndex: number): MESSAGES_SMS_TABLE_NEW[] {
    let ds = db.getDataSource("zhjg_ods");
    let querySQL: string = `
        SELECT SMS_ID,SMS_CONTENT,phones FROM (
            select 
                SMS_ID,SMS_CONTENT,phones,ROWNUM RN
            from 
                MESSAGES_SMS_TABLE_NEW 
            where 
                CREATE_TIME > trunc(sysdate-1) and SMS_ID not in (select SMS_ID from MESSAGES_SMS_LOG where zt='true') and SJLY='1'
            order by CREATE_TIME ASC
        ) mes WHERE RN > ? AND RN <= ?
    `;
    let queryData: MESSAGES_SMS_TABLE_NEW[] = ds.executeQuery(querySQL, [pageIndex * ONCE_SEND_MAX_NUMS, pageIndex * ONCE_SEND_MAX_NUMS + ONCE_SEND_MAX_NUMS]) as MESSAGES_SMS_TABLE_NEW[];
    return queryData;
}

/**
 * 写入日志表
 * @param mesId 消息序号
 * @param resMes 消息回执
 * @param state 发送状态，eg：'false'、'true'
 */
function writeLog(mesId: string, resMes: string, state: string): void {
    if (state == 'false') {
        print(`${mesId} ：发送失败，错误原因：${resMes}`);
    }
    let ds = db.getDataSource("zhjg_ods");
    let dt = ds.openTableData("MESSAGES_SMS_LOG");
    dt.insert({
        SMS_ID: mesId,
        FSSJ: getCurrentTime(),
        XXHZ: resMes,
        zt: state
    });
}

/**
 * 获取当前时间（格式化后的）
 */
function getCurrentTime(): string {
    var sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    var str = sdf.format(java.lang.System.currentTimeMillis());
    return sdf.parse(str);
}

/** 消息主体 */
interface MESSAGES_SMS_TABLE_NEW {
    /** 消息序号 */
    SMS_ID: string;
    /** 发送消息内容 */
    SMS_CONTENT: string;
    /** 电话号码 */
    PHONES: string;
}