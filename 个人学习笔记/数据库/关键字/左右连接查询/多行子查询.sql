/**
 * 
	（1）in 
		等于列表中的《任何值》
		
	（2）ANY,SOME 
		父查询中的结果集《大于》子查询中《任意一个》结果集中的值，则为：true
		
	（3）ALL
		父查询中的结果集大于子查询中《每一个》结果集中的值，则为：true
 
 */
1.
SELECT * FROM T2 
WHERE
	N > all(SELECT N FROM T1)
 
2.
SELECT * FROM T2 
WHERE 
	N > any(SELECT N FROM T1)

3.
SELECT * FROM T2 
WHERE 
	N > some(SELECT N FROM T1) 