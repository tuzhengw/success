import "css!./data-process.css";
import { DataFlowArgs, ResInfo, ScheduleConfPage, ScheduleLogInfo, ScheduleMgrPage, ScheduleMgrSubPage, ScheduleMgrSubPageContanier, ScheduleNewLogPage, SUB_PAGE_TYPE, TaskDataFlow, TaskDataFlowDataProvider, TaskDataProvider, TaskFlowNodeInfo, TaskListPage } from "schedule/schedulemgr";
import { InterActionEvent } from "metadata/metadata-script-api";
import { MetaModulePage, MetaModulePageArgs, ParentDirDataProvider } from "metadata/metamgr";
import { Grid, GridCell, ListItem, GridRow, List } from "commons/tree";
import {
	SZEvent, showMenu, formatDateFriendly, Component, Cancelable, CommandItemInfo, throwError, isEmpty, showErrorMessage, rc, timerv, showConfirmDialog,
	uuid, showProgressDialog, showWaiting, throwInfo, showWarningMessage, message, showToolTip, parseDate, UrlInfo
} from "sys/sys";
import { getCurrentUser, getOutputDbTable, getProjectName } from "metadata/metadata";
import { FlowDiagram, FlowNode, FlowNodeInfo } from "commons/flowDiagram";
import { Link, Toolbar, ToolbarItemAlign, ToolbarItemType } from "commons/basic";
import { getScheduleMgr, TaskLogInfo } from "schedule/schedulemgr";
import { Form, FormItemArgs } from "commons/form";

let ztScheulePage;
export function getZTDataMgrPage() {
	return ztScheulePage;
}

/**
 * 计划日志界面内部数据节点数据结构信息。
 */
interface TaskFlowNode extends FlowNode {
	data: TaskFlowNodeInfo;
}
const TOOLBARITEMS = [{
	align: ToolbarItemAlign.RIGHT, id: "searchTaskNode", layoutTheme: "noborder", toolbarItemType: ToolbarItemType.SEARCHBOX, resultCountVisible: true, resultIconVisible: true, emptyInputSearchEnabled: false, buttonModeEnabled: true, circleLocateEnabled: true, collapseWhenBlur: true, visible: true, fixed: false, icon: "icon-zoomout"
}, {
	align: ToolbarItemAlign.RIGHT, id: "zoomOut", layoutTheme: "smallbtn", toolbarItemType: ToolbarItemType.BUTTON, visible: true, fixed: false, icon: "icon-zoomout"
}, {
	align: ToolbarItemAlign.RIGHT, id: "scale", layoutTheme: "smallbtn", caption: "100%", toolbarItemType: ToolbarItemType.BUTTON, visible: true, fixed: false
}, {
	align: ToolbarItemAlign.RIGHT, id: "zoomIn", layoutTheme: "smallbtn", toolbarItemType: ToolbarItemType.BUTTON, visible: true, fixed: false, icon: "icon-zoomin"
}, {
	align: ToolbarItemAlign.RIGHT, id: "zoomRestore", layoutTheme: "smallbtn", toolbarItemType: ToolbarItemType.BUTTON, visible: true, fixed: false, icon: "icon-originalsize"
}];
/**
 * 20220608
 * liuyz
 *  https://jira.succez.com/browse/BI-44747
 * 计划任务中的流向图能够支持自动添加系统节点和末端的数据源节点
 */
export class SDI_TASK_FLOW extends Component {
	private taskDataFlow: SdiTaskDataFlow;
	private toolbar: Toolbar;
	/** 轮询任务状态，计划发生变化时需要重新查询任务状态 */
	private needTryAgain: boolean;

	private schedule: ScheduleInfo;
	private pageContainer: ScheduleMgrSubPageContanier;
	/**
	 * 轮询任务状态的timer对象。
	 */
	private pollingTimer: Cancelable;
	private dataFlowDp: TaskDataFlowDataProvider;
	/** 当前剩余尝试轮询次数 */
	private remainPollingTimes: number;
	/** 内置对象 */
	private innerComponent: SdiTaskDataFlow;

	/**
	 * @param args.scheduleId 计划ID
	 * @param args.isNeedToolBar? 是否需要工具栏，默认：需要
	 */
	public _init(args): HTMLElement {
		let domBase = super._init(args);
		this.addDomStyle(domBase);

		let scheduleId = args.scheduleId;
		if (isEmpty(scheduleId)) {
			showErrorMessage("请指定一个计划任务id");
			return domBase;
		}
		let isNeedToolBar: boolean = args.isNeedToolBar;
		if (isEmpty(isNeedToolBar)) {
			isNeedToolBar = true;
		}
		if (isNeedToolBar) {
			let toolbar = this.toolbar = new Toolbar({
				domParent: domBase,
				items: TOOLBARITEMS,
				cssText: "height: 30px;",
				commandProvider: this
			});
		}
		this.pageContainer = args.pageContainer;
		let dp = this.dataFlowDp = new TaskDataFlowDataProvider();
		dp.setLogInfo("sys", <any>{ scheduleId });
		this.taskDataFlow = this.innerComponent = new SdiTaskDataFlow({
			domParent: domBase,
			pageContainer: this.pageContainer,
			commandRenderer: this.pageContainer,
			dataProvider: dp,
			id: "custom-scheduleMgr-id",
			className: "scheduleMgr-subPage-content",
			cssText: "height: 100%;  position: relative;"
		});
		this.doBeforeShow();
		return domBase;
	}

	private addDomStyle(domBase: HTMLElement): void {
		domBase.style.width = '100%';
	}

	public cmd_fallback?(sze: SZEvent, component: Component, item?: any): any {
		let cmd = sze?.cmd;
		if (cmd == "locateNode") {
			let node = <FlowNode>item;
			this.taskDataFlow.select1(node);
			this.taskDataFlow.locateNode(node);
			return;
		}
		if (cmd == "searchNodes") {
			return this.taskDataFlow.searchNodes(sze.data);
		}
		return this.taskDataFlow.cmd(sze, component, item);
	}

	public refresh() {
		return this.taskDataFlow.refresh();
	}

	/**
	 * @implements {IPage}
	 */
	public doBeforeShow(): void {
		this.startPollingDataFlowState(this.needTryAgain);
	}

	/**
	 * 开始轮询调度流程图，如果当前有任务正在执行，那么会轮询任务状态。
	 * 轮询的时候会尝试三次，如果三次请求都没有任务正在执行，那么就结束轮询。
	 * @param tryAgain 是否尝试再次轮询。
	 */
	public startPollingDataFlowState(tryAgain = true) {
		if (this.pollingTimer) {
			return;
		}
		let doHeartbeat = (): Promise<boolean> => {
			let nodes = this.taskDataFlow.getData();
			let executing = nodes && nodes.find(e => e.state === DW_RESOURCE_STATE.EXECUTING || e.state === DW_RESOURCE_STATE.WAIT_RETRY || e.state === DW_RESOURCE_STATE.WAIT);
			if (tryAgain && !executing) {
				this.remainPollingTimes = 5;
				this.needTryAgain = false;
			} else if (executing) {
				this.remainPollingTimes = 0;
			}
			if (!executing && this.remainPollingTimes <= 0) {
				this.dataFlowDp.log.state = DW_RESOURCE_STATE.NONE;
				return Promise.resolve(false);
			}
			this.dataFlowDp.log.state = DW_RESOURCE_STATE.EXECUTING;
			this.taskDataFlow.refresh();
			this.remainPollingTimes--;
			tryAgain = false;
			return Promise.resolve(true);
		}
		this.pollingTimer = timerv({
			action: doHeartbeat,
			interval: 1000
		});
	}

	public refreshCommands(cmds: Array<string>) {
		return <Promise<any>>Promise.all([this.toolbar.refreshCommands(cmds)]);
	}

	/**
	 * @implements {ScheduleMgrSubPage}
	 */
	public setSchedule(scheduleInfo: ScheduleInfo): void {
		if (this.schedule?.id == scheduleInfo.id) {
			return;
		}
		this.schedule = scheduleInfo;
		this.needTryAgain = true;
		this.dataFlowDp.setLogInfo((<ScheduleMgrPage>this.pageContainer).projectName, <ScheduleLogInfo>{
			scheduleId: scheduleInfo.id,
			state: DW_RESOURCE_STATE.NONE
		});
	}

	public getAllAvailableCommands(pcmdInfo?: CommandItemInfo): string[] {
		return this.taskDataFlow.getAllAvailableCommands();
	}

	public fetchCommandsProperties?(cmds: Map<string, CommandItemInfo>): Promise<Map<string, CommandItemInfo>> | Map<string, CommandItemInfo> {
		return this.taskDataFlow.fetchCommandsProperties(cmds);
	}

	public dispose() {
		this.cancelPolling();
		this.taskDataFlow?.dispose();
		super.dispose();
	}

	/**
	 * @implements {ScheduleMgrSubPage}
	 */
	public notifychange(type: string) {
		if (type == "tasksChange" && !this.pollingTimer) {
			this.needTryAgain = true;
		}
	}

	/**
	 * @implements {IPage}
	 */
	public doBeforeHide(): void {
		this.cancelPolling();
	}

	public cancelPolling() {
		this.pollingTimer?.cancel();
		this.pollingTimer = undefined;
	}
}

/**
 * 20220608
 * liuyz
 * 定制流向图，单独处理对系统节点和数据源节点的操作
 */
export class SdiTaskDataFlow extends TaskDataFlow {
	constructor(args: DataFlowArgs) {
		super(args);
	}

	protected _init(args: DataFlowArgs): HTMLElement {
		let domBase = super._init(args);

		return domBase;
	}

	public dispose(): void {
		super.dispose();
	}

	protected _init_default(args: DataFlowArgs): void {
		super._init_default(args);
	}

	public fetchCommandsProperties(cmds: Map<string, CommandItemInfo>): Promise<Map<string, CommandItemInfo>> | Map<string, CommandItemInfo> {
		let promises: Promise<any>[] = [];
		let map: Map<string, CommandItemInfo> = new Map();
		for (const [cmd, value] of cmds) {
			let info = map.get(cmd) || { id: value.id, cmd: value.cmd, enabled: false, visible: false };
			switch (cmd) {
				case "scale": {
					map.set(cmd, { cmd: cmd, caption: Math.floor((this.scale * 100)) + "%" });
					break;
				}
				case "zoomIn": {
					map.set(cmd, { cmd: cmd, enabled: this.scale < 5 });
					break;
				}
				case "zoomOut": {
					map.set(cmd, { cmd: cmd, enabled: this.scale > 0.2 });
					break;
				}
				case "zoomRestore": {
					map.set(cmd, { cmd: cmd, enabled: this.scale !== 1 });
					break;
				}
				case "showDatasourceNode": {
					map.set(cmd, { cmd: cmd, checked: this.dataSourceVisible });
					break;
				}
				case "showErrorNode": {
					map.set(cmd, { cmd: cmd, checked: this.onlyErrorNode });
					break;
				}
				case "showAllNode": {
					map.set(cmd, { cmd: cmd, checked: !this.onlyErrorNode });
					break;
				}
				case "executeCurrent":
				case "executeAll":
				case "executeDepends":
				case "executeImpacts": {
					let nodes = this.getSelectedNodes();
					let len = nodes.length;
					let node0 = nodes[0];
					let info: CommandItemInfo = { cmd: cmd, html: len === 1 ? message("schedule.dataflow.menu." + cmd, node0.data.name) : message("schedule.dataflow.menu.multiple." + cmd) };
					if (len === 1 && !node0.data.inputLinks.length && cmd === "viewDepends") {
						info.enabled = false;
					}
					if (cmd == "executeCurrent" || cmd == "executeAll" || cmd == "executeDepends" || cmd == "executeImpacts") {
						info.visible = false;
						info.enabled = false;
					}
					if (!this.executeEnable) {
						info.visible = info.enabled = false;
						map.set(cmd, info);
						break;
					}
					let hasNoScheduleNode = false;
					let allNoSchedule = true;
					nodes.forEach(e => {
						if (!e.data.prevNodes.length) {
							hasNoScheduleNode = true;
						} else {
							allNoSchedule = false;
						}
					})
					info.visible = !allNoSchedule;
					info.enabled = !hasNoScheduleNode;
					map.set(cmd, info);
					break;
				}
				case "searchTaskNode":
				case "viewAll":
				case "viewDepends":
				case "viewImpacts": {
					let nodes = this.getSelectedNodes();
					let len = nodes.length;
					let node0 = nodes[0];
					let info: CommandItemInfo = { cmd: cmd, html: len === 1 ? message("schedule.dataflow.menu." + cmd, node0.data.name) : message("schedule.dataflow.menu.multiple." + cmd) };
					if (len === 1 && !node0.data.inputLinks.length && cmd === "viewDepends") {
						info.enabled = false;
					}
					info.visible = info.enabled = true;
					map.set(cmd, info);
					break;
				}
				case "locateRes": {
					let node = this.getSelectedNode();
					if (node.data.path) {
						promises.push(getCurrentUser().checkAllowed({ path: node.data.path, operation: "view-basic" }).then((flag: boolean) => {
							info.visible = flag;
							map.set(cmd, info);
						}))
					} else {
						info.visible = false;
						map.set(cmd, info);
					}
					break;
				}
			}
		}
		return Promise.all(promises).then(() => map);
	}

	public refreshCommands(cmds: Array<string>) {
		this.commandRenderer && this.commandRenderer.refreshCommands(cmds);
	}

	/**
	 * 布局规则：
	 * 1. 按列渲染，首先render第一列，业务数据库物理表节点，然后ods模型，然后主体模型。
	 * 2. 相同数据源的物理表放一起。
	 * 3. 如果没有ods模型，那么不显示ods模型这一列。
	 * 4. 连线尽量不交叉。
	 *
	 * 2022-06-07 liuyz
	 * 重构流向图展示的内容，当所有的节点列表中存在类型为system节点时，第一列应该是系统，第二列才是物理表
	 */
	public layout(): void {
		let nodes: TaskFlowNode[] = [];
		let dMap: Map<string, boolean> = new Map();
		let data = this.data;
		let edges: Array<{ source: TaskFlowNode, target: TaskFlowNode, animate?: boolean }> = [];
		let map: Map<string, TaskFlowNode> = new Map();
		let row = 0;
		let r: Array<TaskFlowNodeInfo> = data;
		let r1: Array<TaskFlowNodeInfo> = [];
		let i = 0;
		let level = 0;
		let repeatTime = 0;
		let hasSystemNode = false;
		data.forEach(e => {
			dMap.set(e.id, true);
			e.isSystem && (hasSystemNode = true);

		});
		let addNode = (e: TaskFlowNodeInfo, m: Map<string, TaskFlowNode>) => {
			let node = {
				id: e.id,
				data: e,
				row: i,
				level: level,
				x: level * (200 + (level > 1 ? 50 : 0)) + (level < 2 ? 30 : 0),
				y: (i - 1) * 60 + 40,
				selected: e.selected
			}
			nodes.push(node);
			m.set(e.id, node);
		}

		// 将节点按层级分组开来，如果当前引用的节点都已经在前面层级显示，那么此节点就在当前层级，否则往下个层级移，递归寻找层级。
		let dealwithNode = (data: Array<TaskFlowNodeInfo>) => {
			let m: Map<string, TaskFlowNode> = new Map();
			i = 0;
			data.forEach(e => {
				if (level === 0) {
					if (hasSystemNode && e.isSystem) {//如果有系统，那么系统应该是第一层节点
						i++;
						addNode(e, m);
					} else if (!hasSystemNode && ((e.isDtable && !e.isOutputDbtable) || e.isFile)) {//当没有系统节点时，物理表和文件是第一层级节点
						i++;
						addNode(e, m);
					} else {
						r1.push(e);
					}
				} else if ((hasSystemNode && level == 1)) {
					if ((e.isDtable && !e.isOutputDbtable) || e.isFile) {//当没有系统节点时，物理表和文件是第一层级节点
						i++;
						addNode(e, m);
					} else {
						r1.push(e);
					}
				} else if ((hasSystemNode && !e.isSystem) || (!hasSystemNode && !(e.isDtable && !e.isOutputDbtable) && !e.isFile && !e.isSystem)) {
					// 如果有前面来源节点不在前面已经生成的层次，那么此节点应该排除在当前正在生成的层次节点中。
					if (e.prevNodes.length && e.prevNodes.some(e1 => !map.get(e1.id) && dMap.get(e1.id))) {
						r1.push(e);
					} else {
						i++;
						addNode(e, m);
					}
				}
			});
			if (i > row) {
				row = i;
			}
			level++;
			let r = r1;
			r1 = [];
			m.forEach((v, e) => {
				map.set(e, v)
			});
			if (r.length && r.length == data.length) {
				repeatTime++;
			} else {
				repeatTime = 0;
			}
			// 当不能添加任何一个节点时，说明剩下的节点全都位于循环引用链中，抛出异常提示。
			if (repeatTime > 2) {
				let getdesc = (node: FlowNodeInfo) => node.path || `${node.dataSource}/${node.schema}/${node.name}`;
				throwError({
					errorCode: "err.schedule.dataflow.circularRef",
					errorType: "error",
					properties: {
						references: r.map(i => {
							return {
								path: getdesc(i),
								inputs: i.prevNodes && i.prevNodes.map(input => getdesc(input))
							}
						})
					}
				});
			}
			r.length && dealwithNode(r);
		}
		dealwithNode(r);

		// 为节点准备一些后面算法需要的信息，提高点性能。
		nodes.forEach(e => {
			let prevNodes = e.data.prevNodes;
			let bak = <any>e;
			// 依赖节点的map
			bak.preMap = new Map();
			prevNodes.forEach(e => bak.preMap.set(e.id, e));
			// 依赖节点的个数
			bak.preLen = prevNodes.length;
		});

		let nodesImpactMap: Map<string, Map<string, { node: TaskFlowNode, index: number }>> = new Map();

		let levelNodesMap: Map<number, Array<TaskFlowNode>> = new Map();
		// 将节点排序，从层级深的开始排，依赖相似度搞的放一起，依赖节点多的放前面。
		for (let i = level - 1; i >= 0; i--) {
			let levelnodes = nodes.filter(e => e.level === i);
			levelnodes.length > 0 && levelnodes.sort((a, b) => {
				let indexA = 1000000;
				let indexB = 1000000;
				let aData = a.data;
				let bData = b.data;
				let aId = aData.id;
				let bId = bData.id;
				if (nodesImpactMap.has(aId)) {
					let impactMap = nodesImpactMap.get(aId);
					let sumIndex = 0;
					let count = 0;
					let othersIndex = 0;
					let othersCount = 0;
					impactMap.forEach(e => {
						if (e.node.level === (i + 1)) {
							sumIndex += e.index
							count++;
						} else {
							othersIndex += e.index;
							othersCount++;
						}
					});

					let index = count > 0 ? sumIndex / count : othersCount > 0 ? othersIndex / othersCount : Infinity;
					if (index < indexA) {
						indexA = index + indexA - Math.floor(indexA);
					}
					indexA += 0.001;
				}
				if (nodesImpactMap.has(bId)) {
					let impactMap = nodesImpactMap.get(bId);
					let sumIndex = 0;
					let count = 0;
					let othersIndex = 0;
					let othersCount = 0;
					impactMap.forEach(e => {
						if (e.node.level === i + 1) {
							sumIndex += e.index;
							count++;
						} else {
							othersIndex += e.index;
							othersCount++;
						}
					});
					let index = count > 0 ? sumIndex / count : othersCount > 0 ? othersIndex / othersCount : Infinity;
					if (index < indexB) {
						indexB = index + indexB - Math.floor(indexB);
					}
					indexB += 0.001;
				}
				let n = indexA - indexB;
				if (n === 0) {
					n = (<any>b).preLen - (<any>a).preLen;
				}
				return n;
			})
			if (i !== 0) {
				levelnodes.forEach((e, i) => {
					let prevNodes = e.data.prevNodes;
					let similarity = 0;
					let index = -1;
					let swticNode;
					for (let j = i + 1, len = levelnodes.length; j < len; j++) {
						let e1 = levelnodes[j];
						let baksimilarity = 0;
						if (e1 !== e) {
							let preMap = (<any>e1).preMap;
							prevNodes.forEach(p => {
								if (preMap.get(p.id)) {
									baksimilarity++;
								}
							})
						}
						if (baksimilarity > similarity) {
							similarity = baksimilarity;
							index = j;
							swticNode = e1;
						}
					}
					if (index !== -1) {
						levelnodes.splice(index, 1);
						levelnodes.splice(i + 1, 0, swticNode);
					}
				});
			}
			levelnodes.forEach((e, i) => {
				e.data.prevNodes.forEach(e1 => {
					if (!nodesImpactMap.has(e1.id)) {
						nodesImpactMap.set(e1.id, new Map());
					}
					nodesImpactMap.get(e1.id).set(e.data.id, { index: i, node: e });
				});
			});
			levelNodesMap.set(i, levelnodes);
		}

		let nodesIndexMap: Map<string, number> = new Map();
		for (let i = level - 1; i >= 0; i--) {
			let levelnodes = levelNodesMap.get(i);
			levelnodes.forEach((e, i) => nodesIndexMap.set(e.data.id, i));
		}

		// 决定节点的Y坐标，根据节点依赖节点的位置，尽量让节点在依赖节点中间显示，但是尽量会让相邻的层级的结点连线短。
		let maxY = 0;
		for (let i = 0; i < level; i++) {
			let levelnodes = levelNodesMap.get(i);
			if (i === 0) {
				levelnodes.forEach((e, i) => {
					e.row = i + 1;
					e.y = i * 35 + 10;
				})
				maxY = Math.max(maxY, (levelnodes.length - 1) * 35 + 50);
			} else {
				let lastSumY = 10;
				levelnodes.forEach((e, i) => {
					let prevNodes = e.data.prevNodes;
					let level = e.level;
					let sumY = 0;
					let count = 0;
					// 记录每个层级依赖的节点个数，以及纵坐标和，便于计算当前节点纵坐标。
					let levelPrevNodeMap: Map<number, { count: number, sumY: number }> = new Map();
					let minLevel = level;
					prevNodes.forEach(e => {
						let node = map.get(e.id);
						if (node) {
							let prevLevel = node.level;
							if (minLevel > prevLevel) {
								minLevel = prevLevel;
							}
							let levelY = levelPrevNodeMap.get(prevLevel)
							if (!levelY) {
								levelPrevNodeMap.set(prevLevel, { count: 1, sumY: node.y });
							} else {
								levelY.count = ++levelY.count;
								levelY.sumY = levelY.sumY + node.y;
							}
						}
					})

					levelPrevNodeMap.forEach((e, i) => {
						let y = e.sumY / e.count;
						let subLevel = i - minLevel;
						sumY += y * (subLevel + 1);
						count += subLevel + 1;
					});
					let y = Math.max(count !== 0 ? sumY / count : 0, lastSumY + (i === 0 ? 0 : prevNodes.length === 0 ? 60 : 35));
					if (y - i * 35 - 10 < 30) {
						y = i * 35 + 10;
					}
					e.y = y;
					lastSumY = y;
				});
				if (lastSumY + 55 > maxY) {
					maxY = lastSumY + 55;
				}
			}
		}

		// 免得有些数据没有来源时，第一列和第二列对不齐
		let levelnodes0 = levelNodesMap.get(0);
		let levelnodes1 = levelNodesMap.get(1);
		let lastSumY = 0;
		if (levelnodes1 && levelnodes1.length > levelnodes0.length) {
			levelnodes0.forEach((e, i) => {
				let impactMap = nodesImpactMap.get(e.data.id);
				if (impactMap.size === 1) {
					impactMap.forEach(e1 => {
						e1.node.level === 1 && (lastSumY = e.y = Math.max(e1.node.y, lastSumY + (i === 0 ? 0 : 35)));
					});
				} else {
					lastSumY = e.y = Math.max(e.y, lastSumY + (i === 0 ? 10 : 35));
				}
				if (lastSumY + 55 > maxY) {
					maxY = lastSumY + 55;
				}
			})
		}

		nodes.length = 0;
		levelNodesMap.forEach(e => {
			nodes.pushAll(e);
		});

		// 删除多余添加用于排序属性
		nodes.forEach(e => {
			delete (<any>e).preLen;
			delete (<any>e).preMap;
		});

		this.nodes = nodes;
		nodes.forEach(e => {
			let prevNodes = e.data.prevNodes;
			// e.data.state = DW_TASK_STATE.EXECUTING;
			let isExecuting = e.data.state === DW_RESOURCE_STATE.EXECUTING;
			prevNodes && prevNodes.forEach(e1 => {
				let node = map.get(e1.id);
				if (node) {
					edges.push({ source: node, target: e });
					// 如果当前节点正在执行，那么需要一个表示正在执行动画的连线。
					isExecuting && edges.push({ source: node, target: e, animate: true });
				}
			})
		});
		this.edges = edges;
		this.clientX = level * 250 - 90;
		this.clientY = maxY + 30;//2022-06-16 liuyz 由于定制增加了30的高度，这里补上
	}

	/**
	 * 刷新显示。
	 */
	public refresh(): Promise<FlowDiagram> {
		let dp = this.dataProvider;
		if (!dp || !this.domBase) {
			return Promise.resolve(this);
		}
		return dp.fetchData().then((data) => {
			data = this.filterData(data);
			let nodeInfo = this.findNotReferAnddataFlowResidList(data);
			let dbInfo = this.findDataBaseAndTableNode(data);
			return this.queryDataBaseAndSystemInfo(dbInfo, nodeInfo).then(result => {
				this.addSystemNodeAndDbNode(<SDITaskFlowNodeInfo[]>data, result);
				this.load(data);
				this.layout();
				return this.requestRender();
			})
		});
	}

	/**
	 * 查询所有节点中没有被引用的数据加工节点，以及所有加工节点
	  */
	private findNotReferAnddataFlowResidList(data: TaskFlowNodeInfo[]): { notReferResidList: string[], dataFlowResidList: string[] } {
		let map: { [node: string]: number } = {};
		let notReferResidList: string[] = [];
		let dataFlowResidList: string[] = [];
		data.forEach(e => {
			if (!(e.isDtable && !e.isOutputDbtable) && !e.isFile) {
				let preNodes = e.prevNodes;
				if (map[e.id] == null) {
					map[e.id] = 0;
				}
				preNodes && preNodes.forEach(node => {
					if (map[node.id] == null) {
						map[node.id] = 0;
					}
					map[node.id]++;
				});
				dataFlowResidList.push(e.id);
			}
		});
		let keys = Object.keys(map);
		for (let i = 0; i < keys.length; i++) {
			let key = keys[i];
			let num = map[key];
			if (num == 0) {
				notReferResidList.push(key);
			}
		}
		return { notReferResidList, dataFlowResidList };
	}

	/**
	 * 获取所有数据源节点信息
	 * 
	 * 2022-09-08 liuyz
	 * CSTM-20396 现在要将来源节点的物理表也显示出描述，这里收集前端的物理表信息
	 * @param data
	 */
	private findDataBaseAndTableNode(data: TaskFlowNodeInfo[]): { dbList: string[], dbTableNode: { datasource: string, schema: string, name: string }[] } {
		let dataBase: string[] = [];
		let dbTableNode: { datasource: string, schema: string, name: string }[] = [];
		data.forEach(e => {
			if (e.isDtable && !e.isOutputDbtable) {
				let resInfo = e.resInfo;
				!dataBase.includes(e.dataSource) && dataBase.push(e.dataSource);
				dbTableNode.push({ datasource: resInfo.dataSource, schema: resInfo.schema, name: resInfo.name });
			}
		});
		return { dbList: dataBase, dbTableNode };
	}

	/**
	 * 查询物理表节点来源系统节点信息，以及末尾加工节点数据源信息
	 * @param dbInfo
	 * @param residList
	 */
	private queryDataBaseAndSystemInfo(dbInfo: { dbList: string[]; dbTableNode: { datasource: string, schema: string, name: string }[] }, nodeInfo: { notReferResidList: string[], dataFlowResidList: string[] }): Promise<{ outPutDBNodeList: SDITaskFlowNodeInfo[], systemNodeList: SDITaskFlowNodeInfo[], dataFlowNodeList: JSONObject, dbTableNodeList: JSONObject[] }> {
		return rc({
			url: "/zt/dataScheduleMgr/queryNodeInfo",
			data: {
				dbInfo, nodeInfo
			}
		}).then(result => {
			return result;
		})
	}

	/**
	 * 将系统节点和末端的数据源节点添加到data中
	 * @param data
	 * @param args
	 */
	private addSystemNodeAndDbNode(data: SDITaskFlowNodeInfo[], args: {
		outPutDBNodeList: SDITaskFlowNodeInfo[],
		systemNodeList: SDITaskFlowNodeInfo[],
		dataFlowNodeList: JSONObject,
		dbTableNodeList: JSONObject[]
	}) {
		let { outPutDBNodeList, systemNodeList, dataFlowNodeList, dbTableNodeList } = args;
		let systemNodeListNew = [];
		data.forEach(d => {
			if (d.isDtable && !d.isOutputDbtable) {//如果是物理表节点，那么将对应系统添加进preNodes
				let node = systemNodeList.filter(node => {
					if (node.dataSource == d.dataSource) {
						systemNodeListNew.push(node);
						return true;
					} else {
						return false;
					}
				})
				d.prevNodes = node;
				dbTableNodeList.forEach(node => {
					if (node.id == d.id) {
						d.name = node.desc || d.name;
					}
				})
			} else {
				let resid = d.id;
				outPutDBNodeList.forEach(node => {
					if (node.originResid.indexOf(resid) != -1) {
						node.prevNodes.push(d);
					}
				});
			}
			/**
			 * 如果是加工节点，用描述覆盖掉name
			 * 增加上次状态和表数据
			 */
			let resid = d.id;
			let info = dataFlowNodeList[resid];
			if (d?.log?.type == "etl") {
				if (!isEmpty(info)) {
					d.resInfo.lastState = info.lastState;
					d.resInfo.rowCount = info.rowCount;
				}
			}
			if (!isEmpty(info)) {
				info.desc && (d.name = info.desc)
			}
		});
		data.pushAll(outPutDBNodeList)
		data.pushAll(systemNodeList)
	}

	/**
	 * 2022-06-16 liuyz
	 * 自定义节点显示内容
	 * @override
	 */
	protected doMouseoverNode(node: TaskFlowNode, e: MouseEvent): void {
		let target = <HTMLElement>e.target;
		let data = <SDITaskFlowNodeInfo>node.data;
		if (target.classList.contains("flowdiagram-nodeicon-after")) {
			if (data.state === DW_RESOURCE_STATE.FAIL) {
				let log = <any>data.log;
				showToolTip({ text: log ? message("schedulemgr.dataflow.etltask.error", message(log.errorMessage)) : message("schedulemgr.dataflow.etltask.error.default"), showAt: target });
				return;
			}
			if (data.loopPaths) {
				return;
			}
		}
		showTaskNodeToolip(data, <HTMLElement>target.closest(".flowdiagram-node")).then(tooltip =>
			this.contextMenuVisible && tooltip.hideFloat());
	}
}

/**
 * 2022-06-16 liuyz
 * https://jira.succez.com/browse/CSTM-19170
 * 优化节点显示内容
 */
const TOOLTIP_FORMARGS: Array<FormItemArgs> = [{
	id: "name",
	name: "name",
	formItemType: "link"
}, {
	id: "desc",
	name: "desc",
	className: "nodetip-desc",
	formItemType: "text"
}, {
	id: "path",
	name: "path",
	formItemType: "text"
}, {
	id: "tablePath",
	name: "tablePath",
	formItemType: "text"
}, {
	id: "scheduleInfo",
	name: "scheduleInfo",
	formItemType: "text"
}, {
	id: "dataBaseUser",
	name: "dataBaseUser",
	formItemType: "text",
	caption: "数据库用户"
}, {
	id: "dataBaseInfo",
	name: "dataBaseInfo",
	formItemType: "text",
	caption: "数据库连接信息"
}, {
	id: "dataBaseState",
	name: "dataBaseState",
	formItemType: "text",
	caption: "数据库连接状态"
}, {
	id: "rowCount",
	name: "rowCount",
	formItemType: "text",
	caption: "数据行数"
}, {
	id: "lastState",
	name: "lastState",
	formItemType: "text",
	caption: "上次执行状态"
}]

interface SDITaskFlowNodeInfo extends TaskFlowNodeInfo {
	resInfo: SDIResInfo;
}

interface SDIResInfo extends ResInfo {
	dataBaseInfo: string;
	dataBaseUser: string;
	dataBaseState: string;
	lastState: string;
	rowCount: number;
}

let tooltipFormPromise: Promise<Form>;
function showTaskNodeToolip(node: SDITaskFlowNodeInfo, showAt: HTMLElement) {
	let log = <TaskLogInfo>node.log;
	if (!tooltipFormPromise) {
		tooltipFormPromise = import("commons/form").then(m => {
			return new m.Form({
				keyPrefix: "schedule.tasknode.tooltip.form",
				className: "tasknode-tipform",
				autoCalcCaptionWidth: false,
				items: TOOLTIP_FORMARGS
			})
		})
	}
	return tooltipFormPromise.then(form => {
		let res = node.resInfo;
		let needOutput = !res.dbTable && !res.file && (res.subFileType === ModelDataType.DataFlow || res.subFileType === ModelDataType.Ods);
		let desc = log ? log.resDesc : node.desc;
		let path = node.isDtable || node.isFile ? node.dataSource + "/" + node.schema + "/" + node.name : log ? log.resPath : node.path
		let dataBaseInfo = res.dataBaseInfo;
		let dataBaseUser = res.dataBaseUser;
		let dataBaseState = res.dataBaseState;
		let lastState = res.lastState;
		let rowCount = res.rowCount;
		form.setItemsVisible(["tablePath", "rowCount", "lastState"], !!needOutput);
		form.setItemsVisible(["scheduleInfo"], !!log);
		form.setItemsVisible(["dataBaseInfo", "dataBaseUser", "dataBaseState"], ["system", "dataBase"].includes(node.type));
		form.setItemsVisible(["path"], !["system", "dataBase"].includes(node.type));
		form.setItemVisible("desc", desc != null);
		let promises: Promise<any>[] = [];
		if (needOutput) {
			promises.push(getOutputDbTable(res.path || res.resid));
		}

		return Promise.all(promises).then(t => {
			form.loadData({
				desc: desc,
				path: path,
				tablePath: t[0],
				scheduleInfo: log && log.startTime ? message("schedulemgr.tasknode.scheduleInfo", formatDateFriendly(parseDate(log.startTime)), getElapsedTime(log.elapsedMs)) : "-",
				dataBaseInfo,
				dataBaseUser,
				dataBaseState,
				rowCount,
				lastState
			});
			let link = (<Link>form.getFormItemComp("name"));
			link.setCaption(log ? log.resName : node.name);
			link.setEnabled(false);
			return showToolTip({
				id: "schedulemgr.tasknode.dataflow.nodeinfo",
				delayShow: 500,
				maxWidth: 340,
				component: form,
				showAt: showAt
			});
		})
	})
}
/**
 * 将耗费时间转换成一个小时分钟的友好文字。
 */
function getElapsedTime(elapsedMs: number) {
	let mins = Math.ceil(elapsedMs / 1000 / 60);
	let hour = Math.floor(mins / 60);
	if (hour > 0) {
		let min = mins % 60;
		return message("schedulemgr.log.elapsed.hour", hour, min);
	}

	let seconds = Math.ceil(elapsedMs / 1000);
	let min = Math.floor(seconds / 60);
	let second = seconds % 60;
	return min > 0 ? message("schedulemgr.log.elapsed.minute", min, second) : message("schedulemgr.log.elapsed.second", second);
}
