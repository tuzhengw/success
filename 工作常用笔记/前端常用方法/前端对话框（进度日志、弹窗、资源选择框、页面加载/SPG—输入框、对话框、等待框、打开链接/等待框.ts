import { ctx, SZEvent, Component, throwInfo, throwError, showConfirmDialog, showFormDialog, showErrorMessage, showMenu, isEmpty, deepAssign, uuid, message, quoteExpField, showDialog, showSuccessMessage, showProgressDialog, rc, downloadFile, waitRender, showWaiting, assign, IDataProvider, formatDateFriendly, parseDate, formatDate, tryWait, rc_get, showWarningMessage, timerv, showWaitingMessage, hideMessage, DataChangeEvent, EventListenerManager, GroupInfo, IFindArgs, ServiceTaskPromise, ServiceTaskInfo, ServiceTaskState, wait } from 'sys/sys';
import { IMetaFileCustomJS, InterActionEvent, IAppCustomJS, IDwCustomJS, FAppInterActionEvent, IFApp, IDataset, IFAppForm, IFAppCustomJS } from "metadata/metadata-script-api";
import { showDrillPage, getMetaRepository, MetaFileViewer, getCurrentUser, getSysSettings } from 'metadata/metadata';
import { getDwTableDataManager, labelData, refreshModelState, exportQuery, importData, ImportDataResult, getQueryManager, pushData, importDbTables, DwDataChangeEvent } from 'dw/dwapi';
import { DwPseudoDimName, DwTableModelFieldProvider } from 'dw/dwdps';
import { Combobox, Button, showUploadDialog, Toolbar, ToolbarItemAlign, ComboboxArgs, DataProvider, ButtonArgs, TXT_FILETYPE, IMAGE_FILETYPE, showMessageBox, MessageBoxArgs, MessageContentInfo, MessageBoxType, Edit } from 'commons/basic';
import { ExpDialog, ExpDialogValueInfo, IFieldsDataProvider, showExpDialog } from 'commons/exp/expdialog';
import { ExpCompiler, ExpVar, Token, ExpTokenIndex, IExpDataProvider } from 'commons/exp/expcompiler';
import { ExpContentType } from 'commons/exp/exped';
import { runCheckTask } from 'dg/dgapi';

return checkTaskId && showConfirmDialog({
	message: "确认要执行检测任务吗？",
	onok: (event, dialog) => {
		let promise = runCheckTask(checkTaskId, uuid());
		showProgressDialog({
			caption: "任务执行日志",
			uuid: checkTaskId,
			promise: promise,  // 等待的事件
			buttons: [{
				id: "cancel",
				caption: "关闭"
			}],
			logsVisible: true,
			width: 700,
			height: 500
		});
		return showWaiting(promise).then(() => {
			throwInfo("检测完毕");
		});
	}
})