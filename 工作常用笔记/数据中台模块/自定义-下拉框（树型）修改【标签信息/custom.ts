
import { EditLabelModule } from "./JsComponent/编辑标签模块/editLabelModule.js"

export const CustomJS: { [type: string]: IMetaFileCustomJS } = {
	
	"/ZHJG/app/home.app/JsComponent/编辑标签模块/demo.spg": {
		CustomActions: {
			/**
			 * 20220608 tuzw
			 * 问题：增加对一个对象打上不同标签的功能
			 * 地址：https://jira.succez.com/browse/CSTM-19030
			 * @param jsComponentId js控件Id值
			 */
			button_editLabelInfos: (event: InterActionEvent) => {
				let page = event.page;
				let params = event?.params;
				let jsComponentId: string = params.jsComponentId;
				if (!jsComponentId) {
					showWarningMessage("未给定绑定的JS控件ID值");
					return false;
				}
				let jsComp: EditLabelModule = page.getComponent(jsComponentId).component.getInnerComoponent();
				jsComp.adjustLabelInfo(); // 根据勾选差异，更新当前行标签信息 
			}
		}
	}
}