/**
 * =====================================================
 * 作者：tuzhengw
 * 审核人员：liuyz
 * 创建日期: 2022-05-12
 * 功能描述
 * 		此脚本是数据中台定制树控件后端文件-新增右击菜单功能
 * =====================================================
 */
import metadata, { getString, modifyFile } from "svr-api/metadata";
import db, { getTableAlter } from "svr-api/db";
import fs from "svr-api/fs";
import { getDwTable, refreshState } from "svr-api/dw";
import { getBean } from "svr-api/bean";
/**
 * 调整树节点（新增、修改、删除）
 * @param id 维项代码（主键值）
 * @param modelPath 树绑定的模型路径
 * @param option 当前操作，delete insert update
 * @param caption 节点绑定的文字字段值
 * @param originalId 修改维项代码时原来的代码
 * @param parentId  父节点的ID，用于在某节点下新增子节点
 * @param timePoint  时间点，用于修改缓慢变化的维项
 * @param data 维项的数据
 */
export function adjustTreeNode(request: HttpServletRequest, response: HttpServletResponse, param: {
    id: string,
    modelPath: string;
    option: string;
    caption?: string;
    originalId?: string;
    parentId?: string;
    timePoint?: string;
    data?: {
        [fieldName: string]: string;
    }
}): ResultInfo {
    console.debug(`adjustTreeNode()，调整树节点，参数：`);
    console.debug(param);

    let id: string = param.id;
    let modelPath: string = param.modelPath;
    if (!id || !modelPath) {
        return { result: false, message: "参数错误" }
    }
    let resultInfo: ResultInfo = { result: true };
    let parentId: string = param.parentId;
    let caption: string = param.caption;
    let option: string = param.option;
    let originalId: string = param.originalId;

    switch (option) {
        case "insert": resultInfo = addTreeItemNode(modelPath, id, caption, parentId);
            break;
        case "update": resultInfo = updateTreeItemNode(modelPath, originalId, id, caption);
            break;
        case "delete": resultInfo = removeTreeItemNode(modelPath, id);
            break;
        default:
            resultInfo.result = false;
            resultInfo.message = '当前操作不正确';
    }
    refreshState(modelPath); // 清理后端缓存数据
    return resultInfo;
}

/**
 * 删除指定树节点
 * @param modelPath 树绑定的模型路径
 * @param pkId 删除主键值
 */
function removeTreeItemNode(modelPath: string, pkId: string): ResultInfo {
    let hierarchyFields: string[] = modelUtils.getModelSetting(modelPath);
    if (hierarchyFields.length == 0) {
        return { result: false, message: "模型未设置层级关系" };
    }
    let { pkField } = modelUtils.getModelPkAndDescFiled(modelPath);
    if (!pkField) {
        return { result: false, message: "模型没有设置主键，无法调整节点" };
    }
    let { result, message, table, dataSource } = modelUtils.getDbTableData(modelPath);
    if (!result) {
        return { result: result, message: message };
    }
    let levelField: string = modelUtils.getModelLevelField(modelPath);
    if (levelField == "") {
        return { result: false, message: "模型未设置层级字段" };
    }
    let levelInfo = table.selectFirst("*", `${pkField}='${dataSource.formatConstValue(pkId)}'`);
    if (!levelInfo) {
        return { result: false, message: "当前未找到节点数据，请检查数据表值是否正确" };
    }
    let nodeLevel = levelInfo[levelField];
    console.debug(`当前节点层级为：${nodeLevel}`);
    if (!nodeLevel && nodeLevel != 0) {
        return { result: false, message: "当前节点层级值为空" };
    }
    /** 当前节点对应的层级字段 */
    let currentNodehierarchyField: string = hierarchyFields[Number(nodeLevel)];
    let resultInfo: ResultInfo = { result: true };
    try {
        let deleteWhereField = {};
        deleteWhereField[currentNodehierarchyField] = levelInfo[currentNodehierarchyField];
        table.del(deleteWhereField); // 根据当前节点层级字段 删除节点以及子节点
        console.debug("节点删除成功");
    } catch (e) {
        resultInfo.result = false;
        resultInfo.message = "删除节点失败";
        console.error(`删除树节点失败---error`);
        console.error(e);
    }
    return resultInfo;
}

/**
 * 新增树节点
 * 
 * 层级，先根据父节点 id 获取 层级，然后获取对应层级的 层级字段个数，  再查询 父节点 层级字段对应的值
 * @param modelPath 树绑定的模型路径
 * @param pkId 新增主键值
 * @param caption 主键对应的文字字段值
 * @param parentId 父节点Id
 * @return 
 */
function addTreeItemNode(modelPath: string, pkId: string, caption: string, parentId?: string): ResultInfo {
    console.debug(`addTreeItemNode()，新增树节点`);
    let hierarchyFields: string[] = modelUtils.getModelSetting(modelPath);
    if (hierarchyFields.length == 0) {
        return { result: false, message: "模型未设置层级关系" };
    }
    let resultInfo: ResultInfo = { result: true };
    let { pkField, pkTextField } = modelUtils.getModelPkAndDescFiled(modelPath);
    if (!pkField) {
        return { result: false, message: "模型没有设置主键，无法调整节点" };
    }

    let { result, message, table, dataSource } = modelUtils.getDbTableData(modelPath);
    if (!result) {
        return { result: result, message: message };
    }

    let addNodeIsExistResult = table.selectFirst("*", `${pkField}='${dataSource.formatConstValue(pkId)}'`);
    if (!!addNodeIsExistResult) {
        return { result: false, message: "当前节点已存在，新增失败" };
    }
    resultInfo = generatorInsertTreeNode(table, dataSource, modelPath, pkId, caption, parentId, pkField, pkTextField, hierarchyFields);
    if (!resultInfo.result) {
        return resultInfo;
    }
    try {
        table.insert(resultInfo.data);
        console.debug(`节点插入成功`);
    } catch (e) {
        resultInfo.result = false;
        resultInfo.message = `后台执行异常，新增失败，请联系管理员`;
        console.error(`后台执行异常，新增失败，请联系管理员`);
        console.error(e);
    }
    return resultInfo;
}

/**
 * 构建需要插入的树节点信息
 * @param table 表对象
 * @param dataSource 数据库对象
 * @param modelPath 模型路径
 * @param pkId 新增的节点主键值
 * @param caption 新增节点标题字段值
 * @param parentId 节点对应的父节点ID
 * @param pkField 主键字段名
 * @param pkTextField 主键文字字段名
 * @param hierarchyFields 层级字段数组
 * @return 
 */
function generatorInsertTreeNode(
    table: TableData,
    dataSource: Datasource,
    modelPath: string,
    pkId: string,
    caption: string,
    parentId: string,
    pkField: string,
    pkTextField: string,
    hierarchyFields: string[]
): ResultInfo {
    console.debug(`开始构建需要插入的树节点信息`);
    let insertFiled = {};
    insertFiled[pkField] = pkId;
    if (!!pkTextField) {
        insertFiled[pkTextField] = caption;
    }
    let levelField: string = modelUtils.getModelLevelField(modelPath);
    let nodeLevel: number = 0;
    if (!!parentId) { // 校验当前是否为子节点
        let parentField: string = modelUtils.getModelLevelParentField(modelPath);
        if (!parentField) {
            return { result: false, message: "模型未设置父代码字段，新增节点失败" };
        }
        insertFiled[parentField] = parentId; // 将其父节点存入指定：父代码字段

        let levelInfo = table.selectFirst("*", `${pkField}='${dataSource.formatConstValue(parentId)}'`);
        if (!levelInfo) {
            return { result: false, message: "未找到父节点数据" };
        }
        if (levelField == "") {
            return { result: false, message: "模型未设置层级字段" };
        }
        nodeLevel = levelInfo[levelField];
        console.debug(`当前节点层级为：${nodeLevel}`);
        if (!nodeLevel && nodeLevel != 0) {
            return { result: false, message: "当前节点层级值为空" };
        }
        nodeLevel = Number(nodeLevel) + 1; // 若存在父节点，则当前节点层次+1
        if (nodeLevel >= hierarchyFields.length) {
            return { result: false, message: `当前模型层次高度最大为：${hierarchyFields.length}` };
        }
        /**
         * 依次写入当前节点的各父级层级值，最高层为最外层父节点，eg：
         *      insertFiled["SZ_PID0"] = "001"、 insertFiled["SZ_PID1"] = "002"、...
         */
        for (let i = 0; i < nodeLevel; i++) {
            insertFiled[hierarchyFields[i]] = levelInfo[hierarchyFields[i]];
        }
        insertFiled[hierarchyFields[nodeLevel]] = pkId; // 当前层为当前节点主键值
    } else {
        insertFiled[hierarchyFields[0]] = pkId; // 若当前节点为根节点，第一层字段存自己
    }
    insertFiled[levelField] = nodeLevel;
    console.debug(`当前插入的节点信息为：`);
    console.debug(insertFiled);
    return { result: true, data: insertFiled };
}

/**
 * 修改树节点信息
 * @param modelPath 树绑定的模型路径
 * @param originalId 修改前的主键值
 * @param updateId 修改后主键值
 * @param updateCaption 主键对应的文字字段值
 */
function updateTreeItemNode(modelPath: string, originalId: string, updateId: string, updateCaption: string): ResultInfo {
    console.debug(`updateTreeItemNode()，修改树节点信息`);
    if (!originalId) {
        return { result: false, message: "修改节点未传递修改节点原主键值" };
    }

    let hierarchyFields: string[] = modelUtils.getModelSetting(modelPath);
    if (hierarchyFields.length == 0) {
        return { result: false, message: "模型未设置层级关系" };
    }

    let { result, message, table, dataSource } = modelUtils.getDbTableData(modelPath);
    if (!result) {
        return { result: result, message: message };
    }

    let { pkField, pkTextField } = modelUtils.getModelPkAndDescFiled(modelPath);
    if (!pkField) {
        return { result: false, message: "模型没有设置主键，无法调整节点" };
    }

    let levelField: string = modelUtils.getModelLevelField(modelPath);
    if (levelField == "") {
        return { result: false, message: "模型未设置层级字段" };
    }
    let levelInfo = table.selectFirst([levelField], `${pkField}='${dataSource.formatConstValue(originalId)}'`);
    console.debug(levelInfo);
    if (!levelInfo) {
        return { result: false, message: "当前未找到节点数据，请检查数据表值是否正确" };
    }
    let nodeLevel = levelInfo[levelField];
    console.debug(`当前节点层级为：${nodeLevel}`);
    if (!nodeLevel && nodeLevel != 0) {
        return { result: false, message: "当前节点层级值为空" };
    }
    let resultInfo: ResultInfo = { result: true };
    /** 当前节点对应的层级字段 */
    let currentNodehierarchyField: string = hierarchyFields[Number(nodeLevel)];

    let updateWhereField = {};
    updateWhereField[pkField] = originalId;

    let updateFiled = {};
    updateFiled[pkField] = updateId;
    updateFiled[currentNodehierarchyField] = updateId;
    if (!!pkTextField) {
        updateFiled[pkTextField] = updateCaption;
    }
    try {
        table.update(updateFiled, updateWhereField);
        console.debug("节点修改成功");
    } catch (e) {
        resultInfo.result = false;
        resultInfo.message = "后端脚本执行异常，修改树节点失败";
        console.error(`后端脚本执行异常，修改树节点失败--error`);
        console.error(e);
    }
    return resultInfo;
}


/** 模型表工具类 */
class ModelUtils {

    /**
     * 获取模型的数据库源、schema、物理表名
     * @param modelPath 模型路径 
     * @return 
     */
    public getModelDbProperties(modelPath: string): {
        datasource: string,
        schema: string,
        dbTableName: string
    } {
        if (!modelPath) {
            return null;
        }
        let model: DwTableInfo = metadata.getDwTable(modelPath);
        if (!model) {
            return null;
        }
        let dataSource: string = model.properties && model.properties.datasource;
        let schema: string = model.properties && model.properties.dbSchema;
        let dbTableName: string = model.properties && model.properties.dbTableName;
        if (dataSource == "defdw") { // 默认数据源在配置文件中名字不同，需要校验
            dataSource = "default";
        }
        console.debug(`【${modelPath}】的所在数据源名为：${dataSource}、schema：${schema}、dbTableName：${dbTableName}`);
        return { datasource: dataSource, schema: schema, dbTableName: dbTableName };
    }

    /**
     * 获取模型的主键字段 以及 绑定的 文字描述
     * @param modelPath 模型路径 
     * @return {
     *    pkField: "代码",
     *    pkTextField: "标题"
     * }
     */
    public getModelPkAndDescFiled(modelPath: string): { pkField: string, pkTextField?: string } {
        if (!modelPath) {
            return { pkField: "" };
        }
        let modelMetaData: DwTableInfo = metadata.getDwTable(modelPath);
        let pkName: string = modelMetaData.properties.primaryKeys[0]; // 取第一个主键描述
        if (!pkName) {
            return { pkField: "" };
        }
        let modelColumn: ModelColumn[] = this.getModelFields(modelPath);
        let pkTextField: string = "";
        /** 主键绑定的文字字段描述名 */
        let pkTextFieldDesc: string = "";
        let pkField: string = "";
        for (let i = 0; i < modelColumn.length; i++) {
            if (modelColumn[i].name == pkName) {
                pkField = modelColumn[i].dbfield;
                pkTextFieldDesc = modelColumn[i].textField;
                break;
            }
        }
        for (let i = 0; i < modelColumn.length; i++) {
            if (modelColumn[i].name == pkTextFieldDesc) {
                pkTextField = modelColumn[i].dbfield;
                break;
            }
        }
        console.debug(`模型：【${modelPath}】的主键字段：【${pkField}】 以及 绑定的文字描述：【${pkTextField}】`);
        return { pkField: pkField, pkTextField: pkTextField };
    }

    /**
     * 获取模型表字段信息
     * @param modelPath 模型路径 
     * @return 
     */
    public getModelFields(modelPath: string): ModelColumn[] {
        if (!modelPath) {
            return [];
        }
        let modelMetadata = JSON.parse(metadata.getString(modelPath));
        let dimensions: ModelColumn[] = modelMetadata.dimensions;
        return dimensions;
    }

    /**
     * 获得数据库 某表对象
     * @param modelPath 模型路径
     * @return 
     */
    public getDbTableData(modelPath: string): {
        result: boolean,
        table?: TableData,
        dataSource?: Datasource,
        message?: string
    } {
        let { datasource, schema, dbTableName } = modelUtils.getModelDbProperties(modelPath);
        if (!datasource || !dbTableName) {
            return { result: false, message: "模型没有绑定物理表，无法调整节点" };
        }
        let dataSource: Datasource = db.getDataSource(datasource);
        if (!schema) {
            schema = dataSource.getDefaultSchema();
        }
        let table: TableData = dataSource.openTableData(dbTableName, schema);
        return { result: true, table: table, dataSource: dataSource };
    }

    /**
     * 读取模型设置的第一层级字段数组
     * @param modelPath 模型路径 
     * @return eg：["SZ_PID0", "SZ_PID1"]
     */
    public getModelSetting(modelPath: string): string[] {
        if (!modelPath) {
            return [];
        }
        let modelMetadata = JSON.parse(metadata.getString(modelPath));
        let modelHierarchies: Hierarchies = modelMetadata.hierarchies[0]; // 取第一层 层级
        let hierarchyFields: HierarchyFields[] = modelHierarchies.hierarchyFields;
        let fields: string[] = [];
        for (let i = 0; i < hierarchyFields.length; i++) {
            fields.push(hierarchyFields[i].field);
        }
        console.debug(`读取模型【${modelPath}】的层级字段：${fields}`);
        return fields;
    }

    /**
     * 获取模型设置的层级字段
     * @param modelPath 模型路径 
     * @return eg："SZ_LEVEL"
     */
    public getModelLevelField(modelPath: string): string {
        if (!modelPath) {
            console.debug('getModelLevelField()，参数模型路径为空');
            return "";
        }
        let modelMetadata = JSON.parse(metadata.getString(modelPath));
        let modelHierarchies: Hierarchies = modelMetadata.hierarchies[0]; // 取第一层 层级
        let levelFieldDesc: string = modelHierarchies.levelField;
        if (!levelFieldDesc) {
            console.debug('getModelLevelField()，模型未设置层级');
            return "";
        }
        let modelColumn: ModelColumn[] = this.getModelFields(modelPath);
        let levelField: string = "";
        for (let i = 0; i < modelColumn.length; i++) {
            if (modelColumn[i].name == levelFieldDesc) {
                levelField = modelColumn[i].dbfield;
                break;
            }
        }
        console.debug(`模型：【${modelPath}】设置的层级字段：${levelField}`);
        return levelField;
    }

    /**
     * 获取模型设置的层级 父代码字段
     * @param modelPath 模型路径 
     * @return
     */
    public getModelLevelParentField(modelPath: string): string {
        if (!modelPath) {
            console.debug('getModelLevelParentField()，参数模型路径为空');
            return "";
        }
        let modelMetadata = JSON.parse(metadata.getString(modelPath));
        let modelHierarchies: Hierarchies = modelMetadata.hierarchies[0]; // 取第一层 层级
        let parentFieldDesc: string = modelHierarchies.parentField;
        if (!parentFieldDesc) {
            console.debug('getModelLevelParentField()，模型未设置层级');
            return "";
        }
        let modelColumn: ModelColumn[] = this.getModelFields(modelPath);
        let parentField: string = "";
        for (let i = 0; i < modelColumn.length; i++) {
            if (modelColumn[i].name == parentFieldDesc) {
                parentField = modelColumn[i].dbfield;
                break;
            }
        }
        console.debug(`模型：【${modelPath}】设置的层级父代码字段：${parentField}`);
        return parentField;
    }

}
let modelUtils = new ModelUtils();


/** 返回信息 */
interface ResultInfo {
    /** 返回结果 */
    result: boolean;
    /** 返回消息 */
    message?: string;
    /** 返回的数据  */
    data?: any;
}

/** 模型表元数据字段对象 */
interface ModelColumn {
    /** 描述 */
    name?: string;
    /** 字段文字描述 */
    textField?: string;
    /** 字段类型 */
    dataType?: string;
    /** 字段长度 */
    length?: number;
    /** 是否维表 */
    isDimension?: boolean;
    /** 英文字段名 */
    dbfield?: string;
    /** 关联维表路径 */
    dimensionPath?: string;
    /** 计算字段表达式 */
    exp?: string;
    /** 小数位数 */
    decimal?: number;
    [propname: string]: any;
}

/** 层级对象 */
interface Hierarchies {
    /** 层次id */
    id?: string;
    /** 层级名 */
    name?: string;
    /** 表示是层次，用于和其他类型区分 */
    isHierarchy?: boolean;
    /** 层级类型 */
    hierarchyType?: string;
    /** 层级字段集 */
    hierarchyFields?: Array<HierarchyFields>;
    /** 父字段 */
    parentField?: string;
    /** 子字段*/
    childField?: string;
    /** 层次字段。hierarchyType是pc时有效 */
    levelField?: string;
    /** 记录一行数据是否叶子节点的字段 */
    isLeafField?: string;
    /** 是否开启自动生成层级 */
    autoGenerateHierarchy?: boolean;
    [propname: string]: any;
}

/**  多字段层次字段 */
interface HierarchyFields {
    /** 层级字段描述 */
    desc: string;
    /** 字段名【支持表达式】 */
    field: string;
}