
import Base64 = org.apache.commons.codec.binary.Base64;
import JavaString = java.lang.String;
import StandardCharsets = java.nio.charset.StandardCharsets;
/**
 * 批处理脚本模版
 */
function main() {
    let test = {
        name: "张三",
        age: 12
    };
    let content: JavaString = new JavaString(JSON.stringify(test));
    let contentBytes: number[] = content.getBytes(StandardCharsets.UTF_8)
    print(Base64.encodeBase64String(contentBytes))
}