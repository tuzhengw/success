import { SDILog, LogType, isEmpty, ResultInfo } from "/sysdata/app/zt.app/commons/commons.action";
import dw from "svr-api/dw";
import db from "svr-api/db";
import utils from "svr-api/utils";
import sys from "svr-api/sys";
import fs from "svr-api/fs";


// 地址来源：https://wsbi.succez.com/MHSJZTTEST/HLHT/files?:open=d6DoQPpzBzIAubFGmP9bIF
// 页面：下载链接交互
/**
 * 附件文件工具类
 * <p>
 *   使用场景: 代码构建附件文件以及对应的meta文件
 *   主要事项: 附件前缀跟模型对应的附件字段设置有关, 此处设置: 存储工作空间, 则附件字段值: workdir:2022123014562943201_1.病历概要.xml
 * </p>
 */
class AttachmentFileUtil {

    /**
     * 在指定磁盘路径下创建XML文件
     * @param fileContent 文件字符串内容或者二进制数组
     * @param fileName 文件名, eg: test.xml, XML业务则: (附件流水号+文档名).xml
     * @param attachFieldPrefix 附件字段值前缀, eg: workdir 、default, 默认: workdir
     * @param errorIsDelete? 内部执行失败是否删除文件, 默认: true
     * @return {
     *    result: true,
     *    data: workdir:2022123014562943201_1.病历概要.xml
     * }
     *
     * @description 若文件已存在, 则覆盖
     * @description 文件放置附件目录下, 可通过SPG交互：下载文件 或者 预览文件
     */
    public createAttachFile(
        fileContent: string | number[],
        fileName: string,
        attachFieldPrefix?: string,
        errorIsDelete?: boolean
    ): ResultInfo {
        if (isEmpty(fileContent)) {
            return { result: false, message: `存储的附件内容为:[${fileContent}]为空, 不执行保存磁盘操作` };
        }
        if (isEmpty(fileName)) {
            return { result: false, message: `附件文件名为空, 创建附件文件失败` };
        }
        if (isEmpty(errorIsDelete)) {
            errorIsDelete = true;
        }
        if (isEmpty(attachFieldPrefix)) {
            attachFieldPrefix = "workdir"; // 附件前缀
        }
        let resultInfo: ResultInfo = { result: true };
        let fileByteContent = fileContent as number[];
        if (typeof fileContent == 'string') {
            let attachJavaStr = new java.lang.String(fileContent);
            fileByteContent = attachJavaStr.getBytes();
        }
        let fileSavePath: string = this.getAttachFileSavePath(fileName);
        let attachFile = new java.io.File(fileSavePath);
        if (!attachFile.exists()) {
            let parentFile = attachFile.getParentFile();
            if (!parentFile.exists()) {
                parentFile.mkdirs(); // 若文件父目录不存在，则创建文件以及父文件目录
            }
            attachFile.createNewFile();
        }
        attachFile = fs.getFile(fileSavePath);
        let outStream;
        try {
            this.createAttachmentMetaFile(fileSavePath, fileName, fileByteContent.length);
            outStream = attachFile.openOutputStream();
            outStream.write(fileByteContent);
            console.info(`附件文件: [${fileSavePath}] 保存磁盘成功`);
            /** 文件存储信息, eg: default:文件名.xml */
            let attachFieldValue: string = `${attachFieldPrefix}:${fileName}`;
            resultInfo.data = attachFieldValue;
        } catch (e) {
            errorIsDelete && attachFile.delete();
            resultInfo.result = false;
            resultInfo.message = `附件文件: [${fileSavePath}] 失败`;
            console.error(`附件文件: [${fileSavePath}] 失败----error`);
            console.error(e);
        } finally {
            outStream && outStream.close();
        }
        return resultInfo;
    }

    /**
     * 获取附件文件在磁盘的存储路径
     * @param fileName 附件文件名, eg: test.xml
     * @return 存储的绝对路径, eg: /opt/workdir/clusters-share/file-storage/relativePath/2022110214454335601_1.个人基本健康信息登记.xml
     */
    public getAttachFileSavePath(fileName: string): string {
        let shareDir: string = sys.getClusterShareDir();
        let attachParentDir: string = `${shareDir}/file-storage/relativePath`;
        let fileSavePath: string = `${attachParentDir}/${fileName}`;
        return fileSavePath;
    }

    /**
     * 创建配置文件(txt.xml.meta)以供预览时读取文件对应数据
     * @param filePath 文件所存路径, eg: /afa/ata/text.xml
     * @param fileName 文件名
     * @param fileSize 文件大小
     *
     * @description 存储文件的附件信息（文件名、文件大小、文件路径）
     */
    public createAttachmentMetaFile(filePath: string, fileName: string, fileSize: number): void {
        let metaFilePath: string = `${filePath}.meta`;
        let metaFile = new java.io.File(metaFilePath);
        if (!metaFile.exists()) {
            let createResult: boolean = metaFile.createNewFile();
            console.info(`文件[${metaFilePath}] 创建状态: ${createResult}`);
        }
        metaFile = fs.getFile(metaFilePath);
        metaFile.writeJson({
            name: fileName,
            size: fileSize,
            lastModifyTime: new Timestamp(Date.now())
        });
        console.info("创建配置文件(txt.xml.meta)以供预览时读取文件对应数据--完成");
    }
}
/** 附件文件工具示例 */
const attachmentFileUtil = new AttachmentFileUtil();