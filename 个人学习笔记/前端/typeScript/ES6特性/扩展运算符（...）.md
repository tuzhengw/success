---
`typora-root-url: images
---

# ES6扩展运算符

> 参考网址：

# 1 ...

​	扩展运算符（`...`）：**用于取出参数对象中的所有可遍历属性，拷贝到当前对象之中**

> 参考网址：https://www.cnblogs.com/aidixie/p/11765708.html

## 1.1 展开操作符

```ts
let fruits: Array<string> =['apple','balan','organize'];
// [ 'apple', 'balan', 'organize' ]
console.log(fruits); 
// apple balan organize
console.log(...fruits);

let test: Array<string> = [...fruits]; 
// [ 'apple', 'balan', 'organize' ]
console.log(test); 			
```

----

## 1.2 剩余操作符

```ts
//剩余操作符
function breakfase (desert,drink,...foods){
　　console.log(desert) // hi
　　console.log(drink) // hello
　　console.log(foods) // ["ye", "good", "nihao"]
};
breakfase('hi','hello','ye','good','nihao');
```

## 1.3 `...`与map

**将 Map 转 Array：**

```ts
// 将二维数组转换成：map
let myMap1 = new Map([["key1", "value1"], ["key2", "value2"]]);
console.log(myMap1);
// 用...操作符，将 Map 转 Array
let myArray = [...myMap1];
// [["key1", "value1"], ["key2", "value2"]]
console.log((myArray));
```

----

​	**案例**

```ts
let myMap = new Map();
myMap.set("key1", "value1");
myMap.set("key2", "value2");
/**
 * 1. 将map转换为二维数组，然后遍历map
 * 	  [["key1", "value1"], ["key2", "value2"]]
 * 2. 按照返回条件，返回一个新的字符串数组
 *	  key1=value1, key2=value2
 */
let result: Array<string> = [...myMap].map(item => {
	let key = item[0];
	let value = item[1];
	return `${key}=${value}`;
});
// [ 'key1=value1', 'key2=value2' ]
console.log((result));
```

## 1.4 `...`与set

​	用法跟map一致，不过set集合，元素唯一

​	**注意：**

- +0 与 -0 在存储判断唯一性的时候是恒等的，所以不重复；
- undefined 与 undefined 是恒等的，所以不重复；
- `NaN `与 `NaN `是不恒等的，但是在 Set 中只能存一个，不重复。

# 2 `Array.from`

> 参考网址：https://www.runoob.com/jsref/jsref-from.html
>

## 2.1 语法

​	`form()`：用于通过<button>拥有 length 属性的对象</button>或<button>可迭代的对象</button>来返回一个数组。

```ts
/*
 * object：必需，要转换为数组的对象
 * mapFunction：	可选，数组中每个元素要调用的函数
 * thisValue：可选，映射函数(mapFunction)中的 this 对象
 */
Array.from(object, mapFunction, thisValue)
```

## 2.2 示例

### 	* 将字符串对象转换为字符串数组

```ts
let myArr = Array.from("RUNOOB");
// [ 'R', 'U', 'N', 'O', 'O', 'B' ]
console.log(myArr);
// 遍历下标
for(let element in myArr){
    //  1, 2, 3, 4, 5, 6
    console.log((element));
}
// 遍历元素
for(let element of myArr){
    //  'R', 'U', 'N', 'O', 'O', 'B' 
    console.log(element); 
}
```

### 	* 将set集合转换为数组

```ts
let setObj = new Set(["a", "b", "c"]);
let objArr = Array.from(setObj);
objArr[1] == "b";  // true
```

### 	*	映射函数更改元素的值

```ts
let arr = Array.from([1, 2, 3], x => x * 10);
// arr[0] == 10;
// arr[1] == 20;
// arr[2] == 30;
```

### 	*	数组去重

```ts
// 数组转换为set集合的过程中，内部会自己将重复的数值去掉
let arr = Array.from(new Set([1, 2, 1, 2]))
console.log(arr) //	[1, 2]
```



# 3 [...] 和 {...}

​	转：数组

```
let arr = [1, 2, 3];
let res = [...arr];
print(res); // [1, 2, 3]
*/
```

​	转：对象

```ts
let arr = [1, 2, 3];
let res = {...arr};
print(res); 
/*
{
	"0": 1,
	"1": 2,
	"2": 3
}
*/
```